object FrameAgent: TFrameAgent
  Left = 0
  Top = 0
  Width = 595
  Height = 117
  TabOrder = 0
  object Label_Appointment: TLabel
    Left = 10
    Top = 44
    Width = 57
    Height = 13
    Hint = 
      #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1102#1088#1080#1076#1080#1095#1077#1089#1082#1086#1075#1086' '#1083#1080#1094#1072', '#1077#1089#1083#1080' '#1082#1072#1076#1072#1089#1090#1088#1086#1074#1099#1081' '#1080#1085#1078#1077#1085#1077#1088' '#1103#1074#1083#1103#1077#1090#1089 +
      #1103' '#1088#1072#1073#1086#1090#1085#1080#1082#1086#1084' '#1102#1088#1080#1076#1080#1095#1077#1089#1082#1086#1075#1086' '#1083#1080#1094#1072
    Caption = #1044#1086#1083#1078#1085#1086#1089#1090#1100
  end
  object Label1: TLabel
    Left = 12
    Top = 4
    Width = 50
    Height = 13
    Caption = #1060#1072#1084#1080#1083#1080#1103
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 210
    Top = 4
    Width = 23
    Height = 13
    Caption = #1048#1084#1103
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 391
    Top = 3
    Width = 49
    Height = 13
    Caption = #1054#1090#1095#1077#1089#1090#1074#1086
  end
  object FIOSurname_Edit: TEdit
    Left = 11
    Top = 23
    Width = 193
    Height = 21
    BevelKind = bkFlat
    BorderStyle = bsNone
    Color = 15921123
    MaxLength = 45
    TabOrder = 0
    TextHint = #1060#1072#1084#1080#1083#1080#1103
    StyleElements = []
    OnExit = FIOSurname_EditExit
  end
  object FIOFirst_Edit: TEdit
    Left = 210
    Top = 23
    Width = 180
    Height = 21
    BevelKind = bkFlat
    BorderStyle = bsNone
    Color = 15921123
    MaxLength = 255
    TabOrder = 1
    TextHint = #1048#1084#1103
    StyleElements = []
    OnExit = FIOFirst_EditExit
  end
  object FIOPatronymic_Edit: TEdit
    Left = 391
    Top = 23
    Width = 166
    Height = 21
    BevelKind = bkFlat
    BorderStyle = bsNone
    Color = clWhite
    MaxLength = 45
    TabOrder = 2
    StyleElements = []
    OnExit = FIOPatronymic_EditExit
  end
  object mmoAppointMent: TMemo
    Left = 11
    Top = 58
    Width = 546
    Height = 47
    Hint = #1044#1086#1083#1078#1085#1086#1089#1090#1100
    BevelKind = bkFlat
    BorderStyle = bsNone
    Color = clWhite
    Lines.Strings = (
      '')
    MaxLength = 255
    ParentShowHint = False
    ShowHint = True
    TabOrder = 3
    WantTabs = True
    OnExit = Appointment_EditEnter
  end
end
