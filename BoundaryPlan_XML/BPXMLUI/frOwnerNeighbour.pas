unit frOwnerNeighbour;

{
}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ComCtrls,

  STD_MP, ActnList, XMLIntf,
  System.Actions,frDataDocuments, unRussLang;

type
  TFrameOwnerNeighbour = class(TFrame)
    ActionList: TActionList;

    ActionChange_Attribute_DefOrNumPP: TAction;
    ActionChange_ExistingOrNew: TAction;
    Attribute_Panel: TPanel;
    Attribute_NameOwner_Label: TLabel;
    Attribute_NameOwner: TEdit;
    Area_Panel: TPanel;
    Attribute_ContactAddress_Lab: TLabel;
    Attribute_ContactAddress: TEdit;
    ActionChange_Attribute_ContactAdress: TAction;
    grpDocuments: TGroupBox;
    procedure ActionChange_Attribute_DefOrNumPPExecute(Sender: TObject);
    procedure Attribute_NameOwnerKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Attribute_NumberPP_EditKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Attribute_ContactAddressExit(Sender: TObject);
    procedure ActionChange_Attribute_ContactAdressExecute(Sender: TObject);
    procedure Attribute_PanelExit(Sender: TObject);
  private
    { Private declarations }
    FDocument         :  TFrameDataDocuments;
    FXmlOwnerNeighbour: IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbour;

    FOnChange_Attribute_DefOrNum: TNotifyEvent;
    FOnChange_Attribute_ContactAdress: TNotifyEvent;

    procedure Data_attribute_NumberPP_write;
    procedure Data_attribute_NumberPP_read;

    procedure Data_attribute_ContactAdress_write;
    procedure Data_attribute_ContactAdress_read;


    procedure Data_DoRead;
    procedure Data_DoWrite;

  protected
    procedure SetContourInvariable (const aContourInvariable  : IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbour);
    procedure InitializationUIInvariable;
  public
    { Public declarations }
    constructor Create(Owner: TComponent); override;
    function IsAssignedContourIvariable: boolean;
    procedure AsExistingContour();
    procedure AsExistingContour_ContactAdress();

    property XmlOwnerNeighbour : IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbour
            read FXmlOwnerNeighbour write SetContourInvariable;
    property OnChange_Attribute_DefOnNum: TNotifyEvent read FOnChange_Attribute_DefOrNum write FOnChange_Attribute_DefOrNum;
    property OnChange_Attribute_ContactAdress: TNotifyEvent read FOnChange_Attribute_ContactAdress write FOnChange_Attribute_ContactAdress;
   end;

implementation

uses MsXMLAPI;

{$R *.dfm}

procedure TFrameOwnerNeighbour.ActionChange_Attribute_ContactAdressExecute(
  Sender: TObject);
begin
 if not self.IsAssignedContourIvariable then begin
  self.Attribute_ContactAddress.Text:= '';
  raise Exception.Create(ExMSG_NotAssignedROOT);
 end;
 self.Data_attribute_ContactAdress_write;

 if Assigned(self.FOnChange_Attribute_ContactAdress) then
  self.FOnChange_Attribute_ContactAdress(self);
end;

procedure TFrameOwnerNeighbour.ActionChange_Attribute_DefOrNumPPExecute(Sender: TObject);
begin
 if not self.IsAssignedContourIvariable then
 begin
  self.Attribute_NameOwner.Text:= '';
  raise Exception.Create(ExMSG_NotAssignedROOT);
 end;
 self.Data_attribute_NumberPP_write;
 if Assigned(self.FOnChange_Attribute_DefOrNum) then
  self.FOnChange_Attribute_DefOrNum(self);
end;

procedure TFrameOwnerNeighbour.AsExistingContour();
var
 _Def: string;
begin
 //�������������� ����������
 if self.IsAssignedContourIvariable then begin
  _Def:=  MsXml_ReadChildNodeValue(self.FXmlOwnerNeighbour,'NameOwner');
  MsXml_RemoveChildNode(self.FXmlOwnerNeighbour,'NameOwner');
  if _Def<>'' then begin
   FXmlOwnerNeighbour.NameOwner := _Def;
   self.Attribute_NameOwner.Text:= _Def;
  end else
   self.Attribute_NameOwner.Text:= self.FXmlOwnerNeighbour.NameOwner;
 end;
end;


procedure TFrameOwnerNeighbour.AsExistingContour_ContactAdress;
var
 _Def: string;
begin
 if self.IsAssignedContourIvariable then begin
  _Def:=  MsXml_ReadChildNodeValue(self.FXmlOwnerNeighbour,'ContactAddress');
  MsXml_RemoveChildNode(self.FXmlOwnerNeighbour,'ContactAddress');
  if _Def<>'' then begin
   FXmlOwnerNeighbour.ContactAddress := _Def;
   self.Attribute_ContactAddress.Text:= _Def;
  end else
   self.Attribute_ContactAddress.Text:= self.FXmlOwnerNeighbour.ContactAddress;
 end;

end;

procedure TFrameOwnerNeighbour.Attribute_ContactAddressExit(Sender: TObject);
begin
 if not self.IsAssignedContourIvariable then begin
  self.Attribute_ContactAddress.Text:= '';
  raise Exception.Create(ExMSG_NotAssignedROOT);
 end;
 self.Data_attribute_ContactAdress_write;

 if Assigned(self.FOnChange_Attribute_ContactAdress) then
  self.FOnChange_Attribute_ContactAdress(self);
end;

procedure TFrameOwnerNeighbour.Attribute_NameOwnerKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
 if key = VK_RETURN then
  if (Sender is TCustomEdit) then
   TCustomEdit(Sender).Parent.SetFocus;
end;

procedure TFrameOwnerNeighbour.Attribute_NumberPP_EditKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
 if key = VK_RETURN then
  if (Sender is TCustomEdit) then
   TCustomEdit(Sender).Parent.SetFocus;
end;

procedure TFrameOwnerNeighbour.Attribute_PanelExit(Sender: TObject);
begin
  Data_attribute_ContactAdress_write;
end;

constructor TFrameOwnerNeighbour.Create(Owner: TComponent);
begin
  inherited;
  self.FDocument:= TFrameDataDocuments.Create(self.Area_Panel);
  self.FDocument.Parent:= self.Area_Panel;
  self.FDocument.Align:= alClient;
end;


procedure TFrameOwnerNeighbour.InitializationUIInvariable;
begin
 if self.IsAssignedContourIvariable then begin
   self.Data_DoRead;
   self.FDocument.XmlOwnerNeighbour:= self.FXmlOwnerNeighbour.Documents;
 end else begin
   self.Attribute_NameOwner.Text:= '';
   self.Attribute_ContactAddress.Text :='';
   self.FDocument.XmlOwnerNeighbour:= nil;
 end;
end;


function TFrameOwnerNeighbour.IsAssignedContourIvariable: boolean;
begin
 Result:= self.FXmlOwnerNeighbour<>nil;
end;


procedure TFrameOwnerNeighbour.SetContourInvariable(
  const aContourInvariable: IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbour);
begin
 if self.FXmlOwnerNeighbour = aContourInvariable then EXIT;
 self.FXmlOwnerNeighbour:= aContourInvariable;
 self.InitializationUIInvariable;
end;


procedure TFrameOwnerNeighbour.Data_attribute_ContactAdress_read;
var
 _node: IXMLNode;
begin
 _node:= self.FXmlOwnerNeighbour.ChildNodes.FindNode('ContactAddress');
 if (_node<>nil) and (_node.NodeValue<>Null) then
  self.AsExistingContour_ContactAdress()
 else
  Attribute_ContactAddress.Text := '';
end;

procedure TFrameOwnerNeighbour.Data_attribute_ContactAdress_write;
begin
 self.FXmlOwnerNeighbour.ContactAddress:= self.Attribute_ContactAddress.Text;
end;

procedure TFrameOwnerNeighbour.Data_attribute_NumberPP_read;
var
 _node: IXMLNode;
begin
 _node:= self.FXmlOwnerNeighbour.ChildNodes.FindNode('NameOwner');
 if (_node<>nil) and (_node.NodeValue<>Null) then
  self.AsExistingContour()
 else Attribute_NameOwner.Text := '';
end;

procedure TFrameOwnerNeighbour.Data_attribute_NumberPP_write;
begin
 self.FXmlOwnerNeighbour.NameOwner:= self.Attribute_NameOwner.Text;
end;

procedure TFrameOwnerNeighbour.Data_DoRead;
begin
 self.Data_attribute_NumberPP_read;
 self.Data_attribute_ContactAdress_read;
end;


procedure TFrameOwnerNeighbour.Data_DoWrite;
begin
 self.Data_attribute_NumberPP_write;
 self.Data_attribute_ContactAdress_write;
end;


end.

