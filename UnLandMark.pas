unit UnLandMark;

///
/// STD_MP 04 Version
///

interface

uses
  Windows, SysUtils, Forms, System.Classes, Dialogs, Controls, StdCtrls,FileCtrl, ShellAPI,
  Vcl.Ribbon, Vcl.RibbonLunaStyleActnCtrls, Vcl.RibbonSilverStyleActnCtrls, Vcl.clipbrd,
  Vcl.AppEvnts, System.SyncObjs, Vcl.ExtDlgs,
  System.Actions, Vcl.ActnList, Vcl.ActnMan, Vcl.ImgList,  JvExControls,
  JvGradientHeaderPanel, Vcl.Imaging.jpeg, JvLabel, Vcl.Graphics, JvImage, JvBitmapButton,
  Vcl.ComCtrls, JvgLabel, Vcl.ExtCtrls, JvExExtCtrls, JvSpecialImage,
  JvComponentBase,  IOUtils,

  BoundaryPlanXML,
  fmSelectionList,
  unRussLang,
  fmcontainer,
  UnLandMarkLOGO,
  FrPreloader,
  SplashForm,
   {zip}
  fmTextInf,
  JclDebug,
  CodeSiteLogging, Vcl.Imaging.pngimage, Vcl.ActnMenus, Vcl.RibbonActnMenus,
  YouTrack.Intf, {BugTraking}
  Vcl.OleCtrls, SHDocVw,
  Config, unInternet, JvLogFile,
  JvBaseDlg,
  JvBrowseFolder, cxControls,
  dxRibbon, dxBar, cxClasses, dxRibbonBackstageView, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, dxRibbonSkins, FWBackStageButton, JvAppStorage,
  JvAppIniStorage, Vcl.PlatformDefaultStyleActnCtrls, dxSkinsCore, dxSkinBlack,
  dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013White,
  dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinTheAsphaltWorld, dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010,
  dxSkinWhiteprint, dxSkinXmas2008Blue, dxSkinsdxRibbonPainter,
  dxSkinsdxBarPainter;  //,JclHookExcept;{TODO: -- ��������� }

type
  TFrLandMark = class(TForm)
    IL_old: TImageList;
    ActionManager: TActionManager;
    actNewPlan_SpecifyParcel: TAction;
    actNewPlan_SpecifyParcel_EZMode: TAction;
    actNewPlan_FormParcels: TAction;
    actOpenPlan: TAction;
    actClosePlan: TAction;
    actSaveAsPlan: TAction;
    actValidate: TAction;
    actBuildPackage: TAction;
    pgcMain: TPageControl;
    tsDescription: TTabSheet;
    tsContent: TTabSheet;
    actCheckAppliedFiles: TAction;
    aplctnvnts1: TApplicationEvents;
    act_NewParcel: TAction;
    act_ChangeParcel: TAction;
    act_FPSpecifyRelatedParcel: TAction;
    act_ExistParcel: TAction;
    act_SPSpecifyRelatedParcel: TAction;
    act_ExistEZParcels: TAction;
    act_ExistEZEntryParcels: TAction;
    act_SPESpecifyRelatedParcel: TAction;
    act_Contractor: TAction;
    act_Client: TAction;
    act_Documentation: TAction;
    act_Conclusion: TAction;
    act_InputDate: TAction;
    img1: TJvSpecialImage;
    btn4: TJvBitmapButton;
    btn5: TJvBitmapButton;
    img3: TJvImage;
    img4: TJvImage;
    jvlbl1: TJvLabel;
    imgLayer: TImage;
    jvglbl1: TJvgLabel;
    jvgrdnthdrpnlOpenXML: TJvGradientHeaderPanel;
    dlgOpenProj: TOpenTextFileDialog;
    img2: TImage;
    actNewSubParcel: TAction;
    act_NewSubParcel: TAction;
    act_NewSubParcels: TAction;
    actCopyFileXML: TAction;
    actHelp: TAction;
    wb1: TWebBrowser;
    actHelpSpecifyRelated: TAction;
    actSupport: TAction;
    act_Settings: TAction;
    wb2: TWebBrowser;
    jvlgfl: TJvLogFile;
    jvbrwsfrfldrdlg: TJvBrowseForFolderDialog;
    actRecentOpen: TAction;
    dxbrmngr1: TdxBarManager;
    dxbrlrgbtn1: TdxBarLargeButton;
    dxbrlrgbtn2: TdxBarLargeButton;
    dxbrlrgbtn3: TdxBarLargeButton;
    dxbrbtn1: TdxBarButton;
    dxbrlrgbtn4: TdxBarLargeButton;
    dxbrlrgbtn5: TdxBarLargeButton;
    dxbrlrgbtn6: TdxBarLargeButton;
    dxrbn1: TdxRibbon;
    dxrbntbRibbon1Tab: TdxRibbonTab;
    dxrbntbdxrbn1TabInfo: TdxRibbonTab;
    dxrbntbdxrbn1TabMP: TdxRibbonTab;
    dxbrlrgbtnSpecifyRelated: TdxBarLargeButton;
    dxbrsbtmFormParcels: TdxBarSubItem;
    dxbrbtn2: TdxBarButton;
    dxbrsbtm1: TdxBarSubItem;
    dxbrsbtm2: TdxBarSubItem;
    dxbrsbtm3: TdxBarSubItem;
    dxbrsbtmSpecify: TdxBarSubItem;
    dxbrsbtm4: TdxBarSubItem;
    dxbrsbtm5: TdxBarSubItem;
    dxbrmngr1Bar2: TdxBar;
    dxbrbtn3: TdxBarButton;
    dxbrlrgbtnOpen: TdxBarLargeButton;
    dxbrsbtm6: TdxBarSubItem;
    dxbrsbtm7: TdxBarSubItem;
    dxbrsbtm8: TdxBarSubItem;
    dxbrlrgbtn7: TdxBarLargeButton;
    dxbrmngr1Bar3: TdxBar;
    dxbrlrgbtn8: TdxBarLargeButton;
    dxbrsbtm9: TdxBarSubItem;
    dxbrlrgbtn9: TdxBarLargeButton;
    dxbrmngr1Bar4: TdxBar;
    dxbrsbtm10: TdxBarSubItem;
    dxbrsbtm11: TdxBarSubItem;
    dxbrsbtm12: TdxBarSubItem;
    dxbrsbtm13: TdxBarSubItem;
    dxbrmngr1Bar5: TdxBar;
    dxbrlrgbtn10: TdxBarLargeButton;
    dxbrlrgbtn11: TdxBarLargeButton;
    dxbrlrgbtn12: TdxBarLargeButton;
    dxbrlrgbtn13: TdxBarLargeButton;
    dxbrlrgbtn14: TdxBarLargeButton;
    dxbrmngr1Bar6: TdxBar;
    dxbrmngr1Bar7: TdxBar;
    dxbrmngr1Bar8: TdxBar;
    dxbrmngr1Bar9: TdxBar;
    dxbrsbtm14: TdxBarSubItem;
    dxbrbtn4: TdxBarButton;
    dxbrbtn5: TdxBarButton;
    dxbrbtn6: TdxBarButton;
    dxbrbtn7: TdxBarButton;
    dxbrbtn8: TdxBarButton;
    dxbrbtn9: TdxBarButton;
    dxbrlrgbtn15: TdxBarLargeButton;
    dxbrbtn10: TdxBarButton;
    dxbrbtn11: TdxBarButton;
    dxbrlrgbtn16: TdxBarLargeButton;
    dxbrlrgbtn17: TdxBarLargeButton;
    dxbrlrgbtn18: TdxBarLargeButton;
    dxbrlrgbtn19: TdxBarLargeButton;
    dxbrlrgbtn20: TdxBarLargeButton;
    dxbrlrgbtn21: TdxBarLargeButton;
    dxbrlrgbtn22: TdxBarLargeButton;
    dxbrlrgbtn23: TdxBarLargeButton;
    dxbrlrgbtn24: TdxBarLargeButton;
    dxbrlrgbtn25: TdxBarLargeButton;
    dxbrlrgbtn26: TdxBarLargeButton;
    dxbrlrgbtn27: TdxBarLargeButton;
    dxbrlrgbtn28: TdxBarLargeButton;
    dxbrlrgbtn29: TdxBarLargeButton;
    dxRibbonBackstageView: TdxRibbonBackstageView;
    dxrbnbckstgvwtbsht2: TdxRibbonBackstageViewTabSheet;
    lbl1: TLabel;
    pb2: TPaintBox;
    dxrbnbckstgvwtbshtPrinters: TdxRibbonBackstageViewTabSheet;
    lbl13: TLabel;
    pb7: TPaintBox;
    lbl14: TLabel;
    pb8: TPaintBox;
    lbl15: TLabel;
    edt2: TEdit;
    ud1: TUpDown;
    dxrbnbckstgvwtbsht4: TdxRibbonBackstageViewTabSheet;
    pb9: TPaintBox;
    lbl16: TLabel;
    fwbckstgbtn4: TFWBackStageButton;
    fwbckstgbtn1: TFWBackStageButton;
    fwbckstgbtn2: TFWBackStageButton;
    fwbckstgbtn3: TFWBackStageButton;
    fwbckstgbtn5: TFWBackStageButton;
    dxbrbtn12: TdxBarButton;
    dxbrlrgbtnExporttoWord: TdxBarLargeButton;
    actExporttoWord: TAction;

    procedure FormCreate(Sender: TObject);
    procedure LbCreateClick(Sender: TObject);
    procedure LbOpenClick(Sender: TObject);
    procedure lbValidateClick(Sender: TObject);
    procedure LbCreateMouseLeave(Sender: TObject);
    procedure LbCreateMouseMove(Sender: TObject; Shift: TShiftState;
      X, Y: Integer);
    procedure lblXMLClick(Sender: TObject);
    procedure lblSaveAsClick(Sender: TObject);
    procedure lblZIpClick(Sender: TObject);
    procedure imgSpecifyrelatedClick(Sender: TObject);
    procedure ImgCloseClick(Sender: TObject);
    procedure lblCopyPackageClick(Sender: TObject);
    procedure imgFormParclsClick(Sender: TObject);
    procedure imgExistEZParcelsClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure actOpenPlanExecute(Sender: TObject);
    procedure actClosePlanExecute(Sender: TObject);
    procedure actNewPlan_SpecifyParcelExecute(Sender: TObject);
    procedure actNewPlan_FormParcelsExecute(Sender: TObject);
    procedure actNewPlan_SpecifyParcel_EZModeExecute(Sender: TObject);
    procedure actSaveAsPlanExecute(Sender: TObject);
    procedure actValidateExecute(Sender: TObject);
    procedure actBuildPackageExecute(Sender: TObject);
    procedure actCreateZIPExecute(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure actCheckAppliedFilesExecute(Sender: TObject);
    procedure aplctnvnts1Exception(Sender: TObject; E: Exception);
    procedure act_SPESpecifyRelatedParcelExecute(Sender: TObject);

    procedure actSTD_MPExecute(Sender : TObject);
    procedure img2OptionsChanged(Sender: TObject);
    procedure actCopyFileXMLExecute(Sender: TObject);
    procedure actNewSubParcelExecute(Sender: TObject);
    procedure act_NewSubParcelExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure act_NewSubParcelsExecute(Sender: TObject);
    procedure actHelpExecute(Sender: TObject);
    procedure btn2Click(Sender: TObject);
    procedure actHelpSpecifyRelatedExecute(Sender: TObject);
    procedure actSupportExecute(Sender: TObject);
    procedure act_SettingsExecute(Sender: TObject);
    procedure btn3Click(Sender: TObject);
    procedure jvcptnbtn1Click(Sender: TObject);
    procedure dxrbn1TabChanging(Sender: TdxCustomRibbon; ANewTab: TdxRibbonTab;
      var Allow: Boolean);
    procedure actExporttoWordExecute(Sender: TObject);  //TODO:////
  private
    { Private declarations }
    FBoundaryPlanXML             : TMSBoundaryPlanXML;
    Fsplash                      : TSplash<TFrLOGO>;
    FYouTrack                    : IBagTracker;

    procedure CheckEnable;
    procedure CheckForms;
    procedure ShowPageDescription;
    procedure OnChangeActivPageData(Sender : TObject);
  public
    CheckEvent : TEvent;
    ClassRef   : TClass;
    { Public declarations }
  end;

var
  FrLandMark: TFrLandMark;

implementation

uses BpXML_different, SurveyingPlan.DataFormat, SurveyingPlan.Xml.Types,  TypInfo,
     FmListApplidFiles,ShlObj;

{$R *.dfm}

{ create }
procedure TFrLandMark.actBuildPackageExecute(Sender: TObject);
var
  savePath: string;
begin
  self.jvlgfl.Add(DateTimeToStr(Now), 'actBuildPackage', '' );
  if jvbrwsfrfldrdlg.Execute then
  begin
    try
      TFrLoader.Inctance.RunAnimation := true;
      FBoundaryPlanXML.CopyPackage(TSettingPathFile.MakeTempPath + '\',jvbrwsfrfldrdlg.Directory);
    finally
      TFrLoader.Inctance.RunAnimation := false;
    end;
      {��������!}
      MessageDlg('�������� ���������� �������. ����� �����������! ("�������� ������������ �����")',mtInformation,[mbOK],-1);
  end;  
  self.CheckEnable;
end;

{������ ������������ ������}
procedure TFrLandMark.actCheckAppliedFilesExecute(Sender: TObject);
begin
  self.jvlgfl.Add(DateTimeToStr(Now), 'act �������� ������������ ������', '');
  FmListApplidFiles.FrmListAppliedFiles.ShowListAplpiedFiles;
  self.CheckEnable;
end;

procedure TFrLandMark.actClosePlanExecute(Sender: TObject);
begin
  self.jvlgfl.Add(DateTimeToStr(Now), 'actClosePlan', '');
  if MessageDlg(rs�hangesWillbeLost, mtConfirmation, mbYesNo, -1) = mrYes then self.FBoundaryPlanXML.DoClose;
  self.CheckEnable;
  self.CheckForms;
end;

procedure TFrLandMark.actCopyFileXMLExecute(Sender: TObject);
var savePath : string;
begin
   self.jvlgfl.Add(DateTimeToStr(Now), 'actCopyFileXMLExecute', '');
  if FileCtrl.SelectDirectory('�����  ����������', '', savePath,
   [sdValidateDir,sdNewUI],self) then
  begin
    FBoundaryPlanXML.CopyFileXMl(savePath+'\');
    MessageDlg('���� - xml ������� �������� (*��� ������������ ������!)',mtInformation,[mbOK],-1);
  end;
  self.CheckEnable;
end;

procedure TFrLandMark.actCreateZIPExecute(Sender: TObject);
var
  SaveDlg: TSaveDialog;
  LogOut: TFormTextInf;
begin
  self.jvlgfl.Add(DateTimeToStr(Now), 'actCreateZIPExecute', '');

  SaveDlg := TSaveDialog.Create(nil);
  SaveDlg.Filter := '*.zip|*.ZIP';
  if SaveDlg.Execute then
  begin
    { ��������� xml }
    Application.CreateForm(TFormTextInf, LogOut);
    LogOut.Caption := '��������� �������� ������ ������� XML!';
    if not self.FBoundaryPlanXML.ValidData(LogOut.lstValidatorLog.Items) then
      LogOut.ShowModal
  end;
  SaveDlg.Free;
  self.CheckEnable;
end;

procedure TFrLandMark.actExporttoWordExecute(Sender: TObject);
var nameDoc : string;
     seError: Cardinal;
begin
  self.jvlgfl.Add(DateTimeToStr(Now), '������������ ���� ���������', '');
  nameDoc := '';
  nameDoc :=  self.FBoundaryPlanXML.ExportXmltoWord('');
  if nameDoc <> '' then
    begin
        seError := Shellexecute(Handle,'Open',PChar(nameDoc),nil,nil,sw_restore);
        if seError<=32 then
        ShowMessage('������ �������� �����. ��� ������: '+IntToStr(seError));
    end
  else ShowMessage('�������� �������������� �������������� ��� ������������ Word ���������!');
end;

procedure TFrLandMark.actHelpExecute(Sender: TObject);
var errorCode : Integer;
begin
    self.jvlgfl.Add(DateTimeToStr(Now), 'actHelp', '');

    CodeSite.EnterMethod(self,'Help');
    CodeSite.Send(ExtractFilePath(Application.ExeName));
    if ShellExecute(0, 'Open', PChar(ExtractFilePath(Application.ExeName)+'\HELP\������ ������� ������.pdf'), nil, nil, SW_RESTORE) < 32 then
      raise Exception.Create('���� ������� �� ������!');;  
    CodeSite.ExitMethod(self,'Help');
end;

procedure TFrLandMark.actHelpSpecifyRelatedExecute(Sender: TObject);
begin

  if ShellExecute(0, 'Open', PChar(ExtractFilePath(Application.ExeName)+'\HELP\�������� ��������� ������ ������� ��������.docx'), nil, nil, SW_RESTORE) < 32 then
     raise Exception.Create('���� ������� �� ������!');;  
end;

procedure TFrLandMark.actNewPlan_FormParcelsExecute(Sender: TObject);
begin
 try
  self.jvlgfl.Add(DateTimeToStr(Now), '�������� �������� �� ����������� ��', '');

  TFrLoader.Inctance.RunAnimation := true;
  self.FBoundaryPlanXML.CreateNew(bpkRegistration, 0,tpNewParcel);
  self.CheckEnable;
  self.CheckForms;
  self.ShowPageDescription;
 finally
    TFrLoader.Inctance.RunAnimation := false;
 end;
end;

procedure TFrLandMark.actNewPlan_SpecifyParcelExecute(Sender: TObject);
begin
  try
    self.jvlgfl.Add(DateTimeToStr(Now), '�������� �������� �� ��������� ��', '');

    TFrLoader.Inctance.RunAnimation := true;
    self.FBoundaryPlanXML.CreateNew(bpkChanges, 0,tpExistParcel);
    self.CheckEnable;
    self.CheckForms;
    self.ShowPageDescription;
  finally
     TFrLoader.Inctance.RunAnimation := false;
  end;
end;

procedure TFrLandMark.actNewPlan_SpecifyParcel_EZModeExecute(Sender: TObject);
begin
 try
   self.jvlgfl.Add(DateTimeToStr(Now), '�������� �������� �� ��������� �� ','');

  TFrLoader.Inctance.RunAnimation := true;
  self.FBoundaryPlanXML.CreateNew(bpkChanges, 0,tpExistEZParcel);
  self.CheckEnable;
  self.CheckForms;
  self.ShowPageDescription;
 finally
   TFrLoader.Inctance.RunAnimation := false;
 end;
end;

{�������� �� ���������� ������}
procedure TFrLandMark.actNewSubParcelExecute(Sender: TObject);
begin
 try
     self.jvlgfl.Add(DateTimeToStr(Now), '�������� �������� �� �����������  �����  ��', '');

     TFrLoader.Inctance.RunAnimation := true;
     Self.FBoundaryPlanXML.CreateNew(bpNewSubParcels,0,tpNewSubParcel);
     self.CheckEnable;
     self.CheckForms;
 finally
      TFrLoader.Inctance.RunAnimation := false;
 end;
end;

procedure TFrLandMark.actOpenPlanExecute(Sender: TObject);
begin
 try
  self.jvlgfl.Add(DateTimeToStr(Now), '��������� ������. xml ��� ������', '');

  {�������� �������� Xml ������ �� � ������� LandMark}
  if dlgOpenProj.Execute then
  begin
     self.FBoundaryPlanXML.Load(dlgOpenProj.FileName);
     //������ � Reopen.ini
  end;

//  self.Ribbon1.ActivePage := self.rbgDiscription;
  self.dxrbn1.ActiveTab := self.dxrbntbdxrbn1TabInfo;
  self.CheckEnable;
  self.CheckForms;
  self.ShowPageDescription;
 except  on e : Exception do 
    raise Exception.Create('������ ��� ��������� ������|xml-�����!' + E.Message);
 end;
end;

procedure TFrLandMark.actSaveAsPlanExecute(Sender: TObject);
begin
  self.jvlgfl.Add(DateTimeToStr(Now), 'actSaveAsPlan', '');
  if jvbrwsfrfldrdlg.Execute then
    FBoundaryPlanXML.CopyPackage(TSettingPathFile.MakeTempPath + '\', jvbrwsfrfldrdlg.Directory,1);

  self.CheckEnable;
end;

procedure TFrLandMark.act_NewSubParcelExecute(Sender: TObject);
var  tagIndex : integer;
begin
 self.jvlgfl.Add(DateTimeToStr(Now), 'act_NewSubParcel', '');

 tagIndex := (Sender as TAction).Tag;
 self.FBoundaryPlanXML.SetActivPageParcels(TTypePackage(tagIndex));
end;

procedure TFrLandMark.act_NewSubParcelsExecute(Sender: TObject);
begin
try
  self.jvlgfl.Add(DateTimeToStr(Now), 'ct_NewSubParcels', '');

  TFrLoader.Inctance.RunAnimation := true;
  self.FBoundaryPlanXML.CreateNew(bpNewSubParcels, 0,tpNewSubParcel);
  self.CheckEnable;
  self.CheckForms;
  self.ShowPageDescription;
finally
  TFrLoader.Inctance.RunAnimation := False;
end;
end;

procedure TFrLandMark.act_SettingsExecute(Sender: TObject);
begin
  TFrInternet.insConnection.ShowModal;
end;

procedure TFrLandMark.act_SPESpecifyRelatedParcelExecute(Sender: TObject);
begin
end;

///
/// <param name="Sender">
///    Name action Manager ( �������������  ���������� ��������)
///    � Action - ������� Tag - ������������� �������  TTypePackage  
/// </param> 
///
procedure TFrLandMark.actSTD_MPExecute(Sender: TObject);
var  tagIndex : integer;
begin
 tagIndex := (Sender as TAction).Tag;
 self.jvlgfl.Add(DateTimeToStr(Now), 'act_NewSubParcels', 'tag = ' +IntToStr(Tag));
 self.FBoundaryPlanXML.SetActivPageParcels(TTypePackage(tagIndex));
end;

///
/// ��������� ��������� �� ������ �� BuckTracing
/// ���������������� ������ ��������� � ����� 
/// %user%\Application Data\LandMark\LANDMARK.INI 
///
procedure TFrLandMark.actSupportExecute(Sender: TObject);
var 
    proxyHost : string;
    proxyPort : integer;
    is_Proxy   : Integer;
begin
   self.jvlgfl.Add(DateTimeToStr(Now), 'actSupport', '');

  {���� ��� ���������� ���������}  {������ ����� ��������� ini  ����}
  with TConfig.Section<TProxyOptions> do 
  begin
     is_Proxy := isProxy;
     proxyHost := Proxy;
     proxyPort := Port;
     self.FYouTrack.Login := User;
     Self.FYouTrack.Password := Password;
  end;
  self.FYouTrack.Host := cnstYouTrackHost;
  self.FYouTrack.Port := cnstYouTrackPort;
  self.FYouTrack.ProjectName := cnstYouTrackProjName;
  if is_Proxy = 1 then {���������� ����� ������}
  begin
    self.FYouTrack.ProxyHost := proxyHost;
    self.FYouTrack.ProxyPort := proxyPort;
  end;

  try
    self.FYouTrack.Connect(Self.FYouTrack.Login,self.FYouTrack.Password);
  except
    self.FYouTrack.ExecuteDialog_Login(self.ParentWindow);
    if self.FYouTrack.IsConnected then
    begin
       with TConfig.Section<TProxyOptions> do
       begin
          User := self.FYouTrack.Login;
          Password := self.FYouTrack.Password;
       end;
    end
    else Exit;
  end;
  if not FYouTrack.IsConnected  then
  begin
     MessageDlg('�������� ��������� ���������� � ����������!',mtInformation,[mbOk],-1);
     Exit;
  end;     
  self.FYouTrack.ExecuteDialog_NewIssue(self.ParentWindow);

end;


procedure TFrLandMark.actValidateExecute(Sender: TObject);
var
  LogOut: TFormTextInf;
begin
  self.jvlgfl.Add(DateTimeToStr(Now), 'act_Validate', '');

  Application.CreateForm(TFormTextInf, LogOut);
  LogOut.Caption := '��������� �������� ������ ������� XML!';
  self.FBoundaryPlanXML.ValidData(LogOut.lstValidatorLog.Items);
  LogOut.ShowModal;
  self.CheckEnable;
end;


procedure TFrLandMark.aplctnvnts1Exception(Sender: TObject; E: Exception);
var 
 LogFile: textfile;
 ClassTree : string;
 stl : TStringList;
begin

   AssignFile(LogFile,ExtractFilePath(ParamStr(0))+cnstNameErrorLog);
   if FileExists(ExtractFilePath(ParamStr(0))+cnstNameErrorLog) then  Append(LogFile)
   else    Rewrite(LogFile);
   WriteLn(LogFile);
   WriteLn(LogFile,DateToStr(Date)+' '+TimeToStr(Time)+'    '+GetComputerNetName);
   WriteLn(LogFile,'-------------------------------------------------------------');

   ClassTree:=E.ClassName;
   ClassRef:=E.ClassType;
   while ClassRef.ClassParent<>nil do
   begin
      ClassRef:=ClassRef.ClassParent;
      ClassTree:=ClassRef.ClassName+' => '+ClassTree+':' +  ClassRef.UnitScope+ ':'+ Sender.ClassName+
      '  '+Sender.UnitScope+ ' ' + Sender.MethodName(E.ClassInfo)+ ' '+ E.StackTrace;
   end;

   WriteLn(LogFile,ClassTree+':');
   WriteLn(LogFile,E.Message+  ' $' + IntToHex(Integer(ExceptAddr), 8));
   ShowMessage(E.Message+ ' $' + IntToHex(Integer(ExceptAddr), 8));
   {����� ��������� �������� ������}
   stl := TStringList.Create;
   try
     JclLastExceptStackListToStrings(stl,True,True,True,true);
     Stl.Insert(0, E.Message);
     stl.Insert(1,'');
     WriteLn(LogFile,PChar(Stl.Text));
     self.jvlgfl.Add(DateTimeToStr(Now), 'Error #10#13',E.Message);
   finally
     FreeAndNil(stl);
   end;

   WriteLn(LogFile,'-------------------------------------------------------------');
   Flush(LogFile);
   CloseFile(LogFile);
end;


procedure TFrLandMark.btn2Click(Sender: TObject);
begin
  TSettingPathFile.MakeTempPath;
end;

procedure TFrLandMark.btn3Click(Sender: TObject);
begin

 TSettingPathFile.DeleteTempPath;
end;

//--Zip
procedure TFrLandMark.Button1Click(Sender: TObject);
var
 _mc: TSpMContour;
 _st: string;
begin
 Clipboard.Open;
 _st:= Clipboard.AsText;
 Clipboard.Close;

 _mc:= DataFormat.MContour.Text.Parse(_st);
 _st:= DataFormat.MContour.Text.Build(_mc);
 Clipboard.Open;
 Clipboard.AsText:= _st;
 Clipboard.Close;
end;

procedure TFrLandMark.CheckForms;
begin
 self.FBoundaryPlanXML.CloseDialog_GenInf;
 self.FBoundaryPlanXML.CloseDialog_Package;
 if self.FBoundaryPlanXML.IsAssigned then
 begin
   self.FBoundaryPlanXML.ExecuteDialog_GenInf(tsDescription);
   self.FBoundaryPlanXML.ExecuteDialog_Package(tsContent);
   self.Repaint;
 end;
end;

procedure TFrLandMark.dxrbn1TabChanging(Sender: TdxCustomRibbon;
  ANewTab: TdxRibbonTab; var Allow: Boolean);
begin
  if ANewTab.Tag >0 then
   self.pgcMain.ActivePageIndex:= ANewTab.Tag-1;
  //if dxrbn1.ActiveTab.Tag >0  then
//     self.pgcMain.ActivePageIndex := 0;
  if dxrbn1.CanFocus then dxrbn1.SetFocus;

end;

procedure TFrLandMark.CheckEnable;
var
 _bool: boolean;
begin
 _bool:=  self.FBoundaryPlanXML.IsAssigned;
 {SpecifyParcel and  FormParcel and NewSubParcel}
 self.actNewPlan_SpecifyParcel.Enabled:= not _bool;
 self.actNewPlan_SpecifyParcel_EZMode.Enabled:= not _bool;
 self.actNewPlan_FormParcels.Enabled:= not _bool;
 self.act_NewSubParcels.Enabled := not _bool;
 
 self.actOpenPlan.Enabled:= not _bool;
 self.actClosePlan.Enabled:= _bool;
 self.actSaveAsPlan.Enabled:= _bool;
 self.actValidate.Enabled:= _bool;
 self.actBuildPackage.Enabled:= _bool;
 self.actCheckAppliedFiles.Enabled := _bool;
 self.actExporttoWord.Enabled := _bool;

 { �������� � ������� ����� }
 self.act_Client.Enabled := _bool;
 Self.act_Contractor.Enabled := _bool;
 self.act_Documentation.Enabled := _bool;
 self.act_InputDate.Enabled := _bool;
 self.act_Conclusion.Enabled := _bool;

end;

procedure TFrLandMark.FormClose(Sender: TObject; var Action: TCloseAction);
begin

  if MessageDlg(rs�hangesWillbeLost, mtConfirmation, mbYesNo, -1) = mrNo  then
  begin
    Action := caNone;
    Exit;
  end
  else
  begin
    self.FBoundaryPlanXML.DoClose;
    if  Assigned(fmSelectionList.frmSelectionList) then  fmSelectionList.frmSelectionList.Close;
    self.FBoundaryPlanXML.Free;
    Action := caFree;
    self.jvlgfl.Add(DateTimeToStr(Now), '���������� ������!', '');
  end;

end;

procedure TFrLandMark.FormCreate(Sender: TObject);
var dest : TCodeSiteDestination;
    DLL_YouTrack_Factory: function: IBagTracker; stdcall;
    LibHandle:THandle;
begin
   {������������ CodeSite}
   dest := TCodeSiteDestination.Create(self);
   dest.LogFile.FilePath := ExtractFilePath(Application.ExeName);
   dest.LogFile.FileName := 'Log.csl';
   dest.LogFile.Active := True;
   CodeSite.Destination := dest;

   jvlgfl.FileName := TPath.GetDirectoryName(Application.ExeName)+'\Data\logFileLandMark'+GetComputerNetName+'.log';
   if FileExists('Data\logFileLandMark'+GetComputerNetName) then
        jvlgfl.LoadFromFile('Data\logFileLandMark'+GetComputerNetName);

   self.jvlgfl.Add(DateTimeToStr(Now), ' ','');
   self.jvlgfl.Add(DateTimeToStr(Now), ' ',Application.ExeName);
   self.jvlgfl.Add(DateTimeToStr(Now), ' ','');

   {����� ����������}
   Fsplash:= TSplash<TFrLOGO>.Create(ExtractFilePath(Application.ExeName)+'Data\Icon\LOGOLandMArk.png');
   Fsplash.Show(true);
   Sleep(1500);

   self.jvlgfl.Add(DateTimeToStr(Now), 'Create  TMSBoundaryPlanXML','+ OnChangeActivPage#10#13');
   {�� � ..}
   self.FBoundaryPlanXML := TMSBoundaryPlanXML.Create;
   self.FBoundaryPlanXML.OnChangeActivPage :=  OnChangeActivPageData;
   self.CheckEnable;

   {����������� �����}
   OnChangeActivPageData(Sender);


   self.jvlgfl.Add(DateTimeToStr(Now), 'Create  FYouTrack','+ YOuTracke#10#13');

   {YUOTRACKNG}
   if self.FYouTrack=nil then
   begin
      LibHandle := LoadLibrary('Data\Package\'+cnstDLLYOUTrack);
      if LibHandle >= 32 then
      begin
        DLL_YouTrack_Factory := GetProcAddress(LibHandle, 'MsYouTrack_Factory');
        if Assigned(DLL_YouTrack_Factory) then FYouTrack:=DLL_YouTrack_Factory
        else    raise Exception.Create('�� ������� ��������� ������� �� ����������.');
      end;
   end;
   Application.OnException := self.aplctnvnts1.OnException;
end;

procedure TFrLandMark.FormShow(Sender: TObject);
begin
  Fsplash.Close;
  self.wb1.Navigate(ExtractFilePath(Application.ExeName)+'Data\WelcomPage\index.html');
  self.wb2.Navigate(ExtractFilePath(Application.ExeName)+'Data\WelcomPage\index.html');
  self.dxrbn1.ActiveTab := self.dxrbntbRibbon1Tab;
end;

procedure TFrLandMark.img2OptionsChanged(Sender: TObject);
begin
end;

{ close doc }
procedure TFrLandMark.ImgCloseClick(Sender: TObject);
begin
  if MessageDlg(rs�hangesWillbeLost, mtConfirmation, mbYesNo, -1) = mrYes then
    self.FBoundaryPlanXML.DoClose;                                                          
end;

{������ ����������������}
procedure TFrLandMark.imgExistEZParcelsClick(Sender: TObject);
begin
  self.FBoundaryPlanXML.CreateNew(bpkChanges, 0,tpExistEZParcel);
end;

procedure TFrLandMark.imgFormParclsClick(Sender: TObject);
begin
  self.FBoundaryPlanXML.CreateNew(bpkRegistration, 0,tpNewParcel);
end;

{ ����������  ������ ��������  ��� ������ � xml �� ��������� }
procedure TFrLandMark.imgSpecifyrelatedClick(Sender: TObject);
begin
  self.FBoundaryPlanXML.CreateNew(bpkChanges, 0,tpExistParcel);
end;


procedure TFrLandMark.jvcptnbtn1Click(Sender: TObject);
begin
end;

{ desc }
procedure TFrLandMark.LbCreateClick(Sender: TObject);
begin
  self.FBoundaryPlanXML.ExecuteDialog_GenInf;
end;

procedure TFrLandMark.LbCreateMouseLeave(Sender: TObject);
begin
  if TLabel(Sender).Tag = 0 then
    TLabel(Sender).Font.Color := clBlack
  else
    TLabel(Sender).Font.Color := clPurple;
end;

procedure TFrLandMark.LbCreateMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
  TLabel(Sender).Font.Color := clBlue;
end;

procedure TFrLandMark.lblCopyPackageClick(Sender: TObject);
var
  savePath: string;
begin
  if SelectDirectory('�����  ����������', '���� ��������?', savePath,
    [sdNewFolder, sdNewUI, sdValidateDir, sdShowEdit, sdShowShares], self) then
    FBoundaryPlanXML.CopyPackage(TSettingPathFile.MakeTempPath + '\',savePath);
end;

{ xml }
procedure TFrLandMark.lblXMLClick(Sender: TObject);
begin
  self.FBoundaryPlanXML.ExecuteDialog_Package;
end;

{ zip }
procedure TFrLandMark.lblZIpClick(Sender: TObject);
var
  SaveDlg: TSaveDialog;
  LogOut: TFormTextInf;
begin
  SaveDlg := TSaveDialog.Create(nil);
  SaveDlg.Filter := '*.zip|*.ZIP';
  if SaveDlg.Execute then
  begin
    { ��������� xml }
    Application.CreateForm(TFormTextInf, LogOut);
    LogOut.Caption := '��������� �������� ������ ������� XML!';
    if not self.FBoundaryPlanXML.ValidData(LogOut.lstValidatorLog.Items) then
      LogOut.ShowModal
  end;
  SaveDlg.Free;
end;

{ open docS }
procedure TFrLandMark.LbOpenClick(Sender: TObject);
var
  _OpenDLG: TOpenDialog;
begin
  _OpenDLG := TOpenDialog.Create(nil);
  _OpenDLG.Filter := '*.XML|*.xml';
  if _OpenDLG.Execute then
  begin
    self.FBoundaryPlanXML.Load(_OpenDLG.FileName);
    self.FBoundaryPlanXML.ExecuteDialog_Package;
  end;
  _OpenDLG.Free;
end;

{ check }
procedure TFrLandMark.lbValidateClick(Sender: TObject);
var
  LogOut: TFormTextInf;
begin
  Application.CreateForm(TFormTextInf, LogOut);
  LogOut.Caption := '��������� �������� ������ ������� XML!';
  self.FBoundaryPlanXML.ValidData(LogOut.lstValidatorLog.Items);
  LogOut.ShowModal;
end;


procedure TFrLandMark.OnChangeActivPageData(Sender: TObject);
var strCaption : string;
begin
  if Sender is TLabel then strCaption := (Sender as TLabel).Caption {))))}
  else strCaption := '';

  self.jvlgfl.Add(DateTimeToStr(Now), 'OnChangeActivPageData', strCaption);

  { ����������� }
  act_NewParcel.Enabled := (strCaption = rsFormParcels);
  act_NewParcel.Checked := (strCaption = rsFormParcels);
  act_ChangeParcel.Enabled := (strCaption = rsFormParcels);
  act_ChangeParcel.Checked := false;
  act_FPSpecifyRelatedParcel.Enabled := (strCaption = rsFormParcels);
  act_FPSpecifyRelatedParcel.Checked := False;
  { ��������� }
  act_ExistParcel.Enabled := (strCaption = rsSpecifyParcels);
  act_ExistParcel.Checked := (strCaption = rsSpecifyParcels);
  act_SPSpecifyRelatedParcel.Enabled := (strCaption = rsSpecifyParcels);
  act_SPSpecifyRelatedParcel.Checked := false;
  { EE }
  act_ExistEZParcels.Enabled := (strCaption = rsSpecifyParcelsEZ);
  act_ExistEZParcels.Checked := (strCaption = rsSpecifyParcelsEZ);
  act_SPESpecifyRelatedParcel.Enabled := (strCaption = rsSpecifyParcelsEZ);
  act_SPESpecifyRelatedParcel.Checked := False;
  act_ExistEZEntryParcels.Enabled := (strCaption = rsSpecifyParcelsEZ);
  act_ExistEZEntryParcels.Checked := False;
  {����������� ������}
  act_NewSubParcel.Enabled := (strCaption = rsNewSubParcel);
  act_NewSubParcel.Checked := (strCaption = rsNewSubParcel);
  {����� ��������}
  act_Contractor.Enabled := true;
  act_Contractor.Checked := true;

end;

procedure TFrLandMark.ShowPageDescription;
var ok : Boolean;
begin
 // ok := Visible;
 // self.Ribbon1TabChange(self,1,0,ok);

  dxrbn1TabChanging(dxrbn1,dxrbntbdxrbn1TabInfo,ok);
  self.dxrbn1.ActiveTab := self.dxrbntbdxrbn1TabInfo;
//  self.Ribbon1.ActivePage := self.rbgDiscription;
end;

{ save as }
procedure TFrLandMark.lblSaveAsClick(Sender: TObject);
var savePath : string;
begin
  if SelectDirectory('�����  ����������', '���� ��������?', savePath,
   [sdNewFolder, sdNewUI, sdValidateDir, sdShowEdit, sdShowShares], self) then
    FBoundaryPlanXML.CopyPackage(TSettingPathFile.MakeTempPath + '\', savePath,1);
end;

//initialization
//  // ������������� ��������� ��������� ����������
// // Include(JclStackTrackingOptions, stRawMode);
//  //JclAddExceptNotifier(AnyExceptionNotify);
//  JclStackTrackingOptions :=  JclStackTrackingOptions + [stRawMode];
//  JclStartExceptionTracking;
//finalization
//  JclStopExceptionTracking;

initialization
  // Enable raw mode (default mode uses stack frames which aren't always generated by the compiler)
 // Include(JclStackTrackingOptions, stRawMode);

  JCLdebug.JclStackTrackingOptions:=[stStack, stRawMode];
  // Disable stack tracking in dynamically loaded modules (it makes stack tracking code a bit faster)
  Include(JclStackTrackingOptions, stStaticModuleList);
  // Initialize Exception tracking
  JclStartExceptionTracking;
finalization
  JclStopExceptionTracking;

end.
