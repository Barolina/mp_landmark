{*******************************************************}
{                                                       }
{            Delphi Visual Component Library            }
{                                                       }
{ Copyright(c) 1995-2012 Embarcadero Technologies, Inc. }
{                                                       }
{*******************************************************}

unit Vcl.RibbonConsts;

interface

resourcestring
  // Ribbon constants
  SPageCaptionExists = '������ � ������ %s ��� ����������';
  SInvalidRibbonPageParent = 'A Ribbon Pages parent must be TCustomRibbon';
  SRecentDocuments = '����� �������� �����';
  SDuplicateRibbonComponentsNotAllowed = 'Only one Ribbon control on a Form is allowed';
  SRibbonTabIndexInvalid = 'Specified TabIndex is invalid';
  SRibbonControlInvalidParent = 'A %s can only be placed on a Ribbon Group with an ActionManager assigned';
  SInvalidRibbonGroupParent = 'Ribbon Group must have a Ribbon Page as its parent';
  SInvalidRibbonPageChild = 'Only Ribbon Groups can be inserted into a Ribbon Page';

  // Quick Access Toolbar
  SAddToQuickAccessToolbar = '�������� �� ������ �������� �������';
  SRemoveFromQuickAccessToolbar = '��������� ������� �� ������ �������� �������';
  SCustomizeQuickAccessToolbar = '��������� �����...';
  SMoreControls = '������ ��������';
  SMoreCommands = '������ �������...';
  SMinimizeTheRibbon = '�������� �����';
  SQuickAccessToolbarGlyphRequired = '�� �������� ���������� ������� �� ������ �������� �������';
  SQATShowAboveTheRibbon = '���������� ��� ������';
  SQATShowBelowTheRibbon = '���������� ��� ������';
  SShowQuickAccessToolbarAbove = '���������� ������ �������� ������� ��� ������';
  SShowQuickAccessToolbarBelow = '���������� ������ �������� ������� ��� ������';

  // ScreenTips constants
  SCommandIsDisabled = '��� ������� ���������.';
  SUseTheGlyphProperty = 'Use the glyph property to set an image';
  SInvalidScreenTipItemAssign = 'Cannot assign a non-ScreenTipItem to a ScreenTipItem';
  SDefaultFooterText = '������� F1 ��� ��������� �������������� ����������.';
  SGenerateScreenTipsError = 'Unable to generate Screen Tips because there are no linked Action Lists.';

implementation

end.

