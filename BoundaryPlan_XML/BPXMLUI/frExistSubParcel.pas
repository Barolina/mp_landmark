unit frExistSubParcel;

{
   SpecifyRelatedParcel_ExistSubParcel
}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ComCtrls,

  STD_MP, frEntitySpatial,XMLIntf, ActnList,
  System.Actions, frSubParContours;
type
  TFrameExistSubParcel = class(TFrame)
    ActionList: TActionList;
    ActionChange_DefOrNumPP: TAction;
    ActionChange_SubParcelRealty: TAction;
    ActionChange_ExistingOrNewSubParcel: TAction;
    procedure Attribute_DefOrNumPP_EditKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);

  private
   { Private declarations }
    FXMLSpecifyRelatedParcel_ExistSubParcel: IXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel;

    FContours          : TFrameSubParContours;
    FEntitySpatial     : TFrameEntity_Spatial;
    FOnChangeDefOrNumPP: TNotifyEvent;

    procedure CreatteFrameEntityOrContours;
  protected
   procedure InitializationUI; //���������������� - ����������� �������� ��������� �����������
   procedure GetEntityOrContours;
   procedure InitEntityOrContours;

   procedure SetSubParcel( const aSubParcel: IXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel); //��������� ������� IXMLTSubParcel
  public
    { Public declarations }
    constructor Create(Owner: TComponent); override;
    function IsAssignedSubParcel: Boolean;

    property XmlSpecifyRelatedParcel_ExistSubParcel: IXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel
             read FXMLSpecifyRelatedParcel_ExistSubParcel
             write SetSubParcel;
    property OnChangeDefOrNumPP: TNotifyEvent read FOnChangeDefOrNumPP write FOnChangeDefOrNumPP;

  end;

implementation



{$R *.dfm}

procedure _HiddenChangeCheckBoxSate(ChBox: TCheckBox; NewSate: TCheckBoxState);
var
 _OnClick: TNotifyEvent;
begin
 if ChBox.State=NewSate then EXIT;
 _OnClick:= ChBox.OnClick;
 ChBox.OnClick:= nil;
 ChBox.State:= NewSate;
 ChBox.OnClick:= _OnClick;
end;

procedure TFrameExistSubParcel.Attribute_DefOrNumPP_EditKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
 if key = VK_RETURN then
  if (Sender is TCustomEdit) then
   TCustomEdit(Sender).Parent.SetFocus;
end;

constructor TFrameExistSubParcel.Create(Owner: TComponent);
begin
 inherited;
 self.FXMLSpecifyRelatedParcel_ExistSubParcel:= nil;
 CreatteFrameEntityOrContours;
end;

procedure TFrameExistSubParcel.CreatteFrameEntityOrContours;
begin
  if not Assigned(FEntitySpatial) then
  begin
          self.FEntitySpatial := TFrameEntity_Spatial.Create(self);
          self.FEntitySpatial.Parent :=  self;
          self.FEntitySpatial.Align := alClient;
          self.FEntitySpatial.Visible := false;
  end;
  if not Assigned(FContours) then
  begin
          self.FContours:= TFrameSubParContours.Create(self);
          self.FContours.ActivContourKind := TFrameSubParContours.TContourKind.cNewContour;
          self.FContours.cbbContoursType.Visible := False;
          self.FContours.Parent:= self;          
          self.FContours.Align:= alClient;
          self.FContours.Visible := false;
  end;
end;

procedure TFrameExistSubParcel.InitEntityOrContours;
begin
    FContours.XmlContours := nil;
    FEntitySpatial.EntitySpatial := nil
end;

procedure TFrameExistSubParcel.InitializationUI;
begin
 if self.IsAssignedSubParcel then
    GetEntityOrContours
 else  InitEntityOrContours;
end;

function TFrameExistSubParcel.IsAssignedSubParcel: Boolean;
begin
  Result:= self.FXMLSpecifyRelatedParcel_ExistSubParcel<>nil;
end;

procedure TFrameExistSubParcel.SetSubParcel;
begin
  if not Assigned(aSubParcel) then Exit;
  self.FXMLSpecifyRelatedParcel_ExistSubParcel := aSubParcel;
  InitializationUI
end;

procedure TFrameExistSubParcel.GetEntityOrContours;
var
     _node  : IXMLNode;
begin

  self.FEntitySpatial.EntitySpatial := nil;
  self.FContours.XmlContours := nil;
  self.FEntitySpatial.Visible := false;
  self.FContours.Visible := false;

  _node := self.FXMLSpecifyRelatedParcel_ExistSubParcel.ChildNodes.FindNode('Entity_Spatial');
  if _node <> nil then
  begin
    self.FEntitySpatial.EntitySpatial := self.FXMLSpecifyRelatedParcel_ExistSubParcel.Entity_Spatial;
    self.FEntitySpatial.Visible := true;
  end
  else
  begin
     _node := self.FXMLSpecifyRelatedParcel_ExistSubParcel.ChildNodes.FindNode('Contours');
     if _node  <> nil then
     begin
        self.FContours.XmlContours := self.FXMLSpecifyRelatedParcel_ExistSubParcel.Contours;
        self.FContours.Visible := true;
     end;
  end;
end;

end.
