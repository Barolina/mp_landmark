unit frPerson;
{

  ���������� ����
  IXMLSTD_MP_Title_Client_Person
}
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls,

  STD_MP, frFIO, XMLIntf;

type
  TFramePerson = class(TFrame)

   private
    { Private declarations }
    FXmlPerson : IXMLSTD_MP_Title_Client_Person;
    FFIO       : TFrameFIO;
    function IsAssignedPerson: boolean;

 protected
   procedure InitializationUI;
   procedure SetPerson(const aPerson: IXMLSTD_MP_Title_Client_Person);
  public
   { Public declarations }
   constructor Create(AOwner: TComponent); override;
   property XMLPerson:IXMLSTD_MP_Title_Client_Person read FXmlPerson write SetPerson;
  end;

implementation


{$R *.dfm}

{ TFramePerson}

constructor TFramePerson.Create(AOwner: TComponent);
begin
  inherited;
  self.FFIO:= TFrameFIO.Create(Self);
  self.FFIO.Parent:= self;
  self.FFIO.Align:= alClient;
end;


procedure TFramePerson.InitializationUI;
begin
 if self.IsAssignedPerson then
 begin
  self.FFIO.XMLFIO := FXmlPerson.FIO;
 end
 else
  FFIO.XMLFIO := nil;
end;

function TFramePerson.IsAssignedPerson: boolean;
begin
 Result:= self.FXmlPerson<>nil;
end;

procedure TFramePerson.SetPerson(const aPerson: IXMLSTD_MP_Title_Client_Person);
begin
 if self.FXmlPerson=aPerson then EXIT;
 self.FXmlPerson:= aPerson;
 self.InitializationUI;
end;
end.
