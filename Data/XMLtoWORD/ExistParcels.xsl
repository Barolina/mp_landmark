<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:o="urn:schemas-microsoft-com:office:office"
    xmlns:w="http://schemas.microsoft.com/office/word/2003/wordml" 
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" 
    xmlns:ext="my extension" 
    xmlns:v="urn:schemas-microsoft-com:vml" 
    xmlns:fox="http://xmlgraphics.apache.org/fop/extensions"
    xmlns:fo="http://www.w3.org/1999/XSL/Format"
    exclude-result-prefixes="msxsl ext">
    
    <xsl:output method="xml" indent="yes" />
    
    
    <!--****************************************Elements****************************************-->
   
   
    
    <xsl:template match="STD_MP">
        <xsl:processing-instruction name="mso-application">
          <xsl:text>progid="Word.Document"</xsl:text>
        </xsl:processing-instruction>
        
        <w:document>
            <w:body>                
                <xsl:apply-templates select="Title"/>
             
                <xsl:call-template name="Racheti"/>
                <!-- разрыв страницы -->
                <w:p w:rsidR="00FC287A" w:rsidRDefault="00FC287A"/>
                <w:p w:rsidR="00FC287A" w:rsidRDefault="00AA4A27">
                    <w:r>
                        <w:br w:type="page"/>
                    </w:r>
                </w:p>
                
                <xsl:apply-templates select="Package"/>
                <!-- разрыв страницы -->
                <w:p w:rsidR="00D138AC" w:rsidRDefault="00D138AC" w:rsidP="00D138AC">
                    <w:pPr>
                        <w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
                        <w:rPr>
                            <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                        </w:rPr>
                        <w:sectPr w:rsidR="00D138AC" w:rsidSect="00AA64DC">
                            <w:pgSz w:w="11906" w:h="16838"/>
                            <w:pgMar w:top="1134" w:right="850" w:bottom="1134" w:left="1701" w:header="708" w:footer="708" w:gutter="0"/>
                            <w:cols w:space="708"/>
                            <w:docGrid w:linePitch="360"/>
                        </w:sectPr>
                    </w:pPr>
                </w:p>
                
                <!--заключение-->
                <xsl:if test="Conclusion">
                    <xsl:apply-templates select="Conclusion"/>
                </xsl:if>
            	
            	<xsl:call-template name="End"/>
            </w:body>
        </w:document>
     
    </xsl:template> 
    
    
     
    <xsl:template match="Client" mode="Title">
            <xsl:apply-templates select="Person" mode="Title"/>
            <xsl:apply-templates select="Organization" mode="Title"/>
            <xsl:apply-templates select="Foreign_Organization" mode="Title"/>
            <xsl:apply-templates select="Governance" mode="Title"/>
    </xsl:template>
    
    <xsl:template match="Contractor" mode="Title">
    	<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
    		<w:tblPrEx>
    			<w:tblCellMar>
    				<w:top w:w="0" w:type="dxa"/>
    				<w:bottom w:w="0" w:type="dxa"/>
    			</w:tblCellMar>
    		</w:tblPrEx>
    		<w:trPr>
    			<w:cantSplit/>
    			<w:trHeight w:val="360"/>
    		</w:trPr>
    		<w:tc>
    			<w:tcPr>
    				<w:tcW w:w="9670" w:type="dxa"/>
    				<w:gridSpan w:val="22"/>
    				<w:tcBorders>
    					<w:top w:val="double" w:sz="6" w:space="0" w:color="auto"/>
    					<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
    					<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
    					<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
    				</w:tcBorders>
    				<w:vAlign w:val="center"/>
    			</w:tcPr>
    			<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
    				<w:pPr>
    					<w:jc w:val="center"/>
    					<w:rPr>
    						<w:b/>
    						<w:bCs/>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    				</w:pPr>
    				<w:r>
    					<w:rPr>
    						<w:b/>
    						<w:bCs/>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    					<w:t>4. Сведения о кадастровом инженере:</w:t>
    				</w:r>
    			</w:p>
    		</w:tc>
    	</w:tr>
    	<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
    		<w:tblPrEx>
    			<w:tblCellMar>
    				<w:top w:w="0" w:type="dxa"/>
    				<w:bottom w:w="0" w:type="dxa"/>
    			</w:tblCellMar>
    		</w:tblPrEx>
    		<w:trPr>
    			<w:cantSplit/>
    			<w:trHeight w:val="275"/>
    		</w:trPr>
    		<w:tc>
    			<w:tcPr>
    				<w:tcW w:w="4403" w:type="dxa"/>
    				<w:gridSpan w:val="11"/>
    				<w:tcBorders>
    					<w:top w:val="nil"/>
    					<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
    					<w:bottom w:val="nil"/>
    					<w:right w:val="nil"/>
    				</w:tcBorders>
    				<w:vAlign w:val="bottom"/>
    			</w:tcPr>
    			<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
    				<w:pPr>
    					<w:ind w:left="85"/>
    					<w:rPr>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    				</w:pPr>
    				<w:r>
    					<w:rPr>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    					<w:t xml:space="preserve">Фамилия, имя, отчество </w:t>
    				</w:r>
    				<w:r>
    					<w:rPr>
    						<w:sz w:val="19"/>
    						<w:szCs w:val="19"/>
    					</w:rPr>
    					<w:t>(при наличии отчества)</w:t>
    				</w:r>
    			</w:p>
    		</w:tc>
    		<w:tc>
    			<w:tcPr>
    				<w:tcW w:w="5126" w:type="dxa"/>
    				<w:gridSpan w:val="10"/>
    				<w:tcBorders>
    					<w:top w:val="nil"/>
    					<w:left w:val="nil"/>
    					<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
    					<w:right w:val="nil"/>
    				</w:tcBorders>
    				<w:vAlign w:val="bottom"/>
    			</w:tcPr>
    			<w:p w:rsidR="007E23C6" w:rsidRDefault="00D629DE">
    				<w:pPr>
    					<w:rPr>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    				</w:pPr>
    				<w:r>
    					<w:rPr>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    					<w:t xml:space="preserve"><xsl:apply-templates select="FIO" mode="tFIO"/> </w:t>
    				</w:r>
    			</w:p>
    		</w:tc>
    		<w:tc>
    			<w:tcPr>
    				<w:tcW w:w="141" w:type="dxa"/>
    				<w:tcBorders>
    					<w:top w:val="nil"/>
    					<w:left w:val="nil"/>
    					<w:bottom w:val="nil"/>
    					<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
    				</w:tcBorders>
    				<w:vAlign w:val="bottom"/>
    			</w:tcPr>
    			<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
    				<w:pPr>
    					<w:rPr>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    				</w:pPr>
    			</w:p>
    		</w:tc>
    	</w:tr>
    	
    	<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
    		<w:tblPrEx>
    			<w:tblCellMar>
    				<w:top w:w="0" w:type="dxa"/>
    				<w:bottom w:w="0" w:type="dxa"/>
    			</w:tblCellMar>
    		</w:tblPrEx>
    		<w:trPr>
    			<w:cantSplit/>
    		</w:trPr>
    		<w:tc>
    			<w:tcPr>
    				<w:tcW w:w="9670" w:type="dxa"/>
    				<w:gridSpan w:val="22"/>
    				<w:tcBorders>
    					<w:top w:val="nil"/>
    					<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
    					<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
    					<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
    				</w:tcBorders>
    			</w:tcPr>
    			<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
    				<w:pPr>
    					<w:rPr>
    						<w:sz w:val="6"/>
    						<w:szCs w:val="6"/>
    					</w:rPr>
    				</w:pPr>
    			</w:p>
    		</w:tc>
    	</w:tr>
    	<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
    		<w:tblPrEx>
    			<w:tblCellMar>
    				<w:top w:w="0" w:type="dxa"/>
    				<w:bottom w:w="0" w:type="dxa"/>
    			</w:tblCellMar>
    		</w:tblPrEx>
    		<w:trPr>
    			<w:cantSplit/>
    			<w:trHeight w:val="275"/>
    		</w:trPr>
    		<w:tc>
    			<w:tcPr>
    				<w:tcW w:w="5301" w:type="dxa"/>
    				<w:gridSpan w:val="14"/>
    				<w:tcBorders>
    					<w:top w:val="nil"/>
    					<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
    					<w:bottom w:val="nil"/>
    					<w:right w:val="nil"/>
    				</w:tcBorders>
    				<w:vAlign w:val="bottom"/>
    			</w:tcPr>
    			<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
    				<w:pPr>
    					<w:ind w:left="85"/>
    					<w:rPr>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    				</w:pPr>
    				<w:r>
    					<w:rPr>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    					<w:t>№ квалификационного аттестата кадастрового инженера</w:t>
    				</w:r>
    			</w:p>
    		</w:tc>
    		<w:tc>
    			<w:tcPr>
    				<w:tcW w:w="4228" w:type="dxa"/>
    				<w:gridSpan w:val="7"/>
    				<w:tcBorders>
    					<w:top w:val="nil"/>
    					<w:left w:val="nil"/>
    					<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
    					<w:right w:val="nil"/>
    				</w:tcBorders>
    				<w:vAlign w:val="bottom"/>
    			</w:tcPr>
    			<w:p w:rsidR="007E23C6" w:rsidRDefault="00D629DE">
    				<w:pPr>
    					<w:rPr>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    				</w:pPr>
    				<w:r>
    					<w:rPr>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    					<w:t><xsl:value-of select="normalize-space(N_Certificate)"/></w:t>
    				</w:r>
    			</w:p>
    		</w:tc>
    		<w:tc>
    			<w:tcPr>
    				<w:tcW w:w="141" w:type="dxa"/>
    				<w:tcBorders>
    					<w:top w:val="nil"/>
    					<w:left w:val="nil"/>
    					<w:bottom w:val="nil"/>
    					<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
    				</w:tcBorders>
    				<w:vAlign w:val="bottom"/>
    			</w:tcPr>
    			<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
    				<w:pPr>
    					<w:rPr>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    				</w:pPr>
    			</w:p>
    		</w:tc>
    	</w:tr>
    	<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
    		<w:tblPrEx>
    			<w:tblCellMar>
    				<w:top w:w="0" w:type="dxa"/>
    				<w:bottom w:w="0" w:type="dxa"/>
    			</w:tblCellMar>
    		</w:tblPrEx>
    		<w:trPr>
    			<w:cantSplit/>
    		</w:trPr>
    		<w:tc>
    			<w:tcPr>
    				<w:tcW w:w="9670" w:type="dxa"/>
    				<w:gridSpan w:val="22"/>
    				<w:tcBorders>
    					<w:top w:val="nil"/>
    					<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
    					<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
    					<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
    				</w:tcBorders>
    			</w:tcPr>
    			<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
    				<w:pPr>
    					<w:rPr>
    						<w:sz w:val="6"/>
    						<w:szCs w:val="6"/>
    					</w:rPr>
    				</w:pPr>
    			</w:p>
    		</w:tc>
    	</w:tr>
    	<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
    		<w:tblPrEx>
    			<w:tblCellMar>
    				<w:top w:w="0" w:type="dxa"/>
    				<w:bottom w:w="0" w:type="dxa"/>
    			</w:tblCellMar>
    		</w:tblPrEx>
    		<w:trPr>
    			<w:cantSplit/>
    			<w:trHeight w:val="275"/>
    		</w:trPr>
    		<w:tc>
    			<w:tcPr>
    				<w:tcW w:w="2097" w:type="dxa"/>
    				<w:gridSpan w:val="6"/>
    				<w:tcBorders>
    					<w:top w:val="nil"/>
    					<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
    					<w:bottom w:val="nil"/>
    					<w:right w:val="nil"/>
    				</w:tcBorders>
    				<w:vAlign w:val="bottom"/>
    			</w:tcPr>
    			<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
    				<w:pPr>
    					<w:ind w:left="85"/>
    					<w:rPr>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    				</w:pPr>
    				<w:r>
    					<w:rPr>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    					<w:t>Контактный телефон</w:t>
    				</w:r>
    			</w:p>
    		</w:tc>
    		<w:tc>
    			<w:tcPr>
    				<w:tcW w:w="7432" w:type="dxa"/>
    				<w:gridSpan w:val="15"/>
    				<w:tcBorders>
    					<w:top w:val="nil"/>
    					<w:left w:val="nil"/>
    					<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
    					<w:right w:val="nil"/>
    				</w:tcBorders>
    				<w:vAlign w:val="bottom"/>
    			</w:tcPr>
    			<w:p w:rsidR="007E23C6" w:rsidRDefault="00D629DE">
    				<w:pPr>
    					<w:rPr>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    				</w:pPr>
    				<w:r>
    					<w:rPr>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    					<w:t><xsl:value-of select="normalize-space(Telephone)"/></w:t>
    				</w:r>
    			</w:p>
    		</w:tc>
    		<w:tc>
    			<w:tcPr>
    				<w:tcW w:w="141" w:type="dxa"/>
    				<w:tcBorders>
    					<w:top w:val="nil"/>
    					<w:left w:val="nil"/>
    					<w:bottom w:val="nil"/>
    					<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
    				</w:tcBorders>
    				<w:vAlign w:val="bottom"/>
    			</w:tcPr>
    			<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
    				<w:pPr>
    					<w:rPr>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    				</w:pPr>
    			</w:p>
    		</w:tc>
    	</w:tr>
    	
    	<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
    		<w:tblPrEx>
    			<w:tblCellMar>
    				<w:top w:w="0" w:type="dxa"/>
    				<w:bottom w:w="0" w:type="dxa"/>
    			</w:tblCellMar>
    		</w:tblPrEx>
    		<w:trPr>
    			<w:cantSplit/>
    		</w:trPr>
    		<w:tc>
    			<w:tcPr>
    				<w:tcW w:w="9670" w:type="dxa"/>
    				<w:gridSpan w:val="22"/>
    				<w:tcBorders>
    					<w:top w:val="nil"/>
    					<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
    					<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
    					<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
    				</w:tcBorders>
    			</w:tcPr>
    			<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
    				<w:pPr>
    					<w:rPr>
    						<w:sz w:val="6"/>
    						<w:szCs w:val="6"/>
    					</w:rPr>
    				</w:pPr>
    			</w:p>
    		</w:tc>
    	</w:tr>
    	<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
    		<w:tblPrEx>
    			<w:tblCellMar>
    				<w:top w:w="0" w:type="dxa"/>
    				<w:bottom w:w="0" w:type="dxa"/>
    			</w:tblCellMar>
    		</w:tblPrEx>
    		<w:trPr>
    			<w:cantSplit/>
    			<w:trHeight w:val="312"/>
    		</w:trPr>
    		<w:tc>
    			<w:tcPr>
    				<w:tcW w:w="9670" w:type="dxa"/>
    				<w:gridSpan w:val="22"/>
    				<w:tcBorders>
    					<w:top w:val="nil"/>
    					<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
    					<w:bottom w:val="nil"/>
    					<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
    				</w:tcBorders>
    				<w:vAlign w:val="bottom"/>
    			</w:tcPr>
    			<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
    				<w:pPr>
    					<w:ind w:left="85" w:right="85"/>
    					<w:jc w:val="both"/>
    					<w:rPr>
    						<w:sz w:val="2"/>
    						<w:szCs w:val="2"/>
    					</w:rPr>
    				</w:pPr>
    				<w:r>
    					<w:rPr>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    					<w:t>Почтовый адрес и адрес электронной почты, по которым осуществляется связь с кадастровым</w:t>
    				</w:r>
    				<w:r>
    					<w:rPr>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    					<w:br/>
    				</w:r>
    			</w:p>
    		</w:tc>
    	</w:tr>
    	<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
    		<w:tblPrEx>
    			<w:tblCellMar>
    				<w:top w:w="0" w:type="dxa"/>
    				<w:bottom w:w="0" w:type="dxa"/>
    			</w:tblCellMar>
    		</w:tblPrEx>
    		<w:trPr>
    			<w:cantSplit/>
    		</w:trPr>
    		<w:tc>
    			<w:tcPr>
    				<w:tcW w:w="1244" w:type="dxa"/>
    				<w:gridSpan w:val="4"/>
    				<w:tcBorders>
    					<w:top w:val="nil"/>
    					<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
    					<w:bottom w:val="nil"/>
    					<w:right w:val="nil"/>
    				</w:tcBorders>
    				<w:vAlign w:val="bottom"/>
    			</w:tcPr>
    			<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
    				<w:pPr>
    					<w:ind w:left="85"/>
    					<w:rPr>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    				</w:pPr>
    				<w:proofErr w:type="gramStart"/>
    				<w:r>
    					<w:rPr>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    					<w:t>инженером</w:t>
    				</w:r>
    				<w:proofErr w:type="gramEnd"/>
    			</w:p>
    		</w:tc>
    		<w:tc>
    			<w:tcPr>
    				<w:tcW w:w="8285" w:type="dxa"/>
    				<w:gridSpan w:val="17"/>
    				<w:tcBorders>
    					<w:top w:val="nil"/>
    					<w:left w:val="nil"/>
    					<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
    					<w:right w:val="nil"/>
    				</w:tcBorders>
    				<w:vAlign w:val="bottom"/>
    			</w:tcPr>
    			<w:p w:rsidR="007E23C6" w:rsidRDefault="00D629DE">
    				<w:pPr>
    					<w:rPr>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    				</w:pPr>
    				<w:r>
    					<w:rPr>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    					<w:t><xsl:value-of select="normalize-space(Address)"/><xsl:text> </xsl:text><xsl:value-of select="normalize-space(E_mail)"/></w:t>
    				</w:r>
    			</w:p>
    		</w:tc>
    		<w:tc>
    			<w:tcPr>
    				<w:tcW w:w="141" w:type="dxa"/>
    				<w:tcBorders>
    					<w:top w:val="nil"/>
    					<w:left w:val="nil"/>
    					<w:bottom w:val="nil"/>
    					<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
    				</w:tcBorders>
    				<w:vAlign w:val="bottom"/>
    			</w:tcPr>
    			<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
    				<w:pPr>
    					<w:rPr>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    				</w:pPr>
    			</w:p>
    		</w:tc>
    	</w:tr>
    	
    	<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
    		<w:tblPrEx>
    			<w:tblCellMar>
    				<w:top w:w="0" w:type="dxa"/>
    				<w:bottom w:w="0" w:type="dxa"/>
    			</w:tblCellMar>
    		</w:tblPrEx>
    		<w:trPr>
    			<w:cantSplit/>
    		</w:trPr>
    		<w:tc>
    			<w:tcPr>
    				<w:tcW w:w="9670" w:type="dxa"/>
    				<w:gridSpan w:val="22"/>
    				<w:tcBorders>
    					<w:top w:val="nil"/>
    					<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
    					<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
    					<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
    				</w:tcBorders>
    				<w:vAlign w:val="bottom"/>
    			</w:tcPr>
    			<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
    				<w:pPr>
    					<w:rPr>
    						<w:sz w:val="6"/>
    						<w:szCs w:val="6"/>
    					</w:rPr>
    				</w:pPr>
    			</w:p>
    		</w:tc>
    	</w:tr>
    	<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
    		<w:tblPrEx>
    			<w:tblCellMar>
    				<w:top w:w="0" w:type="dxa"/>
    				<w:bottom w:w="0" w:type="dxa"/>
    			</w:tblCellMar>
    		</w:tblPrEx>
    		<w:trPr>
    			<w:cantSplit/>
    			<w:trHeight w:val="312"/>
    		</w:trPr>
    		<w:tc>
    			<w:tcPr>
    				<w:tcW w:w="9670" w:type="dxa"/>
    				<w:gridSpan w:val="22"/>
    				<w:tcBorders>
    					<w:top w:val="nil"/>
    					<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
    					<w:bottom w:val="nil"/>
    					<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
    				</w:tcBorders>
    				<w:vAlign w:val="bottom"/>
    			</w:tcPr>
    			<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
    				<w:pPr>
    					<w:ind w:left="85" w:right="85"/>
    					<w:jc w:val="both"/>
    					<w:rPr>
    						<w:sz w:val="2"/>
    						<w:szCs w:val="2"/>
    					</w:rPr>
    				</w:pPr>
    				<w:r>
    					<w:rPr>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    					<w:t>Сокращенное наименование юридического лица, если кадастровый инженер является работником</w:t>
    				</w:r>
    				<w:r>
    					<w:rPr>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    					<w:br/>
    				</w:r>
    			</w:p>
    		</w:tc>
    	</w:tr>
    	<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
    		<w:tblPrEx>
    			<w:tblCellMar>
    				<w:top w:w="0" w:type="dxa"/>
    				<w:bottom w:w="0" w:type="dxa"/>
    			</w:tblCellMar>
    		</w:tblPrEx>
    		<w:trPr>
    			<w:cantSplit/>
    		</w:trPr>
    		<w:tc>
    			<w:tcPr>
    				<w:tcW w:w="1971" w:type="dxa"/>
    				<w:gridSpan w:val="5"/>
    				<w:tcBorders>
    					<w:top w:val="nil"/>
    					<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
    					<w:bottom w:val="nil"/>
    					<w:right w:val="nil"/>
    				</w:tcBorders>
    				<w:vAlign w:val="bottom"/>
    			</w:tcPr>
    			<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
    				<w:pPr>
    					<w:ind w:left="85"/>
    					<w:rPr>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    				</w:pPr>
    				<w:proofErr w:type="gramStart"/>
    				<w:r>
    					<w:rPr>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    					<w:t>юридического</w:t>
    				</w:r>
    				<w:proofErr w:type="gramEnd"/>
    				<w:r>
    					<w:rPr>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    					<w:t xml:space="preserve"> лица</w:t>
    				</w:r>
    			</w:p>
    		</w:tc>
    		<w:tc>
    			<w:tcPr>
    				<w:tcW w:w="7558" w:type="dxa"/>
    				<w:gridSpan w:val="16"/>
    				<w:tcBorders>
    					<w:top w:val="nil"/>
    					<w:left w:val="nil"/>
    					<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
    					<w:right w:val="nil"/>
    				</w:tcBorders>
    				<w:vAlign w:val="bottom"/>
    			</w:tcPr>
    			<w:p w:rsidR="007E23C6" w:rsidRDefault="00D629DE">
    				<w:pPr>
    					<w:rPr>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    				</w:pPr>
    				<w:proofErr w:type="spellStart"/>
    				<w:r>
    					<w:rPr>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    					<w:t><xsl:value-of select="normalize-space(Organization)"/></w:t>
    				</w:r>
    				<w:proofErr w:type="spellEnd"/>
    				<w:r>
    					<w:rPr>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    					<w:t xml:space="preserve"> </w:t>
    				</w:r>
    				<w:proofErr w:type="spellStart"/>
    				<w:r>
    					<w:rPr>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    					<w:t></w:t>
    				</w:r>
    				<w:proofErr w:type="spellEnd"/>
    			</w:p>
    		</w:tc>
    		<w:tc>
    			<w:tcPr>
    				<w:tcW w:w="141" w:type="dxa"/>
    				<w:tcBorders>
    					<w:top w:val="nil"/>
    					<w:left w:val="nil"/>
    					<w:bottom w:val="nil"/>
    					<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
    				</w:tcBorders>
    				<w:vAlign w:val="bottom"/>
    			</w:tcPr>
    			<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
    				<w:pPr>
    					<w:rPr>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    				</w:pPr>
    			</w:p>
    		</w:tc>
    	</w:tr>
    	
    	<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
    		<w:tblPrEx>
    			<w:tblCellMar>
    				<w:top w:w="0" w:type="dxa"/>
    				<w:bottom w:w="0" w:type="dxa"/>
    			</w:tblCellMar>
    		</w:tblPrEx>
    		<w:trPr>
    			<w:cantSplit/>
    		</w:trPr>
    		<w:tc>
    			<w:tcPr>
    				<w:tcW w:w="9670" w:type="dxa"/>
    				<w:gridSpan w:val="22"/>
    				<w:tcBorders>
    					<w:top w:val="nil"/>
    					<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
    					<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
    					<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
    				</w:tcBorders>
    				<w:vAlign w:val="bottom"/>
    			</w:tcPr>
    			<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
    				<w:pPr>
    					<w:rPr>
    						<w:sz w:val="6"/>
    						<w:szCs w:val="6"/>
    					</w:rPr>
    				</w:pPr>
    			</w:p>
    		</w:tc>
    	</w:tr>
    	<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
    		<w:tblPrEx>
    			<w:tblCellMar>
    				<w:top w:w="0" w:type="dxa"/>
    				<w:bottom w:w="0" w:type="dxa"/>
    			</w:tblCellMar>
    		</w:tblPrEx>
    		<w:trPr>
    			<w:cantSplit/>
    			<w:trHeight w:val="299"/>
    		</w:trPr>
    		<w:tc>
    			<w:tcPr>
    				<w:tcW w:w="3288" w:type="dxa"/>
    				<w:gridSpan w:val="8"/>
    				<w:tcBorders>
    					<w:top w:val="nil"/>
    					<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
    					<w:bottom w:val="nil"/>
    					<w:right w:val="nil"/>
    				</w:tcBorders>
    				<w:vAlign w:val="bottom"/>
    			</w:tcPr>
    			<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
    				<w:pPr>
    					<w:ind w:left="85"/>
    					<w:rPr>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    				</w:pPr>
    				<w:r>
    					<w:rPr>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    					<w:t>Дата подготовки межевого плана “</w:t>
    				</w:r>
    			</w:p>
    		</w:tc>
    		<w:tc>
    			<w:tcPr>
    				<w:tcW w:w="571" w:type="dxa"/>
    				<w:tcBorders>
    					<w:top w:val="nil"/>
    					<w:left w:val="nil"/>
    					<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
    					<w:right w:val="nil"/>
    				</w:tcBorders>
    				<w:vAlign w:val="bottom"/>
    			</w:tcPr>
    			<w:p w:rsidR="007E23C6" w:rsidRDefault="00D629DE">
    				<w:pPr>
    					<w:jc w:val="center"/>
    					<w:rPr>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    				</w:pPr>
    				<w:r>
    					<w:rPr>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    					<w:t>  </w:t>
    				</w:r>
    			</w:p>
    		</w:tc>
    		<w:tc>
    			<w:tcPr>
    				<w:tcW w:w="267" w:type="dxa"/>
    				<w:tcBorders>
    					<w:top w:val="nil"/>
    					<w:left w:val="nil"/>
    					<w:bottom w:val="nil"/>
    					<w:right w:val="nil"/>
    				</w:tcBorders>
    				<w:vAlign w:val="bottom"/>
    			</w:tcPr>
    			<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
    				<w:pPr>
    					<w:rPr>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    				</w:pPr>
    				<w:r>
    					<w:rPr>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    					<w:t>”</w:t>
    				</w:r>
    			</w:p>
    		</w:tc>
    		<w:tc>
    			<w:tcPr>
    				<w:tcW w:w="1434" w:type="dxa"/>
    				<w:gridSpan w:val="5"/>
    				<w:tcBorders>
    					<w:top w:val="nil"/>
    					<w:left w:val="nil"/>
    					<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
    					<w:right w:val="nil"/>
    				</w:tcBorders>
    				<w:vAlign w:val="bottom"/>
    			</w:tcPr>
    			<w:p w:rsidR="007E23C6" w:rsidRDefault="00D629DE">
    				<w:pPr>
    					<w:jc w:val="center"/>
    					<w:rPr>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    				</w:pPr>
    				<w:proofErr w:type="gramStart"/>
    				<w:r>
    					<w:rPr>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    					<w:t>  </w:t>
    				</w:r>
    				<w:proofErr w:type="gramEnd"/>
    			</w:p>
    		</w:tc>
    		<w:tc>
    			<w:tcPr>
    				<w:tcW w:w="113" w:type="dxa"/>
    				<w:tcBorders>
    					<w:top w:val="nil"/>
    					<w:left w:val="nil"/>
    					<w:bottom w:val="nil"/>
    					<w:right w:val="nil"/>
    				</w:tcBorders>
    				<w:vAlign w:val="bottom"/>
    			</w:tcPr>
    			<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
    				<w:pPr>
    					<w:rPr>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    				</w:pPr>
    			</w:p>
    		</w:tc>
    		<w:tc>
    			<w:tcPr>
    				<w:tcW w:w="737" w:type="dxa"/>
    				<w:gridSpan w:val="2"/>
    				<w:tcBorders>
    					<w:top w:val="nil"/>
    					<w:left w:val="nil"/>
    					<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
    					<w:right w:val="nil"/>
    				</w:tcBorders>
    				<w:vAlign w:val="bottom"/>
    			</w:tcPr>
    			<w:p w:rsidR="007E23C6" w:rsidRDefault="00D629DE">
    				<w:pPr>
    					<w:jc w:val="center"/>
    					<w:rPr>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    				</w:pPr>
    				<w:r>
    					<w:rPr>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    					<w:t>    </w:t>
    				</w:r>
    			</w:p>
    		</w:tc>
    		<w:tc>
    			<w:tcPr>
    				<w:tcW w:w="3260" w:type="dxa"/>
    				<w:gridSpan w:val="4"/>
    				<w:tcBorders>
    					<w:top w:val="nil"/>
    					<w:left w:val="nil"/>
    					<w:bottom w:val="nil"/>
    					<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
    				</w:tcBorders>
    				<w:vAlign w:val="bottom"/>
    			</w:tcPr>
    			<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
    				<w:pPr>
    					<w:ind w:left="57"/>
    					<w:rPr>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    				</w:pPr>
    				<w:r>
    					<w:rPr>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    					<w:t>г.</w:t>
    				</w:r>
    			</w:p>
    		</w:tc>
    	</w:tr>
    	<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
    		<w:tblPrEx>
    			<w:tblCellMar>
    				<w:top w:w="0" w:type="dxa"/>
    				<w:bottom w:w="0" w:type="dxa"/>
    			</w:tblCellMar>
    		</w:tblPrEx>
    		<w:trPr>
    			<w:cantSplit/>
    		</w:trPr>
    		<w:tc>
    			<w:tcPr>
    				<w:tcW w:w="9670" w:type="dxa"/>
    				<w:gridSpan w:val="22"/>
    				<w:tcBorders>
    					<w:top w:val="nil"/>
    					<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
    					<w:bottom w:val="double"/>
    					<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
    				</w:tcBorders>
    				<w:vAlign w:val="bottom"/>
    			</w:tcPr>
    			<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
    				<w:pPr>
    					<w:rPr>
    						<w:sz w:val="9"/>
    						<w:szCs w:val="9"/>
    					</w:rPr>
    				</w:pPr>
    			</w:p>
    		</w:tc>
    	</w:tr>
    	
     </xsl:template>
    
    
    
    <!--Физическое лицо-->
    <xsl:template match="Person" mode="Title">
            <xsl:apply-templates select="FIO" mode="tFIO"/>
    </xsl:template>    
    <!--Organization Российское юридическое лицо-->
    <xsl:template match="Organization" mode="Title">
            <xsl:if test="Name!=''">
                    <xsl:value-of select="normalize-space(Name)"/>
            </xsl:if>
    </xsl:template>
    <!--Foreign_Organization Иностранное юридическое лицо-->
    <xsl:template match="Foreign_Organization" mode="Title">
            <xsl:if test="Name!=''">
                    <xsl:value-of select="normalize-space(Name)"/>
            </xsl:if>
            <xsl:if test="Country!=''">
                    <xsl:value-of select="normalize-space(Country)"/>
            </xsl:if>
    </xsl:template>
    <!--Орган гос. власти-->
    <xsl:template match="Governance" mode="Title">
            <xsl:if test="Name!=''">
                    <xsl:value-of select="normalize-space(Name)"/>
            </xsl:if>
    </xsl:template>
    
    <xsl:template match="FIO" mode="tFIO">
            <xsl:call-template name="tFIO"/>
    </xsl:template>
    <xsl:template name="tFIO">
    	<xsl:value-of select="normalize-space(Surname)"/><xsl:text> </xsl:text>
    	<xsl:value-of select="normalize-space(First)"/> <xsl:text> </xsl:text>
    	<xsl:value-of select="normalize-space(Patronymic)"/>
    </xsl:template>
    
    
    
   
    <xsl:template match="Coord_Systems">
            <xsl:apply-templates select="Coord_System"/>
    </xsl:template>
    <xsl:template match="Coord_System">
                    <xsl:value-of select="@Name"/> 
    </xsl:template>
    
  
	
     
   
      <xsl:template name="Racheti">
     	<w:tbl>
						<w:tblPr>
							<w:tblW w:w="0" w:type="auto"/>
							<w:tblInd w:w="-544" w:type="dxa"/>
							<w:tblLayout w:type="fixed"/>
							<w:tblCellMar>
								<w:left w:w="28" w:type="dxa"/>
								<w:right w:w="28" w:type="dxa"/>
							</w:tblCellMar>
							<w:tblLook w:val="0000" w:firstRow="0" w:lastRow="0" w:firstColumn="0" w:lastColumn="0" w:noHBand="0" w:noVBand="0"/>
						</w:tblPr>
						<w:tblGrid>
							<w:gridCol w:w="671"/>
							<w:gridCol w:w="1909"/>
							<w:gridCol w:w="850"/>
							<w:gridCol w:w="567"/>
							<w:gridCol w:w="142"/>
							<w:gridCol w:w="851"/>
							<w:gridCol w:w="711"/>
							<w:gridCol w:w="990"/>
							<w:gridCol w:w="2979"/>
						</w:tblGrid>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:trHeight w:val="440"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="9670" w:type="dxa"/>
									<w:gridSpan w:val="9"/>
									<w:tcBorders>
										<w:top w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="27"/>
											<w:szCs w:val="27"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="27"/>
											<w:szCs w:val="27"/>
										</w:rPr>
										<w:t>Сведения о выполненных измерениях и расчетах</w:t>
									</w:r>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
								<w:trHeight w:val="360"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="9670" w:type="dxa"/>
									<w:gridSpan w:val="9"/>
									<w:tcBorders>
										<w:top w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>1. Метод определения координат характерных точек границ земельных участков и их частей</w:t>
									</w:r>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:trHeight w:val="510"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="671" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>№</w:t>
									</w:r>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:br/>
										<w:t>п/п</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="3468" w:type="dxa"/>
									<w:gridSpan w:val="4"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>Кадастровый номер или обозначение земельного участка, частей земельного участка</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="5531" w:type="dxa"/>
									<w:gridSpan w:val="4"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>Метод определения координат</w:t>
									</w:r>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:trHeight w:val="369"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="671" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>1</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="3468" w:type="dxa"/>
									<w:gridSpan w:val="4"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>2</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="5531" w:type="dxa"/>
									<w:gridSpan w:val="4"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>3</w:t>
									</w:r>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:trHeight w:val="425"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="671" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRPr="00AA4A27" w:rsidRDefault="00AA4A27">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
									</w:pPr>
									<w:proofErr w:type="spellStart"/>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
										<w:t><xsl:value-of select="position()"/></w:t>
									</w:r>
									<w:proofErr w:type="spellEnd"/>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="3468" w:type="dxa"/>
									<w:gridSpan w:val="4"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRPr="00AA4A27" w:rsidRDefault="00AA4A27">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
									</w:pPr>
									<w:proofErr w:type="spellStart"/>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
										<w:t>#Note</w:t>
									</w:r>
									<w:proofErr w:type="spellEnd"/>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="5531" w:type="dxa"/>
									<w:gridSpan w:val="4"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:ind w:left="57" w:right="57"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
								<w:trHeight w:val="360"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="9670" w:type="dxa"/>
									<w:gridSpan w:val="9"/>
									<w:tcBorders>
										<w:top w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>2. Точность положения характерных точек границ земельных участков</w:t>
									</w:r>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:trHeight w:val="510"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="671" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>№</w:t>
									</w:r>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:br/>
										<w:t>п/п</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="3326" w:type="dxa"/>
									<w:gridSpan w:val="3"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>Кадастровый номер или обозначение земельного</w:t>
									</w:r>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:br/>
										<w:t>участка</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="5673" w:type="dxa"/>
									<w:gridSpan w:val="5"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t xml:space="preserve">Формулы, примененные для расчета средней </w:t>
									</w:r>
									<w:proofErr w:type="spellStart"/>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>квадратической</w:t>
									</w:r>
									<w:proofErr w:type="spellEnd"/>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t xml:space="preserve"> погрешности положения</w:t>
									</w:r>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:br/>
										<w:t>характерных точек границ (</w:t>
									</w:r>
									<w:proofErr w:type="spellStart"/>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>М</w:t>
									</w:r>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
											<w:vertAlign w:val="subscript"/>
										</w:rPr>
										<w:t>t</w:t>
									</w:r>
									<w:proofErr w:type="spellEnd"/>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>), м</w:t>
									</w:r>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:trHeight w:val="369"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="671" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>1</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="3326" w:type="dxa"/>
									<w:gridSpan w:val="3"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>2</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="5673" w:type="dxa"/>
									<w:gridSpan w:val="5"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>3</w:t>
									</w:r>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:trHeight w:val="425"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="671" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="3326" w:type="dxa"/>
									<w:gridSpan w:val="3"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="5673" w:type="dxa"/>
									<w:gridSpan w:val="5"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:ind w:left="57" w:right="57"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
								<w:trHeight w:val="360"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="9670" w:type="dxa"/>
									<w:gridSpan w:val="9"/>
									<w:tcBorders>
										<w:top w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>3. Точность положения характерных точек границ частей земельных участков</w:t>
									</w:r>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
								<w:trHeight w:val="992"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="671" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>№</w:t>
									</w:r>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:br/>
										<w:t>п/п</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2759" w:type="dxa"/>
									<w:gridSpan w:val="2"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>Кадастровый номер или обозначение земельного участка</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2271" w:type="dxa"/>
									<w:gridSpan w:val="4"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>Учетный номер или обозначение части</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="3969" w:type="dxa"/>
									<w:gridSpan w:val="2"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>Формулы, примененные для</w:t>
									</w:r>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:br/>
										<w:t xml:space="preserve">расчета средней </w:t>
									</w:r>
									<w:proofErr w:type="spellStart"/>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>квадратической</w:t>
									</w:r>
									<w:proofErr w:type="spellEnd"/>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t xml:space="preserve"> погрешности положения</w:t>
									</w:r>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:br/>
										<w:t>характерных точек границ (</w:t>
									</w:r>
									<w:proofErr w:type="spellStart"/>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>М</w:t>
									</w:r>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
											<w:vertAlign w:val="subscript"/>
										</w:rPr>
										<w:t>t</w:t>
									</w:r>
									<w:proofErr w:type="spellEnd"/>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>), м</w:t>
									</w:r>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
								<w:trHeight w:val="369"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="671" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>1</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2759" w:type="dxa"/>
									<w:gridSpan w:val="2"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>2</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2271" w:type="dxa"/>
									<w:gridSpan w:val="4"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>3</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="3969" w:type="dxa"/>
									<w:gridSpan w:val="2"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>4</w:t>
									</w:r>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
								<w:trHeight w:val="369"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="671" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2759" w:type="dxa"/>
									<w:gridSpan w:val="2"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2271" w:type="dxa"/>
									<w:gridSpan w:val="4"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="3969" w:type="dxa"/>
									<w:gridSpan w:val="2"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:ind w:left="57" w:right="57"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
								<w:trHeight w:val="360"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="9670" w:type="dxa"/>
									<w:gridSpan w:val="9"/>
									<w:tcBorders>
										<w:top w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>4. Точность определения площади земельных участков</w:t>
									</w:r>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
								<w:trHeight w:val="992"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="671" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>№</w:t>
									</w:r>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:br/>
										<w:t>п/п</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2759" w:type="dxa"/>
									<w:gridSpan w:val="2"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>Кадастровый номер или обозначение земельного участка</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2271" w:type="dxa"/>
									<w:gridSpan w:val="4"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>Площадь (Р), м</w:t>
									</w:r>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
											<w:vertAlign w:val="superscript"/>
										</w:rPr>
										<w:t>2</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="3969" w:type="dxa"/>
									<w:gridSpan w:val="2"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>Формулы, примененные для расчета предельной допустимой погрешности определения площади земельного участка (ΔР), м</w:t>
									</w:r>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
											<w:vertAlign w:val="superscript"/>
										</w:rPr>
										<w:t>2</w:t>
									</w:r>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
								<w:trHeight w:val="369"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="671" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>1</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2759" w:type="dxa"/>
									<w:gridSpan w:val="2"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>2</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2271" w:type="dxa"/>
									<w:gridSpan w:val="4"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>3</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="3969" w:type="dxa"/>
									<w:gridSpan w:val="2"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>4</w:t>
									</w:r>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
								<w:trHeight w:val="369"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="671" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2759" w:type="dxa"/>
									<w:gridSpan w:val="2"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2271" w:type="dxa"/>
									<w:gridSpan w:val="4"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="3969" w:type="dxa"/>
									<w:gridSpan w:val="2"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:ind w:left="57" w:right="57"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
								<w:trHeight w:val="360"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="9670" w:type="dxa"/>
									<w:gridSpan w:val="9"/>
									<w:tcBorders>
										<w:top w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>5. Точность определения площади частей земельных участков</w:t>
									</w:r>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
								<w:trHeight w:val="369"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="671" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>1</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1909" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>2</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2410" w:type="dxa"/>
									<w:gridSpan w:val="4"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>3</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1701" w:type="dxa"/>
									<w:gridSpan w:val="2"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>4</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2979" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>5</w:t>
									</w:r>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
								<w:trHeight w:val="369"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="671" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1909" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2410" w:type="dxa"/>
									<w:gridSpan w:val="4"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1701" w:type="dxa"/>
									<w:gridSpan w:val="2"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2979" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:ind w:left="57" w:right="57"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
						</w:tr>
					</w:tbl>
					 
    </xsl:template>
    
    
    <!--Титульный лист-->
    <xsl:template match="Title">
        <w:tbl>
            <w:tblPr>
                <w:tblW w:w="0" w:type="auto"/>
            	<w:tblInd w:w="-544" w:type="dxa"/>
                <w:tblLayout w:type="fixed"/>
                <w:tblCellMar>
                    <w:left w:w="28" w:type="dxa"/>
                    <w:right w:w="28" w:type="dxa"/>
                </w:tblCellMar>
                <w:tblLook w:val="0000" w:firstRow="0" w:lastRow="0" w:firstColumn="0" w:lastColumn="0" w:noHBand="0" w:noVBand="0"/>
            </w:tblPr>
            <w:tblGrid>
                <w:gridCol w:w="170"/>
                <w:gridCol w:w="501"/>
                <w:gridCol w:w="9"/>
                <w:gridCol w:w="564"/>
                <w:gridCol w:w="727"/>
                <w:gridCol w:w="126"/>
                <w:gridCol w:w="1106"/>
                <w:gridCol w:w="85"/>
                <w:gridCol w:w="571"/>
                <w:gridCol w:w="267"/>
                <w:gridCol w:w="277"/>
                <w:gridCol w:w="303"/>
                <w:gridCol w:w="425"/>
                <w:gridCol w:w="170"/>
                <w:gridCol w:w="259"/>
                <w:gridCol w:w="113"/>
                <w:gridCol w:w="28"/>
                <w:gridCol w:w="709"/>
                <w:gridCol w:w="706"/>
                <w:gridCol w:w="428"/>
                <w:gridCol w:w="1985"/>
                <w:gridCol w:w="141"/>
            </w:tblGrid>
            <w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
                <w:tblPrEx>
                    <w:tblCellMar>
                        <w:top w:w="0" w:type="dxa"/>
                        <w:bottom w:w="0" w:type="dxa"/>
                    </w:tblCellMar>
                </w:tblPrEx>
                <w:trPr>
                    <w:trHeight w:val="454"/>
                </w:trPr>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="9670" w:type="dxa"/>
                        <w:gridSpan w:val="22"/>
                        <w:tcBorders>
                            <w:top w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                            <w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                            <w:bottom w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                            <w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                        </w:tcBorders>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
                        <w:pPr>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:b/>
                                <w:bCs/>
                                <w:sz w:val="27"/>
                                <w:szCs w:val="27"/>
                            </w:rPr>
                        </w:pPr>
                        <w:bookmarkStart w:id="0" w:name="_GoBack" w:colFirst="0" w:colLast="0"/>
                        <w:r>
                            <w:rPr>
                                <w:b/>
                                <w:bCs/>
                                <w:sz w:val="27"/>
                                <w:szCs w:val="27"/>
                            </w:rPr>
                            <w:t>МЕЖЕВОЙ ПЛАН</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
            </w:tr>
            <!--== Общие сведения =========================================== -->
            <w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
                <w:tblPrEx>
                    <w:tblCellMar>
                        <w:top w:w="0" w:type="dxa"/>
                        <w:bottom w:w="0" w:type="dxa"/>
                    </w:tblCellMar>
                </w:tblPrEx>
                <w:trPr>
                    <w:trHeight w:val="454"/>
                </w:trPr>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="9670" w:type="dxa"/>
                        <w:gridSpan w:val="22"/>
                        <w:tcBorders>
                            <w:top w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                            <w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                            <w:bottom w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                            <w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                        </w:tcBorders>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
                        <w:pPr>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:b/>
                                <w:bCs/>
                                <w:sz w:val="27"/>
                                <w:szCs w:val="27"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:b/>
                                <w:bCs/>
                                <w:sz w:val="27"/>
                                <w:szCs w:val="27"/>
                            </w:rPr>
                            <w:t>Общие сведения о кадастровых работах</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
            </w:tr>
            <!-- == 1. Межевой план подготовлен в результате выполнения кадастровых работ в связи с -->
            <w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
                <w:tblPrEx>
                    <w:tblCellMar>
                        <w:top w:w="0" w:type="dxa"/>
                        <w:bottom w:w="0" w:type="dxa"/>
                    </w:tblCellMar>
                </w:tblPrEx>
                <w:trPr>
                    <w:cantSplit/>
                    <w:trHeight w:val="312"/>
                </w:trPr>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="9670" w:type="dxa"/>
                        <w:gridSpan w:val="22"/>
                        <w:tcBorders>
                            <w:top w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                            <w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                            <w:bottom w:val="nil"/>
                            <w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                        </w:tcBorders>
                        <w:vAlign w:val="bottom"/>
                    </w:tcPr>
                    <w:p w:rsidR="007E23C6" w:rsidRPr="00D629DE" w:rsidRDefault="007E23C6">
                        <w:pPr>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:b/>
                                <w:bCs/>
                                <w:sz w:val="21"/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:b/>
                                <w:bCs/>
                                <w:sz w:val="21"/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                            <w:t>1. Межевой план подготовлен в результате выполнения кадастровых работ в связи с:</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
            </w:tr>
            <!-- Описание пункта 1.-->
            <w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
                <w:tblPrEx>
                    <w:tblCellMar>
                        <w:top w:w="0" w:type="dxa"/>
                        <w:bottom w:w="0" w:type="dxa"/>
                    </w:tblCellMar>
                </w:tblPrEx>
                <w:trPr>
                    <w:cantSplit/>
                    <w:trHeight w:val="312"/>
                </w:trPr>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="9670" w:type="dxa"/>
                        <w:gridSpan w:val="22"/>
                        <w:tcBorders>
                            <w:top w:val="nil"/>
                            <w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                            <w:bottom w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                            <w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                        </w:tcBorders>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p w:rsidR="007E23C6" w:rsidRPr="00AA4A27" w:rsidRDefault="00D629DE" w:rsidP="00AA4A27">
                        <w:pPr>
                            <w:ind w:left="85" w:right="85"/>
                            <w:rPr>
                                <w:bCs/>
                                <w:sz w:val="21"/>
                                <w:szCs w:val="21"/>
                                <w:lang w:val="en-US"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="00D629DE">
                            <w:rPr>
                                <w:b/>
                                <w:bCs/>
                                <w:sz w:val="21"/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                            <w:t xml:space="preserve"> </w:t>
                        </w:r>
                        <w:r w:rsidRPr="00FC287A">
                            <w:rPr>
                                <w:b/>
                                <w:bCs/>
                                <w:sz w:val="21"/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                            <w:t xml:space="preserve">                                            </w:t>
                        </w:r>
                        <w:proofErr w:type="spellStart"/>
                        <w:r w:rsidR="00AA4A27">
                            <w:rPr>
                                <w:b/>
                                <w:bCs/>
                                <w:sz w:val="21"/>
                                <w:szCs w:val="21"/>
                                <w:lang w:val="en-US"/>
                            </w:rPr>
                            <w:t><xsl:value-of select="normalize-space(Reason)"/></w:t>
                        </w:r>
                        <w:proofErr w:type="spellEnd"/>
                    </w:p>
                </w:tc>
            </w:tr>
            
            <!-- == Цель кадастровых работ  -->
            <w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
                <w:tblPrEx>
                    <w:tblCellMar>
                        <w:top w:w="0" w:type="dxa"/>
                        <w:bottom w:w="0" w:type="dxa"/>
                    </w:tblCellMar>
                </w:tblPrEx>
                <w:trPr>
                    <w:cantSplit/>
                    <w:trHeight w:val="320"/>
                </w:trPr>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="9670" w:type="dxa"/>
                        <w:gridSpan w:val="22"/>
                        <w:tcBorders>
                            <w:top w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                            <w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                            <w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                            <w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                        </w:tcBorders>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p w:rsidR="007E23C6" w:rsidRPr="00AA4A27" w:rsidRDefault="007E23C6">
                        <w:pPr>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:b/>
                                <w:bCs/>
                                <w:sz w:val="21"/>
                                <w:szCs w:val="21"/>
                                <w:lang w:val="en-US"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:b/>
                                <w:bCs/>
                                <w:sz w:val="21"/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                            <w:t>2. Цель кадастровых работ:</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
            </w:tr>
            
            <!-- Описани цели кадастровых работ  -->
            <w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
                <w:tblPrEx>
                    <w:tblCellMar>
                        <w:top w:w="0" w:type="dxa"/>
                        <w:bottom w:w="0" w:type="dxa"/>
                    </w:tblCellMar>
                </w:tblPrEx>
                <w:trPr>
                    <w:cantSplit/>
                    <w:trHeight w:val="312"/>
                </w:trPr>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="9670" w:type="dxa"/>
                        <w:gridSpan w:val="22"/>
                        <w:tcBorders>
                            <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                            <w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                            <w:bottom w:val="nil"/>
                            <w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                        </w:tcBorders>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p w:rsidR="007E23C6" w:rsidRPr="00AA4A27" w:rsidRDefault="00AA4A27" w:rsidP="00AA4A27">
                        <w:pPr>
                            <w:ind w:left="85" w:right="85"/>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:b/>
                                <w:bCs/>
                                <w:sz w:val="21"/>
                                <w:szCs w:val="21"/>
                                <w:lang w:val="en-US"/>
                            </w:rPr>
                        </w:pPr>
                        <w:proofErr w:type="spellStart"/>
                        <w:r>
                            <w:rPr>
                                <w:b/>
                                <w:bCs/>
                                <w:sz w:val="21"/>
                                <w:szCs w:val="21"/>
                                <w:lang w:val="en-US"/>
                            </w:rPr>
                            <w:t> <xsl:choose>
                                <xsl:when test="(Purpose != '')">
                                    <xsl:value-of select="normalize-space(Purpose)"/>                                    
                                </xsl:when>
                                <xsl:otherwise>
                                    <w:sym w:font="Symbol" w:char="F0BE"/>
                                </xsl:otherwise>
                            </xsl:choose> </w:t>
                        </w:r>
                        <w:proofErr w:type="spellEnd"/>
                    </w:p>
                </w:tc>
            </w:tr>
            <!--3. Cведения о заказчике кадатсровых работ  -->
            <w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
                <w:tblPrEx>
                    <w:tblCellMar>
                        <w:top w:w="0" w:type="dxa"/>
                        <w:bottom w:w="0" w:type="dxa"/>
                    </w:tblCellMar>
                </w:tblPrEx>
                <w:trPr>
                    <w:cantSplit/>
                    <w:trHeight w:val="360"/>
                </w:trPr>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="9670" w:type="dxa"/>
                        <w:gridSpan w:val="22"/>
                        <w:tcBorders>
                            <w:top w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                            <w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                            <w:bottom w:val="nil"/>
                            <w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                        </w:tcBorders>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
                        <w:pPr>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:b/>
                                <w:bCs/>
                                <w:sz w:val="21"/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:b/>
                                <w:bCs/>
                                <w:sz w:val="21"/>
                                <w:szCs w:val="21"/>
                            </w:rPr>
                            <w:t>3. Сведения о заказчике кадастровых работ:</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
            </w:tr>
            <!--Описание 3. -->
            <w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
                <w:tblPrEx>
                    <w:tblCellMar>
                        <w:top w:w="0" w:type="dxa"/>
                        <w:bottom w:w="0" w:type="dxa"/>
                    </w:tblCellMar>
                </w:tblPrEx>
                <w:trPr>
                    <w:trHeight w:val="275"/>
                </w:trPr>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="170" w:type="dxa"/>
                        <w:tcBorders>
                            <w:top w:val="nil"/>
                            <w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                            <w:bottom w:val="nil"/>
                            <w:right w:val="nil"/>
                        </w:tcBorders>
                        <w:vAlign w:val="bottom"/>
                    </w:tcPr>
                    <w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
                        <w:pPr>
                            <w:rPr>
                                <w:sz w:val="24"/>
                                <w:szCs w:val="24"/>
                            </w:rPr>
                        </w:pPr>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="9359" w:type="dxa"/>
                        <w:gridSpan w:val="20"/>
                        <w:tcBorders>
                            <w:top w:val="nil"/>
                            <w:left w:val="nil"/>
                            <w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                            <w:right w:val="nil"/>
                        </w:tcBorders>
                        <w:vAlign w:val="bottom"/>
                    </w:tcPr>
                    <w:p w:rsidR="007E23C6" w:rsidRDefault="00D629DE">
                        <w:pPr>
                            <w:rPr>
                                <w:sz w:val="24"/>
                                <w:szCs w:val="24"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:sz w:val="24"/>
                                <w:szCs w:val="24"/>
                            </w:rPr>
                            <w:t xml:space="preserve"> <xsl:apply-templates select="Client" mode="Title"/> </w:t>
                        </w:r>
                        <w:proofErr w:type="spellStart"/>
                        <w:r>
                            <w:rPr>
                                <w:sz w:val="24"/>
                                <w:szCs w:val="24"/>
                            </w:rPr>
                          
                        </w:r>
                        <w:proofErr w:type="spellEnd"/>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="141" w:type="dxa"/>
                        <w:tcBorders>
                            <w:top w:val="nil"/>
                            <w:left w:val="nil"/>
                            <w:bottom w:val="nil"/>
                            <w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                        </w:tcBorders>
                        <w:vAlign w:val="bottom"/>
                    </w:tcPr>
                    <w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
                        <w:pPr>
                            <w:rPr>
                                <w:sz w:val="24"/>
                                <w:szCs w:val="24"/>
                            </w:rPr>
                        </w:pPr>
                    </w:p>
                </w:tc>
            </w:tr>
            <w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
                <w:tblPrEx>
                    <w:tblCellMar>
                        <w:top w:w="0" w:type="dxa"/>
                        <w:bottom w:w="0" w:type="dxa"/>
                    </w:tblCellMar>
                </w:tblPrEx>
                <w:trPr>
                    <w:trHeight w:val="275"/>
                </w:trPr>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="170" w:type="dxa"/>
                        <w:tcBorders>
                            <w:top w:val="nil"/>
                            <w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                            <w:bottom w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                            <w:right w:val="nil"/>
                        </w:tcBorders>
                        <w:vAlign w:val="bottom"/>
                    </w:tcPr>
                    <w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
                        <w:pPr>
                            <w:rPr>
                                <w:sz w:val="24"/>
                                <w:szCs w:val="24"/>
                            </w:rPr>
                        </w:pPr>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="9359" w:type="dxa"/>
                        <w:gridSpan w:val="20"/>
                        <w:tcBorders>
                            <w:top w:val="nil"/>
                            <w:left w:val="nil"/>
                            <w:bottom w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                            <w:right w:val="nil"/>
                        </w:tcBorders>
                    </w:tcPr>
                    <w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
                        <w:pPr>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:i/>
                                <w:iCs/>
                                <w:sz w:val="19"/>
                                <w:szCs w:val="19"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:i/>
                                <w:iCs/>
                                <w:sz w:val="19"/>
                                <w:szCs w:val="19"/>
                            </w:rPr>
                            <w:t>(</w:t>
                        </w:r>
                        <w:proofErr w:type="gramStart"/>
                        <w:r>
                            <w:rPr>
                                <w:i/>
                                <w:iCs/>
                                <w:sz w:val="19"/>
                                <w:szCs w:val="19"/>
                            </w:rPr>
                            <w:t>фамилия</w:t>
                        </w:r>
                        <w:proofErr w:type="gramEnd"/>
                        <w:r>
                            <w:rPr>
                                <w:i/>
                                <w:iCs/>
                                <w:sz w:val="19"/>
                                <w:szCs w:val="19"/>
                            </w:rPr>
                            <w:t>, имя, отчество (при наличии отчества) физического лица, страховой номер индивидуального лицевого счета (при наличии), полное наименование юридического лица, органа государственной власти, органа местного самоуправления, иностранного юридического лица с указанием страны его регистрации (инкорпорации))</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="141" w:type="dxa"/>
                        <w:tcBorders>
                            <w:top w:val="nil"/>
                            <w:left w:val="nil"/>
                            <w:bottom w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                            <w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                        </w:tcBorders>
                        <w:vAlign w:val="bottom"/>
                    </w:tcPr>
                    <w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
                        <w:pPr>
                            <w:rPr>
                                <w:sz w:val="24"/>
                                <w:szCs w:val="24"/>
                            </w:rPr>
                        </w:pPr>
                    </w:p>
                </w:tc>
            </w:tr>
          
            <xsl:apply-templates select="Contractor" mode="Title"/>    
            <xsl:apply-templates select="parent::node()/Input_Data"/>
            <w:bookmarkEnd w:id="0"/>
        </w:tbl>
        <w:p w:rsidR="00AA4A27" w:rsidRDefault="00AA4A27"/>
        <w:p w:rsidR="00AA4A27" w:rsidRDefault="00AA4A27">
            <w:r>
                <w:br w:type="page"/>
            </w:r>
        </w:p>
        
    </xsl:template>
    
    <xsl:template match="Input_Data">    
    	<!-- разрыв страницы -->
    	<w:p w:rsidR="00D138AC" w:rsidRDefault="00D138AC" w:rsidP="00D138AC">
    		<w:pPr>
    			<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
    			<w:rPr>
    				<w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
    			</w:rPr>
    			<w:sectPr w:rsidR="00D138AC" w:rsidSect="00AA64DC">
    				<w:pgSz w:w="11906" w:h="16838"/>
    				<w:pgMar w:top="1134" w:right="850" w:bottom="1134" w:left="1701" w:header="708" w:footer="708" w:gutter="0"/>
    				<w:cols w:space="708"/>
    				<w:docGrid w:linePitch="360"/>
    			</w:sectPr>
    		</w:pPr>
    	</w:p>
    	<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
            <w:tblPrEx>
                <w:tblCellMar>
                    <w:top w:w="0" w:type="dxa"/>
                    <w:bottom w:w="0" w:type="dxa"/>
                </w:tblCellMar>
            </w:tblPrEx>
            <w:trPr>
                <w:trHeight w:val="440"/>
            </w:trPr>
            <w:tc>
                <w:tcPr>
                    <w:tcW w:w="9670" w:type="dxa"/>
                    <w:gridSpan w:val="22"/>
                    <w:tcBorders>
                        <w:top w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                        <w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                        <w:bottom w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                        <w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                    </w:tcBorders>
                    <w:vAlign w:val="center"/>
                </w:tcPr>
                <w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
                    <w:pPr>
                        <w:jc w:val="center"/>
                        <w:rPr>
                            <w:b/>
                            <w:bCs/>
                            <w:sz w:val="27"/>
                            <w:szCs w:val="27"/>
                        </w:rPr>
                    </w:pPr>
                    <w:r>
                        <w:rPr>
                            <w:b/>
                            <w:bCs/>
                            <w:sz w:val="27"/>
                            <w:szCs w:val="27"/>
                        </w:rPr>
                        <w:t>Исходные данные</w:t>
                    </w:r>
                </w:p>
            </w:tc>
        </w:tr>
        
         <!-- Documents -->
    	<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
    		<w:tblPrEx>
    			<w:tblCellMar>
    				<w:top w:w="0" w:type="dxa"/>
    				<w:bottom w:w="0" w:type="dxa"/>
    			</w:tblCellMar>
    		</w:tblPrEx>
    		<w:trPr>
    			<w:cantSplit/>
    			<w:trHeight w:val="360"/>
    		</w:trPr>
    		<w:tc>
    			<w:tcPr>
    				<w:tcW w:w="9670" w:type="dxa"/>
    				<w:gridSpan w:val="22"/>
    				<w:tcBorders>
    					<w:top w:val="double" w:sz="6" w:space="0" w:color="auto"/>
    					<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
    					<w:bottom w:val="nil"/>
    					<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
    				</w:tcBorders>
    				<w:vAlign w:val="center"/>
    			</w:tcPr>
    			<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
    				<w:pPr>
    					<w:jc w:val="center"/>
    					<w:rPr>
    						<w:b/>
    						<w:bCs/>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    				</w:pPr>
    				<w:r>
    					<w:rPr>
    						<w:b/>
    						<w:bCs/>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    					<w:t>1. Перечень документов, использованных при подготовке межевого плана</w:t>
    				</w:r>
    			</w:p>
    		</w:tc>
    	</w:tr>
    	<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
    		<w:tblPrEx>
    			<w:tblCellMar>
    				<w:top w:w="0" w:type="dxa"/>
    				<w:bottom w:w="0" w:type="dxa"/>
    			</w:tblCellMar>
    		</w:tblPrEx>
    		<w:trPr>
    			<w:trHeight w:val="510"/>
    		</w:trPr>
    		<w:tc>
    			<w:tcPr>
    				<w:tcW w:w="680" w:type="dxa"/>
    				<w:gridSpan w:val="3"/>
    				<w:tcBorders>
    					<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
    					<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
    					<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
    					<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
    				</w:tcBorders>
    				<w:vAlign w:val="center"/>
    			</w:tcPr>
    			<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
    				<w:pPr>
    					<w:jc w:val="center"/>
    					<w:rPr>
    						<w:b/>
    						<w:bCs/>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    				</w:pPr>
    				<w:r>
    					<w:rPr>
    						<w:b/>
    						<w:bCs/>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    					<w:t>№</w:t>
    				</w:r>
    				<w:r>
    					<w:rPr>
    						<w:b/>
    						<w:bCs/>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    					<w:br/>
    					<w:t>п/п</w:t>
    				</w:r>
    			</w:p>
    		</w:tc>
    		<w:tc>
    			<w:tcPr>
    				<w:tcW w:w="4451" w:type="dxa"/>
    				<w:gridSpan w:val="10"/>
    				<w:tcBorders>
    					<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
    					<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
    					<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
    					<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
    				</w:tcBorders>
    				<w:vAlign w:val="center"/>
    			</w:tcPr>
    			<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
    				<w:pPr>
    					<w:jc w:val="center"/>
    					<w:rPr>
    						<w:b/>
    						<w:bCs/>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    				</w:pPr>
    				<w:r>
    					<w:rPr>
    						<w:b/>
    						<w:bCs/>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    					<w:t>Наименование документа</w:t>
    				</w:r>
    			</w:p>
    		</w:tc>
    		<w:tc>
    			<w:tcPr>
    				<w:tcW w:w="4539" w:type="dxa"/>
    				<w:gridSpan w:val="9"/>
    				<w:tcBorders>
    					<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
    					<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
    					<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
    					<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
    				</w:tcBorders>
    				<w:vAlign w:val="center"/>
    			</w:tcPr>
    			<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
    				<w:pPr>
    					<w:jc w:val="center"/>
    					<w:rPr>
    						<w:b/>
    						<w:bCs/>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    				</w:pPr>
    				<w:r>
    					<w:rPr>
    						<w:b/>
    						<w:bCs/>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    					<w:t>Реквизиты документа</w:t>
    				</w:r>
    			</w:p>
    		</w:tc>
    	</w:tr>
    	<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
    		<w:tblPrEx>
    			<w:tblCellMar>
    				<w:top w:w="0" w:type="dxa"/>
    				<w:bottom w:w="0" w:type="dxa"/>
    			</w:tblCellMar>
    		</w:tblPrEx>
    		<w:trPr>
    			<w:trHeight w:val="369"/>
    		</w:trPr>
    		<w:tc>
    			<w:tcPr>
    				<w:tcW w:w="680" w:type="dxa"/>
    				<w:gridSpan w:val="3"/>
    				<w:tcBorders>
    					<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
    					<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
    					<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
    					<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
    				</w:tcBorders>
    				<w:vAlign w:val="center"/>
    			</w:tcPr>
    			<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
    				<w:pPr>
    					<w:jc w:val="center"/>
    					<w:rPr>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    				</w:pPr>
    				<w:r>
    					<w:rPr>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    					<w:t>1</w:t>
    				</w:r>
    			</w:p>
    		</w:tc>
    		<w:tc>
    			<w:tcPr>
    				<w:tcW w:w="4451" w:type="dxa"/>
    				<w:gridSpan w:val="10"/>
    				<w:tcBorders>
    					<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
    					<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
    					<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
    					<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
    				</w:tcBorders>
    				<w:vAlign w:val="center"/>
    			</w:tcPr>
    			<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
    				<w:pPr>
    					<w:jc w:val="center"/>
    					<w:rPr>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    				</w:pPr>
    				<w:r>
    					<w:rPr>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    					<w:t>2</w:t>
    				</w:r>
    			</w:p>
    		</w:tc>
    		<w:tc>
    			<w:tcPr>
    				<w:tcW w:w="4539" w:type="dxa"/>
    				<w:gridSpan w:val="9"/>
    				<w:tcBorders>
    					<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
    					<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
    					<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
    					<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
    				</w:tcBorders>
    				<w:vAlign w:val="center"/>
    			</w:tcPr>
    			<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
    				<w:pPr>
    					<w:jc w:val="center"/>
    					<w:rPr>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    				</w:pPr>
    				<w:r>
    					<w:rPr>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    					<w:t>3</w:t>
    				</w:r>
    			</w:p>
    		</w:tc>
    	</w:tr>
    	
            <xsl:if test="Documents/Document">
              
                    <xsl:for-each select="Documents/Document">
                            <xsl:call-template name="tDocument"/>                            
                    </xsl:for-each>
            </xsl:if>
        <!-- Geodesis-->
    	        	 <w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
								<w:trHeight w:val="360"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="9670" w:type="dxa"/>
									<w:gridSpan w:val="22"/>
									<w:tcBorders>
										<w:top w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>2. Сведения о геодезической основе, использованной при подготовке межевого плана</w:t>
									</w:r>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
								<w:trHeight w:val="275"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="4706" w:type="dxa"/>
									<w:gridSpan w:val="12"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="nil"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:ind w:left="85" w:right="57"/>
										<w:jc w:val="right"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>Система координат</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2410" w:type="dxa"/>
									<w:gridSpan w:val="7"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="nil"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRPr="00D629DE" w:rsidRDefault="00D629DE" w:rsidP="00D629DE">
									<w:pPr>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
									</w:pPr>
									<w:proofErr w:type="spellStart"/>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
									    <w:t> <xsl:apply-templates select="../Coord_Systems"/></w:t>
									</w:r>
									<w:proofErr w:type="spellEnd"/>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2554" w:type="dxa"/>
									<w:gridSpan w:val="3"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="9670" w:type="dxa"/>
									<w:gridSpan w:val="22"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:rPr>
											<w:sz w:val="12"/>
											<w:szCs w:val="12"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
						</w:tr>	
					    <w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
								<w:trHeight w:val="369"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="680" w:type="dxa"/>
									<w:gridSpan w:val="3"/>
									<w:vMerge w:val="restart"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>№</w:t>
									</w:r>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:br/>
										<w:t>п/п</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="3179" w:type="dxa"/>
									<w:gridSpan w:val="6"/>
									<w:vMerge w:val="restart"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>Название пункта и тип знака геодезической сети</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1842" w:type="dxa"/>
									<w:gridSpan w:val="8"/>
									<w:vMerge w:val="restart"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>Класс геодезической</w:t>
									</w:r>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:br/>
										<w:t>сети</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="3969" w:type="dxa"/>
									<w:gridSpan w:val="5"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>Координаты, м</w:t>
									</w:r>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
								<w:trHeight w:val="369"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="680" w:type="dxa"/>
									<w:gridSpan w:val="3"/>
									<w:vMerge/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="3179" w:type="dxa"/>
									<w:gridSpan w:val="6"/>
									<w:vMerge/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1842" w:type="dxa"/>
									<w:gridSpan w:val="8"/>
									<w:vMerge/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1843" w:type="dxa"/>
									<w:gridSpan w:val="3"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
										<w:t>X</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2126" w:type="dxa"/>
									<w:gridSpan w:val="2"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
										<w:t>Y</w:t>
									</w:r>
								</w:p>
							</w:tc>
						</w:tr>
						
					    <w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="680" w:type="dxa"/>
									<w:gridSpan w:val="3"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>1</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="3179" w:type="dxa"/>
									<w:gridSpan w:val="6"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>2</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1842" w:type="dxa"/>
									<w:gridSpan w:val="8"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>3</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1843" w:type="dxa"/>
									<w:gridSpan w:val="3"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>4</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2126" w:type="dxa"/>
									<w:gridSpan w:val="2"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>5</w:t>
									</w:r>
								</w:p>
							</w:tc>
						</w:tr>
						
    	<xsl:if test="Geodesic_Bases/Geodesic_Base">
        
                             
                    <xsl:for-each select="Geodesic_Bases/Geodesic_Base">
                        <w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
                            <w:tblPrEx>
                                <w:tblCellMar>
                                    <w:top w:w="0" w:type="dxa"/>
                                    <w:bottom w:w="0" w:type="dxa"/>
                                </w:tblCellMar>
                            </w:tblPrEx>
                            <w:trPr>
                                <w:cantSplit/>
                                <w:trHeight w:val="369"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="680" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                        <w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                                        <w:bottom w:val="single" w:sz="6" w:space="0" w:color="auto"/>
                                        <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                    </w:tcBorders>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="007E23C6" w:rsidRPr="00D629DE" w:rsidRDefault="00D629DE">
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:sz w:val="21"/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="en-US"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:sz w:val="21"/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="en-US"/>
                                        </w:rPr>
                                        <w:t><xsl:value-of select="position()"/></w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="3179" w:type="dxa"/>
                                    <w:gridSpan w:val="6"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                        <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                        <w:bottom w:val="single" w:sz="6" w:space="0" w:color="auto"/>
                                        <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                    </w:tcBorders>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="007E23C6" w:rsidRPr="00AA4A27" w:rsidRDefault="00AA4A27">
                                    <w:pPr>
                                        <w:ind w:left="57" w:right="57"/>
                                        <w:rPr>
                                            <w:sz w:val="21"/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="en-US"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:proofErr w:type="spellStart"/>
                                    <w:r>
                                        <w:rPr>
                                            <w:sz w:val="21"/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="en-US"/>
                                        </w:rPr>
                                        <w:t><xsl:value-of select="normalize-space(PName)"/></w:t>
                                    </w:r>
                                    <w:proofErr w:type="spellEnd"/>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1842" w:type="dxa"/>
                                    <w:gridSpan w:val="8"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                        <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                        <w:bottom w:val="single" w:sz="6" w:space="0" w:color="auto"/>
                                        <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                    </w:tcBorders>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="007E23C6" w:rsidRPr="00AA4A27" w:rsidRDefault="00AA4A27">
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:sz w:val="21"/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="en-US"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:proofErr w:type="spellStart"/>
                                    <w:r>
                                        <w:rPr>
                                            <w:sz w:val="21"/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="en-US"/>
                                        </w:rPr>
                                        <w:t><xsl:value-of select="normalize-space(PKlass)"/></w:t>
                                    </w:r>
                                    <w:proofErr w:type="spellEnd"/>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1843" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                        <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                    	<w:bottom w:val="single" w:sz="6" w:space="0" w:color="auto"/>
                                        <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                    </w:tcBorders>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="007E23C6" w:rsidRPr="00AA4A27" w:rsidRDefault="00AA4A27">
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:sz w:val="21"/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="en-US"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:proofErr w:type="spellStart"/>
                                    <w:r>
                                        <w:rPr>
                                            <w:sz w:val="21"/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="en-US"/>
                                        </w:rPr>
                                        <w:t><xsl:value-of select="normalize-space(OrdX)"/></w:t>
                                    </w:r>
                                    <w:proofErr w:type="spellEnd"/>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2126" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                        <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                        <w:bottom w:val="single" w:sz="6" w:space="0" w:color="auto"/>
                                        <w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                                    </w:tcBorders>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="007E23C6" w:rsidRPr="00AA4A27" w:rsidRDefault="00AA4A27">
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:sz w:val="21"/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="en-US"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:proofErr w:type="spellStart"/>
                                    <w:r>
                                        <w:rPr>
                                            <w:sz w:val="21"/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="en-US"/>
                                        </w:rPr>
                                        <w:t><xsl:value-of select="normalize-space(OrdY)"/></w:t>
                                    </w:r>
                                    <w:proofErr w:type="spellEnd"/>
                                </w:p>
                            </w:tc>
                        </w:tr>
                    </xsl:for-each>
            </xsl:if>
             <w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
								<w:trHeight w:val="360"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="9670" w:type="dxa"/>
									<w:gridSpan w:val="22"/>
									<w:tcBorders>
										<w:top w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>3. Сведения о средствах измерений</w:t>
									</w:r>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="680" w:type="dxa"/>
									<w:gridSpan w:val="3"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>№</w:t>
									</w:r>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:br/>
										<w:t>п/п</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2523" w:type="dxa"/>
									<w:gridSpan w:val="4"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>Наименование прибора (инструмента, аппаратуры)</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2498" w:type="dxa"/>
									<w:gridSpan w:val="10"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>Сведения об утверждении типа измерений</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="3969" w:type="dxa"/>
									<w:gridSpan w:val="5"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>Реквизиты свидетельства</w:t>
									</w:r>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:br/>
										<w:t>о поверке прибора (инструмента, аппаратуры)</w:t>
									</w:r>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="680" w:type="dxa"/>
									<w:gridSpan w:val="3"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>1</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2523" w:type="dxa"/>
									<w:gridSpan w:val="4"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>2</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2498" w:type="dxa"/>
									<w:gridSpan w:val="10"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>3</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="3969" w:type="dxa"/>
									<w:gridSpan w:val="5"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>4</w:t>
									</w:r>
								</w:p>
							</w:tc>
						</w:tr>
				
            <xsl:if test="Means_Survey/Means_Survey">
          				
                    <xsl:for-each select="Means_Survey/Means_Survey">
                        <w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
                            <w:tblPrEx>
                                <w:tblCellMar>
                                    <w:top w:w="0" w:type="dxa"/>
                                    <w:bottom w:w="0" w:type="dxa"/>
                                </w:tblCellMar>
                            </w:tblPrEx>
                            <w:trPr>
                                <w:cantSplit/>
                                <w:trHeight w:val="369"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="680" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                        <w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                                        <w:bottom w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                                        <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                    </w:tcBorders>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="007E23C6" w:rsidRPr="00AA4A27" w:rsidRDefault="00AA4A27">
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:sz w:val="21"/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="en-US"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:proofErr w:type="spellStart"/>
                                    <w:r>
                                        <w:rPr>
                                            <w:sz w:val="21"/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="en-US"/>
                                        </w:rPr>
                                        <w:lastRenderedPageBreak/>
                                        <w:t>  <xsl:value-of select="position()"/></w:t>
                                    </w:r>
                                    <w:proofErr w:type="spellEnd"/>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2523" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                        <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                        <w:bottom w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                                        <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                    </w:tcBorders>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="007E23C6" w:rsidRPr="00AA4A27" w:rsidRDefault="00AA4A27">
                                    <w:pPr>
                                        <w:ind w:left="57" w:right="57"/>
                                        <w:rPr>
                                            <w:sz w:val="21"/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="en-US"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:proofErr w:type="spellStart"/>
                                    <w:r>
                                        <w:rPr>
                                            <w:sz w:val="21"/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="en-US"/>
                                        </w:rPr>
                                        <xsl:value-of select="normalize-space(Name)"/>
                                    </w:r>
                                    <w:proofErr w:type="spellEnd"/>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2498" w:type="dxa"/>
                                    <w:gridSpan w:val="10"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                        <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                        <w:bottom w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                                        <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                    </w:tcBorders>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="007E23C6" w:rsidRPr="00AA4A27" w:rsidRDefault="00AA4A27">
                                    <w:pPr>
                                        <w:ind w:left="57" w:right="57"/>
                                        <w:rPr>
                                            <w:sz w:val="21"/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="en-US"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:proofErr w:type="spellStart"/>
                                    <w:r>
                                        <w:rPr>
                                            <w:sz w:val="21"/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="en-US"/>
                                        </w:rPr>
                                        <xsl:value-of select="normalize-space(Certificate)"/>
                                    </w:r>
                                    <w:proofErr w:type="spellEnd"/>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="3969" w:type="dxa"/>
                                    <w:gridSpan w:val="5"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                        <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                        <w:bottom w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                                        <w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                                    </w:tcBorders>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="007E23C6" w:rsidRPr="00AA4A27" w:rsidRDefault="00AA4A27">
                                    <w:pPr>
                                        <w:ind w:left="57" w:right="57"/>
                                        <w:rPr>
                                            <w:sz w:val="21"/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="en-US"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:proofErr w:type="spellStart"/>
                                    <w:r>
                                        <w:rPr>
                                            <w:sz w:val="21"/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="en-US"/>
                                        </w:rPr>
                                        <w:t><xsl:if test="Certificate_Verification!=''">
                                            <xsl:value-of select="normalize-space(Certificate_Verification)"/>
                                        </xsl:if></w:t>
                                    </w:r>
                                    <w:proofErr w:type="spellEnd"/>
                                </w:p>
                            </w:tc>
                        </w:tr>
                    </xsl:for-each>
            </xsl:if>
            <w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
								<w:trHeight w:val="640"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="9670" w:type="dxa"/>
									<w:gridSpan w:val="22"/>
									<w:tcBorders>
										<w:top w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>4. Сведения о наличии зданий, сооружений, объектов незавершенного строительства</w:t>
									</w:r>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:br/>
										<w:t>на исходных земельных участках</w:t>
									</w:r>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
								<w:trHeight w:val="397"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="671" w:type="dxa"/>
									<w:gridSpan w:val="2"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>№</w:t>
									</w:r>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:br/>
										<w:t>п/п</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2532" w:type="dxa"/>
									<w:gridSpan w:val="5"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>Кадастровый номер земельного участка</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="6467" w:type="dxa"/>
									<w:gridSpan w:val="15"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t xml:space="preserve">Кадастровые или иные номера зданий, </w:t>
									</w:r>
									<w:proofErr w:type="gramStart"/>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>сооружений,</w:t>
									</w:r>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:br/>
										<w:t>объектов</w:t>
									</w:r>
									<w:proofErr w:type="gramEnd"/>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t xml:space="preserve"> незавершенного строительства, расположенных</w:t>
									</w:r>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:br/>
										<w:t>на земельном участке</w:t>
									</w:r>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="671" w:type="dxa"/>
									<w:gridSpan w:val="2"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>1</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2532" w:type="dxa"/>
									<w:gridSpan w:val="5"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>2</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="6467" w:type="dxa"/>
									<w:gridSpan w:val="15"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>3</w:t>
									</w:r>
								</w:p>
							</w:tc>
						</w:tr>
    	<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
    		<w:tblPrEx>
    			<w:tblCellMar>
    				<w:top w:w="0" w:type="dxa"/>
    				<w:bottom w:w="0" w:type="dxa"/>
    			</w:tblCellMar>
    		</w:tblPrEx>
    		<w:trPr>
    			<w:cantSplit/>
    			<w:trHeight w:val="360"/>
    		</w:trPr>
    		<w:tc>
    			<w:tcPr>
    				<w:tcW w:w="9670" w:type="dxa"/>
    				<w:gridSpan w:val="22"/>
    				<w:tcBorders>
    					<w:top w:val="double" w:sz="6" w:space="0" w:color="auto"/>
    					<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
    					<w:bottom w:val="nil"/>
    					<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
    				</w:tcBorders>
    				<w:vAlign w:val="center"/>
    			</w:tcPr>
    			<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
    				<w:pPr>
    					<w:keepNext/>
    					<w:jc w:val="center"/>
    					<w:rPr>
    						<w:b/>
    						<w:bCs/>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    				</w:pPr>
    				<w:r>
    					<w:rPr>
    						<w:b/>
    						<w:bCs/>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    					<w:t>5. Сведения о частях исходных или уточняемых земельных участков</w:t>
    				</w:r>
    			</w:p>
    		</w:tc>
    	</w:tr>
    	<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
    		<w:tblPrEx>
    			<w:tblCellMar>
    				<w:top w:w="0" w:type="dxa"/>
    				<w:bottom w:w="0" w:type="dxa"/>
    			</w:tblCellMar>
    		</w:tblPrEx>
    		<w:trPr>
    			<w:cantSplit/>
    			<w:trHeight w:val="397"/>
    		</w:trPr>
    		<w:tc>
    			<w:tcPr>
    				<w:tcW w:w="671" w:type="dxa"/>
    				<w:gridSpan w:val="2"/>
    				<w:tcBorders>
    					<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
    					<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
    					<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
    					<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
    				</w:tcBorders>
    				<w:vAlign w:val="center"/>
    			</w:tcPr>
    			<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
    				<w:pPr>
    					<w:jc w:val="center"/>
    					<w:rPr>
    						<w:b/>
    						<w:bCs/>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    				</w:pPr>
    				<w:r>
    					<w:rPr>
    						<w:b/>
    						<w:bCs/>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    					<w:t>№</w:t>
    				</w:r>
    				<w:r>
    					<w:rPr>
    						<w:b/>
    						<w:bCs/>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    					<w:br/>
    					<w:t>п/п</w:t>
    				</w:r>
    			</w:p>
    		</w:tc>
    		<w:tc>
    			<w:tcPr>
    				<w:tcW w:w="2532" w:type="dxa"/>
    				<w:gridSpan w:val="5"/>
    				<w:tcBorders>
    					<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
    					<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
    					<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
    					<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
    				</w:tcBorders>
    				<w:vAlign w:val="center"/>
    			</w:tcPr>
    			<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
    				<w:pPr>
    					<w:jc w:val="center"/>
    					<w:rPr>
    						<w:b/>
    						<w:bCs/>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    				</w:pPr>
    				<w:r>
    					<w:rPr>
    						<w:b/>
    						<w:bCs/>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    					<w:t>Кадастровый номер земельного участка</w:t>
    				</w:r>
    			</w:p>
    		</w:tc>
    		<w:tc>
    			<w:tcPr>
    				<w:tcW w:w="6467" w:type="dxa"/>
    				<w:gridSpan w:val="15"/>
    				<w:tcBorders>
    					<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
    					<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
    					<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
    					<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
    				</w:tcBorders>
    				<w:vAlign w:val="center"/>
    			</w:tcPr>
    			<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
    				<w:pPr>
    					<w:jc w:val="center"/>
    					<w:rPr>
    						<w:b/>
    						<w:bCs/>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    				</w:pPr>
    				<w:r>
    					<w:rPr>
    						<w:b/>
    						<w:bCs/>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    					<w:t>Учетные номера частей земельного участка</w:t>
    				</w:r>
    			</w:p>
    		</w:tc>
    	</w:tr>
    	<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
    		<w:tblPrEx>
    			<w:tblCellMar>
    				<w:top w:w="0" w:type="dxa"/>
    				<w:bottom w:w="0" w:type="dxa"/>
    			</w:tblCellMar>
    		</w:tblPrEx>
    		<w:trPr>
    			<w:cantSplit/>
    		</w:trPr>
    		<w:tc>
    			<w:tcPr>
    				<w:tcW w:w="671" w:type="dxa"/>
    				<w:gridSpan w:val="2"/>
    				<w:tcBorders>
    					<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
    					<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
    					<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
    					<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
    				</w:tcBorders>
    				<w:vAlign w:val="center"/>
    			</w:tcPr>
    			<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
    				<w:pPr>
    					<w:jc w:val="center"/>
    					<w:rPr>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    				</w:pPr>
    				<w:r>
    					<w:rPr>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    					<w:t>1</w:t>
    				</w:r>
    			</w:p>
    		</w:tc>
    		<w:tc>
    			<w:tcPr>
    				<w:tcW w:w="2532" w:type="dxa"/>
    				<w:gridSpan w:val="5"/>
    				<w:tcBorders>
    					<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
    					<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
    					<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
    					<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
    				</w:tcBorders>
    				<w:vAlign w:val="center"/>
    			</w:tcPr>
    			<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
    				<w:pPr>
    					<w:jc w:val="center"/>
    					<w:rPr>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    				</w:pPr>
    				<w:r>
    					<w:rPr>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    					<w:t>2</w:t>
    				</w:r>
    			</w:p>
    		</w:tc>
    		<w:tc>
    			<w:tcPr>
    				<w:tcW w:w="6467" w:type="dxa"/>
    				<w:gridSpan w:val="15"/>
    				<w:tcBorders>
    					<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
    					<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
    					<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
    					<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
    				</w:tcBorders>
    				<w:vAlign w:val="center"/>
    			</w:tcPr>
    			<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
    				<w:pPr>
    					<w:jc w:val="center"/>
    					<w:rPr>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    				</w:pPr>
    				<w:r>
    					<w:rPr>
    						<w:sz w:val="21"/>
    						<w:szCs w:val="21"/>
    					</w:rPr>
    					<w:t>3</w:t>
    				</w:r>
    			</w:p>
    		</w:tc>
    	</w:tr>
    	
    	
    	<xsl:if test="Realty/OKS">
            <xsl:for-each select="Realty/OKS">
						     <w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
						         <w:tblPrEx>
						             <w:tblCellMar>
						                 <w:top w:w="0" w:type="dxa"/>
						                 <w:bottom w:w="0" w:type="dxa"/>
						             </w:tblCellMar>
						         </w:tblPrEx>
						         <w:trPr>
						             <w:cantSplit/>
						             <w:trHeight w:val="369"/>
						         </w:trPr>
						         <w:tc>
						             <w:tcPr>
						                 <w:tcW w:w="671" w:type="dxa"/>
						                 <w:gridSpan w:val="2"/>
						                 <w:tcBorders>
						                     <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
						                     <w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
						                     <w:bottom w:val="double" w:sz="6" w:space="0" w:color="auto"/>
						                     <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
						                 </w:tcBorders>
						                 <w:vAlign w:val="center"/>
						             </w:tcPr>
						             <w:p w:rsidR="007E23C6" w:rsidRPr="00AA4A27" w:rsidRDefault="00AA4A27">
						                 <w:pPr>
						                     <w:jc w:val="center"/>
						                     <w:rPr>
						                         <w:sz w:val="21"/>
						                         <w:szCs w:val="21"/>
						                         <w:lang w:val="en-US"/>
						                     </w:rPr>
						                 </w:pPr>
						                 <w:proofErr w:type="spellStart"/>
						                 <w:r>
						                     <w:rPr>
						                         <w:sz w:val="21"/>
						                         <w:szCs w:val="21"/>
						                         <w:lang w:val="en-US"/>
						                     </w:rPr>
						                     <w:t><xsl:value-of select="position()"/></w:t>
						                 </w:r>
						                 <w:proofErr w:type="spellEnd"/>
						             </w:p>
						         </w:tc>
						         <w:tc>
						             <w:tcPr>
						                 <w:tcW w:w="2532" w:type="dxa"/>
						                 <w:gridSpan w:val="5"/>
						                 <w:tcBorders>
						                     <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
						                     <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
						                     <w:bottom w:val="double" w:sz="6" w:space="0" w:color="auto"/>
						                     <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
						                 </w:tcBorders>
						                 <w:vAlign w:val="center"/>
						             </w:tcPr>
						             <w:p w:rsidR="007E23C6" w:rsidRDefault="00AA4A27">
						                 <w:pPr>
						                     <w:ind w:left="57" w:right="57"/>
						                     <w:jc w:val="center"/>
						                     <w:rPr>
						                         <w:sz w:val="21"/>
						                         <w:szCs w:val="21"/>
						                         <w:lang w:val="en-US"/>
						                     </w:rPr>
						                 </w:pPr>
						                 <w:proofErr w:type="spellStart"/>
						                 <w:r>
						                     <w:rPr>
						                         <w:sz w:val="21"/>
						                         <w:szCs w:val="21"/>
						                         <w:lang w:val="en-US"/>
						                     </w:rPr>
						                     <w:t>  <xsl:if test="CadastralNumber!=''">
						                         <xsl:value-of select="normalize-space(CadastralNumber)"/>
						                     </xsl:if></w:t>
						                 </w:r>
						                 <w:proofErr w:type="spellEnd"/>
						             </w:p>
						         </w:tc>
						         <w:tc>
						             <w:tcPr>
						                 <w:tcW w:w="6467" w:type="dxa"/>
						                 <w:gridSpan w:val="15"/>
						                 <w:tcBorders>
						                     <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
						                     <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
						                     <w:bottom w:val="double" w:sz="6" w:space="0" w:color="auto"/>
						                     <w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
						                 </w:tcBorders>
						                 <w:vAlign w:val="center"/>
						             </w:tcPr>
						             <w:p w:rsidR="007E23C6" w:rsidRDefault="00AA4A27">
						                 <w:pPr>
						                     <w:ind w:left="57" w:right="57"/>
						                     <w:rPr>
						                         <w:sz w:val="21"/>
						                         <w:szCs w:val="21"/>
						                         <w:lang w:val="en-US"/>
						                     </w:rPr>
						                 </w:pPr>
						                 <w:proofErr w:type="spellStart"/>
						                 <w:r>
						                     <w:rPr>
						                         <w:sz w:val="21"/>
						                         <w:szCs w:val="21"/>
						                         <w:lang w:val="en-US"/>
						                     </w:rPr>
						                     <w:t><xsl:if test="CadastralNumber_OtherNumber!=''">
						                         <xsl:value-of select="normalize-space(CadastralNumber_OtherNumber)"/>
						                     </xsl:if></w:t>
						                 </w:r>
						                 <w:proofErr w:type="spellEnd"/>
						             </w:p>
						         </w:tc>
						     </w:tr>
						 </xsl:for-each>
            </xsl:if>
            <xsl:if test="SubParcels/SubParcel">
                <xsl:for-each select="SubParcels/SubParcel">
                        <w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
                            <w:tblPrEx>
                                <w:tblCellMar>
                                    <w:top w:w="0" w:type="dxa"/>
                                    <w:bottom w:w="0" w:type="dxa"/>
                                </w:tblCellMar>
                            </w:tblPrEx>
                            <w:trPr>
                                <w:cantSplit/>
                                <w:trHeight w:val="369"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="671" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                        <w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                                        <w:bottom w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                                        <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                    </w:tcBorders>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="007E23C6" w:rsidRPr="00AA4A27" w:rsidRDefault="00AA4A27">
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:sz w:val="21"/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="en-US"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:proofErr w:type="spellStart"/>
                                    <w:r>
                                        <w:rPr>
                                            <w:sz w:val="21"/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="en-US"/>
                                        </w:rPr>
                                        <w:t><xsl:value-of select="position()"/></w:t>
                                    </w:r>
                                    <w:proofErr w:type="spellEnd"/>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2532" w:type="dxa"/>
                                    <w:gridSpan w:val="5"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                        <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                        <w:bottom w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                                        <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                    </w:tcBorders>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="007E23C6" w:rsidRDefault="00AA4A27">
                                    <w:pPr>
                                        <w:ind w:left="57" w:right="57"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:sz w:val="21"/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="en-US"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:proofErr w:type="spellStart"/>
                                    <w:r>
                                        <w:rPr>
                                            <w:sz w:val="21"/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="en-US"/>
                                        </w:rPr>
                                        <w:t><xsl:value-of select="CadastralNumber"/></w:t>
                                    </w:r>
                                    <w:proofErr w:type="spellEnd"/>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="6467" w:type="dxa"/>
                                    <w:gridSpan w:val="15"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                        <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                        <w:bottom w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                                        <w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                                    </w:tcBorders>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w:rsidR="007E23C6" w:rsidRDefault="00AA4A27">
                                    <w:pPr>
                                        <w:ind w:left="57" w:right="57"/>
                                        <w:rPr>
                                            <w:sz w:val="21"/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="en-US"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:proofErr w:type="spellStart"/>
                                    <w:r>
                                        <w:rPr>
                                            <w:sz w:val="21"/>
                                            <w:szCs w:val="21"/>
                                            <w:lang w:val="en-US"/>
                                        </w:rPr>
                                        <w:t><xsl:value-of select="Number_Record"/></w:t>
                                    </w:r>
                                    <w:proofErr w:type="spellEnd"/>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        
                    </xsl:for-each>
            </xsl:if>
        
     
    </xsl:template>
   
    <!--Пакет информации-->
    <xsl:template match="Package">
            <xsl:apply-templates select="FormParcels" mode="Package"/>        
            <xsl:apply-templates select="SpecifyParcel" mode="Package"/>
            <xsl:apply-templates select="NewSubParcel" mode="Package"/>
    </xsl:template>
    
    
    <!--SpecifyParcels уточнение границ-->
    <xsl:template match="SpecifyParcel" mode="Package">
           <xsl:apply-templates select="ExistParcel" mode="SpecifyParcels"/>
            <xsl:apply-templates select="ExistEZ" mode="SpecifyParcels"/>
            <xsl:apply-templates select="SpecifyRelatedParcel" mode="tSpecifyRelatedParcel"/>
    </xsl:template>
    
    <!--Сведения об уточняемых ЗУ (не ЕЗ)-->
    <xsl:template match="ExistParcel" mode="SpecifyParcels">
  
         <xsl:call-template name="tExistParcel"/>
      
    </xsl:template>
    
    <xsl:template match="FormParcels" mode="Package">
            <xsl:apply-templates select="NewParcel" mode="FormParcels"/>
            <xsl:apply-templates select="ChangeParcel" mode="FormParcels"/>
            <xsl:apply-templates select="SpecifyRelatedParcel" mode="tSpecifyRelatedParcel"/>
    </xsl:template>
    
    
    <xsl:template match="ChangeParcel" mode="FormParcels">
          <!--  <xsl:call-template name="tChangeParcel"/>-->
    </xsl:template>
    
   
    
    <!--заключение-->
    <xsl:template match="Conclusion">
        <w:tbl>
            <w:tblPr>
            	<w:tblW w:w="0" w:type="auto"/>
            	<w:tblInd w:w="-544" w:type="dxa"/>
                <w:tblLayout w:type="fixed"/>
                <w:tblCellMar>
                    <w:left w:w="120" w:type="dxa"/>
                    <w:right w:w="120" w:type="dxa"/>
                </w:tblCellMar>
                <w:tblLook w:val="0000" w:firstRow="0" w:lastRow="0" w:firstColumn="0" w:lastColumn="0" w:noHBand="0" w:noVBand="0"/>
            </w:tblPr>
            <w:tblGrid>
            	<w:gridCol w:w="9670"/>
            </w:tblGrid>
            <w:tr w:rsidR="003107B9" w:rsidRPr="00D246F2" w:rsidTr="00A56FB2">
                <w:trPr>
                    <w:cantSplit/>
                    <w:trHeight w:val="352"/>
                </w:trPr>
                <w:tc>
                    <w:tcPr>
                    	<w:tcW w:w="9670" w:type="dxa"/>
                        <w:tcBorders>
                            <w:top w:val="double" w:sz="4" w:space="0" w:color="auto"/>
                            <w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                            <w:bottom w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                            <w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                        </w:tcBorders>
                        <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p w:rsidR="003107B9" w:rsidRPr="0047124C" w:rsidRDefault="003107B9" w:rsidP="00A56FB2">
                        <w:pPr>
                            <w:pStyle w:val="1"/>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:b/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:vertAlign w:val="superscript"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="0047124C">
                            <w:rPr>
                                <w:b/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                            <w:t xml:space="preserve">Заключение кадастрового инженера </w:t>
                        </w:r>
                    </w:p>
                </w:tc>
            </w:tr>
            <w:tr w:rsidR="003107B9" w:rsidRPr="00D246F2" w:rsidTr="00A56FB2">
                <w:trPr>
                    <w:cantSplit/>
                    <w:trHeight w:val="14931"/>
                </w:trPr>
                <w:tc>
                    <w:tcPr>
                    	<w:tcW w:w="9670" w:type="dxa"/>
                        <w:tcBorders>
                            <w:top w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                            <w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                            <w:bottom w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                            <w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                        </w:tcBorders>
                        <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                    </w:tcPr>
                    <w:p w:rsidR="003107B9" w:rsidRPr="00EF61DB" w:rsidRDefault="003107B9" w:rsidP="003107B9">
                        <w:pPr>
                            <w:autoSpaceDE w:val="0"/>
                            <w:autoSpaceDN w:val="0"/>
                            <w:adjustRightInd w:val="0"/>
                            <w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
                            <w:ind w:firstLine="709"/>
                            <w:jc w:val="both"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:eastAsia="Calibri" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="24"/>
                                <w:szCs w:val="24"/>
                                <w:lang w:eastAsia="en-US"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="00BA1999">
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:color w:val="000000"/>
                                <w:sz w:val="24"/>
                                <w:szCs w:val="24"/>
                            </w:rPr>
                            <w:t xml:space="preserve">  <xsl:value-of select="normalize-space(self::node())"/>  </w:t>
                        </w:r>
                        <w:bookmarkStart w:id="0" w:name="_GoBack"/>
                        <w:bookmarkEnd w:id="0"/>
                    </w:p>
                </w:tc>
            </w:tr>
        </w:tbl>
        <w:p w:rsidR="008E6544" w:rsidRDefault="008E6544"/>
        <w:sectPr w:rsidR="008E6544" w:rsidSect="003107B9">
            <w:pgSz w:w="11906" w:h="16838" w:code="9"/>
            <w:pgMar w:top="720" w:right="851" w:bottom="0" w:left="1701" w:header="709" w:footer="709" w:gutter="0"/>
            <w:cols w:space="708"/>
            <w:docGrid w:linePitch="360"/>
        </w:sectPr>
    </xsl:template>
    
    <xsl:template name="tDocument">
        <w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
            <w:tblPrEx>
                <w:tblCellMar>
                    <w:top w:w="0" w:type="dxa"/>
                    <w:bottom w:w="0" w:type="dxa"/>
                </w:tblCellMar>
            </w:tblPrEx>
            <w:trPr>
                <w:trHeight w:val="369"/>
            </w:trPr>
            <w:tc>
                <w:tcPr>
                    <w:tcW w:w="680" w:type="dxa"/>
                    <w:gridSpan w:val="3"/>
                    <w:tcBorders>
                        <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                        <w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                        <w:bottom w:val="nil"/>
                        <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                    </w:tcBorders>
                    <w:vAlign w:val="center"/>
                </w:tcPr>
                <w:p w:rsidR="007E23C6" w:rsidRDefault="00D629DE">
                    <w:pPr>
                        <w:jc w:val="center"/>
                        <w:rPr>
                            <w:sz w:val="21"/>
                            <w:szCs w:val="21"/>
                        </w:rPr>
                    </w:pPr>
                    <w:r>
                        <w:rPr>
                            <w:sz w:val="21"/>
                            <w:szCs w:val="21"/>
                        </w:rPr>
                        <w:t><xsl:value-of select="position()"/></w:t>
                    </w:r>
                </w:p>
            </w:tc>
            <w:tc>
                <w:tcPr>
                    <w:tcW w:w="4451" w:type="dxa"/>
                    <w:gridSpan w:val="10"/>
                    <w:tcBorders>
                        <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                        <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                        <w:bottom w:val="nil"/>
                        <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                    </w:tcBorders>
                    <w:vAlign w:val="center"/>
                </w:tcPr>
                <w:p w:rsidR="007E23C6" w:rsidRDefault="00D629DE">
                    <w:pPr>
                        <w:ind w:left="57" w:right="57"/>
                        <w:rPr>
                            <w:sz w:val="21"/>
                            <w:szCs w:val="21"/>
                        </w:rPr>
                    </w:pPr>
                    <w:r>
                        <w:rPr>
                            <w:sz w:val="21"/>
                            <w:szCs w:val="21"/>
                        </w:rPr>
                        <w:t><xsl:value-of select="Name"/></w:t>
                    </w:r>
                </w:p>
            </w:tc>
            <w:tc>
                <w:tcPr>
                    <w:tcW w:w="4539" w:type="dxa"/>
                    <w:gridSpan w:val="9"/>
                    <w:tcBorders>
                        <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                        <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                        <w:bottom w:val="nil"/>
                        <w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                    </w:tcBorders>
                    <w:vAlign w:val="center"/>
                </w:tcPr>
                <w:p w:rsidR="007E23C6" w:rsidRDefault="00D629DE">
                    <w:pPr>
                        <w:ind w:left="57" w:right="57"/>
                        <w:rPr>
                            <w:sz w:val="21"/>
                            <w:szCs w:val="21"/>
                        </w:rPr>
                    </w:pPr>
                    <w:r>
                        <w:rPr>
                            <w:sz w:val="21"/>
                            <w:szCs w:val="21"/>
                        </w:rPr>
                        <w:t><xsl:value-of select="Date"/><xsl:text>  </xsl:text>
                        	<xsl:value-of select="Number"/><xsl:text>  </xsl:text>
                        	<xsl:value-of select="Series"/></w:t>  
                    </w:r>
                </w:p>
            </w:tc>
        </w:tr>
    </xsl:template>
    
    
    
    


    <xsl:template name="tExistParcel">
         <w:tbl>
						<w:tblPr>
							<w:tblW w:w="9670" w:type="dxa"/>
							<w:tblInd w:w="-544" w:type="dxa"/>
							<w:tblLayout w:type="fixed"/>
							<w:tblCellMar>
								<w:left w:w="28" w:type="dxa"/>
								<w:right w:w="28" w:type="dxa"/>
							</w:tblCellMar>
							<w:tblLook w:val="0000" w:firstRow="0" w:lastRow="0" w:firstColumn="0" w:lastColumn="0" w:noHBand="0" w:noVBand="0"/>
						</w:tblPr>
						<w:tblGrid>
							<w:gridCol w:w="737"/>
							<w:gridCol w:w="269"/>
							<w:gridCol w:w="15"/>
							<w:gridCol w:w="1275"/>
							<w:gridCol w:w="142"/>
							<w:gridCol w:w="425"/>
							<w:gridCol w:w="1063"/>
							<w:gridCol w:w="355"/>
							<w:gridCol w:w="425"/>
							<w:gridCol w:w="425"/>
							<w:gridCol w:w="284"/>
							<w:gridCol w:w="567"/>
							<w:gridCol w:w="142"/>
							<w:gridCol w:w="141"/>
							<w:gridCol w:w="567"/>
							<w:gridCol w:w="426"/>
							<w:gridCol w:w="567"/>
							<w:gridCol w:w="141"/>
							<w:gridCol w:w="426"/>
							<w:gridCol w:w="1278"/>
						</w:tblGrid>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:trHeight w:val="454"/>
								<w:tblHeader/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="9670" w:type="dxa"/>
									<w:gridSpan w:val="26"/>
									<w:tcBorders>
										<w:top w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="27"/>
											<w:szCs w:val="27"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="27"/>
											<w:szCs w:val="27"/>
										</w:rPr>
										<w:t>Сведения об уточняемых земельных участках и их частях</w:t>
									</w:r>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
								<w:trHeight w:val="292"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="9670" w:type="dxa"/>
									<w:gridSpan w:val="26"/>
									<w:tcBorders>
										<w:top w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>1. Сведения о характерных точках границы уточняемого земельного участка</w:t>
									</w:r>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
								<w:trHeight w:val="275"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="4706" w:type="dxa"/>
									<w:gridSpan w:val="13"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="nil"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:ind w:left="85" w:right="57"/>
										<w:jc w:val="right"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:proofErr w:type="gramStart"/>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>с</w:t>
									</w:r>
									<w:proofErr w:type="gramEnd"/>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t xml:space="preserve"> кадастровым номером</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2552" w:type="dxa"/>
									<w:gridSpan w:val="7"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="nil"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRPr="0002329B" w:rsidRDefault="0002329B">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
									</w:pPr>
									<w:proofErr w:type="spellStart"/>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
									    <w:t> <xsl:value-of select="@CadastralNumber"/></w:t>
									</w:r>
									<w:proofErr w:type="spellEnd"/>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2412" w:type="dxa"/>
									<w:gridSpan w:val="6"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="9670" w:type="dxa"/>
									<w:gridSpan w:val="26"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:rPr>
											<w:sz w:val="9"/>
											<w:szCs w:val="9"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblBorders>
									<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									<w:bottom w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									<w:insideH w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									<w:insideV w:val="single" w:sz="4" w:space="0" w:color="auto"/>
								</w:tblBorders>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
								<w:trHeight w:val="369"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1021" w:type="dxa"/>
									<w:gridSpan w:val="3"/>
									<w:vMerge w:val="restart"/>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>Обозначение характерных</w:t>
									</w:r>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:br/>
										<w:t>точек границ</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2905" w:type="dxa"/>
									<w:gridSpan w:val="6"/>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>Существующие координаты, м</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2906" w:type="dxa"/>
									<w:gridSpan w:val="7"/>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>Уточненные координаты, м</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1134" w:type="dxa"/>
									<w:gridSpan w:val="7"/>
									<w:vMerge w:val="restart"/>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t xml:space="preserve">Средняя </w:t>
									</w:r>
									<w:proofErr w:type="spellStart"/>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>квадратическая</w:t>
									</w:r>
									<w:proofErr w:type="spellEnd"/>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t xml:space="preserve"> погрешность положения характерной точки (</w:t>
									</w:r>
									<w:proofErr w:type="spellStart"/>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>М</w:t>
									</w:r>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
											<w:vertAlign w:val="subscript"/>
										</w:rPr>
										<w:t>t</w:t>
									</w:r>
									<w:proofErr w:type="spellEnd"/>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>), м</w:t>
									</w:r>
								</w:p>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1704" w:type="dxa"/>
									<w:gridSpan w:val="3"/>
									<w:vMerge w:val="restart"/>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>Описание закрепления точки</w:t>
									</w:r>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblBorders>
									<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									<w:bottom w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									<w:insideH w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									<w:insideV w:val="single" w:sz="4" w:space="0" w:color="auto"/>
								</w:tblBorders>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
								<w:trHeight w:val="369"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1021" w:type="dxa"/>
									<w:gridSpan w:val="3"/>
									<w:vMerge/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1417" w:type="dxa"/>
									<w:gridSpan w:val="3"/>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>Х</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1488" w:type="dxa"/>
									<w:gridSpan w:val="3"/>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
										<w:t>Y</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1489" w:type="dxa"/>
									<w:gridSpan w:val="5"/>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>Х</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1417" w:type="dxa"/>
									<w:gridSpan w:val="2"/>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
										<w:t>Y</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1134" w:type="dxa"/>
									<w:gridSpan w:val="7"/>
									<w:vMerge/>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1704" w:type="dxa"/>
									<w:gridSpan w:val="3"/>
									<w:vMerge/>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblBorders>
									<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									<w:bottom w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									<w:insideH w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									<w:insideV w:val="single" w:sz="4" w:space="0" w:color="auto"/>
								</w:tblBorders>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
								<w:trHeight w:val="340"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1021" w:type="dxa"/>
									<w:gridSpan w:val="3"/>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>1</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1417" w:type="dxa"/>
									<w:gridSpan w:val="3"/>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>2</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1488" w:type="dxa"/>
									<w:gridSpan w:val="3"/>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>3</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1489" w:type="dxa"/>
									<w:gridSpan w:val="5"/>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>4</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1417" w:type="dxa"/>
									<w:gridSpan w:val="2"/>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>5</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1134" w:type="dxa"/>
									<w:gridSpan w:val="7"/>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>6</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1704" w:type="dxa"/>
									<w:gridSpan w:val="3"/>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>7</w:t>
									</w:r>
								</w:p>
							</w:tc>
						</w:tr>
             <xsl:apply-templates select="Entity_Spatial" mode="ExistParcel"/>
             <xsl:apply-templates select="Contours"/>	
             
              <w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
								<w:trHeight w:val="292"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="9670" w:type="dxa"/>
									<w:gridSpan w:val="26"/>
									<w:tcBorders>
										<w:top w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>2. Сведения о частях границ уточняемого земельного участка</w:t>
									</w:r>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
								<w:trHeight w:val="275"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="4706" w:type="dxa"/>
									<w:gridSpan w:val="13"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="nil"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:ind w:left="85" w:right="57"/>
										<w:jc w:val="right"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:proofErr w:type="gramStart"/>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>с</w:t>
									</w:r>
									<w:proofErr w:type="gramEnd"/>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t xml:space="preserve"> кадастровым номером</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2552" w:type="dxa"/>
									<w:gridSpan w:val="7"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="nil"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRPr="0002329B" w:rsidRDefault="0002329B">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
									</w:pPr>
									<w:proofErr w:type="spellStart"/>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
										<w:t><xsl:value-of select="@CadastralNumber"/></w:t>
									</w:r>
									<w:proofErr w:type="spellEnd"/>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2412" w:type="dxa"/>
									<w:gridSpan w:val="6"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="9670" w:type="dxa"/>
									<w:gridSpan w:val="26"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:rPr>
											<w:sz w:val="9"/>
											<w:szCs w:val="9"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
								<w:trHeight w:val="369"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2296" w:type="dxa"/>
									<w:gridSpan w:val="4"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>Обозначение части границ</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="3828" w:type="dxa"/>
									<w:gridSpan w:val="14"/>
									<w:vMerge w:val="restart"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t xml:space="preserve">Горизонтальное </w:t>
									</w:r>
									<w:proofErr w:type="spellStart"/>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>проложение</w:t>
									</w:r>
									<w:proofErr w:type="spellEnd"/>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t xml:space="preserve"> (S), м</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="3546" w:type="dxa"/>
									<w:gridSpan w:val="8"/>
									<w:vMerge w:val="restart"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>Описание прохождения части границ</w:t>
									</w:r>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
								<w:trHeight w:val="369"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1006" w:type="dxa"/>
									<w:gridSpan w:val="2"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:proofErr w:type="gramStart"/>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>от</w:t>
									</w:r>
									<w:proofErr w:type="gramEnd"/>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t xml:space="preserve"> т.</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1290" w:type="dxa"/>
									<w:gridSpan w:val="2"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:proofErr w:type="gramStart"/>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>до</w:t>
									</w:r>
									<w:proofErr w:type="gramEnd"/>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t xml:space="preserve"> т.</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="3828" w:type="dxa"/>
									<w:gridSpan w:val="14"/>
									<w:vMerge/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="3546" w:type="dxa"/>
									<w:gridSpan w:val="8"/>
									<w:vMerge/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
								<w:trHeight w:val="340"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1006" w:type="dxa"/>
									<w:gridSpan w:val="2"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>1</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1290" w:type="dxa"/>
									<w:gridSpan w:val="2"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>2</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="3828" w:type="dxa"/>
									<w:gridSpan w:val="14"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>3</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="3546" w:type="dxa"/>
									<w:gridSpan w:val="8"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>4</w:t>
									</w:r>
								</w:p>
							</w:tc>
						</w:tr>
						
             <xsl:apply-templates select="Entity_Spatial" mode="Borders"/>
             <xsl:apply-templates select="Contours" mode="Borders"/>	
             
             <xsl:apply-templates select="SubParcels" mode="tExistParcel"/>
                <w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
								<w:trHeight w:val="292"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="9670" w:type="dxa"/>
									<w:gridSpan w:val="26"/>
									<w:tcBorders>
										<w:top w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>4. Общие сведения об уточняемом земельном участке</w:t>
									</w:r>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
								<w:trHeight w:val="275"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="4281" w:type="dxa"/>
									<w:gridSpan w:val="11"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="nil"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:ind w:left="85" w:right="57"/>
										<w:jc w:val="right"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:proofErr w:type="gramStart"/>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>с</w:t>
									</w:r>
									<w:proofErr w:type="gramEnd"/>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t xml:space="preserve"> кадастровым номером</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="3544" w:type="dxa"/>
									<w:gridSpan w:val="11"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="nil"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRPr="0002329B" w:rsidRDefault="0002329B">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
									</w:pPr>
									<w:proofErr w:type="spellStart"/>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
										<w:t><xsl:value-of select="@CadastralNumber"/></w:t>
									</w:r>
									<w:proofErr w:type="spellEnd"/>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1845" w:type="dxa"/>
									<w:gridSpan w:val="4"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="9670" w:type="dxa"/>
									<w:gridSpan w:val="26"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:rPr>
											<w:sz w:val="9"/>
											<w:szCs w:val="9"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:trHeight w:val="320"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="737" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:keepNext/>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>№ п/п</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="5245" w:type="dxa"/>
									<w:gridSpan w:val="16"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:keepNext/>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>Наименование характеристик земельного участка</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="3688" w:type="dxa"/>
									<w:gridSpan w:val="9"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>Значение характеристики</w:t>
									</w:r>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:trHeight w:val="320"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="737" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:keepNext/>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>1</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="5245" w:type="dxa"/>
									<w:gridSpan w:val="16"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:keepNext/>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>2</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="3688" w:type="dxa"/>
									<w:gridSpan w:val="9"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>3</w:t>
									</w:r>
								</w:p>
							</w:tc>
						</w:tr>
			
             <xsl:call-template name="tLocationExist"/> 			
          
             <xsl:apply-templates select="SubParcels" mode="tExistParcelGeneral"/>
              <w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
								<w:trHeight w:val="292"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="9670" w:type="dxa"/>
									<w:gridSpan w:val="26"/>
									<w:tcBorders>
										<w:top w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:keepNext/>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>6. Сведения о земельных участках, смежных с уточняемым земельным участком</w:t>
									</w:r>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
								<w:trHeight w:val="275"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="5131" w:type="dxa"/>
									<w:gridSpan w:val="15"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="nil"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:keepNext/>
										<w:ind w:left="85" w:right="57"/>
										<w:jc w:val="right"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:proofErr w:type="gramStart"/>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>с</w:t>
									</w:r>
									<w:proofErr w:type="gramEnd"/>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t xml:space="preserve"> кадастровым номером (обозначением)</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="3261" w:type="dxa"/>
									<w:gridSpan w:val="9"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="nil"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRPr="0002329B" w:rsidRDefault="0002329B">
									<w:pPr>
										<w:keepNext/>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
									</w:pPr>
									<w:proofErr w:type="spellStart"/>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
										<w:t><xsl:value-of select="@CadastralNumber"/></w:t>
									</w:r>
									<w:proofErr w:type="spellEnd"/>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1278" w:type="dxa"/>
									<w:gridSpan w:val="2"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:keepNext/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="9670" w:type="dxa"/>
									<w:gridSpan w:val="26"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:keepNext/>
										<w:rPr>
											<w:sz w:val="9"/>
											<w:szCs w:val="9"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2863" w:type="dxa"/>
									<w:gridSpan w:val="5"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>Обозначение</w:t>
									</w:r>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:br/>
										<w:t>характерной точки</w:t>
									</w:r>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:br/>
										<w:t>или части границ</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="3402" w:type="dxa"/>
									<w:gridSpan w:val="14"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>Кадастровые номера земельных участков, смежных с уточняемым земельным участком</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="3405" w:type="dxa"/>
									<w:gridSpan w:val="7"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>Сведения о правообладателях смежных земельных участков</w:t>
									</w:r>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:trHeight w:val="369"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2863" w:type="dxa"/>
									<w:gridSpan w:val="5"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>1</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="3402" w:type="dxa"/>
									<w:gridSpan w:val="14"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>2</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="3405" w:type="dxa"/>
									<w:gridSpan w:val="7"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>3</w:t>
									</w:r>
								</w:p>
							</w:tc>
						</w:tr>
						
             
        
        <xsl:apply-templates select="RelatedParcels"/> 
         </w:tbl>
     </xsl:template>
    
    <xsl:template match="Entity_Spatial" mode="ExistParcel">
        <xsl:for-each select="Spatial_Element">
            <xsl:if test="count(child::*) != 0">
                <xsl:call-template name="tSPATIAL_ELEMENT_OLD_NEWExistParcel"/>
            </xsl:if>
        </xsl:for-each>
    </xsl:template> 
    <xsl:template match="Entity_Spatial" mode="Borders">
        
        <xsl:if test="Borders">
            
            
            <xsl:for-each select="Borders/Border">
                <xsl:call-template name="tBorder"/>
            </xsl:for-each>
        </xsl:if>
    </xsl:template>
    <xsl:template name="tBorder">
        <w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
            <w:tblPrEx>
                <w:tblCellMar>
                    <w:top w:w="0" w:type="dxa"/>
                    <w:bottom w:w="0" w:type="dxa"/>
                </w:tblCellMar>
            </w:tblPrEx>
            <w:trPr>
                <w:cantSplit/>
                <w:trHeight w:val="340"/>
            </w:trPr>
            <w:tc>
                <w:tcPr>
                    <w:tcW w:w="1006" w:type="dxa"/>
                    <w:gridSpan w:val="2"/>
                    <w:tcBorders>
                        <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                        <w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                        <w:bottom w:val="single" w:sz="6" w:space="0" w:color="auto"/>
                        <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                    </w:tcBorders>
                    <w:vAlign w:val="center"/>
                </w:tcPr>
                <w:p w:rsidR="007E23C6" w:rsidRPr="0002329B" w:rsidRDefault="0002329B">
                    <w:pPr>
                        <w:jc w:val="center"/>
                        <w:rPr>
                            <w:sz w:val="21"/>
                            <w:szCs w:val="21"/>
                            <w:lang w:val="en-US"/>
                        </w:rPr>
                    </w:pPr>
                    <w:proofErr w:type="spellStart"/>
                    <w:r>
                        <w:rPr>
                            <w:sz w:val="21"/>
                            <w:szCs w:val="21"/>
                            <w:lang w:val="en-US"/>
                        </w:rPr>
                        <w:t><xsl:value-of select="@Point1"/></w:t>
                    </w:r>
                    <w:proofErr w:type="spellEnd"/>
                </w:p>
            </w:tc>
            <w:tc>
                <w:tcPr>
                    <w:tcW w:w="1290" w:type="dxa"/>
                    <w:gridSpan w:val="2"/>
                    <w:tcBorders>
                        <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                        <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                        <w:bottom w:val="single" w:sz="6" w:space="0" w:color="auto"/>
                        <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                    </w:tcBorders>
                    <w:vAlign w:val="center"/>
                </w:tcPr>
                <w:p w:rsidR="007E23C6" w:rsidRPr="0002329B" w:rsidRDefault="0002329B">
                    <w:pPr>
                        <w:jc w:val="center"/>
                        <w:rPr>
                            <w:sz w:val="21"/>
                            <w:szCs w:val="21"/>
                            <w:lang w:val="en-US"/>
                        </w:rPr>
                    </w:pPr>
                    <w:proofErr w:type="spellStart"/>
                    <w:r>
                        <w:rPr>
                            <w:sz w:val="21"/>
                            <w:szCs w:val="21"/>
                            <w:lang w:val="en-US"/>
                        </w:rPr>
                        <w:t> <xsl:value-of select="@Point2"/></w:t>
                    </w:r>
                    <w:proofErr w:type="spellEnd"/>
                </w:p>
            </w:tc>
            <w:tc>
                <w:tcPr>
                    <w:tcW w:w="3828" w:type="dxa"/>
                    <w:gridSpan w:val="14"/>
                    <w:tcBorders>
                        <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                        <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                        <w:bottom w:val="single" w:sz="6" w:space="0" w:color="auto"/>
                        <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                    </w:tcBorders>
                    <w:vAlign w:val="center"/>
                </w:tcPr>
                <w:p w:rsidR="007E23C6" w:rsidRPr="0002329B" w:rsidRDefault="0002329B">
                    <w:pPr>
                        <w:jc w:val="center"/>
                        <w:rPr>
                            <w:sz w:val="21"/>
                            <w:szCs w:val="21"/>
                            <w:lang w:val="en-US"/>
                        </w:rPr>
                    </w:pPr>
                    <w:proofErr w:type="spellStart"/>
                    <w:r>
                        <w:rPr>
                            <w:sz w:val="21"/>
                            <w:szCs w:val="21"/>
                            <w:lang w:val="en-US"/>
                        </w:rPr>
                        <w:t>  <xsl:value-of select="Edge/Length"/></w:t>
                    </w:r>
                    <w:proofErr w:type="spellEnd"/>
                </w:p>
            </w:tc>
            <w:tc>
                <w:tcPr>
                    <w:tcW w:w="3546" w:type="dxa"/>
                    <w:gridSpan w:val="8"/>
                    <w:tcBorders>
                        <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                        <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                        <w:bottom w:val="single" w:sz="6" w:space="0" w:color="auto"/>
                        <w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                    </w:tcBorders>
                    <w:vAlign w:val="center"/>
                </w:tcPr>
                <w:p w:rsidR="007E23C6" w:rsidRPr="0002329B" w:rsidRDefault="0002329B">
                    <w:pPr>
                        <w:jc w:val="center"/>
                        <w:rPr>
                            <w:sz w:val="21"/>
                            <w:szCs w:val="21"/>
                            <w:lang w:val="en-US"/>
                        </w:rPr>
                    </w:pPr>
                    <w:proofErr w:type="spellStart"/>
                    <w:r>
                        <w:rPr>
                            <w:sz w:val="21"/>
                            <w:szCs w:val="21"/>
                            <w:lang w:val="en-US"/>
                        </w:rPr>
                        <w:t><xsl:value-of select="Edge/Definition"/>
                        </w:t>
                    </w:r>
                    <w:proofErr w:type="spellEnd"/>
                </w:p>
            </w:tc>
        </w:tr>
        
        
    </xsl:template>
    
    <!--Элемент контура-->
    <xsl:template name="tSPATIAL_ELEMENT_OLD_NEWExistParcel">
        <xsl:for-each select="Spelement_Unit">
            <xsl:call-template name="tSPELEMENT_UNIT_OLD_NEWExistParcel"/>
        </xsl:for-each>
        <xsl:if test="position() != last()">
            <w:tr w:rsidR="0002329B" w:rsidTr="000C4A3C">
                <w:tblPrEx>
                    <w:tblBorders>
                        <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                        <w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                        <w:bottom w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                        <w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                        <w:insideH w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                        <w:insideV w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                    </w:tblBorders>
                    <w:tblCellMar>
                        <w:top w:w="0" w:type="dxa"/>
                        <w:bottom w:w="0" w:type="dxa"/>
                    </w:tblCellMar>
                </w:tblPrEx>
                <w:trPr>
                    <w:cantSplit/>
                    <w:trHeight w:val="340"/>
                </w:trPr>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="9670" w:type="dxa"/>
                        <w:gridSpan w:val="26"/>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p w:rsidR="0002329B" w:rsidRPr="0002329B" w:rsidRDefault="0002329B">
                        <w:pPr>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:sz w:val="21"/>
                                <w:szCs w:val="21"/>
                                <w:lang w:val="en-US"/>
                            </w:rPr>
                        </w:pPr>
                        <w:proofErr w:type="spellStart"/>
                        <w:r>
                            <w:rPr>
                                <w:sz w:val="21"/>
                                <w:szCs w:val="21"/>
                                <w:lang w:val="en-US"/>
                            </w:rPr>
                            <w:t>      </w:t>
                        </w:r>
                        <w:proofErr w:type="spellEnd"/>
                    </w:p>
                </w:tc>
            </w:tr>
            
        </xsl:if>
    </xsl:template>
    
    
    
    <xsl:template name="tSPELEMENT_UNIT_OLD_NEWExistParcel">
        <w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
            <w:tblPrEx>
                <w:tblBorders>
                    <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                    <w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                    <w:bottom w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                    <w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                    <w:insideH w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                    <w:insideV w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                </w:tblBorders>
                <w:tblCellMar>
                    <w:top w:w="0" w:type="dxa"/>
                    <w:bottom w:w="0" w:type="dxa"/>
                </w:tblCellMar>
            </w:tblPrEx>
            <w:trPr>
                <w:cantSplit/>
                <w:trHeight w:val="340"/>
            </w:trPr>
            <w:tc>
                <w:tcPr>
                    <w:tcW w:w="1021" w:type="dxa"/>
                    <w:gridSpan w:val="3"/>
                    <w:vAlign w:val="center"/>
                </w:tcPr>
                <w:p w:rsidR="007E23C6" w:rsidRPr="0002329B" w:rsidRDefault="0002329B">
                    <w:pPr>
                        <w:jc w:val="center"/>
                        <w:rPr>
                            <w:sz w:val="21"/>
                            <w:szCs w:val="21"/>
                            <w:lang w:val="en-US"/>
                        </w:rPr>
                    </w:pPr>
                    <w:proofErr w:type="spellStart"/>
                    <w:r>
                        <w:rPr>
                            <w:sz w:val="21"/>
                            <w:szCs w:val="21"/>
                            <w:lang w:val="en-US"/>
                        </w:rPr>
                        <w:t><xsl:value-of select="@Point_Pref"/><xsl:value-of select="@Num_Geopoint"/></w:t>
                    </w:r>
                    <w:proofErr w:type="spellEnd"/>
                </w:p>
            </w:tc>
            <w:tc>
                <w:tcPr>
                    <w:tcW w:w="1417" w:type="dxa"/>
                    <w:gridSpan w:val="3"/>
                    <w:vAlign w:val="center"/>
                </w:tcPr>
                <w:p w:rsidR="007E23C6" w:rsidRPr="0002329B" w:rsidRDefault="0002329B">
                    <w:pPr>
                        <w:jc w:val="center"/>
                        <w:rPr>
                            <w:sz w:val="21"/>
                            <w:szCs w:val="21"/>
                            <w:lang w:val="en-US"/>
                        </w:rPr>
                    </w:pPr>
                    <w:proofErr w:type="spellStart"/>
                    <w:r>
                        <w:rPr>
                            <w:sz w:val="21"/>
                            <w:szCs w:val="21"/>
                            <w:lang w:val="en-US"/>
                        </w:rPr>
                        <w:t><xsl:value-of select="OldOrdinate/@X"/></w:t>
                    </w:r>
                    <w:proofErr w:type="spellEnd"/>
                </w:p>
            </w:tc>
            <w:tc>
                <w:tcPr>
                    <w:tcW w:w="1488" w:type="dxa"/>
                    <w:gridSpan w:val="3"/>
                    <w:vAlign w:val="center"/>
                </w:tcPr>
                <w:p w:rsidR="007E23C6" w:rsidRPr="0002329B" w:rsidRDefault="0002329B">
                    <w:pPr>
                        <w:jc w:val="center"/>
                        <w:rPr>
                            <w:sz w:val="21"/>
                            <w:szCs w:val="21"/>
                            <w:lang w:val="en-US"/>
                        </w:rPr>
                    </w:pPr>
                    <w:proofErr w:type="spellStart"/>
                    <w:r>
                        <w:rPr>
                            <w:sz w:val="21"/>
                            <w:szCs w:val="21"/>
                            <w:lang w:val="en-US"/>
                        </w:rPr>
                        <w:t><xsl:value-of select="OldOrdinate/@Y"/></w:t>
                    </w:r>
                    <w:proofErr w:type="spellEnd"/>
                </w:p>
            </w:tc>
            <w:tc>
                <w:tcPr>
                    <w:tcW w:w="1489" w:type="dxa"/>
                    <w:gridSpan w:val="5"/>
                    <w:vAlign w:val="center"/>
                </w:tcPr>
                <w:p w:rsidR="007E23C6" w:rsidRPr="0002329B" w:rsidRDefault="0002329B">
                    <w:pPr>
                        <w:jc w:val="center"/>
                        <w:rPr>
                            <w:sz w:val="21"/>
                            <w:szCs w:val="21"/>
                            <w:lang w:val="en-US"/>
                        </w:rPr>
                    </w:pPr>
                    <w:proofErr w:type="spellStart"/>
                    <w:r>
                        <w:rPr>
                            <w:sz w:val="21"/>
                            <w:szCs w:val="21"/>
                            <w:lang w:val="en-US"/>
                        </w:rPr>
                        <w:t><xsl:value-of select="NewOrdinate/@X"/></w:t>
                    </w:r>
                    <w:proofErr w:type="spellEnd"/>
                </w:p>
            </w:tc>
            <w:tc>
                <w:tcPr>
                    <w:tcW w:w="1417" w:type="dxa"/>
                    <w:gridSpan w:val="2"/>
                    <w:vAlign w:val="center"/>
                </w:tcPr>
                <w:p w:rsidR="007E23C6" w:rsidRPr="0002329B" w:rsidRDefault="0002329B">
                    <w:pPr>
                        <w:jc w:val="center"/>
                        <w:rPr>
                            <w:sz w:val="21"/>
                            <w:szCs w:val="21"/>
                            <w:lang w:val="en-US"/>
                        </w:rPr>
                    </w:pPr>
                    <w:proofErr w:type="spellStart"/>
                    <w:r>
                        <w:rPr>
                            <w:sz w:val="21"/>
                            <w:szCs w:val="21"/>
                            <w:lang w:val="en-US"/>
                        </w:rPr>
                        <w:t><xsl:value-of select="NewOrdinate/@Y"/></w:t>
                    </w:r>
                    <w:proofErr w:type="spellEnd"/>
                </w:p>
            </w:tc>
            <w:tc>
                <w:tcPr>
                    <w:tcW w:w="1134" w:type="dxa"/>
                    <w:gridSpan w:val="7"/>
                    <w:vAlign w:val="center"/>
                </w:tcPr>
                <w:p w:rsidR="007E23C6" w:rsidRPr="0002329B" w:rsidRDefault="0002329B">
                    <w:pPr>
                        <w:jc w:val="center"/>
                        <w:rPr>
                            <w:sz w:val="21"/>
                            <w:szCs w:val="21"/>
                            <w:lang w:val="en-US"/>
                        </w:rPr>
                    </w:pPr>
                    <w:proofErr w:type="spellStart"/>
                    <w:r>
                        <w:rPr>
                            <w:sz w:val="21"/>
                            <w:szCs w:val="21"/>
                            <w:lang w:val="en-US"/>
                        </w:rPr>
                        <w:t><xsl:value-of select="@Delta_Geopoint"/></w:t>
                    </w:r>
                    <w:proofErr w:type="spellEnd"/>
                </w:p>
            </w:tc>
            <w:tc>
                <w:tcPr>
                    <w:tcW w:w="1704" w:type="dxa"/>
                    <w:gridSpan w:val="3"/>
                    <w:vAlign w:val="center"/>
                </w:tcPr>
                <w:p w:rsidR="007E23C6" w:rsidRPr="0002329B" w:rsidRDefault="0002329B">
                    <w:pPr>
                        <w:jc w:val="center"/>
                        <w:rPr>
                            <w:sz w:val="21"/>
                            <w:szCs w:val="21"/>
                            <w:lang w:val="en-US"/>
                        </w:rPr>
                    </w:pPr>
                    <w:proofErr w:type="spellStart"/>
                    <w:r>
                        <w:rPr>
                            <w:sz w:val="21"/>
                            <w:szCs w:val="21"/>
                            <w:lang w:val="en-US"/>
                        </w:rPr>
                        <w:t><xsl:value-of select="@Geopoint_Zacrep"/></w:t>
                    </w:r>
                    <w:proofErr w:type="spellEnd"/>
                </w:p>
            </w:tc>
        </w:tr>
        
    </xsl:template>
    
    
    <xsl:template match="SubParcels" mode="tExistParcelGeneral">
          <w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
								<w:trHeight w:val="292"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="9670" w:type="dxa"/>
									<w:gridSpan w:val="26"/>
									<w:tcBorders>
										<w:top w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>5. Общие сведения о частях уточняемого земельного участка</w:t>
									</w:r>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
								<w:trHeight w:val="275"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="4706" w:type="dxa"/>
									<w:gridSpan w:val="13"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="nil"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:ind w:left="85" w:right="57"/>
										<w:jc w:val="right"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:proofErr w:type="gramStart"/>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>с</w:t>
									</w:r>
									<w:proofErr w:type="gramEnd"/>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t xml:space="preserve"> кадастровым номером</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2552" w:type="dxa"/>
									<w:gridSpan w:val="7"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="nil"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRPr="0002329B" w:rsidRDefault="0002329B">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
									</w:pPr>
									<w:proofErr w:type="spellStart"/>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
										<w:t><xsl:value-of select="parent::node()/@CadastralNumber"/></w:t>
									</w:r>
									<w:proofErr w:type="spellEnd"/>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2412" w:type="dxa"/>
									<w:gridSpan w:val="6"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="9670" w:type="dxa"/>
									<w:gridSpan w:val="26"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:rPr>
											<w:sz w:val="9"/>
											<w:szCs w:val="9"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="737" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>№ п/п</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2693" w:type="dxa"/>
									<w:gridSpan w:val="7"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>Учетный номер или обозначение части</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1276" w:type="dxa"/>
									<w:gridSpan w:val="5"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>Площадь (Р), м</w:t>
									</w:r>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
											<w:vertAlign w:val="superscript"/>
										</w:rPr>
										<w:t>2</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1276" w:type="dxa"/>
									<w:gridSpan w:val="4"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>± ∆Р, м</w:t>
									</w:r>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
											<w:vertAlign w:val="superscript"/>
										</w:rPr>
										<w:t>2</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="3688" w:type="dxa"/>
									<w:gridSpan w:val="9"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>Характеристика части</w:t>
									</w:r>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
								<w:trHeight w:val="369"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="737" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>1</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2693" w:type="dxa"/>
									<w:gridSpan w:val="7"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>2</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1276" w:type="dxa"/>
									<w:gridSpan w:val="5"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>3</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1276" w:type="dxa"/>
									<w:gridSpan w:val="4"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>4</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="3688" w:type="dxa"/>
									<w:gridSpan w:val="9"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>5</w:t>
									</w:r>
								</w:p>
							</w:tc>
						</w:tr>
						
        <xsl:for-each select="FormSubParcel">
            <xsl:call-template  name="tNewParcelGeneral"/>
        </xsl:for-each>  
        <xsl:for-each select="ExistSubParcel">
            <xsl:call-template  name="tNewParcelGeneral"/>
        </xsl:for-each>  
        <xsl:for-each select="InvariableSubParcel">
            <xsl:call-template  name="tNewParcelGeneral"/>
        </xsl:for-each>  
    </xsl:template>
    
    <xsl:template name="tLocationExist">
        <w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="737" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>1</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="5245" w:type="dxa"/>
									<w:gridSpan w:val="16"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:ind w:left="57" w:right="57"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t xml:space="preserve">Площадь земельного участка ± величина погрешности определения площади </w:t>
									</w:r>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t xml:space="preserve">(Р ± ∆Р), </w:t>
									</w:r>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
										</w:rPr>
										<w:t>м</w:t>
									</w:r>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:vertAlign w:val="superscript"/>
										</w:rPr>
										<w:t>2</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="3688" w:type="dxa"/>
									<w:gridSpan w:val="9"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRPr="0002329B" w:rsidRDefault="0002329B">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
									</w:pPr>
									<w:proofErr w:type="spellStart"/>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
									    <w:t><xsl:value-of select="Area/Area"/> ±<xsl:value-of select="Area/Innccuracy"/></w:t>
									</w:r>
									<w:proofErr w:type="spellEnd"/>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="737" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>2</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="5245" w:type="dxa"/>
									<w:gridSpan w:val="16"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:ind w:left="57" w:right="57"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t xml:space="preserve">Площадь земельного участка по сведениям государственного кадастра недвижимости </w:t>
									</w:r>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>(</w:t>
									</w:r>
									<w:proofErr w:type="spellStart"/>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>Ркад</w:t>
									</w:r>
									<w:proofErr w:type="spellEnd"/>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>), м</w:t>
									</w:r>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
											<w:vertAlign w:val="superscript"/>
										</w:rPr>
										<w:t>2</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="3688" w:type="dxa"/>
									<w:gridSpan w:val="9"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRPr="0002329B" w:rsidRDefault="0002329B">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
									</w:pPr>
									<w:proofErr w:type="spellStart"/>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
									    <w:t><xsl:value-of select="Area_In_GKN"/></w:t>
									</w:r>
									<w:proofErr w:type="spellEnd"/>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="737" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>3</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="5245" w:type="dxa"/>
									<w:gridSpan w:val="16"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:ind w:left="57" w:right="57"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t xml:space="preserve">Оценка расхождения </w:t>
									</w:r>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>Р</w:t>
									</w:r>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t xml:space="preserve"> и </w:t>
									</w:r>
									<w:proofErr w:type="spellStart"/>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>Ркад</w:t>
									</w:r>
									<w:proofErr w:type="spellEnd"/>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t xml:space="preserve"> (Р - </w:t>
									</w:r>
									<w:proofErr w:type="spellStart"/>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>Ркад</w:t>
									</w:r>
									<w:proofErr w:type="spellEnd"/>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>), м</w:t>
									</w:r>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
											<w:vertAlign w:val="superscript"/>
										</w:rPr>
										<w:t>2</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="3688" w:type="dxa"/>
									<w:gridSpan w:val="9"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRPr="0002329B" w:rsidRDefault="0002329B">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
									</w:pPr>
									<w:proofErr w:type="spellStart"/>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
									    <w:t><xsl:value-of select="Delta_Area"/></w:t>
									</w:r>
									<w:proofErr w:type="spellEnd"/>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="737" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>4</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="5245" w:type="dxa"/>
									<w:gridSpan w:val="16"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:pStyle w:val="1"/>
										<w:rPr>
											<w:b w:val="0"/>
											<w:bCs w:val="0"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b w:val="0"/>
											<w:bCs w:val="0"/>
										</w:rPr>
										<w:t xml:space="preserve">Предельный минимальный и максимальный размер земельного участка </w:t>
									</w:r>
									<w:r>
										<w:t>(</w:t>
									</w:r>
									<w:proofErr w:type="spellStart"/>
									<w:r>
										<w:t>Рмин</w:t>
									</w:r>
									<w:proofErr w:type="spellEnd"/>
									<w:r>
										<w:t xml:space="preserve"> и </w:t>
									</w:r>
									<w:proofErr w:type="spellStart"/>
									<w:r>
										<w:t>Рмакс</w:t>
									</w:r>
									<w:proofErr w:type="spellEnd"/>
									<w:r>
										<w:t>), м</w:t>
									</w:r>
									<w:r>
										<w:rPr>
											<w:vertAlign w:val="superscript"/>
										</w:rPr>
										<w:t>2</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="3688" w:type="dxa"/>
									<w:gridSpan w:val="9"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRPr="0002329B" w:rsidRDefault="0002329B">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
									</w:pPr>
									<w:proofErr w:type="spellStart"/>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
									    <w:t><xsl:value-of select="Min_Area/Area"/> и <xsl:value-of select="Max_Area/Area"/></w:t>
									</w:r>
									<w:proofErr w:type="spellEnd"/>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="737" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>5</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="5245" w:type="dxa"/>
									<w:gridSpan w:val="16"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:pStyle w:val="1"/>
										<w:rPr>
											<w:b w:val="0"/>
											<w:bCs w:val="0"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b w:val="0"/>
											<w:bCs w:val="0"/>
										</w:rPr>
										<w:t>Кадастровый или иной номер здания, сооружения, объекта незавершенного строительства, расположенного на земельном участке</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="3688" w:type="dxa"/>
									<w:gridSpan w:val="9"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRPr="0002329B" w:rsidRDefault="0002329B">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
									</w:pPr>
									<w:proofErr w:type="spellStart"/>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
									    <w:t><xsl:apply-templates select="Inner_CadastralNumbers"/></w:t>
									</w:r>
									<w:proofErr w:type="spellEnd"/>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:trHeight w:val="312"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="737" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>6</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="5245" w:type="dxa"/>
									<w:gridSpan w:val="16"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:ind w:left="57" w:right="57"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>Иные сведения</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="3688" w:type="dxa"/>
									<w:gridSpan w:val="9"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRPr="0002329B" w:rsidRDefault="0002329B">
									<w:pPr>
										<w:ind w:left="57" w:right="57"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
									</w:pPr>
									<w:proofErr w:type="spellStart"/>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
									    <w:t><xsl:value-of select="Note"/></w:t>
									</w:r>
									<w:proofErr w:type="spellEnd"/>
								</w:p>
							</w:tc>
						</w:tr>
						
    </xsl:template>

    <xsl:template match="Contours">
           <xsl:for-each select="NewContour">
               <w:tr w:rsidR="0002329B" w:rsidTr="000C4A3C">
                   <w:tblPrEx>
                       <w:tblBorders>
                           <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                           <w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                           <w:bottom w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                           <w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                           <w:insideH w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                           <w:insideV w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                       </w:tblBorders>
                       <w:tblCellMar>
                           <w:top w:w="0" w:type="dxa"/>
                           <w:bottom w:w="0" w:type="dxa"/>
                       </w:tblCellMar>
                   </w:tblPrEx>
                   <w:trPr>
                       <w:cantSplit/>
                       <w:trHeight w:val="340"/>
                   </w:trPr>
                   <w:tc>
                       <w:tcPr>
                           <w:tcW w:w="9670" w:type="dxa"/>
                           <w:gridSpan w:val="26"/>
                           <w:vAlign w:val="center"/>
                       </w:tcPr>
                       <w:p w:rsidR="0002329B" w:rsidRPr="0002329B" w:rsidRDefault="0002329B">
                           <w:pPr>
                               <w:jc w:val="center"/>
                               <w:rPr>
                                   <w:sz w:val="21"/>
                                   <w:szCs w:val="21"/>
                                   <w:lang w:val="en-US"/>
                               </w:rPr>
                           </w:pPr>
                           <w:proofErr w:type="spellStart"/>
                           <w:r>
                               <w:rPr>
                                   <w:sz w:val="21"/>
                                   <w:szCs w:val="21"/>
                                   <w:lang w:val="en-US"/>
                               </w:rPr>
                               <w:t>  <xsl:value-of select="@Definition"/>    </w:t>
                           </w:r>
                           <w:proofErr w:type="spellEnd"/>
                       </w:p>
                   </w:tc>
               </w:tr>
               
                   
                    <xsl:call-template name="tContour"/>
            </xsl:for-each>
            <xsl:for-each select="ExistContour">
                <w:tr w:rsidR="0002329B" w:rsidTr="000C4A3C">
                    <w:tblPrEx>
                        <w:tblBorders>
                            <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                            <w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                            <w:bottom w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                            <w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                            <w:insideH w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                            <w:insideV w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                        </w:tblBorders>
                        <w:tblCellMar>
                            <w:top w:w="0" w:type="dxa"/>
                            <w:bottom w:w="0" w:type="dxa"/>
                        </w:tblCellMar>
                    </w:tblPrEx>
                    <w:trPr>
                        <w:cantSplit/>
                        <w:trHeight w:val="340"/>
                    </w:trPr>
                    <w:tc>
                        <w:tcPr>
                            <w:tcW w:w="9670" w:type="dxa"/>
                            <w:gridSpan w:val="26"/>
                            <w:vAlign w:val="center"/>
                        </w:tcPr>
                        <w:p w:rsidR="0002329B" w:rsidRPr="0002329B" w:rsidRDefault="0002329B">
                            <w:pPr>
                                <w:jc w:val="center"/>
                                <w:rPr>
                                    <w:sz w:val="21"/>
                                    <w:szCs w:val="21"/>
                                    <w:lang w:val="en-US"/>
                                </w:rPr>
                            </w:pPr>
                            <w:proofErr w:type="spellStart"/>
                            <w:r>
                                <w:rPr>
                                    <w:sz w:val="21"/>
                                    <w:szCs w:val="21"/>
                                    <w:lang w:val="en-US"/>
                                </w:rPr>
                                <w:t>  <xsl:value-of select="@Number_Record"/>   </w:t>
                            </w:r>
                            <w:proofErr w:type="spellEnd"/>
                        </w:p>
                    </w:tc>
                </w:tr>
               
                    <xsl:call-template name="tContour"/>
            </xsl:for-each>
            <xsl:for-each select="DeleteAllBorder">
                <w:tr w:rsidR="0002329B" w:rsidTr="000C4A3C">
                    <w:tblPrEx>
                        <w:tblBorders>
                            <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                            <w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                            <w:bottom w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                            <w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                            <w:insideH w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                            <w:insideV w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                        </w:tblBorders>
                        <w:tblCellMar>
                            <w:top w:w="0" w:type="dxa"/>
                            <w:bottom w:w="0" w:type="dxa"/>
                        </w:tblCellMar>
                    </w:tblPrEx>
                    <w:trPr>
                        <w:cantSplit/>
                        <w:trHeight w:val="340"/>
                    </w:trPr>
                    <w:tc>
                        <w:tcPr>
                            <w:tcW w:w="9670" w:type="dxa"/>
                            <w:gridSpan w:val="26"/>
                            <w:vAlign w:val="center"/>
                        </w:tcPr>
                        <w:p w:rsidR="0002329B" w:rsidRPr="0002329B" w:rsidRDefault="0002329B">
                            <w:pPr>
                                <w:jc w:val="center"/>
                                <w:rPr>
                                    <w:sz w:val="21"/>
                                    <w:szCs w:val="21"/>
                                    <w:lang w:val="en-US"/>
                                </w:rPr>
                            </w:pPr>
                            <w:proofErr w:type="spellStart"/>
                            <w:r>
                                <w:rPr>
                                    <w:sz w:val="21"/>
                                    <w:szCs w:val="21"/>
                                    <w:lang w:val="en-US"/>
                                </w:rPr>
                                <w:t>  <xsl:value-of select="@Number_Record"/>   </w:t>
                            </w:r>
                            <w:proofErr w:type="spellEnd"/>
                        </w:p>
                    </w:tc>
                </w:tr>
                    <xsl:for-each select="OldOrdinate">
                            <xsl:call-template name="tOrdinate"/>
                    </xsl:for-each>
            </xsl:for-each>
    </xsl:template>
    <xsl:template name="tOrdinate">
        <w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
            <w:tblPrEx>
                <w:tblCellMar>
                    <w:top w:w="0" w:type="dxa"/>
                    <w:bottom w:w="0" w:type="dxa"/>
                </w:tblCellMar>
            </w:tblPrEx>
            <w:trPr>
                <w:cantSplit/>
                <w:trHeight w:val="340"/>
            </w:trPr>
            <w:tc>
                <w:tcPr>
                    <w:tcW w:w="2025" w:type="dxa"/>
                    <w:gridSpan w:val="4"/>
                    <w:tcBorders>
                        <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                        <w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                    	<w:bottom w:val="single" w:sz="6" w:space="0" w:color="auto"/>
                        <w:right w:val="nil"/>
                    </w:tcBorders>
                    <w:vAlign w:val="center"/>
                </w:tcPr>
                <w:p w:rsidR="007E23C6" w:rsidRPr="0002329B" w:rsidRDefault="0002329B">
                    <w:pPr>
                        <w:jc w:val="center"/>
                        <w:rPr>
                            <w:sz w:val="21"/>
                            <w:szCs w:val="21"/>
                            <w:lang w:val="en-US"/>
                        </w:rPr>
                    </w:pPr>
                    <w:proofErr w:type="spellStart"/>
                    <w:r>
                        <w:rPr>
                            <w:sz w:val="21"/>
                            <w:szCs w:val="21"/>
                            <w:lang w:val="en-US"/>
                        </w:rPr>
                        <w:t> <xsl:value-of select="@Point_Pref"/> <xsl:value-of select="@Num_Geopoint"/></w:t>
                    </w:r>
                    <w:proofErr w:type="spellEnd"/>
                </w:p>
            </w:tc>
            <w:tc>
                <w:tcPr>
                    <w:tcW w:w="1524" w:type="dxa"/>
                    <w:gridSpan w:val="3"/>
                    <w:tcBorders>
                        <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                        <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                    	<w:bottom w:val="single" w:sz="6" w:space="0" w:color="auto"/>
                        <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                    </w:tcBorders>
                    <w:vAlign w:val="center"/>
                </w:tcPr>
                <w:p w:rsidR="007E23C6" w:rsidRPr="0002329B" w:rsidRDefault="0002329B">
                    <w:pPr>
                        <w:jc w:val="center"/>
                        <w:rPr>
                            <w:sz w:val="21"/>
                            <w:szCs w:val="21"/>
                            <w:lang w:val="en-US"/>
                        </w:rPr>
                    </w:pPr>
                    <w:proofErr w:type="spellStart"/>
                    <w:r>
                        <w:rPr>
                            <w:sz w:val="21"/>
                            <w:szCs w:val="21"/>
                            <w:lang w:val="en-US"/>
                        </w:rPr>
                        <w:t> <xsl:value-of select="@X"/></w:t>
                    </w:r>
                    <w:proofErr w:type="spellEnd"/>
                </w:p>
            </w:tc>
            <w:tc>
                <w:tcPr>
                    <w:tcW w:w="1701" w:type="dxa"/>
                    <w:gridSpan w:val="5"/>
                    <w:tcBorders>
                        <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                        <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                    	<w:bottom w:val="single" w:sz="6" w:space="0" w:color="auto"/>
                        <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                    </w:tcBorders>
                    <w:vAlign w:val="center"/>
                </w:tcPr>
                <w:p w:rsidR="007E23C6" w:rsidRPr="0002329B" w:rsidRDefault="0002329B">
                    <w:pPr>
                        <w:jc w:val="center"/>
                        <w:rPr>
                            <w:sz w:val="21"/>
                            <w:szCs w:val="21"/>
                            <w:lang w:val="en-US"/>
                        </w:rPr>
                    </w:pPr>
                    <w:proofErr w:type="spellStart"/>
                    <w:r>
                        <w:rPr>
                            <w:sz w:val="21"/>
                            <w:szCs w:val="21"/>
                            <w:lang w:val="en-US"/>
                        </w:rPr>
                        <w:t><xsl:value-of select="@Y"/></w:t>
                    </w:r>
                    <w:proofErr w:type="spellEnd"/>
                </w:p>
            </w:tc>
            <w:tc>
                <w:tcPr>
                    <w:tcW w:w="1696" w:type="dxa"/>
                    <w:gridSpan w:val="9"/>
                    <w:tcBorders>
                        <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                        <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                    	<w:bottom w:val="single" w:sz="6" w:space="0" w:color="auto"/>
                        <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                    </w:tcBorders>
                    <w:vAlign w:val="center"/>
                </w:tcPr>
                <w:p w:rsidR="007E23C6" w:rsidRPr="0002329B" w:rsidRDefault="0002329B">
                    <w:pPr>
                        <w:jc w:val="center"/>
                        <w:rPr>
                            <w:sz w:val="21"/>
                            <w:szCs w:val="21"/>
                            <w:lang w:val="en-US"/>
                        </w:rPr>
                    </w:pPr>
                    <w:proofErr w:type="spellStart"/>
                    <w:r>
                        <w:rPr>
                            <w:sz w:val="21"/>
                            <w:szCs w:val="21"/>
                            <w:lang w:val="en-US"/>
                        </w:rPr>
                        <w:t> <xsl:value-of select="@Delta_Geopoint"/></w:t>
                    </w:r>
                    <w:proofErr w:type="spellEnd"/>
                </w:p>
            </w:tc>
            <w:tc>
                <w:tcPr>
                    <w:tcW w:w="2724" w:type="dxa"/>
                    <w:gridSpan w:val="5"/>
                    <w:tcBorders>
                        <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                        <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                    	<w:bottom w:val="single" w:sz="6" w:space="0" w:color="auto"/>
                        <w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                    </w:tcBorders>
                    <w:vAlign w:val="center"/>
                </w:tcPr>
                <w:p w:rsidR="007E23C6" w:rsidRPr="0002329B" w:rsidRDefault="0002329B">
                    <w:pPr>
                        <w:jc w:val="center"/>
                        <w:rPr>
                            <w:sz w:val="21"/>
                            <w:szCs w:val="21"/>
                            <w:lang w:val="en-US"/>
                        </w:rPr>
                    </w:pPr>
                    <w:proofErr w:type="spellStart"/>
                    <w:r>
                        <w:rPr>
                            <w:sz w:val="21"/>
                            <w:szCs w:val="21"/>
                            <w:lang w:val="en-US"/>
                        </w:rPr>
                        <w:t><xsl:value-of select="@Geopoint_Zacrep"/></w:t>
                    </w:r>
                    <w:proofErr w:type="spellEnd"/>
                </w:p>
            </w:tc>
        </w:tr>
    </xsl:template>
    
    
    <xsl:template match= "Contours" mode="Borders">
        <xsl:for-each select="NewContour">
            <w:tr w:rsidR="0002329B" w:rsidTr="000C4A3C">
                <w:tblPrEx>
                    <w:tblBorders>
                        <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                        <w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                        <w:bottom w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                        <w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                        <w:insideH w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                        <w:insideV w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                    </w:tblBorders>
                    <w:tblCellMar>
                        <w:top w:w="0" w:type="dxa"/>
                        <w:bottom w:w="0" w:type="dxa"/>
                    </w:tblCellMar>
                </w:tblPrEx>
                <w:trPr>
                    <w:cantSplit/>
                    <w:trHeight w:val="340"/>
                </w:trPr>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="9670" w:type="dxa"/>
                        <w:gridSpan w:val="26"/>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p w:rsidR="0002329B" w:rsidRPr="0002329B" w:rsidRDefault="0002329B">
                        <w:pPr>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:sz w:val="21"/>
                                <w:szCs w:val="21"/>
                                <w:lang w:val="en-US"/>
                            </w:rPr>
                        </w:pPr>
                        <w:proofErr w:type="spellStart"/>
                        <w:r>
                            <w:rPr>
                                <w:sz w:val="21"/>
                                <w:szCs w:val="21"/>
                                <w:lang w:val="en-US"/>
                            </w:rPr>
                            <w:t>  <xsl:value-of select="@Definition"/>    </w:t>
                        </w:r>
                        <w:proofErr w:type="spellEnd"/>
                    </w:p>
                </w:tc>
            </w:tr>
            
            
            <xsl:call-template name="tContourBoreders"/>
        </xsl:for-each>
        
        <xsl:for-each select="ExistContour">
            <w:tr w:rsidR="0002329B" w:rsidTr="000C4A3C">
                <w:tblPrEx>
                    <w:tblBorders>
                        <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                        <w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                        <w:bottom w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                        <w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                        <w:insideH w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                        <w:insideV w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                    </w:tblBorders>
                    <w:tblCellMar>
                        <w:top w:w="0" w:type="dxa"/>
                        <w:bottom w:w="0" w:type="dxa"/>
                    </w:tblCellMar>
                </w:tblPrEx>
                <w:trPr>
                    <w:cantSplit/>
                    <w:trHeight w:val="340"/>
                </w:trPr>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="9670" w:type="dxa"/>
                        <w:gridSpan w:val="26"/>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p w:rsidR="0002329B" w:rsidRPr="0002329B" w:rsidRDefault="0002329B">
                        <w:pPr>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:sz w:val="21"/>
                                <w:szCs w:val="21"/>
                                <w:lang w:val="en-US"/>
                            </w:rPr>
                        </w:pPr>
                        <w:proofErr w:type="spellStart"/>
                        <w:r>
                            <w:rPr>
                                <w:sz w:val="21"/>
                                <w:szCs w:val="21"/>
                                <w:lang w:val="en-US"/>
                            </w:rPr>
                            <w:t>  <xsl:value-of select="@Number_Record"/>   </w:t>
                        </w:r>
                        <w:proofErr w:type="spellEnd"/>
                    </w:p>
                </w:tc>
            </w:tr>
            
            <xsl:call-template name="tContourBoreders"/>
        </xsl:for-each>
        <xsl:for-each select="DeleteAllBorder">
            <w:tr w:rsidR="0002329B" w:rsidTr="000C4A3C">
                <w:tblPrEx>
                    <w:tblBorders>
                        <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                        <w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                        <w:bottom w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                        <w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                        <w:insideH w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                        <w:insideV w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                    </w:tblBorders>
                    <w:tblCellMar>
                        <w:top w:w="0" w:type="dxa"/>
                        <w:bottom w:w="0" w:type="dxa"/>
                    </w:tblCellMar>
                </w:tblPrEx>
                <w:trPr>
                    <w:cantSplit/>
                    <w:trHeight w:val="340"/>
                </w:trPr>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="9670" w:type="dxa"/>
                        <w:gridSpan w:val="26"/>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p w:rsidR="0002329B" w:rsidRPr="0002329B" w:rsidRDefault="0002329B">
                        <w:pPr>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:sz w:val="21"/>
                                <w:szCs w:val="21"/>
                                <w:lang w:val="en-US"/>
                            </w:rPr>
                        </w:pPr>
                        <w:proofErr w:type="spellStart"/>
                        <w:r>
                            <w:rPr>
                                <w:sz w:val="21"/>
                                <w:szCs w:val="21"/>
                                <w:lang w:val="en-US"/>
                            </w:rPr>
                            <w:t>  <xsl:value-of select="@Number_Record"/>   </w:t>
                        </w:r>
                        <w:proofErr w:type="spellEnd"/>
                    </w:p>
                </w:tc>
            </w:tr>
          <!--  <xsl:for-each select="OldOrdinate">
                <xsl:call-template name="tOrdinate"/>
            </xsl:for-each>-->
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="SubParcels" mode="tExistParcel">
      
						
           <xsl:apply-templates select="FormSubParcel" mode="tExistParcel"/>
            <xsl:apply-templates select="ExistSubParcel" mode="tExistParcel"/>
            <xsl:apply-templates select="InvariableSubParcel" mode="tExistParcel"/>
    </xsl:template>
    
    <!--Образуемая часть участка-->
    <xsl:template match="FormSubParcel" mode="tExistParcel">
           <w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
								<w:trHeight w:val="292"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="9670" w:type="dxa"/>
									<w:gridSpan w:val="26"/>
									<w:tcBorders>
										<w:top w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>3. Сведения о местоположении границ части уточняемого земельного участка</w:t>
									</w:r>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
								<w:trHeight w:val="275"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="4158" w:type="dxa"/>
									<w:gridSpan w:val="13"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="nil"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:ind w:left="85" w:right="57"/>
										<w:jc w:val="right"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:proofErr w:type="gramStart"/>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>с</w:t>
									</w:r>
									<w:proofErr w:type="gramEnd"/>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t xml:space="preserve"> кадастровым номером</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2256" w:type="dxa"/>
									<w:gridSpan w:val="7"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="nil"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRPr="0002329B" w:rsidRDefault="0002329B">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
									</w:pPr>
									<w:proofErr w:type="spellStart"/>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
										<w:t><xsl:value-of select="parent::node()/parent::node()/@CadastralNumber"/></w:t>
									</w:r>
									<w:proofErr w:type="spellEnd"/>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="3256" w:type="dxa"/>
									<w:gridSpan w:val="6"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="9670" w:type="dxa"/>
									<w:gridSpan w:val="26"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:rPr>
											<w:sz w:val="9"/>
											<w:szCs w:val="9"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
								<w:trHeight w:val="275"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="3845" w:type="dxa"/>
									<w:gridSpan w:val="10"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="nil"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:ind w:left="85"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>Учетный номер или обозначение части</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="4878" w:type="dxa"/>
									<w:gridSpan w:val="15"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="nil"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRPr="0002329B" w:rsidRDefault="0002329B">
									<w:pPr>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
									</w:pPr>
									<w:proofErr w:type="spellStart"/>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
									    <w:t> <xsl:value-of select="@Definition"/></w:t>
									</w:r>
									<w:proofErr w:type="spellEnd"/>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="947" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="9670" w:type="dxa"/>
									<w:gridSpan w:val="26"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:rPr>
											<w:sz w:val="6"/>
											<w:szCs w:val="6"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
								<w:trHeight w:val="369"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2025" w:type="dxa"/>
									<w:gridSpan w:val="4"/>
									<w:vMerge w:val="restart"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>Обозначение характерных точек границ</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="3225" w:type="dxa"/>
									<w:gridSpan w:val="8"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>Координаты, м</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1696" w:type="dxa"/>
									<w:gridSpan w:val="9"/>
									<w:vMerge w:val="restart"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t xml:space="preserve">Средняя </w:t>
									</w:r>
									<w:proofErr w:type="spellStart"/>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>квадратическая</w:t>
									</w:r>
									<w:proofErr w:type="spellEnd"/>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t xml:space="preserve"> погрешность положения характерной точки</w:t>
									</w:r>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:br/>
										<w:t>(</w:t>
									</w:r>
									<w:proofErr w:type="spellStart"/>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>М</w:t>
									</w:r>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
											<w:vertAlign w:val="subscript"/>
										</w:rPr>
										<w:t>t</w:t>
									</w:r>
									<w:proofErr w:type="spellEnd"/>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>), м</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2724" w:type="dxa"/>
									<w:gridSpan w:val="5"/>
									<w:vMerge w:val="restart"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>Описание закрепления</w:t>
									</w:r>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:br/>
										<w:t>точки</w:t>
									</w:r>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
								<w:trHeight w:val="369"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2025" w:type="dxa"/>
									<w:gridSpan w:val="4"/>
									<w:vMerge/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1524" w:type="dxa"/>
									<w:gridSpan w:val="3"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>Х</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1701" w:type="dxa"/>
									<w:gridSpan w:val="5"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
										<w:t>Y</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1696" w:type="dxa"/>
									<w:gridSpan w:val="9"/>
									<w:vMerge/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2724" w:type="dxa"/>
									<w:gridSpan w:val="5"/>
									<w:vMerge/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
								<w:trHeight w:val="340"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2025" w:type="dxa"/>
									<w:gridSpan w:val="4"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>1</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1524" w:type="dxa"/>
									<w:gridSpan w:val="3"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>2</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1701" w:type="dxa"/>
									<w:gridSpan w:val="5"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>3</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1696" w:type="dxa"/>
									<w:gridSpan w:val="9"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>4</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2724" w:type="dxa"/>
									<w:gridSpan w:val="5"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>5</w:t>
									</w:r>
								</w:p>
							</w:tc>
						</w:tr>
         
            <xsl:call-template name="tSubParcel"/>
    </xsl:template>
    <!--Существующая (уточняемая, изменяемая ЧЗУ)-->
    <xsl:template match="ExistSubParcel" mode="tExistParcel">
        					<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
								<w:trHeight w:val="275"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="4158" w:type="dxa"/>
									<w:gridSpan w:val="13"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="nil"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:ind w:left="85" w:right="57"/>
										<w:jc w:val="right"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:proofErr w:type="gramStart"/>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>с</w:t>
									</w:r>
									<w:proofErr w:type="gramEnd"/>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t xml:space="preserve"> кадастровым номером</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2256" w:type="dxa"/>
									<w:gridSpan w:val="7"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="nil"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRPr="0002329B" w:rsidRDefault="0002329B">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
									</w:pPr>
									<w:proofErr w:type="spellStart"/>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
										<w:t><xsl:value-of select="parent::node()/parent::node()/@CadastralNumber"/></w:t>
									</w:r>
									<w:proofErr w:type="spellEnd"/>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="3256" w:type="dxa"/>
									<w:gridSpan w:val="6"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="9670" w:type="dxa"/>
									<w:gridSpan w:val="26"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:rPr>
											<w:sz w:val="9"/>
											<w:szCs w:val="9"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
								<w:trHeight w:val="275"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="3845" w:type="dxa"/>
									<w:gridSpan w:val="10"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="nil"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:ind w:left="85"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>Учетный номер или обозначение части</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="4878" w:type="dxa"/>
									<w:gridSpan w:val="15"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="nil"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRPr="0002329B" w:rsidRDefault="0002329B">
									<w:pPr>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
									</w:pPr>
									<w:proofErr w:type="spellStart"/>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
									    <w:t> <xsl:value-of select="@Number_Record"/></w:t>
									</w:r>
									<w:proofErr w:type="spellEnd"/>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="947" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="9670" w:type="dxa"/>
									<w:gridSpan w:val="26"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:rPr>
											<w:sz w:val="6"/>
											<w:szCs w:val="6"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
								<w:trHeight w:val="369"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2025" w:type="dxa"/>
									<w:gridSpan w:val="4"/>
									<w:vMerge w:val="restart"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>Обозначение характерных точек границ</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="3225" w:type="dxa"/>
									<w:gridSpan w:val="8"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>Координаты, м</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1696" w:type="dxa"/>
									<w:gridSpan w:val="9"/>
									<w:vMerge w:val="restart"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t xml:space="preserve">Средняя </w:t>
									</w:r>
									<w:proofErr w:type="spellStart"/>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>квадратическая</w:t>
									</w:r>
									<w:proofErr w:type="spellEnd"/>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t xml:space="preserve"> погрешность положения характерной точки</w:t>
									</w:r>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:br/>
										<w:t>(</w:t>
									</w:r>
									<w:proofErr w:type="spellStart"/>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>М</w:t>
									</w:r>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
											<w:vertAlign w:val="subscript"/>
										</w:rPr>
										<w:t>t</w:t>
									</w:r>
									<w:proofErr w:type="spellEnd"/>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>), м</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2724" w:type="dxa"/>
									<w:gridSpan w:val="5"/>
									<w:vMerge w:val="restart"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>Описание закрепления</w:t>
									</w:r>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:br/>
										<w:t>точки</w:t>
									</w:r>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
								<w:trHeight w:val="369"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2025" w:type="dxa"/>
									<w:gridSpan w:val="4"/>
									<w:vMerge/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1524" w:type="dxa"/>
									<w:gridSpan w:val="3"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>Х</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1701" w:type="dxa"/>
									<w:gridSpan w:val="5"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
										<w:t>Y</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1696" w:type="dxa"/>
									<w:gridSpan w:val="9"/>
									<w:vMerge/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2724" w:type="dxa"/>
									<w:gridSpan w:val="5"/>
									<w:vMerge/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
								<w:trHeight w:val="340"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2025" w:type="dxa"/>
									<w:gridSpan w:val="4"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>1</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1524" w:type="dxa"/>
									<w:gridSpan w:val="3"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>2</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1701" w:type="dxa"/>
									<w:gridSpan w:val="5"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>3</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1696" w:type="dxa"/>
									<w:gridSpan w:val="9"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>4</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2724" w:type="dxa"/>
									<w:gridSpan w:val="5"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>5</w:t>
									</w:r>
								</w:p>
							</w:tc>
						</w:tr>
         
      
            <xsl:call-template name="tSubParcel"/>
    </xsl:template>
    <!--Существующая (неизменяемая) часть участка-->
    <xsl:template match="InvariableSubParcel" mode="tExistParcel">
                					<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
								<w:trHeight w:val="275"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="4158" w:type="dxa"/>
									<w:gridSpan w:val="13"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="nil"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:ind w:left="85" w:right="57"/>
										<w:jc w:val="right"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:proofErr w:type="gramStart"/>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>с</w:t>
									</w:r>
									<w:proofErr w:type="gramEnd"/>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t xml:space="preserve"> кадастровым номером</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2256" w:type="dxa"/>
									<w:gridSpan w:val="7"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="nil"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRPr="0002329B" w:rsidRDefault="0002329B">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
									</w:pPr>
									<w:proofErr w:type="spellStart"/>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
										<w:t><xsl:value-of select="parent::node()/parent::node()/@CadastralNumber"/></w:t>
									</w:r>
									<w:proofErr w:type="spellEnd"/>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="3256" w:type="dxa"/>
									<w:gridSpan w:val="6"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="9670" w:type="dxa"/>
									<w:gridSpan w:val="26"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:rPr>
											<w:sz w:val="9"/>
											<w:szCs w:val="9"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
								<w:trHeight w:val="275"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="3845" w:type="dxa"/>
									<w:gridSpan w:val="10"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="nil"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:ind w:left="85"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>Учетный номер или обозначение части</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="4878" w:type="dxa"/>
									<w:gridSpan w:val="15"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="nil"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRPr="0002329B" w:rsidRDefault="0002329B">
									<w:pPr>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
									</w:pPr>
									<w:proofErr w:type="spellStart"/>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
									    <w:t> <xsl:value-of select="@Definition"/></w:t>
									</w:r>
									<w:proofErr w:type="spellEnd"/>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="947" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="9670" w:type="dxa"/>
									<w:gridSpan w:val="26"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:rPr>
											<w:sz w:val="6"/>
											<w:szCs w:val="6"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
								<w:trHeight w:val="369"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2025" w:type="dxa"/>
									<w:gridSpan w:val="4"/>
									<w:vMerge w:val="restart"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>Обозначение характерных точек границ</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="3225" w:type="dxa"/>
									<w:gridSpan w:val="8"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>Координаты, м</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1696" w:type="dxa"/>
									<w:gridSpan w:val="9"/>
									<w:vMerge w:val="restart"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t xml:space="preserve">Средняя </w:t>
									</w:r>
									<w:proofErr w:type="spellStart"/>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>квадратическая</w:t>
									</w:r>
									<w:proofErr w:type="spellEnd"/>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t xml:space="preserve"> погрешность положения характерной точки</w:t>
									</w:r>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:br/>
										<w:t>(</w:t>
									</w:r>
									<w:proofErr w:type="spellStart"/>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>М</w:t>
									</w:r>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
											<w:vertAlign w:val="subscript"/>
										</w:rPr>
										<w:t>t</w:t>
									</w:r>
									<w:proofErr w:type="spellEnd"/>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>), м</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2724" w:type="dxa"/>
									<w:gridSpan w:val="5"/>
									<w:vMerge w:val="restart"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>Описание закрепления</w:t>
									</w:r>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:br/>
										<w:t>точки</w:t>
									</w:r>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
								<w:trHeight w:val="369"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2025" w:type="dxa"/>
									<w:gridSpan w:val="4"/>
									<w:vMerge/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1524" w:type="dxa"/>
									<w:gridSpan w:val="3"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>Х</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1701" w:type="dxa"/>
									<w:gridSpan w:val="5"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
										<w:t>Y</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1696" w:type="dxa"/>
									<w:gridSpan w:val="9"/>
									<w:vMerge/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2724" w:type="dxa"/>
									<w:gridSpan w:val="5"/>
									<w:vMerge/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tblPrEx>
								<w:tblCellMar>
									<w:top w:w="0" w:type="dxa"/>
									<w:bottom w:w="0" w:type="dxa"/>
								</w:tblCellMar>
							</w:tblPrEx>
							<w:trPr>
								<w:cantSplit/>
								<w:trHeight w:val="340"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2025" w:type="dxa"/>
									<w:gridSpan w:val="4"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>1</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1524" w:type="dxa"/>
									<w:gridSpan w:val="3"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>2</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1701" w:type="dxa"/>
									<w:gridSpan w:val="5"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>3</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1696" w:type="dxa"/>
									<w:gridSpan w:val="9"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>4</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2724" w:type="dxa"/>
									<w:gridSpan w:val="5"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>5</w:t>
									</w:r>
								</w:p>
							</w:tc>
						</w:tr>
         
      
             <xsl:if test="count(Contours/child::*) != 0">
                   <xsl:for-each select="Contours/Contour">
                            <xsl:value-of select="@Number"/>
                          <!--  <xsl:apply-templates select="Area" mode="tArea_Contour"/>-->
                    </xsl:for-each>
            </xsl:if>
    </xsl:template>

    <xsl:template match="RelatedParcels">
        <xsl:if test="ParcelNeighbours">
         
              <xsl:for-each select="ParcelNeighbours">
                  <w:tr w:rsidR="007E23C6" w:rsidTr="0002329B">
                      <w:tblPrEx>
                          <w:tblCellMar>
                              <w:top w:w="0" w:type="dxa"/>
                              <w:bottom w:w="0" w:type="dxa"/>
                          </w:tblCellMar>
                      </w:tblPrEx>
                      <w:trPr>
                          <w:trHeight w:val="369"/>
                      </w:trPr>
                      <w:tc>
                          <w:tcPr>
                              <w:tcW w:w="2863" w:type="dxa"/>
                              <w:gridSpan w:val="5"/>
                              <w:tcBorders>
                                  <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                  <w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                                	<w:bottom w:val="single" w:sz="6" w:space="0" w:color="auto"/>
                                  <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                              </w:tcBorders>
                              <w:vAlign w:val="center"/>
                          </w:tcPr>
                          <w:p w:rsidR="007E23C6" w:rsidRPr="0002329B" w:rsidRDefault="0002329B">
                              <w:pPr>
                                  <w:jc w:val="center"/>
                                  <w:rPr>
                                      <w:sz w:val="21"/>
                                      <w:szCs w:val="21"/>
                                      <w:lang w:val="en-US"/>
                                  </w:rPr>
                              </w:pPr>
                              <w:proofErr w:type="spellStart"/>
                              <w:r>
                                  <w:rPr>
                                      <w:sz w:val="21"/>
                                      <w:szCs w:val="21"/>
                                      <w:lang w:val="en-US"/>
                                  </w:rPr>
                                  <w:t> <xsl:if test="Definition != ''">		
                                      <xsl:value-of select="normalize-space(Definition)"/>
                                  </xsl:if></w:t>
                              </w:r>
                              <w:proofErr w:type="spellEnd"/>
                          </w:p>
                      </w:tc>
                      <w:tc>
                          <w:tcPr>
                              <w:tcW w:w="3402" w:type="dxa"/>
                              <w:gridSpan w:val="14"/>
                              <w:tcBorders>
                                  <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                  <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                              	<w:bottom w:val="single" w:sz="6" w:space="0" w:color="auto"/>
                                  <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                              </w:tcBorders>
                              <w:vAlign w:val="center"/>
                          </w:tcPr>
                          <w:p w:rsidR="007E23C6" w:rsidRPr="0002329B" w:rsidRDefault="0002329B" w:rsidP="0002329B">
                              <w:pPr>
                                  <w:ind w:left="57" w:right="57"/>
                                  <w:jc w:val="center"/>
                                  <w:rPr>
                                      <w:sz w:val="21"/>
                                      <w:szCs w:val="21"/>
                                      <w:lang w:val="en-US"/>
                                  </w:rPr>
                              </w:pPr>
                              <w:proofErr w:type="spellStart"/>
                              <w:r>
                                  <w:rPr>
                                      <w:sz w:val="21"/>
                                      <w:szCs w:val="21"/>
                                      <w:lang w:val="en-US"/>
                                  </w:rPr>
                                  <w:t>  <xsl:for-each select="ParcelNeighbour">
                                      <xsl:if test="Cadastral_Number!=''">
                                          <xsl:value-of select="normalize-space(Cadastral_Number)"/>
                                      </xsl:if>
                                     <xsl:text>
                                     	
                                     </xsl:text> 
                                  </xsl:for-each></w:t>
                              </w:r>
                              <w:proofErr w:type="spellEnd"/>
                          </w:p>
                      </w:tc>
                      <w:tc>
                          <w:tcPr>
                              <w:tcW w:w="3405" w:type="dxa"/>
                              <w:gridSpan w:val="7"/>
                              <w:tcBorders>
                                  <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                  <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                              	<w:bottom w:val="single" w:sz="6" w:space="0" w:color="auto"/>
                                  <w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                              </w:tcBorders>
                              <w:vAlign w:val="center"/>
                          </w:tcPr>
                          <w:p w:rsidR="007E23C6" w:rsidRPr="0002329B" w:rsidRDefault="0002329B" w:rsidP="0002329B">
                              <w:pPr>
                                  <w:ind w:left="57" w:right="57"/>
                                  <w:jc w:val="center"/>
                                  <w:rPr>
                                      <w:sz w:val="21"/>
                                      <w:szCs w:val="21"/>
                                      <w:lang w:val="en-US"/>
                                  </w:rPr>
                              </w:pPr>
                              <w:proofErr w:type="spellStart"/>
                              <w:r>
                                  <w:rPr>
                                      <w:sz w:val="21"/>
                                      <w:szCs w:val="21"/>
                                      <w:lang w:val="en-US"/>
                                  </w:rPr>
                                  <w:t> <xsl:for-each select="ParcelNeighbour">
                                     
                                      <xsl:if test="OwnerNeighbours !='' and count(OwnerNeighbours/child::*)>0">
                                          <xsl:value-of select="OwnerNeighbours/NameRight"/> <xsl:text>
                                          	
                                          </xsl:text>
                                          <xsl:for-each select="OwnerNeighbours/OwnerNeighbour">
                                              <xsl:value-of select="NameOwner"/><xsl:text> </xsl:text>
                                              <xsl:value-of select="ContactAddress"/>
                                       
                                          </xsl:for-each>
                                      </xsl:if>
                                  </xsl:for-each></w:t>
                              </w:r>
                              <w:proofErr w:type="spellEnd"/>
                          </w:p>
                      </w:tc>
                  </w:tr>
                  
                      
                       
                </xsl:for-each>
        </xsl:if>
    </xsl:template>
    
    <!--Образуемая часть участка-->
    <xsl:template  name="tNewParcelGeneral">
        <xsl:call-template name="tSubParcelGeneral"/>
    </xsl:template>
    
        
	<xsl:template name="tOrdinatSubExist">
		<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
			<w:tblPrEx>
				<w:tblCellMar>
					<w:top w:w="0" w:type="dxa"/>
					<w:bottom w:w="0" w:type="dxa"/>
				</w:tblCellMar>
			</w:tblPrEx>
			<w:trPr>
				<w:cantSplit/>
				<w:trHeight w:val="340"/>
			</w:trPr>
			<w:tc>
				<w:tcPr>
					<w:tcW w:w="2013" w:type="dxa"/>
					<w:gridSpan w:val="3"/>
					<w:tcBorders>
						<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
						<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
						<w:bottom w:val="single" w:sz="6" w:space="0" w:color="auto"/>
						<w:right w:val="nil"/>
					</w:tcBorders>
					<w:vAlign w:val="center"/>
				</w:tcPr>
				<w:p w:rsidR="007E23C6" w:rsidRPr="00AA4A27" w:rsidRDefault="00AA4A27">
					<w:pPr>
						<w:jc w:val="center"/>
						<w:rPr>
							<w:sz w:val="21"/>
							<w:szCs w:val="21"/>
							<w:lang w:val="en-US"/>
						</w:rPr>
					</w:pPr>
					<w:proofErr w:type="spellStart"/>
					<w:r>
						<w:rPr>
							<w:sz w:val="21"/>
							<w:szCs w:val="21"/>
							<w:lang w:val="en-US"/>
						</w:rPr>
						<w:t> <xsl:value-of select="@Point_Pref"/> <xsl:value-of select="@Num_Geopoint"/></w:t>
					</w:r>
					<w:proofErr w:type="spellEnd"/>
				</w:p>
			</w:tc>
			<w:tc>
				<w:tcPr>
					<w:tcW w:w="1275" w:type="dxa"/>
					<w:gridSpan w:val="2"/>
					<w:tcBorders>
						<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
						<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
						<w:bottom w:val="single" w:sz="6" w:space="0" w:color="auto"/>
						<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
					</w:tcBorders>
					<w:vAlign w:val="center"/>
				</w:tcPr>
				<w:p w:rsidR="007E23C6" w:rsidRPr="00AA4A27" w:rsidRDefault="00AA4A27">
					<w:pPr>
						<w:jc w:val="center"/>
						<w:rPr>
							<w:sz w:val="21"/>
							<w:szCs w:val="21"/>
							<w:lang w:val="en-US"/>
						</w:rPr>
					</w:pPr>
					<w:proofErr w:type="spellStart"/>
					<w:r>
						<w:rPr>
							<w:sz w:val="21"/>
							<w:szCs w:val="21"/>
							<w:lang w:val="en-US"/>
						</w:rPr>
						<w:t> <xsl:value-of select="@X"/></w:t>	
					</w:r>
					<w:proofErr w:type="spellEnd"/>
				</w:p>
			</w:tc>
			<w:tc>
				<w:tcPr>
					<w:tcW w:w="1276" w:type="dxa"/>
					<w:gridSpan w:val="4"/>
					<w:tcBorders>
						<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
						<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
						<w:bottom w:val="single" w:sz="6" w:space="0" w:color="auto"/>
						<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
					</w:tcBorders>
					<w:vAlign w:val="center"/>
				</w:tcPr>
				<w:p w:rsidR="007E23C6" w:rsidRPr="00AA4A27" w:rsidRDefault="00AA4A27">
					<w:pPr>
						<w:jc w:val="center"/>
						<w:rPr>
							<w:sz w:val="21"/>
							<w:szCs w:val="21"/>
							<w:lang w:val="en-US"/>
						</w:rPr>
					</w:pPr>
					<w:proofErr w:type="spellStart"/>
					<w:r>
						<w:rPr>
							<w:sz w:val="21"/>
							<w:szCs w:val="21"/>
							<w:lang w:val="en-US"/>
						</w:rPr>
						<w:t><xsl:value-of select="@Y"/></w:t>
					</w:r>
					<w:proofErr w:type="spellEnd"/>
				</w:p>
			</w:tc>
			<w:tc>
				<w:tcPr>
					<w:tcW w:w="2980" w:type="dxa"/>
					<w:gridSpan w:val="6"/>
					<w:tcBorders>
						<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
						<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
						<w:bottom w:val="single" w:sz="6" w:space="0" w:color="auto"/>
						<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
					</w:tcBorders>
					<w:vAlign w:val="center"/>
				</w:tcPr>
				<w:p w:rsidR="007E23C6" w:rsidRPr="00AA4A27" w:rsidRDefault="00AA4A27">
					<w:pPr>
						<w:jc w:val="center"/>
						<w:rPr>
							<w:sz w:val="21"/>
							<w:szCs w:val="21"/>
							<w:lang w:val="en-US"/>
						</w:rPr>
					</w:pPr>
					<w:proofErr w:type="spellStart"/>
					<w:r>
						<w:rPr>
							<w:sz w:val="21"/>
							<w:szCs w:val="21"/>
							<w:lang w:val="en-US"/>
						</w:rPr>
						<w:t> <xsl:value-of select="@Delta_Geopoint"/></w:t>
					</w:r>
					<w:proofErr w:type="spellEnd"/>
				</w:p>
			</w:tc>
			<w:tc>
				<w:tcPr>
					<w:tcW w:w="2126" w:type="dxa"/>
					<w:gridSpan w:val="2"/>
					<w:tcBorders>
						<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
						<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
						<w:bottom w:val="single" w:sz="6" w:space="0" w:color="auto"/>
						<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
					</w:tcBorders>
					<w:vAlign w:val="center"/>
				</w:tcPr>
				<w:p w:rsidR="007E23C6" w:rsidRPr="00AA4A27" w:rsidRDefault="00AA4A27">
					<w:pPr>
						<w:jc w:val="center"/>
						<w:rPr>
							<w:sz w:val="21"/>
							<w:szCs w:val="21"/>
							<w:lang w:val="en-US"/>
						</w:rPr>
					</w:pPr>
					<w:proofErr w:type="spellStart"/>
					<w:r>
						<w:rPr>
							<w:sz w:val="21"/>
							<w:szCs w:val="21"/>
							<w:lang w:val="en-US"/>
						</w:rPr>
						<w:t><xsl:value-of select="@Geopoint_Zacrep"/></w:t>
					</w:r>
					<w:proofErr w:type="spellEnd"/>
				</w:p>
			</w:tc>
		</w:tr>
		
	</xsl:template>
	<xsl:template name="tSubParcel">
		<xsl:choose>
			<xsl:when test="Entity_Spatial">
				<xsl:apply-templates select="Entity_Spatial" mode="tSubParcel"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:apply-templates select="Contours" mode="tSubParcel"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template match="Contours" mode="tSubParcel">
		<xsl:for-each select="Contour">
			<w:tr w:rsidR="0002329B" w:rsidTr="000C4A3C">
				<w:tblPrEx>
					<w:tblBorders>
						<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
						<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
						<w:bottom w:val="double" w:sz="6" w:space="0" w:color="auto"/>
						<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
						<w:insideH w:val="single" w:sz="4" w:space="0" w:color="auto"/>
						<w:insideV w:val="single" w:sz="4" w:space="0" w:color="auto"/>
					</w:tblBorders>
					<w:tblCellMar>
						<w:top w:w="0" w:type="dxa"/>
						<w:bottom w:w="0" w:type="dxa"/>
					</w:tblCellMar>
				</w:tblPrEx>
				<w:trPr>
					<w:cantSplit/>
					<w:trHeight w:val="340"/>
				</w:trPr>
				<w:tc>
					<w:tcPr>
						<w:tcW w:w="9670" w:type="dxa"/>
						<w:gridSpan w:val="26"/>
						<w:vAlign w:val="center"/>
					</w:tcPr>
					<w:p w:rsidR="0002329B" w:rsidRPr="0002329B" w:rsidRDefault="0002329B">
						<w:pPr>
							<w:jc w:val="center"/>
							<w:rPr>
								<w:sz w:val="21"/>
								<w:szCs w:val="21"/>
								<w:lang w:val="en-US"/>
							</w:rPr>
						</w:pPr>
						<w:proofErr w:type="spellStart"/>
						<w:r>
							<w:rPr>
								<w:sz w:val="21"/>
								<w:szCs w:val="21"/>
								<w:lang w:val="en-US"/>
							</w:rPr>
							<w:t>  <xsl:value-of select="@Number"/>    </w:t>
						</w:r>
						<w:proofErr w:type="spellEnd"/>
					</w:p>
				</w:tc>
			</w:tr>
			
			<xsl:apply-templates select="Entity_Spatial" mode="tSubParcel"/>
		</xsl:for-each>
	</xsl:template>
	
	<xsl:template name="tContour">
		<xsl:choose>
			<xsl:when test="name(parent::node()/parent::node())='ExistParcel'">
				<xsl:apply-templates select="Entity_Spatial" mode="ExistParcel"/>	
			</xsl:when>
			<xsl:otherwise>
				<xsl:apply-templates select="Entity_Spatial"/>
			</xsl:otherwise>
		</xsl:choose> 
	</xsl:template>
	
	<xsl:template match="Entity_Spatial" mode="tSubParcel">
		<xsl:for-each select="Spatial_Element">
			<xsl:call-template name="tSPATIAL_ELEMENT_OLD_NEW"/>
		</xsl:for-each>
	</xsl:template>
	<xsl:template name="tSPATIAL_ELEMENT_OLD_NEW">
		<xsl:for-each select="Spelement_Unit">
			<xsl:call-template name="tSPELEMENT_UNIT_OLD_NEW"/>
		</xsl:for-each>
		<xsl:if test="position() != last()">
			<w:tr w:rsidR="00AA4A27" w:rsidTr="00D94FC5">
				<w:tblPrEx>
					<w:tblCellMar>
						<w:top w:w="0" w:type="dxa"/>
						<w:bottom w:w="0" w:type="dxa"/>
					</w:tblCellMar>
				</w:tblPrEx>
				<w:trPr>
					<w:cantSplit/>
					<w:trHeight w:val="340"/>
				</w:trPr>
				<w:tc>
					<w:tcPr>
						<w:tcW w:w="9670" w:type="dxa"/>
						<w:gridSpan w:val="17"/>
						<w:tcBorders>
							<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
							<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
							<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
							<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
						</w:tcBorders>
						<w:vAlign w:val="center"/>
					</w:tcPr>
					<w:p w:rsidR="00AA4A27" w:rsidRPr="00AA4A27" w:rsidRDefault="00AA4A27">
						<w:pPr>
							<w:jc w:val="center"/>
							<w:rPr>
								<w:sz w:val="21"/>
								<w:szCs w:val="21"/>
								<w:lang w:val="en-US"/>
							</w:rPr>
						</w:pPr>
						<w:proofErr w:type="spellStart"/>
						<w:r>
							<w:rPr>
								<w:sz w:val="21"/>
								<w:szCs w:val="21"/>
								<w:lang w:val="en-US"/>
							</w:rPr>
							<w:t>     </w:t>
						</w:r>
						<w:proofErr w:type="spellEnd"/>
					</w:p>
				</w:tc>
			</w:tr>
		</xsl:if>
	</xsl:template>
	<!--Части элементов новых и существующих контуров (точка)-->
	<xsl:template name="tSPELEMENT_UNIT_OLD_NEW">
		<xsl:apply-templates select="NewOrdinate" mode="tOrdinate"/>
	</xsl:template>
	<xsl:template match="NewOrdinate" mode="tOrdinate">
		<xsl:if test="(@X!='')and(@Y!='')">
			<xsl:call-template name="tOrdinate"/>
		</xsl:if>
	</xsl:template>
    <xsl:template name="tContourBoreders">
        <xsl:apply-templates select="Entity_Spatial" mode="Borders"/>
    </xsl:template>
    
   <xsl:template name="tOrdinateExistParcel">
        
        <w:tr w:rsidR="006F4D38" w:rsidRPr="00642347" w:rsidTr="00A56FB2">
            <w:trPr>
                <w:trHeight w:hRule="exact" w:val="240"/>
            </w:trPr>
            <w:tc>
                <w:tcPr>
                    <w:tcW w:w="2798" w:type="dxa"/>
                    <w:gridSpan w:val="3"/>
                    <w:tcBorders>
                        <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                        <w:left w:val="double" w:sz="4" w:space="0" w:color="auto"/>
                        <w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                        <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                    </w:tcBorders>
                    <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                    <w:vAlign w:val="center"/>
                </w:tcPr>
                <w:p w:rsidR="006F4D38" w:rsidRPr="00642347" w:rsidRDefault="006F4D38" w:rsidP="00A56FB2">
                    <w:pPr>
                        <w:jc w:val="center"/>
                        <w:rPr>
                            <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                            <w:i/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:pPr>
                    <w:r w:rsidRPr="00642347">
                        <w:rPr>
                            <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                            <w:i/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                        <w:t> <xsl:value-of select="@Point_Pref"/> <xsl:value-of select="@Num_Geopoint"/></w:t>
                    </w:r>
                </w:p>
            </w:tc>
            <w:tc>
                <w:tcPr>
                    <w:tcW w:w="1260" w:type="dxa"/>
                    <w:gridSpan w:val="3"/>
                    <w:tcBorders>
                        <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                        <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                        <w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                        <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                    </w:tcBorders>
                    <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                    <w:vAlign w:val="center"/>
                </w:tcPr>
                <w:p w:rsidR="006F4D38" w:rsidRPr="00642347" w:rsidRDefault="006F4D38" w:rsidP="00A56FB2">
                    <w:pPr>
                        <w:jc w:val="center"/>
                        <w:rPr>
                            <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                            <w:i/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:pPr>
                    <w:r w:rsidRPr="00642347">
                        <w:rPr>
                            <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                            <w:i/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                        <w:t> <xsl:value-of select="@X"/></w:t>
                    </w:r>
                </w:p>
            </w:tc>
            <w:tc>
                <w:tcPr>
                    <w:tcW w:w="1260" w:type="dxa"/>
                    <w:gridSpan w:val="2"/>
                    <w:tcBorders>
                        <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                        <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                        <w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                        <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                    </w:tcBorders>
                    <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                    <w:vAlign w:val="center"/>
                </w:tcPr>
                <w:p w:rsidR="006F4D38" w:rsidRPr="00642347" w:rsidRDefault="006F4D38" w:rsidP="00A56FB2">
                    <w:pPr>
                        <w:jc w:val="center"/>
                        <w:rPr>
                            <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                            <w:i/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:pPr>
                    <w:r w:rsidRPr="00642347">
                        <w:rPr>
                            <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                            <w:i/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                        <w:t><xsl:value-of select="@Y"/></w:t>
                    </w:r>
                </w:p>
            </w:tc>
            <w:tc>
                <w:tcPr>
                    <w:tcW w:w="3060" w:type="dxa"/>
                    <w:gridSpan w:val="5"/>
                    <w:tcBorders>
                        <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                        <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                        <w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                        <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                    </w:tcBorders>
                    <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                    <w:vAlign w:val="center"/>
                </w:tcPr>
                <w:p w:rsidR="006F4D38" w:rsidRPr="00642347" w:rsidRDefault="006F4D38" w:rsidP="00A56FB2">
                    <w:pPr>
                        <w:jc w:val="center"/>
                        <w:rPr>
                            <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                            <w:i/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:pPr>
                    <w:r w:rsidRPr="00642347">
                        <w:rPr>
                            <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                            <w:i/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                        <w:t> <xsl:value-of select="@Delta_Geopoint"/></w:t>
                    </w:r>
                </w:p>
            </w:tc>
            <w:tc>
                <w:tcPr>
                    <w:tcW w:w="1927" w:type="dxa"/>
                    <w:tcBorders>
                        <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                        <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                        <w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                        <w:right w:val="double" w:sz="4" w:space="0" w:color="auto"/>
                    </w:tcBorders>
                    <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                    <w:vAlign w:val="center"/>
                </w:tcPr>
                <w:p w:rsidR="006F4D38" w:rsidRPr="00642347" w:rsidRDefault="006F4D38" w:rsidP="00A56FB2">
                    <w:pPr>
                        <w:jc w:val="center"/>
                        <w:rPr>
                            <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                            <w:i/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                    </w:pPr>
                    <w:r w:rsidRPr="00642347">
                        <w:rPr>
                            <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                            <w:i/>
                            <w:sz w:val="20"/>
                        </w:rPr>
                        <w:t><xsl:value-of select="@Geopoint_Zacrep"/></w:t>
                    </w:r>
                </w:p>
            </w:tc>
        </w:tr>
    </xsl:template>
  
    
    <!--Описание части земельного участка при образовании ЗУ или уточнении границ ЗУ-->
    <xsl:template name="tSubParcelGeneral">
        <w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
            <w:tblPrEx>
                <w:tblCellMar>
                    <w:top w:w="0" w:type="dxa"/>
                    <w:bottom w:w="0" w:type="dxa"/>
                </w:tblCellMar>
            </w:tblPrEx>
            <w:trPr>
                <w:cantSplit/>
                <w:trHeight w:val="440"/>
            </w:trPr>
            <w:tc>
                <w:tcPr>
                    <w:tcW w:w="737" w:type="dxa"/>
                    <w:tcBorders>
                        <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                        <w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                    	<w:bottom w:val="single" w:sz="6" w:space="0" w:color="auto"/>
                        <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                    </w:tcBorders>
                    <w:vAlign w:val="center"/>
                </w:tcPr>
                <w:p w:rsidR="007E23C6" w:rsidRPr="0002329B" w:rsidRDefault="0002329B">
                    <w:pPr>
                        <w:jc w:val="center"/>
                        <w:rPr>
                            <w:sz w:val="21"/>
                            <w:szCs w:val="21"/>
                            <w:lang w:val="en-US"/>
                        </w:rPr>
                    </w:pPr>
                    <w:proofErr w:type="spellStart"/>
                    <w:r>
                        <w:rPr>
                            <w:sz w:val="21"/>
                            <w:szCs w:val="21"/>
                            <w:lang w:val="en-US"/>
                        </w:rPr>
                        <w:lastRenderedPageBreak/>
                        <w:t><xsl:value-of select="position()"/></w:t>
                    </w:r>
                    <w:proofErr w:type="spellEnd"/>
                </w:p>
            </w:tc>
            <w:tc>
                <w:tcPr>
                    <w:tcW w:w="2693" w:type="dxa"/>
                    <w:gridSpan w:val="7"/>
                    <w:tcBorders>
                        <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                        <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                    	<w:bottom w:val="single" w:sz="6" w:space="0" w:color="auto"/>
                        <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                    </w:tcBorders>
                    <w:vAlign w:val="center"/>
                </w:tcPr>
                <w:p w:rsidR="007E23C6" w:rsidRPr="0002329B" w:rsidRDefault="0002329B">
                    <w:pPr>
                        <w:jc w:val="center"/>
                        <w:rPr>
                            <w:sz w:val="21"/>
                            <w:szCs w:val="21"/>
                            <w:lang w:val="en-US"/>
                        </w:rPr>
                    </w:pPr>
                    <w:proofErr w:type="spellStart"/>
                    <w:r>
                        <w:rPr>
                            <w:sz w:val="21"/>
                            <w:szCs w:val="21"/>
                            <w:lang w:val="en-US"/>
                        </w:rPr>
                        <w:t>
                            <xsl:choose>
                                <xsl:when test="@Definition">
                                    <xsl:value-of select="@Definition"/>		
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="@Number_Record"/>
                                </xsl:otherwise>
                            </xsl:choose> 
                        </w:t>
                    </w:r>
                    <w:proofErr w:type="spellEnd"/>
                </w:p>
            </w:tc>
            <w:tc>
                <w:tcPr>
                    <w:tcW w:w="1276" w:type="dxa"/>
                    <w:gridSpan w:val="5"/>
                    <w:tcBorders>
                        <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                        <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                    	<w:bottom w:val="single" w:sz="6" w:space="0" w:color="auto"/>
                        <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                    </w:tcBorders>
                    <w:vAlign w:val="center"/>
                </w:tcPr>
                <w:p w:rsidR="007E23C6" w:rsidRPr="0002329B" w:rsidRDefault="0002329B">
                    <w:pPr>
                        <w:jc w:val="center"/>
                        <w:rPr>
                            <w:sz w:val="21"/>
                            <w:szCs w:val="21"/>
                            <w:lang w:val="en-US"/>
                        </w:rPr>
                    </w:pPr>
                    <w:proofErr w:type="spellStart"/>
                    <w:r>
                        <w:rPr>
                            <w:sz w:val="21"/>
                            <w:szCs w:val="21"/>
                            <w:lang w:val="en-US"/>
                        </w:rPr>
                        <w:t><xsl:value-of select="Area/Area"/></w:t>
                    </w:r>
                    <w:proofErr w:type="spellEnd"/>
                </w:p>
            </w:tc>
            <w:tc>
                <w:tcPr>
                    <w:tcW w:w="1276" w:type="dxa"/>
                    <w:gridSpan w:val="4"/>
                    <w:tcBorders>
                        <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                        <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                    	<w:bottom w:val="single" w:sz="6" w:space="0" w:color="auto"/>
                        <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                    </w:tcBorders>
                    <w:vAlign w:val="center"/>
                </w:tcPr>
                <w:p w:rsidR="007E23C6" w:rsidRPr="0002329B" w:rsidRDefault="0002329B">
                    <w:pPr>
                        <w:jc w:val="center"/>
                        <w:rPr>
                            <w:sz w:val="21"/>
                            <w:szCs w:val="21"/>
                            <w:lang w:val="en-US"/>
                        </w:rPr>
                    </w:pPr>
                    <w:proofErr w:type="spellStart"/>
                    <w:r>
                        <w:rPr>
                            <w:sz w:val="21"/>
                            <w:szCs w:val="21"/>
                            <w:lang w:val="en-US"/>
                        </w:rPr>
                        <w:t><xsl:value-of select="Area/Innccuracy"/></w:t>
                    </w:r>
                    <w:proofErr w:type="spellEnd"/>
                </w:p>
            </w:tc>
            <w:tc>
                <w:tcPr>
                    <w:tcW w:w="3688" w:type="dxa"/>
                    <w:gridSpan w:val="9"/>
                    <w:tcBorders>
                        <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                        <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                    	<w:bottom w:val="single" w:sz="6" w:space="0" w:color="auto"/>
                        <w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
                    </w:tcBorders>
                    <w:vAlign w:val="center"/>
                </w:tcPr>
                <w:p w:rsidR="007E23C6" w:rsidRPr="0002329B" w:rsidRDefault="0002329B" w:rsidP="0002329B">
                    <w:pPr>
                        <w:ind w:left="57" w:right="57"/>
                        <w:jc w:val="center"/>
                        <w:rPr>
                            <w:sz w:val="21"/>
                            <w:szCs w:val="21"/>
                            <w:lang w:val="en-US"/>
                        </w:rPr>
                    </w:pPr>
                    <w:proofErr w:type="spellStart"/>
                    <w:r>
                        <w:rPr>
                            <w:sz w:val="21"/>
                            <w:szCs w:val="21"/>
                            <w:lang w:val="en-US"/>
                        </w:rPr>
                        <w:t><xsl:value-of select="Encumbrance/Name"/></w:t>
                    </w:r>
                    <w:proofErr w:type="spellEnd"/>
                </w:p>
            </w:tc>
        </w:tr>
    </xsl:template>
    
    <!--Кадастровые или иные номера зданий, сооружений, объектов незавершенного строительства, расположенных на земельном участке-->
    <xsl:template match="Inner_CadastralNumbers">
        <xsl:if test="CadastralNumber!=''">
            <xsl:value-of select="normalize-space(CadastralNumber)"/>
        </xsl:if>
        <xsl:if test="Number != ''">
            <xsl:value-of select="normalize-space(Number)"/>
        </xsl:if>        
    </xsl:template>
   
   <xsl:template name="End">
   	  			<w:tbl>
						<w:tblPr>
							<w:tblW w:w="0" w:type="auto"/>
							<w:tblInd w:w="-544" w:type="dxa"/>
							<w:tblLayout w:type="fixed"/>
							
							<w:tblCellMar>
								<w:left w:w="28" w:type="dxa"/>
								<w:right w:w="28" w:type="dxa"/>
							</w:tblCellMar>
							<w:tblLook w:val="0000" w:firstRow="0" w:lastRow="0" w:firstColumn="0" w:lastColumn="0" w:noHBand="0" w:noVBand="0"/>
						</w:tblPr>
						<w:tblGrid>
							<w:gridCol w:w="1957"/>
							<w:gridCol w:w="573"/>
							<w:gridCol w:w="788"/>
							<w:gridCol w:w="1276"/>
							<w:gridCol w:w="112"/>
							<w:gridCol w:w="967"/>
							<w:gridCol w:w="870"/>
							<w:gridCol w:w="1361"/>
							<w:gridCol w:w="1766"/>
						</w:tblGrid>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:trPr>
								<w:trHeight w:val="440"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="9670" w:type="dxa"/>
									<w:gridSpan w:val="9"/>
									<w:tcBorders>
										<w:top w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:keepNext/>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="27"/>
											<w:szCs w:val="27"/>
										</w:rPr>
									</w:pPr>
									<w:bookmarkStart w:id="0" w:name="_GoBack"/>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="27"/>
											<w:szCs w:val="27"/>
										</w:rPr>
										<w:t>Схема геодезических построений</w:t>
									</w:r>
									<w:bookmarkEnd w:id="0"/>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:trPr>
								<w:cantSplit/>
								<w:trHeight w:val="510"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="9670" w:type="dxa"/>
									<w:gridSpan w:val="9"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:trPr>
								<w:cantSplit/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2530" w:type="dxa"/>
									<w:gridSpan w:val="2"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="nil"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:ind w:left="85" w:right="85"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>Условные обозначения:</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="7140" w:type="dxa"/>
									<w:gridSpan w:val="7"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:ind w:right="85"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:trPr>
								<w:cantSplit/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="9670" w:type="dxa"/>
									<w:gridSpan w:val="9"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:ind w:left="85" w:right="85"/>
										<w:rPr>
											<w:sz w:val="18"/>
											<w:szCs w:val="18"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:trPr>
								<w:trHeight w:val="440"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="9670" w:type="dxa"/>
									<w:gridSpan w:val="9"/>
									<w:tcBorders>
										<w:top w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:keepNext/>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="27"/>
											<w:szCs w:val="27"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="27"/>
											<w:szCs w:val="27"/>
										</w:rPr>
										<w:t>Схема расположения земельных участков</w:t>
									</w:r>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:trPr>
								<w:cantSplit/>
								<w:trHeight w:val="510"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="9670" w:type="dxa"/>
									<w:gridSpan w:val="9"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:trPr>
								<w:cantSplit/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2530" w:type="dxa"/>
									<w:gridSpan w:val="2"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="nil"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:ind w:left="85" w:right="85"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>Условные обозначения:</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="7140" w:type="dxa"/>
									<w:gridSpan w:val="7"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:ind w:right="85"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:trPr>
								<w:cantSplit/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="9670" w:type="dxa"/>
									<w:gridSpan w:val="9"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:ind w:left="85" w:right="85"/>
										<w:rPr>
											<w:sz w:val="18"/>
											<w:szCs w:val="18"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:trPr>
								<w:trHeight w:val="440"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="9670" w:type="dxa"/>
									<w:gridSpan w:val="9"/>
									<w:tcBorders>
										<w:top w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:keepNext/>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="27"/>
											<w:szCs w:val="27"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="27"/>
											<w:szCs w:val="27"/>
										</w:rPr>
										<w:t>Чертеж земельных участков и их частей</w:t>
									</w:r>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="9670" w:type="dxa"/>
									<w:gridSpan w:val="9"/>
									<w:tcBorders>
										<w:top w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:keepNext/>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:trPr>
								<w:cantSplit/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="4706" w:type="dxa"/>
									<w:gridSpan w:val="5"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="nil"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="right"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>Масштаб 1:</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="967" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="nil"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="3997" w:type="dxa"/>
									<w:gridSpan w:val="3"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:trPr>
								<w:cantSplit/>
								<w:trHeight w:val="303"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2530" w:type="dxa"/>
									<w:gridSpan w:val="2"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="nil"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:spacing w:before="120"/>
										<w:ind w:left="85" w:right="85"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>Условные обозначения:</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="7140" w:type="dxa"/>
									<w:gridSpan w:val="7"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:ind w:right="85"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:trPr>
								<w:cantSplit/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="9670" w:type="dxa"/>
									<w:gridSpan w:val="9"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:ind w:left="85" w:right="85"/>
										<w:rPr>
											<w:sz w:val="12"/>
											<w:szCs w:val="12"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:trPr>
								<w:trHeight w:val="440"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="9670" w:type="dxa"/>
									<w:gridSpan w:val="9"/>
									<w:tcBorders>
										<w:top w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:keepNext/>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="27"/>
											<w:szCs w:val="27"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="27"/>
											<w:szCs w:val="27"/>
										</w:rPr>
										<w:t>Абрисы узловых точек границ земельных участков</w:t>
									</w:r>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:trPr>
								<w:cantSplit/>
								<w:trHeight w:val="340"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1957" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="nil"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:keepNext/>
										<w:ind w:left="85"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>Обозначение точки</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1361" w:type="dxa"/>
									<w:gridSpan w:val="2"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="nil"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:keepNext/>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1276" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:keepNext/>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1949" w:type="dxa"/>
									<w:gridSpan w:val="3"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="nil"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:keepNext/>
										<w:ind w:left="85"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>Обозначение точки</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1361" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="nil"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:keepNext/>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1766" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:keepNext/>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:trPr>
								<w:cantSplit/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1957" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="nil"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:keepNext/>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="12"/>
											<w:szCs w:val="12"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1361" w:type="dxa"/>
									<w:gridSpan w:val="2"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="nil"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:keepNext/>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="12"/>
											<w:szCs w:val="12"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1276" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:keepNext/>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="12"/>
											<w:szCs w:val="12"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1949" w:type="dxa"/>
									<w:gridSpan w:val="3"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="nil"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:keepNext/>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="12"/>
											<w:szCs w:val="12"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1361" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="nil"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:keepNext/>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="12"/>
											<w:szCs w:val="12"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1766" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:keepNext/>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="12"/>
											<w:szCs w:val="12"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:trPr>
								<w:cantSplit/>
								<w:trHeight w:val="510"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1957" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="nil"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:keepNext/>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1361" w:type="dxa"/>
									<w:gridSpan w:val="2"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="nil"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:keepNext/>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1276" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:keepNext/>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1949" w:type="dxa"/>
									<w:gridSpan w:val="3"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="nil"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:keepNext/>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1361" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="nil"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:keepNext/>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1766" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:keepNext/>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:trPr>
								<w:cantSplit/>
								<w:trHeight w:val="340"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1957" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="nil"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:keepNext/>
										<w:ind w:left="85"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>Обозначение точки</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1361" w:type="dxa"/>
									<w:gridSpan w:val="2"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="nil"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:keepNext/>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1276" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:keepNext/>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1949" w:type="dxa"/>
									<w:gridSpan w:val="3"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="nil"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:keepNext/>
										<w:ind w:left="85"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>Обозначение точки</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1361" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="nil"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:keepNext/>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1766" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:keepNext/>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:trPr>
								<w:cantSplit/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1957" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="nil"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:keepNext/>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="12"/>
											<w:szCs w:val="12"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1361" w:type="dxa"/>
									<w:gridSpan w:val="2"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="nil"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:keepNext/>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="12"/>
											<w:szCs w:val="12"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1276" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:keepNext/>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="12"/>
											<w:szCs w:val="12"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1949" w:type="dxa"/>
									<w:gridSpan w:val="3"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="nil"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:keepNext/>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="12"/>
											<w:szCs w:val="12"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1361" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="nil"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:keepNext/>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="12"/>
											<w:szCs w:val="12"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1766" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:keepNext/>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="12"/>
											<w:szCs w:val="12"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6" w:rsidTr="00FC287A">
							<w:trPr>
								<w:cantSplit/>
								<w:trHeight w:val="510"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1957" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:bottom w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:right w:val="nil"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:keepNext/>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1361" w:type="dxa"/>
									<w:gridSpan w:val="2"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:right w:val="nil"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:keepNext/>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1276" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:keepNext/>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1949" w:type="dxa"/>
									<w:gridSpan w:val="3"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:right w:val="nil"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:keepNext/>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1361" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:right w:val="nil"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:keepNext/>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1766" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="double" w:sz="6" w:space="0" w:color="auto"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="auto"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:keepNext/>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
						</w:tr>
					</w:tbl>
					<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6"/>
					<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
						<w:pPr>
							<w:pageBreakBefore/>
							<w:spacing w:after="240"/>
							<w:jc w:val="center"/>
							<w:rPr>
								<w:b/>
								<w:bCs/>
								<w:sz w:val="24"/>
								<w:szCs w:val="24"/>
							</w:rPr>
						</w:pPr>
						<w:r>
							<w:rPr>
								<w:b/>
								<w:bCs/>
								<w:sz w:val="24"/>
								<w:szCs w:val="24"/>
							</w:rPr>
							<w:lastRenderedPageBreak/>
							<w:t>АКТ СОГЛАСОВАНИЯ МЕСТОПОЛОЖЕНИЯ ГРАНИЦЫ</w:t>
						</w:r>
						<w:r>
							<w:rPr>
								<w:b/>
								<w:bCs/>
								<w:sz w:val="24"/>
								<w:szCs w:val="24"/>
							</w:rPr>
							<w:br/>
							<w:t>ЗЕМЕЛЬНОГО УЧАСТКА</w:t>
						</w:r>
					</w:p>
					<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
						<w:pPr>
							<w:rPr>
								<w:b/>
								<w:bCs/>
								<w:sz w:val="24"/>
								<w:szCs w:val="24"/>
							</w:rPr>
						</w:pPr>
						<w:r>
							<w:rPr>
								<w:b/>
								<w:bCs/>
								<w:sz w:val="24"/>
								<w:szCs w:val="24"/>
							</w:rPr>
							<w:t xml:space="preserve">Кадастровый номер или обозначение земельного участка  </w:t>
						</w:r>
					</w:p>
					<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
						<w:pPr>
							<w:pBdr>
								<w:top w:val="single" w:sz="4" w:space="1" w:color="auto"/>
							</w:pBdr>
							<w:ind w:left="6379"/>
							<w:rPr>
								<w:sz w:val="2"/>
								<w:szCs w:val="2"/>
							</w:rPr>
						</w:pPr>
					</w:p>
					<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
						<w:pPr>
							<w:rPr>
								<w:b/>
								<w:bCs/>
								<w:sz w:val="24"/>
								<w:szCs w:val="24"/>
							</w:rPr>
						</w:pPr>
						<w:r>
							<w:rPr>
								<w:b/>
								<w:bCs/>
								<w:sz w:val="24"/>
								<w:szCs w:val="24"/>
							</w:rPr>
							<w:t xml:space="preserve">Площадь земельного участка  </w:t>
						</w:r>
					</w:p>
					<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
						<w:pPr>
							<w:pBdr>
								<w:top w:val="single" w:sz="4" w:space="1" w:color="auto"/>
							</w:pBdr>
							<w:ind w:left="3341"/>
							<w:rPr>
								<w:sz w:val="2"/>
								<w:szCs w:val="2"/>
							</w:rPr>
						</w:pPr>
					</w:p>
					<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
						<w:pPr>
							<w:spacing w:after="360"/>
							<w:jc w:val="both"/>
							<w:rPr>
								<w:b/>
								<w:bCs/>
								<w:i/>
								<w:iCs/>
								<w:sz w:val="24"/>
								<w:szCs w:val="24"/>
							</w:rPr>
						</w:pPr>
						<w:r>
							<w:rPr>
								<w:b/>
								<w:bCs/>
								<w:i/>
								<w:iCs/>
								<w:sz w:val="24"/>
								<w:szCs w:val="24"/>
							</w:rPr>
							<w:t>Лица, персональные данные которых содержатся в настоящем Акте согласования местоположения границ, подтверждают свое согласие, а также согласие представляемого ими лица на обработку персональных данных (сбор, систематизацию, накопление, хранение, уточнение (обновление, изменение), использование, распространение (в том числе передачу), обезличивание, блокирование, уничтожение персональных данных, а также иных действий, необходимых для обработки персональных данных в рамках предоставления органами кадастрового учета в соответствии с законодательством Российской Федерации государственных услуг), в том числе в автоматизированном режиме, включая принятие решений на их основе органом кадастрового учета в целях предоставления государственной услуги.</w:t>
						</w:r>
					</w:p>
					<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
						<w:pPr>
							<w:spacing w:after="240"/>
							<w:jc w:val="center"/>
							<w:rPr>
								<w:b/>
								<w:bCs/>
								<w:sz w:val="24"/>
								<w:szCs w:val="24"/>
							</w:rPr>
						</w:pPr>
						<w:r>
							<w:rPr>
								<w:b/>
								<w:bCs/>
								<w:sz w:val="24"/>
								<w:szCs w:val="24"/>
							</w:rPr>
							<w:t>Местоположение границы земельного участка согласовано:</w:t>
						</w:r>
					</w:p>
					<w:tbl>
						<w:tblPr>
							<w:tblW w:w="0" w:type="auto"/>
							
							<w:tblBorders>
								<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
								<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
								<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
								<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
								<w:insideH w:val="single" w:sz="4" w:space="0" w:color="auto"/>
								<w:insideV w:val="single" w:sz="4" w:space="0" w:color="auto"/>
							</w:tblBorders>
							<w:tblLayout w:type="fixed"/>
							<w:tblCellMar>
								<w:left w:w="28" w:type="dxa"/>
								<w:right w:w="28" w:type="dxa"/>
							</w:tblCellMar>
							<w:tblLook w:val="0000" w:firstRow="0" w:lastRow="0" w:firstColumn="0" w:lastColumn="0" w:noHBand="0" w:noVBand="0"/>
						</w:tblPr>
						<w:tblGrid>
							<w:gridCol w:w="1304"/>
							<w:gridCol w:w="1531"/>
							<w:gridCol w:w="2381"/>
							<w:gridCol w:w="2098"/>
							<w:gridCol w:w="1191"/>
							<w:gridCol w:w="1191"/>
						</w:tblGrid>
						<w:tr w:rsidR="007E23C6">
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1304" w:type="dxa"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="22"/>
											<w:szCs w:val="22"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="22"/>
											<w:szCs w:val="22"/>
										</w:rPr>
										<w:t>Обозна</w:t>
									</w:r>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="22"/>
											<w:szCs w:val="22"/>
										</w:rPr>
										<w:softHyphen/>
										<w:t>чение харак</w:t>
									</w:r>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="22"/>
											<w:szCs w:val="22"/>
										</w:rPr>
										<w:softHyphen/>
										<w:t>тер</w:t>
									</w:r>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="22"/>
											<w:szCs w:val="22"/>
										</w:rPr>
										<w:softHyphen/>
										<w:t>ной точки или части границы</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1531" w:type="dxa"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="22"/>
											<w:szCs w:val="22"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="22"/>
											<w:szCs w:val="22"/>
										</w:rPr>
										<w:t>Кадаст</w:t>
									</w:r>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="22"/>
											<w:szCs w:val="22"/>
										</w:rPr>
										<w:softHyphen/>
										<w:t>ровый номер смежного земельного участка</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2381" w:type="dxa"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="22"/>
											<w:szCs w:val="22"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="22"/>
											<w:szCs w:val="22"/>
										</w:rPr>
										<w:t>Фамилия и инициалы право</w:t>
									</w:r>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="22"/>
											<w:szCs w:val="22"/>
										</w:rPr>
										<w:softHyphen/>
										<w:t>облада</w:t>
									</w:r>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="22"/>
											<w:szCs w:val="22"/>
										</w:rPr>
										<w:softHyphen/>
										<w:t>теля или его предста</w:t>
									</w:r>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="22"/>
											<w:szCs w:val="22"/>
										</w:rPr>
										<w:softHyphen/>
										<w:t>вителя, реквизиты документа, удостове</w:t>
									</w:r>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="22"/>
											<w:szCs w:val="22"/>
										</w:rPr>
										<w:softHyphen/>
										<w:t>ряющего личность</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2098" w:type="dxa"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="22"/>
											<w:szCs w:val="22"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="22"/>
											<w:szCs w:val="22"/>
										</w:rPr>
										<w:t>Реквизиты доку</w:t>
									</w:r>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="22"/>
											<w:szCs w:val="22"/>
										</w:rPr>
										<w:softHyphen/>
										<w:t>ментов, подтверж</w:t>
									</w:r>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="22"/>
											<w:szCs w:val="22"/>
										</w:rPr>
										<w:softHyphen/>
										<w:t>дающих полномочия предста</w:t>
									</w:r>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="22"/>
											<w:szCs w:val="22"/>
										</w:rPr>
										<w:softHyphen/>
										<w:t>вителей, участвующих в согла</w:t>
									</w:r>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="22"/>
											<w:szCs w:val="22"/>
										</w:rPr>
										<w:softHyphen/>
										<w:t>совании</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1191" w:type="dxa"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="22"/>
											<w:szCs w:val="22"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="22"/>
											<w:szCs w:val="22"/>
										</w:rPr>
										<w:t>Подпись и дата</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1191" w:type="dxa"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="22"/>
											<w:szCs w:val="22"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="22"/>
											<w:szCs w:val="22"/>
										</w:rPr>
										<w:t>Способ и дата изве</w:t>
									</w:r>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="22"/>
											<w:szCs w:val="22"/>
										</w:rPr>
										<w:softHyphen/>
										<w:t>щения</w:t>
									</w:r>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6">
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1304" w:type="dxa"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="22"/>
											<w:szCs w:val="22"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="22"/>
											<w:szCs w:val="22"/>
										</w:rPr>
										<w:t>1</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1531" w:type="dxa"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="22"/>
											<w:szCs w:val="22"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="22"/>
											<w:szCs w:val="22"/>
										</w:rPr>
										<w:t>2</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2381" w:type="dxa"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="22"/>
											<w:szCs w:val="22"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="22"/>
											<w:szCs w:val="22"/>
										</w:rPr>
										<w:t>3</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2098" w:type="dxa"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="22"/>
											<w:szCs w:val="22"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="22"/>
											<w:szCs w:val="22"/>
										</w:rPr>
										<w:t>4</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1191" w:type="dxa"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="22"/>
											<w:szCs w:val="22"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="22"/>
											<w:szCs w:val="22"/>
										</w:rPr>
										<w:t>5</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1191" w:type="dxa"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="22"/>
											<w:szCs w:val="22"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="22"/>
											<w:szCs w:val="22"/>
										</w:rPr>
										<w:t>6</w:t>
									</w:r>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6">
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1304" w:type="dxa"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="22"/>
											<w:szCs w:val="22"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1531" w:type="dxa"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="22"/>
											<w:szCs w:val="22"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2381" w:type="dxa"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:rPr>
											<w:sz w:val="22"/>
											<w:szCs w:val="22"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2098" w:type="dxa"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:rPr>
											<w:sz w:val="22"/>
											<w:szCs w:val="22"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1191" w:type="dxa"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="22"/>
											<w:szCs w:val="22"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1191" w:type="dxa"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:rPr>
											<w:sz w:val="22"/>
											<w:szCs w:val="22"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6">
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1304" w:type="dxa"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="22"/>
											<w:szCs w:val="22"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1531" w:type="dxa"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="22"/>
											<w:szCs w:val="22"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2381" w:type="dxa"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:rPr>
											<w:sz w:val="22"/>
											<w:szCs w:val="22"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2098" w:type="dxa"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:rPr>
											<w:sz w:val="22"/>
											<w:szCs w:val="22"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1191" w:type="dxa"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="22"/>
											<w:szCs w:val="22"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1191" w:type="dxa"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:rPr>
											<w:sz w:val="22"/>
											<w:szCs w:val="22"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6">
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1304" w:type="dxa"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="22"/>
											<w:szCs w:val="22"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1531" w:type="dxa"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="22"/>
											<w:szCs w:val="22"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2381" w:type="dxa"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:rPr>
											<w:sz w:val="22"/>
											<w:szCs w:val="22"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2098" w:type="dxa"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:rPr>
											<w:sz w:val="22"/>
											<w:szCs w:val="22"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1191" w:type="dxa"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="22"/>
											<w:szCs w:val="22"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1191" w:type="dxa"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:rPr>
											<w:sz w:val="22"/>
											<w:szCs w:val="22"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6">
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1304" w:type="dxa"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="22"/>
											<w:szCs w:val="22"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1531" w:type="dxa"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="22"/>
											<w:szCs w:val="22"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2381" w:type="dxa"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:rPr>
											<w:sz w:val="22"/>
											<w:szCs w:val="22"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2098" w:type="dxa"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:rPr>
											<w:sz w:val="22"/>
											<w:szCs w:val="22"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1191" w:type="dxa"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="22"/>
											<w:szCs w:val="22"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1191" w:type="dxa"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:rPr>
											<w:sz w:val="22"/>
											<w:szCs w:val="22"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
						</w:tr>
					</w:tbl>
					<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
						<w:pPr>
							<w:rPr>
								<w:sz w:val="24"/>
								<w:szCs w:val="24"/>
							</w:rPr>
						</w:pPr>
					</w:p>
					<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
						<w:pPr>
							<w:spacing w:after="240"/>
							<w:jc w:val="center"/>
							<w:rPr>
								<w:b/>
								<w:bCs/>
								<w:sz w:val="24"/>
								<w:szCs w:val="24"/>
							</w:rPr>
						</w:pPr>
						<w:r>
							<w:rPr>
								<w:b/>
								<w:bCs/>
								<w:sz w:val="24"/>
								<w:szCs w:val="24"/>
							</w:rPr>
							<w:t>Наличие разногласий при согласовании местоположения границы земельного участка:</w:t>
						</w:r>
					</w:p>
					<w:tbl>
						<w:tblPr>
							<w:tblW w:w="0" w:type="auto"/>
							<w:tblBorders>
								<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
								<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
								<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
								<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
								<w:insideH w:val="single" w:sz="4" w:space="0" w:color="auto"/>
								<w:insideV w:val="single" w:sz="4" w:space="0" w:color="auto"/>
							</w:tblBorders>
							<w:tblLayout w:type="fixed"/>
							<w:tblCellMar>
								<w:left w:w="28" w:type="dxa"/>
								<w:right w:w="28" w:type="dxa"/>
							</w:tblCellMar>
							<w:tblLook w:val="0000" w:firstRow="0" w:lastRow="0" w:firstColumn="0" w:lastColumn="0" w:noHBand="0" w:noVBand="0"/>
						</w:tblPr>
						<w:tblGrid>
							<w:gridCol w:w="2438"/>
							<w:gridCol w:w="2410"/>
							<w:gridCol w:w="4848"/>
						</w:tblGrid>
						<w:tr w:rsidR="007E23C6">
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2438" w:type="dxa"/>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>Обозначение характерной точки</w:t>
									</w:r>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:br/>
										<w:t>или части границы</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2410" w:type="dxa"/>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>Кадастровый номер смежного земельного участка</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="4848" w:type="dxa"/>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>Содержание возражений о местоположении границ</w:t>
									</w:r>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6">
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2438" w:type="dxa"/>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>1</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2410" w:type="dxa"/>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>2</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="4848" w:type="dxa"/>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>3</w:t>
									</w:r>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6">
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2438" w:type="dxa"/>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2410" w:type="dxa"/>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:ind w:left="57" w:right="57"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="4848" w:type="dxa"/>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:ind w:left="57" w:right="57"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
						</w:tr>
					</w:tbl>
					<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
						<w:pPr>
							<w:rPr>
								<w:sz w:val="24"/>
								<w:szCs w:val="24"/>
							</w:rPr>
						</w:pPr>
					</w:p>
					<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
						<w:pPr>
							<w:spacing w:after="240"/>
							<w:jc w:val="center"/>
							<w:rPr>
								<w:b/>
								<w:bCs/>
								<w:sz w:val="24"/>
								<w:szCs w:val="24"/>
							</w:rPr>
						</w:pPr>
						<w:r>
							<w:rPr>
								<w:b/>
								<w:bCs/>
								<w:sz w:val="24"/>
								<w:szCs w:val="24"/>
							</w:rPr>
							<w:t>Сведения о снятии возражений о местоположении границы земельного участка:</w:t>
						</w:r>
					</w:p>
					<w:tbl>
						<w:tblPr>
							<w:tblW w:w="0" w:type="auto"/>
							<w:tblBorders>
								<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
								<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
								<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
								<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
								<w:insideH w:val="single" w:sz="4" w:space="0" w:color="auto"/>
								<w:insideV w:val="single" w:sz="4" w:space="0" w:color="auto"/>
							</w:tblBorders>
							<w:tblLayout w:type="fixed"/>
							<w:tblCellMar>
								<w:left w:w="28" w:type="dxa"/>
								<w:right w:w="28" w:type="dxa"/>
							</w:tblCellMar>
							<w:tblLook w:val="0000" w:firstRow="0" w:lastRow="0" w:firstColumn="0" w:lastColumn="0" w:noHBand="0" w:noVBand="0"/>
						</w:tblPr>
						<w:tblGrid>
							<w:gridCol w:w="1588"/>
							<w:gridCol w:w="1842"/>
							<w:gridCol w:w="2694"/>
							<w:gridCol w:w="3572"/>
						</w:tblGrid>
						<w:tr w:rsidR="007E23C6">
							<w:trPr>
								<w:cantSplit/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1588" w:type="dxa"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>Обозначение характерной точки или части границы</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1842" w:type="dxa"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>Кадастровый номер смежного земельного участка</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2694" w:type="dxa"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>Фамилия и инициалы правообладателя или</w:t>
									</w:r>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:br/>
										<w:t>его представителя, реквизиты документа, удостоверяющего личность, дата снятия возражений, подпись</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="3572" w:type="dxa"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>Способ снятия возражений о местополо</w:t>
									</w:r>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:softHyphen/>
										<w:t xml:space="preserve">жении </w:t>
									</w:r>
									<w:proofErr w:type="gramStart"/>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>границ</w:t>
									</w:r>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:br/>
										<w:t>(</w:t>
									</w:r>
									<w:proofErr w:type="gramEnd"/>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>изменение местоположения</w:t>
									</w:r>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:br/>
										<w:t>границ, рассмотрение земельного спора в суде, третейском суде)</w:t>
									</w:r>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6">
							<w:trPr>
								<w:cantSplit/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1588" w:type="dxa"/>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>1</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1842" w:type="dxa"/>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>2</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2694" w:type="dxa"/>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>3</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="3572" w:type="dxa"/>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
										<w:t>4</w:t>
									</w:r>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6">
							<w:trPr>
								<w:cantSplit/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1588" w:type="dxa"/>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1842" w:type="dxa"/>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2694" w:type="dxa"/>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="3572" w:type="dxa"/>
									<w:vAlign w:val="center"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:rPr>
											<w:sz w:val="21"/>
											<w:szCs w:val="21"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
						</w:tr>
					</w:tbl>
					<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
						<w:pPr>
							<w:rPr>
								<w:sz w:val="24"/>
								<w:szCs w:val="24"/>
							</w:rPr>
						</w:pPr>
					</w:p>
					<w:tbl>
						<w:tblPr>
							<w:tblW w:w="0" w:type="auto"/>
							<w:tblInd w:w="-544" w:type="dxa"/>
							<w:tblLayout w:type="fixed"/>
							<w:tblCellMar>
								<w:left w:w="28" w:type="dxa"/>
								<w:right w:w="28" w:type="dxa"/>
							</w:tblCellMar>
							<w:tblLook w:val="0000" w:firstRow="0" w:lastRow="0" w:firstColumn="0" w:lastColumn="0" w:noHBand="0" w:noVBand="0"/>
						</w:tblPr>
						<w:tblGrid>
							<w:gridCol w:w="2722"/>
							<w:gridCol w:w="3119"/>
							<w:gridCol w:w="170"/>
							<w:gridCol w:w="3656"/>
						</w:tblGrid>
						<w:tr w:rsidR="007E23C6">
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2722" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="nil"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="24"/>
											<w:szCs w:val="24"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="24"/>
											<w:szCs w:val="24"/>
										</w:rPr>
										<w:t>Кадастровый инженер:</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="3119" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="nil"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="24"/>
											<w:szCs w:val="24"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="170" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="nil"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="24"/>
											<w:szCs w:val="24"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="3656" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
										<w:right w:val="nil"/>
									</w:tcBorders>
									<w:vAlign w:val="bottom"/>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="24"/>
											<w:szCs w:val="24"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
						</w:tr>
						<w:tr w:rsidR="007E23C6">
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="2722" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="nil"/>
									</w:tcBorders>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:rPr>
											<w:sz w:val="16"/>
											<w:szCs w:val="16"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="3119" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="nil"/>
									</w:tcBorders>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:tabs>
											<w:tab w:val="left" w:pos="1247"/>
										</w:tabs>
										<w:rPr>
											<w:sz w:val="16"/>
											<w:szCs w:val="16"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="16"/>
											<w:szCs w:val="16"/>
										</w:rPr>
										<w:t>М.П.</w:t>
									</w:r>
									<w:r>
										<w:rPr>
											<w:sz w:val="16"/>
											<w:szCs w:val="16"/>
										</w:rPr>
										<w:tab/>
										<w:t>(подпись)</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="170" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="nil"/>
									</w:tcBorders>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:rPr>
											<w:sz w:val="16"/>
											<w:szCs w:val="16"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="3656" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="nil"/>
										<w:right w:val="nil"/>
									</w:tcBorders>
								</w:tcPr>
								<w:p w:rsidR="007E23C6" w:rsidRDefault="007E23C6">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="16"/>
											<w:szCs w:val="16"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:sz w:val="16"/>
											<w:szCs w:val="16"/>
										</w:rPr>
										<w:t>(фамилия, инициалы)</w:t>
									</w:r>
								</w:p>
							</w:tc>
						</w:tr>
					</w:tbl>
					
   </xsl:template>
</xsl:stylesheet>