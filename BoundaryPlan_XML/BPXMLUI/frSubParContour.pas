unit frSubParContour;

{
  ������� �������������� �����
}
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ComCtrls,ActnList,System.Actions,JvgPage, JvPageManager,

  FrArea, frEntitySpatial,  STD_MP,  XMLIntf,
  unRussLang;

type
  TFrameSubParContour = class(TFrame)
    ActionList: TActionList;

    ActionChange_Attribute_DefOrNumPP: TAction;
    ActionChange_ExistingOrNew: TAction;
    jvpgmngr1: TJvPageManager;
    New1: TJvgPageControl;
    ts1: TTabSheet;
    ts2: TTabSheet;
    pnl1: TPanel;
    lblAttribute_DefOrNumPP_Label: TLabel;
    edtAttribute_DefOrNumPP_Edit: TEdit;
    procedure Attribute_NumberPP_EditKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure ActionChange_Attribute_DefOrNumPPExecute(Sender: TObject);
  private
    { Private declarations }
    FXmlNode                   : IXMLNode;
    FArea_frame                : TFrameArea;
    FEntitySpatial_frame       : TFrameEntity_Spatial;

    FOnChange_Attribute_DefOrNum: TNotifyEvent;

    function getNewContour: IXMLTSubParcel_Contours_Contour; 

    procedure Data_attribute_write;
    procedure Data_attribute_read;
    procedure Data_DoRead;
    procedure Data_DoWrite;

  protected
    procedure SetContour(const aContour: IXMLTSubParcel_Contours_Contour);
    procedure InitializationUI;
  public
    { Public declarations }
    constructor Create(Owner: TComponent); override;
    function    IsAssignedContour: boolean;
    procedure   AsAttributeContour(LockUserChange: Boolean=False);

    procedure DataRefeshOrdinate;
    procedure SetVisibleTypeCOntoursForm(flag : boolean);

    property XmlContour: IXMLTSubParcel_Contours_Contour read getNewContour write SetContour;
    property OnChange_Attribute_DefOnNum: TNotifyEvent read FOnChange_Attribute_DefOrNum write FOnChange_Attribute_DefOrNum;   end;

implementation

uses MsXMLAPI;
resourcestring
   cnstNumber = 'Number';
   cnstArea          = 'Area';
   cnstEntity_Spatial  = 'Entity_Spatial';


{$R *.dfm}

{ TFrameContour }

procedure TFrameSubParContour.ActionChange_Attribute_DefOrNumPPExecute(
  Sender: TObject);
begin
  if not self.IsAssignedContour then begin
      self.edtAttribute_DefOrNumPP_Edit.Text:= '';
      raise Exception.Create(ExMSG_NotAssignedROOT);
  end;
  Data_attribute_write;
  if Assigned(self.FOnChange_Attribute_DefOrNum) then
 self.FOnChange_Attribute_DefOrNum(self);
end;

procedure TFrameSubParContour.AsAttributeContour(LockUserChange: Boolean);
var
 _Def: string;
begin
 //���� ���� ������� ������
 if self.IsAssignedContour then
 begin
        _Def:=  MsXml_ReadAttribute(self.FXmlNode,cnstNumber);
        MsXml_RemoveAttribute(self.FXmlNode,cnstNumber);
        if _Def<>'' then 
        begin
         self.FXmlNode.Attributes[cnstNumber]:= _Def;
         self.edtAttribute_DefOrNumPP_Edit.Text:= _Def;
        end else
         self.edtAttribute_DefOrNumPP_Edit.Text:= _Def;
 end;
end;

procedure TFrameSubParContour.Attribute_NumberPP_EditKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
 if key = VK_RETURN then
  if (Sender is TCustomEdit) then
   TCustomEdit(Sender).Parent.SetFocus;
end;

constructor TFrameSubParContour.Create(Owner: TComponent);
begin
  inherited;
  DataRefeshOrdinate;
end;

procedure TFrameSubParContour.InitializationUI;
begin
 if self.IsAssignedContour then 
 begin
   self.Data_DoRead;
   self.FArea_frame.XmlArea:= (self.FXmlNode.ChildNodes.Nodes[cnstArea] as IXMLTArea);
   self.FEntitySpatial_frame.EntitySpatial:= (self.FXmlNode.ChildNodes.Nodes[cnstEntity_Spatial]  as IXMLEntity_Spatial);
   self.FEntitySpatial_frame.Visible:= true;
 end else
 begin
   self.edtAttribute_DefOrNumPP_Edit.Text:= '';
   self.FArea_frame.XmlArea:= nil;
   self.FEntitySpatial_frame.EntitySpatial:= nil;
 end;
end;

function TFrameSubParContour.IsAssignedContour: boolean;
begin
 Result := self.FXmlNode <> nil;
end;

procedure TFrameSubParContour.SetContour(const aContour: IXMLTSubParcel_Contours_Contour);
begin
 if FXmlNode = aContour  then Exit
 else
 begin
   self.FXmlNode:= aContour;
   self.InitializationUI;
 end;
end;


procedure TFrameSubParContour.SetVisibleTypeCOntoursForm(flag: boolean);
begin
   self.FArea_frame.Visible := flag;
end;

procedure TFrameSubParContour.DataRefeshOrdinate;
begin
    if not Assigned(FArea_frame) then
    begin
        self.FArea_frame:= TFrameArea.Create(self.ts1);
        self.FArea_frame.Parent:= self.ts1;
        self.FArea_frame.Align:= alClient;
    end;
    if not Assigned(FEntitySpatial_frame) then
    begin
       self.FEntitySpatial_frame:= TFrameEntity_Spatial.Create(ts2);
       self.FEntitySpatial_frame.Parent:= self.ts2;
       self.FEntitySpatial_frame.Align:= alClient;
    end;
end;


procedure TFrameSubParContour.Data_attribute_read;
begin
   self.AsAttributeContour(true);
end;

procedure TFrameSubParContour.Data_attribute_write;
begin
  FXmlNode.Attributes[cnstNumber] := self.edtAttribute_DefOrNumPP_Edit.Text;
end;

procedure TFrameSubParContour.Data_DoRead;
begin
 Data_attribute_read;
end;

procedure TFrameSubParContour.Data_DoWrite;
begin
 Data_attribute_write;
end;

function TFrameSubParContour.getNewContour: IXMLTSubParcel_Contours_Contour;
begin
 if (self.FXmlNode<> nil) then
  Result:= IXMLTSubParcel_Contours_Contour(self.FXmlNode)
 else
  Result:= nil;
end;

end.

