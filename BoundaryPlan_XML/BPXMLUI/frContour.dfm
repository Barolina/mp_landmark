object FrameContour: TFrameContour
  Left = 0
  Top = 0
  Width = 657
  Height = 315
  Color = clBtnFace
  ParentBackground = False
  ParentColor = False
  TabOrder = 0
  object jvgpgcntrlNewParcelsPageControl: TJvgPageControl
    Left = 0
    Top = 0
    Width = 657
    Height = 315
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    ActivePage = tsEnSp_Attribut
    Align = alClient
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clBlack
    Font.Height = -14
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    MultiLine = True
    ParentFont = False
    Style = tsFlatButtons
    TabHeight = 30
    TabOrder = 0
    TabStop = False
    TabWidth = 180
    SingleGlyph = True
    TabStyle.Borders = [fsdLeft, fsdTop, fsdRight, fsdBottom]
    TabStyle.BevelInner = bvNone
    TabStyle.BevelOuter = bvNone
    TabStyle.Bold = False
    TabStyle.BackgrColor = clInactiveBorder
    TabStyle.Font.Charset = RUSSIAN_CHARSET
    TabStyle.Font.Color = clBlack
    TabStyle.Font.Height = -12
    TabStyle.Font.Name = 'Arial'
    TabStyle.Font.Style = [fsBold]
    TabStyle.TextStyle = fstVolumetric
    TabStyle.CaptionHAlign = fhaCenter
    TabStyle.GlyphHAlign = fhaCenter
    TabStyle.Gradient.ToColor = clSilver
    TabStyle.Gradient.Active = True
    TabStyle.Gradient.Orientation = fgdRightBias
    TabSelectedStyle.Borders = [fsdLeft, fsdTop, fsdRight, fsdBottom]
    TabSelectedStyle.BevelInner = bvNone
    TabSelectedStyle.BevelOuter = bvNone
    TabSelectedStyle.Bold = False
    TabSelectedStyle.BackgrColor = 15921123
    TabSelectedStyle.Font.Charset = ANSI_CHARSET
    TabSelectedStyle.Font.Color = clBlack
    TabSelectedStyle.Font.Height = -12
    TabSelectedStyle.Font.Name = 'Arial'
    TabSelectedStyle.Font.Style = [fsBold]
    TabSelectedStyle.TextStyle = fstVolumetric
    TabSelectedStyle.CaptionHAlign = fhaCenter
    TabSelectedStyle.GlyphHAlign = fhaCenter
    TabSelectedStyle.Gradient.FromColor = 15921123
    TabSelectedStyle.Gradient.ToColor = 691384
    TabSelectedStyle.Gradient.Active = True
    TabSelectedStyle.Gradient.Orientation = fgdHorizontal
    TabsPosition = fsdBottom
    Options = [ftoAutoFontDirection, ftoExcludeGlyphs, ftoTabColorAsGradientFrom, ftoTabColorAsGradientTo]
    object tsEnSp_Attribut: TTabSheet
      Caption = #1050#1086#1086#1088#1076#1080#1085#1072#1090#1085#1086#1077' '#1086#1087#1080#1089#1072#1085#1080#1077
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ImageIndex = 5
      ParentFont = False
      TabVisible = False
      object jvgrdntEntity_Spatial: TJvGradient
        Left = 0
        Top = 0
        Width = 649
        Height = 305
        StartColor = 16512
        EndColor = clSilver
        ExplicitLeft = 360
        ExplicitTop = 240
        ExplicitWidth = 100
        ExplicitHeight = 41
      end
    end
    object tsArea: TTabSheet
      Hint = #1055#1083#1086#1097#1072#1076#1100'/'#1054#1073#1086#1079#1085#1072#1095#1077#1085#1080#1077
      Caption = #1055#1083#1086#1097#1072#1076#1100
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clBlack
      Font.Height = -14
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ImageIndex = 2
      ParentFont = False
      TabVisible = False
      object pnl2: TPanel
        Left = 0
        Top = 0
        Width = 649
        Height = 305
        Align = alClient
        BevelOuter = bvNone
        ParentBackground = False
        ParentColor = True
        TabOrder = 0
        object pnlArea: TPanel
          Left = 0
          Top = 41
          Width = 649
          Height = 80
          Align = alTop
          BevelOuter = bvNone
          ParentColor = True
          TabOrder = 0
        end
        object Attribute_Panel: TPanel
          Left = 0
          Top = 0
          Width = 649
          Height = 41
          Align = alTop
          BevelOuter = bvNone
          Color = clWhite
          TabOrder = 1
          object Attribute_DefOrNumPP_Label: TLabel
            Left = 5
            Top = 12
            Width = 79
            Height = 13
            Caption = #1054#1073#1086#1079#1085#1072#1095#1077#1085#1080#1077':'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Attribute_DefOrNumPP_Edit: TEdit
            Left = 123
            Top = 7
            Width = 235
            Height = 21
            BevelKind = bkFlat
            BorderStyle = bsNone
            Color = 15921123
            TabOrder = 0
            OnExit = ActionChange_Attribute_DefOrNumPPExecute
            OnKeyUp = Attribute_DefOrNumPP_EditKeyUp
          end
        end
      end
    end
  end
  object ActionList: TActionList
    Left = 600
    Top = 256
    object ActionChange_Attribute_DefOrNumPP: TAction
      Caption = 'ActionChange_Attribute_DefOrNumPP'
      OnExecute = ActionChange_Attribute_DefOrNumPPExecute
    end
    object ActionChange_ExistingOrNew: TAction
      Caption = 'ActionChange_ExistingOrNew'
    end
  end
end
