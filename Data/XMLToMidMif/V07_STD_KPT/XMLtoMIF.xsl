<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="text" version="1.0" encoding="UTF-8" indent="yes"/>
<xsl:strip-space elements="*"/>

 <xsl:template match="Region_Cadastr">          
 <xsl:element name="M.Gis">
       <xsl:apply-templates select="Package" />
 </xsl:element>
 </xsl:template>
 
 <!--Менять местами X Y  (0 - меняем  ; 1 - нет )--> 
<xsl:param name="IsTypeXY" select="0" />
 <!-- =======================================================================================-->
 
<xsl:template match="Package">
<xsl:text>
 Version 2 
 Delimiter "," 
 CoordSys Nonearth Units "m" Bounds (0,0) (100000000,100000000)
 Columns 8 
 KN Char(40) 
 KNU Char(40)
 KNEZ Char(40) 
 PAreaArea Char (62)
 PAreaUnit Char(3)
 AreaInnccuracy Char (10) <!--погрешность измерения -->
 Category Char(12) 
 Utilization Char(254)  
  <!-- eDocumentCodeType Char(3)
 eDocumentVersion Char(2)  
 SenderKod Char(12) 
 SenderName Char(255) 
 SenderDateUpload Char(10) 
 SenderAppointment	Char(50)
 SenderFIO	Char(100) 
 SenderEMail	 Char(60)
 SenderTelephone	Char(50)   
 CadastralBlockKN Char(40)   
 AreaTotal Char(62)  общая  площадь  кадастровго квартала  
 AreaUnit Char(3)   
 ParcelName Char(62) 
 ParcelState Char(62) статус  зем  уч
 ParcelDateCreated Char(10) дата постановки на учет 
 ParcelDateRemoved Char(10) 
 PAreaAreaCode Char(62)
 LocationCodeOKATO Char(11)
 LocationCodeKLADR Char(20)
 LocationRegion Char(6)
 LocationCityName Char(255)
 LocationCityType Char(255)
 LocationLocalitiName Char(255)
 LocationLocalitiType Char(255)
 LocationStreetName Char(255)
 LocationStreetType Char(255)
 LocationOther  Char(255)
 LocationNote  Char(1000) 
 RightsName  Char(150)
 RightsType Char(62) 
 ContourNumberPP char(42)
 EncumbrancesName char(255) сведения о части, сформированной в связи с наложением обременений 
 CertificationDocOrganisation char(155) наименование ор-гана кадастрового учета 
 CertificationDocDate char(10)
 CertificationDocFIOn char(100)
 CertificationDocNumber char(100) 
 CoordSystemsCsId char(5)
 CoordSystemsName char(50) -->
 Data &#10;</xsl:text>   
 <xsl:apply-templates  select="//Cadastral_Block"/>
 </xsl:template>
 
 <xsl:template match="Cadastral_Block">
     <xsl:apply-templates select="Parcels" />              
 </xsl:template>

  <xsl:template match="Parcels" name="Parcels">
        <xsl:apply-templates select="Parcel"/>
</xsl:template>

<!-- участок --> 
 <!-- ====================================================================================== -->
 <xsl:template match="Parcel">
        <xsl:choose>
        <!-- пространственная составляющая сущности данных -->
				        <xsl:when test="count(child::Entity_Spatial)>0">
     	     	   	   <xsl:apply-templates select="Entity_Spatial"/>
				         </xsl:when>
			    	<!-- конутра многоконтурного участка  -->
				    <xsl:when test="count(child::Contours)>0" >
				         <xsl:apply-templates select="Contours/Contour/Entity_Spatial"/>
				    </xsl:when>				
		</xsl:choose>
 </xsl:template>
 
  <!-- ===================================================================================== -->
 <xsl:template match="Entity_Spatial">
      <xsl:text>REGION </xsl:text>
      <xsl:value-of select="count(child::*)"/>
      <xsl:text>&#10;</xsl:text>
	     	            <xsl:apply-templates select="Spatial_Element"/>
       <xsl:text>Pen (1,2,0) &#10; Brush(2,65535)&#10;</xsl:text>
 </xsl:template>

<!-- элемент  когнтура -->
<!-- ======================================================================================= -->
<xsl:template match="Spatial_Element" name="Spatila_Element">
    <xsl:value-of select="count(child::Spelement_Unit)" />     <xsl:text>&#10;</xsl:text>
    <xsl:choose>
        <xsl:when test="$IsTypeXY=0">
             <xsl:call-template name="GetYX"/>
        </xsl:when>
        <xsl:otherwise>
            <xsl:call-template name="GetXY"/>
        </xsl:otherwise>
    </xsl:choose>  
</xsl:template>  

<!-- =======================================================================================-->
<xsl:template name = "GetXY">
 <xsl:for-each select="current()/child::Spelement_Unit"> 
          <xsl:for-each select="child::*[1]"> <!--TODO: пока что ...-->
               <xsl:value-of select="concat(@X,' ',@Y,'&#10;')"/>
          </xsl:for-each>
     </xsl:for-each>
</xsl:template> 

<xsl:template name = "GetYX">
 <xsl:for-each select="current()/child::Spelement_Unit"> 
          <xsl:for-each select="child::*[1]"> <!--TODO: пока что ...-->
               <xsl:value-of select="concat(@Y,' ',@X,'&#10;')"/>
          </xsl:for-each>
     </xsl:for-each>
</xsl:template> 

     
 <!-- ================================================================================= -->     
      
</xsl:stylesheet>