unit frSubParcel;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ComCtrls,ActnList,
  JvComponentBase, JvNavigationPane, System.Actions, JvExControls,
  JvGradient, JvgPage,

  FrArea, frEntitySpatial,STD_MP,XMLIntf,
  unRussLang, FrSubParContours,frContoursInvariable,
  frEncumbrance,frAreaInnvvuracy;

type
  TSubParcelKind  = (cUndefine,
                     cFormSubParcel,      //���������� ������ ��� ExistParcels
                     cExistSubParcel,
                     cInvariableSubParcel,
                     cNewFormSubParcel);  //���������� ����� ��� FormParcels

  TFrameSubParcel = class(TFrame)
    Area_Panel: TPanel;
    ActionList: TActionList;

    ActionChange_Attribute_DefOrNumPP: TAction;
    ActionChange_ExistingOrNew: TAction;
    pnlSubParcel_Realty: TPanel;
    chkAttribute_SubParcelRealty_CheckBox: TCheckBox;
    NewParcelsPageControl: TJvgPageControl;
    tsEnSp_Attribut: TTabSheet;
    tsEncumbrance: TTabSheet;
    jvnvpnstylmngr1: TJvNavPaneStyleManager;
    pnlCaptioanAction: TPanel;
    tsArea: TTabSheet;
    lblCaption: TLabel;
    edtAttribute_DefOrNumPP_Edit: TEdit;
    jvgrdnt2: TJvGradient;
    jvgrdnt3: TJvGradient;
    pnl1: TPanel;
    pnl2: TPanel;
    procedure Attribute_DefOrNumPP_EditKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Attribute_NumberPP_EditKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure ActionChange_Attribute_DefOrNumPPExecute(Sender: TObject);
    procedure chkAttribute_SubParcelRealty_CheckBoxClick(Sender: TObject);
    procedure NewParcelsPageControlChange(Sender: TObject);
    procedure edtAttribute_DefOrNumPP_EditExit(Sender: TObject);
  private
    { Private declarations }
    FXmlNode            : IXMLNode;
    
    //������������ ������d
    FEntitySpatial_frame: TFrameEntity_Spatial;
    FContours_frame     : TFrameSubParContours;                                  //Conturs

    FArea_Frame         : TFrameArea;                                            //�������
    FEncumbrance_frame  : TFrameEncumbrance;                                     //�������������� �����
    FContoursInvariable : TFrameContoursInvariable;                              //������ ��� SubParcelInvariable
    FArea_Invariable    : TFrameAreaInnccuracy;                                  //������� ��� SubParcelInvariable

    FXmlSubParcel           : IXMLTExistParcel_SubParcels_FormSubParcel;         //�� ExistParcels
    FXmlSubparcelExist      : IXMLTExistParcel_SubParcels_ExistSubParcel;
    FXmlInvariableSubParcel : IXMLTExistParcel_SubParcels_InvariableSubParcel;

    FXmlChangeSubParcel     : IXMLTChangeParcel_SubParcels_FormSubParcel;        //FormParcels
    FXmlChangeSubparcelExist: IXMLTChangeParcel_SubParcels_ExistSubParcel;
    FXmlChangeInvariableSub : IXMLTChangeParcel_SubParcels_InvariableSubParcel;

    FXmlNewParcelSubParcel  :  IXMLTNewParcel_SubParcels_FormSubParcel;          //NewParcels

    FisTypeLandSubParcel :  integer;                                             // ��� ������� Entity_Spatil or Contour
    FActivSubParcelKind  :  TSubParcelKind;

    FOnChange_Attribute_DefOrNum: TNotifyEvent;

    function getFormSubParcel: IXMLTExistParcel_SubParcels_FormSubParcel; //���������� ����� �������
    function getExistSubParcel: IXMLTExistParcel_SubParcels_ExistSubParcel; //������������ (����������, ����������)  ����� �������
    function getInvariableSubParcel : IXMLTExistParcel_SubParcels_InvariableSubParcel; //������������ (������������) ����� �������
    
    procedure Data_attribute_SubParcelRealty_DoRead;    //������ �� ����� �������� ������������
    procedure Data_attribute_SubParcelRealty_DoWrite;

    procedure Data_attribute_write;
    procedure Data_attribute_read;

    procedure Data_DoRead;
    procedure Data_DoWrite;
  protected
    procedure SetSubParcel(const aSubParcel: IXMLTExistParcel_SubParcels_FormSubParcel);
    procedure SetSubParcelExist (const aSubParcel : IXMLTExistParcel_SubParcels_ExistSubParcel);
    procedure SetSubParcelInvariable (const aSubParcel : IXMLTExistParcel_SubParcels_InvariableSubParcel);
    procedure SetNewParcelSubParcel(const aSubParcel : IXMLTNewParcel_SubParcels_FormSubParcel);

    procedure SetChangeSubParcel( const aSubParcel: IXMLTChangeParcel_SubParcels_FormSubParcel);
    procedure SetChangeSubParcelExist(const aSubParcel: IXMLTChangeParcel_SubParcels_ExistSubParcel);
    procedure SetChangeSubParcelInvariable(const aSubParcel: IXMLTChangeParcel_SubParcels_InvariableSubParcel);

    procedure InitializationUI;
  public
    { Public declarations }
    constructor Create(Owner: TComponent); override;
    
    function   IsAssignedSubParcel: boolean;
    procedure  AsAttributeSubParcel(LockUserChange: Boolean=False);
    procedure  ShowFrames(cbKind : TSubParcelKind; isEntity_Spatial : boolean);
    procedure  RepaintCaptiontoInvariable();
 
    property XmlSubParcel: IXMLTExistParcel_SubParcels_FormSubParcel 
                                  read getFormSubParcel 
                                  write SetSubParcel;
    property XmlSubParcelExist : IXMLTExistParcel_SubParcels_ExistSubParcel 
                                  read getExistSubParcel 
                                  write SetSubParcelExist;
    property XmlSubParcelInvariable : IXMLTExistParcel_SubParcels_InvariableSubParcel 
                                  read getInvariableSubParcel 
                                  write SetSubParcelInvariable;
    property XmlNewParcelSubParcel :  IXMLTNewParcel_SubParcels_FormSubParcel
                                  read FXmlNewParcelSubParcel
                                  write SetNewParcelSubParcel;
    property XmlChangeSubParcel : IXMLTChangeParcel_SubParcels_FormSubParcel
                                  read FXmlChangeSubParcel
                                  write SetChangeSubParcel;
    property XmlChangeExistSubParcel : IXMLTChangeParcel_SubParcels_ExistSubParcel
                                  read FXmlChangeSubparcelExist
                                  write SetChangeSubParcelExist;
    property XmlChangeInvariableSubParcel : IXMLTChangeParcel_SubParcels_InvariableSubParcel
                                  read FXmlChangeInvariableSub
                                  write SetChangeSubParcelInvariable;

    property OnChange_Attribute_DefOnNum: TNotifyEvent read FOnChange_Attribute_DefOrNum write FOnChange_Attribute_DefOrNum;

    property IsTypeLandSubParcel : integer read FisTypeLandSubParcel write FisTypeLandSubParcel;

    end;
    

implementation

uses MsXMLAPI;
resourcestring
   cnstTxtDefinition = '�����������: ';
   cnstTxtNumberRecord = '������� �����: ';

{$R *.dfm}

{ TFrameContour }


procedure _HiddenChangeCheckBoxSate(ChBox: TCheckBox; NewSate: TCheckBoxState);
var
 _OnClick: TNotifyEvent;
begin
 if ChBox.State=NewSate then EXIT;
 _OnClick:= ChBox.OnClick;
 ChBox.OnClick:= nil;
 ChBox.State:= NewSate;
 ChBox.OnClick:= _OnClick;
end;

procedure TFrameSubParcel.ActionChange_Attribute_DefOrNumPPExecute(
  Sender: TObject);
begin
  if not self.IsAssignedSubParcel then begin
      self.edtAttribute_DefOrNumPP_Edit.Text:= '';
      raise Exception.Create(ExMSG_NotAssignedROOT);
  end;
  Data_attribute_write;
  if Assigned(self.FOnChange_Attribute_DefOrNum) then
 self.FOnChange_Attribute_DefOrNum(self);
end;

procedure TFrameSubParcel.AsAttributeSubParcel(LockUserChange: Boolean);
var
 _Def: string;
begin
 if self.IsAssignedSubParcel then
 begin
   case FActivSubParcelKind  of
   cFormSubParcel,cNewFormSubParcel:
   begin{FormSubParcel ���  �� �������� ��� � �� �����������}
           self.edtAttribute_DefOrNumPP_Edit.Text:= cnstTxtDefinition;
          _Def:=  MsXml_ReadAttribute(self.FXmlNode,cnstAtrDefinition);
          MsXml_RemoveAttribute(self.FXmlNode,cnstAtrDefinition);
          if _Def<>'' then
          begin
           self.FXmlNode.Attributes[cnstAtrDefinition]:= _Def;
           self.edtAttribute_DefOrNumPP_Edit.Text:= _Def;
          end else self.edtAttribute_DefOrNumPP_Edit.Text:= _Def;
   end;
   cExistSubParcel,cInvariableSubParcel:
   begin {ExistSubParcel and InvariableSubParcel}
           self.edtAttribute_DefOrNumPP_Edit.Text:= cnstTxtNumberRecord;
           _Def:=  MsXml_ReadAttribute(self.FXmlNode,cnstAtrNumber_Record);
           MsXml_RemoveAttribute(self.FXmlNode,cnstAtrNumber_Record);
           if _Def<>'' then 
           begin
            self.FXmlNode.Attributes[cnstAtrNumber_Record]:= strtoint(_Def);
            self.edtAttribute_DefOrNumPP_Edit.Text:= _Def;
           end else
            self.edtAttribute_DefOrNumPP_Edit.Text:= _Def;
      end;
   end;
 end;
end;


procedure TFrameSubParcel.Attribute_DefOrNumPP_EditKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
 if key = VK_RETURN then
  if (Sender is TCustomEdit) then
   TCustomEdit(Sender).Parent.SetFocus;
end;

procedure TFrameSubParcel.Attribute_NumberPP_EditKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
 if key = VK_RETURN then
  if (Sender is TCustomEdit) then
   TCustomEdit(Sender).Parent.SetFocus;
end;

procedure TFrameSubParcel.chkAttribute_SubParcelRealty_CheckBoxClick(
  Sender: TObject);
begin
 if not self.IsAssignedSubParcel then raise Exception.Create(ExMSG_NotAssignedROOT);
 self.Data_attribute_SubParcelRealty_DoWrite;
end;

constructor TFrameSubParcel.Create(Owner: TComponent);
begin
  inherited;
  if not Assigned(FArea_frame) then
  begin
    self.FArea_frame:= TFrameArea.Create(self.Area_Panel);
    self.FArea_frame.Parent:= self.Area_Panel;
    self.FArea_frame.Align:= alClient;
    self.Visible := false;
  end;
  if not Assigned(FEntitySpatial_frame) then
  begin
    self.FEntitySpatial_frame:= TFrameEntity_Spatial.Create(tsEnSp_Attribut);
    self.FEntitySpatial_frame.Parent:= tsEnSp_Attribut;
    self.FEntitySpatial_frame.Align:= alClient;
    self.FEntitySpatial_frame.Visible := False;
  end;
  if not Assigned(FContours_frame) then
  begin
    self.FContours_frame := TFrameSubParContours.Create(tsEnSp_Attribut);
    self.FContours_frame.ActivContourKind:= TFrameSubParContours.TContourKind.cNewContour;
    self.FContours_frame.cbbContoursType.Enabled:= false;
    self.FContours_frame.Parent := tsEnSp_Attribut;
    self.FContours_frame.Align := alClient;
    self.FContours_frame.Contour.AsAttributeContour;
    Self.FContours_frame.Visible := false;
  end;
  if not Assigned(self.FContoursInvariable) then
  begin
    FContoursInvariable := TFrameContoursInvariable.Create(self.tsEnSp_Attribut);
    FContoursInvariable.Parent := self.tsEnSp_Attribut;
    FContoursInvariable.Align := alClient;
    FContoursInvariable.Visible := false;
  end;
  if not Assigned(self.FArea_Invariable) then
  begin
    self.FArea_Invariable:= TFrameAreaInnccuracy.Create(self.Area_Panel);
    self.FArea_Invariable.Parent:= self.Area_Panel;
    self.FArea_Invariable.Align:= alClient;
    self.FArea_Invariable.Visible := false;
  end;
  if not Assigned(FEncumbrance_frame) then
  begin
    self.FEncumbrance_frame:= TFrameEncumbrance.Create(self.tsEncumbrance);
    self.FEncumbrance_frame.Parent:= self.tsEncumbrance;
    self.FEncumbrance_frame.Align:= alClient;
  end;  
  self.NewParcelsPageControl.ActivePage := self.tsEnSp_Attribut;
end;     

procedure TFrameSubParcel.InitializationUI;
var  indexTypeLand : integer;

   function  GetTypeOridinate() : Integer;
   var xmlChildLst  : IXMLNodeList;
   begin
     xmlChildLst := FXmlNode.ChildNodes;
     Result := 0;
     if xmlChildLst.FindNode(cnstXmlContours) <> nil then Result := 1;   
     xmlChildLst := nil;
   end;
begin
 {�������� ����������� ��� ������������ ����� } 
 RepaintCaptiontoInvariable;
 if self.IsAssignedSubParcel then
 begin
   self.Data_DoRead;
   indexTypeLand := GetTypeOridinate;
   case FActivSubParcelKind of
    cFormSubParcel,cExistSubParcel,cNewFormSubParcel:
     begin
          self.FArea_frame.XmlArea:= (self.FXmlNode.ChildNodes.Nodes[cnstXmlArea] as IXMLTArea);
          if indexTypeLand  = 0  then
             self.FEntitySpatial_frame.EntitySpatial:= (self.FXmlNode.ChildNodes.Nodes[cnstXmlEntity_Spatial]  as IXMLEntity_Spatial)
          else
             self.FContours_frame.XmlContours := (self.FXmlNode.ChildNodes.Nodes[cnstXmlContours] as IXMLTSubParcel_Contours);
     end;
   cInvariableSubParcel  :
    begin
          self.FArea_Invariable.XmlAreaInvariable:= self.FXmlNode.ChildNodes.Nodes[cnstXmlArea] as IXMLTExistParcel_SubParcels_InvariableSubParcel_Area;
          self.FContoursInvariable.XmlContours := self.FXmlNode.ChildNodes.Nodes[cnstXmlContours] as IXMLTExistParcel_SubParcels_InvariableSubParcel_Contours;
    end
   else raise Exception.Create('��� �� ����� �� ��� � ContourExistParcel_Init');
   end;
   self.FEncumbrance_frame.Encumbrance:= (self.FXmlNode.ChildNodes.Nodes[cnstXmlEncumbrance] as IXMLTEncumbrance);
   ShowFrames(FActivSubParcelKind,(indexTypeLand = 0));
 end
 else
 begin
    self.edtAttribute_DefOrNumPP_Edit.Text:= '';
    _HiddenChangeCheckBoxSate(self.chkAttribute_SubParcelRealty_CheckBox,cbUnchecked);
    FEntitySpatial_frame.EntitySpatial := nil;
    FContours_frame.XmlContours := nil;
    FContoursInvariable.XmlContours := nil;
    self.FArea_Frame.XmlArea:= nil;
    self.FEncumbrance_frame.Encumbrance:= nil;
    self.FArea_Invariable.XmlAreaInvariable := nil;
 end;
end;

function TFrameSubParcel.IsAssignedSubParcel: boolean;
begin
 Result := self.FXmlNode <> nil;
end;

procedure TFrameSubParcel.NewParcelsPageControlChange(Sender: TObject);
begin
  pnlCaptioanAction.Caption := (Sender as  TJvgPageControl).ActivePage.Caption;
  RepaintCaptiontoInvariable;
end;

///
/// ���� ��������� �� Invariable - ��� ���� ����������� �������� ��  ����������� 
///
procedure TFrameSubParcel.RepaintCaptiontoInvariable;
begin
  self.pnlCaptioanAction.Color :=$00C08000; 
  if (self.NewParcelsPageControl.ActivePage = tsEnSp_Attribut) and            //����� ����������� �������� �� �����������
     (FActivSubParcelKind = cInvariableSubParcel) then
      self.pnlCaptioanAction.Color := clWhite
end;

procedure TFrameSubParcel.SetChangeSubParcel(
  const aSubParcel: IXMLTChangeParcel_SubParcels_FormSubParcel);
begin
  FActivSubParcelKind := cFormSubParcel;
  self.FXmlNode := aSubParcel;
  InitializationUI
end;

procedure TFrameSubParcel.SetChangeSubParcelExist(
  const aSubParcel: IXMLTChangeParcel_SubParcels_ExistSubParcel);
begin
  FActivSubParcelKind := cExistSubParcel;
  self.FXmlNode := aSubParcel;
  InitializationUI
end;

procedure TFrameSubParcel.SetChangeSubParcelInvariable(
  const aSubParcel: IXMLTChangeParcel_SubParcels_InvariableSubParcel);
begin
  FActivSubParcelKind := cInvariableSubParcel;
  self.FXmlNode := aSubParcel;
  InitializationUI
end;

procedure TFrameSubParcel.SetNewParcelSubParcel(
  const aSubParcel: IXMLTNewParcel_SubParcels_FormSubParcel);
begin
  FActivSubParcelKind := cNewFormSubParcel;
  self.FXmlNode:= aSubParcel;
  self.InitializationUI;
end;

procedure TFrameSubParcel.SetSubParcel(const aSubParcel: IXMLTExistParcel_SubParcels_FormSubParcel);
begin
 if FXmlNode = aSubParcel  then Exit
 else
 begin   
   FActivSubParcelKind := cFormSubParcel;
   self.FXmlNode:= aSubParcel;
   self.InitializationUI;
 end;
end;

procedure TFrameSubParcel.SetSubParcelInvariable( const aSubParcel: IXMLTExistParcel_SubParcels_InvariableSubParcel);
begin
 if FXmlNode = aSubParcel  then Exit
 else
 begin
   FActivSubParcelKind := cInvariableSubParcel;
   self.FXmlNode:= aSubParcel;
   self.InitializationUI;
 end;
end;

procedure TFrameSubParcel.SetSubParcelExist(const aSubParcel: IXMLTExistParcel_SubParcels_ExistSubParcel);
begin
 if FXmlNode = aSubParcel  then Exit
 else
 begin
   FActivSubParcelKind := cExistSubParcel;
   self.FXmlNode:= aSubParcel;
   self.InitializationUI;
 end;
end;

procedure TFrameSubParcel.ShowFrames(cbKind :TSubParcelKind;isEntity_Spatial : boolean);
begin
  FEntitySpatial_frame.Visible := (cbKind <> cInvariableSubParcel)and (isEntity_Spatial);
  FContours_frame.Visible :=      (cbKind <> cInvariableSubParcel)and (not (isEntity_Spatial));
  FContoursInvariable.Visible := (cbKind = cInvariableSubParcel);
  FArea_Frame.Visible := (cbKind <> cInvariableSubParcel );
  FArea_Invariable.Visible := (cbKind = cInvariableSubParcel);
  FArea_Frame.Innccuracy_MaskEdit.Color := $00F0F0F0;
  
  FEncumbrance_frame.Visible := True; { ������������  ������ : ��� ���� ����� ������ }
 end;


procedure TFrameSubParcel.Data_attribute_read;
begin
   self.AsAttributeSubParcel(true);
   self.Data_attribute_SubParcelRealty_DoRead;
end;

procedure TFrameSubParcel.Data_attribute_SubParcelRealty_DoRead;
begin
   if (self.FXmlNode.Attributes[cnstAtrSubParcel_Realty] = true) then
    _HiddenChangeCheckBoxSate(self.chkAttribute_SubParcelRealty_CheckBox,cbChecked)
   else
   begin
    self.FXmlNode.Attributes[cnstAtrSubParcel_Realty]:= false;
    _HiddenChangeCheckBoxSate(self.chkAttribute_SubParcelRealty_CheckBox,cbUnchecked);
   end;
end;

procedure TFrameSubParcel.Data_attribute_SubParcelRealty_DoWrite;
begin
   case self.chkAttribute_SubParcelRealty_CheckBox.State of
    cbChecked:  self.FXmlNode.Attributes[cnstAtrSubParcel_Realty]:= true;
    cbUnchecked: self.FXmlNode.Attributes[cnstAtrSubParcel_Realty]:= false;
   end;
end;

procedure TFrameSubParcel.Data_attribute_write;
begin
 case FActivSubParcelKind of
  cFormSubParcel,cNewFormSubParcel   : self.FXmlNode.Attributes[cnstAtrDefinition]    :=  self.edtAttribute_DefOrNumPP_Edit.Text;
  cExistSubParcel,cInvariableSubParcel  : self.FXmlNode.Attributes[cnstAtrNumber_Record] :=  strtoint(self.edtAttribute_DefOrNumPP_Edit.Text);
  else
    raise Exception.Create('Error : attribute is TypeConour?');
 end;
 self.Data_attribute_SubParcelRealty_DoWrite;
end;


procedure TFrameSubParcel.Data_DoRead;
begin
 Data_attribute_read;
end;

procedure TFrameSubParcel.Data_DoWrite;
begin
 Data_attribute_write;
end;

procedure TFrameSubParcel.edtAttribute_DefOrNumPP_EditExit(Sender: TObject);
begin
 if not self.IsAssignedSubParcel then begin
  self.edtAttribute_DefOrNumPP_Edit.Text:= '';
  raise Exception.Create(ExMSG_NotAssignedROOT);
 end;
 Data_attribute_write;
 if Assigned(self.FOnChange_Attribute_DefOrNum) then
  self.FOnChange_Attribute_DefOrNum(self)
end;

function TFrameSubParcel.getInvariableSubParcel: IXMLTExistParcel_SubParcels_InvariableSubParcel;
begin
 if (self.FXmlNode<> nil) then
  Result:= IXMLTExistParcel_SubParcels_InvariableSubParcel(self.FXmlNode)
 else
  Result:= nil;
end;

function TFrameSubParcel.getExistSubParcel: IXMLTExistParcel_SubParcels_ExistSubParcel;
begin
 if (self.FXmlNode<> nil) then
  Result:= IXMLTExistParcel_SubParcels_ExistSubParcel(self.FXmlNode)
 else
  Result:= nil;
end;

function TFrameSubParcel.getFormSubParcel: IXMLTExistParcel_SubParcels_FormSubParcel;
begin
 if (self.FXmlNode<> nil) then
  Result:= IXMLTExistParcel_SubParcels_FormSubParcel(self.FXmlNode)
 else
  Result:= nil;
end;

end.

