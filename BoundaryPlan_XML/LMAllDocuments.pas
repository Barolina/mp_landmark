unit LMAllDocuments;

interface
 uses
  System.classes,
  System.IOUtils,
  System.RegularExpressions,
  System.Generics.Collections,
  System.Generics.Defaults,
  System.SysUtils,
  xmlintf, XMLDoc, STD_MP,BpXML_different;

type

 ISTDMPDictionary = interface
  ['{ED1B0633-2BA8-4C4D-B0B8-1E66147B42B8}']
  function getCount: integer;
  function getKey(index: integer): string;
  function getValue(index: integer): string;
  function indexOfKey(aKey: string): integer;
  function indexOfValue(aValue: string): integer;
  function getByValue(aKey: string): string;
  function getByKey(aValue: string): string;
  function ValuesToText: string;
  function Dictionary(aKeyRegExp: string):ISTDMPDictionary;
 end;

 //312222
 TMask = record
  function Next(aKey: string): string;
 end;

 TAllDocuments = class(TInterfacedObject,ISTDMPDictionary)
  private
   FKey: TList<string>;
   FValue: TList<string>;
  protected
   procedure Add(aKey,aValue: string);
   procedure Del(index: integer);
   procedure LoadFromFile(SourceXSD: string);
  public
   constructor Creare; overload;
   constructor Create(aPath: string); overload;
   destructor Destroy; override;

   {$region 'ISTDMPDictionary'}
   function getCount: integer;
   function getKey(index: integer): string;
   function getValue(index: integer): string;
   function indexOfKey(aKey: string): integer;
   function indexOfValue(aValue: string): integer;
   function getByValue(aValue: string): string;
   function getByKey(aKey: string): string;
   function ValuesToText: string;
   function Dictionary(aKeyRegExp: string):ISTDMPDictionary;
   {$endregion}

 end;

implementation
 resourcestring
  rsMask558Lavel0 = '^558[^0]0*$';
  rsMask558Lavel1 = '^558+\d{1}0*$';
  rsMask558Lavel2 = '^558+\d{3}0*$';
  rsMask558Lavel3 = '^558+\d{5}0*$';
  rsMask558Lavel4 = '^558+\d{7}0*$';
  rsMask558Lavel5 = '^558+\d{9}$';





  rs558_1 = '^558\d{1}((\d[^0])|([^0]\d))0*$';
  rs558_2 = '^558\d{3}((\d[^0])|([^0]\d))0*$';
  rs558_3 = '^558\d{5}((\d[^0])|([^0]\d))0*$';
  rs558_4 = '^558\d{7}((\d[^0])|([^0]\d))0*$';
  rs558_5 = '^558\d{9}((\d[^0])|([^0]\d))0*$';

  rsIdentityPapers = '^008001+0*';
  rsIdentityPapersLegalEntity = '^008002+0*';
  rsConfirmingPayment = '^55500+\d+0*';
  rsApplicationsAndInquiries = '^558+\d+0*';

{ TAllDocuments }

procedure TAllDocuments.Add(aKey, aValue: string);
begin
 self.FKey.Add(akey);
 self.FValue.Add(aValue);
end;

constructor TAllDocuments.Creare;
begin
 inherited;
 self.FKey:= TList<string>.Create(TIStringComparer.Ordinal);
 self.FValue:= TList<string>.Create(TIStringComparer.Ordinal);
end;

constructor TAllDocuments.Create(aPath: string);
begin
  self.Creare;
  self.LoadFromFile(aPath);
end;

procedure TAllDocuments.Del(index: integer);
begin
 self.FKey.Delete(index);
 self.FValue.Delete(index);
end;

destructor TAllDocuments.Destroy;
begin
 self.FKey.Free;
 self.FValue.Free;
 inherited;
end;

function TAllDocuments.getByKey(aKey: string): string;
var
 _i: integer;
begin
 _i:= self.FKey.IndexOf(aKey);
 if _i<>-1 then
  Result:= self.FValue[_i]
 else
  Result:= '';
end;

function TAllDocuments.getByValue(aValue: string): string;
var
 _i: integer;
begin
 _i:= self.FValue.IndexOf(aValue);
 if _i<>-1 then
  Result:= self.FKey[_i]
 else
  Result:= '';
end;

function TAllDocuments.getCount: integer;
begin
 Result:= self.FKey.Count;
end;

function TAllDocuments.getKey(index: integer): string;
begin
 Result:= self.FKey[index];
end;

function TAllDocuments.getValue(index: integer): string;
begin
 Result:= self.FValue[index];
end;

function TAllDocuments.indexOfKey(aKey: string): integer;
begin
 Result:= self.FKey.IndexOf(aKey);
end;

function TAllDocuments.indexOfValue(aValue: string): integer;
begin
 Result:= self.FValue.IndexOf(aValue);
end;

procedure TAllDocuments.LoadFromFile(SourceXSD: string);
var
 _i: Integer;
 _Doc: IXMLDocument;
 _Node, _Enum: IXMLNode;
begin
 _Doc := TXMLDocument.Create(nil);
 _Doc.fileName := SourceXSD;
 _Doc.Active := true;
 _Node := _Doc.DocumentElement.ChildNodes.Nodes['simpleType'];
 _Node := _Node.ChildNodes.Nodes['restriction'];
 for _i := 0 to _Node.ChildNodes.Count - 1 do begin
  _Enum := _Node.ChildNodes.Get(_i);
  self.Add(_Enum.Attributes['value'],
    _Enum.ChildNodes.Nodes['annotation'].ChildNodes.Nodes['documentation'].NodeValue);
 end;
 _Doc := nil;
end;

function TAllDocuments.Dictionary(aKeyRegExp: string): ISTDMPDictionary;
var
 _dic: TAllDocuments;
 _i: integer;
begin
 _dic:= TAllDocuments.Creare;
 for _i := 0 to self.getCount-1 do
  if TRegEx.IsMatch(self.getKey(_i),aKeyRegExp) then
   _dic.Add(self.getKey(_i),self.getValue(_i));
 { TODO :
������� ������ ������� �.�. �� �������� �������� � �� ���� ��
������ ������������ � �������������� ������ }
 Result:= _dic;
end;

function TAllDocuments.ValuestoText: string;
var
 _stb: TStringBuilder;
 _i: integer;
begin
 _stb:= TStringBuilder.Create;
 for _i := 0 to self.getCount-2 do
  _stb.Append(self.getValue(_i)).AppendLine;
 _stb.Append(self.getValue(self.getCount-1));
 Result:= _stb.ToString;
 _stb.Free;
end;

{ TMask }

function TMask.Next(aKey: string): string;
begin
 if TRegEx.IsMatch(aKey,'^585') then begin //312222
  if TRegEx.IsMatch(aKey,'^\d{3}0*$') then Exit('^'+Copy(aKey,low(aKey),3)+'\d{1}0*$');
  if TRegEx.IsMatch(aKey,'^\d{4}0*$') then Exit('^'+Copy(aKey,low(aKey),4)+'\d{2}0*$');
  if TRegEx.IsMatch(aKey,'^\d{6}0*$') then Exit('^'+Copy(aKey,low(aKey),6)+'\d{2}0*$');
  if TRegEx.IsMatch(aKey,'^\d{8}0*$') then Exit('^'+Copy(aKey,low(aKey),8)+'\d{2}0*$');
  if TRegEx.IsMatch(aKey,'^\d{10}0*$') then Exit('^'+Copy(aKey,low(aKey),10)+'\d{2}0*$');
  Exit('');
 end;
 if TRegEx.IsMatch(aKey,'^008|^555') then begin //336
  if TRegEx.IsMatch(aKey,'^\d{3}0*$') then Exit('^'+Copy(aKey,low(aKey),3)+'\d{3}0*$');
  if TRegEx.IsMatch(aKey,'^\d{6}0*$') then Exit('^'+Copy(aKey,low(aKey),6)+'\d{6}$');
  Exit('');
 end;
end;


end.
