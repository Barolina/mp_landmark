unit frNewSubParcel;

{�������� �� ���������� ������}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, Mask, ComCtrls, Buttons, Grids,

  frEntitySpatial, XMLDoc, XMLIntf, STD_MP, ActnList, ButtonGroup,
  PlatformDefaultStyleActnCtrls, ActnMan, PageControlAnimate,
  frArea,  ImgList,
  frEncumbrance,
  System.Actions,
  frSubParContours, unRussLang;

type
  TFrameNewSubParcel = class(TFrame)
    Label3: TLabel;
    NewParcelsPageControl: TPageControl;
    tsAttribut: TTabSheet;
    tsEncumbrance: TTabSheet;
    Coordinats_TabSheet: TTabSheet;
    Panel_Navigation: TPanel;
    SpeedButton_Nav_Back: TSpeedButton;
    SpeedButton_Nav_Next: TSpeedButton;
    ComboBox_Nav_GoTo: TComboBox;
    chk_Encumbrance_IsFill: TCheckBox;
    lblEncumbrance: TLabel;
    OrdinatesPageControl: TPageControl;
    Contours_TabSheet: TTabSheet;
    EntitySpatial_TabSheet: TTabSheet;
    Area_IsFill_CheckBox: TCheckBox;
    Coordinats_Label_Caption: TLabel;
    Coordinats_IsFill_CheckBox: TCheckBox;
    pnlProvCadNum: TPanel;
    lblListCadastralNUmbers: TLabel;
    medtCadastralNumber: TMaskEdit;
    chkAttribute_SubParcelRealty_CheckBox: TCheckBox;

    procedure SpeedButton_Nav_BackClick(Sender: TObject);
    procedure SpeedButton_Nav_NextClick(Sender: TObject);
    procedure ComboBox_Nav_GoToChange(Sender: TObject);

    procedure Attributes_Definition_EditKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Attributes_AdditionalName_EditKeyUp(Sender: TObject;
      var Key: Word; Shift: TShiftState);
    procedure medtCadastralBlock_value_MaskEditKeyUp(Sender: TObject;
      var Key: Word; Shift: TShiftState);
    procedure NewParcelsPageControlChange(Sender: TObject);
    procedure lblEncumbranceClick(Sender: TObject);
    procedure medtCadastralNumberExit(Sender: TObject);
    procedure chkAttribute_SubParcelRealty_CheckBoxClick(Sender: TObject);

  private
    FXmlNewParcel       : IXMLTNewSubParcel; // ������� ��������� �������

    FEncumbrance        : TFrameEncumbrance;
    FArea_frame         : TFrameArea; // �������
    FContours_frame     : TFrameSubParContours; // ���� ��������������
    FEntitySpatial_frame: TFrameEntity_Spatial; // ���� �������������

    FOnChangeDefinition: TNotifyEvent;

    procedure InitializationUI;
    // ����������� ����� ���������� �������
    procedure UI_CadNum_Read;
    procedure UI_CadNum_Write;
    procedure Data_attribute_SubParcelRealty_DoRead;    //������ �� ����� �������� ������������
    procedure Data_attribute_SubParcelRealty_DoWrite;
    
    procedure Data_Read;
    procedure Data_Write;
    // ��������� ������� NewParcel
    procedure SetNewParcel(const aNewParcel: IXMLTNewSubParcel);

    procedure SetType(aValue: integer); //0 - ���������������� 1-�������������� �������
    function  GetType: integer;
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    procedure SetActivTab(index: integer; UseAnimate: Boolean = false);

    function IsAssigned_NewParcel: Boolean;

    property XmlNewParcel: IXMLTNewSubParcel read FXmlNewParcel write SetNewParcel;
    property TypeNewParcel: integer read GetType write SetType;
    Property OnChangeDefinition: TNotifyEvent read FOnChangeDefinition write FOnChangeDefinition;
  end;

implementation

uses MsXMLAPI;

procedure _HiddenChangeCheckBoxSate(ChBox: TCheckBox; NewSate: TCheckBoxState);
var
  _OnClick: TNotifyEvent;
begin
  if ChBox.State = NewSate then
    EXIT;
  _OnClick := ChBox.OnClick;
  ChBox.OnClick := nil;
  ChBox.State := NewSate;
  ChBox.OnClick := _OnClick;
end;

{$R *.dfm}
{ TFrameNewparcel }

procedure TFrameNewSubParcel.Attributes_AdditionalName_EditKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key = VK_RETURN then
    if (Sender is TCustomEdit) then
      TCustomEdit(Sender).Parent.SetFocus;
end;

procedure TFrameNewSubParcel.Attributes_Definition_EditKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key = VK_RETURN then
    if (Sender is TCustomEdit) then
      TCustomEdit(Sender).Parent.SetFocus;
end;

procedure TFrameNewSubParcel.chkAttribute_SubParcelRealty_CheckBoxClick(Sender: TObject);
begin
 if not self.IsAssigned_NewParcel then raise Exception.Create(ExMSG_NotAssignedROOT);
 self.Data_attribute_SubParcelRealty_DoWrite;
end;

procedure TFrameNewSubParcel.medtCadastralBlock_value_MaskEditKeyUp
  (Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if Key = VK_RETURN then
    if (Sender is TCustomEdit) then
      TCustomEdit(Sender).Parent.SetFocus;
end;

procedure TFrameNewSubParcel.medtCadastralNumberExit(Sender: TObject);
begin
  UI_CadNum_Write;
end;

procedure TFrameNewSubParcel.ComboBox_Nav_GoToChange(Sender: TObject);
begin
  self.SetActivTab(self.ComboBox_Nav_GoTo.ItemIndex, false);
end;

constructor TFrameNewSubParcel.Create(AOwner: TComponent);
begin
  inherited;

  self.FArea_frame := TFrameArea.Create(self.tsAttribut);
  self.FArea_frame.Parent := self.tsAttribut;
  self.FArea_frame.Align := alTop;
  self.FArea_frame.Visible := true;

  self.FContours_frame := TFrameSubParContours.Create(self.Contours_TabSheet);
  self.FContours_frame.ActivContourKind:= TFrameSubParContours.TContourKind.cNewContour;
  self.FContours_frame.cbbContoursType.Visible:= false;
  self.FContours_frame.Parent := self.Contours_TabSheet;
  self.FContours_frame.Align := alClient;
  self.FContours_frame.Contour.AsAttributeContour;

  self.FEntitySpatial_frame := TFrameEntity_Spatial.Create(self.EntitySpatial_TabSheet);
  self.FEntitySpatial_frame.Parent := self.EntitySpatial_TabSheet;
  self.FEntitySpatial_frame.Align := alClient;

  self.FEncumbrance := TFrameEncumbrance.Create(self.tsEncumbrance);
  self.FEncumbrance.Parent := self.tsEncumbrance;
  self.FEncumbrance.Align := alClient;
  self.FEncumbrance.Visible := true;
end;

procedure TFrameNewSubParcel.Data_attribute_SubParcelRealty_DoRead;
begin
   if (self.FXmlNewParcel.Attributes[cnstAtrSubParcel_Realty] = true) then
    _HiddenChangeCheckBoxSate(self.chkAttribute_SubParcelRealty_CheckBox,cbChecked)
   else
   begin
    self.FXmlNewParcel.Attributes[cnstAtrSubParcel_Realty]:= false;// �������������� ��������
    _HiddenChangeCheckBoxSate(self.chkAttribute_SubParcelRealty_CheckBox,cbUnchecked);
   end;
end;

procedure TFrameNewSubParcel.Data_attribute_SubParcelRealty_DoWrite;
begin
   case self.chkAttribute_SubParcelRealty_CheckBox.State of
    cbChecked:  self.FXmlNewParcel.Attributes[cnstAtrSubParcel_Realty]:= true;
    cbUnchecked: self.FXmlNewParcel.Attributes[cnstAtrSubParcel_Realty]:= false;
   end;
end;

procedure TFrameNewSubParcel.Data_Read;
begin
  UI_CadNum_Read;
  Data_attribute_SubParcelRealty_DoRead;
end;

procedure TFrameNewSubParcel.Data_Write;
begin
 UI_CadNum_Write;
 Data_attribute_SubParcelRealty_DoWrite;
end;

function TFrameNewSubParcel.GetType: integer;
begin
 Result:= -1;
 if Xml_IsExistChildNode(self.FXmlNewParcel,cnstXmlContours) then
  Result:= 1
 else
  if Xml_IsExistChildNode(self.FXmlNewParcel,cnstXmlEntity_Spatial) then
    Result:= 0;
end;

procedure TFrameNewSubParcel.InitializationUI;
var
  _node: IXMLNode;
begin

  if self.IsAssigned_NewParcel then
  begin
    Data_Read;
    self.NewParcelsPageControl.Visible := true;
    self.Panel_Navigation.Visible := true;
    self.UI_CadNum_Read;
    FArea_frame.XmlArea := FXmlNewParcel.Area;
    FEncumbrance.Encumbrance := FXmlNewParcel.Encumbrance;

    self.OrdinatesPageControl.ActivePage := nil;
    _node := self.FXmlNewParcel.ChildNodes.FindNode(cnstXmlContours);
    if _node <> nil then
    begin
      self.FContours_frame.XmlContours := self.FXmlNewParcel.Contours;
      self.OrdinatesPageControl.ActivePage := self.Contours_TabSheet;
    end
    else    self.FContours_frame.XmlContours := nil;

    _node := self.FXmlNewParcel.ChildNodes.FindNode(cnstXmlEntity_Spatial);
    if _node <> nil then
    begin
      self.FEntitySpatial_frame.EntitySpatial := self.FXmlNewParcel.Entity_Spatial;
      self.OrdinatesPageControl.ActivePage := self.EntitySpatial_TabSheet;
    end
    else self.FEntitySpatial_frame.EntitySpatial := nil;
  end
  else
  begin
    Self.medtCadastralNumber.Text := '';
    _HiddenChangeCheckBoxSate(chkAttribute_SubParcelRealty_CheckBox,cbUnchecked);
    self.NewParcelsPageControl.Visible := false;
    self.Panel_Navigation.Visible := false;
    self.FArea_frame.XmlArea := nil;
    self.FEncumbrance.Encumbrance := nil;
    self.FContours_frame.XmlContours := nil;
    self.FEntitySpatial_frame.EntitySpatial := nil;
  end;

end;

function TFrameNewSubParcel.IsAssigned_NewParcel: Boolean;
begin
  Result := self.FXmlNewParcel <> nil;
end;

procedure TFrameNewSubParcel.lblEncumbranceClick(Sender: TObject);
begin
 self.chk_Encumbrance_IsFill.Checked := not self.chk_Encumbrance_IsFill.Checked;
end;

procedure TFrameNewSubParcel.NewParcelsPageControlChange(Sender: TObject);
begin
  self.SpeedButton_Nav_Back.Visible :=
    self.NewParcelsPageControl.ActivePageIndex = 0;
  self.SpeedButton_Nav_Next.Visible :=
    self.NewParcelsPageControl.ActivePageIndex = self.NewParcelsPageControl.
    PageCount - 1;

end;

procedure TFrameNewSubParcel.SetActivTab(index: integer; UseAnimate: Boolean = false);
begin
  if (index < 0) or (index > self.NewParcelsPageControl.PageCount - 1) then  EXIT;

  if UseAnimate then   pcaSetActivTab(self.NewParcelsPageControl,self.NewParcelsPageControl.Pages[index])
  else
    self.NewParcelsPageControl.ActivePageIndex := index;

  self.ComboBox_Nav_GoTo.ItemIndex :=
    self.NewParcelsPageControl.ActivePageIndex;
  self.SpeedButton_Nav_Back.Enabled := self.ComboBox_Nav_GoTo.ItemIndex <> 0;
  self.SpeedButton_Nav_Next.Enabled := self.ComboBox_Nav_GoTo.ItemIndex <>
    self.ComboBox_Nav_GoTo.Items.Count - 1;
end;

procedure TFrameNewSubParcel.SetNewParcel(const aNewParcel: IXMLTNewSubParcel);
begin
  if self.FXmlNewParcel = aNewParcel then
    EXIT;
  self.FXmlNewParcel := aNewParcel;
  self.InitializationUI;
end;

procedure TFrameNewSubParcel.SetType(aValue: integer);
begin
    case aValue of
      0:
        begin // �������������� -> ����������������
          MsXml_RemoveChildNode(self.FXmlNewParcel,cnstXmlContours);
          self.FContours_frame.XmlContours := nil;
          self.FEntitySpatial_frame.EntitySpatial := self.FXmlNewParcel.Entity_Spatial;
          self.OrdinatesPageControl.ActivePage := self.EntitySpatial_TabSheet;
        end;
      1:
        begin // ���������������� -> ��������������
          MsXml_RemoveChildNode(self.FXmlNewParcel,cnstXmlEntity_Spatial);
          self.FEntitySpatial_frame.EntitySpatial := nil;
          self.FContours_frame.XmlContours := self.FXmlNewParcel.Contours;
          self.OrdinatesPageControl.ActivePage := self.Contours_TabSheet;
        end;
     else
      raise Exception.Create(ExMsg_Data_Invalid);
    end;
end;

procedure TFrameNewSubParcel.SpeedButton_Nav_BackClick(Sender: TObject);
begin
  self.SetActivTab(self.ComboBox_Nav_GoTo.ItemIndex - 1,false);
end;

procedure TFrameNewSubParcel.SpeedButton_Nav_NextClick(Sender: TObject);
begin
  self.SetActivTab(self.ComboBox_Nav_GoTo.ItemIndex + 1, false);
end;

procedure TFrameNewSubParcel.UI_CadNum_Read;
begin
  self.medtCadastralNumber.Text := self.FXmlNewParcel.CadastralNumber_Parcel;
end;

procedure TFrameNewSubParcel.UI_CadNum_Write;
begin
 if not IsAssigned_NewParcel then Exit;
 self.FXmlNewParcel.CadastralNumber_Parcel := medtCadastralNumber.Text;
end;

end.
