object FormNewSubParcels: TFormNewSubParcels
  Left = 0
  Top = 0
  ParentCustomHint = False
  BorderStyle = bsSingle
  Caption = '(XML) '#1054#1073#1088#1072#1079#1086#1074#1072#1085#1080#1077' '#1047#1059' '#1084#1077#1090#1086#1076#1086#1084' '
  ClientHeight = 489
  ClientWidth = 976
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  PixelsPerInch = 96
  TextHeight = 13
  object lblNewSubParcels: TLabel
    Left = 544
    Top = -16
    Width = 121
    Height = 13
    Caption = #1054#1073#1088#1072#1079#1086#1074#1072#1085#1080#1077' '#1095#1072#1089#1090#1077#1081' '#1047#1059
    Visible = False
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 976
    Height = 489
    Align = alClient
    BevelEdges = [beLeft, beRight]
    BevelKind = bkFlat
    BevelOuter = bvNone
    TabOrder = 0
    object jvPageControl_FP: TJvgPageControl
      Left = 0
      Top = 0
      Width = 972
      Height = 489
      Margins.Left = 0
      Margins.Top = 0
      Margins.Right = 0
      Margins.Bottom = 0
      ActivePage = ts_NewParcels
      Align = alClient
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = False
      Style = tsFlatButtons
      TabOrder = 0
      TabStop = False
      StyleElements = []
      TabStyle.Borders = []
      TabStyle.BevelInner = bvNone
      TabStyle.BevelOuter = bvNone
      TabStyle.Bold = False
      TabStyle.BackgrColor = clInactiveBorder
      TabStyle.Font.Charset = RUSSIAN_CHARSET
      TabStyle.Font.Color = clBlack
      TabStyle.Font.Height = -12
      TabStyle.Font.Name = 'Arial'
      TabStyle.Font.Style = [fsBold]
      TabStyle.CaptionHAlign = fhaCenter
      TabStyle.Gradient.ToColor = clSilver
      TabStyle.Gradient.Active = True
      TabStyle.Gradient.Orientation = fgdHorizontal
      TabSelectedStyle.Borders = [fsdLeft, fsdTop, fsdRight, fsdBottom]
      TabSelectedStyle.BevelInner = bvNone
      TabSelectedStyle.BevelOuter = bvNone
      TabSelectedStyle.Bold = False
      TabSelectedStyle.BackgrColor = 15921123
      TabSelectedStyle.Font.Charset = ANSI_CHARSET
      TabSelectedStyle.Font.Color = clBlack
      TabSelectedStyle.Font.Height = -12
      TabSelectedStyle.Font.Name = 'Arial'
      TabSelectedStyle.Font.Style = [fsBold]
      TabSelectedStyle.CaptionHAlign = fhaCenter
      TabSelectedStyle.Gradient.FromColor = 15921123
      TabSelectedStyle.Gradient.ToColor = clSkyBlue
      TabSelectedStyle.Gradient.Active = True
      TabSelectedStyle.Gradient.Orientation = fgdRightBias
      Wallpaper.FillCaptions = False
      Wallpaper.Tile = False
      Wallpaper.IncludeBevels = False
      LookLikeButtons = True
      Options = []
      object ts_NewParcels: TTabSheet
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ImageIndex = 1
        ParentFont = False
        TabVisible = False
        Touch.ParentTabletOptions = False
        Touch.TabletOptions = []
        object Panel_ControlNewParcel: TPanel
          Left = 0
          Top = 0
          Width = 964
          Height = 43
          Align = alTop
          BevelEdges = [beLeft, beRight]
          BevelKind = bkFlat
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label2: TLabel
            Left = 9
            Top = -1
            Width = 88
            Height = 13
            Caption = #1057#1087#1080#1089#1086#1082' '#1095#1072#1089#1090#1077#1081
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object lblName: TLabel
            Left = 381
            Top = -1
            Width = 60
            Height = 13
            Caption = #1058#1080#1087' '#1095#1072#1089#1090#1080
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object NewParcels_ComboBox: TComboBox
            Left = 7
            Top = 15
            Width = 200
            Height = 22
            BevelKind = bkFlat
            Style = csOwnerDrawFixed
            Color = 15921123
            DropDownCount = 20
            TabOrder = 0
            TextHint = #1054#1073#1086#1079#1085#1072#1095#1077#1085#1080#1077' '#1047#1059
            OnChange = Action_NewParcels_SetCurrentExecute
          end
          object ButtonGroup1: TButtonGroup
            Left = 207
            Top = 13
            Width = 154
            Height = 26
            BevelInner = bvNone
            BevelOuter = bvNone
            BorderStyle = bsNone
            ButtonHeight = 25
            ButtonWidth = 25
            ButtonOptions = []
            Images = formContainer.ilCommands
            Items = <
              item
                Action = ActionXML_NewParcel_SetCurrentBack
                Hint = #1055#1088#1077#1076#1099#1076#1091#1072#1103' '#1095#1072#1089#1090#1100
                ImageIndex = 7
              end
              item
                Action = ActionXML_NewParcel_SetCurrentNext
                Hint = #1057#1083#1077#1076#1091#1102#1097#1072#1103' '#1095#1072#1089#1090#1100
                ImageIndex = 8
              end
              item
                Action = ActionXML_NewParcel_AddNew
                Hint = #13#10#1044#1086#1073#1072#1074#1080#1090#1100' '#1085#1086#1074#1091#1102' '#1086#1073#1088#1072#1079#1091#1077#1084#1091#1102' '#1063#1047#1059#13#10
              end
              item
                Action = ActionXML_NewParcel_DelCurrent
                Hint = #13#10#1059#1076#1072#1083#1080#1090#1100' '#1074#1099#1073#1088#1072#1085#1085#1091#1102' '#1086#1073#1088#1072#1079#1091#1077#1084#1091#1102' '#1063#1047#1059#13#10
              end
              item
                Caption = #1048#1079#1084#1077#1085#1080#1090#1100' '#1086#1073#1086#1079#1085#1072#1095#1077#1085#1080#1077
                Hint = #13#10#1048#1079#1084#1077#1085#1080#1090#1100'  '#1086#1073#1086#1079#1085#1072#1095#1077#1085#1080#1077#13#10
                ImageIndex = 9
                OnClick = ButtonGroup1Items5Click
              end>
            ShowHint = True
            TabOrder = 1
          end
          object cbbFormParcel_Type: TComboBox
            Left = 380
            Top = 15
            Width = 196
            Height = 22
            BevelKind = bkFlat
            Style = csOwnerDrawFixed
            Color = 15921123
            TabOrder = 2
            TextHint = #1058#1080#1087' '#1063#1047#1059
            OnChange = cbbFormParcel_TypeChange
            Items.Strings = (
              #1047#1077#1084#1083#1077#1087#1086#1083#1100#1079#1086#1074#1072#1085#1080#1077
              #1050#1086#1085#1090#1091#1088#1085#1086#1077' '#1086#1087#1080#1089#1072#1085#1080#1077' '#1095#1072#1089#1090#1080)
          end
        end
      end
    end
  end
  object ActionManager_FormParcels: TActionManager
    Left = 657
    Top = 91
    StyleName = 'Platform Default'
    object Action_NewParcels_SetCurrent: TAction
      Category = 'NewParcel'
      Caption = #1057#1076#1077#1083#1072#1090#1100' '#1090#1077#1082#1091#1097#1080#1084' '#1074#1099#1073#1088#1072#1085#1085#1099#1081' '#1086#1073#1088#1072#1079#1091#1077#1084#1099#1081' '#1047#1059
      OnExecute = Action_NewParcels_SetCurrentExecute
    end
    object ActionXML_NewParcel_AddNew: TAction
      Category = 'NewParcel'
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1085#1086#1074#1099#1081'  '#1086#1073#1088#1072#1079#1091#1077#1084#1099#1081' '#1047#1059
      Hint = #1044#1086#1073#1072#1074#1080#1090#1100' '#1085#1086#1074#1099#1081'  '#1086#1073#1088#1072#1079#1091#1077#1084#1099#1081' '#1047#1059
      ImageIndex = 0
      OnExecute = ActionXML_NewParcel_AddNewExecute
    end
    object ActionXML_NewParcel_DelCurrent: TAction
      Category = 'NewParcel'
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1074#1099#1073#1088#1072#1085#1085#1099#1081' '#1086#1073#1088#1072#1079#1091#1077#1084#1099#1081' '#1047#1059
      Hint = #1059#1076#1072#1083#1080#1090#1100' '#1074#1099#1073#1088#1072#1085#1085#1099#1081' '#1086#1073#1088#1072#1079#1091#1077#1084#1099#1081' '#1047#1059
      ImageIndex = 1
      OnExecute = ActionXML_NewParcel_DelCurrentExecute
    end
    object ActionXML_NewParcel_SetCurrentNext: TAction
      Category = 'NewParcel'
      Caption = #1057#1083#1077#1076#1091#1102#1097#1080#1081' '#1086#1073#1088#1072#1079#1091#1077#1084#1099#1081' '#1047#1059
      Hint = #1057#1083#1077#1076#1091#1102#1097#1080#1081' '#1086#1073#1088#1072#1079#1091#1077#1084#1099#1081' '#1047#1059
      ImageIndex = 3
      OnExecute = ActionXML_NewParcel_SetCurrentNextExecute
    end
    object ActionXML_NewParcel_SetCurrentBack: TAction
      Category = 'NewParcel'
      Caption = #1055#1088#1077#1076#1099#1076#1091#1097#1080#1081' '#1086#1073#1088#1072#1079#1091#1077#1084#1099#1081' '#1047#1059
      Hint = #1055#1088#1077#1076#1099#1076#1091#1097#1080#1081' '#1086#1073#1088#1072#1079#1091#1077#1084#1099#1081' '#1047#1059
      ImageIndex = 2
      OnExecute = ActionXML_NewParcel_SetCurrentBackExecute
    end
    object ActionXML_NewParcel_GroupEdit: TAction
      Category = 'NewParcel'
      Caption = #1059#1089#1090#1072#1085#1086#1074#1082#1072' '#1075#1088#1091#1087#1086#1074#1099#1093' '#1079#1085#1072#1095#1077#1085#1080#1081
      ImageIndex = 4
      OnExecute = ActionXML_NewParcel_GroupEditExecute
    end
    object actEditDeifinition: TAction
      Category = 'NewParcel'
      Caption = #1048#1079#1084#1077#1085#1080#1090#1100' '#1086#1073#1086#1079#1085#1072#1095#1077#1085#1080#1077
      Hint = #1048#1079#1084#1077#1085#1080#1090#1100' '#1086#1073#1086#1079#1085#1072#1095#1077#1085#1080#1077
      OnExecute = ButtonGroup1Items5Click
    end
    object actHint: TAction
      Category = 'NewParcel'
      Caption = 'actHint'
    end
  end
end
