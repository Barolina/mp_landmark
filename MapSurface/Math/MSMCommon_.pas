﻿{*******************************************************}
{      |----------------------------------------|       }
{      |- (c) 2010-2011 Краснояров Александр.  -|       }
{      |-          kras_ai@mail.ru             -|       }
{      |----------------------------------------|       }
{*******************************************************}

unit MSMCommon_;

interface

const
  PiDiv180 = Pi / 180;
  PiDiv4 = Pi / 4;
  PiDiv2 = Pi / 2;


var
 msmsysDelta:double; //все что меньше считается нулём (НЕ СТАВИТЬ ОТРИЦАТЕЛЬНОЕ ЗНАЧЕНИЕ)

 procedure msmcsysScaleDelta(Procent: Double);
 procedure msmcsysSetDelta(Value: Double);
 procedure msmcsysRestoreDelta;
 procedure msmcsysRestoreDeltaDefault;

//==============================================================================
//                                 ПОЛЕЗНОЕ
//==============================================================================

 //function msmcRound(x: Double): double;

//Обмен значениями X и Y
{OK}procedure msmcDoSwap(var X,Y : double);

//проверка эквивалентности значений x и y с учетом DeltaZero
{OK}function msmcIsEqual(X,Y : double): boolean;
{OK}function msmcIsEqualP(X1,Y1,X2,Y2: double): boolean;

//проверка истинности условия (X <= Y)
{OK}function msmcIsLE(X,Y : double): boolean;

//проверка истинности условия (X >= Y)
{OK}function msmcIsGE(X,Y : double): boolean;

//проверка на нулевое значение с учетом DeltaZero
{OK}function msmcIsZero(X : double): boolean;

//==============================================================================
//                                ТОЧКИ
//==============================================================================

//Принадлежит ли точка P прямоугольной облати A-B.
//ориентация области значения не имееет
{OK}function msmcIsPointInRect(Ax,Ay,Bx,By,Px,Py: Double): Boolean;

//Принадлежит ли точка P отрезку A-B ?
{OK}function msmcIsPointInInterval(Ax,Ay,Bx,By,Px,Py: Double): Boolean;
//тоже самое (реализация через угол). оставлен для сравнения скорости работы алгоритма
{OK}function msmcIsPointInIntervalA(Ax,Ay,Bx,By,Px,Py: Double): Boolean;

//Принадлежит ли точка P прямой A-B ?
{OK}function msmcIsPointInLine(Ax,Ay,Bx,By,Px,Py: Double): Boolean;
// тоже самое. реализация через уравнение прямой
{OK}function msmcIsPointInLineA(Ax,Ay,Bx,By,Px,Py: Double): Boolean;

//расстояние от точки x1,y1 до x2,y2
{OK}function msmcGetDistanceBetweenPoints(x1,y1,x2,y2: Double):Double;

//Расстояние от A до B меньше Limit (безопасная проверка)
{OK}function msmcIsDistanceLessThen(Ax,Ay,Bx,By,Limit : double) : Boolean;
//тоже самое только + возвращяет расстояние
{OK}function msmcIsDistanceLessThenA(Ax,Ay,Bx,By,Limit: double; var Dist: double) : Boolean;

// Поворот точки P вокруг точки C на угол Angle в градусах
{OK}procedure msmcGetRotatePoint(Cx,Cy,Px,Py,Angle: double; var vX,vY: Double);

//Найти проекцию точки P на прямую A-B
{OK}procedure msmcGetProjectionPointToLine(Ax,Ay,Bx,By,Px,Py: Double; var vX,vY: Double);


//==============================================================================
//                                ЛИНИИ
//==============================================================================

//Угол между осью OX и ВЕКТОРОМ ( отрезком,прямой ),
//отсчитанный против часовой стрелки в градусах [0..360)
//если S=E то result=0;
//(он же называется диррекционным углом в ГЕОДЕЗИЧЕСКОЙ системе координат)
{OK}function msmcGetAngleLine(Sx,Sy,Ex,Ey: Double ): double;

//Угол (в градусах) между двумя отрезками ( прямыми ), заданными
//точками S_ ( начало ) и E_ ( конец ), отсчитанный
//от первой прямой до второй против часовой стрелки
{OK}function msmcGetAngleBetweenLines(S1x,S1y,E1x,E1y,S2x,S2y,E2x,E2y: double) : double;

//Определение являвтся ли линии паралельными
{OK}function msmcIsParallelLine(A1x,A1y,B1x,B1y,A2x,A2y,B2x,B2y: double): Boolean;

//Определение являются ли вектора A1B1 и A2B2 коллинеарными
{OK}function msmcIsCollinearlLine(A1x,A1y,B1x,B1y,A2x,A2y,B2x,B2y: double): Boolean;

//Изменение направленности (при необходимости) вектора A2B2. таким образом, что
//A1B1 и A2B2 коллинеарны. Если true то A1B1 и A2B2 коллинеарны; Производится проверка на паралельность
{OK}function msmcDoCheckCollinearlLine(A1x,A1y,B1x,B1y:Double; var A2x,A2y,B2x,B2y: double): Boolean;

//Получить координаты центра С отрезка AB
{OK}procedure msmcGetCentrePointLine(Ax,Ay,Bx,By: Double; var Cx,Cy: double);

//==============================================================================
//                        ПРЯМОУГОЛЬНЫЕ ОБЛАСТИ
//==============================================================================

//приведение бозовых точек прямоугольной области к виду (x1<=x2;y1<=y2)
//если true то ориентация была изменена
{OK}function msmcDoCheckRect(var x1,y1,x2,y2: Double): boolean;

//увеличение длин сторон прямоугольной области на Value*2
{OK}procedure msmcDoEnlargeRect(var x1,y1,x2,y2: Double; Value: double);

//определение еквивалентности двух областей;
//ориентация областей значения не имееет;
{OK}function msmcIsEqualRects(A1x,A1y,B1x,B1y,A2x,A2y,B2x,B2y: Double): Boolean;

//определение содержится ли область A1B1 в A2B2 (а также совпадает сней);
//ориентация областей значения не имееет;
function msmcIsRectInRect(A1x,A1y,B1x,B1y,A2x,A2y,B2x,B2y: double): Boolean;

//Определение имееет ли место пересечение,совпадение,включение прямоугольных областей;
//ориентация областей значения не имееет;
function msmcIsIntersectRect(A1x,A1y,B1x,B1y,A2x,A2y,B2x,B2y: double): Boolean;

//Получение общей облати двух прямоугольных областей. может быть вырождена в точку
{OK}function msmcGetIntersectRects(A1x,A1y,B1x,B1y,A2x,A2y,B2x,B2y: double; var rAx,rAy,rBx,rBy: double): Boolean;

implementation
 Var
  _Delta:double;

  {------------------------------------------------------------------------------}
 procedure msmcsysScaleDelta(Procent: Double);
 begin
  msmcsysSetDelta(msmsysDelta + msmsysDelta/100*Procent);
 end;
{------------------------------------------------------------------------------}
 procedure msmcsysSetDelta(Value: Double);
 begin
  _Delta:= msmsysDelta;
  msmsysDelta:= Value;
 end;
{------------------------------------------------------------------------------}
 procedure msmcsysRestoreDelta;
 begin
  msmcDoSwap(msmsysDelta,_Delta);
 end;
{------------------------------------------------------------------------------}
 procedure msmcsysRestoreDeltaDefault;
 begin
  msmsysDelta:= 0.00001;
  _Delta:= 0.00001;
 end;
{------------------------------------------------------------------------------}
procedure msmcDoSwap(var X,Y : double);
var
 _t: Double;
begin
 _t:= x;
 x:= y;
 y:= _t;
end;
{------------------------------------------------------------------------------}
function msmcIsEqual(X,Y : double) : boolean;
begin
 Result := not ((X - Y > msmsysDelta) or (Y - X > msmsysDelta));
end;
{------------------------------------------------------------------------------}
function msmcIsEqualP(X1,Y1,X2,Y2: double): boolean;
begin
 Result:= msmcIsEqual(X1,X2) and msmcIsEqual(Y1,Y2)
end;
{------------------------------------------------------------------------------}
function msmcIsLE(X,Y : double) : boolean;
begin
 Result := msmcIsEqual(X,Y) or (X < Y);
end;
{------------------------------------------------------------------------------}
function msmcIsGE(X,Y : double) : boolean;
begin
 Result := msmcIsEqual(X,Y) or (X > Y);
end;
{------------------------------------------------------------------------------}
function msmcIsZero(X : double) : boolean;
begin
 Result := not ((X  > msmsysDelta) or (- X > msmsysDelta));
end;
{------------------------------------------------------------------------------}
function msmcGetDistanceBetweenPoints(x1,y1,x2,y2: Double):Double;
begin
 Result:= sqrt(sqr(x1/1 - x2) + sqr(y1/1 - y2));
end;
{------------------------------------------------------------------------------}
procedure msmcGetRotatePoint(Cx,Cy,Px,Py,Angle: double; var vX,vY: Double);
var
 _SinA,_CosA,_Angle: Double;
begin
 _Angle:= Angle*pi/180;
 _SinA := SIN( _Angle );
 _CosA := COS( _Angle );
 vX := (PX-CX)*_CosA - (PY-CY)*_SinA + CX;
 vY := (PX-CX)*_SinA + (PY-CY)*_CosA + CY;
end;
{------------------------------------------------------------------------------}
function msmcGetAngleBetweenLines(S1x,S1y,E1x,E1y,S2x,S2y,E2x,E2y: double) : double;
var a : double;
begin
 a := msmcGetAngleLine(S2x,S2y,E2x,E2y);
 a := a - msmcGetAngleLine(S1x,S1y,E1x,E1y);
 if msmcIsGE(a,0) then
  Result := a
 else
  Result := a + 360.0;
end;
{------------------------------------------------------------------------------}
function msmcGetAngleLine(Sx,Sy,Ex,Ey: Double ) : double;
begin
   if msmcIsEqual(SX , EX ) then begin
     if msmcIsEqual(SY , EY ) then Result:= 0.0 else{ TODO -cMATH : возможно лишенее условие, хотя нет... }
      if SY < EY then Result := 90.0 else Result := 270.0
   end else begin
      Result := ARCTAN( (EY - SY) / (EX - SX) ) / PiDiv180;
      if SX < EX then
         if msmcIsLE( SY , EY)
            then
            else Result := Result + 360.0
      else
         Result := Result + 180.0;
   end;
end;
{------------------------------------------------------------------------------}
function msmcDoCheckRect(var x1,y1,x2,y2: Double): boolean;
begin
 Result:= False;
 if x2<x1 then begin
  msmcDoSwap(x2,x1);
  Result:= true;
 end;
 if y2<y1 then begin
  msmcDoSwap(y2,y1);
  Result:= true;
 end;
end;
{------------------------------------------------------------------------------}
procedure msmcDoEnlargeRect(var x1,y1,x2,y2: Double; Value: double);
begin
 if Value = 0 then Exit;

 if msmcIsLE(x1,x2) then begin
  x1:= x1-Value;
  x2:= x2+Value;
 end else begin
  x1:= x1+Value;
  x2:= x2-Value;
 end;

 if msmcIsLE(y1,y2) then begin
  y1:= y1-Value;
  y2:= y2+Value;
 end else begin
  y1:= y1+Value;
  y2:= y2-Value;
 end;
end;
{------------------------------------------------------------------------------}
function msmcIsPointInRect(Ax,Ay,Bx,By,Px,Py: Double): Boolean;
var
 _ContainX : Boolean;
begin
 Result := False;
 if msmcIsLE(AX,BX) then
  _ContainX:= msmcIsLE(AX,PX) and msmcIsLE(PX,BX)
 else
  _ContainX:= msmcIsLE(BX,PX) and msmcIsLE(PX,AX);
 if _ContainX then
  if msmcIsLE(AY,BY) then
   Result:= msmcIsLE(AY,PY) and msmcIsLE(PY,BY)
  else
   Result:= msmcIsLE(BY,PY) and msmcIsLE(PY,AY)
end;
{------------------------------------------------------------------------------}
function msmcIsPointInInterval(Ax,Ay,Bx,By,Px,Py: Double): Boolean;
begin
 Result:= false;
 if msmcIsPointInRect(Ax,Ay,Bx,By,Px,Py) then
  Result:= msmcIsPointInLine(Ax,Ay,Bx,By,Px,Py);
end;
{------------------------------------------------------------------------------}
function msmcIsPointInIntervalA(Ax,Ay,Bx,By,Px,Py: Double): Boolean;
begin
 Result:= (msmcIsEqual(Ax,Px) and msmcIsEqual(Ay,Py)) or(msmcIsEqual(Bx,Px) and msmcIsEqual(By,Py));
  if not (Result or (msmcIsEqual(Ax,Bx) and msmcIsEqual(Ay,By))) then //если не совпадает с точками AB и линия НЕ вырождена в точку.
   Result:= (msmcIsZero(msmcGetAngleBetweenLines(Px,Py,Ax,Ay,Px,Py,Bx,By) - 180.0));
end;
{------------------------------------------------------------------------------}
procedure msmcGetProjectionPointToLine(Ax,Ay,Bx,By,Px,Py: Double; var vX,vY: Double);
var
  _u1,_u2: double;
  _Tangens1: double;
  _Tangens2: double;
begin
 if msmcIsEqual(AX,BX) then begin //если || оси oX
   vX:= AX; vY:=PY;
 end else
  if msmcIsEqual(AY,BY) then begin //если || оси oY
    vX:= PX; vY:=AY;
  end else begin
   _Tangens1 := (BY-AY) / (BX-AX); //тангенс угла наклона прямой
   _Tangens2 := -1 / _Tangens1; //тангенс улла наклона перпендикулярной прямой
   _u1 := AY - _Tangens1 * AX; //коэф-т смещения прямой
   _u2 := PY - _Tangens2 * PX; //коэф-т смещения перпендикулярной прямой
   //vY= _Tangens1*vX+_u1
   //vY= _Tangens2*vX+_u2
   //_Tangens1*vX+_u1 = _Tangens2*vX+_u2 =>
   //_u1-_u2 = _Tangens2*vX-_Tangens1*vX =>
   //_u1-_u2 = (_Tangens2-_Tangens1)*vX =>
   vX:= (_u1-_u2) / (_Tangens2-_Tangens1);
   //vY = _Tangens1(2) * vX + _u1;
   vY:= _Tangens1 * vX + _u1;
  end;
end;
{------------------------------------------------------------------------------}
function msmcIsPointInLineA(Ax,Ay,Bx,By,Px,Py: Double): Boolean;
var
 _EqX,_EqY: Boolean;
 _Tang,_U:Double;
begin
 _EqX:= msmcIsEqual(Ax,Bx);
 _EqY:= msmcIsEqual(Ay,By);
 if _EqX and _EqY  then //линия вырождена в точку
  Result:= msmcIsEqual(Ax,Px) and msmcIsEqual(Ay,Py)
 else
  if _EqX then
   Result:= msmcIsEqual(Ax,Px)
  else
   if _EqY then
    Result:= msmcIsEqual(Ay,Py)
   else
 begin
  _Tang:= (By-Ay)/(Bx-Ax);
  _U:= Ay-_Tang*Ax;
  Result:= msmcIsZero(_Tang*Px+_U-Py);
 end;
end;
{------------------------------------------------------------------------------}
function msmcIsPointInLine(Ax,Ay,Bx,By,Px,Py: Double): Boolean;
var
 _x,_y: Double;
begin
 msmcGetProjectionPointToLine(Ax,Ay,Bx,By,Px,Py,_x,_y);
 //sult:= msmcIsDistanceLessThen(Px,Py,_x,_y,DeltaZero);//- трудлемкий, безопасный вариант
 //Result:= msmcIsZero(msmcGetDistanceBetweenPoints(Px,Py,_x,_y)); - опасный вариант
 Result:=  msmcIsEqual(Px,_x) and msmcIsEqual(Py,_y); //быстрый, безопасный вариант
end;
{------------------------------------------------------------------------------}
function msmcIsDistanceLessThen(Ax,Ay,Bx,By,Limit : double) : Boolean;
begin
 if msmcIsPointInRect(Ax-Limit,Ay-Limit,Ax+Limit,Ay+Limit,Bx,By) then
  Result:= msmcGetDistanceBetweenPoints(Ax,Ay,Bx,By)<Limit
 else
  Result:= False;
end;
{------------------------------------------------------------------------------}
function msmcIsDistanceLessThenA(Ax,Ay,Bx,By,Limit: double; var Dist: double) : Boolean;
begin
 if msmcIsPointInRect(Ax-Limit,Ay-Limit,Ax+Limit,Ay+Limit,Bx,By) then begin
  Dist:= msmcGetDistanceBetweenPoints(Ax,Ay,Bx,By);
  Result:= Dist<Limit
 end else begin
  Dist:= Limit;
  Result:= False;
 end;
end;
{------------------------------------------------------------------------------}
function msmcIsEqualRects(A1x,A1y,B1x,B1y,A2x,A2y,B2x,B2y: Double): Boolean;
begin
 msmcDoCheckRect(A1x,A1y,B1x,B1y);
 msmcDoCheckRect(A2x,A2y,B2x,B2y);
 Result:= msmcIsEqual(A1x,A2x) and msmcIsEqual(A1y,A2y) and msmcIsEqual(B1x,B2x) and msmcIsEqual(B1y,B2y);
end;
{------------------------------------------------------------------------------}
function msmcIsRectInRect(A1x,A1y,B1x,B1y,A2x,A2y,B2x,B2y: double): Boolean;
begin
 msmcDoCheckRect(A1x,A1y,B1x,B1y);
 msmcDoCheckRect(A2x,A2y,B2x,B2y);
 Result:= msmcIsLE(A2x,A1x) and msmcIsLE(A2y,A1y) and  msmcIsLE(B1x,B2x) and msmcIsLE(B1y,B2y); //A1B1 содержится в A2B2 + совпадение
end;
{------------------------------------------------------------------------------}
function msmcIsIntersectRect(A1x,A1y,B1x,B1y,A2x,A2y,B2x,B2y: double): Boolean;
begin
 msmcDoCheckRect(A1x,A1y,B1x,B1y);
 msmcDoCheckRect(A2x,A2y,B2x,B2y);

 Result:= msmcIsLE(A1x,B2x) and msmcIsLE(A1y,B2y) and msmcIsLE(A2x,B1x) and msmcIsLE(A2y,B1y); //пересечение + совпадение
 if not Result then
  Result:= msmcIsRectInRect(A2x,A2y,B2x,B2y,A1x,A1y,B1x,B1y);
 if not Result then
  Result:= msmcIsRectInRect(A1x,A1y,B1x,B1y,A2x,A2y,B2x,B2y);
end;
{------------------------------------------------------------------------------}
function msmcGetIntersectRects(A1x,A1y,B1x,B1y,A2x,A2y,B2x,B2y: double; var rAx,rAy,rBx,rBy: double): Boolean;
begin
 Result:= msmcIsIntersectRect(A1x,A1y,B1x,B1y,A2x,A2y,B2x,B2y);
 if not Result then EXIT;
 msmcDoCheckRect(A1x,A1y,B1x,B1y);
 msmcDoCheckRect(A2x,A2y,B2x,B2y);
 msmcDoCheckRect(A1x,A1y,A2x,A2y);
 msmcDoCheckRect(B1x,B1y,B2x,B2y);

 msmcDoCheckRect(B1x,B1y,A2x,A2y); //лишнее по идее не нужна

 rAx:= B1x; rAy:= B1y;
 rBx:= A2x; rBy:= A2y;
end;
{------------------------------------------------------------------------------}
function msmcIsParallelLine(A1x,A1y,B1x,B1y,A2x,A2y,B2x,B2y: double): Boolean;
var
 _IsEq1 : Boolean;
 _IsEq2 : Boolean;
begin
 _IsEq1:= msmcIsEqual(A1x,B1x);
 _IsEq2:= msmcIsEqual(A2x,B2x);
 Result := _IsEq1 and _IsEq2; //если обе паралельны оси oX
 if not (_IsEq1 or _IsEq2) then
   //Result := (B1y - A1y) / (B1x - A1x) = (B2y - A2y) / (B2x - A2x); - без учета погрешности
  Result:= msmcIsEqual((B1y - A1y) / (B1x - A1x),(B2y - A2y) / (B2x - A2x));{ TODO -cmath : правельно ли сравнивать коэфициенты с допуском на погрешность }
end;
{------------------------------------------------------------------------------}
function msmcIsCollinearlLine(A1x,A1y,B1x,B1y,A2x,A2y,B2x,B2y: double): Boolean;
begin
 Result:= msmcIsZero(msmcGetAngleBetweenLines(A1x,A1y,B1x,B1y,A2x,A2y,B2x,B2y));
end;
{------------------------------------------------------------------------------}
function msmcDoCheckCollinearlLine(A1x,A1y,B1x,B1y:double; var A2x,A2y,B2x,B2y: double): Boolean;
begin
 if msmcIsParallelLine(A1x,A1y,B1x,B1y,A2x,A2y,B2x,B2y) then begin
  if not msmcIsCollinearlLine(A1x,A1y,B1x,B1y,A2x,A2y,B2x,B2y) then begin
   msmcDoSwap(A2x,B2x);
   msmcDoSwap(A2y,B2y);
  end;
  Result:= true;
 end else
  Result:= False;
end;
{------------------------------------------------------------------------------}
procedure msmcGetCentrePointLine(Ax,Ay,Bx,By: Double; var Cx,Cy: double);
begin
 Cx:= (Ax+Bx)/2;
 Cy:= (Ay+By)/2;
end;

initialization

msmcsysRestoreDeltaDefault;

end.
