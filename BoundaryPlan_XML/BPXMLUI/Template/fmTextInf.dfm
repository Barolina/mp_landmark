object FormTextInf: TFormTextInf
  Left = 0
  Top = 0
  Caption = #1048#1085#1092#1086#1088#1084#1072#1094#1080#1103
  ClientHeight = 499
  ClientWidth = 644
  Color = clBtnFace
  DefaultMonitor = dmMainForm
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object PanelControl: TPanel
    Left = 0
    Top = 474
    Width = 644
    Height = 25
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object CloseBitBtn: TBitBtn
      Left = 569
      Top = 0
      Width = 75
      Height = 25
      Align = alRight
      Caption = #1047#1072#1082#1088#1099#1090#1100
      TabOrder = 0
      OnClick = CloseBitBtnClick
    end
    object SaveBitBtn: TBitBtn
      Left = 0
      Top = 0
      Width = 104
      Height = 25
      Align = alLeft
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1074' '#1092#1072#1081#1083
      TabOrder = 1
      OnClick = SaveBitBtnClick
    end
  end
  object lstValidatorLog: TJvHTListBox
    Left = 0
    Top = 0
    Width = 644
    Height = 474
    HideSel = False
    Align = alClient
    BorderStyle = bsNone
    ColorHighlight = clHighlight
    ColorHighlightText = clHighlightText
    ColorDisabledText = clGrayText
    Items.Strings = (
      '')
    MultiSelect = True
    TabOrder = 1
    ExplicitWidth = 485
    ExplicitHeight = 225
  end
  object SaveDialog1: TSaveDialog
    DefaultExt = '*.txt'
    Filter = '*.txt|*.txt'
    Left = 32
    Top = 40
  end
end
