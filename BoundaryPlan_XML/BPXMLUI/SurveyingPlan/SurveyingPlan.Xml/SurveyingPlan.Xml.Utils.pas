unit SurveyingPlan.Xml.Utils;

interface
 uses
  System.SysUtils,
  MsXmlApi,
  Std_MP,
  System.Generics.collections,

  SurveyingPlan.Xml.Types;

  type
  TSpConvert = record
  public
   class function Convert(aSource: IXMLTOrdinate; aDest:TSpPoint): boolean; overload; static;
   class function Convert(aSource: TSpPoint; aDest: IXMLTOrdinate): boolean; overload; static;

   class function Convert(aSource: IXMLTSPELEMENT_UNIT; aDest:TSpBorderPoint): boolean; overload; static;
   class function Convert(aSource: TSpBorderPoint; aDest: IXMLTSPELEMENT_UNIT): boolean; overload; static;

   class function Convert(aSource: IXMLTSPATIAL_ELEMENT; aDest:TSpBorder): boolean; overload; static;
   class function Convert(aSource: TSpBorder; aDest: IXMLTSPATIAL_ELEMENT): boolean; overload; static;

   class function Convert(aSource: IXMLEntity_Spatial; aDest:TSpContour): boolean; overload; static;
   class function Convert(aSource: IXMLEntity_Spatial; aDest:TSpSubParcel): boolean; overload; static;
   class function Convert(aSource: TSpContour; aDest: IXMLEntity_Spatial; fillBorders: boolean = true): boolean; overload; static;
   class function Convert(aSource: TSpSubParcel; aDest: IXMLEntity_Spatial; fillBorders: boolean = true): boolean; overload; static;

   type Border = record
    class procedure Convert(aSource: TSpBorder; aDest: IXMLTSpecifyRelatedParcel_DeleteAllBorderList; doRewrite: boolean = true); overload; static;
    class procedure Convert(aSource: TSpBorder; aDest: IXMLContours_DeleteAllBorder; doRewrite: boolean = true); overload; static;

    class procedure Convert(aSource: IXMLTSpecifyRelatedParcel_DeleteAllBorderList; aDest:TSpBorder ; doRewrite: boolean = true); overload; static;
    class procedure Convert(aSource: IXMLContours_DeleteAllBorder; aDest:TSpBorder ; doRewrite: boolean = true); overload; static;
   end;

   type MContour = record
     class procedure Convert(aSource: TSpMContour; aDest: IXMLContours_NewContourList); overload; static;
     class procedure Convert(aSource: TSpMContour; aDest: IXMLContours_ExistContourList); overload; static;
     class procedure Convert(aSource: TSpMContour; aDest: IXMLContours_DeleteAllBorderList); overload; static;
     class procedure Convert(aSource: TSpMContour; aDest: IXMLTSubParcel_Contours); overload; static;
     class procedure Convert(aSource: TSpMContour; aDest: IXMLSTD_MP_Package_SpecifyParcel_ExistEZ_ExistEZEntryParcels; doRewrite: boolean = true); overload; static;
     class procedure Convert(aSource: TSpMContour; aDest: IXMLTExistEZParcel_Composition_EZ_InsertEntryParcels; doRewrite: boolean = true); overload; static;

     class procedure Convert(aSource: IXMLContours_NewContourList; aDest: TSpMContour); overload; static;
     class procedure Convert(aSource: IXMLContours_ExistContourList; aDest: TSpMContour); overload; static;
     class procedure Convert(aSource: IXMLContours_DeleteAllBorderList; aDest: TSpMContour); overload; static;
     class procedure Convert(aSource: IXMLTSubParcel_Contours; aDest: TSpMContour); overload; static;

     class procedure Convert(aSource: IXMLSTD_MP_Package_SpecifyParcel_ExistEZ_ExistEZEntryParcels; aDest: TSpMContour; doRewrite: boolean = true); overload; static;
     class procedure Convert(aSource: IXMLTExistEZParcel_Composition_EZ_InsertEntryParcels; aDest:TSpMContour; doRewrite: boolean = true); overload; static;

   end;

   type MSubParcel = record
     class procedure Convert(aSource: TSpMSubParcel; aDest: IXMLTExistParcel_SubParcels_FormSubParcelList); overload; static;
     class procedure Convert(aSource: TSpMSubParcel; aDest: IXMLTExistParcel_SubParcels_ExistSubParcelList); overload; static;
     class procedure Convert(aSource: TSpMSubParcel; aDest: IXMLTExistParcel_SubParcels_InvariableSubParcelList); overload; static;
     class procedure Convert(aSource: TSpMSubParcel; aDest: IXMLTNewParcel_SubParcels); overload; static;

     
     class procedure Convert(aSource: IXMLTExistParcel_SubParcels_FormSubParcelList; aDest: TSpMSubParcel); overload; static;
     class procedure Convert(aSource: IXMLTExistParcel_SubParcels_ExistSubParcelList; aDest: TSpMSubParcel); overload; static;
     class procedure Convert(aSource: IXMLTExistParcel_SubParcels_InvariableSubParcelList; aDest: TSpMSubParcel); overload; static;    
     class procedure Convert(aSource: IXMLTNewParcel_SubParcels; aDest: TSpMSubParcel); overload; static;    

   end;
  end;

implementation

uses MSMCommon_, SurveyingPlan.Common;


resourcestring
 rsNum_Geopoint = 'Num_Geopoint';
 rsGeopoint_Zacrep = 'Geopoint_Zacrep';
 rsDelta_Geopoint = 'Delta_Geopoint';
 rsPoint_Pref = 'Point_Pref';
 rsNewOrdinate = 'NewOrdinate';
 rsOldOrdinate = 'OldOrdinate';
 rsSpatial_Element = 'Spatial_Element';
 rsNewEntryParcel = 'NewEntryParcel';
 rsX = 'X';
 rsY = 'Y';
 rsBorders = 'Borders';

 rs5F54EAED = '� ����������� ������� ��� ����� ������ ����� ��� "���������"';
 rsC8E6F7A6 = '������� ������� ������ �������� �� ����� ������� �������';

{ TSpIOConvertorStdMp }

class function TSpConvert.Convert(aSource: IXMLTOrdinate; aDest: TSpPoint): boolean;
begin
 aDest.X:= StrToFloatDef(MsXml_ReadAttribute(aSource,rsX),0);
 aDest.Y:=  StrToFloatDef(MsXml_ReadAttribute(aSource,rsY),0);
 if aDest is TSpBorderPoint.TNewPoint then begin
  TSpBorderPoint.TNewPoint(aDest).Number:= MsXml_ReadAttribute(aSource,rsNum_Geopoint);
  TSpBorderPoint.TNewPoint(aDest).Zacrep:= MsXml_ReadAttribute(aSource,rsGeopoint_Zacrep);
  TSpBorderPoint.TNewPoint(aDest).Delta:= MsXml_ReadAttribute(aSource,rsDelta_Geopoint);
  TSpBorderPoint.TNewPoint(aDest).Prefix:= MsXml_ReadAttribute(aSource,rsPoint_Pref);
 end;
 if aDest is TSpBorderPoint.TOldPoint then
  TSpBorderPoint.TOldPoint(aDest).Number:= MsXml_ReadAttribute(aSource,rsNum_Geopoint);
end;

class function TSpConvert.Convert(aSource: TSpPoint; aDest: IXMLTOrdinate): boolean;
begin

     aDest.X:= FloatToStr(aSource.X);
     aDest.Y:= FloatToStr(aSource.Y);

     if aSource is TSpBorderPoint.TNewPoint then begin

      if TSpBorderPoint.TNewPoint(aSource).Number.Empty then
       MsXml_RemoveAttribute(aDest,rsNum_Geopoint)
      else
       aDest.Num_Geopoint:= TSpBorderPoint.TNewPoint(aSource).Number;

      if TSpBorderPoint.TNewPoint(aSource).Zacrep <>'' then
       aDest.Geopoint_Zacrep:= TSpBorderPoint.TNewPoint(aSource).Zacrep
      else
       MsXml_RemoveAttribute(aDest,rsGeopoint_Zacrep);

      if not TSpBorderPoint.TNewPoint(aSource).Delta.Empty then
        aDest.Delta_Geopoint:= TSpBorderPoint.TNewPoint(aSource).Delta
      else
        MsXml_RemoveAttribute(aDest,rsDelta_Geopoint);

      if TSpBorderPoint.TNewPoint(aSource).Prefix.IsNew then
       aDest.Point_Pref:= TSpBorderPoint.TNewPoint(aSource).Prefix
      else
       MsXml_RemoveAttribute(aDest,rsPoint_Pref);
     end;

     if aSource is TSpBorderPoint.TOldPoint then
      if TSpBorderPoint.TOldPoint(aSource).Number.empty  then
        MsXml_RemoveAttribute(aDest,rsNum_Geopoint)
      else
       aDest.Num_Geopoint:= TSpBorderPoint.TOldPoint(aSource).Number;

end;

class function TSpConvert.Convert(aSource: IXMLTSPELEMENT_UNIT; aDest: TSpBorderPoint): boolean;
var
 _IsExistNew,_IsExistOld: Boolean;
begin
 _IsExistNew:= Xml_IsExistChildNode(aSource,rsNewOrdinate);
 _IsExistOld:= Xml_IsExistChildNode(aSource,rsOldOrdinate);

 if _IsExistNew and _IsExistOld then
  aDest.Kind:= psbExisted
 else
  if _IsExistNew then
   aDest.Kind:= psbCreated
  else
   if _IsExistOld then
    aDest.Kind:= psbRemoved
   else
     EXIT;

 if _IsExistNew then
  TSpConvert.convert(aSource.NewOrdinate,aDest.NewPoint);
 if _IsExistOld then
  TSpConvert.convert(aSource.OldOrdinate,aDest.OldPoint);
end;

class function TSpConvert.Convert(aSource: TSpBorderPoint; aDest: IXMLTSPELEMENT_UNIT): boolean;
begin

 case aSource.Kind of
   psbCreated:begin
    MsXml_RemoveChildNode(aDest,rsOldOrdinate);
    TSpConvert.convert(aSource.NewPoint,aDest.NewOrdinate);
   end;
   psbRemoved:begin
    MsXml_RemoveChildNode(aDest,rsNewOrdinate);
    TSpConvert.convert(aSource.OldPoint,aDest.OldOrdinate);
   end;
   psbExisted:begin
    TSpConvert.convert(aSource.NewPoint,aDest.NewOrdinate);
    TSpConvert.convert(aSource.OldPoint,aDest.OldOrdinate);
   end;
 end;
end;

class function TSpConvert.Convert(aSource: IXMLTSPATIAL_ELEMENT; aDest: TSpBorder): boolean;
var
 _i: integer;
 _spunit: IXMLTSPELEMENT_UNIT;
 _point: TSpBorderPoint;
begin
 aDest.Clear;
 for _i := 0 to aSource.Count-1 do begin
  _spunit:= aSource.Spelement_Unit[_i];
  _point:= TSpBorderPoint.Create;
  TSpConvert.Convert(_spunit,_point);
  aDest.Add(_point);
 end;
end;

class function TSpConvert.Convert(aSource: TSpBorder; aDest: IXMLTSPATIAL_ELEMENT): boolean;
var
 _i: integer;
 _spunit: IXMLTSPELEMENT_UNIT;
begin
 aDest.Clear;
 for _i := 0 to aSource.Count-1 do begin
  _spunit:= aDest.Add;
  TSpConvert.Convert(aSource[_i],_spunit);
 end;
end;

class function TSpConvert.Convert(aSource: IXMLEntity_Spatial; aDest: TSpContour): boolean;
var
 _i: integer;
 _SpEl: IXMLTSPATIAL_ELEMENT;
 _border: TSpBorder;
begin
 aDest.Clear;
 for _i := 0 to aSource.Spatial_Element.Count-1 do begin
  _SpEl:= aSource.Spatial_Element[_i];
  _border:= TSpBorder.Create;
  TSpConvert.Convert(_SpEl,_border);
  aDest.Add(_border);
 end;
end;

class function TSpConvert.Convert(aSource: TSpContour; aDest: IXMLEntity_Spatial; fillBorders: boolean): boolean;
var
 _l: Double;
 _a,_b: TSpPoint;
 _i,_j: integer;
 _SpEl: IXMLTSPATIAL_ELEMENT;
 _border: TSpBorder;
 _xmlborder: IXMLEntity_Spatial_Borders_Border;
 _listp: TList<TSpBorderPoint.TNewPoint>;
begin
 if aDest = nil then Exit;
  
 //aDest.Spatial_Element.Clear; ent_sys delete
 MsXml_RemoveChildNode(aDest,rsSpatial_Element,TRUE);
 for _border in aSource do begin
  _SpEl:= aDest.Spatial_Element.Add;
  TSpConvert.Convert(_border,_SpEl);
 end;
 MsXml_RemoveChildNode(aDest,rsBorders);

 if fillBorders then begin
  _listp:= TList<TSpBorderPoint.TNewPoint>.Create;
  _xmlborder:= nil;
  if aSource.Count<>0 then  begin
  for _i := 0 to aSource.Count-1 do begin
    _listp.Clear;
    aSource[_i].NewPointsToList(_listp);
    for _j := 0 to _listp.Count-2 do begin
     _a:= _listp[_j];
     _b:= _listp[_j+1];
     _xmlborder:= aDest.Borders.Add;
     _xmlborder.Spatial:= _i+1;
     _xmlborder.Point1:= _j+1;
     _xmlborder.Point2:= _j+2;
     _xmlborder.Edge.Length:= FloatToStrF(
          msmcGetDistanceBetweenPoints(_a.X,_a.Y,_b.X,_b.Y),
          ffFixed,10,2);
    end;
  end;
  { TODO -c��������������� xsd : ������� ������������� �������� ��������� ����� �� ������? }
  //������������� ��������� ��� ��������� ����� ������� (�� ����������!!!!)
  //�������� ����� � ������ ������
  if _xmlborder<>nil then
  _xmlborder.Point2:= 1;
  end;
  _listp.Free;
 end;

end;


{ TSpConvert.MContour }

class procedure TSpConvert.MContour.Convert(aSource: TSpMContour; aDest: IXMLContours_NewContourList);
var
 _contour: TSpContour;
 _xmlcontour: IXMLContours_NewContour;
begin
 aDest.Clear;
 for _contour in aSource do begin
//  if _contour.Count<>1then
//   raise Exception.Create(rsC8E6F7A6);
  _xmlcontour:= aDest.Add;
  _xmlcontour.Definition:= _contour.getCadNumItem(TCadNumItemsKind.cnContour);
  if _xmlcontour.Definition='' then
  _xmlcontour.Definition:= _contour.getCadNumItem(TCadNumItemsKind.cnNewContour);
  _xmlcontour.Area.Area:= FloatToStr(_contour.getArea);
  if  _contour.getDP > 0  then  _xmlcontour.Area.Innccuracy:= _contour.getDP;
  TSpConvert.Convert(_contour,_xmlcontour.Entity_Spatial);
 end;
end;

class procedure TSpConvert.MContour.Convert(aSource: TSpMContour; aDest: IXMLContours_ExistContourList);
var
 _contour: TSpContour;
 _xmlcontour: IXMLContours_ExistContour;
begin
 aDest.Clear;
 for _contour in aSource do begin
  if _contour.Count<>1then
   raise Exception.Create(rsC8E6F7A6);
  _xmlcontour:= aDest.Add;
  _xmlcontour.Number_Record:=  StrToIntDef(_contour.getCadNumItem(TCadNumItemsKind.cnContour),0);
  _xmlcontour.Area.Area:= FloatToStr(_contour.getArea);
  if  _contour.getDP > 0  then  _xmlcontour.Area.Innccuracy:= _contour.getDP;
  TSpConvert.Convert(_contour,_xmlcontour.Entity_Spatial);
 end;
end;

class procedure TSpConvert.MContour.Convert(aSource: TSpMContour; aDest: IXMLContours_DeleteAllBorderList);
var
 _contour: TSpContour;
 _border: TSpBorder;
 _xmlcontour: IXMLContours_DeleteAllBorder;
 _xmlOldOrdinate: IXMLTOrdinate;
 _bpoint: TSpBorderPoint;
begin

 aDest.Clear;
 for _contour in aSource do begin
  if _contour.Count<>1then
   raise Exception.Create(rsC8E6F7A6);
  _xmlcontour:= aDest.Add;
  _xmlcontour.Number_Record:= StrToIntDef(_contour.getCadNumItem(TCadNumItemsKind.cnContour),0);
  _border:= _contour.Items[0];
  for _bpoint in _border do begin
   if _bpoint.Kind<>psbRemoved then
    raise Exception.Create(rs5F54EAED);
   _xmlOldOrdinate:= _xmlcontour.Add;
   TSpConvert.Convert(_bpoint.OldPoint,_xmlOldOrdinate);
  end;
 end;
end;

class procedure TSpConvert.MContour.Convert(
  aSource: IXMLContours_ExistContourList; aDest: TSpMContour);
var
 _i: integer;
 _contour: TSpContour;
 _xmlContour: IXMLContours_ExistContour;
begin
 aDest.Clear;
 for _i:= 0 to aSource.Count-1 do begin
  _xmlContour:= aSource.Items[_i];
  _contour:= TSpContour.Create;
  _contour.Caption:= IntToStr(_xmlContour.Number_Record);
  aDest.Add(_contour);
  TSpConvert.Convert(_xmlContour.Entity_Spatial,_contour);
 end;
end;

class procedure TSpConvert.MContour.Convert(
  aSource: IXMLContours_DeleteAllBorderList; aDest: TSpMContour);
var
 _i,_j: integer;
 _contour: TSpContour;
 _border: TSpBorder;
 _point: TSpBorderPoint;
 _xmlOldOrdinate: IXMLTOrdinate;
 _xmlContour: IXMLContours_DeleteAllBorder;
begin
 aDest.Clear;
 for _i:= 0 to aSource.Count-1 do begin
  _xmlContour:= aSource.Items[_i];
  _contour:= TSpContour.Create;
  _contour.Caption:= IntToStr(_xmlContour.Number_Record);
  aDest.Add(_contour);
  _border:= TSpBorder.Create;
  _contour.Add(_border);
  for _j := 0 to _xmlContour.Count-1 do begin
   _xmlOldOrdinate:= _xmlContour.OldOrdinate[_j];
   _point:= TSpBorderPoint.Create;
   _point.Kind:= psbRemoved;
   _border.Add(_point);
   TSpConvert.Convert(_xmlOldOrdinate,_point.OldPoint);
  end;
 end;
end;

class procedure TSpConvert.MContour.Convert(aSource: TSpMContour;
                   aDest: IXMLTExistEZParcel_Composition_EZ_InsertEntryParcels;
                   doRewrite: boolean);
var
 _new: IXMLTNewEZEntryParcel;
 _contour: TSpContour;
begin
 if doRewrite then aDest.Clear;
 for _contour in aSource do
  if _contour.getCadNumItem(cnNewNum)<>'' then begin
   _new:= aDest.Add.NewEntryParcel;
   _new.Name:= '03';
   //_new.Definition:= _contour.getCadNumItem(cnNewNum);
   _new.Definition:= _contour.Caption;
   TSpConvert.Convert(_contour,_new.Entity_Spatial);
   _new.Area.Area:= Round(_contour.getArea);
   _new.Area.Innccuracy:= _contour.getDP;
  end else
   aDest.Add.ExistEntryParcel.CadastralNumber:= _contour.Caption;
end;

class procedure TSpConvert.MContour.Convert(aSource: TSpMContour; aDest: IXMLSTD_MP_Package_SpecifyParcel_ExistEZ_ExistEZEntryParcels; doRewrite: boolean);
var
 _item: IXMLTExistEZEntryParcel;
 _contour: TSpContour;
begin
 if doRewrite then aDest.Clear;
 for _contour in aSource do begin
  _item:= aDest.Add;
  _item.CadastralNumber:= Trim(_contour.Caption);
  TSpConvert.Convert(_contour,_item.Entity_Spatial);
  _item.Area.Area:= Round(_contour.getArea);
  _item.Area.Innccuracy:= _contour.getDP;
 end;
end;

class procedure TSpConvert.MContour.Convert(
  aSource: IXMLTExistEZParcel_Composition_EZ_InsertEntryParcels;
  aDest: TSpMContour; doRewrite: boolean);
var
 _i: integer;
 _contour: TSpContour;
begin
 if doRewrite  then aDest.Clear;
 for _i:= 0 to aSource.Count-1 do
  if Xml_IsExistChildNode(aSource.InsertEntryParcel[_i],rsNewEntryParcel) then begin
   _contour:= TSpContour.Create;
   aDest.Add(_contour);
   _contour.Caption:= aSource.InsertEntryParcel[_i].NewEntryParcel.Definition;
   TSpConvert.Convert(aSource.InsertEntryParcel[_i].NewEntryParcel.Entity_Spatial,_contour);
  end;
end;

class procedure TSpConvert.MContour.Convert(aSource: IXMLTSubParcel_Contours; aDest: TSpMContour);
var
 _i: integer;
 _contour: TSpContour;
 _xmlContour: IXMLTSubParcel_Contours_Contour;
begin
 aDest.Clear;
 for _i:= 0 to aSource.Count-1 do begin
  _xmlContour:= aSource.Contour[_i];
  _contour:= TSpContour.Create;
  _contour.Caption:= _xmlContour.Number;
  aDest.Add(_contour);
  TSpConvert.Convert(_xmlContour.Entity_Spatial,_contour);
 end;

end;

class procedure TSpConvert.MContour.Convert(aSource: TSpMContour; aDest: IXMLTSubParcel_Contours);
var
 _contour: TSpContour;
 _xmlcontour: IXMLTSubParcel_Contours_Contour;
begin
 aDest.Clear;
 for _contour in aSource do begin
  if _contour.Count<>1then
   raise Exception.Create(rsC8E6F7A6);
  _xmlcontour:= aDest.Add;
  _xmlcontour.Number:=  _contour.getCadNumItem(TCadNumItemsKind.cnContour);
  _xmlcontour.Area.Area:= FloatToStr(_contour.getArea);
  _xmlcontour.Area.Innccuracy:= _contour.getDP;
  TSpConvert.Convert(_contour,_xmlcontour.Entity_Spatial,false);
 end;
end;

{
class procedure TSpConvert.MContour.Convert(aSource: TSpMContour; aDest: IXMLTSpecifyRelatedParcel_DeleteAllBorderList; doRewrite: boolean);
var
 _item: IXMLTSpecifyRelatedParcel_DeleteAllBorder;
 _contour: TSpContour;
begin
 if doRewrite then aDest.Clear;
 for _contour in aSource do begin
  _item:= aDest.Add;

  TSpConvert.Convert(_contour,_item.Entity_Spatial);
  _item.Area.Area:= Round(_contour.getArea);
  _item.Area.Innccuracy:= _contour.getDP;
 end;
end;
}

class procedure TSpConvert.MContour.Convert(
  aSource: IXMLContours_NewContourList; aDest: TSpMContour);
var
 _i: integer;
 _contour: TSpContour;
 _xmlContour: IXMLContours_NewContour;
begin
 aDest.Clear;
 for _i:= 0 to aSource.Count-1 do begin
  _xmlContour:= aSource.Items[_i];
  _contour:= TSpContour.Create;
  _contour.Caption:= _xmlContour.Definition;
  aDest.Add(_contour);
  TSpConvert.Convert(_xmlContour.Entity_Spatial,_contour);
 end;
end;

class procedure TSpConvert.MContour.Convert(
  aSource: IXMLSTD_MP_Package_SpecifyParcel_ExistEZ_ExistEZEntryParcels;
  aDest: TSpMContour; doRewrite: boolean);
var
 _i: integer;
 _contour: TSpContour;
begin
 if doRewrite  then aDest.Clear;
  for _i:= 0 to aSource.Count-1 do begin
   _contour:= TSpContour.Create;
   aDest.Add(_contour);
   _contour.Caption:= aSource.ExistEZEntryParcel[_i].CadastralNumber;
   TSpConvert.Convert(aSource.ExistEZEntryParcel[_i].Entity_Spatial,_contour);
  end;
end;

{ TSpConvert.Border }

class procedure TSpConvert.Border.Convert(aSource: TSpBorder;
      aDest: IXMLTSpecifyRelatedParcel_DeleteAllBorderList; doRewrite: boolean);
var
 _Point: TSpBorderPoint;
begin
 if doRewrite then
  aDest.Clear;
 for _point in aSource do
  TSpConvert.Convert(_point.OldPoint,aDest.Add.OldOrdinate)
end;

class procedure TSpConvert.Border.Convert(aSource: TSpBorder;
  aDest: IXMLContours_DeleteAllBorder; doRewrite: boolean);
var
 _Point: TSpBorderPoint;
begin
 if doRewrite then
  aDest.Clear;
 for _point in aSource do
  TSpConvert.Convert(_point.OldPoint,aDest.Add)
end;

class procedure TSpConvert.Border.Convert(
  aSource: IXMLTSpecifyRelatedParcel_DeleteAllBorderList; aDest: TSpBorder;
  doRewrite: boolean);
var
 _i: integer;
 _item: IXMLTSpecifyRelatedParcel_DeleteAllBorder;
 _point: TSpBorderPoint;
begin
 if doRewrite then
  aDest.Clear;
 for _i:= 0 to aSource.Count-1 do begin
  _point:= TSpBorderPoint.Create;
  _point.Kind:= psbRemoved;
  TSpConvert.Convert(aSource[_i].OldOrdinate,_point.OldPoint);
 end;
end;

class procedure TSpConvert.Border.Convert(aSource: IXMLContours_DeleteAllBorder;
  aDest: TSpBorder; doRewrite: boolean);
var
 _i: integer;
 _point: TSpBorderPoint;
begin
 if doRewrite then
  aDest.Clear;
 for _i:= 0 to aSource.Count-1 do begin
  _point:= TSpBorderPoint.Create;
  _point.Kind:= psbRemoved;
  TSpConvert.Convert(aSource.OldOrdinate[_i],_point.OldPoint);
 end;
end;

{ TSpConvert.MSubParcel }

class procedure TSpConvert.MSubParcel.Convert(aSource: TSpMSubParcel;
  aDest: IXMLTExistParcel_SubParcels_InvariableSubParcelList);
begin
  {TODO: ?????}
end;

class procedure TSpConvert.MSubParcel.Convert(aSource: TSpMSubParcel;
  aDest: IXMLTExistParcel_SubParcels_ExistSubParcelList);
var
 _subParcel : TSpSubParcel;
 _xmlsubParcel : IXMLTExistParcel_SubParcels_ExistSubParcel;
begin
 aDest.Clear;
 for _subParcel in aSource do begin
  if _subParcel.Count<>1then
   raise Exception.Create(rsC8E6F7A6);
  _xmlsubParcel:= aDest.Add;
  _xmlsubParcel.Number_Record:=  StrToIntDef(_subParcel.getCadNumItem(TCadNumItemsKind.cnSubParcelContour),0);
  _xmlsubParcel.Area.Area := FloatToStr(_subParcel.getArea);
  _xmlsubParcel.Area.Innccuracy := _subParcel.getDP;
  TSpConvert.Convert(_subParcel,_xmlsubParcel.Entity_Spatial);
 end;
end;

{������ ���}
class procedure TSpConvert.MSubParcel.Convert(aSource: TSpMSubParcel;
  aDest: IXMLTExistParcel_SubParcels_FormSubParcelList);
var
 _subParcel: TSpSubParcel;
 _xmlsubParcel: IXMLTExistParcel_SubParcels_FormSubParcel;
 str1 : string;
begin
 aDest.Clear;
 for _subParcel in aSource do begin
  _xmlsubParcel:= aDest.Add;
  _xmlsubParcel.Definition := _subParcel.getCadNumItem(TCadNumItemsKind.cnSubParcelNum);
  if _xmlsubParcel.Definition='' then
  _xmlsubParcel.Definition:= _subParcel.getCadNumItem(TCadNumItemsKind.cnSubParcelContour);
  _xmlsubParcel.Area.Area:= FloatToStr(_subParcel.getArea);
  _xmlsubParcel.Area.Innccuracy:= _subParcel.getDP;
  TSpConvert.Convert(_subParcel,_xmlsubParcel.Entity_Spatial);
 end;
end;

class procedure TSpConvert.MSubParcel.Convert(
  aSource: IXMLTExistParcel_SubParcels_InvariableSubParcelList; aDest: TSpMSubParcel);
var
 _i: integer;
 _contour: TSpSubParcel;
 _xmlContour: IXMLTExistParcel_SubParcels_InvariableSubParcel;
begin
 aDest.Clear;
 for _i:= 0 to aSource.Count-1 do begin
  _xmlContour:= aSource.Items[_i];
  _contour:= TSpSubParcel.Create;
  _contour.Caption:= IntToStr(_xmlContour.Number_Record);
  aDest.Add(_contour);
 end;
end;

class procedure TSpConvert.MSubParcel.Convert(
  aSource: IXMLTExistParcel_SubParcels_ExistSubParcelList; aDest: TSpMSubParcel);
var
 _i: integer;
 _contour: TSpSubParcel;
 _xmlContour: IXMLTExistParcel_SubParcels_ExistSubParcel;
begin
 aDest.Clear;
 for _i:= 0 to aSource.Count-1 do begin
  _xmlContour:= aSource.Items[_i];
  _contour:= TSpSubParcel.Create;
  _contour.Caption:= IntToStr(_xmlContour.Number_Record);
  aDest.Add(_contour);
  TSpConvert.Convert(_xmlContour.Entity_Spatial,_contour);
 end;
end;

class procedure TSpConvert.MSubParcel.Convert(
  aSource: IXMLTExistParcel_SubParcels_FormSubParcelList; aDest: TSpMSubParcel);
var 
 _i: integer;
 _contour: TSpSubParcel;
 _xmlContour: IXMLTExistParcel_SubParcels_FormSubParcel;
begin
 aDest.Clear;
 for _i:= 0 to aSource.Count-1 do begin
  _xmlContour:= aSource.Items[_i];
  _contour:= TSpSubParcel.Create;
  _contour.Caption:= _xmlContour.Definition;
  aDest.Add(_contour);
  TSpConvert.Convert(_xmlContour.Entity_Spatial,_contour);
 end;

end;

class function TSpConvert.Convert(aSource: TSpSubParcel; aDest: IXMLEntity_Spatial;
  fillBorders: boolean): boolean;
var
 _l: Double;
 _a,_b: TSpPoint;
 _i,_j: integer;
 _SpEl: IXMLTSPATIAL_ELEMENT;
 _border: TSpBorder;
 _xmlborder: IXMLEntity_Spatial_Borders_Border;
 _listp: TList<TSpBorderPoint.TNewPoint>;
begin
 //aDest.Spatial_Element.Clear; ent_sys delete
 MsXml_RemoveChildNode(aDest,rsSpatial_Element,TRUE);
 for _border in aSource do begin
  _SpEl:= aDest.Spatial_Element.Add;
  TSpConvert.Convert(_border,_SpEl);
 end;
 MsXml_RemoveChildNode(aDest,rsBorders);

 if fillBorders then begin
  _listp:= TList<TSpBorderPoint.TNewPoint>.Create;
  _xmlborder:= nil;
  if aSource.Count<>0 then  begin
  for _i := 0 to aSource.Count-1 do begin
    _listp.Clear;
    aSource[_i].NewPointsToList(_listp);
    for _j := 0 to _listp.Count-2 do begin
     _a:= _listp[_j];
     _b:= _listp[_j+1];
     _xmlborder:= aDest.Borders.Add;
     _xmlborder.Spatial:= _i+1;
     _xmlborder.Point1:= _j+1;
     _xmlborder.Point2:= _j+2;
     _xmlborder.Edge.Length:= FloatToStrF(
          msmcGetDistanceBetweenPoints(_a.X,_a.Y,_b.X,_b.Y),
          ffFixed,10,2);
    end;
  end;
  { TODO -c��������������� xsd : ������� ������������� �������� ��������� ����� �� ������? }
  //������������� ��������� ��� ��������� ����� ������� (�� ����������!!!!)
  //�������� ����� � ������ ������
  if _xmlborder<>nil then
  _xmlborder.Point2:= 1;
  end;
  _listp.Free;
 end;
end;

class function TSpConvert.Convert(aSource: IXMLEntity_Spatial; aDest: TSpSubParcel): boolean;
var
 _i: integer;
 _SpEl: IXMLTSPATIAL_ELEMENT;
 _border: TSpBorder;
begin
 aDest.Clear;
 for _i := 0 to aSource.Spatial_Element.Count-1 do begin
  _SpEl:= aSource.Spatial_Element[_i];
  _border:= TSpBorder.Create;
  TSpConvert.Convert(_SpEl,_border);
  aDest.Add(_border);
 end;
end;

class procedure TSpConvert.MSubParcel.Convert(aSource: TSpMSubParcel;
  aDest: IXMLTNewParcel_SubParcels);
var
 _subParcel : TSpSubParcel;
 _xmlsubParcel : IXMLTNewParcel_SubParcels_FormSubParcel;
begin
 aDest.Clear;
 for _subParcel in aSource do begin
  if _subParcel.Count<>1then
   raise Exception.Create(rsC8E6F7A6);
  _xmlsubParcel:= aDest.Add;
  _xmlsubParcel.Definition:=  _subParcel.getCadNumItem(TCadNumItemsKind.cnContour);
  _xmlsubParcel.SubParcel_Realty := false;
  _xmlsubParcel.Area.Area := FloatToStr(_subParcel.getArea);
  _xmlsubParcel.Area.Innccuracy := _subParcel.getDP;
  TSpConvert.Convert(_subParcel,_xmlsubParcel.Entity_Spatial);
 end;
end;

class procedure TSpConvert.MSubParcel.Convert(aSource: IXMLTNewParcel_SubParcels;
  aDest: TSpMSubParcel);
var
 _i: integer;
 _contour: TSpSubParcel;
 _xmlContour: IXMLTNewParcel_SubParcels_FormSubParcel;
begin
 aDest.Clear;
 for _i:= 0 to aSource.Count-1 do begin
  _xmlContour:= aSource.FormSubParcel[_i];
  _contour:= TSpSubParcel.Create;
  _contour.Caption:= _xmlContour.Definition;
  aDest.Add(_contour);
  TSpConvert.Convert(_xmlContour.Entity_Spatial,_contour);
 end;
end;

end.
