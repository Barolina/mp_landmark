unit frContours;
{
  Contours :

  ������� ��������������� �������
   (���� ������� � ���������� ��������� ����������/�������� ��������������)
}
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ActnList, ButtonGroup, ExtCtrls,
  Vcl.ComCtrls,
  Vcl.Buttons,

  VCL.clipbrd,
  frContour,STD_MP, ImgList, System.Actions,
  fmContainer, unRussLang, FrPreloader, Vcl.ToolWin, JvExComCtrls, JvToolBar,
  JvExControls, JvGroupHeader;

type
  TFrameContours = class(TFrame)
    actlst: TActionList;
    Contours_List_ListBox: TListBox;
    actContrours_AddNew: TAction;
    actContours_DeleteCurrent: TAction;
    cbbContoursType: TComboBox;
    actClipBrd_Paste: TAction;
    actClipBrd_Copy: TAction;
    pnlControls: TPanel;
    btnEdCurContour: TButtonedEdit;
    actContour_next: TAction;
    actContour_prev: TAction;
    actValid: TAction;
    tlbTypeSubparcels: TToolBar;
    btnNewContour: TToolButton;
    btnExistContour: TToolButton;
    btnDeleteAllBorder: TToolButton;
    jvtlbr2: TJvToolBar;
    btnContour_prev1: TToolButton;
    btnContour_next1: TToolButton;
    btnContrours_AddNew1: TToolButton;
    btnContours_DeleteCurrent1: TToolButton;
    btnClipBrd_Copy1: TToolButton;
    btnClipBrd_Paste1: TToolButton;
    btnValid1: TToolButton;
    btn1: TToolButton;
    btnEntity_Spatial: TToolButton;
    btnArea: TToolButton;
    jvgrphdr1: TJvGroupHeader;

    procedure actContrours_AddNewExecute(Sender: TObject);
    procedure actContours_DeleteCurrentExecute(Sender: TObject);
    procedure Action_Contours_SetCurrentExecute(Sender: TObject);
        procedure Contours_List_ListBoxMouseLeave(Sender: TObject);
    procedure btnEdCurContourRightButtonClick(Sender: TObject);
    procedure Contours_List_ListBoxKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure btnEdCurContourKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure btnEdCurContourExit(Sender: TObject);

    procedure cbbContoursTypeChange(Sender: TObject);
    procedure actClipBrd_CopyExecute(Sender: TObject);
    procedure actClipBrd_PasteExecute(Sender: TObject);
    procedure actContour_nextExecute(Sender: TObject);
    procedure actContour_prevExecute(Sender: TObject);
    procedure actValidExecute(Sender: TObject);
    procedure tlbTypeSubparcelsClick(Sender: TObject);
    procedure btnEntity_SpatialClick(Sender: TObject);
    procedure btnAreaClick(Sender: TObject);
  public type
   TContourKind = (cUndefine,cNewContour,cExistContour,cDeleteAllBorder);

  private
   { Private declarations }
   FXmlContours            : IXMLContours;
   FContour_frame          : TFrameContour;

   procedure ContourNewListDorefresh(A,B : Integer);
   procedure ContourExistListDorefresh(A,B : Integer);
   procedure ContourDeleteAllBorderListDorefresh(A,B : Integer);
   procedure Contours_List_DoRefresh(A: Integer = -1; B: Integer = -1);

   procedure Contours_List_DoSetCurrentItem(IndexItem: integer); //������� ������� ������� ������

   procedure Contours_List_DoUpDate;
   procedure Contours_List_DoAddNewItem;
   procedure Contours_List_DoDeleteItem(IndexItem: Integer);

   procedure OnChange_Attribute_DefinitionOrNumberPP(sender: Tobject);
   procedure _setContourKind(aValue: TContourKind);
  protected
   procedure InitializationUI;
   procedure SetContours(const aContours: IXMLContours);

   procedure SetActivContourUIIndex(index: integer);
   function GetActivContourUIIndex: integer;

   procedure setActivContourKind(aValue: TContourKind);
   function getActivContourKind: TContourKind;
  public
   { Public declarations }
   constructor Create(Owner: TComponent); override;

   function IsAssignedContours: Boolean;

   procedure getAsText(aContours: TContourKind; var OutputText: string);
   procedure setAsText(aContours: TContourKind; InputText: string);

   procedure ContoursList_Show;
   procedure ContoursList_Hide;

   property ActivContourUIIndex: integer read getActivContourUIIndex write setActivContourUIIndex;
   property ActivContourKind: TContourKind read getActivContourKind write setActivContourKind;

   property Contour: TFrameContour read FContour_frame;
   property XmlContours: IXMLContours read FXmlContours write SetContours;
  end;

implementation

uses MsXMLAPI, SurveyingPlan.DataFormat, SurveyingPlan.Xml.Utils,
  SurveyingPlan.Xml.Types, fmLog;

{$R *.dfm}

{ TFrameContours }

procedure TFrameContours.actClipBrd_CopyExecute(Sender: TObject);
var
 _st: string;
begin
 self.getAsText(self.ActivContourKind,_st);
 Clipboard.Open;
 TRY
  Clipboard.AsText:= _st;
 FINALLY
  Clipboard.Close;
 END;
end;

procedure TFrameContours.actClipBrd_PasteExecute(Sender: TObject);
var
 _st: string;
begin
 try
   TFrLoader.Inctance.RunAnimation := True;
   Clipboard.Open;
   TRY
   _st:= Clipboard.AsText;
   FINALLY
    Clipboard.Close;
   END;
   self.setAsText(self.ActivContourKind,_st);
finally   
  TFrLoader.Inctance.RunAnimation := false;
end;
end;

procedure TFrameContours.actContour_nextExecute(Sender: TObject);
begin
 self.ActivContourUIIndex:= self.ActivContourUIIndex+1;
end;

procedure TFrameContours.actContour_prevExecute(Sender: TObject);
begin
self.ActivContourUIIndex:= self.ActivContourUIIndex-1;
end;

procedure TFrameContours.actContours_DeleteCurrentExecute(Sender: TObject);
begin
 if not IsAssignedContours then EXIT;
 if self.Contours_List_ListBox.ItemIndex=-1 then EXIT;
 if MessageDlg('������� ������: '+' '+'?',mtInformation,mbYesNo,-1) = mrNo then Exit;

 self.Contours_List_DoDeleteItem(self.Contours_List_ListBox.ItemIndex);
 self.SetActivContourUIIndex(self.Contours_List_ListBox.ItemIndex);
 if not self.FContour_frame.IsAssignedContour then
  self.SetActivContourUIIndex(0);
end;

procedure TFrameContours.Action_Contours_SetCurrentExecute(Sender: TObject);
begin
 self.ActivContourUIIndex:= self.Contours_List_ListBox.ItemIndex;
 self.Contours_List_ListBox.Visible:= false;
end;

procedure TFrameContours.actContrours_AddNewExecute(Sender: TObject);
begin
 if not self.IsAssignedContours then EXIT;
 self.Contours_List_DoAddNewItem;
 self.SetActivContourUIIndex(self.Contours_List_ListBox.Count-1);
end;

procedure TFrameContours.actValidExecute(Sender: TObject);
var
 _st: string;
 _mcontour: TSpMContour;
 _log: TStringBuilder;
 _dlog: TformLog;
begin
 _mcontour:= TSpMContour.Create;
 case self.ActivContourKind of
   cNewContour:  TSpConvert.MContour.Convert(self.FXmlContours.NewContour,_mcontour);
   cExistContour: TSpConvert.MContour.Convert(self.FXmlContours.ExistContour,_mcontour);
   cDeleteAllBorder: TSpConvert.MContour.Convert(self.FXmlContours.DeleteAllBorder,_mcontour);
 end;


 _log:= TStringBuilder.Create;
 _dlog:= TformLog.Create(nil);
 if not TSpValidator.Validate(_mcontour,_log) then
  _dlog.TextLog:= _log.ToString
 else
  _dlog.TextLog:= '������ �� ����������';
 _dlog.ShowModal;
 _log.Free;
 _dlog.Free;


end;

procedure TFrameContours.btnAreaClick(Sender: TObject);
begin
  if not IsAssignedContours  then Exit;
  if self.Contours_List_ListBox.Count <= 0  then Exit;

  self.FContour_frame.jvgpgcntrlNewParcelsPageControl.ActivePage := self.FContour_frame.tsArea;
  self.jvgrphdr1.Caption := '�������/�����������';
end;

procedure TFrameContours.btnEdCurContourExit(Sender: TObject);
var
 _index: integer;
begin
 if TryStrToInt(self.btnEdCurContour.Text,_index) then
  self.ActivContourUIIndex:= _index-1
 else
  self.ActivContourUIIndex:= self.Contours_List_ListBox.ItemIndex;
end;

procedure TFrameContours.btnEdCurContourKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if key = VK_RETURN then
  self.SetFocus;
end;

procedure TFrameContours.btnEdCurContourRightButtonClick(Sender: TObject);
begin
 if self.Contours_List_ListBox.Visible then
  self.ContoursList_Hide
 else
  self.ContoursList_Show;
end;

procedure TFrameContours.btnEntity_SpatialClick(Sender: TObject);
begin
  if not IsAssignedContours  then Exit;
  if self.Contours_List_ListBox.Count <= 0  then Exit;

  self.FContour_frame.jvgpgcntrlNewParcelsPageControl.ActivePage := self.FContour_frame.tsEnSp_Attribut;
  self.jvgrphdr1.Caption := '������������ ��������';
end;

procedure TFrameContours.ContourDeleteAllBorderListDorefresh(A,
  B: Integer);
var
 _Contour: IXMLContours_DeleteAllBorder;
 _i,_j: Integer;
 _ItemName: string;
begin
 if self.Contours_List_ListBox.Items.Count = 0 then EXIT;

 if (A<0) or (a>=self.Contours_List_ListBox.Items.Count)  then
  a:= 0;

 if (B<A) or (B>=self.Contours_List_ListBox.Items.Count)  then
  B:= self.Contours_List_ListBox.Items.Count-1;

 _j:= self.Contours_List_ListBox.ItemIndex;
 for _i := A to B do begin
  _Contour:= IXMLContours_DeleteAllBorder(pointer(self.Contours_List_ListBox.Items.Objects[_i]));
 _ItemName:= MsXml_ReadAttribute(_Contour,cnstAtrNumber_Record);
  self.Contours_List_ListBox.Items.Strings[_i]:= _ItemName;
 end;
 self.Contours_List_ListBox.ItemIndex:= _j;

end;

procedure TFrameContours.ContourExistListDorefresh(A, B: Integer);
var
 _Contour: IXMLContours_ExistContour;
 _i,_j: Integer;
 _ItemName: string;
begin
 if self.Contours_List_ListBox.Items.Count = 0 then EXIT;

 if (A<0) or (a>=self.Contours_List_ListBox.Items.Count)  then
  a:= 0;

 if (B<A) or (B>=self.Contours_List_ListBox.Items.Count)  then
  B:= self.Contours_List_ListBox.Items.Count-1;

 _j:= self.Contours_List_ListBox.ItemIndex;
 for _i := A to B do begin
  _Contour:= IXMLContours_ExistContour(pointer(self.Contours_List_ListBox.Items.Objects[_i]));
 _ItemName:= MsXml_ReadAttribute(_Contour,cnstAtrNumber_Record);
  self.Contours_List_ListBox.Items.Strings[_i]:= _ItemName;
 end;
 self.Contours_List_ListBox.ItemIndex:= _j;
end;

procedure TFrameContours.ContourNewListDorefresh(A,B : Integer);
var
 _Contour: IXMLContours_NewContour;
 _i,_j: Integer;
 _ItemName: string;
begin
 if self.Contours_List_ListBox.Items.Count = 0 then EXIT;

 if (A<0) or (a>=self.Contours_List_ListBox.Items.Count)  then
  a:= 0;

 if (B<A) or (B>=self.Contours_List_ListBox.Items.Count)  then
  B:= self.Contours_List_ListBox.Items.Count-1;

 _j:= self.Contours_List_ListBox.ItemIndex;
 for _i := A to B do begin
  _Contour:= IXMLContours_NewContour(pointer(self.Contours_List_ListBox.Items.Objects[_i]));
 _ItemName:= MsXml_ReadAttribute(_Contour,cnstAtrDefinition);
  self.Contours_List_ListBox.Items.Strings[_i]:= _ItemName;
 end;
 self.Contours_List_ListBox.ItemIndex:= _j;
end;

procedure TFrameContours.Contours_List_DoRefresh(A, B: Integer);
begin
  case self.ActivContourKind of
    cNewContour : ContourNewListDorefresh(A,B);
    cExistContour : ContourExistListDorefresh(A,B);
    cDeleteAllBorder : ContourDeleteAllBorderListDorefresh(A,B);
  end;
end;

procedure TFrameContours.Contours_List_DoSetCurrentItem(IndexItem: integer);
var
 _contour: IXMLContours_NewContour;
 _contourExist : IXMLContours_ExistContour;
 _contourDeleteAllBorder  : IXMLContours_DeleteAllBorder;
begin
  case self.ActivContourKind of
    cNewContour : begin
       _contour:= IXMLContours_NewContour(pointer(self.Contours_List_ListBox.Items.Objects[IndexItem]));
        self.FContour_frame.XmlContour:= _contour;
    end;
    cExistContour : begin
       _contourExist:= IXMLContours_ExistContour(pointer(self.Contours_List_ListBox.Items.Objects[IndexItem]));
        self.FContour_frame.XmlContourExist:= _contourExist;
    end;
    cDeleteAllBorder : begin
        _contourDeleteAllBorder:= IXMLContours_DeleteAllBorder(pointer(self.Contours_List_ListBox.Items.Objects[IndexItem]));
         self.FContour_frame.XmlContourDeleteAllBorder:= _contourDeleteAllBorder;
    end;
  end;
end;

procedure TFrameContours.SetActivContourUIIndex(index: integer);
begin
 if (index>-1) and (index<self.Contours_List_ListBox.Count) then begin
  self.Contours_List_DoSetCurrentItem(index);
  self.Contours_List_ListBox.ItemIndex:= index;
  case self.ActivContourKind of
    cNewContour: self.btnEdCurContour.Text:= self.FXmlContours.NewContour[index].Definition;
    cExistContour: self.btnEdCurContour.Text:= IntToStr(self.FXmlContours.ExistContour[index].Number_Record);
    cDeleteAllBorder: self.btnEdCurContour.Text:= IntToStr(self.FXmlContours.DeleteAllBorder[index].Number_Record);
  end;
  self.FContour_frame.Visible:= True;

  btnEntity_Spatial.Down := (self.FContour_frame.jvgpgcntrlNewParcelsPageControl.ActivePage = self.FContour_frame.tsEnSp_Attribut);
  btnArea.Down := (self.FContour_frame.jvgpgcntrlNewParcelsPageControl.ActivePage = self.FContour_frame.tsArea);
  jvgrphdr1.Caption := self.FContour_frame.jvgpgcntrlNewParcelsPageControl.ActivePage.Hint;

 end else begin
  jvgrphdr1.Caption :='';
  self.FContour_frame.Visible:= false;
  self.Contours_List_ListBox.ItemIndex:= -1;
  self.btnEdCurContour.Text:='';
  case self.ActivContourKind of
    cNewContour : self.FContour_frame.XmlContour:= nil;
    cExistContour : self.FContour_frame.XmlContourExist := nil;
    cDeleteAllBorder : self.FContour_frame.XmlContourDeleteAllBorder := nil;
  end;
 end;
end;

constructor TFrameContours.Create(Owner: TComponent);
begin
 inherited;
 self.FContour_frame:= TFrameContour.Create(self);
 self.FContour_frame.OnChange_Attribute_DefOnNum:= self.OnChange_Attribute_DefinitionOrNumberPP;
 self.FContour_frame.Parent:= self;
 self.FContour_frame.Align:= alClient;
 self.FContour_frame.Visible:= False;
 self.tlbTypeSubparcels.Buttons[0].Down := True;
end;

function TFrameContours.GetActivContourUIIndex: integer;
begin
 Result:= self.Contours_List_ListBox.ItemIndex;
end;

procedure TFrameContours.getAsText(aContours: TContourKind; var OutputText: string);
var
 _mcontour: TSpMContour;
begin
 _mcontour:= TSpMContour.Create;
 TRY
  case aContours of
    cUndefine: ;
    cNewContour: TSpConvert.MContour.Convert(self.FXmlContours.NewContour,_mcontour);
    cExistContour: TSpConvert.MContour.Convert(self.FXmlContours.ExistContour,_mcontour);
    cDeleteAllBorder: TSpConvert.MContour.Convert(self.FXmlContours.DeleteAllBorder,_mcontour);
  end;
  OutputText:= DataFormat.MContour.Text.Build(_mcontour);
 FINALLY
  _mcontour.Free;
 END;
end;

function TFrameContours.getActivContourKind: TContourKind;

 function GetItemIndex() : Integer;
 var  i : Integer;
 begin
   Result := -1;
   for I := 0 to self.tlbTypeSubparcels.ButtonCount-1 do
     if tlbTypeSubparcels.Buttons[i].Down then Result := tlbTypeSubparcels.Buttons[i].Tag;
 end;
begin
  if not  Self.tlbTypeSubparcels.Visible  then //�� ���� - ������ ���� ������� -
   Result := cNewContour
  else
  begin
     if GetItemIndex in [0..Self.tlbTypeSubparcels.ButtonCount-1] then
       Result := TContourKind(GetItemIndex+1)
     else
       Result := cUndefine;
  end;
end;

procedure TFrameContours.InitializationUI;
begin
 self.Contours_List_ListBox.Clear;
 if self.IsAssignedContours then begin
  self.Contours_List_DoUpDate;
  self.SetActivContourUIIndex(0);
 end;
 {����  ����� SpecifyRelatedpatcel  = ������ ������ ���� ������� ������������
  - �����  ������ �� � ��������� ��  �������   }
 if (FXmlContours <> nil ) and
    ( FXmlContours.ParentNode.NodeName = cnstXmlSpecifyRelatedParcel )then
    self.ActivContourKind :=  cNewContour;

end;

function TFrameContours.IsAssignedContours: Boolean;
begin
 Result:= self.FXmlContours<>nil;
end;

procedure TFrameContours.OnChange_Attribute_DefinitionOrNumberPP(sender: tobject);
begin
 self.Contours_List_DoRefresh(self.Contours_List_ListBox.ItemIndex,self.Contours_List_ListBox.ItemIndex);
end;

procedure TFrameContours.setAsText(aContours: TContourKind;InputText: string);
var
 _mcontour: TSpMContour;
 _log: TStringBuilder;
 _dlog: TformLog;
begin
 _mcontour:= nil;
 _log:= TStringBuilder.Create;
 TRY
  _mcontour:= DataFormat.MContour.Text.Parse(InputText);
  _mcontour._checkDirection;
  if not TSpValidator.Validate(_mcontour,_log) then begin
  _dlog:= TformLog.Create(nil);
  _dlog.TextLog:= _log.ToString;
  FreeAndNil(_log);
  _dlog.ShowModal;
  if MessageDlg('������������ ��������� ������?',mtConfirmation,mbYesNo,-1) = mrNo then begin
   _dlog.Free;
   EXIT;
  end;
  _dlog.Free;
 end;
 FreeAndNil(_log);

  case aContours of
    cUndefine: ;
    cNewContour: TSpConvert.MContour.Convert(_mcontour,self.FXmlContours.NewContour);
    cExistContour: TSpConvert.MContour.Convert(_mcontour,self.FXmlContours.ExistContour);
    cDeleteAllBorder: TSpConvert.MContour.Convert(_mcontour,self.FXmlContours.DeleteAllBorder);
  end;

 FINALLY
  _mcontour.Free;
  _log.Free;
 END;
 InitializationUI;
end;

procedure TFrameContours.SetContours(const aContours: IXMLContours);
begin
 if self.FXmlContours=aContours then EXIT;
 self.SetActivContourUIIndex(-1);
 self.FXmlContours:= aContours;
 self.InitializationUI;
end;

procedure TFrameContours.tlbTypeSubparcelsClick(Sender: TObject);
begin
  setActivContourKind(Self.getActivContourKind);
end;

procedure TFrameContours.setActivContourKind(aValue: TContourKind);
begin
  self._setContourKind(aValue);
  if self.IsAssignedContours then begin
  case aValue of
   cNewContour,cExistContour :  begin
            self.FContour_frame.SetVisibleTypeCOntoursForm(true);
            self.FContour_frame.AsAttributeContour(true);
   end;
   cDeleteAllBorder   :  self.FContour_frame.SetVisibleTypeCOntoursForm(false);
  end;
   self.Contours_List_DoUpDate;
  end;

end;

procedure TFrameContours._setContourKind(aValue: TContourKind);
var i : Integer;
begin
  case aValue of
   cUndefine:begin
               for I := 0 to tlbTypeSubparcels.ButtonCount-1 do
                   tlbTypeSubparcels.Buttons[i].Down := false;
   end;
   cNewContour: self.tlbTypeSubparcels.Buttons[0].Down := True;
   cExistContour: self.tlbTypeSubparcels.Buttons[1].Down := True;
   cDeleteAllBorder: self.tlbTypeSubparcels.Buttons[2].Down := True;
 end;
end;

procedure TFrameContours.cbbContoursTypeChange(Sender: TObject);
begin
  self.setActivContourKind(self.getActivContourKind);
end;

procedure TFrameContours.ContoursList_Hide;
begin
 self.Contours_List_ListBox.Visible:= False;
end;

procedure TFrameContours.ContoursList_Show;
begin
 self.Contours_List_ListBox.Parent:= nil;
 self.Contours_List_ListBox.Parent:= self;
 self.Contours_List_ListBox.Visible:= true;
 self.Contours_List_ListBox.SetFocus;
end;

procedure TFrameContours.Contours_List_DoAddNewItem;
var
 _contour: IXMLContours_NewContour;
 _contourExist : IXMLContours_ExistContour;
 _contourDeleteAllBorder : IXMLContours_DeleteAllBorder;
 _ItemName: string;
begin
  case self.ActivContourKind  of
  cNewContour:
  begin
     _contour:= self.FXmlContours.NewContour.Add;
     _ItemName:= IntToStr(self.FXmlContours.NewContour.Count);
     _contour.Definition := _ItemName;
     _contour.Entity_Spatial.Spatial_Element.Add;
     _contour.Definition:= _ItemName;
     self.Contours_List_ListBox.AddItem(_ItemName,pointer(_contour));
  end;
  cExistContour:
  begin
     _contourExist:= self.FXmlContours.ExistContour.Add;
     _ItemName:= IntToStr(self.FXmlContours.ExistContour.Count);
     _contourExist.Number_Record := strtoint(_ItemName);
     _contourExist.Entity_Spatial.Spatial_Element.Add;
      _contourExist.Number_Record:= strtoint(_ItemName);
     self.Contours_List_ListBox.AddItem(_ItemName,pointer(_contourExist));
  end;
  cDeleteAllBorder:  begin
     _contourDeleteAllBorder:= self.FXmlContours.DeleteAllBorder.Add;
     _ItemName:= IntToStr(self.FXmlContours.DeleteAllBorder.Count);
     _contourDeleteAllBorder.Number_Record:= strtoint(_ItemName);
      self.Contours_List_ListBox.AddItem(_ItemName,pointer(_contourDeleteAllBorder));
  end;

  end;
end;


procedure TFrameContours.Contours_List_DoDeleteItem(IndexItem: Integer);
var
 _contour: IXMLContours_NewContour;
 _contourExist : IXMLContours_ExistContour;
 _contourDeleteAllBorder : IXMLContours_DeleteAllBorder;
 _index: Integer;
begin
 case self.ActivContourKind of
 cNewContour:
 begin
     _contour:= IXMLContours_NewContour(pointer(self.Contours_List_ListBox.Items.Objects[IndexItem]));
     if self.FContour_frame.XmlContour=_contour then
      self.SetActivContourUIIndex(IndexItem-1);
     self.Contours_List_ListBox.Items.Delete(IndexItem);
     self.FXmlContours.NewContour.Remove(_contour);
     _contour:= nil;
 end;
 cExistContour:
 begin
     _contourExist:= IXMLContours_ExistContour(pointer(self.Contours_List_ListBox.Items.Objects[IndexItem]));
     if self.FContour_frame.XmlContourExist=_contourExist then
      self.SetActivContourUIIndex(IndexItem-1);
     self.Contours_List_ListBox.Items.Delete(IndexItem);
     self.FXmlContours.ExistContour.Remove(_contourExist);
     _contourExist:= nil;
 end;
 cDeleteAllBorder:
 begin
     _contourDeleteAllBorder:= IXMLContours_DeleteAllBorder(pointer(self.Contours_List_ListBox.Items.Objects[IndexItem]));
     if self.FContour_frame.XmlContourDeleteAllBorder=_contourDeleteAllBorder then
      self.SetActivContourUIIndex(IndexItem-1);
     self.Contours_List_ListBox.Items.Delete(IndexItem);
     self.FXmlContours.DeleteAllBorder.Remove(_contourDeleteAllBorder);
     _contourDeleteAllBorder:= nil;
 end;

 end;
end;

procedure TFrameContours.Contours_List_DoUpDate;
var
 _i, _index: Integer;
 _Contour: IXMLContours_NewContour;
 _ContourExist : IXMLContours_ExistContour;
 _ContourDeleteAllBorder : IXMLContours_DeleteAllBorder;
 _ItemName: string;
begin

 {���������� ��� ������� }
 if self.ActivContourKind = cUndefine then
 if FXmlContours.ChildNodes.FindNode(cnstXmlNewContour) <> nil  then
   self.ActivContourKind := cNewContour
 else
   if FXmlContours.ChildNodes.FindNode(cnstXmlExistContour) <> nil then
     self.ActivContourKind := cExistContour
   else
     if FXmlContours.ChildNodes.FindNode(cnstXmlDeleteAllBorder) <> nil then
      self.ActivContourKind := cDeleteAllBorder;

 FContour_frame.Visible := (self.ActivContourKind <> cDeleteAllBorder);
 self.Contours_List_ListBox.Clear;

 case self.ActivContourKind of
 cNewContour:
     begin
       for _i := 0 to self.FXmlContours.NewContour.Count-1 do begin
        _Contour:= self.FXmlContours.NewContour.Items[_i];
       _ItemName:= MsXml_ReadAttribute(_Contour,cnstAtrDefinition);
        //_Contour.Definition := _ItemName;
        self.Contours_List_ListBox.AddItem(_ItemName,pointer(_Contour));
       end;
       _index := FXmlContours.NewContour.Count-1;
     end;
 cExistContour:
      begin
         for _i := 0 to self.FXmlContours.ExistContour.Count-1 do begin
          _ContourExist:= self.FXmlContours.ExistContour.Items[_i];
         _ItemName:= MsXml_ReadAttribute(_ContourExist,cnstAtrNumber_Record);
          //_ContourExist.Number_Record :=  strtoint(_ItemName);
          self.Contours_List_ListBox.AddItem(_ItemName,pointer(_ContourExist));
         end;
         _index := FXmlContours.ExistContour.Count-1;
      end;
 cDeleteAllBorder:
     begin
         for _i := 0 to self.FXmlContours.DeleteAllBorder.Count-1 do begin
          _ContourDeleteAllBorder:= self.FXmlContours.DeleteAllBorder.Items[_i];
         _ItemName:= MsXml_ReadAttribute(_ContourDeleteAllBorder,cnstAtrNumber_Record);
          self.Contours_List_ListBox.AddItem(_ItemName,pointer(_ContourDeleteAllBorder));
         end;
         _index := FXmlContours.DeleteAllBorder.Count-1;
     end;
 end;
 self.SetActivContourUIIndex(_index);
 if not self.FContour_frame.IsAssignedContour then
 self.SetActivContourUIIndex(0);
end;

procedure TFrameContours.Contours_List_ListBoxKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  case key of
   VK_ESCAPE: self.Contours_List_ListBox.Visible:= false;
   VK_RETURN: self.Contours_List_ListBox.Visible:= false;
  end;

end;

procedure TFrameContours.Contours_List_ListBoxMouseLeave(Sender: TObject);
begin
 self.Contours_List_ListBox.Visible:= false;
end;

end.
