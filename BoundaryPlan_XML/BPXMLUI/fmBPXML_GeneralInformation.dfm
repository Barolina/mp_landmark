object FormBPXML_GeneralInformation: TFormBPXML_GeneralInformation
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu, biMinimize, biMaximize, biHelp]
  Caption = #1054#1087#1080#1089#1072#1085#1080#1077' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
  ClientHeight = 463
  ClientWidth = 954
  Color = clBtnFace
  DefaultMonitor = dmMainForm
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'MS Sans Serif'
  Font.Pitch = fpVariable
  Font.Style = []
  Font.Quality = fqClearType
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object PanelAttribute: TPanel
    Left = 0
    Top = 0
    Width = 954
    Height = 45
    Align = alTop
    BevelEdges = [beLeft, beTop, beRight]
    BevelKind = bkFlat
    BevelOuter = bvNone
    TabOrder = 0
    object Label1: TLabel
      Left = 8
      Top = 0
      Width = 129
      Height = 13
      Caption = #1042#1080#1076' '#1087#1088#1086#1080#1079#1074#1086#1076#1080#1084#1099#1093' '#1088#1072#1073#1086#1090
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Pitch = fpVariable
      Font.Style = []
      Font.Quality = fqClearType
      ParentFont = False
    end
    object Label2: TLabel
      Left = 273
      Top = 0
      Width = 62
      Height = 13
      Caption = #1042#1077#1088#1089#1080#1103' XML'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Pitch = fpVariable
      Font.Style = []
      Font.Quality = fqClearType
      ParentFont = False
    end
    object Label3: TLabel
      Left = 409
      Top = 1
      Width = 181
      Height = 13
      Caption = #1059#1085#1080#1082#1072#1083#1100#1085#1099#1081' '#1080#1076#1077#1085#1090#1080#1092#1080#1082#1072#1090#1086#1088' (GUID)'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Pitch = fpVariable
      Font.Style = []
      Font.Quality = fqClearType
      ParentFont = False
    end
    object AttribyteCodeType_Edit: TEdit
      Left = 8
      Top = 16
      Width = 259
      Height = 21
      BevelInner = bvLowered
      BevelKind = bkFlat
      BorderStyle = bsNone
      Color = clWhite
      Enabled = False
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Pitch = fpVariable
      Font.Style = []
      Font.Quality = fqClearType
      ParentFont = False
      TabOrder = 0
      StyleElements = []
    end
    object AttribyteVersion_Edit: TEdit
      Left = 273
      Top = 16
      Width = 130
      Height = 21
      BevelKind = bkFlat
      BorderStyle = bsNone
      Color = clWhite
      Enabled = False
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Pitch = fpVariable
      Font.Style = []
      Font.Quality = fqClearType
      ParentFont = False
      TabOrder = 1
      StyleElements = []
    end
    object AttributeGUID_Edit: TEdit
      Left = 409
      Top = 16
      Width = 336
      Height = 21
      BevelKind = bkFlat
      BorderStyle = bsNone
      Color = clWhite
      Enabled = False
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Pitch = fpVariable
      Font.Style = []
      Font.Quality = fqClearType
      ParentFont = False
      TabOrder = 2
      StyleElements = []
    end
  end
  object GIPageControl: TPageControl
    Left = 0
    Top = 45
    Width = 954
    Height = 418
    ActivePage = ConclusionTabSheet
    Align = alClient
    Style = tsFlatButtons
    TabOrder = 1
    object TabSheetContractor: TTabSheet
      Caption = #1050#1072#1076#1072#1089#1090#1088#1086#1074#1099#1081' '#1080#1085#1078#1077#1085#1077#1088
      ImageIndex = 2
      TabVisible = False
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 946
        Height = 32
        Align = alTop
        BevelOuter = bvNone
        Caption = #1054#1073#1097#1080#1077' '#1089#1074#1077#1076#1077#1085#1080#1103
        Color = 11364884
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWhite
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Pitch = fpVariable
        Font.Style = [fsBold]
        Font.Quality = fqClearType
        ParentBackground = False
        ParentFont = False
        TabOrder = 0
        StyleElements = []
      end
      object PanelTitle: TPanel
        Left = 0
        Top = 32
        Width = 946
        Height = 71
        Align = alTop
        Alignment = taLeftJustify
        BevelOuter = bvNone
        TabOrder = 1
        object lblPurpose: TLabel
          Left = 407
          Top = 3
          Width = 127
          Height = 13
          Caption = #1062#1077#1083#1100' '#1082#1072#1076#1072#1089#1090#1088#1086#1074#1099#1093' '#1088#1072#1073#1086#1090
        end
        object Label9: TLabel
          Left = 6
          Top = 3
          Width = 142
          Height = 13
          Caption = #1042#1080#1076' '#1082#1072#1076#1072#1089#1090#1088#1086#1074#1099#1093' '#1088#1072#1073#1086#1090
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Pitch = fpVariable
          Font.Style = [fsBold]
          Font.Quality = fqClearType
          ParentFont = False
        end
        object mmoReason: TMemo
          Left = 6
          Top = 18
          Width = 363
          Height = 47
          BevelKind = bkFlat
          BorderStyle = bsNone
          Color = 15921123
          ScrollBars = ssVertical
          TabOrder = 0
          OnExit = Reason_EditExit
        end
        object mmoPurpose: TMemo
          Left = 407
          Top = 18
          Width = 410
          Height = 47
          BevelKind = bkFlat
          BorderStyle = bsNone
          Color = clWhite
          ScrollBars = ssVertical
          TabOrder = 1
          OnExit = Purpose_EditExit
        end
        object btnPurpose: TBitBtn
          Left = 802
          Top = 2
          Width = 24
          Height = 24
          Hint = #1059#1074#1077#1083#1080#1095#1080#1090#1100
          Glyph.Data = {
            36030000424D3603000000000000360000002800000010000000100000000100
            18000000000000030000C30E0000C30E00000000000000000000A39D9D340E0E
            3D2727B5B3B3FEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFF645C5C320401320401340E0EA7A5A5FDFDFDFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA9A7A7330505
            320401320401340E0EA2A0A0FCFCFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFBFBFB817E7E3306063204013204013610109C9A9AFC
            FCFCFFFFFFFEFEFEFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F9F9
            868282330808320401381212878383A49E9E6952524E2D2D4E2D2D6B5252AFA6
            A6F7F7F7FFFFFFFFFFFFFFFFFFFFFFFFF9F9F98985853A1C1C9690905D4E4E36
            0909320401320401320401320401360A08645454ECEAEAFFFFFFFFFFFFFFFFFF
            FFFFFFF9F9F9C5C2C25D4D4D320503330505553C3C807878807777523C3C3205
            03320503635252F9F9F9FFFFFFFFFFFFFFFFFFFFFFFFAFA6A63508083305057F
            7676EEEDEDFFFFFFFFFFFFEDECEC7C7373320503340606B9B0B0FFFFFFFFFFFF
            FFFFFFFFFFFF6B55553204014F3333EDEBEBFFFFFFFFFFFFFFFFFFFFFFFFECEA
            EA472C2C320301725C5CFFFFFFFFFFFFFFFFFFFEFEFE5338383204016D6464FF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFCFC696060320401513E3EFFFFFFFFFFFF
            FFFFFFFFFFFF5B3D3D320401685454FCFCFCFFFFFFFFFFFFFFFFFFFFFFFFF9F9
            F96251513204015C4343FFFFFFFFFFFFFFFFFFFFFFFF8170703304023D1D1DD2
            CFCFFFFFFFFFFFFFFFFFFFFFFFFFD3D0D0371515330402887777FFFFFFFFFFFF
            FFFFFFFFFFFFCFC9C93E16163204014C3737C5C0C0F0EFEFEFEEEEC2BFBF4931
            31320401391111DAD5D5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF938D8D340C0C32
            04013207073C26263C2525320707320401360B0B9C9595FFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFDFDFDA7A0A0432020350604330402330402350604451F
            1FACA6A6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0
            EFEFBAB1B19C8E8E9A8D8DBBB1B1F3F2F2FFFFFFFFFFFFFFFFFF}
          ParentShowHint = False
          ShowHint = True
          Style = bsNew
          TabOrder = 2
          OnClick = btnPurposeClick
        end
        object btnReason: TBitBtn
          Left = 354
          Top = 2
          Width = 24
          Height = 24
          Hint = #1059#1074#1077#1083#1080#1095#1080#1090#1100
          Glyph.Data = {
            36030000424D3603000000000000360000002800000010000000100000000100
            18000000000000030000C30E0000C30E00000000000000000000A39D9D340E0E
            3D2727B5B3B3FEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFF645C5C320401320401340E0EA7A5A5FDFDFDFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA9A7A7330505
            320401320401340E0EA2A0A0FCFCFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFBFBFB817E7E3306063204013204013610109C9A9AFC
            FCFCFFFFFFFEFEFEFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F9F9
            868282330808320401381212878383A49E9E6952524E2D2D4E2D2D6B5252AFA6
            A6F7F7F7FFFFFFFFFFFFFFFFFFFFFFFFF9F9F98985853A1C1C9690905D4E4E36
            0909320401320401320401320401360A08645454ECEAEAFFFFFFFFFFFFFFFFFF
            FFFFFFF9F9F9C5C2C25D4D4D320503330505553C3C807878807777523C3C3205
            03320503635252F9F9F9FFFFFFFFFFFFFFFFFFFFFFFFAFA6A63508083305057F
            7676EEEDEDFFFFFFFFFFFFEDECEC7C7373320503340606B9B0B0FFFFFFFFFFFF
            FFFFFFFFFFFF6B55553204014F3333EDEBEBFFFFFFFFFFFFFFFFFFFFFFFFECEA
            EA472C2C320301725C5CFFFFFFFFFFFFFFFFFFFEFEFE5338383204016D6464FF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFCFC696060320401513E3EFFFFFFFFFFFF
            FFFFFFFFFFFF5B3D3D320401685454FCFCFCFFFFFFFFFFFFFFFFFFFFFFFFF9F9
            F96251513204015C4343FFFFFFFFFFFFFFFFFFFFFFFF8170703304023D1D1DD2
            CFCFFFFFFFFFFFFFFFFFFFFFFFFFD3D0D0371515330402887777FFFFFFFFFFFF
            FFFFFFFFFFFFCFC9C93E16163204014C3737C5C0C0F0EFEFEFEEEEC2BFBF4931
            31320401391111DAD5D5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF938D8D340C0C32
            04013207073C26263C2525320707320401360B0B9C9595FFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFDFDFDA7A0A0432020350604330402330402350604451F
            1FACA6A6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0
            EFEFBAB1B19C8E8E9A8D8DBBB1B1F3F2F2FFFFFFFFFFFFFFFFFF}
          Layout = blGlyphTop
          ParentShowHint = False
          ShowHint = True
          Style = bsNew
          TabOrder = 3
          OnClick = btnReasonClick
        end
      end
      object grbxContractor: TGroupBox
        Left = 0
        Top = 103
        Width = 946
        Height = 305
        Align = alClient
        Caption = #1057#1074#1077#1076#1077#1085#1080#1103' '#1086' '#1082#1072#1076#1072#1089#1090#1088#1086#1074#1086#1084' '#1080#1085#1078#1077#1085#1077#1088#1077
        Color = clBtnFace
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Pitch = fpVariable
        Font.Style = [fsBold]
        Font.Quality = fqClearType
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 2
      end
    end
    object TabSheetClient: TTabSheet
      Caption = #1057#1074#1077#1076#1077#1085#1080#1103' '#1086' '#1079#1072#1082#1072#1079#1095#1080#1082#1077' '#1082#1072#1076#1072#1089#1090#1088#1086#1074#1099#1093' '#1088#1072#1073#1086#1090
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Pitch = fpVariable
      Font.Style = []
      Font.Quality = fqClearType
      ImageIndex = 4
      ParentFont = False
      TabVisible = False
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Panel8: TPanel
        Left = 0
        Top = 0
        Width = 946
        Height = 33
        Align = alTop
        BevelEdges = []
        BevelKind = bkFlat
        BevelOuter = bvNone
        Caption = #1057#1074#1077#1076#1077#1085#1080#1103' '#1086' '#1079#1072#1082#1072#1079#1095#1080#1082#1077' '#1082#1072#1076#1072#1089#1090#1088#1086#1074#1099#1093' '#1088#1072#1073#1086#1090
        Color = 11364884
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWhite
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Pitch = fpVariable
        Font.Style = [fsBold]
        Font.Quality = fqClearType
        ParentBackground = False
        ParentFont = False
        TabOrder = 0
        StyleElements = []
      end
    end
    object TabSheet2: TTabSheet
      Caption = #1044#1086#1082#1091#1084#1077#1085#1090#1072#1094#1080#1103
      ImageIndex = 1
      TabVisible = False
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Splitter4: TSplitter
        AlignWithMargins = True
        Left = 300
        Top = 36
        Width = 2
        Height = 369
        Beveled = True
        ExplicitLeft = 301
        ExplicitTop = 37
        ExplicitHeight = 370
      end
      object PanelRecipient: TPanel
        Left = 305
        Top = 33
        Width = 641
        Height = 375
        Align = alClient
        BevelOuter = bvNone
        Color = clWhite
        ParentBackground = False
        TabOrder = 0
        OnMouseLeave = PanelRecipientMouseLeave
        OnMouseMove = PanelRecipientMouseMove
      end
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 946
        Height = 33
        Align = alTop
        BevelOuter = bvNone
        Caption = #1044#1086#1082#1091#1084#1077#1085#1090#1072#1094#1080#1103
        Color = 11364884
        DoubleBuffered = False
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWhite
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Pitch = fpVariable
        Font.Style = [fsBold]
        Font.Quality = fqClearType
        ParentBackground = False
        ParentDoubleBuffered = False
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        StyleElements = []
      end
      object PanelAttributes: TPanel
        Left = 0
        Top = 33
        Width = 297
        Height = 375
        Align = alLeft
        BevelEdges = []
        BevelKind = bkFlat
        BevelOuter = bvNone
        TabOrder = 2
        object lbSurvey: TLabel
          AlignWithMargins = True
          Left = 3
          Top = 3
          Width = 253
          Height = 40
          Cursor = crHandPoint
          ParentCustomHint = False
          Align = alTop
          BiDiMode = bdLeftToRight
          Caption = #1057#1074#1077#1076#1077#1085#1080#1103' '#1086' '#1074#1099#1087#1086#1083#1085#1077#1085#1085#1099#1093' '#1080#1079#1084#1077#1088#1077#1085#1080#1103#1093' '#1080' '#1088#1072#1089#1095#1077#1090#1072#1093
          Color = clBtnFace
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Arial Narrow'
          Font.Pitch = fpVariable
          Font.Style = [fsBold, fsItalic]
          Font.Quality = fqClearType
          ParentBiDiMode = False
          ParentColor = False
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          Transparent = True
          WordWrap = True
          StyleElements = []
          OnClick = lbSurveyClick
        end
        object lbScheme_Geodesic_Plotting: TLabel
          AlignWithMargins = True
          Left = 3
          Top = 49
          Width = 219
          Height = 20
          Cursor = crHandPoint
          ParentCustomHint = False
          Align = alTop
          BiDiMode = bdLeftToRight
          Caption = #1057#1093#1077#1084#1072' '#1075#1077#1086#1076#1077#1079#1080#1095#1077#1089#1082#1080#1093' '#1087#1086#1089#1090#1088#1086#1077#1085#1080#1081
          Color = clBtnFace
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Arial Narrow'
          Font.Pitch = fpVariable
          Font.Style = [fsBold, fsItalic]
          Font.Quality = fqClearType
          ParentBiDiMode = False
          ParentColor = False
          ParentFont = False
          ParentShowHint = False
          ShowHint = False
          WordWrap = True
          StyleElements = []
          OnClick = lbScheme_Geodesic_PlottingClick
        end
        object lbScheme_Disposition_Parcels: TLabel
          AlignWithMargins = True
          Left = 3
          Top = 121
          Width = 270
          Height = 20
          Cursor = crHandPoint
          ParentCustomHint = False
          Align = alTop
          BiDiMode = bdLeftToRight
          Caption = #1057#1093#1077#1084#1072' '#1088#1072#1089#1087#1086#1083#1086#1078#1077#1085#1080#1103' '#1079#1077#1084#1077#1083#1100#1085#1099#1093' '#1091#1095#1072#1089#1090#1082#1086#1074
          Color = clBtnFace
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Arial Narrow'
          Font.Pitch = fpVariable
          Font.Style = [fsBold, fsItalic]
          Font.Quality = fqClearType
          ParentBiDiMode = False
          ParentColor = False
          ParentFont = False
          ParentShowHint = False
          ShowHint = False
          WordWrap = True
          StyleElements = []
          OnClick = lbScheme_Disposition_ParcelsClick
        end
        object lbDiagram_Parcels_SubParcels: TLabel
          AlignWithMargins = True
          Left = 3
          Top = 147
          Width = 250
          Height = 20
          Cursor = crHandPoint
          ParentCustomHint = False
          Align = alTop
          BiDiMode = bdLeftToRight
          Caption = #1063#1077#1088#1090#1077#1078' '#1079#1077#1084#1077#1083#1100#1085#1099#1093' '#1091#1095#1072#1089#1090#1082#1086#1074' '#1080' '#1080#1093' '#1095#1072#1089#1090#1077#1081
          Color = clBtnFace
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Arial Narrow'
          Font.Pitch = fpVariable
          Font.Style = [fsBold, fsUnderline]
          Font.Quality = fqClearType
          ParentBiDiMode = False
          ParentColor = False
          ParentFont = False
          ParentShowHint = False
          ShowHint = False
          WordWrap = True
          StyleElements = []
          OnClick = lbDiagram_Parcels_SubParcelsClick
        end
        object lbAgreement_Document: TLabel
          AlignWithMargins = True
          Left = 3
          Top = 75
          Width = 237
          Height = 40
          Cursor = crHandPoint
          ParentCustomHint = False
          Align = alTop
          BiDiMode = bdLeftToRight
          Caption = #1040#1082#1090' '#1089#1086#1075#1083#1072#1089#1086#1074#1072#1085#1080#1103' '#1084#1077#1089#1090#1086#1087#1086#1083#1086#1078#1077#1085#1080#1103' '#1075#1088#1072#1085#1080#1094#1099' '#1079#1077#1084#1077#1083#1100#1085#1086#1075#1086' '#1091#1095#1072#1089#1090#1082#1072
          Color = clBtnFace
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Arial Narrow'
          Font.Pitch = fpVariable
          Font.Style = [fsBold, fsItalic]
          Font.Quality = fqClearType
          ParentBiDiMode = False
          ParentColor = False
          ParentFont = False
          ParentShowHint = False
          ShowHint = False
          WordWrap = True
          StyleElements = []
          OnClick = lbAgreement_DocumentClick
        end
        object lbNodalPointSchemes: TLabel
          AlignWithMargins = True
          Left = 3
          Top = 173
          Width = 274
          Height = 40
          Cursor = crHandPoint
          ParentCustomHint = False
          Align = alTop
          BiDiMode = bdLeftToRight
          Caption = #1040#1073#1088#1080#1089#1099' '#1091#1079#1083#1086#1074#1099#1093' '#1090#1086#1095#1077#1082' '#1075#1088#1072#1085#1080#1094' '#1079#1077#1084#1077#1083#1100#1085#1099#1093' '#1091#1095#1072#1089#1090#1082#1086#1074
          Color = clBtnFace
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Arial Narrow'
          Font.Pitch = fpVariable
          Font.Style = [fsBold, fsItalic]
          Font.Quality = fqClearType
          ParentBiDiMode = False
          ParentColor = False
          ParentFont = False
          ParentShowHint = False
          ShowHint = False
          WordWrap = True
          StyleElements = []
          OnClick = lbNodalPointSchemesClick
        end
        object lbAppendix: TLabel
          AlignWithMargins = True
          Left = 3
          Top = 219
          Width = 79
          Height = 20
          Cursor = crHandPoint
          ParentCustomHint = False
          Align = alTop
          BiDiMode = bdLeftToRight
          Caption = #1055#1088#1080#1083#1086#1078#1077#1085#1080#1103
          Color = clBtnFace
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Arial Narrow'
          Font.Pitch = fpVariable
          Font.Style = [fsBold, fsItalic]
          Font.Quality = fqClearType
          ParentBiDiMode = False
          ParentColor = False
          ParentFont = False
          ParentShowHint = False
          ShowHint = False
          WordWrap = True
          StyleElements = []
          OnClick = lbAppendixClick
        end
      end
    end
    object ConclusionTabSheet: TTabSheet
      Caption = #1047#1072#1082#1083#1102#1095#1077#1085#1080#1077' '#1082#1072#1076#1072#1089#1090#1088#1086#1074#1086#1075#1086' '#1080#1085#1078#1080#1085#1077#1088#1072
      ImageIndex = 4
      TabVisible = False
      object MemoConclusion: TMemo
        Left = 0
        Top = 33
        Width = 946
        Height = 255
        Hint = #13#10#1047#1072#1082#1083#1102#1095#1077#1085#1080#1077' '#1082#1072#1076#1072#1089#1090#1088#1086#1074#1086#1075#1086' '#1080#1085#1078#1077#1085#1077#1088#1072#13#10
        Align = alClient
        BevelEdges = [beLeft, beRight]
        BevelInner = bvNone
        BevelKind = bkFlat
        BevelOuter = bvNone
        BorderStyle = bsNone
        Color = clWhite
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Pitch = fpVariable
        Font.Style = []
        Font.Quality = fqClearType
        ParentFont = False
        ParentShowHint = False
        ScrollBars = ssVertical
        ShowHint = True
        TabOrder = 1
        WantTabs = True
        StyleElements = []
        OnExit = MemoConclusionEnter
      end
      object Panel5: TPanel
        Left = 0
        Top = 0
        Width = 946
        Height = 33
        Align = alTop
        BevelOuter = bvNone
        Caption = #1047#1072#1082#1083#1102#1095#1077#1085#1080#1077' '#1082#1072#1076#1072#1089#1090#1088#1086#1074#1086#1075#1086' '#1080#1085#1078#1077#1085#1077#1088#1072
        Color = clSilver
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWhite
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Pitch = fpVariable
        Font.Style = [fsBold]
        Font.Quality = fqClearType
        ParentBackground = False
        ParentFont = False
        TabOrder = 0
        StyleElements = []
      end
      object pnl1: TPanel
        Left = 0
        Top = 288
        Width = 946
        Height = 120
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 2
        object lbl1: TLabel
          Left = 16
          Top = 48
          Width = 180
          Height = 13
          Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1089#1080#1089#1090#1077#1084#1099' '#1082#1086#1086#1088#1076#1080#1085#1072#1090
        end
        object pnl2: TPanel
          Left = 0
          Top = 0
          Width = 946
          Height = 33
          Align = alTop
          BevelOuter = bvNone
          Caption = #1057#1080#1089#1090#1077#1084#1072' '#1082#1086#1086#1088#1076#1080#1085#1072#1090
          Color = clSilver
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Pitch = fpVariable
          Font.Style = [fsBold]
          Font.Quality = fqClearType
          ParentBackground = False
          ParentFont = False
          TabOrder = 0
          StyleElements = []
        end
        object edtSystemCoorName: TEdit
          Left = 234
          Top = 42
          Width = 515
          Height = 23
          BevelInner = bvLowered
          BevelKind = bkFlat
          BorderStyle = bsNone
          TabOrder = 1
          Text = #1057#1050' '#1082#1072#1076#1072#1089#1090#1088#1086#1074#1086#1075#1086' '#1086#1082#1088#1091#1075#1072
          OnExit = edtSystemCoorNameExit
        end
      end
    end
    object TabSheetInputDate: TTabSheet
      Caption = #1048#1089#1093#1086#1076#1085#1099#1077' '#1076#1072#1085#1085#1099#1077
      ImageIndex = 5
      TabVisible = False
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Splitter3: TSplitter
        Left = 249
        Top = 33
        Width = 4
        Height = 375
        Beveled = True
        ResizeStyle = rsUpdate
        ExplicitHeight = 377
      end
      object pnlAttributeInputDate: TPanel
        Left = 0
        Top = 33
        Width = 249
        Height = 375
        Align = alLeft
        BevelEdges = []
        BevelKind = bkFlat
        BevelOuter = bvNone
        TabOrder = 0
        object lbGeodesic_Bases: TLabel
          AlignWithMargins = True
          Left = 3
          Top = 3
          Width = 218
          Height = 20
          Cursor = crHandPoint
          ParentCustomHint = False
          Align = alTop
          BiDiMode = bdLeftToRight
          Caption = #1057#1074#1077#1076#1077#1085#1080#1103' '#1086' '#1075#1077#1086#1076#1077#1079#1080#1095#1077#1089#1082#1086#1081' '#1086#1089#1085#1086#1074#1077' '
          Color = clBtnFace
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Arial Narrow'
          Font.Pitch = fpVariable
          Font.Style = [fsBold, fsItalic]
          Font.Quality = fqClearType
          ParentBiDiMode = False
          ParentColor = False
          ParentFont = False
          ParentShowHint = False
          ShowHint = False
          Transparent = False
          WordWrap = True
          StyleElements = []
          OnClick = lbGeodesic_BasesClick
        end
        object lbMeans_Survey: TLabel
          AlignWithMargins = True
          Left = 3
          Top = 147
          Width = 214
          Height = 20
          Cursor = crHandPoint
          ParentCustomHint = False
          Align = alTop
          BiDiMode = bdLeftToRight
          Caption = #1057#1074#1077#1076#1077#1085#1080#1103' '#1086' '#1089#1088#1077#1076#1089#1090#1074#1072#1093' '#1080#1079#1084#1077#1088#1077#1085#1080#1103
          Color = clBtnFace
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Arial Narrow'
          Font.Pitch = fpVariable
          Font.Style = [fsBold, fsItalic]
          Font.Quality = fqClearType
          ParentBiDiMode = False
          ParentColor = False
          ParentFont = False
          ParentShowHint = False
          ShowHint = False
          Transparent = False
          WordWrap = True
          StyleElements = []
          OnClick = lbMeans_SurveyClick
        end
        object lbRealty: TLabel
          AlignWithMargins = True
          Left = 3
          Top = 29
          Width = 174
          Height = 20
          Cursor = crHandPoint
          Hint = 
            #1057#1074#1077#1076#1077#1085#1080#1103' '#1086' '#1085#1072#1083#1080#1095#1080#1080' '#1079#1076#1072#1085#1080#1081', '#1089#1086#1086#1088#1091#1078#1077#1085#1080#1081', '#1086#1073#1098#1077#1082#1090#1086#1074' '#1085#1077#1079#1072#1074#1077#1088#1096#1077#1085#1085#1086#1075#1086' '#1089 +
            #1090#1088#1086#1080#1090#1077#1083#1100#1089#1090#1074#1072' '#1085#1072' '#1080#1089#1093#1086#1076#1085#1099#1093' '#1080#1083#1080' '#1080#1079#1084#1077#1085#1077#1085#1085#1099#1093' '#1079#1077#1084#1077#1083#1100#1085#1099#1093' '#1091#1095#1072#1089#1090#1082#1072#1093
          ParentCustomHint = False
          Align = alTop
          BiDiMode = bdLeftToRight
          Caption = #1057#1074#1077#1076#1077#1085#1080#1103' '#1086' '#1085#1072#1083#1080#1095#1080#1080' '#1079#1076#1072#1085#1080#1081
          Color = clBtnFace
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Arial Narrow'
          Font.Pitch = fpVariable
          Font.Style = [fsBold, fsItalic]
          Font.Quality = fqClearType
          ParentBiDiMode = False
          ParentColor = False
          ParentFont = False
          ParentShowHint = False
          ShowHint = False
          Transparent = False
          WordWrap = True
          StyleElements = []
          OnClick = lbRealtyClick
        end
        object lbSubParcels: TLabel
          AlignWithMargins = True
          Left = 3
          Top = 55
          Width = 193
          Height = 60
          Cursor = crHandPoint
          ParentCustomHint = False
          Align = alTop
          BiDiMode = bdLeftToRight
          Caption = 
            #1057#1074#1077#1076#1077#1085#1080#1103' '#1086' '#1095#1072#1089#1090#1103#1093' '#1080#1089#1093#1086#1076#1085#1099#1093', '#1080#1079#1084#1077#1085#1077#1085#1085#1099#1093' '#1080#1083#1080' '#1091#1090#1086#1095#1085#1103#1077#1084#1099#1093' '#1079#1077#1084#1077#1083#1100#1085#1099#1093' ' +
            #1091#1095#1072#1089#1090#1082#1086#1074
          Color = clBtnFace
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Arial Narrow'
          Font.Pitch = fpVariable
          Font.Style = [fsBold, fsItalic]
          Font.Quality = fqClearType
          ParentBiDiMode = False
          ParentColor = False
          ParentFont = False
          ParentShowHint = False
          ShowHint = False
          Transparent = False
          WordWrap = True
          StyleElements = []
          OnClick = lbSubParcelsClick
          OnMouseUp = lbSubParcelsMouseUp
        end
        object lbDocuments: TLabel
          AlignWithMargins = True
          Left = 3
          Top = 121
          Width = 141
          Height = 20
          Cursor = crHandPoint
          ParentCustomHint = False
          Align = alTop
          BiDiMode = bdLeftToRight
          Caption = #1055#1077#1088#1077#1095#1077#1085#1100' '#1076#1086#1082#1091#1084#1077#1085#1090#1086#1074
          Color = clBtnFace
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Arial Narrow'
          Font.Pitch = fpVariable
          Font.Style = [fsBold, fsItalic]
          Font.Quality = fqClearType
          ParentBiDiMode = False
          ParentColor = False
          ParentFont = False
          ParentShowHint = False
          ShowHint = False
          Transparent = False
          WordWrap = True
          StyleElements = []
          OnClick = lbDocumentsClick
        end
      end
      object Panel6: TPanel
        Left = 0
        Top = 0
        Width = 946
        Height = 33
        Align = alTop
        BevelOuter = bvNone
        Caption = #1048#1089#1093#1086#1076#1085#1099#1077' '#1076#1072#1085#1085#1099#1077
        Color = 11364884
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWhite
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Pitch = fpVariable
        Font.Style = [fsBold]
        Font.Quality = fqClearType
        ParentBackground = False
        ParentFont = False
        TabOrder = 1
        StyleElements = []
      end
    end
  end
  object OpenDialog: TOpenDialog
    DefaultExt = #1052#1077#1078#1077#1074#1086#1081' '#1087#1083#1072#1085' '#1074' '#1101#1083#1077#1082#1090#1088#1086#1085#1085#1086#1084' '#1092#1086#1088#1084#1072#1090#1077' xml|*.xml'
    Left = 24
    Top = 288
  end
  object XPManifest1: TXPManifest
    Left = 104
    Top = 288
  end
end
