unit fmTextInf;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ButtonGroup, StdCtrls, ActnList, JvHtControls, Vcl.Buttons, Vcl.ExtCtrls, JvExStdCtrls,
  JvListBox;

type
  TFormTextInf = class(TForm)
    PanelControl: TPanel;
    CloseBitBtn: TBitBtn;
    SaveBitBtn: TBitBtn;
    SaveDialog1: TSaveDialog;
    lstValidatorLog: TJvHTListBox;
    procedure CloseBitBtnClick(Sender: TObject);
    procedure SaveBitBtnClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }

  end;

var
  FormTextInf: TFormTextInf;

implementation

{$R *.dfm}

{ TTextInfForm }


procedure TFormTextInf.CloseBitBtnClick(Sender: TObject);
begin
 self.close;
end;

procedure TFormTextInf.SaveBitBtnClick(Sender: TObject);
begin
 if self.SaveDialog1.Execute then
  self.lstValidatorLog.Items.SaveToFile(self.SaveDialog1.FileName);
end;


end.
