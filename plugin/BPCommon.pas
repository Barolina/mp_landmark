unit BpCommon;

interface
 uses
  Math,
  MSMType_, {MsVectorObjects,} msStringParser, SysUtils;

 const
  cBPField_DesignationLand = 'KN';
  cBPCategoryLP = 'BPCategoryLP';
  cBPCaptionScale = 'BPScale';
  cBpDefinitionLand = '��';
  cBpDefinitionPart = '���';

  cBpMethod_NewParcel_Videl = 1;
  cBpMethod_NewParcel_Razdel = 2;
  cBpMethod_NewParcel_RazdelSIzmenennim = 3;
  cBpMethod_NewParcel_Pereraspredelenie = 4;
  cBpMethod_NewParcel_ObrazovanieIzZemel = 5;
  cBpMethod_NewParcel_Obidinenie = 6;

  cBpMethod_ExistParcel: byte = 0;



  _DefDelemitersCadNumItem: string = ':';
  _DefDelemitersCadNumDefinitionPart = '/';
  _DefDelemitersCadNumExDefinition_l: string = '(';
  _DefDelemitersCadNumExDefinition_r: string = ')';

 cBPMaskCadastralNumber: string = '00:00:9999999:!9999;1;';




 type
 //����������� �����

 //������� ����� ���.������
   //*?(?)   - ������ ����������� �����. ��1: 25. ��2: 25(1)
   //?:��*?(?) � ���. ������������ (��� ������� ��� ����������� ������ ������)
   //--> ��1: 25:��1. ��2: 25:��1(1). ��3: ��1(1)
   //*?/���*?(?) ����������� ����� ��. ��1: 1/���1. ��2: 1/���1(1)
   //?:��*?/���*?(?) ����� ����������� ��. ��1: ��1/���1(1)

 //����������� �����. ������� ����� ����������� �����:��� ���:����:�����
   //**:**:*******:*?(?)
   //**:**:*******:?:��*?(?)
   //**:**:*******:*?/���*?(?)
   //**:**:*******:?:��*?/���*?(?)


 TCadNumItem = (
                cnDistrict,cnMunicipality,cnBlock,cnNum,
                cnDefinitionLand,      {��*-->��*}
                cnIndexDefinitionLand, {��*-->*}
                cnDefinitionPart,      {���*-->���*}
                cnIndexDefinitionPart, {���*-->*}
                cnIndexDefinitionContour,    {(*)-->*}
                cnDefinitionContour    {(*)-->(*)}
               );
 TCadNumItems = set of TCadNumItem;
 //�������� �������� ��������(��)
 function BPcCadNum_GetItemValue(CadNum: string; Items: TCadNumItems): string;

 //��������� ������������� �������� ������������ ������
 function BPcCadNum_IsExistItemValue(CadNum: string; Items: TCadNumItems): Boolean;

 //�������� �������� ������������ ������
 function BPcCadNum_IsEqual(aCadNum,bCadNum: string; ItemsCompare: TCadNumItems): Boolean;

implementation





 function BPcCadNum_GetItemValue(CadNum: string; Items: TCadNumItems): string;
 var
  _St: string;
  _i,_j,_k: Integer;
  _parse: TMSStringParser;
 begin
  Result:= '';
  if CadNum = '' then EXIT;

  _parse:= TMSStringParser.Create(_DefDelemitersCadNumItem+_DefDelemitersCadNumDefinitionPart+
                                  _DefDelemitersCadNumExDefinition_l+_DefDelemitersCadNumExDefinition_r);
  _parse.Parse(CadNum);

  if _parse.GetCountToken=0 then EXIT;

  if cnDistrict in Items then
    Result:= _parse.GetToken(0);

  if (cnMunicipality in Items) and (_parse.GetCountToken>1) then begin
    if Result<>'' then
     Result:= Result+_DefDelemitersCadNumItem;
    Result:= Result + _parse.GetToken(1);
  end;

  if (cnBlock in Items) and (_parse.GetCountToken>2) then begin
    if Result<>'' then
     Result:= Result+_DefDelemitersCadNumItem;
    Result:= Result + _parse.GetToken(2);
  end;

  if (cnNum in Items) and (_parse.GetCountToken>3) then begin
   //���� ����� �� �������� ������������
   if Pos(UpperCase(cBpDefinitionLand),UpperCase(_parse.GetToken(3))) =0 then begin
    if Result<>'' then
      Result:= Result+_DefDelemitersCadNumItem;
     Result:= Result + _parse.GetToken(3);
   end;
  end;

  _parse.Free;

  if (cnDefinitionLand in Items) then begin
    _i:= AnsiPos(UpperCase(cBpDefinitionLand),UpperCase(CadNum));
    if _i<>0 then begin
     if Result<>'' then
       Result:= Result+_DefDelemitersCadNumItem;
     Result:= Result+cBpDefinitionLand;
     for _j := _i+Length(cBpDefinitionLand) to length(CadNum) do
      if TryStrToInt(CadNum[_j],_k) then
       Result:= Result+CadNum[_j]
      else
       Break;
    end;
  end;

  if (cnIndexDefinitionLand in Items) then begin
    _i:= AnsiPos(AnsiUpperCase(cBpDefinitionLand),AnsiUpperCase(CadNum));
    if _i<>0 then begin
      for _j := _i+Length(cBpDefinitionLand) to length(CadNum) do
      if TryStrToInt(CadNum[_j],_k) then
       Result:= Result+CadNum[_j]
      else
       Break;
    end;
  end;

  if (cnDefinitionPart in Items) then begin
    _i:= AnsiPos(AnsiUpperCase(cBpDefinitionPart),AnsiUpperCase(CadNum));
    if _i<>0 then begin
     if Result<>'' then
       Result:= Result+_DefDelemitersCadNumDefinitionPart;
     Result:= Result+cBpDefinitionPart;
     for _j := _i+Length(cBpDefinitionPart) to length(CadNum) do
      if TryStrToInt(CadNum[_j],_k) then
       Result:= Result+CadNum[_j]
      else
       break;
    end;
  end;

  if (cnIndexDefinitionPart in items) then begin
    _i:= AnsiPos(AnsiUpperCase(cBpDefinitionPart),AnsiUpperCase(CadNum));
    if _i<>0 then begin
      for _j := _i+Length(cBpDefinitionPart) to length(CadNum) do
      if TryStrToInt(CadNum[_j],_k) then
       Result:= Result+CadNum[_j]
      else
       Break;
    end;
  end;

  if (cnIndexDefinitionContour in  Items) then begin
    _i:= AnsiPos(_DefDelemitersCadNumExDefinition_l,CadNum);
    if _i<>0 then begin
     if Result<>'' then begin
       Result:= Result+_DefDelemitersCadNumExDefinition_l;
       for _j := _i+1 to length(CadNum) do begin
         if CadNum[_j] = _DefDelemitersCadNumExDefinition_r then BREAK;
         Result:= Result+CadNum[_j];
       end;
       Result:= Result+_DefDelemitersCadNumExDefinition_r;
     end else
       for _j := _i+1 to length(CadNum) do begin
         if CadNum[_j] = _DefDelemitersCadNumExDefinition_r then BREAK;
         Result:= Result+CadNum[_j];
       end;
    end;
  end;

  if (cnDefinitionContour in  Items) then begin
    _i:= AnsiPos(_DefDelemitersCadNumExDefinition_l,CadNum);
    if _i<>0 then
       for _j := _i to length(CadNum) do begin
        Result:= Result+CadNum[_j];
        if CadNum[_j] = _DefDelemitersCadNumExDefinition_r then BREAK;
       end;
  end;

 end;

 function BPcCadNum_IsExistItemValue(CadNum: string; Items: TCadNumItems): boolean;
var
  _parse: TMSStringParser;
 begin
  Result:= true;
  if CadNum = '' then begin
   Result:= false;
   EXIT;
  end;

  _parse:= TMSStringParser.Create(_DefDelemitersCadNumItem);
  _parse.Parse(CadNum);

   if _parse.GetCountToken=0 then EXIT;

   if cnDistrict in Items then
     Result:= (_parse.GetToken(0)<>'');

   if (cnMunicipality in Items) then
     Result:= Result and (_parse.GetCountToken>1);

   if (cnBlock in Items) then
     Result:= Result and (_parse.GetCountToken>2);

   if (cnNum in Items) then
     Result:= Result and (_parse.GetCountToken>3);

  _parse.Free;

  if (cnDefinitionLand in Items) or (cnIndexDefinitionLand in Items) then
    Result:= Result and (AnsiPos(UpperCase(cBpDefinitionLand),UpperCase(CadNum))<>0);

  if (cnDefinitionPart in Items) or (cnIndexDefinitionPart in items) then
    Result:= Result and (AnsiPos(AnsiUpperCase(cBpDefinitionPart),AnsiUpperCase(CadNum))<>0);

  if (cnDefinitionContour in  Items) or (cnIndexDefinitionContour in  Items) then
    Result:= Result and (AnsiPos(_DefDelemitersCadNumExDefinition_l,CadNum)<>0);

 end;

 function BPcCadNum_IsEqual(aCadNum,bCadNum: string; ItemsCompare: TCadNumItems): Boolean;
 begin
   Result:= CompareText(BPcCadNum_GetItemValue(aCadNum,ItemsCompare),
                        BPcCadNum_GetItemValue(bCadNum,ItemsCompare))=0;

 end;

end.
