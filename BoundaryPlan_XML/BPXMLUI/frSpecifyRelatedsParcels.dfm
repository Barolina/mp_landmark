object FrameSpecifyRelatedsParcels: TFrameSpecifyRelatedsParcels
  Left = 0
  Top = 0
  Width = 879
  Height = 374
  TabOrder = 0
  object RelatedPanel: TPanel
    Left = 0
    Top = 0
    Width = 879
    Height = 374
    Align = alClient
    BevelOuter = bvNone
    Caption = #1059#1090#1086#1095#1085#1077#1085#1080#1077' '#1075#1088#1072#1085#1080#1094' '#1089#1084#1077#1078#1085#1099#1093' '#1091#1095#1072#1089#1090#1082#1086#1074
    ParentBackground = False
    TabOrder = 0
    object Panel_PanelControlSpecifyRelatedParce: TPanel
      Left = 0
      Top = 0
      Width = 879
      Height = 41
      Align = alTop
      Anchors = [akLeft, akRight]
      BevelEdges = [beLeft, beRight]
      BevelKind = bkFlat
      BevelOuter = bvNone
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentBackground = False
      ParentFont = False
      TabOrder = 0
      object pnl1: TPanel
        Left = 0
        Top = 0
        Width = 409
        Height = 41
        Align = alLeft
        BevelEdges = [beLeft, beRight]
        BevelOuter = bvNone
        TabOrder = 0
        object Label4: TLabel
          Left = 6
          Top = -10
          Width = 143
          Height = 26
          Alignment = taCenter
          Caption = #13#10#1057#1087#1080#1089#1086#1082' '#1091#1090#1086#1095#1085#1103#1077#1084#1099#1093' '#1047#1059':'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object SpecifyRelatedParcelItems_ComboBox: TComboBox
          Left = 5
          Top = 16
          Width = 321
          Height = 22
          BevelInner = bvNone
          BevelKind = bkFlat
          BevelOuter = bvRaised
          Style = csOwnerDrawFixed
          Color = 15921123
          Constraints.MaxHeight = 22
          DropDownCount = 20
          TabOrder = 0
          TextHint = #1050#1072#1076#1072#1089#1090#1088#1086#1074#1099#1081' '#1085#1086#1084#1077#1088' '#1047#1059
          OnChange = SpecifyRelatedParcelItems_ComboBoxChange
        end
      end
      object jvtlbr2: TJvToolBar
        Left = 330
        Top = 16
        Width = 455
        Height = 29
        ParentCustomHint = False
        Align = alNone
        ButtonWidth = 200
        Caption = 'jvtlbr1'
        Color = 15711412
        DoubleBuffered = False
        DrawingStyle = dsGradient
        EdgeInner = esNone
        EdgeOuter = esNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        GradientEndColor = 15197907
        GradientStartColor = clWhite
        Images = formContainer.ilCommands
        List = True
        GradientDirection = gdHorizontal
        ParentColor = False
        ParentDoubleBuffered = False
        ParentFont = False
        ParentShowHint = False
        AllowTextButtons = True
        ShowHint = True
        TabOrder = 1
        Transparent = False
        StyleElements = []
        Wrapable = False
        HintColor = 13551291
        object btnAdd: TToolButton
          Left = 0
          Top = 0
          Hint = #13#10#1044#1086#1073#1072#1074#1080#1090#1100' '#1089#1084#1077#1078#1085#1080#1082#1072#13#10
          Action = actSpecifyRelatedParcel_AddNew
        end
        object btnDel: TToolButton
          Left = 24
          Top = 0
          Hint = #13#10#1059#1076#1072#1083#1080#1090#1100' '#1089#1084#1077#1078#1085#1080#1082#1072#13#10
          Action = actSpecifyRelatedParcel_Delete
        end
        object btnEdit: TToolButton
          Left = 48
          Top = 0
          Hint = #13#10#1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1090#1100'  '#1047#1059#13#10
          Action = actSpecifyRelatedParcel_EditDefinition
        end
        object btn1: TToolButton
          Left = 72
          Top = 0
          Width = 25
          ParentCustomHint = False
          Caption = 'btn1'
          ImageIndex = 5
          ParentShowHint = False
          ShowHint = True
          Style = tbsSeparator
        end
        object btnEncumbrance: TToolButton
          Left = 97
          Top = 0
          Grouped = True
          ImageIndex = 20
          Marked = True
          Style = tbsTextButton
          Visible = False
          OnClick = btnEncumbranceClick
        end
        object btnEntity_Spatial: TToolButton
          Left = 131
          Top = 0
          Hint = 
            #13#10#1054#1087#1080#1089#1072#1085#1080#1077' '#1075#1088#1072#1085#1080#1094' '#1089#1091#1097#1077#1089#1090#1074#1091#1102#1097#1080#1093' '#1095#1072#1089#1090#1077#1081#13#10' '#1091#1090#1086#1095#1085#1103#1077#1084#1086#1075#1086' '#1089#1084#1077#1078#1085#1086#1075#1086' '#1091#1095#1072 +
            #1089#1090#1082#1072#13#10' ('#1074' '#1090'.'#1095'. '#1084#1085#1086#1075#1086#1082#1086#1085#1090#1091#1088#1085#1099#1093' '#1095#1072#1089#1090#1077#1081
          Caption = #1054#1087#1080#1089#1072#1085#1080#1077' '#1075#1088#1072#1085#1080#1094' '#1089#1091#1097'. '#1095#1072#1089#1090#1077#1081' '
          Grouped = True
          ImageIndex = 19
          Marked = True
          Style = tbsTextButton
          Visible = False
          OnClick = btnEntity_SpatialClick
        end
      end
    end
    object RelatedElemPanel: TPanel
      Left = 0
      Top = 41
      Width = 879
      Height = 333
      Align = alClient
      BevelOuter = bvNone
      ParentBackground = False
      TabOrder = 1
      object lblArea: TLabel
        Left = 0
        Top = 0
        Width = 879
        Height = 33
        Align = alTop
        Alignment = taCenter
        Anchors = [akLeft]
        AutoSize = False
        Caption = #1050#1086#1086#1088#1076#1080#1085#1072#1090#1085#1086#1077' '#1086#1087#1080#1089#1072#1085#1080#1077
        Color = 11364884
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        Transparent = False
        Layout = tlCenter
        StyleElements = []
        ExplicitTop = 2
        ExplicitWidth = 877
      end
    end
  end
  object actlst: TActionList
    Images = formContainer.ilCommands
    Left = 24
    Top = 48
    object actSpecifyRelatedParcel_AddNew: TAction
      Category = #1059#1090#1086#1095#1085#1077#1085#1080#1077' '#1089#1084#1077#1078#1085#1099#1093' '#1047#1059
      Caption = 'actSpecifyRelatedParcel_AddNew'
      ImageIndex = 0
      OnExecute = actSpecifyRelatedParcel_AddNewExecute
    end
    object actSpecifyRelatedParcel_Delete: TAction
      Category = #1059#1090#1086#1095#1085#1077#1085#1080#1077' '#1089#1084#1077#1078#1085#1099#1093' '#1047#1059
      Caption = 'actSpecifyRelatedParcel_Delete'
      ImageIndex = 1
      OnExecute = actSpecifyRelatedParcel_DeleteExecute
    end
    object actSpecifyRelatedParcel_EditDefinition: TAction
      Category = #1059#1090#1086#1095#1085#1077#1085#1080#1077' '#1089#1084#1077#1078#1085#1099#1093' '#1047#1059
      Caption = 'actSpecifyRelatedParcel_EditDefinition'
      ImageIndex = 9
      OnExecute = actSpecifyRelatedParcel_EditDefinitionExecute
    end
  end
end
