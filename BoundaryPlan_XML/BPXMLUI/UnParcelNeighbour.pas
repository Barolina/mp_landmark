unit UnParcelNeighbour;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, System.Actions, Vcl.ActnList,

   STD_MP, ImgList, frOwnerNeighbours,XMLIntf, Vcl.ExtCtrls, Vcl.StdCtrls;

type
  TFrmParcelNeighbour = class(TForm)
    ActionList: TActionList;
    Action_Contrours_AddNew: TAction;
    Action_Contours_DeleteCurrent: TAction;
    Action_Contours_SetCurrent: TAction;
    pnl1: TPanel;
    btn1: TButton;
    pnl2: TPanel;
    Button1: TButton;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    FXmlParcelNeighbour            : IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour;
    FOwnerNeighbourr_frames        : TFrameOwnerNeighbours;
    FOnChangeData                  : TNotifyEvent;
  protected
   procedure InitializationUI;
   procedure SetContoursInvariable(const aContours: IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour);

  public
    { Public declarations }
   function IsAssignedContours: Boolean;

   property OwnerNeighbours: TFrameOwnerNeighbours read FOwnerNeighbourr_frames;
   property XmlOwnerNeighbours: IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour read FXmlParcelNeighbour write SetContoursInvariable;
   property OnChangeData : TNotifyEvent read FOnChangeData write FOnChangeData;

  end;

var
  FrmParcelNeighbour: TFrmParcelNeighbour;

implementation

{$R *.dfm}

{ TFrmParcelNeighbour }


procedure TFrmParcelNeighbour.FormCreate(Sender: TObject);
begin
  self.FOwnerNeighbourr_frames:= TFrameOwnerNeighbours.Create(self.pnl1);
 self.FOwnerNeighbourr_frames.Parent:= self.pnl1;
 self.FOwnerNeighbourr_frames.Align:= alClient;
end;

procedure TFrmParcelNeighbour.InitializationUI;
begin
 if self.IsAssignedContours then
  FOwnerNeighbourr_frames.XmlOwnerNeighbours := FXmlParcelNeighbour.OwnerNeighbours
 else
  FOwnerNeighbourr_frames.XmlOwnerNeighbours := nil;
end;

function TFrmParcelNeighbour.IsAssignedContours: Boolean;
begin
 Result:= self.FXmlParcelNeighbour<>nil;
end;

procedure TFrmParcelNeighbour.SetContoursInvariable(
  const aContours: IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour);
begin
 if self.FXmlParcelNeighbour=aContours then EXIT;
 begin
     self.FXmlParcelNeighbour := aContours;
     self.InitializationUI;
 end;
end;

end.
