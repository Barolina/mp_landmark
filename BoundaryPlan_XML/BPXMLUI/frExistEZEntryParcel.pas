unit frExistEZEntryParcel;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, Mask, ComCtrls, Buttons, Grids,

  frEntitySpatial, XMLDoc, XMLIntf, STD_MP, ActnList, ButtonGroup,
  PlatformDefaultStyleActnCtrls, ActnMan, PageControlAnimate,
  frArea,

  frEncumbrance,
  ImgList,
  System.Actions, frAreaDelta,
  unRussLang;

type
  TFrameExistEZEntryParcel = class(TFrame)
    Label3: TLabel;
    NewParcelsPageControl: TPageControl;
    Area_TabSheet: TTabSheet;
    Encumbrance_TabSheet: TTabSheet;
    Coordinats_TabSheet: TTabSheet;
    Panel_Navigation: TPanel;
    SpeedButton_Nav_Back: TSpeedButton;
    SpeedButton_Nav_Next: TSpeedButton;
    ComboBox_Nav_GoTo: TComboBox;
    SubParcels_LabelCaption: TLabel;
    OrdinatesPageControl: TPageControl;
    Contours_TabSheet: TTabSheet;
    EntitySpatial_TabSheet: TTabSheet;
    Area_IsFill_CheckBox: TCheckBox;
    Coordinats_Label_Caption: TLabel;
    Coordinats_IsFill_CheckBox: TCheckBox;

    ActionManager_FormParcels: TActionList;
    ActionXML_Attributes_Write_Name: TAction;
    ActionXML_Attributes_Write_AdditionalName: TAction;
    ActionXML_Attributes_Write_Method: TAction;
    ActionXML_Attributes_Write_Definition: TAction;
    ActionXML_CadastralBlock_Write: TAction;
    ActionXML_Category_Write: TAction;
    Action_PrevCadNums_IsFill: TAction;
    Action_ProvPassCadNums_IsFil: TAction;
    Action_InnerCadNums_IsFill: TAction;
    Action_LocationAddress_IsFill: TAction;
    Action_NaturalObject_IsFill: TAction;
    Action_SubParcels_IsFill: TAction;
    Action_Encumbrance_IsFill: TAction;
    chkEncumbrnce: TCheckBox;
    pnl1: TPanel;

    procedure SpeedButton_Nav_BackClick(Sender: TObject);
    procedure SpeedButton_Nav_NextClick(Sender: TObject);
    procedure ComboBox_Nav_GoToChange(Sender: TObject);

    procedure Attributes_Definition_EditKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Attributes_AdditionalName_EditKeyUp(Sender: TObject;
      var Key: Word; Shift: TShiftState);
    procedure medtCadastralBlock_value_MaskEditKeyUp(Sender: TObject;
      var Key: Word; Shift: TShiftState);
    procedure NewParcelsPageControlChange(Sender: TObject);
    procedure chkEncumbrnceClick(Sender: TObject);
    procedure SubParcels_LabelCaptionClick(Sender: TObject);

  private
    FXmlNewParcel        : IXMLTExistEZEntryParcel; // ������� ��������� �������

    FArea_frame          : TFrameArea; // �������
    FAreaDelta_frame     : TFrameAreaDelta; // �������������� ���������
    FEncumbrance_frame   : TFrameEncumbrance;
    FEntitySpatial_frame : TFrameEntity_Spatial; // ���� �������������
    FOnChangeDefinition  : TNotifyEvent;

    procedure InitializationUI; // ���������������� ����������

    // ��������� ������� NewParcel
    procedure SetNewParcel(const aNewParcel: IXMLTExistEZEntryParcel);
    function IsAssigned_NewParcel: Boolean;
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    property XmlNewParcel: IXMLTExistEZEntryParcel read FXmlNewParcel write SetNewParcel;
    procedure SetActivTab(index: integer; UseAnimate: Boolean = false);

    Property OnChangeDefinition: TNotifyEvent read FOnChangeDefinition
      write FOnChangeDefinition;
  end;

implementation

uses MsXMLAPI;

procedure _HiddenChangeCheckBoxSate(ChBox: TCheckBox; NewSate: TCheckBoxState);
var
  _OnClick: TNotifyEvent;
begin
  if ChBox.State = NewSate then
    EXIT;
  _OnClick := ChBox.OnClick;
  ChBox.OnClick := nil;
  ChBox.State := NewSate;
  ChBox.OnClick := _OnClick;
end;

{$R *.dfm}
{ TFrameNewparcel }

procedure TFrameExistEZEntryParcel.Attributes_AdditionalName_EditKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key = VK_RETURN then
    if (Sender is TCustomEdit) then
      TCustomEdit(Sender).Parent.SetFocus;
end;

procedure TFrameExistEZEntryParcel.Attributes_Definition_EditKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key = VK_RETURN then
    if (Sender is TCustomEdit) then
      TCustomEdit(Sender).Parent.SetFocus;
end;

procedure TFrameExistEZEntryParcel.medtCadastralBlock_value_MaskEditKeyUp
  (Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if Key = VK_RETURN then
    if (Sender is TCustomEdit) then
      TCustomEdit(Sender).Parent.SetFocus;
end;

procedure TFrameExistEZEntryParcel.ComboBox_Nav_GoToChange(Sender: TObject);
begin
  self.SetActivTab(self.ComboBox_Nav_GoTo.ItemIndex, false);
end;

constructor TFrameExistEZEntryParcel.Create(AOwner: TComponent);
begin
  inherited;

  self.FArea_frame := TFrameArea.Create(self.Area_TabSheet);
  self.FArea_frame.Parent := self.Area_TabSheet;
  self.FArea_frame.Align := alTop;
  self.FArea_frame.Visible := true;

  self.FAreaDelta_frame := TFrameAreaDelta.Create(self.Area_TabSheet);
  self.FAreaDelta_frame.Parent := self.Area_TabSheet;
  self.FAreaDelta_frame.Align := alClient;
  self.FAreaDelta_frame.Visible := true;
  { ������ �� ������� ��� �����������  ���� ��� ����� }
  self.FAreaDelta_frame.Area_In_GKN_MaskEdit.Visible := false;
  self.FAreaDelta_frame.Area_In_GKN_Label_Caption.Visible := false;
  self.FAreaDelta_frame.Delta_Area_MaskEdit.Visible := false;
  self.FAreaDelta_frame.Delta_Area_Label_Caption.Visible := false;

  Self.FEncumbrance_frame := TFrameEncumbrance.Create(self.Encumbrance_TabSheet);
  self.FEncumbrance_frame.Parent := self.Encumbrance_TabSheet;
  self.FEncumbrance_frame.Align := alClient;
  self.FEncumbrance_frame.Visible := false;

  self.FEntitySpatial_frame := TFrameEntity_Spatial.Create(self.EntitySpatial_TabSheet);
  self.FEntitySpatial_frame.Parent := self.EntitySpatial_TabSheet;
  self.FEntitySpatial_frame.Align := alClient;
  self.FEntitySpatial_frame.Show;
end;

procedure TFrameExistEZEntryParcel.InitializationUI;
var
  _node: IXMLNode;
begin

  if self.IsAssigned_NewParcel then
  begin
    self.NewParcelsPageControl.Visible := true;
    self.Panel_Navigation.Visible := true;
    FArea_frame.XmlAreaExistEZ := FXmlNewParcel.Area;
    FAreaDelta_frame.XmlExistEZExistParcel := FXmlNewParcel;

    _node := self.FXmlNewParcel.ChildNodes.FindNode(cnstXmlEncumbrance);
    chkEncumbrnce.Checked := (_node <> nil);
    if _node <> nil then
      self.FEncumbrance_frame.Encumbrance := FXmlNewParcel.Encumbrance
    else
      self.FEncumbrance_frame.Encumbrance := nil;
      
    self.FEntitySpatial_frame.EntitySpatial:=  self.FXmlNewParcel.Entity_Spatial
      
   end
  else
  begin
    self.NewParcelsPageControl.Visible := false;
    self.Panel_Navigation.Visible := false;

    self.FArea_frame.XmlAreaExistEZ := nil;
    self.FAreaDelta_frame.XmlExistEZExistParcel := nil;
    self.FEncumbrance_frame.Encumbrance := nil;
    self.FEntitySpatial_frame.EntitySpatial := nil;
  end;

end;

function TFrameExistEZEntryParcel.IsAssigned_NewParcel: Boolean;
begin
  Result := self.FXmlNewParcel <> nil;
end;

procedure TFrameExistEZEntryParcel.NewParcelsPageControlChange(Sender: TObject);
begin
  self.SpeedButton_Nav_Back.Visible :=
    self.NewParcelsPageControl.ActivePageIndex = 0;
  self.SpeedButton_Nav_Next.Visible :=
    self.NewParcelsPageControl.ActivePageIndex = self.NewParcelsPageControl.
    PageCount - 1;

end;

procedure TFrameExistEZEntryParcel.SetActivTab(index: integer; UseAnimate: Boolean = false);
begin
  if (index < 0) or (index > self.NewParcelsPageControl.PageCount - 1) then
    EXIT;

  if UseAnimate then
    pcaSetActivTab(self.NewParcelsPageControl,
      self.NewParcelsPageControl.Pages[index])
  else
    self.NewParcelsPageControl.ActivePageIndex := index;

  self.ComboBox_Nav_GoTo.ItemIndex :=
    self.NewParcelsPageControl.ActivePageIndex;
  self.SpeedButton_Nav_Back.Enabled := self.ComboBox_Nav_GoTo.ItemIndex <> 0;
  self.SpeedButton_Nav_Next.Enabled := self.ComboBox_Nav_GoTo.ItemIndex <>
    self.ComboBox_Nav_GoTo.Items.Count - 1;
end;

procedure TFrameExistEZEntryParcel.SetNewParcel(const aNewParcel: IXMLTExistEZEntryParcel);
begin
  if self.FXmlNewParcel = aNewParcel then EXIT;
  self.FXmlNewParcel := aNewParcel;
  self.InitializationUI;
end;

procedure TFrameExistEZEntryParcel.SpeedButton_Nav_BackClick(Sender: TObject);
begin
  self.SetActivTab(self.ComboBox_Nav_GoTo.ItemIndex - 1, false);
end;

procedure TFrameExistEZEntryParcel.SpeedButton_Nav_NextClick(Sender: TObject);
begin
  self.SetActivTab(self.ComboBox_Nav_GoTo.ItemIndex + 1, false);
end;

procedure TFrameExistEZEntryParcel.SubParcels_LabelCaptionClick(Sender: TObject);
begin
  self.chkEncumbrnce.Checked := not self.chkEncumbrnce.Checked;
end;

procedure TFrameExistEZEntryParcel.chkEncumbrnceClick(Sender: TObject);
begin
    if not self.chkEncumbrnce.Checked then
    begin
      if FEncumbrance_frame.Encumbrance <> nil then
        if MessageDlg('������� ������: '+SubParcels_LabelCaption.Caption+'?',mtInformation,mbYesNo,-1) = mrYes then
          begin
              MsXml_RemoveChildNode(self.FXmlNewParcel,cnstXmlEncumbrance);
              self.FEncumbrance_frame.Encumbrance:= nil;
              self.FEncumbrance_frame.Visible:= false;
          end else self.chkEncumbrnce.Checked := true;
    end else
    if self.chkEncumbrnce.Checked then begin
        self.FEncumbrance_frame.Encumbrance:= self.FXmlNewParcel.Encumbrance;
        self.FEncumbrance_frame.Visible:= true;
    end;

end;

end.
