unit SurveyingPlan.Editor.Utils;

interface
 uses
  System.classes,
  System.Sysutils,
  System.RegularExpressions,
  SurveyingPlan.Editor.Prototypes;

type

 TSpParseText = record
  public
   class procedure Parse(const aText: string; aDest: TSpPrototypeContour); static;
 end;

 TSpBuildText = record
  public
   class function Build(aDest: TSpPrototypePoint): string; overload; static;
   class function Build(aDest: TSpPrototypeBorder): string; overload; static;
   class function Build(aDest: TSpPrototypeContour): string; overload; static;
 end;

implementation

uses MSMCommon_;

resourcestring
 rsDefDelimiter = ' ';
 rsPref = 'pref';
 rsNump = 'nump';
 rsOldx = 'oldx';
 rsOldy = 'oldy';
 rsNewx = 'newx';
 rsNewy = 'newy';
 rsDx = 'dx';
 rsZakrep = 'zakrep';

 rsDefaultRegExp = '(?<pref>�)?[ ,;\t]?+(?<nump>(-+|[0-9]*))[ ,;\t]+(?<oldx>(-+|[0-9.]*))[ ,;\t]+(?<oldy>(-+|[0-9.]*))[ ,;\t](?<newx>(-+|[0-9.]*))[ ,;\t]+(?<newy>(-+|[0-9.]*))[ ,;\t](?<dx>(-+|[0-9.]*))[ ,;\t]+(?<zakrep>(-+|\w*))';


{ TSpParseText }

class procedure TSpParseText.Parse(const aText: string; aDest: TSpPrototypeContour);
var
 _point: TSpPrototypePoint;
 _border: TSpPrototypeBorder;
 _str: TStringReader;
 _StrDataOfPoint: string;
 _Matchs: TMatchCollection;
 _Match: TMatch;
begin
 //������ ������ ����� ����������� �� ��������.
 //�������������� ��� � ������ ������
 _str:= TStringReader.Create(aText);
 while _str.Peek>=0 do begin
  _StrDataOfPoint:= _str.ReadLine;
  //������ ������� �� ������������
  if not tregex.IsMatch(_StrDataOfPoint,'[^ ,;\t]') then begin
   if Assigned(_border) and (_border.Count<>0) then _border:= nil;
   CONTINUE
  end;
  _Matchs:= TRegEx.Matches(_StrDataOfPoint,rsDefaultRegExp);

  if (_Matchs.Count=0) then Continue;

  _point:= TSpPrototypePoint.Create;
  if _border=nil then begin
    _border:= TSpPrototypeBorder.Create;
    aDest.Add(_border);
  end;
  TRY
   _Match:= _Matchs.Item[0];
   if _Match.Groups.Item['pref'].Value<>'-' then
     _point.Prefix:= _Match.Groups.Item['pref'].Value;
   if _Match.Groups.Item['nump'].Value<>'-' then
   _point.Number:= _Match.Groups.Item['nump'].Value;
   if _Match.Groups.Item['oldx'].Value<>'-' then
   _point.OldX:= _Match.Groups.Item['oldx'].Value;
   if _Match.Groups.Item['oldy'].Value<>'-' then
   _point.OldY:= _Match.Groups.Item['oldy'].Value;
   if _Match.Groups.Item['newx'].Value<>'-' then
  _point.NewX:= _Match.Groups.Item['newx'].Value;
   if _Match.Groups.Item['newy'].Value<>'-' then
   _point.NewY:= _Match.Groups.Item['newy'].Value;
   if _Match.Groups.Item['dx'].Value<>'-' then
   _point.Delta:= _Match.Groups.Item['dx'].Value;
   if _Match.Groups.Item['zakrep'].Value<>'-' then
   _point.Zacrep:= _Match.Groups.Item['zakrep'].Value;
  FINALLY
   _border.Add(_point);
  END;

 end;
 _str.Free;
end;

{ TSpBuildText }

class function TSpBuildText.Build(aDest: TSpPrototypePoint): string;
var
 _stb: TStringBuilder;
begin
 _stb:= TStringBuilder.Create;
 _stb.Append(aDest.Prefix).Append(rsDefDelimiter);
 _stb.Append(aDest.Number).Append(rsDefDelimiter);
 _stb.Append(aDest.OldY).Append(rsDefDelimiter);
 _stb.Append(aDest.OldY).Append(rsDefDelimiter);
 _stb.Append(aDest.NewX).Append(rsDefDelimiter);
 _stb.Append(aDest.NewY).Append(rsDefDelimiter);
 _stb.Append(aDest.Delta).Append(rsDefDelimiter);
 _stb.Append(aDest.Zacrep);
 Result:= _stb.ToString;
 _stb.Free;
end;

class function TSpBuildText.Build(aDest: TSpPrototypeBorder): string;
var
 _stb: TStringBuilder;
 _p: TSpPrototypePoint;
begin
 _stb:= TStringBuilder.Create;
 for _p in aDest do begin
  _stb.Append(TSpBuildText.Build(_p));
  _stb.AppendLine;
 end;
 Result:= _stb.ToString;
 _stb.Free;
end;

class function TSpBuildText.Build(aDest: TSpPrototypeContour): string;
var
 _stb: TStringBuilder;
 _p: TSpPrototypeBorder;
begin
 _stb:= TStringBuilder.Create;
 for _p in aDest do begin
  _stb.AppendLine;
  _stb.Append(TSpBuildText.Build(_p));
 end;
 Result:= _stb.ToString;
 _stb.Free;
end;

end.
