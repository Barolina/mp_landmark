unit VBScript_RegExp_10_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// $Rev: 52393 $
// File generated on 05.11.2014 11:46:07 from Type Library described below.

// ************************************************************************  //
// Type Lib: C:\Windows\SysWOW64\vbscript.dll\2 (1)
// LIBID: {3F4DACA7-160D-11D2-A8E9-00104B365C9F}
// LCID: 0
// Helpfile: 
// HelpString: Microsoft VBScript Regular Expressions 1.0
// DepndLst: 
//   (1) v2.0 stdole, (C:\Windows\SysWOW64\stdole2.tlb)
// SYS_KIND: SYS_WIN32
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
{$ALIGN 4}

interface

uses Winapi.Windows, System.Classes, System.Variants, System.Win.StdVCL, Vcl.Graphics, Vcl.OleServer, Winapi.ActiveX;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  VBScript_RegExp_10MajorVersion = 1;
  VBScript_RegExp_10MinorVersion = 0;

  LIBID_VBScript_RegExp_10: TGUID = '{3F4DACA7-160D-11D2-A8E9-00104B365C9F}';

  IID_IRegExp: TGUID = '{3F4DACA0-160D-11D2-A8E9-00104B365C9F}';
  IID_IMatch: TGUID = '{3F4DACA1-160D-11D2-A8E9-00104B365C9F}';
  IID_IMatchCollection: TGUID = '{3F4DACA2-160D-11D2-A8E9-00104B365C9F}';
  CLASS_RegExp: TGUID = '{3F4DACA4-160D-11D2-A8E9-00104B365C9F}';
  CLASS_Match: TGUID = '{3F4DACA5-160D-11D2-A8E9-00104B365C9F}';
  CLASS_MatchCollection: TGUID = '{3F4DACA6-160D-11D2-A8E9-00104B365C9F}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IRegExp = interface;
  IRegExpDisp = dispinterface;
  IMatch = interface;
  IMatchDisp = dispinterface;
  IMatchCollection = interface;
  IMatchCollectionDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  RegExp = IRegExp;
  Match = IMatch;
  MatchCollection = IMatchCollection;


// *********************************************************************//
// Interface: IRegExp
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {3F4DACA0-160D-11D2-A8E9-00104B365C9F}
// *********************************************************************//
  IRegExp = interface(IDispatch)
    ['{3F4DACA0-160D-11D2-A8E9-00104B365C9F}']
    function Get_Pattern: WideString; safecall;
    procedure Set_Pattern(const pPattern: WideString); safecall;
    function Get_IgnoreCase: WordBool; safecall;
    procedure Set_IgnoreCase(pIgnoreCase: WordBool); safecall;
    function Get_Global: WordBool; safecall;
    procedure Set_Global(pGlobal: WordBool); safecall;
    function Execute(const sourceString: WideString): IDispatch; safecall;
    function Test(const sourceString: WideString): WordBool; safecall;
    function Replace(const sourceString: WideString; const replaceString: WideString): WideString; safecall;
    property Pattern: WideString read Get_Pattern write Set_Pattern;
    property IgnoreCase: WordBool read Get_IgnoreCase write Set_IgnoreCase;
    property Global: WordBool read Get_Global write Set_Global;
  end;

// *********************************************************************//
// DispIntf:  IRegExpDisp
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {3F4DACA0-160D-11D2-A8E9-00104B365C9F}
// *********************************************************************//
  IRegExpDisp = dispinterface
    ['{3F4DACA0-160D-11D2-A8E9-00104B365C9F}']
    property Pattern: WideString dispid 10001;
    property IgnoreCase: WordBool dispid 10002;
    property Global: WordBool dispid 10003;
    function Execute(const sourceString: WideString): IDispatch; dispid 10004;
    function Test(const sourceString: WideString): WordBool; dispid 10005;
    function Replace(const sourceString: WideString; const replaceString: WideString): WideString; dispid 10006;
  end;

// *********************************************************************//
// Interface: IMatch
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {3F4DACA1-160D-11D2-A8E9-00104B365C9F}
// *********************************************************************//
  IMatch = interface(IDispatch)
    ['{3F4DACA1-160D-11D2-A8E9-00104B365C9F}']
    function Get_Value: WideString; safecall;
    function Get_FirstIndex: Integer; safecall;
    function Get_Length: Integer; safecall;
    property Value: WideString read Get_Value;
    property FirstIndex: Integer read Get_FirstIndex;
    property Length: Integer read Get_Length;
  end;

// *********************************************************************//
// DispIntf:  IMatchDisp
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {3F4DACA1-160D-11D2-A8E9-00104B365C9F}
// *********************************************************************//
  IMatchDisp = dispinterface
    ['{3F4DACA1-160D-11D2-A8E9-00104B365C9F}']
    property Value: WideString readonly dispid 0;
    property FirstIndex: Integer readonly dispid 10001;
    property Length: Integer readonly dispid 10002;
  end;

// *********************************************************************//
// Interface: IMatchCollection
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {3F4DACA2-160D-11D2-A8E9-00104B365C9F}
// *********************************************************************//
  IMatchCollection = interface(IDispatch)
    ['{3F4DACA2-160D-11D2-A8E9-00104B365C9F}']
    function Get_Item(index: Integer): IDispatch; safecall;
    function Get_Count: Integer; safecall;
    function Get__NewEnum: IUnknown; safecall;
    property Item[index: Integer]: IDispatch read Get_Item;
    property Count: Integer read Get_Count;
    property _NewEnum: IUnknown read Get__NewEnum;
  end;

// *********************************************************************//
// DispIntf:  IMatchCollectionDisp
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {3F4DACA2-160D-11D2-A8E9-00104B365C9F}
// *********************************************************************//
  IMatchCollectionDisp = dispinterface
    ['{3F4DACA2-160D-11D2-A8E9-00104B365C9F}']
    property Item[index: Integer]: IDispatch readonly dispid 10001;
    property Count: Integer readonly dispid 1;
    property _NewEnum: IUnknown readonly dispid -4;
  end;

// *********************************************************************//
// The Class CoRegExp provides a Create and CreateRemote method to          
// create instances of the default interface IRegExp exposed by              
// the CoClass RegExp. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoRegExp = class
    class function Create: IRegExp;
    class function CreateRemote(const MachineName: string): IRegExp;
  end;

// *********************************************************************//
// The Class CoMatch provides a Create and CreateRemote method to          
// create instances of the default interface IMatch exposed by              
// the CoClass Match. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoMatch = class
    class function Create: IMatch;
    class function CreateRemote(const MachineName: string): IMatch;
  end;

// *********************************************************************//
// The Class CoMatchCollection provides a Create and CreateRemote method to          
// create instances of the default interface IMatchCollection exposed by              
// the CoClass MatchCollection. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoMatchCollection = class
    class function Create: IMatchCollection;
    class function CreateRemote(const MachineName: string): IMatchCollection;
  end;

implementation

uses System.Win.ComObj;

class function CoRegExp.Create: IRegExp;
begin
  Result := CreateComObject(CLASS_RegExp) as IRegExp;
end;

class function CoRegExp.CreateRemote(const MachineName: string): IRegExp;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_RegExp) as IRegExp;
end;

class function CoMatch.Create: IMatch;
begin
  Result := CreateComObject(CLASS_Match) as IMatch;
end;

class function CoMatch.CreateRemote(const MachineName: string): IMatch;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_Match) as IMatch;
end;

class function CoMatchCollection.Create: IMatchCollection;
begin
  Result := CreateComObject(CLASS_MatchCollection) as IMatchCollection;
end;

class function CoMatchCollection.CreateRemote(const MachineName: string): IMatchCollection;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_MatchCollection) as IMatchCollection;
end;

end.
