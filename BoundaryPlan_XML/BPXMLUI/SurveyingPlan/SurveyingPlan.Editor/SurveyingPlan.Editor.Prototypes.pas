unit SurveyingPlan.Editor.Prototypes;

interface
 uses
  System.Sysutils,
  System.Generics.Collections,
  SurveyingPlan.Xml.Types;

 type
  TSpPrototypeContour = class;
  TSpPrototypeBorder = class;

  TSpPrototypePoint = class
   public type
    TField = (fenPrefix,fenNumber,fenOldX,fenOldY,fenNewX,fenNewY,fenDelta,fenZacrep);
    TFields = set of TField;
   private
    FData: array[fenPrefix..fenZacrep] of string;
    FInvalidFields: TFields;
    FIsActualInvalidFields: boolean;
   strict protected
    function getField(aField: TField): string;
    procedure setField(aField: TField; aValue: string);
    procedure doValidation;
   protected
    //���������� ��� �����. ������������ �������� �������� ��������� ������ ���
    //������� isValid=TRUE
    function getKind: TSpBorderPoint.TKind;
   public
    constructor Create;
    property Data[aField: TField]: string read getField write setField; default;

    property Prefix: string index fenPrefix read getField write setField;
    property Number: string index fenNumber read getField write setField;
    property OldX: string index fenOldX read getField write setField;
    property OldY:string index fenOldY read getField write setField;
    property NewX:string index fenNewX read getField write setField;
    property NewY:string index fenNewY read getField write setField;
    property Delta:string index fenDelta read getField write setField;
    property Zacrep: string index fenZacrep read getField write setField;

    function getInvalid: TFields;
    function isValid: boolean;

    procedure ReadData(aSource: TSpBorderPoint);
    procedure WriteData(aDest: TSpBorderPoint);
  end;

  TSpPrototypeBorder = class(TObjectList<TSpPrototypePoint>)
  strict private
   FOwner: TSpPrototypeContour;
  protected
   procedure setOwner(aOwner: TSpPrototypeContour);
   function getIndex: integer;
  public
   property Owner: TSpPrototypeContour read FOwner;
   property IndexInOwner: integer read getIndex;

   procedure ReadData(aSource: TSpBorder);
   procedure WriteData(aDest: TSpBorder);
  end;

  TSpPrototypeContour = class(TObjectList<TSpPrototypeBorder>)
  protected
   procedure Notify(const Item: TSpPrototypeBorder; Action: TCollectionNotification); override;
  public
   procedure ReadData(aSource: TSpContour);
   procedure WriteData(aDest: TSpContour);
  end;


implementation

{ TSpFormatExistedNodal }

constructor TSpPrototypePoint.Create;
begin
 self.FInvalidFields:= [];
 self.FIsActualInvalidFields:= False;
end;

procedure TSpPrototypePoint.doValidation;
var
 _emptyOld,_emptyNew: boolean;
 _float: Double;
begin
 _emptyOld:= (self.Data[fenOldX]='') and (self.Data[fenOldY]='');
 _emptyNew:= (self.Data[fenNewX]='') and (self.Data[fenNewY]='');
 if _emptyOld and _emptyNew then
  self.FInvalidFields:= [fenOldX,fenOldY,fenNewX,fenNewY]
 else
  self.FInvalidFields:= [];

 //��������� �����
 if (not _emptyOld) and _emptyNew then begin
  if self.Data[fenPrefix]<>'' then Include(self.FInvalidFields,fenPrefix);
  if not TSpPoint.TNumber.isAcceptable(self.Data[fenNumber]) then Include(self.FInvalidFields,fenNumber);
  if not TryStrToFloat(self.Data[fenOldX],_float) then Include(self.FInvalidFields,fenOldX);
  if not TryStrToFloat(self.Data[fenOldY],_float) then Include(self.FInvalidFields,fenOldy);

  if self.Data[fenDelta]<>'' then Include(self.FInvalidFields,fenDelta);
  if self.Data[fenZacrep]<>'' then Include(self.FInvalidFields,fenZacrep);
 end;

 //����� �����
 if (not _emptyNew) and _emptyOld then begin
  if {(self.Data[fenPrefix]='') or ����� ����� �� ����� �������� ��������� �� ������������ ����� � ��������}
   (not TSpPoint.TPrefix.isAcceptable(self.Data[fenPrefix]))
     then Include(self.FInvalidFields,fenPrefix);
  if not TSpPoint.TNumber.isAcceptable(self.Data[fenNumber]) then Include(self.FInvalidFields,fenNumber);
  if not TryStrToFloat(self.Data[fenNewX],_float) then Include(self.FInvalidFields,fenNewX);
  if not TryStrToFloat(self.Data[fenNewY],_float) then Include(self.FInvalidFields,fenNewY);
  if not TSpPoint.TDelta.isAcceptable(self.Data[fenDelta]) then Include(self.FInvalidFields,fenDelta);
 end;

 //����������(������������) �����
 if (not _emptyNew) and (not _emptyOld) then begin
  if self.Data[fenPrefix]<>'' then Include(self.FInvalidFields,fenPrefix);
  if not TSpPoint.TNumber.isAcceptable(self.Data[fenNumber]) then Include(self.FInvalidFields,fenNumber);
  if not TryStrToFloat(self.Data[fenOldX],_float) then Include(self.FInvalidFields,fenOldX);
  if not TryStrToFloat(self.Data[fenOldY],_float) then Include(self.FInvalidFields,fenOldy);
  if not TryStrToFloat(self.Data[fenNewX],_float) then Include(self.FInvalidFields,fenNewX);
  if not TryStrToFloat(self.Data[fenNewY],_float) then Include(self.FInvalidFields,fenNewY);
  if not TSpPoint.TDelta.isAcceptable(self.Data[fenDelta]) then Include(self.FInvalidFields,fenDelta);
 end;
 self.FIsActualInvalidFields:= true;
end;

function TSpPrototypePoint.getField(aField: TField): string;
begin
 Result:= self.FData[aField];
end;

function TSpPrototypePoint.getInvalid: TFields;
begin
 if not self.FIsActualInvalidFields then
  self.doValidation;
 Result:= self.FInvalidFields
end;

function TSpPrototypePoint.getKind: TSpBorderPoint.TKind;
var
 _emptyOld,_emptyNew: boolean;
 _float: Double;
begin
 _emptyOld:= (self.Data[fenOldX]='') and (self.Data[fenOldY]='');
 _emptyNew:= (self.Data[fenNewX]='') and (self.Data[fenNewY]='');
 if (not _emptyOld) and _emptyNew then //��������� �����
   Result:= psbRemoved
 else
  if (not _emptyNew) and _emptyOld then //��������� �����
    Result:= psbCreated
  else
   //if not( _emptyNew and _emptyOld) then  //������������
    Result:= psbExisted;
end;

function TSpPrototypePoint.isValid: boolean;
begin
 Result:= self.getInvalid=[];
end;

procedure TSpPrototypePoint.ReadData(aSource: TSpBorderPoint);
begin
 case aSource.Kind of
  psbExisted: begin
   self.Prefix:= aSource.NewPoint.Prefix;
   self.Number:= aSource.NewPoint.Number;
   self.OldX:= FloatToStr(aSource.OldPoint.X);
   self.OldY:= FloatToStr(aSource.OldPoint.Y);
   self.NewX:= FloatToStr(aSource.NewPoint.X);
   self.NewY:= FloatToStr(aSource.NewPoint.Y);
   self.Delta:= aSource.NewPoint.Delta;
   self.Zacrep:= aSource.NewPoint.Zacrep;
  end;
  psbCreated:begin
   self.Prefix:= aSource.NewPoint.Prefix;
   self.Number:= aSource.NewPoint.Number;
   self.OldX:= '';
   self.OldY:= '';
   self.NewX:= FloatToStr(aSource.NewPoint.X);
   self.NewY:= FloatToStr(aSource.NewPoint.Y);
   self.Delta:= aSource.NewPoint.Delta;
   self.Zacrep:= aSource.NewPoint.Zacrep;
  end;
  psbRemoved:begin
   self.Prefix:= '';
   self.Number:= aSource.OldPoint.Number;
   self.OldX:= FloatToStr(aSource.OldPoint.X);
   self.OldY:= FloatToStr(aSource.OldPoint.Y);
   self.NewX:= '';
   self.NewY:= '';
   self.Delta:= '';
   self.Zacrep:= '';
  end;
 end;
end;

procedure TSpPrototypePoint.setField(aField: TField; aValue: string);
begin
 self.FData[aField]:= aValue;
 self.FIsActualInvalidFields:= False;
end;

procedure TSpPrototypePoint.WriteData(aDest: TSpBorderPoint);
begin
 if not self.isValid then raise Exception.Create('Invalid data point');
 aDest.Kind:= self.getKind;
 case aDest.Kind of
  psbExisted: begin
   aDest.NewPoint.Prefix:= self.Prefix;
   aDest.NewPoint.Number:= self.Number;
   aDest.OldPoint.X:=  StrToFloat(self.OldX);
   aDest.OldPoint.Y:=  StrToFloat(self.OldY);
   aDest.NewPoint.X:=  StrToFloat(self.NewX);
   aDest.NewPoint.Y:=  StrToFloat(self.NewY);
   aDest.NewPoint.Delta:= self.Delta;
   aDest.NewPoint.Zacrep:= self.Zacrep;
  end;
  psbCreated:begin
   aDest.NewPoint.Prefix:= self.Prefix;
   aDest.NewPoint.Number:= self.Number;
   aDest.NewPoint.X:=  StrToFloat(self.NewX);
   aDest.NewPoint.Y:=  StrToFloat(self.NewY);
   aDest.NewPoint.Delta:= self.Delta;
   aDest.NewPoint.Zacrep:= self.Zacrep;
  end;
  psbRemoved:begin
   aDest.OldPoint.Number:= self.Number;
   aDest.OldPoint.X:=  StrToFloat(self.OldX);
   aDest.OldPoint.Y:=  StrToFloat(self.OldY);
  end;
 end;
end;

{ TSpPrototypeBorder }

function TSpPrototypeBorder.getIndex: integer;
begin
 if self.Owner<>nil then
  Result:= self.FOwner.IndexOf(self)
 else
  Result:= -1;
end;

procedure TSpPrototypeBorder.ReadData(aSource: TSpBorder);
var
 _p: TSpBorderPoint;
 _pr: TSpPrototypePoint;
begin
 for _p in aSource do begin
  _pr:= TSpPrototypePoint.Create;
  _pr.ReadData(_p);
  self.Add(_pr);
 end;
end;

procedure TSpPrototypeBorder.setOwner(aOwner: TSpPrototypeContour);
begin
 self.FOwner:= aOwner;
end;

procedure TSpPrototypeBorder.WriteData(aDest: TSpBorder);
var
 _p: TSpBorderPoint;
 _pr: TSpPrototypePoint;
begin
 for _pr in self do begin
  _p:= TSpBorderPoint.Create;
  _pr.WriteData(_p);
  if _p.Kind = psbExisted then
   _p.OldPoint.Number:= _p.NewPoint.Number;

  aDest.Add(_p);
 end;
end;

{ TSpPrototypeMBorder }

procedure TSpPrototypeContour.Notify(const Item: TSpPrototypeBorder; Action: TCollectionNotification);
begin
 case Action of
   cnAdded: Item.setOwner(self) ;
   cnRemoved: if not self.OwnsObjects then Item.setOwner(nil);
   cnExtracted: Item.setOwner(nil);
 end;
end;

procedure TSpPrototypeContour.ReadData(aSource: TSpContour);
var
 _prBorder: TSpPrototypeBorder;
 _border: TSpBorder;
begin
 for _border in aSource do begin
  _prBorder:= TSpPrototypeBorder.Create;
  _prBorder.ReadData(_border);
  self.Add(_prBorder);
 end;
end;

procedure TSpPrototypeContour.WriteData(aDest: TSpContour);
var
 _prBorder: TSpPrototypeBorder;
 _border: TSpBorder;
begin
 for _prBorder in self do begin
  _Border:= TSpBorder.Create;
  _prBorder.WriteData(_border);
  aDest.Add(_Border);
 end;
end;

end.
