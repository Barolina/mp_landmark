unit frCadastralOrganization;

{
  TODO : ���� �����
}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls,

  STD_MP, BpXML_different, ExtCtrls;

type
  TFrameCadastralOrganization = class(TFrame)
    Name_Edit: TEdit;
    CodeORGN_Edit: TEdit;
    INN_Edit: TEdit;
    Telephone_Edit: TEdit;
    EMail_Edit: TEdit;
    Address_Edit: TEdit;

    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Bevel1: TBevel;
    Bevel2: TBevel;
    procedure Name_EditKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CodeORGN_EditKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure INN_EditKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Telephone_EditKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EMail_EditKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Address_EditKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Name_EditExit(Sender: TObject);
    procedure CodeORGN_EditExit(Sender: TObject);
    procedure INN_EditExit(Sender: TObject);
    procedure Telephone_EditExit(Sender: TObject);
    procedure EMail_EditExit(Sender: TObject);
    procedure Address_EditExit(Sender: TObject);
  private
   { Private declarations }
   FCadastralOrganization: IXMLTProviding_Pass_CadastralNumbers;
  protected
   procedure InitializationUI;
   procedure SetCadatstralOrganization(const CadOrg: IXMLTProviding_Pass_CadastralNumbers);
  public
    { Public declarations }

    function IsAssignedXmlNode: boolean;

   // procedure UIName_ReadOf(const CadOrg: IXMLNode);
    procedure UICodeORGN_ReadOf(const CadOrg: IXMLTProviding_Pass_CadastralNumbers);
    procedure UIINN_ReadOf(const CadOrg: IXMLTProviding_Pass_CadastralNumbers);
    procedure UITelephone_ReadOf(const CadOrg: IXMLTProviding_Pass_CadastralNumbers);
    procedure UIEMail_ReadOf(const CadOrg: IXMLTProviding_Pass_CadastralNumbers);
    procedure UIAddress_ReadOf(const CadOrg: IXMLTProviding_Pass_CadastralNumbers);
    procedure UI_ReadOf(const CadOrg: IXMLTProviding_Pass_CadastralNumbers);

    procedure UIName_WriteTo(const CadOrg: IXMLTProviding_Pass_CadastralNumbers);
    procedure UICodeORGN_WriteTo(const CadOrg: IXMLTProviding_Pass_CadastralNumbers);
    procedure UIINN_WriteTo(const CadOrg: IXMLTProviding_Pass_CadastralNumbers);
    procedure UITelephone_WriteTo(const CadOrg: IXMLTProviding_Pass_CadastralNumbers);
    procedure UIEMail_WriteTo(const CadOrg: IXMLTProviding_Pass_CadastralNumbers);
    procedure UIAddress_WriteTo(const CadOrg: IXMLTProviding_Pass_CadastralNumbers);
    procedure UI_WriteTo(const CadOrg: IXMLTProviding_Pass_CadastralNumbers);

   property XmlCadastralOrganization: IXMLTProviding_Pass_CadastralNumbers read FCadastralOrganization write SetCadatstralOrganization;
  end;

implementation

uses
 MsXmlApi;

resourcestring
 rsName = 'Name';
 rsCodeOGRN = 'Code_OGRN';
 rsTelephone = 'Telephone';
 rsEMail = 'E_mail';
 rsAddress = 'Address';
 rsINN = 'INN';

{$R *.dfm}

{ TFrameCadastralOrganization }

procedure TFrameCadastralOrganization.Address_EditExit(Sender: TObject);
begin
 if not self.IsAssignedXmlNode then raise Exception.Create(ExMSG_NotAssignedROOT);
 self.UIAddress_WriteTo(self.FCadastralOrganization);
end;

procedure TFrameCadastralOrganization.Address_EditKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
 if key = VK_RETURN then
  if (Sender is TCustomEdit) then
   TCustomEdit(Sender).Parent.SetFocus;
end;

procedure TFrameCadastralOrganization.CodeORGN_EditExit(Sender: TObject);
begin
 if not self.IsAssignedXmlNode then raise Exception.Create(ExMSG_NotAssignedROOT);
 self.UICodeORGN_WriteTo(self.FCadastralOrganization);
end;

procedure TFrameCadastralOrganization.CodeORGN_EditKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
 if key = VK_RETURN then
  if (Sender is TCustomEdit) then
   TCustomEdit(Sender).Parent.SetFocus;
end;

procedure TFrameCadastralOrganization.EMail_EditExit(Sender: TObject);
begin
 if not self.IsAssignedXmlNode then raise Exception.Create(ExMSG_NotAssignedROOT);
 self.UIEMail_WriteTo(self.FCadastralOrganization);

end;

procedure TFrameCadastralOrganization.EMail_EditKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
 if key = VK_RETURN then
  if (Sender is TCustomEdit) then
   TCustomEdit(Sender).Parent.SetFocus;
end;

procedure TFrameCadastralOrganization.InitializationUI;
begin
 if Self.IsAssignedXmlNode then
  self.UI_ReadOf(self.FCadastralOrganization)
 else begin
  self.Name_Edit.Text:= '';
  self.CodeORGN_Edit.Text:= '';
  self.INN_Edit.Text:= '';
  self.Telephone_Edit.Text:= '';
  self.EMail_Edit.Text:= '';
  self.Address_Edit.Text:= '';
 end;
end;

procedure TFrameCadastralOrganization.INN_EditExit(Sender: TObject);
begin
 if not self.IsAssignedXmlNode then raise Exception.Create(ExMSG_NotAssignedROOT);
 self.UIINN_WriteTo(self.FCadastralOrganization);
end;

procedure TFrameCadastralOrganization.INN_EditKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
 if key = VK_RETURN then
  if (Sender is TCustomEdit) then
   TCustomEdit(Sender).Parent.SetFocus;
end;

function TFrameCadastralOrganization.IsAssignedXmlNode: boolean;
begin
 result:= self.FCadastralOrganization<>nil;
end;

procedure TFrameCadastralOrganization.Name_EditExit(Sender: TObject);
begin
 if not self.IsAssignedXmlNode then raise Exception.Create(ExMSG_NotAssignedROOT);
 self.UIName_WriteTo(self.FCadastralOrganization);
end;

procedure TFrameCadastralOrganization.Name_EditKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
 if key = VK_RETURN then
  if (Sender is TCustomEdit) then
   TCustomEdit(Sender).Parent.SetFocus;
end;

procedure TFrameCadastralOrganization.SetCadatstralOrganization(const CadOrg: IXMLTProviding_Pass_CadastralNumbers);
begin
 if self.FCadastralOrganization = CadOrg then EXIT;
 self.FCadastralOrganization:= CadOrg;
 self.InitializationUI;
end;

procedure TFrameCadastralOrganization.Telephone_EditExit(Sender: TObject);
begin
 if not self.IsAssignedXmlNode then raise Exception.Create(ExMSG_NotAssignedROOT);
 self.UITelephone_WriteTo(self.FCadastralOrganization);

end;

procedure TFrameCadastralOrganization.Telephone_EditKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
 if key = VK_RETURN then
  if (Sender is TCustomEdit) then
   TCustomEdit(Sender).Parent.SetFocus;
end;

procedure TFrameCadastralOrganization.UIAddress_ReadOf(const CadOrg: IXMLTProviding_Pass_CadastralNumbers);
begin
 self.Address_Edit.Text:= MsXml_ReadChildNodeValue(CadOrg,rsAddress)
end;

procedure TFrameCadastralOrganization.UIAddress_WriteTo(const CadOrg: IXMLTProviding_Pass_CadastralNumbers);
var
 _st: string;
begin
 _st:= self.Address_Edit.Text;
 if _st<>'' then
//  CadOrg.Address:= _st
 else
  MsXml_RemoveChildNode(CadOrg,rsAddress);
end;

procedure TFrameCadastralOrganization.UICodeORGN_ReadOf(const CadOrg: IXMLTProviding_Pass_CadastralNumbers);
begin
 self.CodeORGN_Edit.Text:= MsXml_ReadChildNodeValue(CadOrg,rsCodeOGRN);
end;

procedure TFrameCadastralOrganization.UICodeORGN_WriteTo(const CadOrg: IXMLTProviding_Pass_CadastralNumbers);
var
 _st: string;
begin
 _st:= self.CodeORGN_Edit.Text;
 if _st<>'' then
//  CadOrg.Code_OGRN:= _st
 else
  MsXml_RemoveChildNode(CadOrg,rsCodeOGRN);
end;

procedure TFrameCadastralOrganization.UIEMail_ReadOf(const CadOrg: IXMLTProviding_Pass_CadastralNumbers);
begin
 self.EMail_Edit.Text:= MsXml_ReadChildNodeValue(CadOrg,rsEMail);
end;

procedure TFrameCadastralOrganization.UIEMail_WriteTo(const CadOrg: IXMLTProviding_Pass_CadastralNumbers);
var
 _st: string;
begin
 _st:= self.EMail_Edit.Text;
 if _st<>'' then
//  CadOrg.E_Mail:= _st
 else
  MsXml_RemoveChildNode(CadOrg,rsEMail);
end;

procedure TFrameCadastralOrganization.UIINN_ReadOf(const CadOrg: IXMLTProviding_Pass_CadastralNumbers);
begin
 self.INN_Edit.Text:= MsXml_ReadChildNodeValue(CadOrg,rsINN);
end;

procedure TFrameCadastralOrganization.UIINN_WriteTo(const CadOrg: IXMLTProviding_Pass_CadastralNumbers);
var
 _st: string;
begin
 _st:= self.INN_Edit.Text;
 if _st<>'' then
//  CadOrg.INN:= _st
 else
  MsXml_RemoveChildNode(CadOrg,rsINN);
end;

{procedure TFrameCadastralOrganization.UIName_ReadOf(const CadOrg: IXMLTProviding_Pass_CadastralNumbers);
begin
 self.Name_Edit.Text:= MsXml_ReadChildNodeValue(CadOrg,rsName);
end;}

procedure TFrameCadastralOrganization.UIName_WriteTo(const CadOrg: IXMLTProviding_Pass_CadastralNumbers);
var
 _st: string;
begin
 _st:= self.Name_Edit.Text;
 if _st<>'' then
//  CadOrg.Name:= _st
 else
  MsXml_RemoveChildNode(CadOrg,rsName);
end;

procedure TFrameCadastralOrganization.UITelephone_ReadOf(const CadOrg: IXMLTProviding_Pass_CadastralNumbers);
begin
 self.Telephone_Edit.Text:= MsXml_ReadChildNodeValue(CadOrg,rsTelephone);
end;

procedure TFrameCadastralOrganization.UITelephone_WriteTo(const CadOrg: IXMLTProviding_Pass_CadastralNumbers);
var
 _st: string;
begin
 _st:= self.Telephone_Edit.Text;
 if _st<>'' then
//  CadOrg.Telephone:= _st
 else
  MsXml_RemoveChildNode(CadOrg,rsTelephone);
end;

procedure TFrameCadastralOrganization.UI_ReadOf( const CadOrg: IXMLTProviding_Pass_CadastralNumbers);
begin
// self.UIName_ReadOf(CadOrg);
 self.UICodeORGN_ReadOf(CadOrg);
 self.UIINN_ReadOf(CadOrg);
 self.UITelephone_ReadOf(CadOrg);
 self.UIEMail_ReadOf(CadOrg);
 self.UIAddress_ReadOf(CadOrg);
end;

procedure TFrameCadastralOrganization.UI_WriteTo(const CadOrg: IXMLTProviding_Pass_CadastralNumbers);
begin
 self.UIName_WriteTo(CadOrg);
 self.UICodeORGN_WriteTo(CadOrg);
 self.UIINN_WriteTo(CadOrg);
 self.UITelephone_WriteTo(CadOrg);
 self.UIEMail_WriteTo(CadOrg);
 self.UIAddress_WriteTo(CadOrg);
end;

end.
