unit Config;

interface
uses forms, iniFiles, rtti, sysUtils, generics.Collections,
  Windows, Messages, Variants, Classes, Graphics, Controls,
  Dialogs, StdCtrls, ExtCtrls, XPMan,IOUtils;

const
    DEFAULT_CONFIG_SECTION = 'COMMON';
    NAME_CTL  = 'LANDMARK';
    cnstRS  = '.INI';

type
    /// <summary>
    ///     ������� ������ �������� ������ � INI-�����
    /// </summary>
    SectionAttribute = class(TCustomAttribute)
      strict private
        FSection : string;
      public
        constructor Create(SectionName : string);
        property Section : string read FSection;
    end;

    /// <summary>
    ///     �������� ���� �� ��������� (��� int, bool, string)
    /// </summary>
    DefaultValueAttribute = class(TCustomAttribute)
      strict private
        FValue : TValue;
      public
        constructor Create(aIntValue : integer); overload;
        constructor Create(aBoolValue : boolean); overload;
        constructor Create(aStringValue : string); overload;
        property Value : TValue read FValue;
    end;

    /// <summary>
    ///    ������� ����� ������� � ���������.
    ///    ������ �������� ��� ����������/��������� � INI *������* ����� ������.
    ///    � �������� ������� ��� ������� ���� ��������� ������� ��������� ���� ��
    ///    ������� ������/������:
    ///    - getIntegerValue/SetIntegerValue
    ///    - getBooleanValue/setBooleanValue
    ///    - getStringValue/SetStringValue
    ///    ����� ������ ��������� ������ ���� ������ ������� DefaulValue
    ///    �������� �� ��������� ���������� �������� ����� ����� ���������
    ///    %PATH% ��� ������ �� ����������� ���������, ������� "/" ���������
    /// </summary>
    TBaseOptions = class(TObject)
      strict protected
        FCtx : TRttiContext;
        FIni : TIniFile;
        FSection : string;
        function getDefaultAttribute(prop : TRttiProperty) : DefaultValueAttribute;

        function getGenericValue<T>(index : integer): T;
        procedure SetGenericValue<T>(index : integer; value : T);

        function getBooleanValue(index : integer):boolean; virtual;
        function getIntegerValue(index : integer):integer; virtual;
        function getStringValue(index : integer):string; virtual;

        procedure SetBooleanValue(index : integer; value : boolean); virtual;
        procedure SetIntegerValue(index : integer; value : integer); virtual;
        procedure SetStringValue(index : integer; value: string); virtual;

        function getProperty(index : integer) : TRttiProperty;
      public
        constructor Create(iniFile : TIniFile);
        destructor Destroy(); override;
    end;

    TBaseOptionsClass = class of TBaseOptions;


    [Section('Proxy')]
    TProxyOptions = class(TBaseOptions)
      public
        [DefaultValue('192.168.2.120')]
        property Proxy : string index 0 read getStringValue write SetStringValue;
        [DefaultValue(8080)]
        property Port : integer index 1 read getIntegerValue write SetIntegerValue;
        [DefaultValue('user')]
        property User : string index 2 read getStringValue write SetStringValue;        
        [DefaultValue('user')]
        property Password : string index 3 read getStringValue write SetStringValue;
        [DefaultValue(1)]{����������� ����� ������}
        property isProxy  : integer index 4 read getIntegerValue write SetIntegerValue;
    end;

    [ReopenFile('ReopenFiles')]
    TReopenFile = class(TBaseOptions)
       public
         property FilePath : string index 0 read getStringValue write SetStringValue;
    end;


    /// <summary>
    ///     ����� ��� ������ � �����������
    /// </summary>
    TConfig = class(TObject)
      strict private
       class var
        FIni : TIniFile;
        FAppPath : string;
        FOptions : TObjectList<TBaseOptions>;
      public
        class constructor Create();
        class destructor  Destroy();

        class procedure RegisterOptions(OptionsClass : TBaseOptionsClass);
        class function  Section<T:TBaseOptions>() : T;

        class property AppPath : string read FAppPath;
    end;

    EConfigException = Exception;

implementation
uses typinfo, strUtils,SHFolder;

{$REGION '--------------------- Create/Destroy ------------------------'}

class constructor TConfig.Create();
var filename : string;
    path : string;
    ///
    ///  ���������� ���� �� ��������� �����
    ///
    function GetSpecialFolderPath(folder: integer): string;
    const
          SHGFP_TYPE_CURRENT = 0;
    var
      path: array [0..MAX_PATH] of char;
    begin
      if SUCCEEDED(SHGetFolderPath(0,folder,0,SHGFP_TYPE_CURRENT,@path[0])) then
        Result := path
      else
        Result := '';
    end;
begin
 try
    path :=  GetSpecialFolderPath(CSIDL_PERSONAL)+'\'+NAME_CTL;
    if not DirectoryExists(path) then    ForceDirectories(path);    
    FileName := path+'\'+NAME_CTL+cnstRS;
    FIni := TIniFile.Create(FileName);    
    FAppPath := GetSpecialFolderPath(CSIDL_PERSONAL)+'\'+NAME_CTL+'\';

    FOptions := TObjectList<TBaseOptions>.Create();
 except on e : Exception    do
    ShowMessage(E.Message);        
 end;
end;

class destructor TConfig.Destroy;
begin
    FOptions.Free();
    FIni.Free();
end;


class procedure TConfig.RegisterOptions(OptionsClass: TBaseOptionsClass);
var opt : TBaseOptions;
begin
    opt := OptionsClass.Create(FIni);
    FOptions.Add(opt);
end;

class function TConfig.Section<T>(): T;
var opt : TBaseOptions;
    ti : TRttiType;
    ctx : TRttiContext;
begin
    ti := ctx.GetType(typeInfo(T));
    try
        for opt in FOptions do begin
            if opt is ti.AsInstance.MetaclassType then
                exit(T(opt));
        end;
    finally
        ctx.Free();
    end;

    raise EConfigException.Create('Unregistered option group' + PTypeInfo(typeinfo(t)).Name);
end;

{$ENDREGION}


{$REGION '--------------------- TBaseOptions --------------------------'}
constructor TBaseOptions.Create(iniFile: TIniFile);
var ctx : TRttiContext;
    attr : TCustomAttribute;
    t : TRttiType;
begin
    inherited Create();
    FIni := iniFile;

    FCtx  := TRttiContext.Create();
    t := ctx.GetType(self.ClassType);
    for attr in t.GetAttributes() do begin
        if attr is SectionAttribute then begin
            FSection := SectionAttribute(attr).Section;
        end;
    end;
end;

destructor TBaseOptions.Destroy();
begin
    FCtx.Free();
    inherited;
end;

/// <summary>
/// ���������� �������� DefaultValue ��� ��������
/// </summary>
function TBaseOptions.getDefaultAttribute(prop: TRttiProperty): DefaultValueAttribute;
var attr : TCustomAttribute;
begin
    result := nil;
    for attr in prop.GetAttributes() do begin
        if attr is DefaultValueAttribute then begin
            result := attr as DefaultValueAttribute;
            exit;
        end;
    end;
end;

{$REGION '--------------------- get-Methods ---------------------------}
/// <summary>
///     Generic ����� ��������� �������� ��������.
///  ���� �������� � INI �����������, �� ������������ �������� �������� DefaultValue
///  ���� ������� �� ������, �� Default(T)
/// </summary>
function TBaseOptions.getGenericValue<T>(index: integer): T;
var prop : TRttiProperty;
    default : DefaultValueAttribute;
    value : TValue;
begin
    prop := getProperty(index);
    if not assigned(prop) then
        raise EConfigException.Create('Undefined property name');

    default := getDefaultAttribute(prop);

    if FIni.ValueExists(FSection, Prop.Name) then begin

        case prop.PropertyType.TypeKind of
            tkInteger : value := FIni.ReadInteger(FSection, prop.name, Default.Value.AsInteger);
            tkString,
            tkUString : value := FIni.ReadString(FSection, prop.Name, Default.Value.asString);
            tkEnumeration : value := FIni.ReadInteger(FSection, prop.Name, ord(Default.Value.AsBoolean)) <> 0;
        end;

        result := value.AsType<T>;
    end
    else begin
        if assigned(default) then
             result := default.Value.AsType<T>
        else result := system.Default(T);
    end;
end;

function TBaseOptions.getBooleanValue(index: integer): boolean;
begin
    result := getGenericValue<boolean>(index);
end;

function TBaseOptions.getIntegerValue(index: integer): integer;
begin
    result := getGenericValue<integer>(index);
end;

function TBaseOptions.getStringValue(index: integer): string;
begin
    result := getGenericValue<string>(index);
    if ContainsText(result, '%PATH%') then begin
         result := ReplaceText(result, '%PATH%', TConfig.AppPath);
         result := ReplaceText(result, '\\', '\');
    end;
end;
{$ENDREGION}


{$REGION '--------------------- set Methods ---------------------------'}

/// <summary>
///     Generic-����� ������ �������� � INI ����.
/// </summary>
procedure TBaseOptions.SetGenericValue<T>(index: integer; value: T);
var prop : TRttiProperty;
    newValue : TValue;
begin

    prop := getProperty(index);
    if not assigned(prop) then
        raise EConfigException.Create('Undefined property name');

    newValue := TValue.From<T>(value);

    case PTypeInfo(TypeInfo(T)).Kind of
        tkInteger : FIni.WriteInteger(FSection, prop.Name, newValue.AsInteger);
        tkString,
        tkUString : Fini.WriteString(FSection, prop.Name, newValue.AsString);
        tkEnumeration : FIni.WriteBool(FSection, prop.Name, newValue.AsBoolean);
    end;
end;

procedure TBaseOptions.SetBooleanValue(index: integer; value: boolean);
begin
    setGenericValue<boolean>(index, value);
end;


procedure TBaseOptions.SetIntegerValue(index, value: integer);
begin
    setGenericValue<integer>(index, value);
end;

procedure TBaseOptions.SetStringValue(index: integer; value: string);
begin
    setGenericValue<string>(index, value);
end;
{$ENDREGION}

/// <summary>
///     �������� ������ �� ��� �������
/// </summary>
function TBaseOptions.getProperty(index: integer): TRttiProperty;
var t : TRttiType;
    props : TArray<TRttiProperty>;
begin
    result := nil;

    t := FCtx.GetType(self.ClassType);
    props := t.GetProperties();
    if index > Length(props) then
        raise EArgumentOutOfRangeException.Create(Format('TPrnsConfig. Property %d does not exists',[index]));

    result := props[index];
end;


{$ENDREGION}

{ TSectionAttribute }

constructor SectionAttribute.Create(SectionName: string);
begin
    inherited Create();
    FSection := SectionName;
end;

{ DefaultValueAttribute }

constructor DefaultValueAttribute.Create(aIntValue: integer);
begin
    inherited Create();
    FValue := aIntValue;
end;

constructor DefaultValueAttribute.Create(aBoolValue: boolean);
begin
    inherited Create();
    FValue := aBoolValue;
end;

constructor DefaultValueAttribute.Create(aStringValue: string);
begin
    inherited Create();
    FValue := aStringValue;
end;


initialization
    TConfig.RegisterOptions(TProxyOptions);
    TConfig.RegisterOptions(TReopenFile);

finalization

end.
