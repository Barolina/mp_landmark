unit fmLog;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, ButtonGroup, StdCtrls, ActnList, System.Actions, Vcl.ComCtrls, Vcl.ExtCtrls;


type
  TformLog = class(TForm)
    pnlBottom: TPanel;
    btnClose: TButton;
    RichEdit: TRichEdit;
    btng: TButtonGroup;
    actlst: TActionList;
    actSaveToFile: TAction;
    procedure actSaveToFileExecute(Sender: TObject);
    procedure btnCloseClick(Sender: TObject);
  private
    { Private declarations }
    procedure SetTextLog(const aValue: string);
    function GetTextLog: string;
  public
    { Public declarations }
    property TextLog: string read GetTextLog write SetTextLog;
  end;

var
  formLog: TformLog;

implementation


{$R *.dfm}

procedure TformLog.actSaveToFileExecute(Sender: TObject);
var
 _dlgSave: TSaveDialog;
begin
 _dlgSave:= TSaveDialog.Create(nil);
 _dlgSave.Filter:= '*.txt|*.txt';
 if _dlgSave.Execute then
  self.RichEdit.Lines.SaveToFile(ChangeFileExt(_dlgSave.FileName,'.txt'));
 _dlgSave.Free;
end;

procedure TformLog.btnCloseClick(Sender: TObject);
begin
 self.Close;
end;

function TformLog.GetTextLog: string;
begin
 Result:= self.RichEdit.Lines.Text;
end;

procedure TformLog.SetTextLog(const aValue: string);
begin
 self.RichEdit.Lines.Text:= aValue;
end;

end.
