unit frContoursInvariable;

{
   SubParcels - > Invariable- > Contours

   IXMLTExistParcel_SubParcels_InvariableSubParcel_Contours
   (�������� �� ������)
   ������� ������� �� ����� ��������
}
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ActnList, ButtonGroup, ExtCtrls,

  STD_MP, ImgList, System.Actions,frContour_Invariable,unRussLang,fmContainer,IdGlobal;

type
  TFrameContoursInvariable = class(TFrame)
    PanelContours_List: TPanel;
    Contours_List_ButtonGroup: TButtonGroup;
    ActionList: TActionList;
    Contours_List_ListBox: TListBox;
    Action_Contrours_AddNew: TAction;
    Action_Contours_DeleteCurrent: TAction;
    Action_Contours_SetCurrent: TAction;
    actImportContour: TAction;

    procedure Action_Contrours_AddNewExecute(Sender: TObject);
    procedure Action_Contours_DeleteCurrentExecute(Sender: TObject);
    procedure Action_Contours_SetCurrentExecute(Sender: TObject);
    procedure actImportContourExecute(Sender: TObject);
  private
   { Private declarations }
   FXmlContoursInvariable: IXMLTExistParcel_SubParcels_InvariableSubParcel_Contours;  //TODO : ??? ����������� ���  ���
   FContour_frame        : TFrameContourInvariable;


   procedure Contours_List_DoSetCurrentItem(IndexItem: integer); 
   procedure Contours_List_DoRefresh(A: Integer = -1; B: Integer = -1);
   procedure Contours_List_DoUpDate;
   procedure Contours_List_DoAddNewItem;
   procedure Contours_List_DoDeleteItem(IndexItem: Integer);


   procedure OnChange_Attribute_DefinitionOrNumberPP(sender: Tobject);
  protected
   procedure InitializationUI;
   procedure SetContoursInvariable(const aContours: IXMLTExistParcel_SubParcels_InvariableSubParcel_Contours);

   procedure Contours_List_SetCurrent(index: integer);
  public
   { Public declarations }
   constructor Create(Owner: TComponent); override;

   function IsAssignedContours: Boolean;
   function IsAssignedContoursInvariable: Boolean;

   property Contour: TFrameContourInvariable read FContour_frame;
   property XmlContours: IXMLTExistParcel_SubParcels_InvariableSubParcel_Contours read FXmlContoursInvariable write SetContoursInvariable;
  end;

implementation

uses MsXMLAPI,Clipbrd,RegularExpressions;

{$R *.dfm}
 resourcestring
      rgxListContours = '(\d*)[;\t]?(\d*)[;\t]?(\d*)';

{ TFrameContours }

///
///  ������ ������ �������� � �������
///  ����� �������;�������� �������;�����������(�� �����������);
///
procedure TFrameContoursInvariable.actImportContourExecute(Sender: TObject);
var
  ClipbrdData: TStringList;
  i, j, RowCnt: Integer;
  key : string;
  Matchs: TMatchCollection;
  Match: TMatch;
  RegX : TRegEx;
  _xmlNode  : IXMLTExistParcel_SubParcels_InvariableSubParcel_Contours_Contour;
begin
 try
  if MessageDlg('������ ������, ������ � ������ ��� ��������� ������, ������� ����������?',mtConfirmation,mbYesNo,-1) = mrNo then Exit;
  self.XmlContours.Clear;
  Self.InitializationUI;
  if not Clipboard.HasFormat(CF_TEXT) then Exit;
  ClipbrdData := TStringList.Create;
  RegX := TRegEx.Create(rgxListContours,[roIgnoreCase,roMultiline]);
  try
    Clipboard.Open;
    ClipbrdData.Text := Clipboard.AsText;
    RowCnt := ClipbrdData.Count;
    for i := 0 to RowCnt - 1 do
    begin
      Matchs := Regx.Matches(ClipbrdData.Strings[I]);
      if  Matchs.Count = 0 then  Continue;
      for j := 0 to Matchs.Count -1 do
      begin
          Match := Matchs.Item[j];

          _xmlNode := self.XmlContours.Add;
          _xmlNode.Number := Match.Groups.Item[1].Value;
          _xmlNode.Area.Area := Match.Groups.Item[2].Value;
          if (Match.Groups.Count = 4) and (not Match.Groups.Item[3].Value.IsEmpty) then
          _xmlNode.Area.ChildValues['Innccuracy'] := StrToInt(Match.Groups.Item[3].Value);
      end;
    end;
    self.InitializationUI;
    ShowMessage('������ ��������! ���������� ������������� ��������+ '+ inttostr(self.Contours_List_ListBox.Count));
  finally
    Clipboard.Close;
    FreeAndNil(ClipbrdData);
  end;
 Except raise Exception.Create('������ � ������� ������!');
 end;
end;

procedure TFrameContoursInvariable.Action_Contours_DeleteCurrentExecute(Sender: TObject);
begin
 if not IsAssignedContours then EXIT;
 if self.Contours_List_ListBox.ItemIndex=-1 then EXIT;
 self.Contours_List_DoDeleteItem(self.Contours_List_ListBox.ItemIndex);
 self.Contours_List_SetCurrent(self.Contours_List_ListBox.ItemIndex);
 if not self.FContour_frame.IsAssignedContourIvariable then
  self.Contours_List_SetCurrent(0);
end;

procedure TFrameContoursInvariable.Action_Contours_SetCurrentExecute(Sender: TObject);
begin
 self.Contours_List_SetCurrent(self.Contours_List_ListBox.ItemIndex);
end;

procedure TFrameContoursInvariable.Action_Contrours_AddNewExecute(Sender: TObject);
begin
 if not self.IsAssignedContours then EXIT;
 self.Contours_List_DoAddNewItem;
 self.Contours_List_SetCurrent(self.Contours_List_ListBox.Count-1);
end;

procedure TFrameContoursInvariable.Contours_List_DoRefresh(A, B: Integer);
var
 _Contour: IXMLTExistParcel_SubParcels_InvariableSubParcel_Contours_Contour;
 _i,_j: Integer;
 _ItemName: string;
begin
 if self.Contours_List_ListBox.Items.Count = 0 then EXIT;

 if (A<0) or (a>=self.Contours_List_ListBox.Items.Count)  then
  a:= 0;

 if (B<A) or (B>=self.Contours_List_ListBox.Items.Count)  then
  B:= self.Contours_List_ListBox.Items.Count-1;

 _j:= self.Contours_List_ListBox.ItemIndex;
     for _i := A to B do begin
     _Contour:= IXMLTExistParcel_SubParcels_InvariableSubParcel_Contours_Contour(pointer(self.Contours_List_ListBox.Items.Objects[_i]));
     _ItemName:= MsXml_ReadAttribute(_Contour,cnstAtrNumber);
      self.Contours_List_ListBox.Items.Strings[_i]:= _ItemName;
     end;

 self.Contours_List_ListBox.ItemIndex:= _j;
end;



procedure TFrameContoursInvariable.Contours_List_DoSetCurrentItem(IndexItem: integer);
var
 _contour: IXMLTExistParcel_SubParcels_InvariableSubParcel_Contours_Contour;
begin
 _contour:= IXMLTExistParcel_SubParcels_InvariableSubParcel_Contours_Contour(pointer(self.Contours_List_ListBox.Items.Objects[IndexItem]));
 self.FContour_frame.XmlContourInvariable:= _contour;
end;


procedure TFrameContoursInvariable.Contours_List_SetCurrent(index: integer);
begin
 if (index>-1) and (index<self.Contours_List_ListBox.Count) then begin
  self.Contours_List_DoSetCurrentItem(index);
  self.Contours_List_ListBox.ItemIndex:= index;
  self.FContour_frame.Visible:= True;
 end else begin
  self.FContour_frame.Visible:= false;
  self.Contours_List_ListBox.ItemIndex:= -1;
  self.FContour_frame.XmlContourInvariable:= nil;
 end;
end;


constructor TFrameContoursInvariable.Create(Owner: TComponent);
begin
 inherited;
 self.FContour_frame:= TFrameContourInvariable.Create(self);
 self.FContour_frame.OnChange_Attribute_DefOnNum:= self.OnChange_Attribute_DefinitionOrNumberPP;
 self.FContour_frame.Parent:= self;
 self.FContour_frame.Align:= alClient;
 self.FContour_frame.Visible:= False;

end;

procedure TFrameContoursInvariable.InitializationUI;
begin
 self.Contours_List_ListBox.Clear;
 if self.IsAssignedContours then begin
  self.Contours_List_DoUpDate;
  self.Contours_List_SetCurrent(0);
 end;
end;


function TFrameContoursInvariable.IsAssignedContours: Boolean;
begin
 Result:= self.FXmlContoursInvariable<>nil;
end;

function TFrameContoursInvariable.IsAssignedContoursInvariable: Boolean;
begin
 result := self.FXmlContoursInvariable <> nil;
end;

procedure TFrameContoursInvariable.OnChange_Attribute_DefinitionOrNumberPP(sender: tobject);
begin
 self.Contours_List_DoRefresh(self.Contours_List_ListBox.ItemIndex,self.Contours_List_ListBox.ItemIndex);
end;


procedure TFrameContoursInvariable.SetContoursInvariable(
  const aContours: IXMLTExistParcel_SubParcels_InvariableSubParcel_Contours);
begin
 if self.FXmlContoursInvariable=aContours then EXIT;
 begin
     self.FXmlContoursInvariable := aContours;
     self.Contours_List_SetCurrent(-1);
     self.InitializationUI;
 end;
end;

procedure TFrameContoursInvariable.Contours_List_DoAddNewItem;
var
 _contour: IXMLTExistParcel_SubParcels_InvariableSubParcel_Contours_Contour;
 _ItemName: string;
begin
 _contour:= self.FXmlContoursInvariable.Add;
 _ItemName:= IntToStr(self.FXmlContoursInvariable.Count);
 _contour.Number:= _ItemName;
 self.Contours_List_ListBox.AddItem(_ItemName,pointer(_contour));
end;

procedure TFrameContoursInvariable.Contours_List_DoDeleteItem(IndexItem: Integer);
var
 _contour: IXMLTExistParcel_SubParcels_InvariableSubParcel_Contours_Contour;
 _index: Integer;
begin
 _contour:= IXMLTExistParcel_SubParcels_InvariableSubParcel_Contours_Contour(pointer(self.Contours_List_ListBox.Items.Objects[IndexItem]));
 if self.FContour_frame.XmlContourInvariable=_contour then
  self.Contours_List_SetCurrent(IndexItem-1);
 self.Contours_List_ListBox.Items.Delete(IndexItem);
 self.FXmlContoursInvariable.Remove(_contour);
 _contour:= nil;
end;

procedure TFrameContoursInvariable.Contours_List_DoUpDate;
var
 _i, _index: Integer;
 _Contour: IXMLTExistParcel_SubParcels_InvariableSubParcel_Contours_Contour;
 _ItemName: string;
begin
 _index:= self.Contours_List_ListBox.ItemIndex;
  self.Contours_List_ListBox.Clear;
     for _i := 0 to self.FXmlContoursInvariable.Count-1 do begin
      _Contour:= self.FXmlContoursInvariable.Contour[_i];
     _ItemName:= MsXml_ReadAttribute(_Contour,cnstAtrNumber);
      _Contour.Number := _ItemName;
      self.Contours_List_ListBox.AddItem(_ItemName,pointer(_Contour));
     end;
 self.Contours_List_SetCurrent(_index);
 if not self.FContour_frame.IsAssignedContourIvariable then
  self.Contours_List_SetCurrent(0);
end;



end.

