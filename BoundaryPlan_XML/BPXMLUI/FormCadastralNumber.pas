unit FormCadastralNumber;


interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls, Mask;

type
  TFormsCadastralNumber = class(TForm)
    OKBtn: TButton;
    CancelBtn: TButton;
    medtInput: TMaskEdit;
    Panel1: TPanel;
    procedure FormShow(Sender: TObject);

  private
    { Private declarations }
    class var _Instance: TFormsCadastralNumber;
    function getInputValue: string;
    procedure setInputValue(const aValue:string);
  public
    { Public declarations }
    class function Instance: TFormsCadastralNumber; static;
    property InputValue: string read getInputValue write setInputValue;
  end;



implementation


{$R *.dfm}

//-->>--------------------------------------------------------------------------
procedure TFormsCadastralNumber.FormShow(Sender: TObject);
begin
  self.medtInput.SetFocus;
end;


{ SingletonFormCadNum }


function TFormsCadastralNumber.getInputValue: string;
begin
 Result:= TrimRight(TrimLeft(self.medtInput.Text));
end;

class function TFormsCadastralNumber.Instance: TFormsCadastralNumber;
begin
 if not Assigned(_Instance) then
   Application.CreateForm(TFormsCadastralNumber,_Instance);
 Result:= _Instance;
 
end;

procedure TFormsCadastralNumber.setInputValue(const aValue: string);
begin
 self.medtInput.Text:= aValue;
end;

end.
