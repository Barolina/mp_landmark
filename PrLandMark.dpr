        // JCL_DEBUG_EXPERT_DELETEMAPFILE OFF
// JCL_DEBUG_EXPERT_INSERTJDBG ON
// JCL_DEBUG_EXPERT_GENERATEJDBG ON
program PrLandMark;
(*
{$WEAKLINKRTTI ON}
{$RTTI EXPLICIT METHODS([]) FIELDS([]) PROPERTIES([])}
*)
uses
  IOUtils,
  Forms,
  UnLandMark in 'UnLandMark.pas' {FrLandMark},
  BpXML_different in 'BoundaryPlan_XML\BpXML_different.pas',
  edGrid in 'plugin\edGrid.pas',
  PageControlAnimate in 'plugin\PageControlAnimate.pas',
  MSMath in 'MapSurface\Math\MSMath.pas',
  MSMath_ in 'MapSurface\Math\MSMath_.pas',
  MSMCommon in 'MapSurface\Math\MSMCommon.pas',
  MSMCommon_ in 'MapSurface\Math\MSMCommon_.pas',
  MsXmlApi in 'MapSurface\Xml\MsXmlApi.pas',
  MSXML2_TLB in 'MapSurface\Xml\MSXML2_TLB.pas',
  STD_MP in 'BoundaryPlan_XML\STD_MP.pas',
  fmNewSubParcels in 'BoundaryPlan_XML\fmNewSubParcels.pas' {FormNewSubParcels},
  UnCheckTablesOrdinate in 'BoundaryPlan_XML\BPXMLUI\Template\UnCheckTablesOrdinate.pas' {FrCheckTablesOrdinate},
  fmBPXML_GeneralInformation in 'BoundaryPlan_XML\BPXMLUI\fmBPXML_GeneralInformation.pas' {FormBPXML_GeneralInformation},
  fmBpXml_WizardGroupValues in 'BoundaryPlan_XML\BPXMLUI\fmBpXml_WizardGroupValues.pas' {FormBpXml_WizardGroupValues},
  fmEntitySpatial_Attributs in 'BoundaryPlan_XML\BPXMLUI\fmEntitySpatial_Attributs.pas' {FormEntitySpatial_Attributs},
  fmEntitySpatial_EditorNeighbours in 'BoundaryPlan_XML\BPXMLUI\fmEntitySpatial_EditorNeighbours.pas' {formEntitySpatial_EditorNeighbours},
  frAddress in 'BoundaryPlan_XML\BPXMLUI\frAddress.pas' {FrameAddress: TFrame},
  frChangeBorder in 'BoundaryPlan_XML\BPXMLUI\frChangeBorder.pas' {FrameBorder: TFrame},
  frExistEZParcels in 'BoundaryPlan_XML\BPXMLUI\frExistEZParcels.pas' {FrameExistEZParcels: TFrame},
  frPerson in 'BoundaryPlan_XML\BPXMLUI\frPerson.pas' {FramePerson: TFrame},
  frNaturalObject in 'BoundaryPlan_XML\BPXMLUI\frNaturalObject.pas' {n: TFrame},
  frSpecifyRelatedsParcel in 'BoundaryPlan_XML\BPXMLUI\frSpecifyRelatedsParcel.pas' {FrameSpecifyParcelrelated: TFrame},
  frCategory in 'BoundaryPlan_XML\BPXMLUI\frCategory.pas' {FrameCategory: TFrame},
  frAllBorders in 'BoundaryPlan_XML\BPXMLUI\frAllBorders.pas' {FrameAllBorders: TFrame},
  frEncumbrance in 'BoundaryPlan_XML\BPXMLUI\frEncumbrance.pas' {FrameEncumbrance: TFrame},
  frProvCadastralNumbers in 'BoundaryPlan_XML\BPXMLUI\frProvCadastralNumbers.pas' {FrameProvCadastralNumber: TFrame},
  frAreaDelta in 'BoundaryPlan_XML\BPXMLUI\frAreaDelta.pas' {FrameAreaDelta: TFrame},
  frRelatedParcels in 'BoundaryPlan_XML\BPXMLUI\frRelatedParcels.pas' {FrameRelatedParcels: TFrame},
  frOwnerNeighbour in 'BoundaryPlan_XML\BPXMLUI\frOwnerNeighbour.pas' {FrameOwnerNeighbour: TFrame},
  frAreaInnvvuracy in 'BoundaryPlan_XML\BPXMLUI\frAreaInnvvuracy.pas' {FrameAreaInnccuracy: TFrame},
  frChangeParcel in 'BoundaryPlan_XML\BPXMLUI\frChangeParcel.pas' {FrameChangeParcel: TFrame},
  frExistSubParcel in 'BoundaryPlan_XML\BPXMLUI\frExistSubParcel.pas' {FrameExistSubParcel: TFrame},
  frExistSubParcels in 'BoundaryPlan_XML\BPXMLUI\frExistSubParcels.pas' {FrameExistSubParcels: TFrame},
  FormCadastralNumber in 'BoundaryPlan_XML\BPXMLUI\FormCadastralNumber.pas' {FormsCadastralNumber},
  frArea in 'BoundaryPlan_XML\BPXMLUI\frArea.pas' {FrameArea: TFrame},
  frFIO in 'BoundaryPlan_XML\BPXMLUI\frFIO.pas' {FrameFIO: TFrame},
  frAgent in 'BoundaryPlan_XML\BPXMLUI\frAgent.pas' {FrameAgent: TFrame},
  frForeign_Organisation in 'BoundaryPlan_XML\BPXMLUI\frForeign_Organisation.pas' {FrameForeign_Organisation: TFrame},
  frContractor in 'BoundaryPlan_XML\BPXMLUI\frContractor.pas' {FrameContractor: TFrame},
  frGovernance in 'BoundaryPlan_XML\BPXMLUI\frGovernance.pas' {FrameGovernance: TFrame},
  frClients in 'BoundaryPlan_XML\BPXMLUI\frClients.pas' {FrameClients: TFrame},
  frClient in 'BoundaryPlan_XML\BPXMLUI\frClient.pas' {FrameClient: TFrame},
  frDataDocuments in 'BoundaryPlan_XML\BPXMLUI\frDataDocuments.pas' {FrameDataDocuments: TFrame},
  Vcl.Themes,
  Vcl.Styles,
  windows,
  frAttributesSTD_MP in 'BoundaryPlan_XML\BPXMLUI\frAttributesSTD_MP.pas' {FrameAttributedSTD_MP: TFrame},
  frNodalPointSchemes in 'BoundaryPlan_XML\BPXMLUI\frNodalPointSchemes.pas' {FrameNodalPointSchemes: TFrame},
  frDocument in 'BoundaryPlan_XML\BPXMLUI\frDocument.pas' {FrameDocument: TFrame},
  fmTextInf in 'BoundaryPlan_XML\BPXMLUI\Template\fmTextInf.pas' {FormTextInf},
  frPrevCadastralNumbers in 'BoundaryPlan_XML\BPXMLUI\frPrevCadastralNumbers.pas' {FramePrevSCadastralNumbers: TFrame},
  frSubParcel in 'BoundaryPlan_XML\BPXMLUI\frSubParcel.pas' {FrameSubParcel: TFrame},
  frOrganisation in 'BoundaryPlan_XML\BPXMLUI\frOrganisation.pas' {FrameOrganisation: TFrame},
  frContoursInvariable in 'BoundaryPlan_XML\BPXMLUI\frContoursInvariable.pas' {FrameContoursInvariable: TFrame},
  frContour_Invariable in 'BoundaryPlan_XML\BPXMLUI\frContour_Invariable.pas' {FrameContourInvariable: TFrame},
  frOwnerNeighbours in 'BoundaryPlan_XML\BPXMLUI\frOwnerNeighbours.pas' {FrameOwnerNeighbours: TFrame},
  frParcelNeighbour in 'BoundaryPlan_XML\BPXMLUI\frParcelNeighbour.pas' {FrameParcelNeighbour: TFrame},
  fmSpVCLEditor in 'BoundaryPlan_XML\BPXMLUI\SurveyingPlan\SurveyingPlan.VCLEditor\fmSpVCLEditor.pas' {FormEntitySpatial},
  SurveyingPlan.Editor.Prototypes in 'BoundaryPlan_XML\BPXMLUI\SurveyingPlan\SurveyingPlan.Editor\SurveyingPlan.Editor.Prototypes.pas',
  SurveyingPlan.Editor.Utils in 'BoundaryPlan_XML\BPXMLUI\SurveyingPlan\SurveyingPlan.Editor\SurveyingPlan.Editor.Utils.pas',
  SurveyingPlan.Xml.Types in 'BoundaryPlan_XML\BPXMLUI\SurveyingPlan\SurveyingPlan.Xml\SurveyingPlan.Xml.Types.pas',
  SurveyingPlan.Xml.Utils in 'BoundaryPlan_XML\BPXMLUI\SurveyingPlan\SurveyingPlan.Xml\SurveyingPlan.Xml.Utils.pas',
  frContours in 'BoundaryPlan_XML\BPXMLUI\frContours.pas' {FrameContours: TFrame},
  frContour in 'BoundaryPlan_XML\BPXMLUI\frContour.pas' {FrameContour: TFrame},
  MSStringParser in 'MapSurface\MSStringParser.pas',
  MSMType_ in 'MapSurface\Math\MSMType_.pas',
  BPCommon in 'plugin\BPCommon.pas',
  MSType in 'MapSurface\MSType.pas',
  frEntitySpatial in 'BoundaryPlan_XML\BPXMLUI\frEntitySpatial.pas' {FrameEntity_Spatial: TFrame},
  Fm_SpecifyParcels in 'BoundaryPlan_XML\Fm_SpecifyParcels.pas' {FormSpecifyParcels},
  fmSelectionList in 'BoundaryPlan_XML\BPXMLUI\Template\fmSelectionList.pas' {frmSelectionList},
  unRussLang in 'Language\ru\unRussLang.pas',
  AllDocuments in 'BoundaryPlan_XML\BPXMLUI\allDocuments\AllDocuments.pas',
  fmAllDocumentsMain in 'BoundaryPlan_XML\BPXMLUI\allDocuments\fmAllDocumentsMain.pas' {FormAllDocumentsMain},
  Fm_ExistParcel in 'BoundaryPlan_XML\BPXMLUI\Fm_ExistParcel.pas' {frmExistParcel},
  frExistEZEntryParcel in 'BoundaryPlan_XML\BPXMLUI\frExistEZEntryParcel.pas' {FrameExistEZEntryParcel: TFrame},
  SurveyingPlan.DataFormat in 'BoundaryPlan_XML\SurveyingPlan.DataFormat.pas',
  SurveyingPlan.Common in 'BoundaryPlan_XML\SurveyingPlan.Common.pas',
  FrTypeParcel in 'BoundaryPlan_XML\BPXMLUI\Template\FrTypeParcel.pas' {frmTypeParcel},
  fmLog in 'BoundaryPlan_XML\fmLog.pas' {formLog},
  fmContainer in 'BoundaryPlan_XML\fmContainer.pas' {formContainer},
  frDeleteAllBorder in 'BoundaryPlan_XML\BPXMLUI\frDeleteAllBorder.pas' {FrameDeleteAllBorder: TFrame},
  frSpecifyRelatedsParcels in 'BoundaryPlan_XML\BPXMLUI\frSpecifyRelatedsParcels.pas' {FrameSpecifyRelatedsParcels: TFrame},
  FmListApplidFiles in 'BoundaryPlan_XML\BPXMLUI\Template\FmListApplidFiles.pas' {FrmListAppliedFiles},
  frDeleteEntryParcels in 'BoundaryPlan_XML\BPXMLUI\frDeleteEntryParcels.pas' {FrameDeleteEntryPArcels: TFrame},
  BoundaryPlanXML in 'BoundaryPlan_XML\BoundaryPlanXML.pas',
  frInputDatesubParcels in 'BoundaryPlan_XML\BPXMLUI\frInputDatesubParcels.pas' {FrameInputDateSubParcels: TFrame},
  frGeodesisBases in 'BoundaryPlan_XML\BPXMLUI\frGeodesisBases.pas' {FrameGeodesisBases: TFrame},
  frRealty in 'BoundaryPlan_XML\BPXMLUI\frRealty.pas' {FrameRealty: TFrame},
  frMeans_Surveys in 'BoundaryPlan_XML\BPXMLUI\frMeans_Surveys.pas' {FrameMeans_Surveys: TFrame},
  FWZipWriter in 'MapSurface\ZIP\FWZipWriter.pas',
  FWZipConsts in 'MapSurface\ZIP\FWZipConsts.pas',
  FWZipCrc32 in 'MapSurface\ZIP\FWZipCrc32.pas',
  FWZipCrypt in 'MapSurface\ZIP\FWZipCrypt.pas',
  FWZipReader in 'MapSurface\ZIP\FWZipReader.pas',
  FWZipStream in 'MapSurface\ZIP\FWZipStream.pas',
  LMProject in 'plugin\LMProject\LMProject.pas',
  frSubParContours in 'BoundaryPlan_XML\BPXMLUI\frSubParContours.pas' {FrameSubParContours: TFrame},
  frSubParContour in 'BoundaryPlan_XML\BPXMLUI\frSubParContour.pas' {FrameSubParContour: TFrame},
  frSubParcels in 'BoundaryPlan_XML\BPXMLUI\frSubParcels.pas' {FrameSubParcels: TFrame},
  UnLandMarkLOGO in 'UnLandMarkLOGO.pas' {FrLOGO},
  FrPreloader in 'BoundaryPlan_XML\BPXMLUI\Template\FrPreloader.pas' {FrLoader},
  frNewSubParcel in 'BoundaryPlan_XML\BPXMLUI\frNewSubParcel.pas' {FrameNewSubParcel: TFrame},
  frNewParcel in 'BoundaryPlan_XML\BPXMLUI\frNewParcel.pas' {FrameNewParcel: TFrame},
  fmBPXML_FormParcels in 'BoundaryPlan_XML\fmBPXML_FormParcels.pas' {FormBPXML_FormParcels},
  SplashForm in 'plugin\SplashForm.pas',
  UnShowText in 'BoundaryPlan_XML\BPXMLUI\Template\UnShowText.pas' {FrShowText},
  Vcl.RibbonConsts in 'plugin\Vcl.RibbonConsts.pas',
  GlbCfg in 'plugin\Support\GlbCfg.pas',
  Issues.API.Intf in 'plugin\Support\Issues.API.Intf.pas',
  ProxyCfg in 'plugin\Support\ProxyCfg.pas',
  YouTrack.Intf in 'plugin\Support\YouTrack.Intf.pas',
  Config in 'plugin\LMProject\Config.pas',
  UnInternet in 'plugin\Interner\UnInternet.pas' {FrInternet},
  GridsEX in 'plugin\GridsEX.pas',
  fmComposition_EZ in 'BoundaryPlan_XML\BPXMLUI\fmComposition_EZ.pas' {FrameCompostion_EZ: TFrame},
  UnParcelNeighbour in 'BoundaryPlan_XML\BPXMLUI\UnParcelNeighbour.pas' {FrmParcelNeighbour},
  frUtilizationList in 'BoundaryPlan_XML\BPXMLUI\frUtilizationList.pas' {FrameUtilizationList: TFrame},
  frInnerCadastralNumbers in 'BoundaryPlan_XML\BPXMLUI\frInnerCadastralNumbers.pas' {FrameInnerCadastralNumbers: TFrame},
  FWZipZLib in 'MapSurface\ZIP\FWZipZLib.pas';

(*
{$WEAKLINKRTTI ON}
 {$RTTI EXPLICIT METHODS([]) PROPERTIES([]) FIELDS([])}
{$SETPEFlAGS IMAGE_FILE_RELOCS_STRIPPED or IMAGE_FILE_DEBUG_STRIPPED or
IMAGE_FILE_LINE_NUMS_STRIPPED or IMAGE_FILE_LOCAL_SYMS_STRIPPED or
IMAGE_FILE_REMOVABLE_RUN_FROM_SWAP or IMAGE_FILE_NET_RUN_FROM_SWAP}
*)

 {$R *.res}


begin

  ///
  ///  ��������  ������ ������
  ///
  { ReportMemoryLeaksOnShutdown := True;}
  STD_MP_WorkDir:= TPath.GetDirectoryName(Application.ExeName) + '\Data\BoundaryPlanXML\V04_STD_�P\';
  STD_MP_TEMPDIR:= TPath.GetDocumentsPath+ '\LANDMARK';
  STD_DirXSLTDOC:= TPath.GetDirectoryName(Application.ExeName)+'\Data\XMLtoWORD\';

  Application.Initialize;

  Application.MainFormOnTaskbar := true;
  Application.Title := '������� ���� 04';
  Application.HelpFile := 'D:\projects\bitBakProject\LandMark\HELP\������ ������� ������.pdf';

  Application.CreateForm(TFrLandMark, FrLandMark);
  Application.CreateForm(TFormAllDocumentsMain, FormAllDocumentsMain);
  Application.CreateForm(TformContainer, formContainer);
  Application.CreateForm(TFrmParcelNeighbour, FrmParcelNeighbour);
  FormAllDocumentsMain.LoadXSD(STD_MP_WorkDir+'dAllDocuments.xsd');

  Application.Run;
end.
