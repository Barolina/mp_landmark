object formAllDocuments: TformAllDocuments
  Left = 0
  Top = 0
  BorderStyle = bsSizeToolWin
  Caption = #1050#1083#1072#1089#1089#1080#1092#1080#1082#1072#1090#1086#1088' '#1076#1086#1082#1091#1084#1077#1085#1090#1086#1074
  ClientHeight = 233
  ClientWidth = 471
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 209
    Width = 471
    Height = 24
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitTop = 320
    ExplicitWidth = 630
    object btnOk: TSpeedButton
      Left = 396
      Top = 0
      Width = 75
      Height = 24
      Align = alRight
      Caption = #1042#1099#1073#1088#1072#1090#1100
      ExplicitLeft = 448
    end
    object btnCancel: TSpeedButton
      Left = 321
      Top = 0
      Width = 75
      Height = 24
      Align = alRight
      Caption = #1054#1090#1084#1077#1085#1080#1090#1100
      ExplicitLeft = 376
    end
  end
  object edtSearch: TEdit
    Left = 0
    Top = 0
    Width = 471
    Height = 21
    Align = alTop
    TabOrder = 1
    ExplicitLeft = 64
    ExplicitTop = 72
    ExplicitWidth = 121
  end
  object tvDocuments: TTreeView
    Left = 0
    Top = 21
    Width = 471
    Height = 188
    Align = alClient
    Indent = 19
    TabOrder = 2
    ExplicitLeft = 104
    ExplicitTop = 64
    ExplicitWidth = 121
    ExplicitHeight = 97
  end
end
