unit SurveyingPlan.Xml.StdMp;

interface
uses
 System.classes,
 System.SysUtils,
 SurveyingPlan.Xml.Types,
 STD_MP,MsXmlApi;
type

 TSpValid_Simply = record
  class function IsValid(aValue: IXMLEntity_Spatial):string; static;
 end;


implementation

resourcestring
 rsNum_Geopoint = 'Num_Geopoint';
 rsGeopoint_Zacrep = 'Geopoint_Zacrep';
 rsDelta_Geopoint = 'Delta_Geopoint';
 rsPoint_Pref = 'Point_Pref';
 rsNewOrdinate = 'NewOrdinate';
 rsOldOrdinate = 'OldOrdinate';
 rsSpatial_Element = 'Spatial_Element';
 rsX = 'X';
 rsY = 'Y';
 type
  TConvertorStdMp = record
   class function Convert(aSource: IXMLTOrdinate; aDest:TSpPoint): boolean; overload; static;
   class function Convert(aSource: TSpPoint; aDest: IXMLTOrdinate): boolean; overload; static;
   class function Convert(aSource: IXMLTSPELEMENT_UNIT; aDest:TSpNodeBorder): boolean; overload; static;
   class function Convert(aSource: TSpNodeBorder; aDest: IXMLTSPELEMENT_UNIT): boolean; overload; static;
   class function Convert(aSource: IXMLTSPATIAL_ELEMENT; aDest:TSpBorderLandplot): boolean; overload; static;
   class function Convert(aSource: TSpBorderLandplot; aDest: IXMLTSPATIAL_ELEMENT): boolean; overload; static;
   class function Convert(aSource: IXMLEntity_Spatial; aDest:TSpMBordersLandplot): boolean; overload; static;
   class function Convert(aSource: TSpMBordersLandplot; aDest: IXMLEntity_Spatial): boolean; overload; static;
  end;

{ TSpIOConvertorStdMp }

class function TConvertorStdMp.Convert(aSource: IXMLTOrdinate; aDest: TSpPoint): boolean;
begin
 aDest.X:= StrToIntDef(MsXml_ReadAttribute(aSource,rsX),0);
 aDest.Y:=  StrToIntDef(MsXml_ReadAttribute(aSource,rsY),0);
 if aDest is TSpNodeBorder.TSpNewPoint then begin
  TSpNodeBorder.TSpNewPoint(aDest).NumGeopoint:= MsXml_ReadAttribute(aSource,rsNum_Geopoint);
  TSpNodeBorder.TSpNewPoint(aDest).GeopointZacrep:= MsXml_ReadAttribute(aSource,rsGeopoint_Zacrep);
  TSpNodeBorder.TSpNewPoint(aDest).DeltaGeopount:= MsXml_ReadAttribute(aSource,rsDelta_Geopoint);
  TSpNodeBorder.TSpNewPoint(aDest).PointPref:= MsXml_ReadAttribute(aSource,rsPoint_Pref);
 end;
 if aDest is TSpNodeBorder.TSpOldPoint then begin
  TSpNodeBorder.TSpOldPoint(aDest).NumGeopoint:= MsXml_ReadAttribute(aSource,rsNum_Geopoint);
  TSpNodeBorder.TSpOldPoint(aDest).DeltaGeopount:= MsXml_ReadAttribute(aSource,rsDelta_Geopoint);
 end;
end;

class function TConvertorStdMp.Convert(aSource: TSpPoint; aDest: IXMLTOrdinate): boolean;
begin
 aDest.X:= FloatToStr(aSource.X);
 aDest.Y:= FloatToStr(aSource.Y);
 if aDest is TSpNodeBorder.TSpNewPoint then begin

  if TSpNodeBorder.TSpNewPoint(aDest).NumGeopoint.IsValid then
    aDest.Num_Geopoint:= StrToInt(TSpNodeBorder.TSpNewPoint(aSource).NumGeopoint)
  else MsXml_RemoveAttribute(aDest,rsNum_Geopoint);

  if TSpNodeBorder.TSpNewPoint(aSource).GeopointZacrep.IsValid then
   aDest.Geopoint_Zacrep:= TSpNodeBorder.TSpNewPoint(aSource).GeopointZacrep
  else MsXml_RemoveAttribute(aDest,rsGeopoint_Zacrep);

  if TSpNodeBorder.TSpNewPoint(aSource).DeltaGeopount.IsValid then
   aDest.Delta_Geopoint:= TSpNodeBorder.TSpNewPoint(aSource).DeltaGeopount
  else MsXml_RemoveAttribute(aDest,rsDelta_Geopoint);

  if TSpNodeBorder.TSpNewPoint(aSource).PointPref.IsValid then
   aDest.Point_Pref:= TSpNodeBorder.TSpNewPoint(aSource).PointPref
  else MsXml_RemoveAttribute(aDest,rsPoint_Pref);
 end;

 if aDest is TSpNodeBorder.TSpOldPoint then begin

  if TSpNodeBorder.TSpNewPoint(aDest).NumGeopoint.IsValid then
    aDest.Num_Geopoint:= StrToInt(TSpNodeBorder.TSpNewPoint(aSource).NumGeopoint)
  else MsXml_RemoveAttribute(aDest,rsNum_Geopoint);

  if TSpNodeBorder.TSpNewPoint(aSource).DeltaGeopount.IsValid then
   aDest.Delta_Geopoint:= TSpNodeBorder.TSpNewPoint(aSource).DeltaGeopount
  else MsXml_RemoveAttribute(aDest,rsDelta_Geopoint);
 end;
end;

class function TConvertorStdMp.Convert(aSource: IXMLTSPELEMENT_UNIT; aDest: TSpNodeBorder): boolean;
var
 _IsExistNew,_IsExistOld: Boolean;
begin
 _IsExistNew:= Xml_IsExistChildNode(aSource,rsNewOrdinate);
 _IsExistOld:= Xml_IsExistChildNode(aSource,rsOldOrdinate);
 adest.Kind:= SpNb_kUndefined;
 aDest.TypeUnit:= aSource.Type_Unit;
 if _IsExistNew and _IsExistOld then
  aDest.Kind:= SpNb_kExist
 else
  if _IsExistNew then
   aDest.Kind:= SpNb_kCreated
  else
   if _IsExistOld then
    aDest.Kind:= SpNb_kRemoved
   else
     EXIT;

 if _IsExistNew then
  TConvertorStdMp.convert(aSource.NewOrdinate,aDest.NewPoint);
 if _IsExistOld then
  TConvertorStdMp.convert(aSource.OldOrdinate,aDest.OldPoint);
end;

class function TConvertorStdMp.Convert(aSource: TSpNodeBorder; aDest: IXMLTSPELEMENT_UNIT): boolean;
begin
 aDest.Type_Unit:= aSource.TypeUnit;
 case aSource.Kind of
   SpNb_kUndefined: begin
    MsXml_RemoveChildNode(aDest,rsNewOrdinate);
    MsXml_RemoveChildNode(aDest,rsOldOrdinate);
   end;
   SpNb_kCreated:begin
    MsXml_RemoveChildNode(aDest,rsOldOrdinate);
    TConvertorStdMp.convert(aSource.NewPoint,aDest.NewOrdinate);
   end;
   SpNb_kRemoved:begin
    MsXml_RemoveChildNode(aDest,rsNewOrdinate);
    TConvertorStdMp.convert(aSource.OldPoint,aDest.OldOrdinate);
   end;
   SpNb_kExist,SpNb_kModified:begin
    TConvertorStdMp.convert(aSource.NewPoint,aDest.NewOrdinate);
    TConvertorStdMp.convert(aSource.OldPoint,aDest.OldOrdinate);
   end;
 end;
end;

class function TConvertorStdMp.Convert(aSource: IXMLTSPATIAL_ELEMENT;
  aDest: TSpBorderLandplot): boolean;
var
 _i: integer;
 _spunit: IXMLTSPELEMENT_UNIT;
 _NodeBorder: TSpNodeBorder;
begin
 aDest.Clear;
 for _i := 0 to aSource.Count-1 do begin
  _spunit:= aSource.Spelement_Unit[_i];
  _NodeBorder:= TSpNodeBorder.Create;
  TConvertorStdMp.Convert(_spunit,_NodeBorder);
  aDest.Add(_NodeBorder);
 end;
end;

class function TConvertorStdMp.Convert(aSource: TSpBorderLandplot;
  aDest: IXMLTSPATIAL_ELEMENT): boolean;
var
 _i: integer;
 _spunit: IXMLTSPELEMENT_UNIT;
begin
 aDest.Clear;
 for _i := 0 to aSource.Count-1 do begin
  _spunit:= aDest.Add;
  TConvertorStdMp.Convert(aSource[_i],_spunit);
 end;
end;

class function TConvertorStdMp.Convert(aSource: IXMLEntity_Spatial;
  aDest: TSpMBordersLandplot): boolean;
var
 _i: integer;
 _SpEl: IXMLTSPATIAL_ELEMENT;
 _border: TSpBorderLandplot;
begin
 aDest.Clear;
 for _i := 0 to aSource.Spatial_Element.Count-1 do begin
  _SpEl:= aSource.Spatial_Element[_i];
  _border:= TSpBorderLandplot.Create;
  TConvertorStdMp.Convert(_SpEl,_border);
  aDest.Add(_border);
 end;
end;

class function TConvertorStdMp.Convert(aSource: TSpMBordersLandplot;
  aDest: IXMLEntity_Spatial): boolean;
var
 _SpEl: IXMLTSPATIAL_ELEMENT;
 _border: TSpBorderLandplot;
begin
 //aDest.Spatial_Element.Clear; ent_sys delete
 MsXml_RemoveChildNode(aDest,rsSpatial_Element,TRUE);
 for _border in aSource do begin
  _SpEl:= aDest.Spatial_Element.Add;
  TConvertorStdMp.Convert(_border,_SpEl);
 end;
end;

{ TSpValid }

class function TSpValid_Simply.IsValid(aValue: IXMLEntity_Spatial): string;
var
 _i: integer;
 _stb: TStringBuilder;
 _BorderInvalid: TSpBorderLandplot.TSpBl_ValidKinds;
 _MBorders: TSpMBordersLandplot;
 _MBordersInvalid: TSpMBordersLandplot.TSpMBl_ValidKinds;
begin
 _stb:= TStringBuilder.Create;
 _MBorders:= TSpMBordersLandplot.Create;
 TConvertorStdMp.Convert(aValue,_MBorders);
 _MBordersInvalid:= _MBorders.getInvalid;
 _stb.AppendLine(_MBordersInvalid.ToText);

 if SpMBl_vkItems in _MBordersInvalid then
  for _i := 0 to _MBorders.Count-1 do begin
   _BorderInvalid:= _MBorders[_i].getInvalid;
   if _BorderInvalid<>[] then
    _stb.AppendLine(format('%d) %s',[_i,_BorderInvalid.ToText]));
  end;

 _MBorders.Free;
 Result:= _stb.ToString;
 _stb.Free;
end;

end.
