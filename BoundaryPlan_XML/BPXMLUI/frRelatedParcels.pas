unit frRelatedParcels;

{
�������� � ��������� ��������, ������� � ���������� ��������� ��������
}
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ActnList, ButtonGroup, ExtCtrls,

  ImgList, System.Actions, Vcl.Clipbrd,

  STD_MP, UnParcelNeighbour,unRussLang,fmContainer, System.IOUtils,  System.RegularExpressions,//VBScript_RegExp_55_TLB,
  FrPreloader, JvComponentBase, JvBalloonHint, frOwnerNeighbours;

type

  TFrameRelatedParcels = class(TFrame)
    ActionList: TActionList;
    Action_Contrours_AddNew: TAction;
    Action_Contours_DeleteCurrent: TAction;
    Action_Contours_SetCurrent: TAction;
    lblRelatedParcels: TLabel;
    RelatedParcels_ListBox: TListBox;
    pnlDifinition: TPanel;
    pnlRight: TPanel;
    ParcelNeighbours_ListBox: TListBox;
    pnlAttributNeighbours: TPanel;
    actlstNeigh: TActionList;
    actNeight: TAction;
    actHeigh: TAction;
    actNeigh: TAction;
    btn1: TButtonGroup;
    lblNeighbours: TLabel;
    pnlCadastralNumber: TPanel;
    spl: TSplitter;
    pnlAttrRelatedParcels: TPanel;
    pnlList: TPanel;
    splattribut: TSplitter;
    pnlGeneralRelateparcel: TPanel;
    lblRelatedCountLabel: TLabel;
    actHint: TAction;
    actShowOwner: TAction;
    lbl2: TLabel;
    lbl3: TLabel;
    lblNameRight: TLabel;
    lblCountOwner: TLabel;
    pnl1: TPanel;
    btngrp1: TButtonGroup;
    scrlbx: TScrollBox;

    procedure Action_Contrours_AddNewExecute(Sender: TObject);
    procedure Action_Contours_DeleteCurrentExecute(Sender: TObject);
    procedure Action_Contours_SetCurrentExecute(Sender: TObject);
    procedure RelatedParcels_ListBoxDblClick(Sender: TObject);
    procedure actNeightExecute(Sender: TObject);
    procedure actHeighExecute(Sender: TObject);
    procedure RelatedParcelsRightButtonClick(Sender: TObject);
    procedure ParcelNeighbours_ListBoxClick(Sender: TObject);
    procedure ParcelNeighbours_ListBoxDblClick(Sender: TObject);
    procedure RelatedParcelsDblClick(Sender: TObject);
    procedure btnRelatedParcelsParcelNeighboursDblClick(Sender: TObject);
    procedure btnContours_List_ButtonGroupItems0Click(Sender: TObject);
    procedure RelatedParcels_ListBoxKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure ParcelNeighbours_ListBoxKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure actShowOwnerExecute(Sender: TObject);
    procedure pnlRightClick(Sender: TObject);
  private
   { Private declarations }
   FXmlRelatedParcels              : IXMLRelatedParcels;
   FXmlParcelNeighbours            : IXMLRelatedParcels_ParcelNeighbours;
   {�����}
   FParcelNeighbour_frames         : TFrmParcelNeighbour;

   procedure xmlRelatedSetEdit(name : string; itemIndex : integer); 
   {RelatedParcels}
   procedure RelatedParcels_DoSetCurrentItem(IndexItem: integer);
   procedure RelatedParcels_DoRefresh(A: Integer = -1; B: Integer = -1);
   procedure RelatedParcels_DoUpDate;
   procedure RelatedParcels_DoAddNewItem;
   procedure RelatedParcels_DoDeleteItem(IndexItem: Integer);   
   procedure RelatedParcelsEditDefinition;
   {Neighbours}
   procedure ParcelNeighbours_DoSetCurrentItem(IndexItem: integer);
   procedure ParcelNeighbours_DoRefresh(A: Integer = -1; B: Integer = -1);
   procedure ParcelNeighbours_DoUpDate;
   procedure ParcelNeighbours_DoAddNewItem;
   procedure ParcelNeighbours_DoDeleteItem(IndexItem: Integer);
   procedure ParcelNeighboursEditCadNum;

   procedure OnChange_Attribute_DefinitionOrNumberPP(sender: Tobject);
   procedure OnCnageData (sender : TObject);
  protected
   {RelatedParcels}
   procedure InitializationUI;
   procedure SetRelatedParcels(const aContours: IXMLRelatedParcels);
   procedure RelatedParcels_SetCurrent(index: integer);
   procedure RelatedParcelsDelete(ItemIndex : Integer);

   {Neighbours}
   procedure InitialisationNeighbours;
   procedure ParcelNeighbours_SetCurrent(index: integer);
   procedure SetNeighbours(const aContours: IXMLRelatedParcels_ParcelNeighbours);
   procedure Neighbours_DeleteItem( ItemIndex : integer);

   procedure ClipbrdPaste;

  public
   { Public declarations }
   constructor Create(Owner: TComponent); override;

   function IsAssignedRelatedPArcels: Boolean;
   function IsAssignNeighBours : Boolean;

   property XmlParcelNeighbours: IXMLRelatedParcels read FXmlRelatedParcels write SetRelatedParcels;
   property XmlOwnerNeighbours : TFrmParcelNeighbour read FParcelNeighbour_frames;

  end;

implementation

uses MsXMLAPI;


resourcestring  
       //rgxRelatedParcels ='(?<nt>((((�|)\d*)([,( )|-]*))*))[;\t](?<cadnum>(-|(\d+:\d+:\d+:\d+)))[;\t](?<prav>([^;]*))';
       rgxRelatedParcels = '((�|\d*[, |-]*)*)?[;\t](-|\d+:\d+:\d+:\d+)?[;\t]([^;]*)';
       rgxRelatedDefinition = '((�|\d*[, |-]*)*)';
       rsLblCountRelatedParcel = '���-�� ���������: ';
{$R *.dfm}

procedure TFrameRelatedParcels.actHeighExecute(Sender: TObject);
begin
 if not IsAssignedRelatedPArcels then EXIT;
 if self.ParcelNeighbours_ListBox.ItemIndex=-1 then EXIT;
 if MessageDlg('������� ������: '+'��������� ����������� �����'+'?',mtInformation,mbYesNo,-1) = mrNo then Exit;


 Neighbours_DeleteItem(Self.ParcelNeighbours_ListBox.ItemIndex);
 if not FrmParcelNeighbour.IsAssignedContours then
  self.ParcelNeighbours_SetCurrent(0);
end;

procedure TFrameRelatedParcels.Action_Contours_DeleteCurrentExecute(Sender: TObject);
begin
 if not IsAssignedRelatedPArcels then EXIT;
 if self.RelatedParcels_ListBox.ItemIndex=-1 then EXIT;
 if MessageDlg('������� ������: '+'����������� ����������� ����� ��� ����� �������,  � �������� � ��'+'?',mtInformation,mbYesNo,-1) = mrNo then Exit;

 RelatedParcelsDelete(RelatedParcels_ListBox.ItemIndex);
 if not self.IsAssignedRelatedPArcels then
  self.RelatedParcels_SetCurrent(0);
end;

procedure TFrameRelatedParcels.Action_Contours_SetCurrentExecute(Sender: TObject);
begin
 self.RelatedParcels_SetCurrent(self.RelatedParcels_ListBox.ItemIndex);
end;

procedure TFrameRelatedParcels.Action_Contrours_AddNewExecute(Sender: TObject);
begin
 if not self.IsAssignedRelatedPArcels then EXIT;
 self.RelatedParcels_DoAddNewItem;
 self.RelatedParcels_SetCurrent(self.RelatedParcels_ListBox.Count-1);
end;

procedure TFrameRelatedParcels.actNeightExecute(Sender: TObject);
begin
 if not self.IsAssignNeighBours then EXIT;
 self.ParcelNeighbours_DoAddNewItem;
 self.ParcelNeighbours_SetCurrent(self.ParcelNeighbours_ListBox.Count-1);
end;


procedure TFrameRelatedParcels.actShowOwnerExecute(Sender: TObject);
begin
  if not IsAssignedRelatedPArcels  then Exit;
  if not IsAssignNeighBours then Exit;
  FrmParcelNeighbour.ShowModal;
  if FrmParcelNeighbour.ModalResult = mrOk then
  begin
     if ParcelNeighbours_ListBox.ItemIndex  <> -1 then     
       ParcelNeighbours_DoSetCurrentItem(self.ParcelNeighbours_ListBox.ItemIndex);
  end;
end;

procedure TFrameRelatedParcels.btnContours_List_ButtonGroupItems0Click(Sender: TObject);
begin
try
 TFrLoader.Inctance.RunAnimation := true;
 try
   ClipbrdPaste;
 except on E: Exception do
     ShowMessage(E.Message);
 end;
finally
 TFrLoader.Inctance.RunAnimation := false;
end;
end;

procedure TFrameRelatedParcels.btnRelatedParcelsParcelNeighboursDblClick(Sender: TObject);
begin
 ParcelNeighboursEditCadNum;
end;



procedure TFrameRelatedParcels.RelatedParcels_DoRefresh(A, B: Integer);
var
 Contour: IXMLRelatedParcels_ParcelNeighbours;
 i,j: Integer;
 ItemName: string;
begin
 if self.RelatedParcels_ListBox.Items.Count = 0 then EXIT;

 if (A<0) or (a>=self.RelatedParcels_ListBox.Items.Count)  then
  a:= 0;

 if (B<A) or (B>=self.RelatedParcels_ListBox.Items.Count)  then
  B:= self.RelatedParcels_ListBox.Items.Count-1;

 j:= self.RelatedParcels_ListBox.ItemIndex;
     for i := A to B do begin
      Contour:= IXMLRelatedParcels_ParcelNeighbours(pointer(self.RelatedParcels_ListBox.Items.Objects[i]));
      ItemName:= Contour.Definition;
      self.RelatedParcels_ListBox.Items.Strings[i]:= ItemName;
     end;

 self.RelatedParcels_ListBox.ItemIndex:= j;
end;


procedure TFrameRelatedParcels.RelatedParcels_DoSetCurrentItem(IndexItem: integer);
var
 contour: IXMLRelatedParcels_ParcelNeighbours;
begin
 contour:= IXMLRelatedParcels_ParcelNeighbours(pointer(self.RelatedParcels_ListBox.Items.Objects[IndexItem]));
 SetNeighbours(contour);
end;

procedure TFrameRelatedParcels.RelatedParcels_SetCurrent(index: integer);
var countLblRelatedParcels : Integer;
begin
 countLblRelatedParcels := self.RelatedParcels_ListBox.Count;
 lblRelatedCountLabel.Caption := rsLblCountRelatedParcel+ IntToStr(countLblRelatedParcels);
 if (index>-1) and (index<countLblRelatedParcels ) then
 begin
    self.RelatedParcels_DoSetCurrentItem(index);
    self.RelatedParcels_ListBox.ItemIndex:= index;
    self.RelatedParcels_ListBox.Selected[index] := True;
    pnlCadastralNumber.Visible := true;
    self.pnlAttributNeighbours.Visible := true;
    self.pnlRight.Visible := True;

 end
 else
   begin
    self.RelatedParcels_ListBox.ItemIndex:= -1;
    self.ParcelNeighbours_ListBox.Clear;
    FXmlParcelNeighbours := nil;
    //todo :self.FParcelNeighbour_frames.Visible := false;
    lblNameRight.Caption := '-';
    lblCountOwner.Caption := '-';
    self.pnlAttributNeighbours.Visible := false;
    self.pnlRight.Visible := false;

   end;
end;

///
/// <format>
/// �185-�185, �189-�189, �197-�197, �201S;70:14:0300083:168;��� �����
//  </format>
/// 
procedure TFrameRelatedParcels.ClipbrdPaste;
var
  ClipbrData : TStringList;
  i, RowCnt : integer;
  nodeNeigboursParcel : IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour;
  nodeOwners : IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours;
  countLblRelatedParcel : Integer;
  strClipBoard  : STRING;
  regexpr : TRegEx;
  lstMat      : TArray<string>;
  _Definition : string;
  _Cad_Num    : string;
  _Right      : string;
begin
  if not Clipboard.HasFormat(CF_TEXT) then Exit;
  ClipbrData := TStringList.Create;
  MsXml_RemoveChildNode(FXmlRelatedParcels,cnstXmlParcelNeighbours,true);
  RelatedParcels_ListBox.Clear;
  ParcelNeighbours_ListBox.Clear;
  countLblRelatedParcel := 0;
  try
        ClipbrData.Text := Clipboard.AsText;
        RowCnt := ClipbrData.Count;
        for i := 0 to RowCnt - 1 do
        begin
          strClipBoard := ClipbrData.Strings[I];  /// ������� �� ����������� ������ �������
          if strClipBoard.IsEmpty then Break;

          if strClipBoard[Length(strClipBoard)] = ',' then
             strClipBoard := strClipBoard.Remove(Length(strClipBoard)-1,1);
          strClipBoard := strClipBoard.Trim;
          _Definition := ''; _Cad_Num := ''; _Right := '';

          lstMat := strClipBoard.Split([#9,';']);//
          if Length(lstMat) = 1  then
          begin
              raise Exception.Create('�������� ������! �������� �� ����� �� � ����� ������ ������  ������� ������� '+#13#10+lstMat[0]);
              Exit;
          end;

          if (Length(lstMat) <= 1) or (length(lstMat)>3)  then
          begin
             raise Exception.Create('�������� ������ �����!'+#13#10+ strClipBoard+#13#10+'�������� ��������� �� ���������� ";" ��� "tab"');
             Exit;
          end;
          ///���������������� definition �������� ��������� ///TODO: ����� ������������ ������� � ������  � ���������  ������ ���� �� ������������
          _Definition := lstMat[0];
          _Cad_Num    := lstMat[1];
          ///�������� ���������� �� ����������� �����
          regexpr := TRegEx.Create(regCadastralNumber);
          if not  regexpr.IsMatch(_Cad_Num) then
          begin
             raise Exception.Create('�������� ����� ����������� �������');
             Exit;
          end;
          if Length(lstMat) = 3 then _Right := lstMat[2];

          FXmlParcelNeighbours :=  FXmlRelatedParcels.Add;
          Inc(countLblRelatedParcel);
          if _Definition <> '-' then
             begin
               FXmlParcelNeighbours.Definition := _Definition;
               nodeNeigboursParcel := FXmlParcelNeighbours.ParcelNeighbour.Add;
               //�������� �����������  �����
               nodeNeigboursParcel.Cadastral_Number := _Cad_Num;
               //�������� ��� �����
               if _Right <> '-' then
                   nodeNeigboursParcel.OwnerNeighbours.NameRight  := _Right;
             end;
        end;
    InitializationUI;
  finally
    lblRelatedCountLabel.Caption := rsLblCountRelatedParcel+IntToStr(countLblRelatedParcel);
    ClipbrData.Free;
  end;
 
end;

constructor TFrameRelatedParcels.Create(Owner: TComponent);
begin
 inherited;
end;

procedure TFrameRelatedParcels.InitialisationNeighbours;
begin
 self.ParcelNeighbours_ListBox.Clear;
 if self.IsAssignNeighBours then
 begin
    self.ParcelNeighbours_DoUpDate;
    self.ParcelNeighbours_SetCurrent(0);
 end;
end;

procedure TFrameRelatedParcels.InitializationUI;
begin
 self.RelatedParcels_ListBox.Clear;
 if self.IsAssignedRelatedPArcels then
 begin
  self.RelatedParcels_DoUpDate;
  self.RelatedParcels_SetCurrent(0);
 end;
end;


function TFrameRelatedParcels.IsAssignedRelatedPArcels: Boolean;
begin
 Result:= self.FXmlRelatedParcels<>nil;
end;

function TFrameRelatedParcels.IsAssignNeighBours: Boolean;
begin
 Result := FXmlParcelNeighbours <> nil;
end;

procedure TFrameRelatedParcels.Neighbours_DeleteItem(ItemIndex: integer);
begin
 self.ParcelNeighbours_DoDeleteItem(self.ParcelNeighbours_ListBox.ItemIndex);
 self.ParcelNeighbours_SetCurrent(self.ParcelNeighbours_ListBox.ItemIndex); 
end;

procedure TFrameRelatedParcels.OnChange_Attribute_DefinitionOrNumberPP(sender: tobject);
begin
 self.RelatedParcels_DoRefresh(self.RelatedParcels_ListBox.ItemIndex,self.RelatedParcels_ListBox.ItemIndex);
end;

procedure TFrameRelatedParcels.OnCnageData(sender: TObject);
begin
  if not IsAssignedRelatedPArcels  then Exit;
  if not IsAssignNeighBours  then Exit;

end;

procedure TFrameRelatedParcels.ParcelNeighboursEditCadNum;
var itemIndex : integer;
    nameCadNum : string;
begin
 if(not IsAssignNeighBours) then Exit;
 itemIndex := self.ParcelNeighbours_ListBox.ItemIndex; 
 if itemIndex  <0  then exit; 
 nameCadNum := InputBox(rsChangeTheValue, rsEnterTheNumber,'');
 if (nameCadNum = '') then   Exit;
 FXmlParcelNeighbours.ParcelNeighbour.Items[itemIndex].Cadastral_Number := nameCadNum;
 ParcelNeighbours_ListBox.Items[itemIndex] := nameCadNum;
end;

procedure TFrameRelatedParcels.ParcelNeighbours_DoAddNewItem;
var
 contour: IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour;
 ItemName: string;
begin
 contour:= (self.FXmlParcelNeighbours.AddChild(cnstXmlParcelNeighbour) as IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour);
 ItemName:= rsEnterTheNumber;
 contour.Cadastral_Number:= ItemName;
 self.ParcelNeighbours_ListBox.AddItem(ItemName,pointer(contour));
end;

procedure TFrameRelatedParcels.ParcelNeighbours_DoDeleteItem(IndexItem: Integer);
var
 contour: IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour;
 index: Integer;
begin
 contour:= IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour(pointer(self.ParcelNeighbours_ListBox.Items.Objects[IndexItem]));
 if FrmParcelNeighbour.XmlOwnerNeighbours=contour then
  self.ParcelNeighbours_SetCurrent(IndexItem-1);
 self.ParcelNeighbours_ListBox.Items.Delete(IndexItem);
 self.FXmlParcelNeighbours.ChildNodes.Remove(contour);
 contour:= nil;
end;

procedure TFrameRelatedParcels.ParcelNeighbours_DoRefresh(A, B: Integer);
var
 Contour: IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour;
 i,j: Integer;
 ItemName: string;
begin
 if self.ParcelNeighbours_ListBox.Items.Count = 0 then EXIT;
 if (A<0) or (a>=self.ParcelNeighbours_ListBox.Items.Count)  then
  a:= 0;
 if (B<A) or (B>=self.ParcelNeighbours_ListBox.Items.Count)  then
  B:= self.ParcelNeighbours_ListBox.Items.Count-1;
 j:= self.ParcelNeighbours_ListBox.ItemIndex;
     for i := A to B do begin
     Contour:= IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour(pointer(self.ParcelNeighbours_ListBox.Items.Objects[i]));
     ItemName:=  Contour.Cadastral_Number;
     self.ParcelNeighbours_ListBox.Items.Strings[i]:= ItemName;
     end;
 self.ParcelNeighbours_ListBox.ItemIndex:= j;
end;

procedure TFrameRelatedParcels.ParcelNeighbours_DoSetCurrentItem(IndexItem: integer);
var
 _contour: IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour;
begin
_contour:= IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour(pointer(self.ParcelNeighbours_ListBox.Items.Objects[IndexItem]));
 FrmParcelNeighbour.XmlOwnerNeighbours:= _contour;

 lblNameRight.Caption := '-';
 lblCountOwner.Caption := '-';

 if (_contour<> nil) and
     (_contour.OwnerNeighbours <> nil)  then
     begin
        lblNameRight.Caption := _contour.OwnerNeighbours.NameRight;
        if (_contour.OwnerNeighbours.OwnerNeighbour <> nil) then
             lblCountOwner.Caption :=  IntToStr(_contour.OwnerNeighbours.OwnerNeighbour.Count)
        else
     end;
end;

procedure TFrameRelatedParcels.ParcelNeighbours_DoUpDate;
var
 i, index: Integer;
 Contour: IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour;
 ItemName: string;
begin
  index:= self.ParcelNeighbours_ListBox.ItemIndex;
  self.ParcelNeighbours_ListBox.Clear;
     for i := 0 to self.FXmlParcelNeighbours.ParcelNeighbour.Count -1 do begin
      Contour:=  FXmlParcelNeighbours.ParcelNeighbour.Items[i];
      ItemName:= Contour.Cadastral_Number;
      Contour.Cadastral_Number := ItemName;
      self.ParcelNeighbours_ListBox.AddItem(ItemName,pointer(Contour));
     end;
 self.ParcelNeighbours_SetCurrent(index);
 if not FrmParcelNeighbour.IsAssignedContours then
  self.ParcelNeighbours_SetCurrent(0);
end;

procedure TFrameRelatedParcels.ParcelNeighbours_ListBoxClick(Sender: TObject);
begin
 self.ParcelNeighbours_SetCurrent(self.ParcelNeighbours_ListBox.ItemIndex);
end;

procedure TFrameRelatedParcels.ParcelNeighbours_ListBoxDblClick(Sender: TObject);
begin
 ParcelNeighboursEditCadNum;
end;

{�������� ���������� ����������� �������}
procedure TFrameRelatedParcels.ParcelNeighbours_ListBoxKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
  var i : integer;
begin
  if ParcelNeighbours_ListBox.Count <= 0  then Exit;  
  if (key = VK_DELETE)  then
   begin
     if MessageDlg('������� ��������� ��������?', mtConfirmation,[mbYes, mbNo], -1) = mrYes then    
       for i :=  ParcelNeighbours_ListBox.Count-1 downto 0 do
       begin
         if ParcelNeighbours_ListBox.Selected[i] then
           Neighbours_DeleteItem(i);
       end;
     if not IsAssignNeighBours then ParcelNeighbours_SetCurrent(0);       
   end;
end;

procedure TFrameRelatedParcels.ParcelNeighbours_SetCurrent(index: integer);
begin
 if (index>-1) and (index<self.ParcelNeighbours_ListBox.Count) then begin
  self.ParcelNeighbours_DoSetCurrentItem(index);
  self.ParcelNeighbours_ListBox.ItemIndex:= index;
  Self.ParcelNeighbours_ListBox.Selected[index] := True;
 // FrmParcelNeighbour.Visible:= True;
 end else begin

 // self.FParcelNeighbour_frames.Visible:= false;
    self.ParcelNeighbours_ListBox.ItemIndex:= -1;
    lblNameRight.Caption := '-';
    lblCountOwner.Caption := '-';
 end;
end;

procedure TFrameRelatedParcels.pnlRightClick(Sender: TObject);
begin
  if not IsAssignedRelatedPArcels  then Exit;
  if not IsAssignNeighBours then Exit;
  FrmParcelNeighbour.Show;
end;

procedure TFrameRelatedParcels.SetRelatedParcels(
  const aContours: IXMLRelatedParcels);
begin
 if self.FXmlRelatedParcels=aContours then EXIT;
 begin
     self.FXmlRelatedParcels := aContours;
     self.RelatedParcels_SetCurrent(-1);
     self.InitializationUI;
 end;
end;

procedure TFrameRelatedParcels.SetNeighbours(const aContours: IXMLRelatedParcels_ParcelNeighbours);
begin
 if self.FXmlParcelNeighbours=aContours then EXIT;
 self.FXmlParcelNeighbours := aContours;
 self.ParcelNeighbours_SetCurrent(-1);
 self.InitialisationNeighbours;
end;

procedure TFrameRelatedParcels.RelatedParcelsDblClick(Sender: TObject);
begin
 RelatedParcelsEditDefinition;
end;

procedure TFrameRelatedParcels.RelatedParcelsDelete(ItemIndex: Integer);
begin
 self.RelatedParcels_DoDeleteItem(self.RelatedParcels_ListBox.ItemIndex);
 self.RelatedParcels_SetCurrent(self.RelatedParcels_ListBox.ItemIndex);   
end;

procedure TFrameRelatedParcels.xmlRelatedSetEdit(name : string; itemIndex : integer);
begin
  FXmlRelatedParcels.ParcelNeighbours[itemIndex].Definition := name;
  RelatedParcels_ListBox.Items[itemIndex] := name;
end;

procedure TFrameRelatedParcels.RelatedParcelsEditDefinition;
var itemIndex : integer;
    nameDifinion : string;
    old_str :  string;
begin
 if(not IsAssignedRelatedPArcels) then Exit;
  itemIndex := self.RelatedParcels_ListBox.ItemIndex; 
  if itemIndex < 0  then Exit;
  old_str := self.RelatedParcels_ListBox.Items[itemIndex];
  nameDifinion := InputBox(rsChangeTheValue, rsEnterThe + rsDefinitionNumber, old_str);
  if nameDifinion = old_str then EXIT;
  xmlRelatedSetEdit(nameDifinion,itemIndex);
end;

procedure TFrameRelatedParcels.RelatedParcelsRightButtonClick(Sender: TObject);
begin
 if self.RelatedParcels_ListBox.Visible then
 begin
   RelatedParcels_ListBox.Visible := false;
   self.pnlAttributNeighbours.Parent  := self.pnlGeneralRelateparcel;
   self.pnlAttributNeighbours.Align   := alClient;
   self.pnlAttributNeighbours.Constraints.MaxWidth   := 0;  
  end  
 else
 begin
    self.pnlGeneralRelateparcel.Align  := alClient;  
    self.pnlAttributNeighbours.Constraints.MaxWidth   := 301;  
    self.pnlAttributNeighbours.Parent  := self.pnlList;
    self.pnlAttributNeighbours.Align   := alRight; 
    self.RelatedParcels_ListBox.Visible:= true;
    self.RelatedParcels_ListBox.SetFocus;
 end;
end;

procedure TFrameRelatedParcels.RelatedParcels_DoAddNewItem;
var
 contour: IXMLRelatedParcels_ParcelNeighbours;
 ItemName: string;
begin
 contour:= FXmlRelatedParcels.Add;
 ItemName:= rsDefinitionNumber;
 contour.Definition:= ItemName;
 self.RelatedParcels_ListBox.AddItem(ItemName,pointer(contour));
end;

procedure TFrameRelatedParcels.RelatedParcels_DoDeleteItem(IndexItem: Integer);
var
 contour: IXMLRelatedParcels_ParcelNeighbours;
 index: Integer;
begin
   contour:= IXMLRelatedParcels_ParcelNeighbours(pointer(self.RelatedParcels_ListBox.Items.Objects[IndexItem]));
   if self.FXmlParcelNeighbours =contour then self.RelatedParcels_SetCurrent(IndexItem-1);
   self.RelatedParcels_ListBox.Items.Delete(IndexItem);
   self.FXmlRelatedParcels.Remove(contour);
   contour:= nil;
end;

procedure TFrameRelatedParcels.RelatedParcels_DoUpDate;
var
 i, index: Integer;
 Contour: IXMLRelatedParcels_ParcelNeighbours;
 ItemName: string;
 countLblRelatedParcel  : Integer;
begin
 index:= self.RelatedParcels_ListBox.ItemIndex;
 self.RelatedParcels_ListBox.Clear;
 countLblRelatedParcel := self.FXmlRelatedParcels.Count;
 for i := 0 to countLblRelatedParcel -1 do 
 begin
  Contour:=  FXmlRelatedParcels.ParcelNeighbours[i];
  ItemName:=Contour.Definition;
  Contour.Definition := ItemName;
  self.RelatedParcels_ListBox.AddItem(ItemName,pointer(Contour));
 end;
 self.RelatedParcels_SetCurrent(index);
 if not self.IsAssignNeighBours then self.RelatedParcels_SetCurrent(0);
end;

procedure TFrameRelatedParcels.RelatedParcels_ListBoxDblClick(Sender: TObject);
begin
 RelatedParcelsEditDefinition;
end;


{������ ��������� �������� RelatedParcel}
procedure TFrameRelatedParcels.RelatedParcels_ListBoxKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
  var i : Integer;
begin
   if RelatedParcels_ListBox.Count <= 0  then Exit;   
   if (key = VK_DELETE)  then
   begin
     if MessageDlg('������� ��������� ��������?', mtConfirmation,[mbYes, mbNo], -1) = mrYes then    
       for i :=  RelatedParcels_ListBox.Count-1 downto 0 do 
       begin
         if RelatedParcels_ListBox.Selected[i] then
           RelatedParcelsDelete(i);
       end;
     if not IsAssignedRelatedPArcels then RelatedParcels_SetCurrent(0);       
   end;
end;

end.


