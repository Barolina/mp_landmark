﻿
{***********************************************************}
{                                                           }
{                     XML Data Binding                      }
{                                                           }
{         Generated on: 16.06.2013 16:53:02                 }
{       Generated from: E:\LandMark\V04_STD_MP\STD_MP.xdb   }
{   Settings stored in: E:\LandMark\V04_STD_MP\STD_MP.xdb   }
{                                                           }
{***********************************************************}
{$define disableIXMLTNewParcel_Contours}
{$define disableIXMLTChangeParcel_SubParcels}
{$define disableIXMLTNewSubParcel_Contours}
{$DEFINE disableIXMLTNewSubParcel_Entity_Spatial}
{
 Upadte on 14.06.2013 ----------------------------------------------------------
 - добавлен  интерфейс и клас IXMLTInner_CadastralNumbers  вместо
                              IXMLTExistParcel_Inner_CadastralNumbers
 -добавлен класс и интерфейс IXMLTInner_CadastralNumbers_CadastralNumberList
  вместо IXMLTExistParcel_Inner_CadastralNumbers_CadastralNumberList

Upadte on 17.06.2013 -----------------------------------------------------------
 - убран класс и интерфейс IXMLTAreaNew и оставлен вместо него IXMLTArea
 - переименованны класс и интерфейс
   IXMLTSPATIAL_ELEMENT_OLD_NEW = interface;
   IXMLTSPATIAL_ELEMENTList_OLD_NEW = interface;
   IXMLTSPELEMENT_UNIT_OLD_NEw = interface; на

   IXMLTSPATIAL_ELEMENT = interface;
   IXMLTSPATIAL_ELEMENTList = interface;
   IXMLTSPELEMENT_UNIT = interface;

Update  on 19.06.2013-----------------------------------------------------------
  - убран интерфейс и класс IXMLTArea_Contour он похож  с IXMLTArea
  - IXMLTSubParcel_Contours_Contour_Entity_Spatial - убран

Update on 20.06.2013-----------------------------------------------------------
  - заменне  IXMLTSpecifyRelatedParcel_ChangeBorder на  IXMLTChangeBorder
   (тоже часое и с таким же классом)

Update  on 25.06.2013-----------------------------------------------------------
  - В классе и интерфейсе IXMLTExistParcel_SubParcels_InvariableSubParcel_Contours_Contour :
      property Area: IXMLTArea_Contour, заменен на
                     IXMLTArea;

Upadate on 27.06.2013 -----------------------------------------------------------
  -  IXMLSTD_MP_Title_Client_Foreign_Organization_Agent = interface;
    заменен на     IXMLSTD_MP_Title_Client_Organization_Agent = interface
    (одинаковые)

  - Замена
        IXMLSTD_MP_Agreement_Document_AppliedFile
        IXMLSTD_MP_Diagram_Parcels_SubParcels_AppliedFile
        IXMLSTD_MP_Scheme_Disposition_Parcels = interface
       IXMLSTD_MP_NodalPointSchemes_NodalPointScheme_AppliedFile;
     на
        IXMLSTD_MP_Survey_AppliedFile

 Update on  25.07.2013
   замена
    IXMLSTD_MP_Input_Data_Documents_Document
  на
 Update on 22.08.2013
   TXMLTAreaNew  замен на TXMLTArea -*для  FormParcels (похожи)

 Update on 04.09.2013
   MLSTD_MP_Title_Client_Governance_Agent  заменн на MLSTD_MP_Title_Client_Organisation_Agent

 Update on 11.09.2013
   TXMLTExistEZParcel_SubParcels заменен на TXMLTExistParcel_SubParcels);

 Update on 22.10.2013 IXMLTNewParcel_Contours - отключен директивой "disableIXMLTNewParcel_Contours"
   и заменен на IXMLContours->IXMLContours_NewContour т.к. они полностью идентичны

 Update on 03.03.2014 IXMLTChangeParcel_SubParcels - отключен директивой "disableIXMLTChangeParcel_SubParcels"
   и заменен на  IXMLTExistParcel_SubParcels т.к. они полностью идентичны

 Update on 05.03.2014 IXMLTNewSubParcel_Contours отключен и заменен на   IXMLTSubParcel_Contours
                      IXMLTNewSubParcel_Entity_Spatial отключен и заменен на IXMLTEntity_Spatial    
 }
unit STD_MP;

interface

uses xmldom, XMLDoc, XMLIntf;

Const
 StdMp_Area_Unit = '055'; //метры

 StdMp_dParcels_LandUse = '01';//землепользование
 StdMp_dParcels_SingleLandUse = '02'; //единое землепользование
 StdMp_dParcels_SeparatePlot =  '03'; //Обособленный участок
 StdMp_dParcels_ConditionalPlot = '04';// Условный участок
 StdMp_dParcels_MulticontoursPlot ='05';// Многоконтурный участок
 StdMp_dParcels_None =	'06';// Значение отсутствует


type

{ Forward Decls }

  IXMLSTD_MP = interface;
  IXMLSTD_MP_eDocument = interface;
  IXMLSTD_MP_Title = interface;
  IXMLSTD_MP_Title_Contractor = interface;
  IXMLTFIO = interface;
  IXMLSTD_MP_Title_Client = interface;
  IXMLSTD_MP_Title_ClientList = interface;
  IXMLSTD_MP_Title_Client_Person = interface;
  IXMLSTD_MP_Title_Client_Organization = interface;
  IXMLSTD_MP_Title_Client_Organization_Agent = interface;
  IXMLSTD_MP_Title_Client_Foreign_Organization = interface;
  //IXMLSTD_MP_Title_Client_Foreign_Organization_Agent = interface;
  IXMLSTD_MP_Title_Client_Governance = interface;
  IXMLSTD_MP_Title_Client_Governance_Agent = interface;
  IXMLSTD_MP_Package = interface;
  IXMLSTD_MP_Package_FormParcels = interface;
  IXMLTNewParcel = interface;
  IXMLTNewParcelList = interface;
  IXMLTNewParcel_Prev_CadastralNumbers = interface;
  IXMLTProviding_Pass_CadastralNumbers = interface;
  IXMLTProviding_Pass_CadastralNumbers_Documents = interface;
  IXMLTDocument = interface;
  IXMLDuration = interface;
  IXMLAppliedFiles = interface;
  IXMLTAppliedFile = interface;
  IXMLTInner_CadastralNumbers = interface; //TODO:L:
//  IXMLTAreaNew = interface;
  IXMLTAddress = interface;
  IXMLDistrict = interface;
  IXMLCity = interface;
  IXMLUrban_District = interface;
  IXMLSoviet_Village = interface;
  IXMLLocality = interface;
  IXMLStreet = interface;
  IXMLLevel1 = interface;
  IXMLLevel2 = interface;
  IXMLLevel3 = interface;
  IXMLApartment = interface;
  IXMLTNewParcel_Location = interface;
  IXMLTCategory = interface;
  IXMLTNatural_Object = interface;
  IXMLTUtilization = interface;
  IXMLTNewParcel_SubParcels = interface;
  IXMLTSubParcel = interface;
  IXMLTArea = interface;
  IXMLTEncumbrance = interface;
  IXMLTEncumbrance_Documents = interface;
  IXMLEntity_Spatial = interface;
  IXMLTSPATIAL_ELEMENT = interface;
  IXMLTSPATIAL_ELEMENTList = interface;
  IXMLTSPELEMENT_UNIT = interface;
  IXMLTOrdinate = interface;
  IXMLEntity_Spatial_Borders = interface;
  IXMLTBorder = interface;
  IXMLTBorder_Edge = interface;
  IXMLEntity_Spatial_Borders_Border = interface;
  IXMLTSubParcel_Contours = interface;
  IXMLTSubParcel_Contours_Contour = interface;
  //IXMLTArea_Contour = interface;
  //IXMLTSubParcel_Contours_Contour_Entity_Spatial = interface;
  IXMLTNewParcel_SubParcels_FormSubParcel = interface;
  {$ifndef disableIXMLTNewParcel_Contours}
  IXMLTNewParcel_Contours = interface;
  IXMLTNewParcel_Contours_NewContour = interface;
  {$endif}
  IXMLTContour = interface;
  IXMLTArea_without_Innccuracy = interface;
  IXMLTChangeParcel = interface;
  IXMLTChangeParcelList = interface;
  IXMLTChangeParcel_Inner_CadastralNumbers = interface;
  {$IFndef disableIXMLTChangeParcel_SubParcels}
  IXMLTChangeParcel_SubParcels = interface;
  {$endif}
  IXMLTChangeParcel_SubParcels_FormSubParcel = interface;
  IXMLTChangeParcel_SubParcels_FormSubParcelList = interface;
  IXMLTChangeParcel_SubParcels_ExistSubParcel = interface;
  IXMLTChangeParcel_SubParcels_ExistSubParcelList = interface;
  IXMLTChangeParcel_SubParcels_InvariableSubParcel = interface;
  IXMLTChangeParcel_SubParcels_InvariableSubParcelList = interface;
  IXMLTChangeParcel_SubParcels_InvariableSubParcel_Area = interface;
  IXMLTChangeParcel_SubParcels_InvariableSubParcel_Contours = interface;
  IXMLTChangeParcel_SubParcels_InvariableSubParcel_Contours_Contour = interface;
  IXMLTChangeParcel_DeleteEntryParcels = interface;
  IXMLTChangeParcel_DeleteEntryParcels_DeleteEntryParcel = interface;
  IXMLTSpecifyRelatedParcel = interface;
  IXMLTSpecifyRelatedParcel_AllBorder = interface;
  //IXMLTSpecifyRelatedParcel_ChangeBorder = interface;
  IXMLTChangeBorder = interface;
  IXMLTSpecifyRelatedParcel_ChangeBorderList = interface;
  IXMLTSpecifyRelatedParcel_Contours = interface;
  IXMLTSpecifyRelatedParcel_Contours_NewContour = interface;
  IXMLTSpecifyRelatedParcel_DeleteAllBorder = interface;
  IXMLTSpecifyRelatedParcel_DeleteAllBorderList = interface;
  IXMLTSpecifyRelatedParcel_ExistSubParcels = interface;
  IXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel = interface;
  IXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Entity_Spatial = interface;
  IXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Contours = interface;
  IXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Contours_Contour = interface;
  IXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Contours_Contour_Entity_Spatial = interface;
  IXMLSTD_MP_Package_FormParcels_SpecifyRelatedParcel = interface;
  IXMLSTD_MP_Package_FormParcels_SpecifyRelatedParcelList = interface;
  IXMLSTD_MP_Package_SpecifyParcel = interface;
  IXMLTExistParcel = interface;
  //IXMLTExistParcel_Inner_CadastralNumbers = interface;

  IXMLTExistParcel_SubParcels = interface;
  IXMLTExistParcel_SubParcels_FormSubParcel = interface;
  IXMLTExistParcel_SubParcels_FormSubParcelList = interface;
  IXMLTExistParcel_SubParcels_ExistSubParcel = interface;
  IXMLTExistParcel_SubParcels_ExistSubParcelList = interface;
  IXMLTExistParcel_SubParcels_InvariableSubParcel = interface;
  IXMLTExistParcel_SubParcels_InvariableSubParcelList = interface;
  IXMLTExistParcel_SubParcels_InvariableSubParcel_Area = interface;
  IXMLTExistParcel_SubParcels_InvariableSubParcel_Contours = interface;
  IXMLTExistParcel_SubParcels_InvariableSubParcel_Contours_Contour = interface;
  IXMLContours = interface;
  IXMLContours_NewContour = interface;
  IXMLContours_NewContourList = interface;
  IXMLContours_ExistContour = interface;
  IXMLContours_ExistContourList = interface;
  IXMLContours_DeleteAllBorder = interface;
  IXMLContours_DeleteAllBorderList = interface;
  IXMLRelatedParcels = interface;
  IXMLRelatedParcels_ParcelNeighbours = interface;
  IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour = interface;
  IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbourList = interface;
  IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours = interface;
  IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbour = interface;
  IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbourList = interface;
  IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbour_Documents = interface;
  IXMLSTD_MP_Package_SpecifyParcel_ExistEZ = interface;
  IXMLTExistEZParcel = interface;

  IXMLTExistEZParcel_Inner_CadastralNumbers = interface;

  IXMLTExistEZParcel_SubParcels = interface;
  IXMLTExistEZParcel_SubParcels_FormSubParcel = interface;
  IXMLTExistEZParcel_SubParcels_FormSubParcelList = interface;
  IXMLTExistEZParcel_SubParcels_ExistSubParcel = interface;
  IXMLTExistEZParcel_SubParcels_ExistSubParcelList = interface;
  IXMLTExistEZParcel_SubParcels_InvariableSubParcel = interface;
  IXMLTExistEZParcel_SubParcels_InvariableSubParcelList = interface;
  IXMLTExistEZParcel_SubParcels_InvariableSubParcel_Area = interface;
  IXMLTExistEZParcel_SubParcels_InvariableSubParcel_Contours = interface;
  IXMLTExistEZParcel_SubParcels_InvariableSubParcel_Contours_Contour = interface;
  IXMLTExistEZParcel_Composition_EZ = interface;
  IXMLTExistEZParcel_Composition_EZ_InsertEntryParcels = interface;
  IXMLTExistEZParcel_Composition_EZ_InsertEntryParcels_InsertEntryParcel = interface;
  IXMLTExistEZParcel_Composition_EZ_InsertEntryParcels_InsertEntryParcel_ExistEntryParcel = interface;
  IXMLTNewEZEntryParcel = interface;
  IXMLTExistEZParcel_Composition_EZ_DeleteEntryParcels = interface;
  IXMLTExistEZParcel_Composition_EZ_DeleteEntryParcels_DeleteEntryParcel = interface;
  IXMLSTD_MP_Package_SpecifyParcel_ExistEZ_ExistEZEntryParcels = interface;
  IXMLSTD_MP_Package_SpecifyParcel_ExistEZ_ExistEZEntryParcelsList = interface;
  IXMLTExistEZEntryParcel = interface;
  IXMLSTD_MP_Package_SpecifyParcel_SpecifyRelatedParcel = interface;
  IXMLSTD_MP_Package_SpecifyParcel_SpecifyRelatedParcelList = interface;
  IXMLTNewSubParcel = interface;
  IXMLTNewSubParcelList = interface;
  {$IFNDEF disableIXMLTNewSubParcel_Entity_Spatial}
  IXMLTNewSubParcel_Entity_Spatial = interface;
  {$ENDIF}
  {$IFNDEF disableIXMLTNewSubParcel_Contours}
  IXMLTNewSubParcel_Contours = interface;
  {$ENDIF}
  IXMLTNewSubParcel_Contours_Contour = interface;
  IXMLTNewSubParcel_Contours_Contour_Entity_Spatial = interface;
  IXMLSTD_MP_Package_SpecifyParcelsApproximal = interface;
  IXMLSTD_MP_Package_SpecifyParcelsApproximalList = interface;
  IXMLSTD_MP_Package_SpecifyParcelsApproximal_ExistEZ = interface;
  IXMLSTD_MP_Package_SpecifyParcelsApproximal_ExistEZ_ExistEZEntryParcels = interface;
  IXMLSTD_MP_Package_SpecifyParcelsApproximal_ExistEZ_ExistEZEntryParcelsList = interface;
  IXMLCoord_Systems = interface;
  IXMLCoord_System = interface;
  IXMLSTD_MP_Input_Data = interface;
  IXMLSTD_MP_Input_Data_Documents = interface;
  IXMLSTD_MP_Input_Data_Documents_Document = interface;
  IXMLSTD_MP_Input_Data_Geodesic_Bases = interface;
  IXMLSTD_MP_Input_Data_Geodesic_Bases_Geodesic_Base = interface;
  IXMLSTD_MP_Input_Data_Means_Survey = interface;
  IXMLSTD_MP_Input_Data_Means_Survey_Means_Survey = interface;
  IXMLSTD_MP_Input_Data_Realty = interface;
  IXMLSTD_MP_Input_Data_Realty_OKS = interface;
  IXMLSTD_MP_Input_Data_SubParcels = interface;
  IXMLSTD_MP_Input_Data_SubParcels_SubParcel = interface;
  IXMLSTD_MP_Survey = interface;
  IXMLSTD_MP_Survey_AppliedFile = interface;
  IXMLSTD_MP_Scheme_Geodesic_Plotting = interface;
  IXMLSTD_MP_Scheme_Geodesic_Plotting_AppliedFile = interface;
  IXMLSTD_MP_Scheme_Disposition_Parcels = interface;
  IXMLSTD_MP_Scheme_Disposition_Parcels_AppliedFile = interface;
  IXMLSTD_MP_Diagram_Parcels_SubParcels = interface;
  IXMLSTD_MP_Diagram_Parcels_SubParcels_AppliedFile = interface;
  IXMLSTD_MP_Agreement_Document = interface;
  IXMLSTD_MP_Agreement_Document_AppliedFile = interface;
  IXMLSTD_MP_NodalPointSchemes = interface;
  IXMLSTD_MP_NodalPointSchemes_NodalPointScheme = interface;
  IXMLSTD_MP_NodalPointSchemes_NodalPointScheme_AppliedFile = interface;
  IXMLSTD_MP_Appendix = interface;
  IXMLString_List = interface;
  IXMLTChangeParcel_Inner_CadastralNumbers_CadastralNumberList = interface;
  IXMLTExistEZParcel_Inner_CadastralNumbers_CadastralNumberList = interface;

//  IXMLTExistParcel_Inner_CadastralNumbers_CadastralNumberList = interface;
  IXMLTInner_CadastralNumbers_CadastralNumberList = interface;

  IXMLTProviding_Pass_CadastralNumbers_CadastralNumberList = interface;

{ IXMLSTD_MP }

  IXMLSTD_MP = interface(IXMLNode)
    ['{9BED72E1-C9E8-428B-8A9B-FD380FE07C80}']
    { Property Accessors }
    function Get_EDocument: IXMLSTD_MP_eDocument;
    function Get_Title: IXMLSTD_MP_Title;
    function Get_Package: IXMLSTD_MP_Package;
    function Get_Coord_Systems: IXMLCoord_Systems;
    function Get_Input_Data: IXMLSTD_MP_Input_Data;
    function Get_Survey: IXMLSTD_MP_Survey;
    function Get_Conclusion: UnicodeString;
    function Get_Scheme_Geodesic_Plotting: IXMLSTD_MP_Scheme_Geodesic_Plotting;
    function Get_Scheme_Disposition_Parcels: IXMLSTD_MP_Scheme_Disposition_Parcels;
    function Get_Diagram_Parcels_SubParcels: IXMLSTD_MP_Diagram_Parcels_SubParcels;
    function Get_Agreement_Document: IXMLSTD_MP_Agreement_Document;
    function Get_NodalPointSchemes: IXMLSTD_MP_NodalPointSchemes;
    function Get_Appendix: IXMLSTD_MP_Appendix;
    procedure Set_Conclusion(Value: UnicodeString);
    { Methods & Properties }
    property EDocument: IXMLSTD_MP_eDocument read Get_EDocument;
    property Title: IXMLSTD_MP_Title read Get_Title;
    property Package: IXMLSTD_MP_Package read Get_Package;
    property Coord_Systems: IXMLCoord_Systems read Get_Coord_Systems;
    property Input_Data: IXMLSTD_MP_Input_Data read Get_Input_Data;
    property Survey: IXMLSTD_MP_Survey read Get_Survey;
    property Conclusion: UnicodeString read Get_Conclusion write Set_Conclusion;
    property Scheme_Geodesic_Plotting: IXMLSTD_MP_Scheme_Geodesic_Plotting read Get_Scheme_Geodesic_Plotting;
    property Scheme_Disposition_Parcels: IXMLSTD_MP_Scheme_Disposition_Parcels read Get_Scheme_Disposition_Parcels;
    property Diagram_Parcels_SubParcels: IXMLSTD_MP_Diagram_Parcels_SubParcels read Get_Diagram_Parcels_SubParcels;
    property Agreement_Document: IXMLSTD_MP_Agreement_Document read Get_Agreement_Document;
    property NodalPointSchemes: IXMLSTD_MP_NodalPointSchemes read Get_NodalPointSchemes;
    property Appendix: IXMLSTD_MP_Appendix read Get_Appendix;
  end;

{ IXMLSTD_MP_eDocument }

  IXMLSTD_MP_eDocument = interface(IXMLNode)
    ['{0FB43938-0046-4AF4-9288-85DFE01FF591}']
    { Property Accessors }
    function Get_CodeType: UnicodeString;
    function Get_Version: UnicodeString;
    function Get_GUID: UnicodeString;
    procedure Set_CodeType(Value: UnicodeString);
    procedure Set_Version(Value: UnicodeString);
    procedure Set_GUID(Value: UnicodeString);
    { Methods & Properties }
    property CodeType: UnicodeString read Get_CodeType write Set_CodeType;
    property Version: UnicodeString read Get_Version write Set_Version;
    property GUID: UnicodeString read Get_GUID write Set_GUID;
  end;

{ IXMLSTD_MP_Title }

  IXMLSTD_MP_Title = interface(IXMLNode)
    ['{144CB5D2-3A8E-4057-93E6-9D668ED5AAD8}']
    { Property Accessors }
    function Get_Contractor: IXMLSTD_MP_Title_Contractor;
    function Get_Purpose: UnicodeString;
    function Get_Reason: UnicodeString;
    function Get_Client: IXMLSTD_MP_Title_ClientList;
    procedure Set_Purpose(Value: UnicodeString);
    procedure Set_Reason(Value: UnicodeString);
    { Methods & Properties }
    property Contractor: IXMLSTD_MP_Title_Contractor read Get_Contractor;
    property Purpose: UnicodeString read Get_Purpose write Set_Purpose;
    property Reason: UnicodeString read Get_Reason write Set_Reason;
    property Client: IXMLSTD_MP_Title_ClientList read Get_Client;
  end;

{ IXMLSTD_MP_Title_Contractor }

  IXMLSTD_MP_Title_Contractor = interface(IXMLNode)
    ['{8EA6EFCB-0F8B-45E7-AE80-513C55F3E5F4}']
    { Property Accessors }
    function Get_Date: UnicodeString;
    function Get_FIO: IXMLTFIO;
    function Get_N_Certificate: UnicodeString;
    function Get_Telephone: UnicodeString;
    function Get_Address: UnicodeString;
    function Get_E_mail: UnicodeString;
    function Get_Organization: UnicodeString;
    procedure Set_Date(Value: UnicodeString);
    procedure Set_N_Certificate(Value: UnicodeString);
    procedure Set_Telephone(Value: UnicodeString);
    procedure Set_Address(Value: UnicodeString);
    procedure Set_E_mail(Value: UnicodeString);
    procedure Set_Organization(Value: UnicodeString);
    { Methods & Properties }
    property Date: UnicodeString read Get_Date write Set_Date;
    property FIO: IXMLTFIO read Get_FIO;
    property N_Certificate: UnicodeString read Get_N_Certificate write Set_N_Certificate;
    property Telephone: UnicodeString read Get_Telephone write Set_Telephone;
    property Address: UnicodeString read Get_Address write Set_Address;
    property E_mail: UnicodeString read Get_E_mail write Set_E_mail;
    property Organization: UnicodeString read Get_Organization write Set_Organization;
  end;

{ IXMLTFIO }

  IXMLTFIO = interface(IXMLNode)
    ['{E2180F5B-1484-4D43-90F3-B6A762AC53A3}']
    { Property Accessors }
    function Get_Surname: UnicodeString;
    function Get_First: UnicodeString;
    function Get_Patronymic: UnicodeString;
    procedure Set_Surname(Value: UnicodeString);
    procedure Set_First(Value: UnicodeString);
    procedure Set_Patronymic(Value: UnicodeString);
    { Methods & Properties }
    property Surname: UnicodeString read Get_Surname write Set_Surname;
    property First: UnicodeString read Get_First write Set_First;
    property Patronymic: UnicodeString read Get_Patronymic write Set_Patronymic;
  end;

{ IXMLSTD_MP_Title_Client }

  IXMLSTD_MP_Title_Client = interface(IXMLNode)
    ['{EF609F5B-5EF1-4D21-A2BF-CED0AADFBC40}']
    { Property Accessors }
    function Get_Date: UnicodeString;
    function Get_Person: IXMLSTD_MP_Title_Client_Person;
    function Get_Organization: IXMLSTD_MP_Title_Client_Organization;
    function Get_Foreign_Organization: IXMLSTD_MP_Title_Client_Foreign_Organization;
    function Get_Governance: IXMLSTD_MP_Title_Client_Governance;
    procedure Set_Date(Value: UnicodeString);
    { Methods & Properties }
    property Date: UnicodeString read Get_Date write Set_Date;
    property Person: IXMLSTD_MP_Title_Client_Person read Get_Person;
    property Organization: IXMLSTD_MP_Title_Client_Organization read Get_Organization;
    property Foreign_Organization: IXMLSTD_MP_Title_Client_Foreign_Organization read Get_Foreign_Organization;
    property Governance: IXMLSTD_MP_Title_Client_Governance read Get_Governance;
  end;

{ IXMLSTD_MP_Title_ClientList }

  IXMLSTD_MP_Title_ClientList = interface(IXMLNodeCollection)
    ['{EA539B76-14D7-4274-A963-4E7A73E54AF1}']
    { Methods & Properties }
    function Add: IXMLSTD_MP_Title_Client;
    function Insert(const Index: Integer): IXMLSTD_MP_Title_Client;

    function Get_Item(Index: Integer): IXMLSTD_MP_Title_Client;
    property Items[Index: Integer]: IXMLSTD_MP_Title_Client read Get_Item; default;
  end;

{ IXMLSTD_MP_Title_Client_Person }

  IXMLSTD_MP_Title_Client_Person = interface(IXMLNode)
    ['{B10EBF25-400D-4181-A07B-1AD8AF7F937D}']
    { Property Accessors }
    function Get_FIO: IXMLTFIO;
    { Methods & Properties }
    property FIO: IXMLTFIO read Get_FIO;
  end;

{ IXMLSTD_MP_Title_Client_Organization }

  IXMLSTD_MP_Title_Client_Organization = interface(IXMLNode)
    ['{46C11A0A-B209-4517-AB1C-B98B87A33C1C}']
    { Property Accessors }
    function Get_Name: UnicodeString;
    function Get_Agent: IXMLSTD_MP_Title_Client_Organization_Agent;
    procedure Set_Name(Value: UnicodeString);
    { Methods & Properties }
    property Name: UnicodeString read Get_Name write Set_Name;
    property Agent: IXMLSTD_MP_Title_Client_Organization_Agent read Get_Agent;
  end;

{ IXMLSTD_MP_Title_Client_Organization_Agent }

  IXMLSTD_MP_Title_Client_Organization_Agent = interface(IXMLTFIO)
    ['{AF0D3E79-78A0-44B7-972D-CB027DF62B52}']
    { Property Accessors }
    function Get_Appointment: UnicodeString;
    procedure Set_Appointment(Value: UnicodeString);
    { Methods & Properties }
    property Appointment: UnicodeString read Get_Appointment write Set_Appointment;
  end;

{ IXMLSTD_MP_Title_Client_Foreign_Organization }

  IXMLSTD_MP_Title_Client_Foreign_Organization = interface(IXMLNode)
    ['{60932E28-11C4-4DD9-B160-7AA210C5FB19}']
    { Property Accessors }
    function Get_Name: UnicodeString;
    function Get_Country: UnicodeString;
    function Get_Agent: IXMLSTD_MP_Title_Client_Organization_Agent; //TODO:L   IXMLSTD_MP_Title_Client_Foreign_Organization_Agent
    procedure Set_Name(Value: UnicodeString);
    procedure Set_Country(Value: UnicodeString);
    { Methods & Properties }
    property Name: UnicodeString read Get_Name write Set_Name;
    property Country: UnicodeString read Get_Country write Set_Country;
    property Agent: IXMLSTD_MP_Title_Client_Organization_Agent read Get_Agent;
  end;

{ IXMLSTD_MP_Title_Client_Foreign_Organization_Agent }

  IXMLSTD_MP_Title_Client_Foreign_Organization_Agent = interface(IXMLTFIO)
    ['{767C15EF-7969-48CF-8A68-7199AFAE6FBC}']
    { Property Accessors }
    function Get_Appointment: UnicodeString;
    procedure Set_Appointment(Value: UnicodeString);
    { Methods & Properties }
    property Appointment: UnicodeString read Get_Appointment write Set_Appointment;
  end;

{ IXMLSTD_MP_Title_Client_Governance }

  IXMLSTD_MP_Title_Client_Governance = interface(IXMLNode)
    ['{5277EC51-3E0B-4201-A69A-8D83C26E516B}']
    { Property Accessors }
    function Get_Name: UnicodeString;
    function Get_Agent: IXMLSTD_MP_Title_Client_Organization_Agent;
    procedure Set_Name(Value: UnicodeString);
    { Methods & Properties }
    property Name: UnicodeString read Get_Name write Set_Name;
    property Agent: IXMLSTD_MP_Title_Client_Organization_Agent read Get_Agent;
  end;

{ IXMLSTD_MP_Title_Client_Governance_Agent }

  IXMLSTD_MP_Title_Client_Governance_Agent = interface(IXMLTFIO)
    ['{F6DD9D4A-D887-41A8-991E-2B44D47A5A1F}']
    { Property Accessors }
    function Get_Appointment: UnicodeString;
    procedure Set_Appointment(Value: UnicodeString);
    { Methods & Properties }
    property Appointment: UnicodeString read Get_Appointment write Set_Appointment;
  end;

{ IXMLSTD_MP_Package }

  IXMLSTD_MP_Package = interface(IXMLNode)
    ['{9108D5FF-AAD7-40F4-B15E-673A5D63E765}']
    { Property Accessors }
    function Get_FormParcels: IXMLSTD_MP_Package_FormParcels;
    function Get_SpecifyParcel: IXMLSTD_MP_Package_SpecifyParcel;
    function Get_NewSubParcel: IXMLTNewSubParcelList;
    function Get_SpecifyParcelsApproximal: IXMLSTD_MP_Package_SpecifyParcelsApproximalList;
    { Methods & Properties }
    property FormParcels: IXMLSTD_MP_Package_FormParcels read Get_FormParcels;
    property SpecifyParcel: IXMLSTD_MP_Package_SpecifyParcel read Get_SpecifyParcel;
    property NewSubParcel: IXMLTNewSubParcelList read Get_NewSubParcel;
    property SpecifyParcelsApproximal: IXMLSTD_MP_Package_SpecifyParcelsApproximalList read Get_SpecifyParcelsApproximal;
  end;

{ IXMLSTD_MP_Package_FormParcels }

  IXMLSTD_MP_Package_FormParcels = interface(IXMLNode)
    ['{4F4F0792-10D6-459D-883F-654E1B0CA824}']
    { Property Accessors }
    function Get_Method: UnicodeString;
    function Get_NewParcel: IXMLTNewParcelList;
    function Get_ChangeParcel: IXMLTChangeParcelList;
    //function Get_SpecifyRelatedParcel: IXMLSTD_MP_Package_FormParcels_SpecifyRelatedParcelList;
    function Get_SpecifyRelatedParcel: IXMLSTD_MP_Package_SpecifyParcel_SpecifyRelatedParcelList;
    procedure Set_Method(Value: UnicodeString);
    { Methods & Properties }
    property Method: UnicodeString read Get_Method write Set_Method;
    property NewParcel: IXMLTNewParcelList read Get_NewParcel;
    property ChangeParcel: IXMLTChangeParcelList read Get_ChangeParcel;
    //property SpecifyRelatedParcel: IXMLSTD_MP_Package_FormParcels_SpecifyRelatedParcelList read Get_SpecifyRelatedParcel;
    property SpecifyRelatedParcel: IXMLSTD_MP_Package_SpecifyParcel_SpecifyRelatedParcelList read Get_SpecifyRelatedParcel;
  end;

{ IXMLTNewParcel }

  IXMLTNewParcel = interface(IXMLNode)
    ['{E43E8F57-65EB-48FD-B579-0EAF3B0F1EED}']
    { Property Accessors }
    function Get_Definition: UnicodeString;
    function Get_CadastralBlock: UnicodeString;
    function Get_Prev_CadastralNumbers: IXMLTNewParcel_Prev_CadastralNumbers;
    function Get_Providing_Pass_CadastralNumbers: IXMLTProviding_Pass_CadastralNumbers;
    function Get_Inner_CadastralNumbers: IXMLTInner_CadastralNumbers;
    function Get_Area: IXMLTArea;
    function Get_Location: IXMLTNewParcel_Location;
    function Get_Category: IXMLTCategory;
    function Get_NaturalObject: IXMLTNatural_Object;
    function Get_Utilization: IXMLTUtilization;
    function Get_SubParcels: IXMLTNewParcel_SubParcels;
    {$ifndef disableIXMLTNewParcel_Contours}
    function Get_Contours: IXMLTNewParcel_Contours;
    {$else}
    function Get_Contours: IXMLContours;
    {$endif}
    function Get_Entity_Spatial: IXMLEntity_Spatial;
    function Get_Min_Area: IXMLTArea_without_Innccuracy;
    function Get_Max_Area: IXMLTArea_without_Innccuracy;
    function Get_Note: UnicodeString;
    procedure Set_Definition(Value: UnicodeString);
    procedure Set_CadastralBlock(Value: UnicodeString);
    procedure Set_Note(Value: UnicodeString);
    { Methods & Properties }
    property Definition: UnicodeString read Get_Definition write Set_Definition;
    property CadastralBlock: UnicodeString read Get_CadastralBlock write Set_CadastralBlock;
    property Prev_CadastralNumbers: IXMLTNewParcel_Prev_CadastralNumbers read Get_Prev_CadastralNumbers;
    property Providing_Pass_CadastralNumbers: IXMLTProviding_Pass_CadastralNumbers read Get_Providing_Pass_CadastralNumbers;
    property Inner_CadastralNumbers: IXMLTInner_CadastralNumbers read Get_Inner_CadastralNumbers;
    property Area: IXMLTArea read Get_Area;
    property Location: IXMLTNewParcel_Location read Get_Location;
    property Category: IXMLTCategory read Get_Category;
    property NaturalObject: IXMLTNatural_Object read Get_NaturalObject;
    property Utilization: IXMLTUtilization read Get_Utilization;
    property SubParcels: IXMLTNewParcel_SubParcels read Get_SubParcels;
    {$ifndef disableIXMLTNewParcel_Contours}
    property Contours: IXMLTNewParcel_Contours read Get_Contours;
    {$else}
    property Contours: IXMLContours read Get_Contours;
    {$endif}
    property Entity_Spatial: IXMLEntity_Spatial read Get_Entity_Spatial;
    property Min_Area: IXMLTArea_without_Innccuracy read Get_Min_Area;
    property Max_Area: IXMLTArea_without_Innccuracy read Get_Max_Area;
    property Note: UnicodeString read Get_Note write Set_Note;
  end;

{ IXMLTNewParcelList }

  IXMLTNewParcelList = interface(IXMLNodeCollection)
    ['{24A0974E-1BC2-44D6-A2B3-5AF26DD7F074}']
    { Methods & Properties }
    function Add: IXMLTNewParcel;
    function Insert(const Index: Integer): IXMLTNewParcel;

    function Get_Item(Index: Integer): IXMLTNewParcel;
    property Items[Index: Integer]: IXMLTNewParcel read Get_Item; default;
  end;

{ IXMLTNewParcel_Prev_CadastralNumbers }

  IXMLTNewParcel_Prev_CadastralNumbers = interface(IXMLNodeCollection)
    ['{1FD4E8FD-8F8E-4680-AAEB-494ED5405429}']
    { Property Accessors }
    function Get_CadastralNumber(Index: Integer): UnicodeString;
    { Methods & Properties }
    function Add(const CadastralNumber: UnicodeString): IXMLNode;
    function Insert(const Index: Integer; const CadastralNumber: UnicodeString): IXMLNode;
    property CadastralNumber[Index: Integer]: UnicodeString read Get_CadastralNumber; default;
  end;

{ IXMLTProviding_Pass_CadastralNumbers }

  IXMLTProviding_Pass_CadastralNumbers = interface(IXMLNode)
    ['{0467AAA8-AD3F-45C8-B54A-ABC86950398F}']
    { Property Accessors }
    function Get_CadastralNumber: IXMLTProviding_Pass_CadastralNumbers_CadastralNumberList;
    function Get_Definition: IXMLString_List;
    function Get_Other: UnicodeString;
    function Get_Documents: IXMLTProviding_Pass_CadastralNumbers_Documents;
    procedure Set_Other(Value: UnicodeString);
    { Methods & Properties }
    property CadastralNumber: IXMLTProviding_Pass_CadastralNumbers_CadastralNumberList read Get_CadastralNumber;
    property Definition: IXMLString_List read Get_Definition;
    property Other: UnicodeString read Get_Other write Set_Other;
    property Documents: IXMLTProviding_Pass_CadastralNumbers_Documents read Get_Documents;
  end;

{ IXMLTProviding_Pass_CadastralNumbers_Documents }

  IXMLTProviding_Pass_CadastralNumbers_Documents = interface(IXMLNodeCollection)
    ['{822B3AEA-6194-4344-9221-4391B5CD6FEC}']
    { Property Accessors }
    function Get_Document(Index: Integer): IXMLTDocument;
    { Methods & Properties }
    function Add: IXMLTDocument;
    function Insert(const Index: Integer): IXMLTDocument;
    property Document[Index: Integer]: IXMLTDocument read Get_Document; default;
  end;

{ IXMLTDocument }

  IXMLTDocument = interface(IXMLNode)
    ['{80F26D6C-73B0-4712-9533-741895DD5D3B}']
    { Property Accessors }
    function Get_Code_Document: UnicodeString;
    function Get_Name: UnicodeString;
    function Get_Series: UnicodeString;
    function Get_Number: UnicodeString;
    function Get_Date: UnicodeString;
    function Get_IssueOrgan: UnicodeString;
    function Get_NumberReg: UnicodeString;
    function Get_DateReg: UnicodeString;
    function Get_Duration: IXMLDuration;
    function Get_Register: UnicodeString;
    function Get_Desc: UnicodeString;
    function Get_IssueOrgan_Code: UnicodeString;
    function Get_AppliedFiles: IXMLAppliedFiles;
    procedure Set_Code_Document(Value: UnicodeString);
    procedure Set_Name(Value: UnicodeString);
    procedure Set_Series(Value: UnicodeString);
    procedure Set_Number(Value: UnicodeString);
    procedure Set_Date(Value: UnicodeString);
    procedure Set_IssueOrgan(Value: UnicodeString);
    procedure Set_NumberReg(Value: UnicodeString);
    procedure Set_DateReg(Value: UnicodeString);
    procedure Set_Register(Value: UnicodeString);
    procedure Set_Desc(Value: UnicodeString);
    procedure Set_IssueOrgan_Code(Value: UnicodeString);
    { Methods & Properties }
    property Code_Document: UnicodeString read Get_Code_Document write Set_Code_Document;
    property Name: UnicodeString read Get_Name write Set_Name;
    property Series: UnicodeString read Get_Series write Set_Series;
    property Number: UnicodeString read Get_Number write Set_Number;
    property Date: UnicodeString read Get_Date write Set_Date;
    property IssueOrgan: UnicodeString read Get_IssueOrgan write Set_IssueOrgan;
    property NumberReg: UnicodeString read Get_NumberReg write Set_NumberReg;
    property DateReg: UnicodeString read Get_DateReg write Set_DateReg;
    property Duration: IXMLDuration read Get_Duration;
    property Register: UnicodeString read Get_Register write Set_Register;
    property Desc: UnicodeString read Get_Desc write Set_Desc;
    property IssueOrgan_Code: UnicodeString read Get_IssueOrgan_Code write Set_IssueOrgan_Code;
    property AppliedFiles: IXMLAppliedFiles read Get_AppliedFiles;
  end;

{ IXMLDuration }

  IXMLDuration = interface(IXMLNode)
    ['{B1CD7778-6C2E-45BC-955A-26C3FA656FFD}']
    { Property Accessors }
    function Get_Started: UnicodeString;
    function Get_Stopped: UnicodeString;
    procedure Set_Started(Value: UnicodeString);
    procedure Set_Stopped(Value: UnicodeString);
    { Methods & Properties }
    property Started: UnicodeString read Get_Started write Set_Started;
    property Stopped: UnicodeString read Get_Stopped write Set_Stopped;
  end;

{ IXMLAppliedFiles }

  IXMLAppliedFiles = interface(IXMLNodeCollection)
    ['{FB388E7F-42FE-4B9C-9AC1-83F2148E4A24}']
    { Property Accessors }
    function Get_AppliedFile(Index: Integer): IXMLTAppliedFile;
    { Methods & Properties }
    function Add: IXMLTAppliedFile;
    function Insert(const Index: Integer): IXMLTAppliedFile;
    property AppliedFile[Index: Integer]: IXMLTAppliedFile read Get_AppliedFile; default;
  end;

{ IXMLTAppliedFile }

  IXMLTAppliedFile = interface(IXMLNode)
    ['{EE9C9D68-0D82-4858-AF7F-842CA13CE381}']
    { Property Accessors }
    function Get_Kind: UnicodeString;
    function Get_Name: UnicodeString;
    procedure Set_Kind(Value: UnicodeString);
    procedure Set_Name(Value: UnicodeString);
    { Methods & Properties }
    property Kind: UnicodeString read Get_Kind write Set_Kind;
    property Name: UnicodeString read Get_Name write Set_Name;
  end;

{ IXMLTInner_CadastralNumbers }

  IXMLTInner_CadastralNumbers = interface(IXMLNode)
    ['{A59870C3-7BF6-48B4-A5BA-B10596344B56}']
    { Property Accessors }
    function Get_CadastralNumber: IXMLTInner_CadastralNumbers_CadastralNumberList;
    function Get_Number: IXMLString_List;
    { Methods & Properties }
    property CadastralNumber: IXMLTInner_CadastralNumbers_CadastralNumberList read Get_CadastralNumber;
    property Number: IXMLString_List read Get_Number;
  end;


{ IXMLTAreaNew }

  IXMLTAreaNew = interface(IXMLNode)
    ['{55A4BB38-A24D-46A0-95C8-0B9240F6EC91}']
    { Property Accessors }
    function Get_Area: LongWord;
    function Get_Unit_: UnicodeString;
    function Get_Innccuracy: LongWord;
    procedure Set_Area(Value: LongWord);
    procedure Set_Unit_(Value: UnicodeString);
    procedure Set_Innccuracy(Value: LongWord);
    { Methods & Properties }
    property Area: LongWord read Get_Area write Set_Area;
    property Unit_: UnicodeString read Get_Unit_ write Set_Unit_;
    property Innccuracy: LongWord read Get_Innccuracy write Set_Innccuracy;
  end;

{ IXMLTAddress }

  IXMLTAddress = interface(IXMLNode)
    ['{3B5DDE47-69A7-4C8E-A76E-114F08F716DE}']
    { Property Accessors }
    function Get_Code_OKATO: UnicodeString;
    function Get_Code_KLADR: UnicodeString;
    function Get_Postal_Code: UnicodeString;
    function Get_Region: UnicodeString;
    function Get_District: IXMLDistrict;
    function Get_City: IXMLCity;
    function Get_Urban_District: IXMLUrban_District;
    function Get_Soviet_Village: IXMLSoviet_Village;
    function Get_Locality: IXMLLocality;
    function Get_Street: IXMLStreet;
    function Get_Level1: IXMLLevel1;
    function Get_Level2: IXMLLevel2;
    function Get_Level3: IXMLLevel3;
    function Get_Apartment: IXMLApartment;
    function Get_Other: UnicodeString;
    function Get_Note: UnicodeString;
    procedure Set_Code_OKATO(Value: UnicodeString);
    procedure Set_Code_KLADR(Value: UnicodeString);
    procedure Set_Postal_Code(Value: UnicodeString);
    procedure Set_Region(Value: UnicodeString);
    procedure Set_Other(Value: UnicodeString);
    procedure Set_Note(Value: UnicodeString);
    { Methods & Properties }
    property Code_OKATO: UnicodeString read Get_Code_OKATO write Set_Code_OKATO;
    property Code_KLADR: UnicodeString read Get_Code_KLADR write Set_Code_KLADR;
    property Postal_Code: UnicodeString read Get_Postal_Code write Set_Postal_Code;
    property Region: UnicodeString read Get_Region write Set_Region;
    property District: IXMLDistrict read Get_District;
    property City: IXMLCity read Get_City;
    property Urban_District: IXMLUrban_District read Get_Urban_District;
    property Soviet_Village: IXMLSoviet_Village read Get_Soviet_Village;
    property Locality: IXMLLocality read Get_Locality;
    property Street: IXMLStreet read Get_Street;
    property Level1: IXMLLevel1 read Get_Level1;
    property Level2: IXMLLevel2 read Get_Level2;
    property Level3: IXMLLevel3 read Get_Level3;
    property Apartment: IXMLApartment read Get_Apartment;
    property Other: UnicodeString read Get_Other write Set_Other;
    property Note: UnicodeString read Get_Note write Set_Note;
  end;

{ IXMLDistrict }

  IXMLDistrict = interface(IXMLNode)
    ['{E4DDB76B-F513-4F6D-9975-3084B59B42AD}']
    { Property Accessors }
    function Get_Name: UnicodeString;
    function Get_Type_: UnicodeString;
    procedure Set_Name(Value: UnicodeString);
    procedure Set_Type_(Value: UnicodeString);
    { Methods & Properties }
    property Name: UnicodeString read Get_Name write Set_Name;
    property Type_: UnicodeString read Get_Type_ write Set_Type_;
  end;

{ IXMLCity }

  IXMLCity = interface(IXMLNode)
    ['{4CCEAEF7-6FA5-47F3-988A-9AF03A37FA4A}']
    { Property Accessors }
    function Get_Name: UnicodeString;
    function Get_Type_: UnicodeString;
    procedure Set_Name(Value: UnicodeString);
    procedure Set_Type_(Value: UnicodeString);
    { Methods & Properties }
    property Name: UnicodeString read Get_Name write Set_Name;
    property Type_: UnicodeString read Get_Type_ write Set_Type_;
  end;

{ IXMLUrban_District }

  IXMLUrban_District = interface(IXMLNode)
    ['{B1237F62-B17A-4379-B466-AB6F19698D1E}']
    { Property Accessors }
    function Get_Name: UnicodeString;
    function Get_Type_: UnicodeString;
    procedure Set_Name(Value: UnicodeString);
    procedure Set_Type_(Value: UnicodeString);
    { Methods & Properties }
    property Name: UnicodeString read Get_Name write Set_Name;
    property Type_: UnicodeString read Get_Type_ write Set_Type_;
  end;

{ IXMLSoviet_Village }

  IXMLSoviet_Village = interface(IXMLNode)
    ['{C41156ED-0DE7-444E-96C0-AAADA622EEE5}']
    { Property Accessors }
    function Get_Name: UnicodeString;
    function Get_Type_: UnicodeString;
    procedure Set_Name(Value: UnicodeString);
    procedure Set_Type_(Value: UnicodeString);
    { Methods & Properties }
    property Name: UnicodeString read Get_Name write Set_Name;
    property Type_: UnicodeString read Get_Type_ write Set_Type_;
  end;

{ IXMLLocality }

  IXMLLocality = interface(IXMLNode)
    ['{2044E684-2708-4EEC-AEF1-88A50036C445}']
    { Property Accessors }
    function Get_Name: UnicodeString;
    function Get_Type_: UnicodeString;
    procedure Set_Name(Value: UnicodeString);
    procedure Set_Type_(Value: UnicodeString);
    { Methods & Properties }
    property Name: UnicodeString read Get_Name write Set_Name;
    property Type_: UnicodeString read Get_Type_ write Set_Type_;
  end;

{ IXMLStreet }

  IXMLStreet = interface(IXMLNode)
    ['{6098FA96-783F-4EF3-8A74-5743A4C2B61C}']
    { Property Accessors }
    function Get_Name: UnicodeString;
    function Get_Type_: UnicodeString;
    procedure Set_Name(Value: UnicodeString);
    procedure Set_Type_(Value: UnicodeString);
    { Methods & Properties }
    property Name: UnicodeString read Get_Name write Set_Name;
    property Type_: UnicodeString read Get_Type_ write Set_Type_;
  end;

{ IXMLLevel1 }

  IXMLLevel1 = interface(IXMLNode)
    ['{26373B64-09F0-4EA4-9301-9404846CFF94}']
    { Property Accessors }
    function Get_Type_: UnicodeString;
    function Get_Value: UnicodeString;
    procedure Set_Type_(Value: UnicodeString);
    procedure Set_Value(Value: UnicodeString);
    { Methods & Properties }
    property Type_: UnicodeString read Get_Type_ write Set_Type_;
    property Value: UnicodeString read Get_Value write Set_Value;
  end;

{ IXMLLevel2 }

  IXMLLevel2 = interface(IXMLNode)
    ['{105913B4-E45A-49A6-97BA-365521468AD6}']
    { Property Accessors }
    function Get_Type_: UnicodeString;
    function Get_Value: UnicodeString;
    procedure Set_Type_(Value: UnicodeString);
    procedure Set_Value(Value: UnicodeString);
    { Methods & Properties }
    property Type_: UnicodeString read Get_Type_ write Set_Type_;
    property Value: UnicodeString read Get_Value write Set_Value;
  end;

{ IXMLLevel3 }

  IXMLLevel3 = interface(IXMLNode)
    ['{8B3D9B5E-9A77-493B-ACDB-52999D31807A}']
    { Property Accessors }
    function Get_Type_: UnicodeString;
    function Get_Value: UnicodeString;
    procedure Set_Type_(Value: UnicodeString);
    procedure Set_Value(Value: UnicodeString);
    { Methods & Properties }
    property Type_: UnicodeString read Get_Type_ write Set_Type_;
    property Value: UnicodeString read Get_Value write Set_Value;
  end;

{ IXMLApartment }

  IXMLApartment = interface(IXMLNode)
    ['{5A404C4D-E1E2-48BC-AC5E-B580FCCED1AF}']
    { Property Accessors }
    function Get_Type_: UnicodeString;
    function Get_Value: UnicodeString;
    procedure Set_Type_(Value: UnicodeString);
    procedure Set_Value(Value: UnicodeString);
    { Methods & Properties }
    property Type_: UnicodeString read Get_Type_ write Set_Type_;
    property Value: UnicodeString read Get_Value write Set_Value;
  end;

{ IXMLTNewParcel_Location }

  IXMLTNewParcel_Location = interface(IXMLTAddress)
    ['{879BE7AE-9112-4D86-A5E3-D736C3AA20CA}']
    { Property Accessors }
    function Get_Document: IXMLTDocument;
    { Methods & Properties }
    property Document: IXMLTDocument read Get_Document;
  end;

{ IXMLTCategory }

  IXMLTCategory = interface(IXMLNode)
    ['{3F86A107-3083-4B82-93E3-440E1A648855}']
    { Property Accessors }
    function Get_Category: UnicodeString;
    function Get_DocCategory: IXMLTDocument;
    procedure Set_Category(Value: UnicodeString);
    { Methods & Properties }
    property Category: UnicodeString read Get_Category write Set_Category;
    property DocCategory: IXMLTDocument read Get_DocCategory;
  end;

{ IXMLTNatural_Object }

  IXMLTNatural_Object = interface(IXMLNode)
    ['{4ADB39BD-C802-49F9-9BC6-53FF083678EB}']
    { Property Accessors }
    function Get_Name: UnicodeString;
    function Get_ForestUse: UnicodeString;
    function Get_Type_ProtectiveForest: UnicodeString;
    procedure Set_Name(Value: UnicodeString);
    procedure Set_ForestUse(Value: UnicodeString);
    procedure Set_Type_ProtectiveForest(Value: UnicodeString);
    { Methods & Properties }
    property Name: UnicodeString read Get_Name write Set_Name;
    property ForestUse: UnicodeString read Get_ForestUse write Set_ForestUse;
    property Type_ProtectiveForest: UnicodeString read Get_Type_ProtectiveForest write Set_Type_ProtectiveForest;
  end;

{ IXMLTUtilization }

  IXMLTUtilization = interface(IXMLNode)
    ['{920ACE71-3114-4C40-B5AE-CC5FC1424FC9}']
    { Property Accessors }
    function Get_Utilization: UnicodeString;
    function Get_ByDoc: UnicodeString;
    function Get_AdditionalName: UnicodeString;
    function Get_DocUtilization: IXMLTDocument;
    procedure Set_Utilization(Value: UnicodeString);
    procedure Set_ByDoc(Value: UnicodeString);
    procedure Set_AdditionalName(Value: UnicodeString);
    { Methods & Properties }
    property Utilization: UnicodeString read Get_Utilization write Set_Utilization;
    property ByDoc: UnicodeString read Get_ByDoc write Set_ByDoc;
    property AdditionalName: UnicodeString read Get_AdditionalName write Set_AdditionalName;
    property DocUtilization: IXMLTDocument read Get_DocUtilization;
  end;

{ IXMLTNewParcel_SubParcels }

  IXMLTNewParcel_SubParcels = interface(IXMLNodeCollection)
    ['{86E79A6F-4026-4DCE-BCD0-63D5F0D968B4}']
    { Property Accessors }
    function Get_FormSubParcel(Index: Integer): IXMLTNewParcel_SubParcels_FormSubParcel;
    { Methods & Properties }
    function Add: IXMLTNewParcel_SubParcels_FormSubParcel;
    function Insert(const Index: Integer): IXMLTNewParcel_SubParcels_FormSubParcel;
    property FormSubParcel[Index: Integer]: IXMLTNewParcel_SubParcels_FormSubParcel read Get_FormSubParcel; default;
  end;

{ IXMLTSubParcel }

  IXMLTSubParcel = interface(IXMLNode)
    ['{2C4250C0-08E0-48F1-B09C-D3B2D02986CA}']
    { Property Accessors }
    function Get_SubParcel_Realty: Boolean;
    function Get_Area: IXMLTArea;
    function Get_Encumbrance: IXMLTEncumbrance;
    function Get_Entity_Spatial: IXMLEntity_Spatial;
    function Get_Contours: IXMLTSubParcel_Contours;
    procedure Set_SubParcel_Realty(Value: Boolean);
    { Methods & Properties }
    property SubParcel_Realty: Boolean read Get_SubParcel_Realty write Set_SubParcel_Realty;
    property Area: IXMLTArea read Get_Area;
    property Encumbrance: IXMLTEncumbrance read Get_Encumbrance;
    property Entity_Spatial: IXMLEntity_Spatial read Get_Entity_Spatial;
    property Contours: IXMLTSubParcel_Contours read Get_Contours;
  end;

{ IXMLTArea }

  IXMLTArea = interface(IXMLNode)
    ['{D0BE2463-D029-4C77-9790-FF0A02E84620}']
    { Property Accessors }
    function Get_Area: UnicodeString;
    function Get_Unit_: UnicodeString;
    function Get_Innccuracy: LongWord;
    procedure Set_Area(Value: UnicodeString);
    procedure Set_Unit_(Value: UnicodeString);
    procedure Set_Innccuracy(Value: LongWord);
    { Methods & Properties }
    property Area: UnicodeString read Get_Area write Set_Area;
    property Unit_: UnicodeString read Get_Unit_ write Set_Unit_;
    property Innccuracy: LongWord read Get_Innccuracy write Set_Innccuracy;
  end;

{ IXMLTEncumbrance }

  IXMLTEncumbrance = interface(IXMLNode)
    ['{6C61B751-7D39-45A9-8669-108A7A6A89B8}']
    { Property Accessors }
    function Get_Name: UnicodeString;
    function Get_Type_: UnicodeString;
    function Get_CadastralNumber_Restriction: UnicodeString;
    function Get_Documents: IXMLTEncumbrance_Documents; //IXMLTDEncumbrance_Documents;
    procedure Set_Name(Value: UnicodeString);
    procedure Set_Type_(Value: UnicodeString);
    procedure Set_CadastralNumber_Restriction(Value: UnicodeString);
    { Methods & Properties }
    property Name: UnicodeString read Get_Name write Set_Name;
    property Type_: UnicodeString read Get_Type_ write Set_Type_;
    property CadastralNumber_Restriction: UnicodeString read Get_CadastralNumber_Restriction write Set_CadastralNumber_Restriction;
    property Documents: IXMLTEncumbrance_Documents read Get_Documents;
  end;

{ IXMLTEncumbrance_Documents }

  IXMLTEncumbrance_Documents = interface(IXMLNodeCollection)
    ['{03BD6809-1B47-42B9-A7FF-D3B0F5183642}']
    { Property Accessors }
    function Get_Document(Index: Integer): IXMLTDocument;
    { Methods & Properties }
    function Add: IXMLTDocument;
    function Insert(const Index: Integer): IXMLTDocument;
    property Document[Index: Integer]: IXMLTDocument read Get_Document; default;
  end;

{ IXMLEntity_Spatial }

  IXMLEntity_Spatial = interface(IXMLNode)
    ['{9573CBB8-3505-4E2B-89B5-1B10C20019AE}']
    { Property Accessors }
    function Get_Ent_Sys: UnicodeString;
    function Get_Spatial_Element: IXMLTSPATIAL_ELEMENTList;
    function Get_Borders: IXMLEntity_Spatial_Borders;
    procedure Set_Ent_Sys(Value: UnicodeString);
    { Methods & Properties }
    property Ent_Sys: UnicodeString read Get_Ent_Sys write Set_Ent_Sys;
    property Spatial_Element: IXMLTSPATIAL_ELEMENTList read Get_Spatial_Element;
    property Borders: IXMLEntity_Spatial_Borders read Get_Borders;
  end;

{ IXMLTSPATIAL_ELEMENT_OLD_NEW }

  IXMLTSPATIAL_ELEMENT = interface(IXMLNodeCollection)
    ['{750941D1-FFE6-4BFB-A4AF-2DF5BD2EB288}']
    { Property Accessors }
    function Get_Spelement_Unit(Index: Integer): IXMLTSPELEMENT_UNIT;
    { Methods & Properties }
    function Add: IXMLTSPELEMENT_UNIT;
    function Insert(const Index: Integer): IXMLTSPELEMENT_UNIT;
    property Spelement_Unit[Index: Integer]: IXMLTSPELEMENT_UNIT read Get_Spelement_Unit; default;
  end;

{ IXMLTSPATIAL_ELEMENTList }

  IXMLTSPATIAL_ELEMENTList = interface(IXMLNodeCollection)
    ['{D620BC3E-0C88-437B-86AD-F7DA826FEC22}']
    { Methods & Properties }
    function Add: IXMLTSPATIAL_ELEMENT;
    function Insert(const Index: Integer): IXMLTSPATIAL_ELEMENT;

    function Get_Item(Index: Integer): IXMLTSPATIAL_ELEMENT;
    property Items[Index: Integer]: IXMLTSPATIAL_ELEMENT read Get_Item; default;
  end;

{ IXMLTSPELEMENT_UNIT }

  IXMLTSPELEMENT_UNIT = interface(IXMLNode)
    ['{503487E8-62F4-4CA6-9BBD-C1019EA0FF14}']
    { Property Accessors }
    function Get_Type_Unit: UnicodeString;
    function Get_NewOrdinate: IXMLTOrdinate;
    function Get_OldOrdinate: IXMLTOrdinate;
    procedure Set_Type_Unit(Value: UnicodeString);
    { Methods & Properties }
    property Type_Unit: UnicodeString read Get_Type_Unit write Set_Type_Unit;
    property NewOrdinate: IXMLTOrdinate read Get_NewOrdinate;
    property OldOrdinate: IXMLTOrdinate read Get_OldOrdinate;
  end;

{ IXMLTOrdinate }

  IXMLTOrdinate = interface(IXMLNode)
    ['{0FE721BD-2AFB-43B7-A815-6D695F008A0A}']
    { Property Accessors }
    function Get_X: UnicodeString;
    function Get_Y: UnicodeString;
    function Get_Num_Geopoint: LongWord;
    function Get_Geopoint_Zacrep: UnicodeString;
    function Get_Delta_Geopoint: UnicodeString;
    function Get_Point_Pref: UnicodeString;
    procedure Set_X(Value: UnicodeString);
    procedure Set_Y(Value: UnicodeString);
    procedure Set_Num_Geopoint(Value: LongWord);
    procedure Set_Geopoint_Zacrep(Value: UnicodeString);
    procedure Set_Delta_Geopoint(Value: UnicodeString);
    procedure Set_Point_Pref(Value: UnicodeString);
    { Methods & Properties }
    property X: UnicodeString read Get_X write Set_X;
    property Y: UnicodeString read Get_Y write Set_Y;
    property Num_Geopoint: LongWord read Get_Num_Geopoint write Set_Num_Geopoint;
    property Geopoint_Zacrep: UnicodeString read Get_Geopoint_Zacrep write Set_Geopoint_Zacrep;
    property Delta_Geopoint: UnicodeString read Get_Delta_Geopoint write Set_Delta_Geopoint;
    property Point_Pref: UnicodeString read Get_Point_Pref write Set_Point_Pref;
  end;

{ IXMLEntity_Spatial_Borders }

  IXMLEntity_Spatial_Borders = interface(IXMLNodeCollection)
    ['{3EBFA963-2BD4-4664-860A-187E0A7D4B1B}']
    { Property Accessors }
    function Get_Border(Index: Integer): IXMLEntity_Spatial_Borders_Border;
    { Methods & Properties }
    function Add: IXMLEntity_Spatial_Borders_Border;
    function Insert(const Index: Integer): IXMLEntity_Spatial_Borders_Border;
    property Border[Index: Integer]: IXMLEntity_Spatial_Borders_Border read Get_Border; default;
  end;

{ IXMLTBorder }

  IXMLTBorder = interface(IXMLNode)
    ['{B6E8FD3F-BCAD-4295-84EE-BB1AA0EB6D87}']
    { Property Accessors }
    function Get_Spatial: Integer;
    function Get_Point1: Integer;
    function Get_Point2: Integer;
    function Get_ByDef: Boolean;
    function Get_Edge: IXMLTBorder_Edge;
    procedure Set_Spatial(Value: Integer);
    procedure Set_Point1(Value: Integer);
    procedure Set_Point2(Value: Integer);
    procedure Set_ByDef(Value: Boolean);
    { Methods & Properties }
    property Spatial: Integer read Get_Spatial write Set_Spatial;
    property Point1: Integer read Get_Point1 write Set_Point1;
    property Point2: Integer read Get_Point2 write Set_Point2;
    property ByDef: Boolean read Get_ByDef write Set_ByDef;
    property Edge: IXMLTBorder_Edge read Get_Edge;
  end;

{ IXMLTBorder_Edge }

  IXMLTBorder_Edge = interface(IXMLNode)
    ['{727DEBC7-2D2C-40E2-9F94-2216BC774DB3}']
    { Property Accessors }
    function Get_Length: UnicodeString;
    function Get_Definition: UnicodeString;
    procedure Set_Length(Value: UnicodeString);
    procedure Set_Definition(Value: UnicodeString);
    { Methods & Properties }
    property Length: UnicodeString read Get_Length write Set_Length;
    property Definition: UnicodeString read Get_Definition write Set_Definition;
  end;

{ IXMLEntity_Spatial_Borders_Border }

  IXMLEntity_Spatial_Borders_Border = interface(IXMLTBorder)
    ['{AFBB95F8-C912-40DA-88DF-32079119896F}']
  end;

{ IXMLTSubParcel_Contours }

  IXMLTSubParcel_Contours = interface(IXMLNodeCollection)
    ['{D670A4F5-13E4-4534-8F08-14BF012EFB66}']
    { Property Accessors }
    function Get_Contour(Index: Integer): IXMLTSubParcel_Contours_Contour;
    { Methods & Properties }
    function Add: IXMLTSubParcel_Contours_Contour;
    function Insert(const Index: Integer): IXMLTSubParcel_Contours_Contour;
    property Contour[Index: Integer]: IXMLTSubParcel_Contours_Contour read Get_Contour; default;
  end;

{ IXMLTSubParcel_Contours_Contour }

  IXMLTSubParcel_Contours_Contour = interface(IXMLNode)
    ['{BE14D836-9E96-4463-BB1B-3E14234745C8}']
    { Property Accessors }
    function Get_Number: UnicodeString;
    function Get_Area: IXMLTArea;//TODO:L:
    function Get_Entity_Spatial: IXMLEntity_Spatial; //TODO:L
    procedure Set_Number(Value: UnicodeString);
    { Methods & Properties }
    property Number: UnicodeString read Get_Number write Set_Number;
    property Area: IXMLTArea read Get_Area;////TODO:L:
    property Entity_Spatial: IXMLEntity_Spatial read Get_Entity_Spatial;
  end;

{ IXMLTArea_Contour }

  IXMLTArea_Contour = interface(IXMLNode)
    ['{F1468606-ACC9-45EB-A7E5-DAA08F0FE371}']
    { Property Accessors }
    function Get_Area: UnicodeString;
    function Get_Unit_: UnicodeString;
    function Get_Innccuracy: LongWord;
    procedure Set_Area(Value: UnicodeString);
    procedure Set_Unit_(Value: UnicodeString);
    procedure Set_Innccuracy(Value: LongWord);
    { Methods & Properties }
    property Area: UnicodeString read Get_Area write Set_Area;
    property Unit_: UnicodeString read Get_Unit_ write Set_Unit_;
    property Innccuracy: LongWord read Get_Innccuracy write Set_Innccuracy;
  end;

{ IXMLTSubParcel_Contours_Contour_Entity_Spatial }

  IXMLTSubParcel_Contours_Contour_Entity_Spatial = interface(IXMLNodeCollection)
    ['{B741EB04-B2A2-445F-9CCA-7F1AA541D6C9}']
    { Property Accessors }
    function Get_Ent_Sys: UnicodeString;
    function Get_Spatial_Element(Index: Integer): IXMLTSPATIAL_ELEMENT;
    procedure Set_Ent_Sys(Value: UnicodeString);
    { Methods & Properties }
    function Add: IXMLTSPATIAL_ELEMENT;
    function Insert(const Index: Integer): IXMLTSPATIAL_ELEMENT;
    property Ent_Sys: UnicodeString read Get_Ent_Sys write Set_Ent_Sys;
    property Spatial_Element[Index: Integer]: IXMLTSPATIAL_ELEMENT read Get_Spatial_Element; default;
  end;

{ IXMLTNewParcel_SubParcels_FormSubParcel }

  IXMLTNewParcel_SubParcels_FormSubParcel = interface(IXMLTSubParcel)
    ['{C0FE4AA5-69C2-4FBB-80C6-C99157BC0E95}']
    { Property Accessors }
    function Get_Definition: UnicodeString;
    procedure Set_Definition(Value: UnicodeString);
    { Methods & Properties }
    property Definition: UnicodeString read Get_Definition write Set_Definition;
  end;

{$ifndef disableIXMLTNewParcel_Contours}
{ IXMLTNewParcel_Contours }

  IXMLTNewParcel_Contours = interface(IXMLNodeCollection)
    ['{03786BB8-D15A-4461-A617-82C51E24CE2B}']
    { Property Accessors }
    function Get_NewContour(Index: Integer): IXMLTNewParcel_Contours_NewContour;
    { Methods & Properties }
    function Add: IXMLTNewParcel_Contours_NewContour;
    function Insert(const Index: Integer): IXMLTNewParcel_Contours_NewContour;
    property NewContour[Index: Integer]: IXMLTNewParcel_Contours_NewContour read Get_NewContour; default;
  end;
{$endif}

{ IXMLTContour }

  IXMLTContour = interface(IXMLNode)
    ['{1C858DBF-7B82-4D29-B256-97E68C489ECB}']
    { Property Accessors }
    function Get_Area: IXMLTArea; //TODO:L:
    function Get_Entity_Spatial: IXMLEntity_Spatial;
    { Methods & Properties }
    property Area: IXMLTArea read Get_Area; //TODO:L:
    property Entity_Spatial: IXMLEntity_Spatial read Get_Entity_Spatial;
  end;

{ IXMLTNewParcel_Contours_NewContour }

  IXMLTNewParcel_Contours_NewContour = interface(IXMLTContour)
    ['{60826AAE-03C4-4E17-A078-0AF9BEF662E8}']
    { Property Accessors }
    function Get_Definition: UnicodeString;
    function Get_Providing_Pass_CadastralNumbers: IXMLTProviding_Pass_CadastralNumbers;
    procedure Set_Definition(Value: UnicodeString);
    { Methods & Properties }
    property Definition: UnicodeString read Get_Definition write Set_Definition;
    property Providing_Pass_CadastralNumbers: IXMLTProviding_Pass_CadastralNumbers read Get_Providing_Pass_CadastralNumbers;
  end;

{ IXMLTArea_without_Innccuracy }

  IXMLTArea_without_Innccuracy = interface(IXMLNode)
    ['{E1CD9098-DD4E-45B1-9EC5-58A79E86F206}']
    { Property Accessors }
    function Get_Area: LongWord;
    function Get_Unit_: UnicodeString;
    procedure Set_Area(Value: LongWord);
    procedure Set_Unit_(Value: UnicodeString);
    { Methods & Properties }
    property Area: LongWord read Get_Area write Set_Area;
    property Unit_: UnicodeString read Get_Unit_ write Set_Unit_;
  end;

{ IXMLTChangeParcel }

  IXMLTChangeParcel = interface(IXMLNode)
    ['{FAA019D8-2AF4-4874-8DCE-93ABE208F4EA}']
    { Property Accessors }
    function Get_CadastralNumber: UnicodeString;
    function Get_CadastralBlock: UnicodeString;
    function Get_Providing_Pass_CadastralNumbers: IXMLTProviding_Pass_CadastralNumbers;
    function Get_Inner_CadastralNumbers: IXMLTChangeParcel_Inner_CadastralNumbers;
    {$IFNDEF disableIXMLTChangeParcel_SubParcels}
    function Get_SubParcels: IXMLTChangeParcel_SubParcels;
    {$else}
    function Get_SubParcels: IXMLTExistParcel_SubParcels;
    {$ENDIF}
  
    function Get_DeleteEntryParcels: IXMLTChangeParcel_DeleteEntryParcels;
    function Get_Note: UnicodeString;
    procedure Set_CadastralNumber(Value: UnicodeString);
    procedure Set_CadastralBlock(Value: UnicodeString);
    procedure Set_Note(Value: UnicodeString);
    { Methods & Properties }
    property CadastralNumber: UnicodeString read Get_CadastralNumber write Set_CadastralNumber;
    property CadastralBlock: UnicodeString read Get_CadastralBlock write Set_CadastralBlock;
    property Providing_Pass_CadastralNumbers: IXMLTProviding_Pass_CadastralNumbers read Get_Providing_Pass_CadastralNumbers;
    property Inner_CadastralNumbers: IXMLTChangeParcel_Inner_CadastralNumbers read Get_Inner_CadastralNumbers;
    {$IFNDEF  disableIXMLTChangeParcel_SubParcels}
    property SubParcels: IXMLTChangeParcel_SubParcels read Get_SubParcels;
    {$ELSE}
    property SubParcels: IXMLTExistParcel_SubParcels read Get_SubParcels;
    {$ENDIF} 
    property DeleteEntryParcels: IXMLTChangeParcel_DeleteEntryParcels read Get_DeleteEntryParcels;
    property Note: UnicodeString read Get_Note write Set_Note;
  end;

{ IXMLTChangeParcelList }

  IXMLTChangeParcelList = interface(IXMLNodeCollection)
    ['{1633CF33-30E0-4A71-A250-88F00D07D665}']
    { Methods & Properties }
    function Add: IXMLTChangeParcel;
    function Insert(const Index: Integer): IXMLTChangeParcel;

    function Get_Item(Index: Integer): IXMLTChangeParcel;
    property Items[Index: Integer]: IXMLTChangeParcel read Get_Item; default;
  end;

{ IXMLTChangeParcel_Inner_CadastralNumbers }

  IXMLTChangeParcel_Inner_CadastralNumbers = interface(IXMLNode)
    ['{A8E4CFD1-DADD-4EAA-9118-1C770B627517}']
    { Property Accessors }
    function Get_CadastralNumber: IXMLTChangeParcel_Inner_CadastralNumbers_CadastralNumberList;
    function Get_Number: IXMLString_List;
    { Methods & Properties }
    property CadastralNumber: IXMLTChangeParcel_Inner_CadastralNumbers_CadastralNumberList read Get_CadastralNumber;
    property Number: IXMLString_List read Get_Number;
  end;

  {$IFNDEF  disableIXMLTChangeParcel_SubParcels} 
{ IXMLTChangeParcel_SubParcels }

  IXMLTChangeParcel_SubParcels = interface(IXMLNode)
    ['{DAEA3BFE-BDC7-45C5-8F3F-30490C3E2E6F}']
    { Property Accessors }
    function Get_FormSubParcel: IXMLTChangeParcel_SubParcels_FormSubParcelList;
    function Get_ExistSubParcel: IXMLTChangeParcel_SubParcels_ExistSubParcelList;
    function Get_InvariableSubParcel: IXMLTChangeParcel_SubParcels_InvariableSubParcelList;
    { Methods & Properties }
    property FormSubParcel: IXMLTChangeParcel_SubParcels_FormSubParcelList read Get_FormSubParcel;
    property ExistSubParcel: IXMLTChangeParcel_SubParcels_ExistSubParcelList read Get_ExistSubParcel;
    property InvariableSubParcel: IXMLTChangeParcel_SubParcels_InvariableSubParcelList read Get_InvariableSubParcel;
  end;
  {$ENDIF}

{ IXMLTChangeParcel_SubParcels_FormSubParcel }

  IXMLTChangeParcel_SubParcels_FormSubParcel = interface(IXMLTSubParcel)
    ['{7F664F32-2BE5-42DD-89E8-C31A4B7FFF4B}']
    { Property Accessors }
    function Get_Definition: UnicodeString;
    procedure Set_Definition(Value: UnicodeString);
    { Methods & Properties }
    property Definition: UnicodeString read Get_Definition write Set_Definition;
  end;

{ IXMLTChangeParcel_SubParcels_FormSubParcelList }

  IXMLTChangeParcel_SubParcels_FormSubParcelList = interface(IXMLNodeCollection)
    ['{5C01253A-BACF-4758-806A-1C78CF16FE76}']
    { Methods & Properties }
    function Add: IXMLTChangeParcel_SubParcels_FormSubParcel;
    function Insert(const Index: Integer): IXMLTChangeParcel_SubParcels_FormSubParcel;

    function Get_Item(Index: Integer): IXMLTChangeParcel_SubParcels_FormSubParcel;
    property Items[Index: Integer]: IXMLTChangeParcel_SubParcels_FormSubParcel read Get_Item; default;
  end;

{ IXMLTChangeParcel_SubParcels_ExistSubParcel }

  IXMLTChangeParcel_SubParcels_ExistSubParcel = interface(IXMLTSubParcel)
    ['{55232749-17EF-4AC9-9B07-B1C9D18BC31D}']
    { Property Accessors }
    function Get_Number_Record: LongWord;
    function Get_CadastralNumber_EntryParcel: UnicodeString;
    procedure Set_Number_Record(Value: LongWord);
    procedure Set_CadastralNumber_EntryParcel(Value: UnicodeString);
    { Methods & Properties }
    property Number_Record: LongWord read Get_Number_Record write Set_Number_Record;
    property CadastralNumber_EntryParcel: UnicodeString read Get_CadastralNumber_EntryParcel write Set_CadastralNumber_EntryParcel;
  end;

{ IXMLTChangeParcel_SubParcels_ExistSubParcelList }

  IXMLTChangeParcel_SubParcels_ExistSubParcelList = interface(IXMLNodeCollection)
    ['{FC5854FA-F9FC-498E-AC44-A8C820BC6676}']
    { Methods & Properties }
    function Add: IXMLTChangeParcel_SubParcels_ExistSubParcel;
    function Insert(const Index: Integer): IXMLTChangeParcel_SubParcels_ExistSubParcel;

    function Get_Item(Index: Integer): IXMLTChangeParcel_SubParcels_ExistSubParcel;
    property Items[Index: Integer]: IXMLTChangeParcel_SubParcels_ExistSubParcel read Get_Item; default;
  end;

{ IXMLTChangeParcel_SubParcels_InvariableSubParcel }

  IXMLTChangeParcel_SubParcels_InvariableSubParcel = interface(IXMLNode)
    ['{FE3E30DD-68B9-47DD-A771-C8093ED5D3A4}']
    { Property Accessors }
    function Get_Number_Record: LongWord;
    function Get_SubParcel_Realty: Boolean;
    function Get_Area: IXMLTChangeParcel_SubParcels_InvariableSubParcel_Area;
    function Get_Encumbrance: IXMLTEncumbrance;
    function Get_Contours: IXMLTChangeParcel_SubParcels_InvariableSubParcel_Contours;
    procedure Set_Number_Record(Value: LongWord);
    procedure Set_SubParcel_Realty(Value: Boolean);
    { Methods & Properties }
    property Number_Record: LongWord read Get_Number_Record write Set_Number_Record;
    property SubParcel_Realty: Boolean read Get_SubParcel_Realty write Set_SubParcel_Realty;
    property Area: IXMLTChangeParcel_SubParcels_InvariableSubParcel_Area read Get_Area;
    property Encumbrance: IXMLTEncumbrance read Get_Encumbrance;
    property Contours: IXMLTChangeParcel_SubParcels_InvariableSubParcel_Contours read Get_Contours;
  end;

{ IXMLTChangeParcel_SubParcels_InvariableSubParcelList }

  IXMLTChangeParcel_SubParcels_InvariableSubParcelList = interface(IXMLNodeCollection)
    ['{5761EEB3-2FE9-4EB9-83E5-2108EEC0BB4C}']
    { Methods & Properties }
    function Add: IXMLTChangeParcel_SubParcels_InvariableSubParcel;
    function Insert(const Index: Integer): IXMLTChangeParcel_SubParcels_InvariableSubParcel;

    function Get_Item(Index: Integer): IXMLTChangeParcel_SubParcels_InvariableSubParcel;
    property Items[Index: Integer]: IXMLTChangeParcel_SubParcels_InvariableSubParcel read Get_Item; default;
  end;

{ IXMLTChangeParcel_SubParcels_InvariableSubParcel_Area }

  IXMLTChangeParcel_SubParcels_InvariableSubParcel_Area = interface(IXMLNode)
    ['{0E217AA0-F970-45E1-B3A6-994E9A3CD16D}']
    { Property Accessors }
    function Get_Area: LongWord;
    function Get_Unit_: UnicodeString;
    procedure Set_Area(Value: LongWord);
    procedure Set_Unit_(Value: UnicodeString);
    { Methods & Properties }
    property Area: LongWord read Get_Area write Set_Area;
    property Unit_: UnicodeString read Get_Unit_ write Set_Unit_;
  end;

{ IXMLTChangeParcel_SubParcels_InvariableSubParcel_Contours }

  IXMLTChangeParcel_SubParcels_InvariableSubParcel_Contours = interface(IXMLNodeCollection)
    ['{602FAB79-C507-4194-A9C7-AD0CD473F837}']
    { Property Accessors }
    function Get_Contour(Index: Integer): IXMLTChangeParcel_SubParcels_InvariableSubParcel_Contours_Contour;
    { Methods & Properties }
    function Add: IXMLTChangeParcel_SubParcels_InvariableSubParcel_Contours_Contour;
    function Insert(const Index: Integer): IXMLTChangeParcel_SubParcels_InvariableSubParcel_Contours_Contour;
    property Contour[Index: Integer]: IXMLTChangeParcel_SubParcels_InvariableSubParcel_Contours_Contour read Get_Contour; default;
  end;

{ IXMLTChangeParcel_SubParcels_InvariableSubParcel_Contours_Contour }

  IXMLTChangeParcel_SubParcels_InvariableSubParcel_Contours_Contour = interface(IXMLNode)
    ['{633E46A3-F4EC-4058-BCD6-EB9935C33D2A}']
    { Property Accessors }
    function Get_Number: UnicodeString;
    function Get_Area: IXMLTArea_Contour;
    procedure Set_Number(Value: UnicodeString);
    { Methods & Properties }
    property Number: UnicodeString read Get_Number write Set_Number;
    property Area: IXMLTArea_Contour read Get_Area;
  end;

{ IXMLTChangeParcel_DeleteEntryParcels }

  IXMLTChangeParcel_DeleteEntryParcels = interface(IXMLNodeCollection)
    ['{F356BAA5-B1F4-454E-A4E6-F827139492C8}']
    { Property Accessors }
    function Get_DeleteEntryParcel(Index: Integer): IXMLTChangeParcel_DeleteEntryParcels_DeleteEntryParcel;
    { Methods & Properties }
    function Add: IXMLTChangeParcel_DeleteEntryParcels_DeleteEntryParcel;
    function Insert(const Index: Integer): IXMLTChangeParcel_DeleteEntryParcels_DeleteEntryParcel;
    property DeleteEntryParcel[Index: Integer]: IXMLTChangeParcel_DeleteEntryParcels_DeleteEntryParcel read Get_DeleteEntryParcel; default;
  end;

{ IXMLTChangeParcel_DeleteEntryParcels_DeleteEntryParcel }

  IXMLTChangeParcel_DeleteEntryParcels_DeleteEntryParcel = interface(IXMLNode)
    ['{6F4F5663-57E4-4DCC-82AF-2E1CA895BC36}']
    { Property Accessors }
    function Get_CadastralNumber: UnicodeString;
    procedure Set_CadastralNumber(Value: UnicodeString);
    { Methods & Properties }
    property CadastralNumber: UnicodeString read Get_CadastralNumber write Set_CadastralNumber;
  end;

{ IXMLTSpecifyRelatedParcel }

  IXMLTSpecifyRelatedParcel = interface(IXMLNode)
    ['{59E4E018-AF02-464C-AE4B-9AA7773CDAAF}']
    { Property Accessors }
    function Get_CadastralNumber: UnicodeString;
    function Get_Number_Record: LongWord;
    function Get_AllBorder: IXMLTSpecifyRelatedParcel_AllBorder;
    function Get_ChangeBorder: IXMLTSpecifyRelatedParcel_ChangeBorderList;
    function Get_Contours: IXMLContours;
    function Get_DeleteAllBorder: IXMLTSpecifyRelatedParcel_DeleteAllBorderList;
    function Get_ExistSubParcels: IXMLTSpecifyRelatedParcel_ExistSubParcels;
    procedure Set_CadastralNumber(Value: UnicodeString);
    procedure Set_Number_Record(Value: LongWord);
    { Methods & Properties }
    property CadastralNumber: UnicodeString read Get_CadastralNumber write Set_CadastralNumber;
    property Number_Record: LongWord read Get_Number_Record write Set_Number_Record;
    property AllBorder: IXMLTSpecifyRelatedParcel_AllBorder read Get_AllBorder;
    property ChangeBorder: IXMLTSpecifyRelatedParcel_ChangeBorderList read Get_ChangeBorder;
    property Contours: IXMLContours read Get_Contours;    //XMLTSpecifyRelatedParcel_Contours
    property DeleteAllBorder: IXMLTSpecifyRelatedParcel_DeleteAllBorderList read Get_DeleteAllBorder;
    property ExistSubParcels: IXMLTSpecifyRelatedParcel_ExistSubParcels read Get_ExistSubParcels;
  end;

{ IXMLTSpecifyRelatedParcel_AllBorder }

  IXMLTSpecifyRelatedParcel_AllBorder = interface(IXMLNode)
    ['{6D9B0A24-6F60-4AEF-A1E6-79FE2842E0D3}']
    { Property Accessors }
    function Get_Entity_Spatial: IXMLEntity_Spatial;
    { Methods & Properties }
    property Entity_Spatial: IXMLEntity_Spatial read Get_Entity_Spatial;
  end;

{ IXMLTSpecifyRelatedParcel_ChangeBorder }

  IXMLTSpecifyRelatedParcel_ChangeBorder = interface(IXMLNode)
    ['{34EEFD1D-37E1-43F4-9B71-A21A291BD7A5}']
    { Property Accessors }
    function Get_OldOrdinate: IXMLTOrdinate;
    function Get_NewOrdinate: IXMLTOrdinate;
    { Methods & Properties }
    property OldOrdinate: IXMLTOrdinate read Get_OldOrdinate;
    property NewOrdinate: IXMLTOrdinate read Get_NewOrdinate;
  end;

  IXMLTChangeBorder = interface(IXMLNode)
    ['{34EEFD1D-37E1-43F4-9B71-A21A291BD7A5}']
    { Property Accessors }
    function Get_OldOrdinate: IXMLTOrdinate;
    function Get_NewOrdinate: IXMLTOrdinate;
    { Methods & Properties }
    property OldOrdinate: IXMLTOrdinate read Get_OldOrdinate;
    property NewOrdinate: IXMLTOrdinate read Get_NewOrdinate;
  end;


{ IXMLTSpecifyRelatedParcel_ChangeBorderList }

  IXMLTSpecifyRelatedParcel_ChangeBorderList = interface(IXMLNodeCollection)
    ['{F70A47A2-8AC5-48A7-A627-4C0B6A660E20}']
    { Methods & Properties }
    function Add: IXMLTChangeBorder;
    function Insert(const Index: Integer): IXMLTChangeBorder;

    function Get_Item(Index: Integer): IXMLTChangeBorder;
    property Items[Index: Integer]: IXMLTChangeBorder read Get_Item; default;
  end;

{ IXMLTSpecifyRelatedParcel_Contours }

  IXMLTSpecifyRelatedParcel_Contours = interface(IXMLNodeCollection)
    ['{8E61AC73-CA7D-4232-ADC5-A83413956F2E}']
    { Property Accessors }
    function Get_NewContour(Index: Integer): IXMLTSpecifyRelatedParcel_Contours_NewContour;
    { Methods & Properties }
    function Add: IXMLTSpecifyRelatedParcel_Contours_NewContour;
    function Insert(const Index: Integer): IXMLTSpecifyRelatedParcel_Contours_NewContour;
    property NewContour[Index: Integer]: IXMLTSpecifyRelatedParcel_Contours_NewContour read Get_NewContour; default;
  end;

{ IXMLTSpecifyRelatedParcel_Contours_NewContour }

  IXMLTSpecifyRelatedParcel_Contours_NewContour = interface(IXMLTContour)
    ['{3A430562-A0E4-4E91-8FAD-46E6B269F592}']
    { Property Accessors }
    function Get_Definition: UnicodeString;
    function Get_Providing_Pass_CadastralNumbers: IXMLTProviding_Pass_CadastralNumbers;
    procedure Set_Definition(Value: UnicodeString);
    { Methods & Properties }
    property Definition: UnicodeString read Get_Definition write Set_Definition;
    property Providing_Pass_CadastralNumbers: IXMLTProviding_Pass_CadastralNumbers read Get_Providing_Pass_CadastralNumbers;
  end;

{ IXMLTSpecifyRelatedParcel_DeleteAllBorder }

  IXMLTSpecifyRelatedParcel_DeleteAllBorder = interface(IXMLNode)
    ['{81567910-E379-404B-838F-1C6011AE732C}']
    { Property Accessors }
    function Get_OldOrdinate: IXMLTOrdinate;
    { Methods & Properties }
    property OldOrdinate: IXMLTOrdinate read Get_OldOrdinate;
  end;

{ IXMLTSpecifyRelatedParcel_DeleteAllBorderList }

  IXMLTSpecifyRelatedParcel_DeleteAllBorderList = interface(IXMLNodeCollection)
    ['{452DC931-2623-4ED9-BBAA-C0C8C15D13D4}']
    { Methods & Properties }
    function Add: IXMLTSpecifyRelatedParcel_DeleteAllBorder;
    function Insert(const Index: Integer): IXMLTSpecifyRelatedParcel_DeleteAllBorder;

    function Get_Item(Index: Integer): IXMLTSpecifyRelatedParcel_DeleteAllBorder;
    property Items[Index: Integer]: IXMLTSpecifyRelatedParcel_DeleteAllBorder read Get_Item; default;
  end;

{ IXMLTSpecifyRelatedParcel_ExistSubParcels }

  IXMLTSpecifyRelatedParcel_ExistSubParcels = interface(IXMLNodeCollection)
    ['{62068E97-1760-468B-9DEE-07D5B70430A0}']
    { Property Accessors }
    function Get_ExistSubParcel(Index: Integer): IXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel;
    { Methods & Properties }
    function Add: IXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel;
    function Insert(const Index: Integer): IXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel;
    property ExistSubParcel[Index: Integer]: IXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel read Get_ExistSubParcel; default;
  end;

{ IXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel }

  IXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel = interface(IXMLNode)
    ['{EB9F0EF2-74AC-4835-95D9-A1AE4012E457}']
    { Property Accessors }
    function Get_Number_Record: LongWord;
    function Get_Entity_Spatial: IXMLEntity_Spatial; //IXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Entity_Spatial;
    function Get_Contours: IXMLTSubParcel_Contours; //IXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Contours;
    procedure Set_Number_Record(Value: LongWord);
    { Methods & Properties }
    property Number_Record: LongWord read Get_Number_Record write Set_Number_Record;
    property Entity_Spatial: IXMLEntity_Spatial read Get_Entity_Spatial;
    property Contours: IXMLTSubParcel_Contours read Get_Contours;
  end;

{ IXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Entity_Spatial }

  IXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Entity_Spatial = interface(IXMLNodeCollection)
    ['{FF78F8E8-1EA9-4FA8-A0C3-5101DB527DE2}']
    { Property Accessors }
    function Get_Ent_Sys: UnicodeString;
    function Get_Spatial_Element(Index: Integer): IXMLTSPATIAL_ELEMENT;
    procedure Set_Ent_Sys(Value: UnicodeString);
    { Methods & Properties }
    function Add: IXMLTSPATIAL_ELEMENT;
    function Insert(const Index: Integer): IXMLTSPATIAL_ELEMENT;
    property Ent_Sys: UnicodeString read Get_Ent_Sys write Set_Ent_Sys;
    property Spatial_Element[Index: Integer]: IXMLTSPATIAL_ELEMENT read Get_Spatial_Element; default;
  end;

{ IXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Contours }

  IXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Contours = interface(IXMLNodeCollection)
    ['{CC5ED0B0-2501-491D-BA17-C1805E2A9E68}']
    { Property Accessors }
    function Get_Contour(Index: Integer): IXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Contours_Contour;
    { Methods & Properties }
    function Add: IXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Contours_Contour;
    function Insert(const Index: Integer): IXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Contours_Contour;
    property Contour[Index: Integer]: IXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Contours_Contour read Get_Contour; default;
  end;

{ IXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Contours_Contour }

  IXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Contours_Contour = interface(IXMLNode)
    ['{DB15F0CF-5EF3-4C7B-A413-FBF632C9E22C}']
    { Property Accessors }
    function Get_Number: LongWord;
    function Get_Entity_Spatial: IXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Contours_Contour_Entity_Spatial;
    procedure Set_Number(Value: LongWord);
    { Methods & Properties }
    property Number: LongWord read Get_Number write Set_Number;
    property Entity_Spatial: IXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Contours_Contour_Entity_Spatial read Get_Entity_Spatial;
  end;

{ IXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Contours_Contour_Entity_Spatial }

  IXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Contours_Contour_Entity_Spatial = interface(IXMLNodeCollection)
    ['{56EF14F4-ACEB-409E-92E5-8163641CA39D}']
    { Property Accessors }
    function Get_Ent_Sys: UnicodeString;
    function Get_Spatial_Element(Index: Integer): IXMLTSPATIAL_ELEMENT;
    procedure Set_Ent_Sys(Value: UnicodeString);
    { Methods & Properties }
    function Add: IXMLTSPATIAL_ELEMENT;
    function Insert(const Index: Integer): IXMLTSPATIAL_ELEMENT;
    property Ent_Sys: UnicodeString read Get_Ent_Sys write Set_Ent_Sys;
    property Spatial_Element[Index: Integer]: IXMLTSPATIAL_ELEMENT read Get_Spatial_Element; default;
  end;

{ IXMLSTD_MP_Package_FormParcels_SpecifyRelatedParcel }

  IXMLSTD_MP_Package_FormParcels_SpecifyRelatedParcel = interface(IXMLTSpecifyRelatedParcel)
    ['{66F922FC-3274-4521-B546-FE7C42AE944F}']
  end;

{ IXMLSTD_MP_Package_FormParcels_SpecifyRelatedParcelList }

  IXMLSTD_MP_Package_FormParcels_SpecifyRelatedParcelList = interface(IXMLNodeCollection)
    ['{31B230A7-A2DC-4F5A-8694-A59BBFE962DC}']
    { Methods & Properties }
    function Add: IXMLSTD_MP_Package_FormParcels_SpecifyRelatedParcel;
    function Insert(const Index: Integer): IXMLSTD_MP_Package_FormParcels_SpecifyRelatedParcel;

    function Get_Item(Index: Integer): IXMLSTD_MP_Package_FormParcels_SpecifyRelatedParcel;
    property Items[Index: Integer]: IXMLSTD_MP_Package_FormParcels_SpecifyRelatedParcel read Get_Item; default;
  end;

{ IXMLSTD_MP_Package_SpecifyParcel }

  IXMLSTD_MP_Package_SpecifyParcel = interface(IXMLNode)
    ['{9EEECA5D-30A5-4941-B44F-2131F2CB49F6}']
    { Property Accessors }
    function Get_ExistParcel: IXMLTExistParcel;
    function Get_ExistEZ: IXMLSTD_MP_Package_SpecifyParcel_ExistEZ;
    function Get_SpecifyRelatedParcel: IXMLSTD_MP_Package_SpecifyParcel_SpecifyRelatedParcelList;
    { Methods & Properties }
    property ExistParcel: IXMLTExistParcel read Get_ExistParcel;
    property ExistEZ: IXMLSTD_MP_Package_SpecifyParcel_ExistEZ read Get_ExistEZ;
    property SpecifyRelatedParcel: IXMLSTD_MP_Package_SpecifyParcel_SpecifyRelatedParcelList read Get_SpecifyRelatedParcel;
  end;

{ IXMLTExistParcel }

  IXMLTExistParcel = interface(IXMLNode)
    ['{939B24DE-39AB-431C-A53C-058175B90364}']
    { Property Accessors }
    function Get_CadastralNumber: UnicodeString;
    function Get_CadastralBlock: UnicodeString;
    function Get_Inner_CadastralNumbers: IXMLTInner_CadastralNumbers;
    function Get_Area: IXMLTArea;
    function Get_SubParcels: IXMLTExistParcel_SubParcels;
    function Get_Entity_Spatial: IXMLEntity_Spatial;
    function Get_Contours: IXMLContours;
    function Get_Area_In_GKN: LongWord;
    function Get_Delta_Area: LongWord;
    function Get_Min_Area: IXMLTArea_without_Innccuracy;
    function Get_Max_Area: IXMLTArea_without_Innccuracy;
    function Get_Note: UnicodeString;
    function Get_RelatedParcels: IXMLRelatedParcels;
    procedure Set_CadastralNumber(Value: UnicodeString);
    procedure Set_CadastralBlock(Value: UnicodeString);
    procedure Set_Area_In_GKN(Value: LongWord);
    procedure Set_Delta_Area(Value: LongWord);
    procedure Set_Note(Value: UnicodeString);
    { Methods & Properties }
    property CadastralNumber: UnicodeString read Get_CadastralNumber write Set_CadastralNumber;
    property CadastralBlock: UnicodeString read Get_CadastralBlock write Set_CadastralBlock;
    property Inner_CadastralNumbers: IXMLTInner_CadastralNumbers read Get_Inner_CadastralNumbers;
    property Area: IXMLTArea read Get_Area;
    property SubParcels: IXMLTExistParcel_SubParcels read Get_SubParcels;
    property Entity_Spatial: IXMLEntity_Spatial read Get_Entity_Spatial;
    property Contours: IXMLContours read Get_Contours;
    property Area_In_GKN: LongWord read Get_Area_In_GKN write Set_Area_In_GKN;
    property Delta_Area: LongWord read Get_Delta_Area write Set_Delta_Area;
    property Min_Area: IXMLTArea_without_Innccuracy read Get_Min_Area;
    property Max_Area: IXMLTArea_without_Innccuracy read Get_Max_Area;
    property Note: UnicodeString read Get_Note write Set_Note;
    property RelatedParcels: IXMLRelatedParcels read Get_RelatedParcels;
  end;


{ IXMLTExistParcel_Inner_CadastralNumbers }

{  IXMLTExistParcel_Inner_CadastralNumbers = interface(IXMLNode)
    { Property Accessors }
  {  function Get_CadastralNumber: IXMLTInner_CadastralNumbers_CadastralNumberList;
    function Get_Number: IXMLString_List;
    { Methods & Properties }
  {  property CadastralNumber: IXMLTInner_CadastralNumbers_CadastralNumberList read Get_CadastralNumber;
    property Number: IXMLString_List read Get_Number;
  end;

{ IXMLTExistParcel_SubParcels }

  IXMLTExistParcel_SubParcels = interface(IXMLNode)
    ['{BE18CAA0-EAEE-4F8C-A40B-40D65C94780D}']
    { Property Accessors }
    function Get_FormSubParcel: IXMLTExistParcel_SubParcels_FormSubParcelList;
    function Get_ExistSubParcel: IXMLTExistParcel_SubParcels_ExistSubParcelList;
    function Get_InvariableSubParcel: IXMLTExistParcel_SubParcels_InvariableSubParcelList;
    { Methods & Properties }
    property FormSubParcel: IXMLTExistParcel_SubParcels_FormSubParcelList read Get_FormSubParcel;
    property ExistSubParcel: IXMLTExistParcel_SubParcels_ExistSubParcelList read Get_ExistSubParcel;
    property InvariableSubParcel: IXMLTExistParcel_SubParcels_InvariableSubParcelList read Get_InvariableSubParcel;
  end;

{ IXMLTExistParcel_SubParcels_FormSubParcel }

  IXMLTExistParcel_SubParcels_FormSubParcel = interface(IXMLTSubParcel)
    ['{1E72A7A2-C170-445C-9515-F3FF47AFF36F}']
    { Property Accessors }
    function Get_Definition: UnicodeString;
    procedure Set_Definition(Value: UnicodeString);
    { Methods & Properties }
    property Definition: UnicodeString read Get_Definition write Set_Definition;
  end;

{ IXMLTExistParcel_SubParcels_FormSubParcelList }

  IXMLTExistParcel_SubParcels_FormSubParcelList = interface(IXMLNodeCollection)
    ['{901BD72D-DBBA-4692-BF73-888006C85706}']
    { Methods & Properties }
    function Add: IXMLTExistParcel_SubParcels_FormSubParcel;
    function Insert(const Index: Integer): IXMLTExistParcel_SubParcels_FormSubParcel;

    function Get_Item(Index: Integer): IXMLTExistParcel_SubParcels_FormSubParcel;
    property Items[Index: Integer]: IXMLTExistParcel_SubParcels_FormSubParcel read Get_Item; default;
  end;

{ IXMLTExistParcel_SubParcels_ExistSubParcel }

  IXMLTExistParcel_SubParcels_ExistSubParcel = interface(IXMLTSubParcel)
    ['{2233C771-52EB-462C-892E-2E218782CEDF}']
    { Property Accessors }
    function Get_Number_Record: LongWord;
    procedure Set_Number_Record(Value: LongWord);
    { Methods & Properties }
    property Number_Record: LongWord read Get_Number_Record write Set_Number_Record;
  end;

{ IXMLTExistParcel_SubParcels_ExistSubParcelList }

  IXMLTExistParcel_SubParcels_ExistSubParcelList = interface(IXMLNodeCollection)
    ['{093B5E83-533B-4FC5-B121-19AEFB9EC5C8}']
    { Methods & Properties }
    function Add: IXMLTExistParcel_SubParcels_ExistSubParcel;
    function Insert(const Index: Integer): IXMLTExistParcel_SubParcels_ExistSubParcel;

    function Get_Item(Index: Integer): IXMLTExistParcel_SubParcels_ExistSubParcel;
    property Items[Index: Integer]: IXMLTExistParcel_SubParcels_ExistSubParcel read Get_Item; default;
  end;

{ IXMLTExistParcel_SubParcels_InvariableSubParcel }

  IXMLTExistParcel_SubParcels_InvariableSubParcel = interface(IXMLNode)
    ['{18604BC4-8AE9-46D8-98A3-D9952C56216A}']
    { Property Accessors }
    function Get_Number_Record: LongWord;
    function Get_SubParcel_Realty: Boolean;
    function Get_Area: IXMLTExistParcel_SubParcels_InvariableSubParcel_Area;
    function Get_Encumbrance: IXMLTEncumbrance;
    function Get_Contours: IXMLTExistParcel_SubParcels_InvariableSubParcel_Contours;
    procedure Set_Number_Record(Value: LongWord);
    procedure Set_SubParcel_Realty(Value: Boolean);
    { Methods & Properties }
    property Number_Record: LongWord read Get_Number_Record write Set_Number_Record;
    property SubParcel_Realty: Boolean read Get_SubParcel_Realty write Set_SubParcel_Realty;
    property Area: IXMLTExistParcel_SubParcels_InvariableSubParcel_Area read Get_Area;
    property Encumbrance: IXMLTEncumbrance read Get_Encumbrance;
    property Contours: IXMLTExistParcel_SubParcels_InvariableSubParcel_Contours read Get_Contours;
  end;

{ IXMLTExistParcel_SubParcels_InvariableSubParcelList }

  IXMLTExistParcel_SubParcels_InvariableSubParcelList = interface(IXMLNodeCollection)
    ['{0E89D35E-290F-439F-AB72-DA38B1E21CE0}']
    { Methods & Properties }
    function Add: IXMLTExistParcel_SubParcels_InvariableSubParcel;
    function Insert(const Index: Integer): IXMLTExistParcel_SubParcels_InvariableSubParcel;

    function Get_Item(Index: Integer): IXMLTExistParcel_SubParcels_InvariableSubParcel;
    property Items[Index: Integer]: IXMLTExistParcel_SubParcels_InvariableSubParcel read Get_Item; default;
  end;

{ IXMLTExistParcel_SubParcels_InvariableSubParcel_Area }

  IXMLTExistParcel_SubParcels_InvariableSubParcel_Area = interface(IXMLNode)
    ['{2F17005B-20EC-4296-A7CA-7DD3794E9574}']
    { Property Accessors }
    function Get_Area: LongWord;
    function Get_Unit_: UnicodeString;
    procedure Set_Area(Value: LongWord);
    procedure Set_Unit_(Value: UnicodeString);
    { Methods & Properties }
    property Area: LongWord read Get_Area write Set_Area;
    property Unit_: UnicodeString read Get_Unit_ write Set_Unit_;
  end;

{ IXMLTExistParcel_SubParcels_InvariableSubParcel_Contours }

  IXMLTExistParcel_SubParcels_InvariableSubParcel_Contours = interface(IXMLNodeCollection)
    ['{0464B92A-3D59-4818-A83C-F4E8550B1076}']
    { Property Accessors }
    function Get_Contour(Index: Integer): IXMLTExistParcel_SubParcels_InvariableSubParcel_Contours_Contour;
    { Methods & Properties }
    function Add: IXMLTExistParcel_SubParcels_InvariableSubParcel_Contours_Contour;
    function Insert(const Index: Integer): IXMLTExistParcel_SubParcels_InvariableSubParcel_Contours_Contour;
    property Contour[Index: Integer]: IXMLTExistParcel_SubParcels_InvariableSubParcel_Contours_Contour read Get_Contour; default;
  end;

{ IXMLTExistParcel_SubParcels_InvariableSubParcel_Contours_Contour }

  IXMLTExistParcel_SubParcels_InvariableSubParcel_Contours_Contour = interface(IXMLNode)
    ['{8B14C559-B728-43DE-BD2B-45AB953779AB}']
    { Property Accessors }
    function Get_Number: UnicodeString;
    function Get_Area: IXMLTArea; //TODO bilo IXMlTArea_Contour
    procedure Set_Number(Value: UnicodeString);
    { Methods & Properties }
    property Number: UnicodeString read Get_Number write Set_Number;
    property Area: IXMLTArea read Get_Area;
  end;

{ IXMLContours }

  IXMLContours = interface(IXMLNode)
    ['{6404A848-6E8E-4B0E-BD9F-EA9CF4C08473}']
    { Property Accessors }
    function Get_NewContour: IXMLContours_NewContourList;
    function Get_ExistContour: IXMLContours_ExistContourList;
    function Get_DeleteAllBorder: IXMLContours_DeleteAllBorderList;
    { Methods & Properties }
    property NewContour: IXMLContours_NewContourList read Get_NewContour;
    property ExistContour: IXMLContours_ExistContourList read Get_ExistContour;
    property DeleteAllBorder: IXMLContours_DeleteAllBorderList read Get_DeleteAllBorder;
  end;

{ IXMLContours_NewContour }

  IXMLContours_NewContour = interface(IXMLTContour)
    ['{9A4A4BDE-B06C-4E09-9B9A-3DA47977AC84}']
    { Property Accessors }
    function Get_Definition: UnicodeString;
    function Get_Providing_Pass_CadastralNumbers: IXMLTProviding_Pass_CadastralNumbers;
    procedure Set_Definition(Value: UnicodeString);
    { Methods & Properties }
    property Definition: UnicodeString read Get_Definition write Set_Definition;
    property Providing_Pass_CadastralNumbers: IXMLTProviding_Pass_CadastralNumbers read Get_Providing_Pass_CadastralNumbers;
  end;

{ IXMLContours_NewContourList }

  IXMLContours_NewContourList = interface(IXMLNodeCollection)
    ['{3C578D5F-0611-44BA-A382-980FDDCDC087}']
    { Methods & Properties }
    function Add: IXMLContours_NewContour;
    function Insert(const Index: Integer): IXMLContours_NewContour;

    function Get_Item(Index: Integer): IXMLContours_NewContour;
    property Items[Index: Integer]: IXMLContours_NewContour read Get_Item; default;
  end;

{ IXMLContours_ExistContour }

  IXMLContours_ExistContour = interface(IXMLTContour)
    ['{AB013E15-5C8F-4731-94DA-2E56661BEC2B}']
    { Property Accessors }
    function Get_Number_Record: LongWord;
    procedure Set_Number_Record(Value: LongWord);
    { Methods & Properties }
    property Number_Record: LongWord read Get_Number_Record write Set_Number_Record;
  end;

{ IXMLContours_ExistContourList }

  IXMLContours_ExistContourList = interface(IXMLNodeCollection)
    ['{79F44D96-0DCC-4936-B056-DC23BF7DE797}']
    { Methods & Properties }
    function Add: IXMLContours_ExistContour;
    function Insert(const Index: Integer): IXMLContours_ExistContour;

    function Get_Item(Index: Integer): IXMLContours_ExistContour;
    property Items[Index: Integer]: IXMLContours_ExistContour read Get_Item; default;
  end;

{ IXMLContours_DeleteAllBorder }

  IXMLContours_DeleteAllBorder = interface(IXMLNodeCollection)
    ['{C13249F2-2258-428F-9021-28799DE28C32}']
    { Property Accessors }
    function Get_Number_Record: LongWord;
    function Get_OldOrdinate(Index: Integer): IXMLTOrdinate;
    procedure Set_Number_Record(Value: LongWord);
    { Methods & Properties }
    function Add: IXMLTOrdinate;
    function Insert(const Index: Integer): IXMLTOrdinate;
    property Number_Record: LongWord read Get_Number_Record write Set_Number_Record;
    property OldOrdinate[Index: Integer]: IXMLTOrdinate read Get_OldOrdinate; default;
  end;

{ IXMLContours_DeleteAllBorderList }

  IXMLContours_DeleteAllBorderList = interface(IXMLNodeCollection)
    ['{6C1D8DB0-3FC9-4D25-AAFA-0D4ECCA6A024}']
    { Methods & Properties }
    function Add: IXMLContours_DeleteAllBorder;
    function Insert(const Index: Integer): IXMLContours_DeleteAllBorder;

    function Get_Item(Index: Integer): IXMLContours_DeleteAllBorder;
    property Items[Index: Integer]: IXMLContours_DeleteAllBorder read Get_Item; default;
  end;

{ IXMLRelatedParcels }

  IXMLRelatedParcels = interface(IXMLNodeCollection)
    ['{8CB75262-0902-49CE-BB7E-10E55DEA1F3E}']
    { Property Accessors }
    function Get_ParcelNeighbours(Index: Integer): IXMLRelatedParcels_ParcelNeighbours;
    { Methods & Properties }
    function Add: IXMLRelatedParcels_ParcelNeighbours;
    function Insert(const Index: Integer): IXMLRelatedParcels_ParcelNeighbours;
    property ParcelNeighbours[Index: Integer]: IXMLRelatedParcels_ParcelNeighbours read Get_ParcelNeighbours; default;
  end;

{ IXMLRelatedParcels_ParcelNeighbours }

  IXMLRelatedParcels_ParcelNeighbours = interface(IXMLNode)
    ['{27405AA7-AE5F-4B7A-B0F6-AC211C4CCB60}']
    { Property Accessors }
    function Get_Definition: UnicodeString;
    function Get_ParcelNeighbour: IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbourList;
    procedure Set_Definition(Value: UnicodeString);
    { Methods & Properties }
    property Definition: UnicodeString read Get_Definition write Set_Definition;
    property ParcelNeighbour: IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbourList read Get_ParcelNeighbour;
  end;

{ IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour }

  IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour = interface(IXMLNode)
    ['{945B3253-8537-4275-987B-007051CBCB0C}']
    { Property Accessors }
    function Get_Cadastral_Number: UnicodeString;
    function Get_OwnerNeighbours: IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours;
    procedure Set_Cadastral_Number(Value: UnicodeString);
    { Methods & Properties }
    property Cadastral_Number: UnicodeString read Get_Cadastral_Number write Set_Cadastral_Number;
    property OwnerNeighbours: IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours read Get_OwnerNeighbours;
  end;

{ IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbourList }

  IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbourList = interface(IXMLNodeCollection)
    ['{E6946F68-F663-4797-8B37-E48EB3AE1936}']
    { Methods & Properties }
    function Add: IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour;
    function Insert(const Index: Integer): IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour;

    function Get_Item(Index: Integer): IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour;
    property Items[Index: Integer]: IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour read Get_Item; default;
  end;

{ IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours }

  IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours = interface(IXMLNode)
    ['{F5BC6129-A83D-4AEA-BAE0-DB5530F9397D}']
    { Property Accessors }
    function Get_NameRight: UnicodeString;
    function Get_OwnerNeighbour: IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbourList;
    procedure Set_NameRight(Value: UnicodeString);
    { Methods & Properties }
    property NameRight: UnicodeString read Get_NameRight write Set_NameRight;
    property OwnerNeighbour: IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbourList read Get_OwnerNeighbour;
  end;

{ IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbour }

  IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbour = interface(IXMLNode)
    ['{82BB9BCC-8B63-4B21-971D-1B7CB64AB2F0}']
    { Property Accessors }
    function Get_NameOwner: UnicodeString;
    function Get_ContactAddress: UnicodeString;
    function Get_Documents: IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbour_Documents;
    procedure Set_NameOwner(Value: UnicodeString);
    procedure Set_ContactAddress(Value: UnicodeString);
    { Methods & Properties }
    property NameOwner: UnicodeString read Get_NameOwner write Set_NameOwner;
    property ContactAddress: UnicodeString read Get_ContactAddress write Set_ContactAddress;
    property Documents: IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbour_Documents read Get_Documents;
  end;

{ IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbourList }

  IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbourList = interface(IXMLNodeCollection)
    ['{D3314FC3-5474-47D5-9315-C030D174B166}']
    { Methods & Properties }
    function Add: IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbour;
    function Insert(const Index: Integer): IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbour;

    function Get_Item(Index: Integer): IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbour;
    property Items[Index: Integer]: IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbour read Get_Item; default;
  end;

{ IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbour_Documents }

  IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbour_Documents = interface(IXMLNodeCollection)
    ['{B7B1AC39-8411-4144-A16C-69940931CD10}']
    { Property Accessors }
    function Get_Document(Index: Integer): IXMLTDocument;
    { Methods & Properties }
    function Add: IXMLTDocument;
    function Insert(const Index: Integer): IXMLTDocument;
    property Document[Index: Integer]: IXMLTDocument read Get_Document; default;
  end;

{ IXMLSTD_MP_Package_SpecifyParcel_ExistEZ }

  IXMLSTD_MP_Package_SpecifyParcel_ExistEZ = interface(IXMLNode)
    ['{00D6947C-BADB-47D3-9B45-3C47DDA0681B}']
    { Property Accessors }
    function Get_ExistEZParcels: IXMLTExistEZParcel;
    function Get_ExistEZEntryParcels: IXMLSTD_MP_Package_SpecifyParcel_ExistEZ_ExistEZEntryParcels;
    { Methods & Properties }
    property ExistEZParcels: IXMLTExistEZParcel read Get_ExistEZParcels;
    property ExistEZEntryParcels: IXMLSTD_MP_Package_SpecifyParcel_ExistEZ_ExistEZEntryParcels read Get_ExistEZEntryParcels;
  end;

{ IXMLTExistEZParcel }

  IXMLTExistEZParcel = interface(IXMLNode)
    ['{F344DC6D-D90E-4F38-8953-EEB01E8AC51D}']
    { Property Accessors }
    function Get_CadastralNumber: UnicodeString;
    function Get_CadastralBlock: UnicodeString;
    function Get_Inner_CadastralNumbers: IXMLTExistEZParcel_Inner_CadastralNumbers;
    function Get_Area: IXMLTAreaNew;
    function Get_SubParcels: IXMLTExistParcel_SubParcels;  //IXMLTExistEZParcel_SubParcels;
    function Get_Composition_EZ: IXMLTExistEZParcel_Composition_EZ;
    function Get_Area_In_GKN: LongWord;
    function Get_Delta_Area: LongWord;
    function Get_Min_Area: IXMLTArea_without_Innccuracy;
    function Get_Max_Area: IXMLTArea_without_Innccuracy;
    function Get_Note: UnicodeString;
    function Get_RelatedParcels: IXMLRelatedParcels;
    procedure Set_CadastralNumber(Value: UnicodeString);
    procedure Set_CadastralBlock(Value: UnicodeString);
    procedure Set_Area_In_GKN(Value: LongWord);
    procedure Set_Delta_Area(Value: LongWord);
    procedure Set_Note(Value: UnicodeString);
    { Methods & Properties }
    property CadastralNumber: UnicodeString read Get_CadastralNumber write Set_CadastralNumber;
    property CadastralBlock: UnicodeString read Get_CadastralBlock write Set_CadastralBlock;
    property Inner_CadastralNumbers: IXMLTExistEZParcel_Inner_CadastralNumbers read Get_Inner_CadastralNumbers;
    property Area: IXMLTAreaNew read Get_Area;
    property SubParcels: IXMLTExistParcel_SubParcels read Get_SubParcels; //  IXMLTExistEZParcel_SubParcels;
    property Composition_EZ: IXMLTExistEZParcel_Composition_EZ read Get_Composition_EZ;
    property Area_In_GKN: LongWord read Get_Area_In_GKN write Set_Area_In_GKN;
    property Delta_Area: LongWord read Get_Delta_Area write Set_Delta_Area;
    property Min_Area: IXMLTArea_without_Innccuracy read Get_Min_Area;
    property Max_Area: IXMLTArea_without_Innccuracy read Get_Max_Area;
    property Note: UnicodeString read Get_Note write Set_Note;
    property RelatedParcels: IXMLRelatedParcels read Get_RelatedParcels;
  end;

{ IXMLTExistEZParcel_Inner_CadastralNumbers }

  IXMLTExistEZParcel_Inner_CadastralNumbers = interface(IXMLNode)
    ['{48B71FEE-75ED-496C-A7AC-EE1C74F9DC26}']
    { Property Accessors }
    function Get_CadastralNumber: IXMLTExistEZParcel_Inner_CadastralNumbers_CadastralNumberList;
    function Get_Number: IXMLString_List;
    { Methods & Properties }
    property CadastralNumber: IXMLTExistEZParcel_Inner_CadastralNumbers_CadastralNumberList read Get_CadastralNumber;
    property Number: IXMLString_List read Get_Number;
  end;

{ IXMLTExistEZParcel_SubParcels }

  IXMLTExistEZParcel_SubParcels = interface(IXMLNode)
    ['{4C7A1CBC-578C-49E8-831A-B8CB0F4C3197}']
    { Property Accessors }
    function Get_FormSubParcel: IXMLTExistEZParcel_SubParcels_FormSubParcelList;
    function Get_ExistSubParcel: IXMLTExistEZParcel_SubParcels_ExistSubParcelList;
    function Get_InvariableSubParcel: IXMLTExistEZParcel_SubParcels_InvariableSubParcelList;
    { Methods & Properties }
    property FormSubParcel: IXMLTExistEZParcel_SubParcels_FormSubParcelList read Get_FormSubParcel;
    property ExistSubParcel: IXMLTExistEZParcel_SubParcels_ExistSubParcelList read Get_ExistSubParcel;
    property InvariableSubParcel: IXMLTExistEZParcel_SubParcels_InvariableSubParcelList read Get_InvariableSubParcel;
  end;

{ IXMLTExistEZParcel_SubParcels_FormSubParcel }

  IXMLTExistEZParcel_SubParcels_FormSubParcel = interface(IXMLTSubParcel)
    ['{58A1E30A-6498-4994-AEC8-7D3BC51634EA}']
    { Property Accessors }
    function Get_Definition: UnicodeString;
    procedure Set_Definition(Value: UnicodeString);
    { Methods & Properties }
    property Definition: UnicodeString read Get_Definition write Set_Definition;
  end;

{ IXMLTExistEZParcel_SubParcels_FormSubParcelList }

  IXMLTExistEZParcel_SubParcels_FormSubParcelList = interface(IXMLNodeCollection)
    ['{D3B27FF1-2FB1-4982-902E-F820330943F0}']
    { Methods & Properties }
    function Add: IXMLTExistEZParcel_SubParcels_FormSubParcel;
    function Insert(const Index: Integer): IXMLTExistEZParcel_SubParcels_FormSubParcel;

    function Get_Item(Index: Integer): IXMLTExistEZParcel_SubParcels_FormSubParcel;
    property Items[Index: Integer]: IXMLTExistEZParcel_SubParcels_FormSubParcel read Get_Item; default;
  end;

{ IXMLTExistEZParcel_SubParcels_ExistSubParcel }

  IXMLTExistEZParcel_SubParcels_ExistSubParcel = interface(IXMLTSubParcel)
    ['{558F16A4-078F-4285-89D0-23ED0BF21D6C}']
    { Property Accessors }
    function Get_Number_Record: LongWord;
    procedure Set_Number_Record(Value: LongWord);
    { Methods & Properties }
    property Number_Record: LongWord read Get_Number_Record write Set_Number_Record;
  end;

{ IXMLTExistEZParcel_SubParcels_ExistSubParcelList }

  IXMLTExistEZParcel_SubParcels_ExistSubParcelList = interface(IXMLNodeCollection)
    ['{3AF35B68-95CC-48CF-A164-6ED7952CC4B6}']
    { Methods & Properties }
    function Add: IXMLTExistEZParcel_SubParcels_ExistSubParcel;
    function Insert(const Index: Integer): IXMLTExistEZParcel_SubParcels_ExistSubParcel;

    function Get_Item(Index: Integer): IXMLTExistEZParcel_SubParcels_ExistSubParcel;
    property Items[Index: Integer]: IXMLTExistEZParcel_SubParcels_ExistSubParcel read Get_Item; default;
  end;

{ IXMLTExistEZParcel_SubParcels_InvariableSubParcel }

  IXMLTExistEZParcel_SubParcels_InvariableSubParcel = interface(IXMLNode)
    ['{F28E5C1A-785E-4BBC-B165-9E44C30765E0}']
    { Property Accessors }
    function Get_Number_Record: LongWord;
    function Get_SubParcel_Realty: Boolean;
    function Get_Area: IXMLTExistEZParcel_SubParcels_InvariableSubParcel_Area;
    function Get_Encumbrance: IXMLTEncumbrance;
    function Get_Contours: IXMLTExistEZParcel_SubParcels_InvariableSubParcel_Contours;
    procedure Set_Number_Record(Value: LongWord);
    procedure Set_SubParcel_Realty(Value: Boolean);
    { Methods & Properties }
    property Number_Record: LongWord read Get_Number_Record write Set_Number_Record;
    property SubParcel_Realty: Boolean read Get_SubParcel_Realty write Set_SubParcel_Realty;
    property Area: IXMLTExistEZParcel_SubParcels_InvariableSubParcel_Area read Get_Area;
    property Encumbrance: IXMLTEncumbrance read Get_Encumbrance;
    property Contours: IXMLTExistEZParcel_SubParcels_InvariableSubParcel_Contours read Get_Contours;
  end;

{ IXMLTExistEZParcel_SubParcels_InvariableSubParcelList }

  IXMLTExistEZParcel_SubParcels_InvariableSubParcelList = interface(IXMLNodeCollection)
    ['{9D1FCD93-F541-4BDF-9E10-2D50EF19C1E6}']
    { Methods & Properties }
    function Add: IXMLTExistEZParcel_SubParcels_InvariableSubParcel;
    function Insert(const Index: Integer): IXMLTExistEZParcel_SubParcels_InvariableSubParcel;

    function Get_Item(Index: Integer): IXMLTExistEZParcel_SubParcels_InvariableSubParcel;
    property Items[Index: Integer]: IXMLTExistEZParcel_SubParcels_InvariableSubParcel read Get_Item; default;
  end;

{ IXMLTExistEZParcel_SubParcels_InvariableSubParcel_Area }

  IXMLTExistEZParcel_SubParcels_InvariableSubParcel_Area = interface(IXMLNode)
    ['{4B31ED69-8508-4971-8ADF-59D81AF3CC50}']
    { Property Accessors }
    function Get_Area: LongWord;
    function Get_Unit_: UnicodeString;
    procedure Set_Area(Value: LongWord);
    procedure Set_Unit_(Value: UnicodeString);
    { Methods & Properties }
    property Area: LongWord read Get_Area write Set_Area;
    property Unit_: UnicodeString read Get_Unit_ write Set_Unit_;
  end;

{ IXMLTExistEZParcel_SubParcels_InvariableSubParcel_Contours }

  IXMLTExistEZParcel_SubParcels_InvariableSubParcel_Contours = interface(IXMLNodeCollection)
    ['{8ACD5239-F3CD-4AEF-B58B-985678BE69EE}']
    { Property Accessors }
    function Get_Contour(Index: Integer): IXMLTExistEZParcel_SubParcels_InvariableSubParcel_Contours_Contour;
    { Methods & Properties }
    function Add: IXMLTExistEZParcel_SubParcels_InvariableSubParcel_Contours_Contour;
    function Insert(const Index: Integer): IXMLTExistEZParcel_SubParcels_InvariableSubParcel_Contours_Contour;
    property Contour[Index: Integer]: IXMLTExistEZParcel_SubParcels_InvariableSubParcel_Contours_Contour read Get_Contour; default;
  end;

{ IXMLTExistEZParcel_SubParcels_InvariableSubParcel_Contours_Contour }

  IXMLTExistEZParcel_SubParcels_InvariableSubParcel_Contours_Contour = interface(IXMLNode)
    ['{39085E6B-A025-46D7-9F61-1754A79D7C3B}']
    { Property Accessors }
    function Get_Number: UnicodeString;
    function Get_Area: IXMLTArea_Contour;
    procedure Set_Number(Value: UnicodeString);
    { Methods & Properties }
    property Number: UnicodeString read Get_Number write Set_Number;
    property Area: IXMLTArea_Contour read Get_Area;
  end;

{ IXMLTExistEZParcel_Composition_EZ }

  IXMLTExistEZParcel_Composition_EZ = interface(IXMLNode)
    ['{4F4CEF22-63A2-4FFB-A005-6FED54A0EEA7}']
    { Property Accessors }
    function Get_InsertEntryParcels: IXMLTExistEZParcel_Composition_EZ_InsertEntryParcels;
    function Get_DeleteEntryParcels: IXMLTExistEZParcel_Composition_EZ_DeleteEntryParcels;
    { Methods & Properties }
    property InsertEntryParcels: IXMLTExistEZParcel_Composition_EZ_InsertEntryParcels read Get_InsertEntryParcels;
    property DeleteEntryParcels: IXMLTExistEZParcel_Composition_EZ_DeleteEntryParcels read Get_DeleteEntryParcels;
  end;

{ IXMLTExistEZParcel_Composition_EZ_InsertEntryParcels }

  IXMLTExistEZParcel_Composition_EZ_InsertEntryParcels = interface(IXMLNodeCollection)
    ['{1B6A24AE-CB70-4F34-B960-2A762851A76B}']
    { Property Accessors }
    function Get_InsertEntryParcel(Index: Integer): IXMLTExistEZParcel_Composition_EZ_InsertEntryParcels_InsertEntryParcel;
    { Methods & Properties }
    function Add: IXMLTExistEZParcel_Composition_EZ_InsertEntryParcels_InsertEntryParcel;
    function Insert(const Index: Integer): IXMLTExistEZParcel_Composition_EZ_InsertEntryParcels_InsertEntryParcel;
    property InsertEntryParcel[Index: Integer]: IXMLTExistEZParcel_Composition_EZ_InsertEntryParcels_InsertEntryParcel read Get_InsertEntryParcel; default;
  end;

{ IXMLTExistEZParcel_Composition_EZ_InsertEntryParcels_InsertEntryParcel }

  IXMLTExistEZParcel_Composition_EZ_InsertEntryParcels_InsertEntryParcel = interface(IXMLNode)
    ['{3CFB1251-7087-4A96-9094-E217431E6667}']
    { Property Accessors }
    function Get_ExistEntryParcel: IXMLTExistEZParcel_Composition_EZ_InsertEntryParcels_InsertEntryParcel_ExistEntryParcel;
    function Get_NewEntryParcel: IXMLTNewEZEntryParcel;
    { Methods & Properties }
    property ExistEntryParcel: IXMLTExistEZParcel_Composition_EZ_InsertEntryParcels_InsertEntryParcel_ExistEntryParcel read Get_ExistEntryParcel;
    property NewEntryParcel: IXMLTNewEZEntryParcel read Get_NewEntryParcel;
  end;

{ IXMLTExistEZParcel_Composition_EZ_InsertEntryParcels_InsertEntryParcel_ExistEntryParcel }

  IXMLTExistEZParcel_Composition_EZ_InsertEntryParcels_InsertEntryParcel_ExistEntryParcel = interface(IXMLNode)
    ['{530EAB7A-E2DD-495A-A4A6-CC1B73D58903}']
    { Property Accessors }
    function Get_CadastralNumber: UnicodeString;
    procedure Set_CadastralNumber(Value: UnicodeString);
    { Methods & Properties }
    property CadastralNumber: UnicodeString read Get_CadastralNumber write Set_CadastralNumber;
  end;

{ IXMLTNewEZEntryParcel }

  IXMLTNewEZEntryParcel = interface(IXMLNode)
    ['{D760687A-1218-4036-BCB1-33FC94D4EB9E}']
    { Property Accessors }
    function Get_Name: UnicodeString;
    function Get_Definition: UnicodeString;
    function Get_Area: IXMLTAreaNew;
    function Get_Entity_Spatial: IXMLEntity_Spatial;
    function Get_Encumbrance: IXMLTEncumbrance;
    procedure Set_Name(Value: UnicodeString);
    procedure Set_Definition(Value: UnicodeString);
    { Methods & Properties }
    property Name: UnicodeString read Get_Name write Set_Name;
    property Definition: UnicodeString read Get_Definition write Set_Definition;
    property Area: IXMLTAreaNew read Get_Area;
    property Entity_Spatial: IXMLEntity_Spatial read Get_Entity_Spatial;
    property Encumbrance: IXMLTEncumbrance read Get_Encumbrance;
  end;

{ IXMLTExistEZParcel_Composition_EZ_DeleteEntryParcels }

  IXMLTExistEZParcel_Composition_EZ_DeleteEntryParcels = interface(IXMLNodeCollection)
    ['{5E0C883A-9F8C-48AB-84C4-BD0F492F1160}']
    { Property Accessors }
    function Get_DeleteEntryParcel(Index: Integer): IXMLTExistEZParcel_Composition_EZ_DeleteEntryParcels_DeleteEntryParcel;
    { Methods & Properties }
    function Add: IXMLTExistEZParcel_Composition_EZ_DeleteEntryParcels_DeleteEntryParcel;
    function Insert(const Index: Integer): IXMLTExistEZParcel_Composition_EZ_DeleteEntryParcels_DeleteEntryParcel;
    property DeleteEntryParcel[Index: Integer]: IXMLTExistEZParcel_Composition_EZ_DeleteEntryParcels_DeleteEntryParcel read Get_DeleteEntryParcel; default;
  end;

{ IXMLTExistEZParcel_Composition_EZ_DeleteEntryParcels_DeleteEntryParcel }

  IXMLTExistEZParcel_Composition_EZ_DeleteEntryParcels_DeleteEntryParcel = interface(IXMLNode)
    ['{3C79A523-7B4A-4446-8E03-A450AF6FE1B7}']
    { Property Accessors }
    function Get_CadastralNumber: UnicodeString;
    procedure Set_CadastralNumber(Value: UnicodeString);
    { Methods & Properties }
    property CadastralNumber: UnicodeString read Get_CadastralNumber write Set_CadastralNumber;
  end;

{ IXMLSTD_MP_Package_SpecifyParcel_ExistEZ_ExistEZEntryParcels }

  IXMLSTD_MP_Package_SpecifyParcel_ExistEZ_ExistEZEntryParcels = interface(IXMLNodeCollection)
    ['{4981D69D-97C3-4472-A3B7-F72394FCE2FF}']
    { Property Accessors }
    function Get_ExistEZEntryParcel(Index: Integer): IXMLTExistEZEntryParcel;
    { Methods & Properties }
    function Add: IXMLTExistEZEntryParcel;
    function Insert(const Index: Integer): IXMLTExistEZEntryParcel;
    property ExistEZEntryParcel[Index: Integer]: IXMLTExistEZEntryParcel read Get_ExistEZEntryParcel; default;
  end;

{ IXMLSTD_MP_Package_SpecifyParcel_ExistEZ_ExistEZEntryParcelsList }

  IXMLSTD_MP_Package_SpecifyParcel_ExistEZ_ExistEZEntryParcelsList = interface(IXMLNodeCollection)
    ['{5B82D42C-DC35-429A-ACB6-FCEF1E3B1F2B}']
    { Methods & Properties }
    function Add: IXMLSTD_MP_Package_SpecifyParcel_ExistEZ_ExistEZEntryParcels;
    function Insert(const Index: Integer): IXMLSTD_MP_Package_SpecifyParcel_ExistEZ_ExistEZEntryParcels;

    function Get_Item(Index: Integer): IXMLSTD_MP_Package_SpecifyParcel_ExistEZ_ExistEZEntryParcels;
    property Items[Index: Integer]: IXMLSTD_MP_Package_SpecifyParcel_ExistEZ_ExistEZEntryParcels read Get_Item; default;
  end;

{ IXMLTExistEZEntryParcel }

  IXMLTExistEZEntryParcel = interface(IXMLNode)
    ['{01A52685-9D3F-4807-AD62-68D89E20AE68}']
    { Property Accessors }
    function Get_CadastralNumber: UnicodeString;
    function Get_Area: IXMLTAreaNew;
    function Get_Area_In_GKN: LongWord;
    function Get_Encumbrance: IXMLTEncumbrance;
    function Get_Entity_Spatial: IXMLEntity_Spatial;
    function Get_Note: UnicodeString;
    procedure Set_CadastralNumber(Value: UnicodeString);
    procedure Set_Area_In_GKN(Value: LongWord);
    procedure Set_Note(Value: UnicodeString);
    { Methods & Properties }
    property CadastralNumber: UnicodeString read Get_CadastralNumber write Set_CadastralNumber;
    property Area: IXMLTAreaNew read Get_Area;
    property Area_In_GKN: LongWord read Get_Area_In_GKN write Set_Area_In_GKN;
    property Encumbrance: IXMLTEncumbrance read Get_Encumbrance;
    property Entity_Spatial: IXMLEntity_Spatial read Get_Entity_Spatial;
    property Note: UnicodeString read Get_Note write Set_Note;
  end;

{ IXMLSTD_MP_Package_SpecifyParcel_SpecifyRelatedParcel }

  IXMLSTD_MP_Package_SpecifyParcel_SpecifyRelatedParcel = interface(IXMLTSpecifyRelatedParcel)
    ['{604B4CA5-F557-4429-ABCC-EC708B7E8C7D}']
  end;

{ IXMLSTD_MP_Package_SpecifyParcel_SpecifyRelatedParcelList }

  IXMLSTD_MP_Package_SpecifyParcel_SpecifyRelatedParcelList = interface(IXMLNodeCollection)
    ['{913B24EF-FC7D-4728-AD1F-4681D746979A}']
    { Methods & Properties }
    function Add:IXMLTSpecifyRelatedParcel; //IXMLSTD_MP_Package_SpecifyParcel_SpecifyRelatedParcel;
    function Insert(const Index: Integer):IXMLTSpecifyRelatedParcel; //IXMLSTD_MP_Package_SpecifyParcel_SpecifyRelatedParcel;

    function Get_Item(Index: Integer): IXMLTSpecifyRelatedParcel;//IXMLSTD_MP_Package_SpecifyParcel_SpecifyRelatedParcel;
    property Items[Index: Integer]: IXMLTSpecifyRelatedParcel read Get_Item; default;
  end;

{ IXMLTNewSubParcel }

  IXMLTNewSubParcel = interface(IXMLNode)
    ['{C97D3853-B598-4D56-8129-AF05BAE02640}']
    { Property Accessors }
    function Get_Definition: UnicodeString;
    function Get_SubParcel_Realty: Boolean;
    function Get_CadastralNumber_Parcel: UnicodeString;
    function Get_Area: IXMLTArea;
    function Get_Encumbrance: IXMLTEncumbrance;
    {$IFNDEF disableIXMLTNewSubParcel_Entity_Spatial}
    function Get_Entity_Spatial: IXMLTNewSubParcel_Entity_Spatial;
    {$ELSE}
    function Get_Entity_Spatial: IXMLEntity_Spatial;
    {$ENDIF}
    {$IFNDEF disableIXMLTNewSubParcel_Contours}
    function Get_Contours: IXMLTNewSubParcel_Contours;
    {$ELSE}
    function Get_Contours: IXMLTSubParcel_Contours;
    {$ENDIF}
    procedure Set_Definition(Value: UnicodeString);
    procedure Set_SubParcel_Realty(Value: Boolean);
    procedure Set_CadastralNumber_Parcel(Value: UnicodeString);
    { Methods & Properties }
    property Definition: UnicodeString read Get_Definition write Set_Definition;
    property SubParcel_Realty: Boolean read Get_SubParcel_Realty write Set_SubParcel_Realty;
    property CadastralNumber_Parcel: UnicodeString read Get_CadastralNumber_Parcel write Set_CadastralNumber_Parcel;
    property Area: IXMLTArea read Get_Area;
    property Encumbrance: IXMLTEncumbrance read Get_Encumbrance;
    {$IFNDEF  disableIXMLTNewSubParcel_Entity_Spatial}
    property Entity_Spatial: IXMLTNewSubParcel_Entity_Spatial read Get_Entity_Spatial;
    {$ELSE}
    property Entity_Spatial: IXMLEntity_Spatial read Get_Entity_Spatial;
    {$ENDIF}
    {$IFNDEF disableIXMLTNewSubParcel_Contours}
    property Contours: IXMLTNewSubParcel_Contours read Get_Contours;
    {$ELSE}
    property Contours: IXMLTSubParcel_Contours read Get_Contours;
    {$ENDIF}
  end;

{ IXMLTNewSubParcelList }

  IXMLTNewSubParcelList = interface(IXMLNodeCollection)
    ['{8E7B62C0-9397-43D9-932C-F2EB58846272}']
    { Methods & Properties }
    function Add: IXMLTNewSubParcel;
    function Insert(const Index: Integer): IXMLTNewSubParcel;

    function Get_Item(Index: Integer): IXMLTNewSubParcel;
    property Items[Index: Integer]: IXMLTNewSubParcel read Get_Item; default;
  end;

  {$IFNDEF  disableIXMLTNewSubParcel_Entity_Spatial}
{ IXMLTNewSubParcel_Entity_Spatial }

  IXMLTNewSubParcel_Entity_Spatial = interface(IXMLNodeCollection)
    ['{9A178121-43AB-4D5A-A90F-6D01085E1292}']
    { Property Accessors }
    function Get_Ent_Sys: UnicodeString;
    function Get_Spatial_Element(Index: Integer): IXMLTSPATIAL_ELEMENT;
    procedure Set_Ent_Sys(Value: UnicodeString);
    { Methods & Properties }
    function Add: IXMLTSPATIAL_ELEMENT;
    function Insert(const Index: Integer): IXMLTSPATIAL_ELEMENT;
    property Ent_Sys: UnicodeString read Get_Ent_Sys write Set_Ent_Sys;
    property Spatial_Element[Index: Integer]: IXMLTSPATIAL_ELEMENT read Get_Spatial_Element; default;
  end;
  {$ENDIF}

  {$IFNDEF disableIXMLTNewSubParcel_Contours}
{ IXMLTNewSubParcel_Contours }

  IXMLTNewSubParcel_Contours = interface(IXMLNodeCollection)
    ['{7EC53D3D-B5AC-4B78-9672-A9C58CCCB7AE}']
    { Property Accessors }
    function Get_Contour(Index: Integer): IXMLTNewSubParcel_Contours_Contour;
    { Methods & Properties }
    function Add: IXMLTNewSubParcel_Contours_Contour;
    function Insert(const Index: Integer): IXMLTNewSubParcel_Contours_Contour;
    property Contour[Index: Integer]: IXMLTNewSubParcel_Contours_Contour read Get_Contour; default;
  end;
  {$ENDIF}

{ IXMLTNewSubParcel_Contours_Contour }

  IXMLTNewSubParcel_Contours_Contour = interface(IXMLNode)
    ['{C0E7D8C2-182C-4F25-985B-CD9B529D4242}']
    { Property Accessors }
    function Get_Number: UnicodeString;
    function Get_Area: IXMLTArea_Contour;
    function Get_Entity_Spatial: IXMLTNewSubParcel_Contours_Contour_Entity_Spatial;
    procedure Set_Number(Value: UnicodeString);
    { Methods & Properties }
    property Number: UnicodeString read Get_Number write Set_Number;
    property Area: IXMLTArea_Contour read Get_Area;
    property Entity_Spatial: IXMLTNewSubParcel_Contours_Contour_Entity_Spatial read Get_Entity_Spatial;
  end;

{ IXMLTNewSubParcel_Contours_Contour_Entity_Spatial }

  IXMLTNewSubParcel_Contours_Contour_Entity_Spatial = interface(IXMLNodeCollection)
    ['{F817D70D-ED3C-4EC6-BC3B-D3DAAE679461}']
    { Property Accessors }
    function Get_Ent_Sys: UnicodeString;
    function Get_Spatial_Element(Index: Integer): IXMLTSPATIAL_ELEMENT;
    procedure Set_Ent_Sys(Value: UnicodeString);
    { Methods & Properties }
    function Add: IXMLTSPATIAL_ELEMENT;
    function Insert(const Index: Integer): IXMLTSPATIAL_ELEMENT;
    property Ent_Sys: UnicodeString read Get_Ent_Sys write Set_Ent_Sys;
    property Spatial_Element[Index: Integer]: IXMLTSPATIAL_ELEMENT read Get_Spatial_Element; default;
  end;

{ IXMLSTD_MP_Package_SpecifyParcelsApproximal }

  IXMLSTD_MP_Package_SpecifyParcelsApproximal = interface(IXMLNode)
    ['{8F2DA1C7-BC5B-436D-B810-58176885B2C7}']
    { Property Accessors }
    function Get_ExistParcel: IXMLTExistParcel;
    function Get_ExistEZ: IXMLSTD_MP_Package_SpecifyParcelsApproximal_ExistEZ;
    { Methods & Properties }
    property ExistParcel: IXMLTExistParcel read Get_ExistParcel;
    property ExistEZ: IXMLSTD_MP_Package_SpecifyParcelsApproximal_ExistEZ read Get_ExistEZ;
  end;

{ IXMLSTD_MP_Package_SpecifyParcelsApproximalList }

  IXMLSTD_MP_Package_SpecifyParcelsApproximalList = interface(IXMLNodeCollection)
    ['{6CB03E6E-E1D6-4B3F-BD04-1DFF11A2EDB9}']
    { Methods & Properties }
    function Add: IXMLSTD_MP_Package_SpecifyParcelsApproximal;
    function Insert(const Index: Integer): IXMLSTD_MP_Package_SpecifyParcelsApproximal;

    function Get_Item(Index: Integer): IXMLSTD_MP_Package_SpecifyParcelsApproximal;
    property Items[Index: Integer]: IXMLSTD_MP_Package_SpecifyParcelsApproximal read Get_Item; default;
  end;

{ IXMLSTD_MP_Package_SpecifyParcelsApproximal_ExistEZ }

  IXMLSTD_MP_Package_SpecifyParcelsApproximal_ExistEZ = interface(IXMLNode)
    ['{1A97FEB9-B123-409B-B889-BD3B336871A9}']
    { Property Accessors }
    function Get_ExistEZParcels: IXMLTExistEZParcel;
    function Get_ExistEZEntryParcels: IXMLSTD_MP_Package_SpecifyParcelsApproximal_ExistEZ_ExistEZEntryParcelsList;
    { Methods & Properties }
    property ExistEZParcels: IXMLTExistEZParcel read Get_ExistEZParcels;
    property ExistEZEntryParcels: IXMLSTD_MP_Package_SpecifyParcelsApproximal_ExistEZ_ExistEZEntryParcelsList read Get_ExistEZEntryParcels;
  end;

{ IXMLSTD_MP_Package_SpecifyParcelsApproximal_ExistEZ_ExistEZEntryParcels }

  IXMLSTD_MP_Package_SpecifyParcelsApproximal_ExistEZ_ExistEZEntryParcels = interface(IXMLNode)
    ['{3491E912-5024-4323-889D-E152CE2D5058}']
    { Property Accessors }
    function Get_ExistEZEntryParcel: IXMLTExistEZEntryParcel;
    { Methods & Properties }
    property ExistEZEntryParcel: IXMLTExistEZEntryParcel read Get_ExistEZEntryParcel;
  end;

{ IXMLSTD_MP_Package_SpecifyParcelsApproximal_ExistEZ_ExistEZEntryParcelsList }

  IXMLSTD_MP_Package_SpecifyParcelsApproximal_ExistEZ_ExistEZEntryParcelsList = interface(IXMLNodeCollection)
    ['{7874CDFD-123D-468D-8D28-91E6224FE433}']
    { Methods & Properties }
    function Add: IXMLSTD_MP_Package_SpecifyParcelsApproximal_ExistEZ_ExistEZEntryParcels;
    function Insert(const Index: Integer): IXMLSTD_MP_Package_SpecifyParcelsApproximal_ExistEZ_ExistEZEntryParcels;

    function Get_Item(Index: Integer): IXMLSTD_MP_Package_SpecifyParcelsApproximal_ExistEZ_ExistEZEntryParcels;
    property Items[Index: Integer]: IXMLSTD_MP_Package_SpecifyParcelsApproximal_ExistEZ_ExistEZEntryParcels read Get_Item; default;
  end;

{ IXMLCoord_Systems }

  IXMLCoord_Systems = interface(IXMLNode)
    ['{389C5D1F-8862-4CB0-9673-F4E267E36794}']
    { Property Accessors }
    function Get_Coord_System: IXMLCoord_System;
    { Methods & Properties }
    property Coord_System: IXMLCoord_System read Get_Coord_System;
  end;

{ IXMLCoord_System }

  IXMLCoord_System = interface(IXMLNode)
    ['{03C51DFC-081B-4086-88D1-05DD8D395B0A}']
    { Property Accessors }
    function Get_Name: UnicodeString;
    function Get_Cs_Id: UnicodeString;
    procedure Set_Name(Value: UnicodeString);
    procedure Set_Cs_Id(Value: UnicodeString);
    { Methods & Properties }
    property Name: UnicodeString read Get_Name write Set_Name;
    property Cs_Id: UnicodeString read Get_Cs_Id write Set_Cs_Id;
  end;

{ IXMLSTD_MP_Input_Data }

  IXMLSTD_MP_Input_Data = interface(IXMLNode)
    ['{5A755148-F728-4F88-8169-A235311FF565}']
    { Property Accessors }
    function Get_Documents: IXMLSTD_MP_Input_Data_Documents;
    function Get_Geodesic_Bases: IXMLSTD_MP_Input_Data_Geodesic_Bases;
    function Get_Means_Survey: IXMLSTD_MP_Input_Data_Means_Survey;
    function Get_Realty: IXMLSTD_MP_Input_Data_Realty;
    function Get_SubParcels: IXMLSTD_MP_Input_Data_SubParcels;
    { Methods & Properties }
    property Documents: IXMLSTD_MP_Input_Data_Documents read Get_Documents;
    property Geodesic_Bases: IXMLSTD_MP_Input_Data_Geodesic_Bases read Get_Geodesic_Bases;
    property Means_Survey: IXMLSTD_MP_Input_Data_Means_Survey read Get_Means_Survey;
    property Realty: IXMLSTD_MP_Input_Data_Realty read Get_Realty;
    property SubParcels: IXMLSTD_MP_Input_Data_SubParcels read Get_SubParcels;
  end;

{ IXMLSTD_MP_Input_Data_Documents }

  IXMLSTD_MP_Input_Data_Documents = interface(IXMLNodeCollection)
    ['{DF750EB9-F0CA-4FC6-864C-AED687DD2478}']
    { Property Accessors }
    function Get_Document(Index: Integer): IXMLSTD_MP_Input_Data_Documents_Document;
    { Methods & Properties }
    function Add: IXMLSTD_MP_Input_Data_Documents_Document;
    function Insert(const Index: Integer): IXMLSTD_MP_Input_Data_Documents_Document;
    property Document[Index: Integer]: IXMLSTD_MP_Input_Data_Documents_Document read Get_Document; default;
  end;

{ IXMLSTD_MP_Input_Data_Documents_Document }

  IXMLSTD_MP_Input_Data_Documents_Document = interface(IXMLTDocument)
    ['{C9C14BE1-EFE7-4E79-99C2-AB1D40AF22D4}']
    { Property Accessors }
    function Get_Scale: UnicodeString;
    function Get_Date_Create: UnicodeString;
    function Get_Date_Update: UnicodeString;
    procedure Set_Scale(Value: UnicodeString);
    procedure Set_Date_Create(Value: UnicodeString);
    procedure Set_Date_Update(Value: UnicodeString);
    { Methods & Properties }
    property Scale: UnicodeString read Get_Scale write Set_Scale;
    property Date_Create: UnicodeString read Get_Date_Create write Set_Date_Create;
    property Date_Update: UnicodeString read Get_Date_Update write Set_Date_Update;
  end;

{ IXMLSTD_MP_Input_Data_Geodesic_Bases }

  IXMLSTD_MP_Input_Data_Geodesic_Bases = interface(IXMLNodeCollection)
    ['{C6078713-3D97-4E46-83AA-7B9E2D91CE3A}']
    { Property Accessors }
    function Get_Geodesic_Base(Index: Integer): IXMLSTD_MP_Input_Data_Geodesic_Bases_Geodesic_Base;
    { Methods & Properties }
    function Add: IXMLSTD_MP_Input_Data_Geodesic_Bases_Geodesic_Base;
    function Insert(const Index: Integer): IXMLSTD_MP_Input_Data_Geodesic_Bases_Geodesic_Base;
    property Geodesic_Base[Index: Integer]: IXMLSTD_MP_Input_Data_Geodesic_Bases_Geodesic_Base read Get_Geodesic_Base; default;
  end;

{ IXMLSTD_MP_Input_Data_Geodesic_Bases_Geodesic_Base }

  IXMLSTD_MP_Input_Data_Geodesic_Bases_Geodesic_Base = interface(IXMLNode)
    ['{8CF699A0-7EB3-4AC1-97E5-E8C21569D011}']
    { Property Accessors }
    function Get_PName: UnicodeString;
    function Get_PKind: UnicodeString;
    function Get_PKlass: UnicodeString;
    function Get_OrdX: UnicodeString;
    function Get_OrdY: UnicodeString;
    procedure Set_PName(Value: UnicodeString);
    procedure Set_PKind(Value: UnicodeString);
    procedure Set_PKlass(Value: UnicodeString);
    procedure Set_OrdX(Value: UnicodeString);
    procedure Set_OrdY(Value: UnicodeString);
    { Methods & Properties }
    property PName: UnicodeString read Get_PName write Set_PName;
    property PKind: UnicodeString read Get_PKind write Set_PKind;
    property PKlass: UnicodeString read Get_PKlass write Set_PKlass;
    property OrdX: UnicodeString read Get_OrdX write Set_OrdX;
    property OrdY: UnicodeString read Get_OrdY write Set_OrdY;
  end;

{ IXMLSTD_MP_Input_Data_Means_Survey }

  IXMLSTD_MP_Input_Data_Means_Survey = interface(IXMLNodeCollection)
    ['{4D10E231-D87B-4C4D-9DDE-FB4E71B00853}']
    { Property Accessors }
    function Get_Means_Survey(Index: Integer): IXMLSTD_MP_Input_Data_Means_Survey_Means_Survey;
    { Methods & Properties }
    function Add: IXMLSTD_MP_Input_Data_Means_Survey_Means_Survey;
    function Insert(const Index: Integer): IXMLSTD_MP_Input_Data_Means_Survey_Means_Survey;
    property Means_Survey[Index: Integer]: IXMLSTD_MP_Input_Data_Means_Survey_Means_Survey read Get_Means_Survey; default;
  end;

{ IXMLSTD_MP_Input_Data_Means_Survey_Means_Survey }

  IXMLSTD_MP_Input_Data_Means_Survey_Means_Survey = interface(IXMLNode)
    ['{A9B7D262-7C4F-4196-8FDC-CC52DAAFBCE0}']
    { Property Accessors }
    function Get_Name: UnicodeString;
    function Get_Certificate: UnicodeString;
    function Get_Certificate_Verification: UnicodeString;
    procedure Set_Name(Value: UnicodeString);
    procedure Set_Certificate(Value: UnicodeString);
    procedure Set_Certificate_Verification(Value: UnicodeString);
    { Methods & Properties }
    property Name: UnicodeString read Get_Name write Set_Name;
    property Certificate: UnicodeString read Get_Certificate write Set_Certificate;
    property Certificate_Verification: UnicodeString read Get_Certificate_Verification write Set_Certificate_Verification;
  end;

{ IXMLSTD_MP_Input_Data_Realty }

  IXMLSTD_MP_Input_Data_Realty = interface(IXMLNodeCollection)
    ['{E0D10E4B-3E6B-4121-AC8C-F49D785F7C07}']
    { Property Accessors }
    function Get_OKS(Index: Integer): IXMLSTD_MP_Input_Data_Realty_OKS;
    { Methods & Properties }
    function Add: IXMLSTD_MP_Input_Data_Realty_OKS;
    function Insert(const Index: Integer): IXMLSTD_MP_Input_Data_Realty_OKS;
    property OKS[Index: Integer]: IXMLSTD_MP_Input_Data_Realty_OKS read Get_OKS; default;
  end;

{ IXMLSTD_MP_Input_Data_Realty_OKS }

  IXMLSTD_MP_Input_Data_Realty_OKS = interface(IXMLNode)
    ['{BB3CE0F3-49FF-42E9-A424-C5DA1C4FC5DB}']
    { Property Accessors }
    function Get_CadastralNumber: UnicodeString;
    function Get_CadastralNumber_OtherNumber: UnicodeString;
    function Get_Name_OKS: UnicodeString;
    procedure Set_CadastralNumber(Value: UnicodeString);
    procedure Set_CadastralNumber_OtherNumber(Value: UnicodeString);
    procedure Set_Name_OKS(Value: UnicodeString);
    { Methods & Properties }
    property CadastralNumber: UnicodeString read Get_CadastralNumber write Set_CadastralNumber;
    property CadastralNumber_OtherNumber: UnicodeString read Get_CadastralNumber_OtherNumber write Set_CadastralNumber_OtherNumber;
    property Name_OKS: UnicodeString read Get_Name_OKS write Set_Name_OKS;
  end;

{ IXMLSTD_MP_Input_Data_SubParcels }

  IXMLSTD_MP_Input_Data_SubParcels = interface(IXMLNodeCollection)
    ['{99E5B3D1-436D-4763-94D6-BCECED5D2020}']
    { Property Accessors }
    function Get_SubParcel(Index: Integer): IXMLSTD_MP_Input_Data_SubParcels_SubParcel;
    { Methods & Properties }
    function Add: IXMLSTD_MP_Input_Data_SubParcels_SubParcel;
    function Insert(const Index: Integer): IXMLSTD_MP_Input_Data_SubParcels_SubParcel;
    property SubParcel[Index: Integer]: IXMLSTD_MP_Input_Data_SubParcels_SubParcel read Get_SubParcel; default;
  end;

{ IXMLSTD_MP_Input_Data_SubParcels_SubParcel }

  IXMLSTD_MP_Input_Data_SubParcels_SubParcel = interface(IXMLNode)
    ['{06FF7409-2579-4870-BE04-F5E03558230D}']
    { Property Accessors }
    function Get_CadastralNumber: UnicodeString;
    function Get_Number_Record: UnicodeString;
    procedure Set_CadastralNumber(Value: UnicodeString);
    procedure Set_Number_Record(Value: UnicodeString);
    { Methods & Properties }
    property CadastralNumber: UnicodeString read Get_CadastralNumber write Set_CadastralNumber;
    property Number_Record: UnicodeString read Get_Number_Record write Set_Number_Record;
  end;

{ IXMLSTD_MP_Survey }

  IXMLSTD_MP_Survey = interface(IXMLNodeCollection)
    ['{1280F655-1869-4446-9C2D-61251E6C9FDD}']
    { Property Accessors }
    function Get_AppliedFile(Index: Integer): IXMLSTD_MP_Survey_AppliedFile;
    { Methods & Properties }
    function Add: IXMLSTD_MP_Survey_AppliedFile;
    function Insert(const Index: Integer): IXMLSTD_MP_Survey_AppliedFile;
    property AppliedFile[Index: Integer]: IXMLSTD_MP_Survey_AppliedFile read Get_AppliedFile; default;
  end;

{ IXMLSTD_MP_Survey_AppliedFile }

  IXMLSTD_MP_Survey_AppliedFile = interface(IXMLTAppliedFile)
    ['{3410320D-FFD8-44F3-B6D9-A2B749DAB945}']
  end;

{ IXMLSTD_MP_Scheme_Geodesic_Plotting }

  IXMLSTD_MP_Scheme_Geodesic_Plotting = interface(IXMLNodeCollection)
    ['{7D29F6BA-9487-4CEA-AC9E-D8844629A8EE}']
    { Property Accessors }
    function Get_AppliedFile(Index: Integer): IXMLSTD_MP_Survey_AppliedFile;
    { Methods & Properties }
    function Add: IXMLSTD_MP_Survey_AppliedFile;
    function Insert(const Index: Integer): IXMLSTD_MP_Survey_AppliedFile;
    property AppliedFile[Index: Integer]: IXMLSTD_MP_Survey_AppliedFile read Get_AppliedFile; default;
  end;

{ IXMLSTD_MP_Scheme_Geodesic_Plotting_AppliedFile }

  IXMLSTD_MP_Scheme_Geodesic_Plotting_AppliedFile = interface(IXMLTAppliedFile)
    ['{EE3F4922-575A-421E-9E6C-636DBF87B9FE}']
  end;

{ IXMLSTD_MP_Scheme_Disposition_Parcels }

  IXMLSTD_MP_Scheme_Disposition_Parcels = interface(IXMLNodeCollection)
    ['{6FDA03E5-FA4A-42F4-A210-D333F36ED3E4}']
    { Property Accessors }
    function Get_AppliedFile(Index: Integer): IXMLSTD_MP_Survey_AppliedFile;
    { Methods & Properties }
    function Add: IXMLSTD_MP_Survey_AppliedFile;
    function Insert(const Index: Integer): IXMLSTD_MP_Survey_AppliedFile;
    property AppliedFile[Index: Integer]: IXMLSTD_MP_Survey_AppliedFile read Get_AppliedFile; default;
  end;

{ IXMLSTD_MP_Scheme_Disposition_Parcels_AppliedFile }

  IXMLSTD_MP_Scheme_Disposition_Parcels_AppliedFile = interface(IXMLTAppliedFile)
    ['{90CB06C0-380F-4768-883D-AB31A7714111}']
  end;

{ IXMLSTD_MP_Diagram_Parcels_SubParcels }

  IXMLSTD_MP_Diagram_Parcels_SubParcels = interface(IXMLNodeCollection)
    ['{1E9740E8-B690-4911-B600-D6D1DE422DAA}']
    { Property Accessors }
    function Get_AppliedFile(Index: Integer): IXMLSTD_MP_Survey_AppliedFile; //todo
    { Methods & Properties }
    function Add: IXMLSTD_MP_Survey_AppliedFile;
    function Insert(const Index: Integer): IXMLSTD_MP_Survey_AppliedFile;
    property AppliedFile[Index: Integer]: IXMLSTD_MP_Survey_AppliedFile read Get_AppliedFile; default;
  end;

{ IXMLSTD_MP_Diagram_Parcels_SubParcels_AppliedFile }

  IXMLSTD_MP_Diagram_Parcels_SubParcels_AppliedFile = interface(IXMLTAppliedFile)
    ['{60DBE09B-7ADF-4A52-9ADC-368EB9F551AE}']
  end;

{ IXMLSTD_MP_Agreement_Document }

  IXMLSTD_MP_Agreement_Document = interface(IXMLNodeCollection)
    ['{E454990A-46CD-4921-9610-C7572CA9A82B}']
    { Property Accessors }
    function Get_AppliedFile(Index: Integer): IXMLSTD_MP_Survey_AppliedFile;
    { Methods & Properties }
    function Add: IXMLSTD_MP_Survey_AppliedFile;
    function Insert(const Index: Integer): IXMLSTD_MP_Survey_AppliedFile;
    property AppliedFile[Index: Integer]: IXMLSTD_MP_Survey_AppliedFile read Get_AppliedFile; default;
  end;

{ IXMLSTD_MP_Agreement_Document_AppliedFile }

  IXMLSTD_MP_Agreement_Document_AppliedFile = interface(IXMLTAppliedFile)
    ['{8B0F3B92-3455-4413-9A20-DDAD4FDC3D59}']
  end;

{ IXMLSTD_MP_NodalPointSchemes }

  IXMLSTD_MP_NodalPointSchemes = interface(IXMLNodeCollection)
    ['{6B7239AA-977D-48B8-9CFA-ECAC7361AA36}']
    { Property Accessors }
    function Get_NodalPointScheme(Index: Integer): IXMLSTD_MP_NodalPointSchemes_NodalPointScheme;
    { Methods & Properties }
    function Add: IXMLSTD_MP_NodalPointSchemes_NodalPointScheme;
    function Insert(const Index: Integer): IXMLSTD_MP_NodalPointSchemes_NodalPointScheme;
    property NodalPointScheme[Index: Integer]: IXMLSTD_MP_NodalPointSchemes_NodalPointScheme read Get_NodalPointScheme; default;
  end;

{ IXMLSTD_MP_NodalPointSchemes_NodalPointScheme }

  IXMLSTD_MP_NodalPointSchemes_NodalPointScheme = interface(IXMLNodeCollection)
    ['{107AFA25-F9CA-4188-B1FB-56C229C943B0}']
    { Property Accessors }
    function Get_Definition: UnicodeString;
    function Get_AppliedFile(Index: Integer):IXMLSTD_MP_Survey_AppliedFile; // IXMLSTD_MP_NodalPointSchemes_NodalPointScheme_AppliedFile;
    procedure Set_Definition(Value: UnicodeString);
    { Methods & Properties }
    function Add: IXMLSTD_MP_Survey_AppliedFile;
    function Insert(const Index: Integer): IXMLSTD_MP_Survey_AppliedFile;
    property Definition: UnicodeString read Get_Definition write Set_Definition;
    property AppliedFile[Index: Integer]: IXMLSTD_MP_Survey_AppliedFile read Get_AppliedFile; default;
  end;

{ IXMLSTD_MP_NodalPointSchemes_NodalPointScheme_AppliedFile }

  IXMLSTD_MP_NodalPointSchemes_NodalPointScheme_AppliedFile = interface(IXMLTAppliedFile)
    ['{82A1E645-685B-45C8-AE20-0D33B37964EE}']
  end;

{ IXMLSTD_MP_Appendix }

  IXMLSTD_MP_Appendix = interface(IXMLNodeCollection)
    ['{CC216F91-0437-4004-9ECE-74550CBF9D71}']
    { Property Accessors }
    function Get_Document(Index: Integer): IXMLTDocument;
    { Methods & Properties }
    function Add: IXMLTDocument;
    function Insert(const Index: Integer): IXMLTDocument;
    property Document[Index: Integer]: IXMLTDocument read Get_Document; default;
  end;

{ IXMLString_List }

  IXMLString_List = interface(IXMLNodeCollection)
    ['{D7E531D3-DA14-47B3-B0B5-359E1476BCF9}']
    { Methods & Properties }
    function Add(const Value: UnicodeString): IXMLNode;
    function Insert(const Index: Integer; const Value: UnicodeString): IXMLNode;

    function Get_Item(Index: Integer): UnicodeString;
    property Items[Index: Integer]: UnicodeString read Get_Item; default;
  end;

{ IXMLTChangeParcel_Inner_CadastralNumbers_CadastralNumberList }

  IXMLTChangeParcel_Inner_CadastralNumbers_CadastralNumberList = interface(IXMLNodeCollection)
    ['{23D5F24E-E082-467D-9F6A-D20201AEF3E0}']
    { Methods & Properties }
    function Add(const Value: UnicodeString): IXMLNode;
    function Insert(const Index: Integer; const Value: UnicodeString): IXMLNode;

    function Get_Item(Index: Integer): UnicodeString;
    property Items[Index: Integer]: UnicodeString read Get_Item; default;
  end;

{ IXMLTExistEZParcel_Inner_CadastralNumbers_CadastralNumberList }

  IXMLTExistEZParcel_Inner_CadastralNumbers_CadastralNumberList = interface(IXMLNodeCollection)
    ['{9DFD1F9A-2B3D-48AF-8D9F-334129F3C203}']
    { Methods & Properties }
    function Add(const Value: UnicodeString): IXMLNode;
    function Insert(const Index: Integer; const Value: UnicodeString): IXMLNode;

    function Get_Item(Index: Integer): UnicodeString;
    property Items[Index: Integer]: UnicodeString read Get_Item; default;
  end;

{ IXMLTExistParcel_Inner_CadastralNumbers_CadastralNumberList }

{  IXMLTExistParcel_Inner_CadastralNumbers_CadastralNumberList = interface(IXMLNodeCollection)

    { Methods & Properties }

{    function Add(const Value: UnicodeString): IXMLNode;
    function Insert(const Index: Integer; const Value: UnicodeString): IXMLNode;

    function Get_Item(Index: Integer): UnicodeString;
    property Items[Index: Integer]: UnicodeString read Get_Item; default;
  end;

{ IXMLTInner_CadastralNumbers_CadastralNumberList }

  IXMLTInner_CadastralNumbers_CadastralNumberList = interface(IXMLNodeCollection)
    ['{1DFE495F-3A96-4B17-BA0D-A4C13523C291}']
    { Methods & Properties }
    function Add(const Value: UnicodeString): IXMLNode;
    function Insert(const Index: Integer; const Value: UnicodeString): IXMLNode;

    function Get_Item(Index: Integer): UnicodeString;
    property Items[Index: Integer]: UnicodeString read Get_Item; default;
  end;

  { IXMLTInner_CadastralNumbers_CadastralNumberList }



{ IXMLTProviding_Pass_CadastralNumbers_CadastralNumberList }

  IXMLTProviding_Pass_CadastralNumbers_CadastralNumberList = interface(IXMLNodeCollection)
    ['{430288CC-D87C-4AD9-943B-CEBCEC3A5FDC}']
    { Methods & Properties }
    function Add(const Value: UnicodeString): IXMLNode;
    function Insert(const Index: Integer; const Value: UnicodeString): IXMLNode;

    function Get_Item(Index: Integer): UnicodeString;
    property Items[Index: Integer]: UnicodeString read Get_Item; default;
  end;

{ Forward Decls }

  TXMLSTD_MP = class;
  TXMLSTD_MP_eDocument = class;
  TXMLSTD_MP_Title = class;
  TXMLSTD_MP_Title_Contractor = class;
  TXMLTFIO = class;
  TXMLSTD_MP_Title_Client = class;
  TXMLSTD_MP_Title_ClientList = class;
  TXMLSTD_MP_Title_Client_Person = class;
  TXMLSTD_MP_Title_Client_Organization = class;
  TXMLSTD_MP_Title_Client_Organization_Agent = class;
  TXMLSTD_MP_Title_Client_Foreign_Organization = class;
  TXMLSTD_MP_Title_Client_Foreign_Organization_Agent = class;
  TXMLSTD_MP_Title_Client_Governance = class;
  TXMLSTD_MP_Title_Client_Governance_Agent = class;
  TXMLSTD_MP_Package = class;
  TXMLSTD_MP_Package_FormParcels = class;
  TXMLTNewParcel = class;
  TXMLTNewParcelList = class;
  TXMLTNewParcel_Prev_CadastralNumbers = class;
  TXMLTProviding_Pass_CadastralNumbers = class;
  TXMLTProviding_Pass_CadastralNumbers_Documents = class;
  TXMLTDocument = class;
  TXMLDuration = class;
  TXMLAppliedFiles = class;
  TXMLTAppliedFile = class;
  TXMLTInner_CadastralNumbers = class;


//  TXMLTAreaNew = class;
  TXMLTAddress = class;
  TXMLDistrict = class;
  TXMLCity = class;
  TXMLUrban_District = class;
  TXMLSoviet_Village = class;
  TXMLLocality = class;
  TXMLStreet = class;
  TXMLLevel1 = class;
  TXMLLevel2 = class;
  TXMLLevel3 = class;
  TXMLApartment = class;
  TXMLTNewParcel_Location = class;
  TXMLTCategory = class;
  TXMLTNatural_Object = class;
  TXMLTUtilization = class;
  TXMLTNewParcel_SubParcels = class;
  TXMLTSubParcel = class;
  TXMLTArea = class;
  TXMLTEncumbrance = class;
  TXMLTEncumbrance_Documents = class;
  TXMLEntity_Spatial = class;
  TXMLTSPATIAL_ELEMENT_OLD_NEW = class;
  TXMLTSPATIAL_ELEMENT_OLD_NEWList = class;
  TXMLTSPELEMENT_UNIT_OLD_NEW = class;
  TXMLTOrdinate = class;
  TXMLEntity_Spatial_Borders = class;
  TXMLTBorder = class;
  TXMLTBorder_Edge = class;
  TXMLEntity_Spatial_Borders_Border = class;
  TXMLTSubParcel_Contours = class;
  TXMLTSubParcel_Contours_Contour = class;
//  TXMLTArea_Contour = class;
  TXMLTSubParcel_Contours_Contour_Entity_Spatial = class;
  {$ifndef disableIXMLTNewParcel_Contours}
  TXMLTNewParcel_SubParcels_FormSubParcel = class;
  TXMLTNewParcel_Contours = class;
  {$endif}
  TXMLTNewParcel_Contours_NewContour = class;
  TXMLTContour = class;

  TXMLTArea_without_Innccuracy = class;
  TXMLTChangeParcel = class;
  TXMLTChangeParcelList = class;
  TXMLTChangeParcel_Inner_CadastralNumbers = class;
  {$IFNDEF  disableIXMLTChangeParcel_SubParcels}
  TXMLTChangeParcel_SubParcels = class;
  {$ENDIF}
  TXMLTChangeParcel_SubParcels_FormSubParcel = class;
  TXMLTChangeParcel_SubParcels_FormSubParcelList = class;
  TXMLTChangeParcel_SubParcels_ExistSubParcel = class;
  TXMLTChangeParcel_SubParcels_ExistSubParcelList = class;
  TXMLTChangeParcel_SubParcels_InvariableSubParcel = class;
  TXMLTChangeParcel_SubParcels_InvariableSubParcelList = class;
  TXMLTChangeParcel_SubParcels_InvariableSubParcel_Area = class;
  TXMLTChangeParcel_SubParcels_InvariableSubParcel_Contours = class;
  TXMLTChangeParcel_SubParcels_InvariableSubParcel_Contours_Contour = class;
  TXMLTChangeParcel_DeleteEntryParcels = class;
  TXMLTChangeParcel_DeleteEntryParcels_DeleteEntryParcel = class;
  TXMLTSpecifyRelatedParcel = class;
  TXMLTSpecifyRelatedParcel_AllBorder = class;
  //TXMLTSpecifyRelatedParcel_ChangeBorder = class;
  TXMLTChangeBorder = class;
  TXMLTSpecifyRelatedParcel_ChangeBorderList = class;
  TXMLTSpecifyRelatedParcel_Contours = class;
  TXMLTSpecifyRelatedParcel_Contours_NewContour = class;
  TXMLTSpecifyRelatedParcel_DeleteAllBorder = class;
  TXMLTSpecifyRelatedParcel_DeleteAllBorderList = class;
  TXMLTSpecifyRelatedParcel_ExistSubParcels = class;
  TXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel = class;
  TXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Entity_Spatial = class;
  TXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Contours = class;
  TXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Contours_Contour = class;
  TXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Contours_Contour_Entity_Spatial = class;
  TXMLSTD_MP_Package_FormParcels_SpecifyRelatedParcel = class;
  TXMLSTD_MP_Package_FormParcels_SpecifyRelatedParcelList = class;

  TXMLSTD_MP_Package_SpecifyParcel = class;
  TXMLTExistParcel = class;

//  TXMLTExistParcel_Inner_CadastralNumbers = class;
  TXMLTExistParcel_SubParcels = class;
  TXMLTExistParcel_SubParcels_FormSubParcel = class;
  TXMLTExistParcel_SubParcels_FormSubParcelList = class;
  TXMLTExistParcel_SubParcels_ExistSubParcel = class;
  TXMLTExistParcel_SubParcels_ExistSubParcelList = class;
  TXMLTExistParcel_SubParcels_InvariableSubParcel = class;
  TXMLTExistParcel_SubParcels_InvariableSubParcelList = class;
  TXMLTExistParcel_SubParcels_InvariableSubParcel_Area = class;
  TXMLTExistParcel_SubParcels_InvariableSubParcel_Contours = class;
  TXMLTExistParcel_SubParcels_InvariableSubParcel_Contours_Contour = class;
  TXMLContours = class;
  TXMLContours_NewContour = class;
  TXMLContours_NewContourList = class;
  TXMLContours_ExistContour = class;
  TXMLContours_ExistContourList = class;
  TXMLContours_DeleteAllBorder = class;
  TXMLContours_DeleteAllBorderList = class;
  TXMLRelatedParcels = class;
  TXMLRelatedParcels_ParcelNeighbours = class;
  TXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour = class;
  TXMLRelatedParcels_ParcelNeighbours_ParcelNeighbourList = class;
  TXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours = class;
  TXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbour = class;
  TXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbourList = class;
  TXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbour_Documents = class;
  TXMLSTD_MP_Package_SpecifyParcel_ExistEZ = class;
  TXMLTExistEZParcel = class;
  TXMLTExistEZParcel_Inner_CadastralNumbers = class;
  TXMLTExistEZParcel_SubParcels = class;
  TXMLTExistEZParcel_SubParcels_FormSubParcel = class;
  TXMLTExistEZParcel_SubParcels_FormSubParcelList = class;
  TXMLTExistEZParcel_SubParcels_ExistSubParcel = class;
  TXMLTExistEZParcel_SubParcels_ExistSubParcelList = class;
  TXMLTExistEZParcel_SubParcels_InvariableSubParcel = class;
  TXMLTExistEZParcel_SubParcels_InvariableSubParcelList = class;
  TXMLTExistEZParcel_SubParcels_InvariableSubParcel_Area = class;
  TXMLTExistEZParcel_SubParcels_InvariableSubParcel_Contours = class;
  TXMLTExistEZParcel_SubParcels_InvariableSubParcel_Contours_Contour = class;
  TXMLTExistEZParcel_Composition_EZ = class;
  TXMLTExistEZParcel_Composition_EZ_InsertEntryParcels = class;
  TXMLTExistEZParcel_Composition_EZ_InsertEntryParcels_InsertEntryParcel = class;
  TXMLTExistEZParcel_Composition_EZ_InsertEntryParcels_InsertEntryParcel_ExistEntryParcel = class;
  TXMLTNewEZEntryParcel = class;
  TXMLTExistEZParcel_Composition_EZ_DeleteEntryParcels = class;
  TXMLTExistEZParcel_Composition_EZ_DeleteEntryParcels_DeleteEntryParcel = class;
  TXMLSTD_MP_Package_SpecifyParcel_ExistEZ_ExistEZEntryParcels = class;
  TXMLTExistEZEntryParcel = class;
  TXMLSTD_MP_Package_SpecifyParcel_SpecifyRelatedParcel = class;
  TXMLSTD_MP_Package_SpecifyParcel_SpecifyRelatedParcelList = class;
  TXMLTNewSubParcel = class;
  TXMLTNewSubParcelList = class;
  {$IFNDEF disableIXMLTNewSubParcel_Entity_Spatial}
  TXMLTNewSubParcel_Entity_Spatial = class;
  {$ENDIF}
  {$IFNDEF disableIXMLTNewParcel_Contours}
  TXMLTNewSubParcel_Contours = class;
  {$ENDIF}
  TXMLTNewSubParcel_Contours_Contour = class;
  TXMLTNewSubParcel_Contours_Contour_Entity_Spatial = class;
  TXMLSTD_MP_Package_SpecifyParcelsApproximal = class;
  TXMLSTD_MP_Package_SpecifyParcelsApproximalList = class;
  TXMLSTD_MP_Package_SpecifyParcelsApproximal_ExistEZ = class;
  TXMLSTD_MP_Package_SpecifyParcelsApproximal_ExistEZ_ExistEZEntryParcels = class;
  TXMLSTD_MP_Package_SpecifyParcelsApproximal_ExistEZ_ExistEZEntryParcelsList = class;
  TXMLCoord_Systems = class;
  TXMLCoord_System = class;
  TXMLSTD_MP_Input_Data = class;
  TXMLSTD_MP_Input_Data_Documents = class;
  TXMLSTD_MP_Input_Data_Documents_Document = class;
  TXMLSTD_MP_Input_Data_Geodesic_Bases = class;
  TXMLSTD_MP_Input_Data_Geodesic_Bases_Geodesic_Base = class;
  TXMLSTD_MP_Input_Data_Means_Survey = class;
  TXMLSTD_MP_Input_Data_Means_Survey_Means_Survey = class;
  TXMLSTD_MP_Input_Data_Realty = class;
  TXMLSTD_MP_Input_Data_Realty_OKS = class;
  TXMLSTD_MP_Input_Data_SubParcels = class;
  TXMLSTD_MP_Input_Data_SubParcels_SubParcel = class;
  TXMLSTD_MP_Survey = class;
  TXMLSTD_MP_Survey_AppliedFile = class;
  TXMLSTD_MP_Scheme_Geodesic_Plotting = class;
  TXMLSTD_MP_Scheme_Geodesic_Plotting_AppliedFile = class;
  TXMLSTD_MP_Scheme_Disposition_Parcels = class;
  TXMLSTD_MP_Scheme_Disposition_Parcels_AppliedFile = class;
  TXMLSTD_MP_Diagram_Parcels_SubParcels = class;
  TXMLSTD_MP_Diagram_Parcels_SubParcels_AppliedFile = class;
  TXMLSTD_MP_Agreement_Document = class;
  TXMLSTD_MP_Agreement_Document_AppliedFile = class;
  TXMLSTD_MP_NodalPointSchemes = class;
  TXMLSTD_MP_NodalPointSchemes_NodalPointScheme = class;
  TXMLSTD_MP_NodalPointSchemes_NodalPointScheme_AppliedFile = class;
  TXMLSTD_MP_Appendix = class;
  TXMLString_List = class;
  TXMLTChangeParcel_Inner_CadastralNumbers_CadastralNumberList = class;
  TXMLTExistEZParcel_Inner_CadastralNumbers_CadastralNumberList = class;

//  TXMLTExistParcel_Inner_CadastralNumbers_CadastralNumberList = class;

  TXMLTInner_CadastralNumbers_CadastralNumberList = class;
  TXMLTProviding_Pass_CadastralNumbers_CadastralNumberList = class;

{ TXMLSTD_MP }

  TXMLSTD_MP = class(TXMLNode, IXMLSTD_MP)
  protected
    { IXMLSTD_MP }
    function Get_EDocument: IXMLSTD_MP_eDocument;
    function Get_Title: IXMLSTD_MP_Title;
    function Get_Package: IXMLSTD_MP_Package;
    function Get_Coord_Systems: IXMLCoord_Systems;
    function Get_Input_Data: IXMLSTD_MP_Input_Data;
    function Get_Survey: IXMLSTD_MP_Survey;
    function Get_Conclusion: UnicodeString;
    function Get_Scheme_Geodesic_Plotting: IXMLSTD_MP_Scheme_Geodesic_Plotting;
    function Get_Scheme_Disposition_Parcels: IXMLSTD_MP_Scheme_Disposition_Parcels;
    function Get_Diagram_Parcels_SubParcels: IXMLSTD_MP_Diagram_Parcels_SubParcels;
    function Get_Agreement_Document: IXMLSTD_MP_Agreement_Document;
    function Get_NodalPointSchemes: IXMLSTD_MP_NodalPointSchemes;
    function Get_Appendix: IXMLSTD_MP_Appendix;
    procedure Set_Conclusion(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLSTD_MP_eDocument }

  TXMLSTD_MP_eDocument = class(TXMLNode, IXMLSTD_MP_eDocument)
  protected
    { IXMLSTD_MP_eDocument }
    function Get_CodeType: UnicodeString;
    function Get_Version: UnicodeString;
    function Get_GUID: UnicodeString;
    procedure Set_CodeType(Value: UnicodeString);
    procedure Set_Version(Value: UnicodeString);
    procedure Set_GUID(Value: UnicodeString);
  end;

{ TXMLSTD_MP_Title }

  TXMLSTD_MP_Title = class(TXMLNode, IXMLSTD_MP_Title)
  private
    FClient: IXMLSTD_MP_Title_ClientList;
  protected
    { IXMLSTD_MP_Title }
    function Get_Contractor: IXMLSTD_MP_Title_Contractor;
    function Get_Purpose: UnicodeString;
    function Get_Reason: UnicodeString;
    function Get_Client: IXMLSTD_MP_Title_ClientList;
    procedure Set_Purpose(Value: UnicodeString);
    procedure Set_Reason(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLSTD_MP_Title_Contractor }

  TXMLSTD_MP_Title_Contractor = class(TXMLNode, IXMLSTD_MP_Title_Contractor)
  protected
    { IXMLSTD_MP_Title_Contractor }
    function Get_Date: UnicodeString;
    function Get_FIO: IXMLTFIO;
    function Get_N_Certificate: UnicodeString;
    function Get_Telephone: UnicodeString;
    function Get_Address: UnicodeString;
    function Get_E_mail: UnicodeString;
    function Get_Organization: UnicodeString;
    procedure Set_Date(Value: UnicodeString);
    procedure Set_N_Certificate(Value: UnicodeString);
    procedure Set_Telephone(Value: UnicodeString);
    procedure Set_Address(Value: UnicodeString);
    procedure Set_E_mail(Value: UnicodeString);
    procedure Set_Organization(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTFIO }

  TXMLTFIO = class(TXMLNode, IXMLTFIO)
  protected
    { IXMLTFIO }
    function Get_Surname: UnicodeString;
    function Get_First: UnicodeString;
    function Get_Patronymic: UnicodeString;
    procedure Set_Surname(Value: UnicodeString);
    procedure Set_First(Value: UnicodeString);
    procedure Set_Patronymic(Value: UnicodeString);
  end;

{ TXMLSTD_MP_Title_Client }

  TXMLSTD_MP_Title_Client = class(TXMLNode, IXMLSTD_MP_Title_Client)
  protected
    { IXMLSTD_MP_Title_Client }
    function Get_Date: UnicodeString;
    function Get_Person: IXMLSTD_MP_Title_Client_Person;
    function Get_Organization: IXMLSTD_MP_Title_Client_Organization;
    function Get_Foreign_Organization: IXMLSTD_MP_Title_Client_Foreign_Organization;
    function Get_Governance: IXMLSTD_MP_Title_Client_Governance;
    procedure Set_Date(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLSTD_MP_Title_ClientList }

  TXMLSTD_MP_Title_ClientList = class(TXMLNodeCollection, IXMLSTD_MP_Title_ClientList)
  protected
    { IXMLSTD_MP_Title_ClientList }
    function Add: IXMLSTD_MP_Title_Client;
    function Insert(const Index: Integer): IXMLSTD_MP_Title_Client;

    function Get_Item(Index: Integer): IXMLSTD_MP_Title_Client;
  end;

{ TXMLSTD_MP_Title_Client_Person }

  TXMLSTD_MP_Title_Client_Person = class(TXMLNode, IXMLSTD_MP_Title_Client_Person)
  protected
    { IXMLSTD_MP_Title_Client_Person }
    function Get_FIO: IXMLTFIO;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLSTD_MP_Title_Client_Organization }

  TXMLSTD_MP_Title_Client_Organization = class(TXMLNode, IXMLSTD_MP_Title_Client_Organization)
  protected
    { IXMLSTD_MP_Title_Client_Organization }
    function Get_Name: UnicodeString;
    function Get_Agent: IXMLSTD_MP_Title_Client_Organization_Agent;
    procedure Set_Name(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLSTD_MP_Title_Client_Organization_Agent }

  TXMLSTD_MP_Title_Client_Organization_Agent = class(TXMLTFIO, IXMLSTD_MP_Title_Client_Organization_Agent)
  protected
    { IXMLSTD_MP_Title_Client_Organization_Agent }
    function Get_Appointment: UnicodeString;
    procedure Set_Appointment(Value: UnicodeString);
  end;

{ TXMLSTD_MP_Title_Client_Foreign_Organization }

  TXMLSTD_MP_Title_Client_Foreign_Organization = class(TXMLNode, IXMLSTD_MP_Title_Client_Foreign_Organization)
  protected
    { IXMLSTD_MP_Title_Client_Foreign_Organization }
    function Get_Name: UnicodeString;
    function Get_Country: UnicodeString;
    function Get_Agent: IXMLSTD_MP_Title_Client_Organization_Agent;//TODO IXMLSTD_MP_Title_Client_Foreign_Organization_Agent;
    procedure Set_Name(Value: UnicodeString);
    procedure Set_Country(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLSTD_MP_Title_Client_Foreign_Organization_Agent }

  TXMLSTD_MP_Title_Client_Foreign_Organization_Agent = class(TXMLTFIO, IXMLSTD_MP_Title_Client_Foreign_Organization_Agent)
  protected
    { IXMLSTD_MP_Title_Client_Foreign_Organization_Agent }
    function Get_Appointment: UnicodeString;
    procedure Set_Appointment(Value: UnicodeString);
  end;

{ TXMLSTD_MP_Title_Client_Governance }

  TXMLSTD_MP_Title_Client_Governance = class(TXMLNode, IXMLSTD_MP_Title_Client_Governance)
  protected
    { IXMLSTD_MP_Title_Client_Governance }
    function Get_Name: UnicodeString;
    function Get_Agent: IXMLSTD_MP_Title_Client_Organization_Agent;
    procedure Set_Name(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLSTD_MP_Title_Client_Governance_Agent }

  TXMLSTD_MP_Title_Client_Governance_Agent = class(TXMLTFIO, IXMLSTD_MP_Title_Client_Governance_Agent)
  protected
    { IXMLSTD_MP_Title_Client_Governance_Agent }
    function Get_Appointment: UnicodeString;
    procedure Set_Appointment(Value: UnicodeString);
  end;

{ TXMLSTD_MP_Package }

  TXMLSTD_MP_Package = class(TXMLNode, IXMLSTD_MP_Package)
  private
    FNewSubParcel: IXMLTNewSubParcelList;
    FSpecifyParcelsApproximal: IXMLSTD_MP_Package_SpecifyParcelsApproximalList;
  protected
    { IXMLSTD_MP_Package }
    function Get_FormParcels: IXMLSTD_MP_Package_FormParcels;
    function Get_SpecifyParcel: IXMLSTD_MP_Package_SpecifyParcel;
    function Get_NewSubParcel: IXMLTNewSubParcelList;
    function Get_SpecifyParcelsApproximal: IXMLSTD_MP_Package_SpecifyParcelsApproximalList;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLSTD_MP_Package_FormParcels }

  TXMLSTD_MP_Package_FormParcels = class(TXMLNode, IXMLSTD_MP_Package_FormParcels)
  private
    FNewParcel: IXMLTNewParcelList;
    FChangeParcel: IXMLTChangeParcelList;
    //FSpecifyRelatedParcel: IXMLSTD_MP_Package_FormParcels_SpecifyRelatedParcelList;
    FSpecifyRelatedParcel: IXMLSTD_MP_Package_SpecifyParcel_SpecifyRelatedParcelList;
  protected
    { IXMLSTD_MP_Package_FormParcels }
    function Get_Method: UnicodeString;
    function Get_NewParcel: IXMLTNewParcelList;
    function Get_ChangeParcel: IXMLTChangeParcelList;
    //function Get_SpecifyRelatedParcel: IXMLSTD_MP_Package_FormParcels_SpecifyRelatedParcelList;
    function Get_SpecifyRelatedParcel: IXMLSTD_MP_Package_SpecifyParcel_SpecifyRelatedParcelList;
    procedure Set_Method(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTNewParcel }

  TXMLTNewParcel = class(TXMLNode, IXMLTNewParcel)
  protected
    { IXMLTNewParcel }
    function Get_Definition: UnicodeString;
    function Get_CadastralBlock: UnicodeString;
    function Get_Prev_CadastralNumbers: IXMLTNewParcel_Prev_CadastralNumbers;
    function Get_Providing_Pass_CadastralNumbers: IXMLTProviding_Pass_CadastralNumbers;
    function Get_Inner_CadastralNumbers: IXMLTInner_CadastralNumbers;
    function Get_Area: IXMLTArea;
    function Get_Location: IXMLTNewParcel_Location;
    function Get_Category: IXMLTCategory;
    function Get_NaturalObject: IXMLTNatural_Object;
    function Get_Utilization: IXMLTUtilization;
    function Get_SubParcels: IXMLTNewParcel_SubParcels;
    {$ifndef disableIXMLTNewParcel_Contours}
    function Get_Contours: IXMLTNewParcel_Contours;
    {$else}
    function Get_Contours: IXMLContours;
    {$endif}
    function Get_Entity_Spatial: IXMLEntity_Spatial;
    function Get_Min_Area: IXMLTArea_without_Innccuracy;
    function Get_Max_Area: IXMLTArea_without_Innccuracy;
    function Get_Note: UnicodeString;
    procedure Set_Definition(Value: UnicodeString);
    procedure Set_CadastralBlock(Value: UnicodeString);
    procedure Set_Note(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTNewParcelList }

  TXMLTNewParcelList = class(TXMLNodeCollection, IXMLTNewParcelList)
  protected
    { IXMLTNewParcelList }
    function Add: IXMLTNewParcel;
    function Insert(const Index: Integer): IXMLTNewParcel;

    function Get_Item(Index: Integer): IXMLTNewParcel;
  end;

{ TXMLTNewParcel_Prev_CadastralNumbers }

  TXMLTNewParcel_Prev_CadastralNumbers = class(TXMLNodeCollection, IXMLTNewParcel_Prev_CadastralNumbers)
  protected
    { IXMLTNewParcel_Prev_CadastralNumbers }
    function Get_CadastralNumber(Index: Integer): UnicodeString;
    function Add(const CadastralNumber: UnicodeString): IXMLNode;
    function Insert(const Index: Integer; const CadastralNumber: UnicodeString): IXMLNode;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTProviding_Pass_CadastralNumbers }

  TXMLTProviding_Pass_CadastralNumbers = class(TXMLNode, IXMLTProviding_Pass_CadastralNumbers)
  private
    FCadastralNumber: IXMLTProviding_Pass_CadastralNumbers_CadastralNumberList;
    FDefinition: IXMLString_List;
  protected
    { IXMLTProviding_Pass_CadastralNumbers }
    function Get_CadastralNumber: IXMLTProviding_Pass_CadastralNumbers_CadastralNumberList;
    function Get_Definition: IXMLString_List;
    function Get_Other: UnicodeString;
    function Get_Documents: IXMLTProviding_Pass_CadastralNumbers_Documents;
    procedure Set_Other(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTProviding_Pass_CadastralNumbers_Documents }

  TXMLTProviding_Pass_CadastralNumbers_Documents = class(TXMLNodeCollection, IXMLTProviding_Pass_CadastralNumbers_Documents)
  protected
    { IXMLTProviding_Pass_CadastralNumbers_Documents }
    function Get_Document(Index: Integer): IXMLTDocument;
    function Add: IXMLTDocument;
    function Insert(const Index: Integer): IXMLTDocument;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTDocument }

  TXMLTDocument = class(TXMLNode, IXMLTDocument)
  protected
    { IXMLTDocument }
    function Get_Code_Document: UnicodeString;
    function Get_Name: UnicodeString;
    function Get_Series: UnicodeString;
    function Get_Number: UnicodeString;
    function Get_Date: UnicodeString;
    function Get_IssueOrgan: UnicodeString;
    function Get_NumberReg: UnicodeString;
    function Get_DateReg: UnicodeString;
    function Get_Duration: IXMLDuration;
    function Get_Register: UnicodeString;
    function Get_Desc: UnicodeString;
    function Get_IssueOrgan_Code: UnicodeString;
    function Get_AppliedFiles: IXMLAppliedFiles;
    procedure Set_Code_Document(Value: UnicodeString);
    procedure Set_Name(Value: UnicodeString);
    procedure Set_Series(Value: UnicodeString);
    procedure Set_Number(Value: UnicodeString);
    procedure Set_Date(Value: UnicodeString);
    procedure Set_IssueOrgan(Value: UnicodeString);
    procedure Set_NumberReg(Value: UnicodeString);
    procedure Set_DateReg(Value: UnicodeString);
    procedure Set_Register(Value: UnicodeString);
    procedure Set_Desc(Value: UnicodeString);
    procedure Set_IssueOrgan_Code(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLDuration }

  TXMLDuration = class(TXMLNode, IXMLDuration)
  protected
    { IXMLDuration }
    function Get_Started: UnicodeString;
    function Get_Stopped: UnicodeString;
    procedure Set_Started(Value: UnicodeString);
    procedure Set_Stopped(Value: UnicodeString);
  end;

{ TXMLAppliedFiles }

  TXMLAppliedFiles = class(TXMLNodeCollection, IXMLAppliedFiles)
  protected
    { IXMLAppliedFiles }
    function Get_AppliedFile(Index: Integer): IXMLTAppliedFile;
    function Add: IXMLTAppliedFile;
    function Insert(const Index: Integer): IXMLTAppliedFile;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTAppliedFile }

  TXMLTAppliedFile = class(TXMLNode, IXMLTAppliedFile)
  protected
    { IXMLTAppliedFile }
    function Get_Kind: UnicodeString;
    function Get_Name: UnicodeString;
    procedure Set_Kind(Value: UnicodeString);
    procedure Set_Name(Value: UnicodeString);
  end;

{ TXMLTInner_CadastralNumbers }

  TXMLTInner_CadastralNumbers = class(TXMLNode, IXMLTInner_CadastralNumbers)
  private
    FCadastralNumber: IXMLTInner_CadastralNumbers_CadastralNumberList;
    FNumber: IXMLString_List;
  protected
    { IXMLTInner_CadastralNumbers }
    function Get_CadastralNumber: IXMLTInner_CadastralNumbers_CadastralNumberList;
    function Get_Number: IXMLString_List;
  public
    procedure AfterConstruction; override;
  end;


{ TXMLTAreaNew }

  TXMLTAreaNew = class(TXMLNode, IXMLTAreaNew)
  protected
    { IXMLTAreaNew }
    function Get_Area: LongWord;
    function Get_Unit_: UnicodeString;
    function Get_Innccuracy: LongWord;
    procedure Set_Area(Value: LongWord);
    procedure Set_Unit_(Value: UnicodeString);
    procedure Set_Innccuracy(Value: LongWord);
  end;

{ TXMLTAddress }

  TXMLTAddress = class(TXMLNode, IXMLTAddress)
  protected
    { IXMLTAddress }
    function Get_Code_OKATO: UnicodeString;
    function Get_Code_KLADR: UnicodeString;
    function Get_Postal_Code: UnicodeString;
    function Get_Region: UnicodeString;
    function Get_District: IXMLDistrict;
    function Get_City: IXMLCity;
    function Get_Urban_District: IXMLUrban_District;
    function Get_Soviet_Village: IXMLSoviet_Village;
    function Get_Locality: IXMLLocality;
    function Get_Street: IXMLStreet;
    function Get_Level1: IXMLLevel1;
    function Get_Level2: IXMLLevel2;
    function Get_Level3: IXMLLevel3;
    function Get_Apartment: IXMLApartment;
    function Get_Other: UnicodeString;
    function Get_Note: UnicodeString;
    procedure Set_Code_OKATO(Value: UnicodeString);
    procedure Set_Code_KLADR(Value: UnicodeString);
    procedure Set_Postal_Code(Value: UnicodeString);
    procedure Set_Region(Value: UnicodeString);
    procedure Set_Other(Value: UnicodeString);
    procedure Set_Note(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLDistrict }

  TXMLDistrict = class(TXMLNode, IXMLDistrict)
  protected
    { IXMLDistrict }
    function Get_Name: UnicodeString;
    function Get_Type_: UnicodeString;
    procedure Set_Name(Value: UnicodeString);
    procedure Set_Type_(Value: UnicodeString);
  end;

{ TXMLCity }

  TXMLCity = class(TXMLNode, IXMLCity)
  protected
    { IXMLCity }
    function Get_Name: UnicodeString;
    function Get_Type_: UnicodeString;
    procedure Set_Name(Value: UnicodeString);
    procedure Set_Type_(Value: UnicodeString);
  end;

{ TXMLUrban_District }

  TXMLUrban_District = class(TXMLNode, IXMLUrban_District)
  protected
    { IXMLUrban_District }
    function Get_Name: UnicodeString;
    function Get_Type_: UnicodeString;
    procedure Set_Name(Value: UnicodeString);
    procedure Set_Type_(Value: UnicodeString);
  end;

{ TXMLSoviet_Village }

  TXMLSoviet_Village = class(TXMLNode, IXMLSoviet_Village)
  protected
    { IXMLSoviet_Village }
    function Get_Name: UnicodeString;
    function Get_Type_: UnicodeString;
    procedure Set_Name(Value: UnicodeString);
    procedure Set_Type_(Value: UnicodeString);
  end;

{ TXMLLocality }

  TXMLLocality = class(TXMLNode, IXMLLocality)
  protected
    { IXMLLocality }
    function Get_Name: UnicodeString;
    function Get_Type_: UnicodeString;
    procedure Set_Name(Value: UnicodeString);
    procedure Set_Type_(Value: UnicodeString);
  end;

{ TXMLStreet }

  TXMLStreet = class(TXMLNode, IXMLStreet)
  protected
    { IXMLStreet }
    function Get_Name: UnicodeString;
    function Get_Type_: UnicodeString;
    procedure Set_Name(Value: UnicodeString);
    procedure Set_Type_(Value: UnicodeString);
  end;

{ TXMLLevel1 }

  TXMLLevel1 = class(TXMLNode, IXMLLevel1)
  protected
    { IXMLLevel1 }
    function Get_Type_: UnicodeString;
    function Get_Value: UnicodeString;
    procedure Set_Type_(Value: UnicodeString);
    procedure Set_Value(Value: UnicodeString);
  end;

{ TXMLLevel2 }

  TXMLLevel2 = class(TXMLNode, IXMLLevel2)
  protected
    { IXMLLevel2 }
    function Get_Type_: UnicodeString;
    function Get_Value: UnicodeString;
    procedure Set_Type_(Value: UnicodeString);
    procedure Set_Value(Value: UnicodeString);
  end;

{ TXMLLevel3 }

  TXMLLevel3 = class(TXMLNode, IXMLLevel3)
  protected
    { IXMLLevel3 }
    function Get_Type_: UnicodeString;
    function Get_Value: UnicodeString;
    procedure Set_Type_(Value: UnicodeString);
    procedure Set_Value(Value: UnicodeString);
  end;

{ TXMLApartment }

  TXMLApartment = class(TXMLNode, IXMLApartment)
  protected
    { IXMLApartment }
    function Get_Type_: UnicodeString;
    function Get_Value: UnicodeString;
    procedure Set_Type_(Value: UnicodeString);
    procedure Set_Value(Value: UnicodeString);
  end;

{ TXMLTNewParcel_Location }

  TXMLTNewParcel_Location = class(TXMLTAddress, IXMLTNewParcel_Location)
  protected
    { IXMLTNewParcel_Location }
    function Get_Document: IXMLTDocument;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTCategory }

  TXMLTCategory = class(TXMLNode, IXMLTCategory)
  protected
    { IXMLTCategory }
    function Get_Category: UnicodeString;
    function Get_DocCategory: IXMLTDocument;
    procedure Set_Category(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTNatural_Object }

  TXMLTNatural_Object = class(TXMLNode, IXMLTNatural_Object)
  protected
    { IXMLTNatural_Object }
    function Get_Name: UnicodeString;
    function Get_ForestUse: UnicodeString;
    function Get_Type_ProtectiveForest: UnicodeString;
    procedure Set_Name(Value: UnicodeString);
    procedure Set_ForestUse(Value: UnicodeString);
    procedure Set_Type_ProtectiveForest(Value: UnicodeString);
  end;

{ TXMLTUtilization }

  TXMLTUtilization = class(TXMLNode, IXMLTUtilization)
  protected
    { IXMLTUtilization }
    function Get_Utilization: UnicodeString;
    function Get_ByDoc: UnicodeString;
    function Get_AdditionalName: UnicodeString;
    function Get_DocUtilization: IXMLTDocument;
    procedure Set_Utilization(Value: UnicodeString);
    procedure Set_ByDoc(Value: UnicodeString);
    procedure Set_AdditionalName(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTNewParcel_SubParcels }

  TXMLTNewParcel_SubParcels = class(TXMLNodeCollection, IXMLTNewParcel_SubParcels)
  protected
    { IXMLTNewParcel_SubParcels }
    function Get_FormSubParcel(Index: Integer): IXMLTNewParcel_SubParcels_FormSubParcel;
    function Add: IXMLTNewParcel_SubParcels_FormSubParcel;
    function Insert(const Index: Integer): IXMLTNewParcel_SubParcels_FormSubParcel;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTSubParcel }

  TXMLTSubParcel = class(TXMLNode, IXMLTSubParcel)
  protected
    { IXMLTSubParcel }
    function Get_SubParcel_Realty: Boolean;
    function Get_Area: IXMLTArea;
    function Get_Encumbrance: IXMLTEncumbrance;
    function Get_Entity_Spatial: IXMLEntity_Spatial;
    function Get_Contours: IXMLTSubParcel_Contours;
    procedure Set_SubParcel_Realty(Value: Boolean);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTArea }

  TXMLTArea = class(TXMLNode, IXMLTArea)
  protected
    { IXMLTArea }
    function Get_Area: UnicodeString;
    function Get_Unit_: UnicodeString;
    function Get_Innccuracy: LongWord;
    procedure Set_Area(Value: UnicodeString);
    procedure Set_Unit_(Value: UnicodeString);
    procedure Set_Innccuracy(Value: LongWord);
  end;

{ TXMLTEncumbrance }

  TXMLTEncumbrance = class(TXMLNode, IXMLTEncumbrance)
  protected
    { IXMLTEncumbrance }
    function Get_Name: UnicodeString;
    function Get_Type_: UnicodeString;
    function Get_CadastralNumber_Restriction: UnicodeString;
    function Get_Documents: IXMLTEncumbrance_Documents; //IXMLTEncumbrance_Documents;
    procedure Set_Name(Value: UnicodeString);
    procedure Set_Type_(Value: UnicodeString);
    procedure Set_CadastralNumber_Restriction(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTEncumbrance_Documents }

  TXMLTEncumbrance_Documents = class(TXMLNodeCollection, IXMLTEncumbrance_Documents)
  protected
    { IXMLTEncumbrance_Documents }
    function Get_Document(Index: Integer): IXMLTDocument;
    function Add: IXMLTDocument;
    function Insert(const Index: Integer): IXMLTDocument;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLEntity_Spatial }

  TXMLEntity_Spatial = class(TXMLNode, IXMLEntity_Spatial)
  private
    FSpatial_Element: IXMLTSPATIAL_ELEMENTList;
  protected
    { IXMLEntity_Spatial }
    function Get_Ent_Sys: UnicodeString;
    function Get_Spatial_Element: IXMLTSPATIAL_ELEMENTList;
    function Get_Borders: IXMLEntity_Spatial_Borders;
    procedure Set_Ent_Sys(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTSPATIAL_ELEMENT_OLD_NEW }

  TXMLTSPATIAL_ELEMENT_OLD_NEW = class(TXMLNodeCollection, IXMLTSPATIAL_ELEMENT)
  protected
    { IXMLTSPATIAL_ELEMENT }
    function Get_Spelement_Unit(Index: Integer): IXMLTSPELEMENT_UNIT;
    function Add: IXMLTSPELEMENT_UNIT;
    function Insert(const Index: Integer): IXMLTSPELEMENT_UNIT;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTSPATIAL_ELEMENT_OLD_NEWList }

  TXMLTSPATIAL_ELEMENT_OLD_NEWList = class(TXMLNodeCollection, IXMLTSPATIAL_ELEMENTList)
  protected
    { IXMLTSPATIAL_ELEMENTList }
    function Add: IXMLTSPATIAL_ELEMENT;
    function Insert(const Index: Integer): IXMLTSPATIAL_ELEMENT;

    function Get_Item(Index: Integer): IXMLTSPATIAL_ELEMENT;
  end;

{ TXMLTSPELEMENT_UNIT_OLD_NEW }

  TXMLTSPELEMENT_UNIT_OLD_NEW = class(TXMLNode, IXMLTSPELEMENT_UNIT)
  protected
    { IXMLTSPELEMENT_UNIT }
    function Get_Type_Unit: UnicodeString;
    function Get_NewOrdinate: IXMLTOrdinate;
    function Get_OldOrdinate: IXMLTOrdinate;
    procedure Set_Type_Unit(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTOrdinate }

  TXMLTOrdinate = class(TXMLNode, IXMLTOrdinate)
  protected
    { IXMLTOrdinate }
    function Get_X: UnicodeString;
    function Get_Y: UnicodeString;
    function Get_Num_Geopoint: LongWord;
    function Get_Geopoint_Zacrep: UnicodeString;
    function Get_Delta_Geopoint: UnicodeString;
    function Get_Point_Pref: UnicodeString;
    procedure Set_X(Value: UnicodeString);
    procedure Set_Y(Value: UnicodeString);
    procedure Set_Num_Geopoint(Value: LongWord);
    procedure Set_Geopoint_Zacrep(Value: UnicodeString);
    procedure Set_Delta_Geopoint(Value: UnicodeString);
    procedure Set_Point_Pref(Value: UnicodeString);
  end;

{ TXMLEntity_Spatial_Borders }

  TXMLEntity_Spatial_Borders = class(TXMLNodeCollection, IXMLEntity_Spatial_Borders)
  protected
    { IXMLEntity_Spatial_Borders }
    function Get_Border(Index: Integer): IXMLEntity_Spatial_Borders_Border;
    function Add: IXMLEntity_Spatial_Borders_Border;
    function Insert(const Index: Integer): IXMLEntity_Spatial_Borders_Border;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTBorder }

  TXMLTBorder = class(TXMLNode, IXMLTBorder)
  protected
    { IXMLTBorder }
    function Get_Spatial: Integer;
    function Get_Point1: Integer;
    function Get_Point2: Integer;
    function Get_ByDef: Boolean;
    function Get_Edge: IXMLTBorder_Edge;
    procedure Set_Spatial(Value: Integer);
    procedure Set_Point1(Value: Integer);
    procedure Set_Point2(Value: Integer);
    procedure Set_ByDef(Value: Boolean);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTBorder_Edge }

  TXMLTBorder_Edge = class(TXMLNode, IXMLTBorder_Edge)
  protected
    { IXMLTBorder_Edge }
    function Get_Length: UnicodeString;
    function Get_Definition: UnicodeString;
    procedure Set_Length(Value: UnicodeString);
    procedure Set_Definition(Value: UnicodeString);
  end;

{ TXMLEntity_Spatial_Borders_Border }

  TXMLEntity_Spatial_Borders_Border = class(TXMLTBorder, IXMLEntity_Spatial_Borders_Border)
  protected
    { IXMLEntity_Spatial_Borders_Border }
  end;

{ TXMLTSubParcel_Contours }

  TXMLTSubParcel_Contours = class(TXMLNodeCollection, IXMLTSubParcel_Contours)
  protected
    { IXMLTSubParcel_Contours }
    function Get_Contour(Index: Integer): IXMLTSubParcel_Contours_Contour;
    function Add: IXMLTSubParcel_Contours_Contour;
    function Insert(const Index: Integer): IXMLTSubParcel_Contours_Contour;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTSubParcel_Contours_Contour }

  TXMLTSubParcel_Contours_Contour = class(TXMLNode, IXMLTSubParcel_Contours_Contour)
  protected
    { IXMLTSubParcel_Contours_Contour }
    function Get_Number: UnicodeString;
    function Get_Area: IXMLTArea;
    function Get_Entity_Spatial: IXMLEntity_Spatial; //TODO:L:
    procedure Set_Number(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTArea_Contour }

  TXMLTArea_Contour = class(TXMLNode, IXMLTArea_Contour)
  protected
    { IXMLTArea_Contour }
    function Get_Area: UnicodeString;
    function Get_Unit_: UnicodeString;
    function Get_Innccuracy: LongWord;
    procedure Set_Area(Value: UnicodeString);
    procedure Set_Unit_(Value: UnicodeString);
    procedure Set_Innccuracy(Value: LongWord);
  end;

{ TXMLTSubParcel_Contours_Contour_Entity_Spatial }

  TXMLTSubParcel_Contours_Contour_Entity_Spatial = class(TXMLNodeCollection, IXMLTSubParcel_Contours_Contour_Entity_Spatial)
  protected
    { IXMLTSubParcel_Contours_Contour_Entity_Spatial }
    function Get_Ent_Sys: UnicodeString;
    function Get_Spatial_Element(Index: Integer): IXMLTSPATIAL_ELEMENT;
    procedure Set_Ent_Sys(Value: UnicodeString);
    function Add: IXMLTSPATIAL_ELEMENT;
    function Insert(const Index: Integer): IXMLTSPATIAL_ELEMENT;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTNewParcel_SubParcels_FormSubParcel }

  TXMLTNewParcel_SubParcels_FormSubParcel = class(TXMLTSubParcel, IXMLTNewParcel_SubParcels_FormSubParcel)
  protected
    { IXMLTNewParcel_SubParcels_FormSubParcel }
    function Get_Definition: UnicodeString;
    procedure Set_Definition(Value: UnicodeString);
  end;

{$ifndef disableIXMLTNewParcel_Contours}
{ TXMLTNewParcel_Contours }

  TXMLTNewParcel_Contours = class(TXMLNodeCollection, IXMLTNewParcel_Contours)
  protected
    { IXMLTNewParcel_Contours }
    function Get_NewContour(Index: Integer): IXMLTNewParcel_Contours_NewContour;
    function Add: IXMLTNewParcel_Contours_NewContour;
    function Insert(const Index: Integer): IXMLTNewParcel_Contours_NewContour;
  public
    procedure AfterConstruction; override;
  end;
{$endif}

{ TXMLTContour }

  TXMLTContour = class(TXMLNode, IXMLTContour)
  protected
    { IXMLTContour }
    function Get_Area: IXMLTArea; //TODO:L;
    function Get_Entity_Spatial: IXMLEntity_Spatial;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTNewParcel_Contours_NewContour }

  TXMLTNewParcel_Contours_NewContour = class(TXMLTContour, IXMLTNewParcel_Contours_NewContour)
  protected
    { IXMLTNewParcel_Contours_NewContour }
    function Get_Definition: UnicodeString;
    function Get_Providing_Pass_CadastralNumbers: IXMLTProviding_Pass_CadastralNumbers;
    procedure Set_Definition(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTArea_without_Innccuracy }

  TXMLTArea_without_Innccuracy = class(TXMLNode, IXMLTArea_without_Innccuracy)
  protected
    { IXMLTArea_without_Innccuracy }
    function Get_Area: LongWord;
    function Get_Unit_: UnicodeString;
    procedure Set_Area(Value: LongWord);
    procedure Set_Unit_(Value: UnicodeString);
  end;

{ TXMLTChangeParcel }

  TXMLTChangeParcel = class(TXMLNode, IXMLTChangeParcel)
  protected
    { IXMLTChangeParcel }
    function Get_CadastralNumber: UnicodeString;
    function Get_CadastralBlock: UnicodeString;
    function Get_Providing_Pass_CadastralNumbers: IXMLTProviding_Pass_CadastralNumbers;
    function Get_Inner_CadastralNumbers: IXMLTChangeParcel_Inner_CadastralNumbers;
    {$IFNDEF  disableIXMLTChangeParcel_SubParcels}    
    function Get_SubParcels: IXMLTChangeParcel_SubParcels;
    {$ELSE}
    function Get_SubParcels: IXMLTExistParcel_SubParcels;
    {$ENDIF}
    function Get_DeleteEntryParcels: IXMLTChangeParcel_DeleteEntryParcels;
    function Get_Note: UnicodeString;
    procedure Set_CadastralNumber(Value: UnicodeString);
    procedure Set_CadastralBlock(Value: UnicodeString);
    procedure Set_Note(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTChangeParcelList }

  TXMLTChangeParcelList = class(TXMLNodeCollection, IXMLTChangeParcelList)
  protected
    { IXMLTChangeParcelList }
    function Add: IXMLTChangeParcel;
    function Insert(const Index: Integer): IXMLTChangeParcel;

    function Get_Item(Index: Integer): IXMLTChangeParcel;
  end;

{ TXMLTChangeParcel_Inner_CadastralNumbers }

  TXMLTChangeParcel_Inner_CadastralNumbers = class(TXMLNode, IXMLTChangeParcel_Inner_CadastralNumbers)
  private
    FCadastralNumber: IXMLTChangeParcel_Inner_CadastralNumbers_CadastralNumberList;
    FNumber: IXMLString_List;
  protected
    { IXMLTChangeParcel_Inner_CadastralNumbers }
    function Get_CadastralNumber: IXMLTChangeParcel_Inner_CadastralNumbers_CadastralNumberList;
    function Get_Number: IXMLString_List;
  public
    procedure AfterConstruction; override;
  end;

  {$IFNDEF  disableIXMLTChangeParcel_SubParcels}
{ TXMLTChangeParcel_SubParcels }

  TXMLTChangeParcel_SubParcels = class(TXMLNode, IXMLTChangeParcel_SubParcels)
  private
    FFormSubParcel: IXMLTChangeParcel_SubParcels_FormSubParcelList;
    FExistSubParcel: IXMLTChangeParcel_SubParcels_ExistSubParcelList;
    FInvariableSubParcel: IXMLTChangeParcel_SubParcels_InvariableSubParcelList;
  protected
    { IXMLTChangeParcel_SubParcels }
    function Get_FormSubParcel: IXMLTChangeParcel_SubParcels_FormSubParcelList;
    function Get_ExistSubParcel: IXMLTChangeParcel_SubParcels_ExistSubParcelList;
    function Get_InvariableSubParcel: IXMLTChangeParcel_SubParcels_InvariableSubParcelList;
  public
    procedure AfterConstruction; override;
  end;
 {$ENDIF}
{ TXMLTChangeParcel_SubParcels_FormSubParcel }

  TXMLTChangeParcel_SubParcels_FormSubParcel = class(TXMLTSubParcel, IXMLTChangeParcel_SubParcels_FormSubParcel)
  protected
    { IXMLTChangeParcel_SubParcels_FormSubParcel }
    function Get_Definition: UnicodeString;
    procedure Set_Definition(Value: UnicodeString);
  end;

{ TXMLTChangeParcel_SubParcels_FormSubParcelList }

  TXMLTChangeParcel_SubParcels_FormSubParcelList = class(TXMLNodeCollection, IXMLTChangeParcel_SubParcels_FormSubParcelList)
  protected
    { IXMLTChangeParcel_SubParcels_FormSubParcelList }
    function Add: IXMLTChangeParcel_SubParcels_FormSubParcel;
    function Insert(const Index: Integer): IXMLTChangeParcel_SubParcels_FormSubParcel;

    function Get_Item(Index: Integer): IXMLTChangeParcel_SubParcels_FormSubParcel;
  end;

{ TXMLTChangeParcel_SubParcels_ExistSubParcel }

  TXMLTChangeParcel_SubParcels_ExistSubParcel = class(TXMLTSubParcel, IXMLTChangeParcel_SubParcels_ExistSubParcel)
  protected
    { IXMLTChangeParcel_SubParcels_ExistSubParcel }
    function Get_Number_Record: LongWord;
    function Get_CadastralNumber_EntryParcel: UnicodeString;
    procedure Set_Number_Record(Value: LongWord);
    procedure Set_CadastralNumber_EntryParcel(Value: UnicodeString);
  end;

{ TXMLTChangeParcel_SubParcels_ExistSubParcelList }

  TXMLTChangeParcel_SubParcels_ExistSubParcelList = class(TXMLNodeCollection, IXMLTChangeParcel_SubParcels_ExistSubParcelList)
  protected
    { IXMLTChangeParcel_SubParcels_ExistSubParcelList }
    function Add: IXMLTChangeParcel_SubParcels_ExistSubParcel;
    function Insert(const Index: Integer): IXMLTChangeParcel_SubParcels_ExistSubParcel;

    function Get_Item(Index: Integer): IXMLTChangeParcel_SubParcels_ExistSubParcel;
  end;

{ TXMLTChangeParcel_SubParcels_InvariableSubParcel }

  TXMLTChangeParcel_SubParcels_InvariableSubParcel = class(TXMLNode, IXMLTChangeParcel_SubParcels_InvariableSubParcel)
  protected
    { IXMLTChangeParcel_SubParcels_InvariableSubParcel }
    function Get_Number_Record: LongWord;
    function Get_SubParcel_Realty: Boolean;
    function Get_Area: IXMLTChangeParcel_SubParcels_InvariableSubParcel_Area;
    function Get_Encumbrance: IXMLTEncumbrance;
    function Get_Contours: IXMLTChangeParcel_SubParcels_InvariableSubParcel_Contours;
    procedure Set_Number_Record(Value: LongWord);
    procedure Set_SubParcel_Realty(Value: Boolean);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTChangeParcel_SubParcels_InvariableSubParcelList }

  TXMLTChangeParcel_SubParcels_InvariableSubParcelList = class(TXMLNodeCollection, IXMLTChangeParcel_SubParcels_InvariableSubParcelList)
  protected
    { IXMLTChangeParcel_SubParcels_InvariableSubParcelList }
    function Add: IXMLTChangeParcel_SubParcels_InvariableSubParcel;
    function Insert(const Index: Integer): IXMLTChangeParcel_SubParcels_InvariableSubParcel;

    function Get_Item(Index: Integer): IXMLTChangeParcel_SubParcels_InvariableSubParcel;
  end;

{ TXMLTChangeParcel_SubParcels_InvariableSubParcel_Area }

  TXMLTChangeParcel_SubParcels_InvariableSubParcel_Area = class(TXMLNode, IXMLTChangeParcel_SubParcels_InvariableSubParcel_Area)
  protected
    { IXMLTChangeParcel_SubParcels_InvariableSubParcel_Area }
    function Get_Area: LongWord;
    function Get_Unit_: UnicodeString;
    procedure Set_Area(Value: LongWord);
    procedure Set_Unit_(Value: UnicodeString);
  end;

{ TXMLTChangeParcel_SubParcels_InvariableSubParcel_Contours }

  TXMLTChangeParcel_SubParcels_InvariableSubParcel_Contours = class(TXMLNodeCollection, IXMLTChangeParcel_SubParcels_InvariableSubParcel_Contours)
  protected
    { IXMLTChangeParcel_SubParcels_InvariableSubParcel_Contours }
    function Get_Contour(Index: Integer): IXMLTChangeParcel_SubParcels_InvariableSubParcel_Contours_Contour;
    function Add: IXMLTChangeParcel_SubParcels_InvariableSubParcel_Contours_Contour;
    function Insert(const Index: Integer): IXMLTChangeParcel_SubParcels_InvariableSubParcel_Contours_Contour;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTChangeParcel_SubParcels_InvariableSubParcel_Contours_Contour }

  TXMLTChangeParcel_SubParcels_InvariableSubParcel_Contours_Contour = class(TXMLNode, IXMLTChangeParcel_SubParcels_InvariableSubParcel_Contours_Contour)
  protected
    { IXMLTChangeParcel_SubParcels_InvariableSubParcel_Contours_Contour }
    function Get_Number: UnicodeString;
    function Get_Area: IXMLTArea_Contour;
    procedure Set_Number(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTChangeParcel_DeleteEntryParcels }

  TXMLTChangeParcel_DeleteEntryParcels = class(TXMLNodeCollection, IXMLTChangeParcel_DeleteEntryParcels)
  protected
    { IXMLTChangeParcel_DeleteEntryParcels }
    function Get_DeleteEntryParcel(Index: Integer): IXMLTChangeParcel_DeleteEntryParcels_DeleteEntryParcel;
    function Add: IXMLTChangeParcel_DeleteEntryParcels_DeleteEntryParcel;
    function Insert(const Index: Integer): IXMLTChangeParcel_DeleteEntryParcels_DeleteEntryParcel;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTChangeParcel_DeleteEntryParcels_DeleteEntryParcel }

  TXMLTChangeParcel_DeleteEntryParcels_DeleteEntryParcel = class(TXMLNode, IXMLTChangeParcel_DeleteEntryParcels_DeleteEntryParcel)
  protected
    { IXMLTChangeParcel_DeleteEntryParcels_DeleteEntryParcel }
    function Get_CadastralNumber: UnicodeString;
    procedure Set_CadastralNumber(Value: UnicodeString);
  end;

{ TXMLTSpecifyRelatedParcel }

  TXMLTSpecifyRelatedParcel = class(TXMLNode, IXMLTSpecifyRelatedParcel)
  private
    FChangeBorder: IXMLTSpecifyRelatedParcel_ChangeBorderList;
    FDeleteAllBorder: IXMLTSpecifyRelatedParcel_DeleteAllBorderList;
  protected
    { IXMLTSpecifyRelatedParcel }
    function Get_CadastralNumber: UnicodeString;
    function Get_Number_Record: LongWord;
    function Get_AllBorder: IXMLTSpecifyRelatedParcel_AllBorder;
    function Get_ChangeBorder: IXMLTSpecifyRelatedParcel_ChangeBorderList;
    function Get_Contours: IXMLContours;
    function Get_DeleteAllBorder: IXMLTSpecifyRelatedParcel_DeleteAllBorderList;
    function Get_ExistSubParcels: IXMLTSpecifyRelatedParcel_ExistSubParcels;
    procedure Set_CadastralNumber(Value: UnicodeString);
    procedure Set_Number_Record(Value: LongWord);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTSpecifyRelatedParcel_AllBorder }

  TXMLTSpecifyRelatedParcel_AllBorder = class(TXMLNode, IXMLTSpecifyRelatedParcel_AllBorder)
  protected
    { IXMLTSpecifyRelatedParcel_AllBorder }
    function Get_Entity_Spatial: IXMLEntity_Spatial;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTSpecifyRelatedParcel_ChangeBorder }

  TXMLTSpecifyRelatedParcel_ChangeBorder = class(TXMLNode, IXMLTSpecifyRelatedParcel_ChangeBorder)
  protected
    { IXMLTSpecifyRelatedParcel_ChangeBorder }
    function Get_OldOrdinate: IXMLTOrdinate;
    function Get_NewOrdinate: IXMLTOrdinate;
  public
    procedure AfterConstruction; override;
  end;

  TXMLTChangeBorder = class(TXMLNode, IXMLTChangeBorder)
  protected
    { IXMLTSpecifyRelatedParcel_ChangeBorder }
    function Get_OldOrdinate: IXMLTOrdinate;
    function Get_NewOrdinate: IXMLTOrdinate;
  public
    procedure AfterConstruction; override;
  end;


{ TXMLTSpecifyRelatedParcel_ChangeBorderList }

  TXMLTSpecifyRelatedParcel_ChangeBorderList = class(TXMLNodeCollection, IXMLTSpecifyRelatedParcel_ChangeBorderList)
  protected
    { IXMLTSpecifyRelatedParcel_ChangeBorderList }
    function Add: IXMLTChangeBorder;
    function Insert(const Index: Integer): IXMLTChangeBorder;

    function Get_Item(Index: Integer): IXMLTChangeBorder;
  end;

{ TXMLTSpecifyRelatedParcel_Contours }

  TXMLTSpecifyRelatedParcel_Contours = class(TXMLNodeCollection, IXMLTSpecifyRelatedParcel_Contours)
  protected
    { IXMLTSpecifyRelatedParcel_Contours }
    function Get_NewContour(Index: Integer): IXMLTSpecifyRelatedParcel_Contours_NewContour;
    function Add: IXMLTSpecifyRelatedParcel_Contours_NewContour;
    function Insert(const Index: Integer): IXMLTSpecifyRelatedParcel_Contours_NewContour;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTSpecifyRelatedParcel_Contours_NewContour }

  TXMLTSpecifyRelatedParcel_Contours_NewContour = class(TXMLTContour, IXMLTSpecifyRelatedParcel_Contours_NewContour)
  protected
    { IXMLTSpecifyRelatedParcel_Contours_NewContour }
    function Get_Definition: UnicodeString;
    function Get_Providing_Pass_CadastralNumbers: IXMLTProviding_Pass_CadastralNumbers;
    procedure Set_Definition(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTSpecifyRelatedParcel_DeleteAllBorder }

  TXMLTSpecifyRelatedParcel_DeleteAllBorder = class(TXMLNode, IXMLTSpecifyRelatedParcel_DeleteAllBorder)
  protected
    { IXMLTSpecifyRelatedParcel_DeleteAllBorder }
    function Get_OldOrdinate: IXMLTOrdinate;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTSpecifyRelatedParcel_DeleteAllBorderList }

  TXMLTSpecifyRelatedParcel_DeleteAllBorderList = class(TXMLNodeCollection, IXMLTSpecifyRelatedParcel_DeleteAllBorderList)
  protected
    { IXMLTSpecifyRelatedParcel_DeleteAllBorderList }
    function Add: IXMLTSpecifyRelatedParcel_DeleteAllBorder;
    function Insert(const Index: Integer): IXMLTSpecifyRelatedParcel_DeleteAllBorder;

    function Get_Item(Index: Integer): IXMLTSpecifyRelatedParcel_DeleteAllBorder;
  end;

{ TXMLTSpecifyRelatedParcel_ExistSubParcels }

  TXMLTSpecifyRelatedParcel_ExistSubParcels = class(TXMLNodeCollection, IXMLTSpecifyRelatedParcel_ExistSubParcels)
  protected
    { IXMLTSpecifyRelatedParcel_ExistSubParcels }
    function Get_ExistSubParcel(Index: Integer): IXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel;
    function Add: IXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel;
    function Insert(const Index: Integer): IXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel }

  TXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel = class(TXMLNode, IXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel)
  protected
    { IXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel }
    function Get_Number_Record: LongWord;
    function Get_Entity_Spatial: IXMLEntity_Spatial;
    function Get_Contours: IXMLTSubParcel_Contours;
    procedure Set_Number_Record(Value: LongWord);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Entity_Spatial }

  TXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Entity_Spatial = class(TXMLNodeCollection, IXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Entity_Spatial)
  protected
    { IXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Entity_Spatial }
    function Get_Ent_Sys: UnicodeString;
    function Get_Spatial_Element(Index: Integer): IXMLTSPATIAL_ELEMENT;
    procedure Set_Ent_Sys(Value: UnicodeString);
    function Add: IXMLTSPATIAL_ELEMENT;
    function Insert(const Index: Integer): IXMLTSPATIAL_ELEMENT;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Contours }

  TXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Contours = class(TXMLNodeCollection, IXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Contours)
  protected
    { IXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Contours }
    function Get_Contour(Index: Integer): IXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Contours_Contour;
    function Add: IXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Contours_Contour;
    function Insert(const Index: Integer): IXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Contours_Contour;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Contours_Contour }

  TXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Contours_Contour = class(TXMLNode, IXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Contours_Contour)
  protected
    { IXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Contours_Contour }
    function Get_Number: LongWord;
    function Get_Entity_Spatial: IXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Contours_Contour_Entity_Spatial;
    procedure Set_Number(Value: LongWord);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Contours_Contour_Entity_Spatial }

  TXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Contours_Contour_Entity_Spatial = class(TXMLNodeCollection, IXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Contours_Contour_Entity_Spatial)
  protected
    { IXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Contours_Contour_Entity_Spatial }
    function Get_Ent_Sys: UnicodeString;
    function Get_Spatial_Element(Index: Integer): IXMLTSPATIAL_ELEMENT;
    procedure Set_Ent_Sys(Value: UnicodeString);
    function Add: IXMLTSPATIAL_ELEMENT;
    function Insert(const Index: Integer): IXMLTSPATIAL_ELEMENT;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLSTD_MP_Package_FormParcels_SpecifyRelatedParcel }

  TXMLSTD_MP_Package_FormParcels_SpecifyRelatedParcel = class(TXMLTSpecifyRelatedParcel, IXMLSTD_MP_Package_FormParcels_SpecifyRelatedParcel)
  protected
    { IXMLSTD_MP_Package_FormParcels_SpecifyRelatedParcel }
  end;

{ TXMLSTD_MP_Package_FormParcels_SpecifyRelatedParcelList }

  TXMLSTD_MP_Package_FormParcels_SpecifyRelatedParcelList = class(TXMLNodeCollection, IXMLSTD_MP_Package_FormParcels_SpecifyRelatedParcelList)
  protected
    { IXMLSTD_MP_Package_FormParcels_SpecifyRelatedParcelList }
    function Add: IXMLSTD_MP_Package_FormParcels_SpecifyRelatedParcel;
    function Insert(const Index: Integer): IXMLSTD_MP_Package_FormParcels_SpecifyRelatedParcel;

    function Get_Item(Index: Integer): IXMLSTD_MP_Package_FormParcels_SpecifyRelatedParcel;
  end;

{ TXMLSTD_MP_Package_SpecifyParcel }

  TXMLSTD_MP_Package_SpecifyParcel = class(TXMLNode, IXMLSTD_MP_Package_SpecifyParcel)
  private
    FExistParcel: IXMLTExistParcel;
    FSpecifyRelatedParcel: IXMLSTD_MP_Package_SpecifyParcel_SpecifyRelatedParcelList;
  protected
    { IXMLSTD_MP_Package_SpecifyParcel }
    function Get_ExistParcel: IXMLTExistParcel;
    function Get_ExistEZ: IXMLSTD_MP_Package_SpecifyParcel_ExistEZ;
    function Get_SpecifyRelatedParcel: IXMLSTD_MP_Package_SpecifyParcel_SpecifyRelatedParcelList;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTExistParcel }

  TXMLTExistParcel = class(TXMLNode, IXMLTExistParcel)
  protected
    { IXMLTExistParcel }
    function Get_CadastralNumber: UnicodeString;
    function Get_CadastralBlock: UnicodeString;
    function Get_Inner_CadastralNumbers: IXMLTInner_CadastralNumbers;
    function Get_Area: IXMLTArea;
    function Get_SubParcels: IXMLTExistParcel_SubParcels;
    function Get_Entity_Spatial: IXMLEntity_Spatial;
    function Get_Contours: IXMLContours;
    function Get_Area_In_GKN: LongWord;
    function Get_Delta_Area: LongWord;
    function Get_Min_Area: IXMLTArea_without_Innccuracy;
    function Get_Max_Area: IXMLTArea_without_Innccuracy;
    function Get_Note: UnicodeString;
    function Get_RelatedParcels: IXMLRelatedParcels;
    procedure Set_CadastralNumber(Value: UnicodeString);
    procedure Set_CadastralBlock(Value: UnicodeString);
    procedure Set_Area_In_GKN(Value: LongWord);
    procedure Set_Delta_Area(Value: LongWord);
    procedure Set_Note(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;


{ TXMLTExistParcel_Inner_CadastralNumbers }

{  TXMLTExistParcel_Inner_CadastralNumbers = class(TXMLNode, IXMLTExistParcel_Inner_CadastralNumbers)
  private
    FCadastralNumber: IXMLTExistParcel_Inner_CadastralNumbers_CadastralNumberList;
    FNumber: IXMLString_List;
  protected
    { IXMLTExistParcel_Inner_CadastralNumbers }
 {   function Get_CadastralNumber: IXMLTExistParcel_Inner_CadastralNumbers_CadastralNumberList;
    function Get_Number: IXMLString_List;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTExistParcel_SubParcels }

  TXMLTExistParcel_SubParcels = class(TXMLNode, IXMLTExistParcel_SubParcels)
  private
    FFormSubParcel: IXMLTExistParcel_SubParcels_FormSubParcelList;
    FExistSubParcel: IXMLTExistParcel_SubParcels_ExistSubParcelList;
    FInvariableSubParcel: IXMLTExistParcel_SubParcels_InvariableSubParcelList;
  protected
    { IXMLTExistParcel_SubParcels }
    function Get_FormSubParcel: IXMLTExistParcel_SubParcels_FormSubParcelList;
    function Get_ExistSubParcel: IXMLTExistParcel_SubParcels_ExistSubParcelList;
    function Get_InvariableSubParcel: IXMLTExistParcel_SubParcels_InvariableSubParcelList;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTExistParcel_SubParcels_FormSubParcel }

  TXMLTExistParcel_SubParcels_FormSubParcel = class(TXMLTSubParcel, IXMLTExistParcel_SubParcels_FormSubParcel)
  protected
    { IXMLTExistParcel_SubParcels_FormSubParcel }
    function Get_Definition: UnicodeString;
    procedure Set_Definition(Value: UnicodeString);
  end;

{ TXMLTExistParcel_SubParcels_FormSubParcelList }

  TXMLTExistParcel_SubParcels_FormSubParcelList = class(TXMLNodeCollection, IXMLTExistParcel_SubParcels_FormSubParcelList)
  protected
    { IXMLTExistParcel_SubParcels_FormSubParcelList }
    function Add: IXMLTExistParcel_SubParcels_FormSubParcel;
    function Insert(const Index: Integer): IXMLTExistParcel_SubParcels_FormSubParcel;

    function Get_Item(Index: Integer): IXMLTExistParcel_SubParcels_FormSubParcel;
  end;

{ TXMLTExistParcel_SubParcels_ExistSubParcel }

  TXMLTExistParcel_SubParcels_ExistSubParcel = class(TXMLTSubParcel, IXMLTExistParcel_SubParcels_ExistSubParcel)
  protected
    { IXMLTExistParcel_SubParcels_ExistSubParcel }
    function Get_Number_Record: LongWord;
    procedure Set_Number_Record(Value: LongWord);
  end;

{ TXMLTExistParcel_SubParcels_ExistSubParcelList }

  TXMLTExistParcel_SubParcels_ExistSubParcelList = class(TXMLNodeCollection, IXMLTExistParcel_SubParcels_ExistSubParcelList)
  protected
    { IXMLTExistParcel_SubParcels_ExistSubParcelList }
    function Add: IXMLTExistParcel_SubParcels_ExistSubParcel;
    function Insert(const Index: Integer): IXMLTExistParcel_SubParcels_ExistSubParcel;

    function Get_Item(Index: Integer): IXMLTExistParcel_SubParcels_ExistSubParcel;
  end;

{ TXMLTExistParcel_SubParcels_InvariableSubParcel }

  TXMLTExistParcel_SubParcels_InvariableSubParcel = class(TXMLNode, IXMLTExistParcel_SubParcels_InvariableSubParcel)
  protected
    { IXMLTExistParcel_SubParcels_InvariableSubParcel }
    function Get_Number_Record: LongWord;
    function Get_SubParcel_Realty: Boolean;
    function Get_Area: IXMLTExistParcel_SubParcels_InvariableSubParcel_Area;
    function Get_Encumbrance: IXMLTEncumbrance;
    function Get_Contours: IXMLTExistParcel_SubParcels_InvariableSubParcel_Contours;
    procedure Set_Number_Record(Value: LongWord);
    procedure Set_SubParcel_Realty(Value: Boolean);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTExistParcel_SubParcels_InvariableSubParcelList }

  TXMLTExistParcel_SubParcels_InvariableSubParcelList = class(TXMLNodeCollection, IXMLTExistParcel_SubParcels_InvariableSubParcelList)
  protected
    { IXMLTExistParcel_SubParcels_InvariableSubParcelList }
    function Add: IXMLTExistParcel_SubParcels_InvariableSubParcel;
    function Insert(const Index: Integer): IXMLTExistParcel_SubParcels_InvariableSubParcel;

    function Get_Item(Index: Integer): IXMLTExistParcel_SubParcels_InvariableSubParcel;
  end;

{ TXMLTExistParcel_SubParcels_InvariableSubParcel_Area }

  TXMLTExistParcel_SubParcels_InvariableSubParcel_Area = class(TXMLNode, IXMLTExistParcel_SubParcels_InvariableSubParcel_Area)
  protected
    { IXMLTExistParcel_SubParcels_InvariableSubParcel_Area }
    function Get_Area: LongWord;
    function Get_Unit_: UnicodeString;
    procedure Set_Area(Value: LongWord);
    procedure Set_Unit_(Value: UnicodeString);
  end;

{ TXMLTExistParcel_SubParcels_InvariableSubParcel_Contours }

  TXMLTExistParcel_SubParcels_InvariableSubParcel_Contours = class(TXMLNodeCollection, IXMLTExistParcel_SubParcels_InvariableSubParcel_Contours)
  protected
    { IXMLTExistParcel_SubParcels_InvariableSubParcel_Contours }
    function Get_Contour(Index: Integer): IXMLTExistParcel_SubParcels_InvariableSubParcel_Contours_Contour;
    function Add: IXMLTExistParcel_SubParcels_InvariableSubParcel_Contours_Contour;
    function Insert(const Index: Integer): IXMLTExistParcel_SubParcels_InvariableSubParcel_Contours_Contour;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTExistParcel_SubParcels_InvariableSubParcel_Contours_Contour }

  TXMLTExistParcel_SubParcels_InvariableSubParcel_Contours_Contour = class(TXMLNode, IXMLTExistParcel_SubParcels_InvariableSubParcel_Contours_Contour)
  protected
    { IXMLTExistParcel_SubParcels_InvariableSubParcel_Contours_Contour }
    function Get_Number: UnicodeString;
    function Get_Area: IXMLTArea;
    procedure Set_Number(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLContours }

  TXMLContours = class(TXMLNode, IXMLContours)
  private
    FNewContour: IXMLContours_NewContourList;
    FExistContour: IXMLContours_ExistContourList;
    FDeleteAllBorder: IXMLContours_DeleteAllBorderList;
  protected
    { IXMLContours }
    function Get_NewContour: IXMLContours_NewContourList;
    function Get_ExistContour: IXMLContours_ExistContourList;
    function Get_DeleteAllBorder: IXMLContours_DeleteAllBorderList;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLContours_NewContour }

  TXMLContours_NewContour = class(TXMLTContour, IXMLContours_NewContour)
  protected
    { IXMLContours_NewContour }
    function Get_Definition: UnicodeString;
    function Get_Providing_Pass_CadastralNumbers: IXMLTProviding_Pass_CadastralNumbers;
    procedure Set_Definition(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLContours_NewContourList }

  TXMLContours_NewContourList = class(TXMLNodeCollection, IXMLContours_NewContourList)
  protected
    { IXMLContours_NewContourList }
    function Add: IXMLContours_NewContour;
    function Insert(const Index: Integer): IXMLContours_NewContour;

    function Get_Item(Index: Integer): IXMLContours_NewContour;
  end;

{ TXMLContours_ExistContour }

  TXMLContours_ExistContour = class(TXMLTContour, IXMLContours_ExistContour)
  protected
    { IXMLContours_ExistContour }
    function Get_Number_Record: LongWord;
    procedure Set_Number_Record(Value: LongWord);
  end;

{ TXMLContours_ExistContourList }

  TXMLContours_ExistContourList = class(TXMLNodeCollection, IXMLContours_ExistContourList)
  protected
    { IXMLContours_ExistContourList }
    function Add: IXMLContours_ExistContour;
    function Insert(const Index: Integer): IXMLContours_ExistContour;

    function Get_Item(Index: Integer): IXMLContours_ExistContour;
  end;

{ TXMLContours_DeleteAllBorder }

  TXMLContours_DeleteAllBorder = class(TXMLNodeCollection, IXMLContours_DeleteAllBorder)
  protected
    { IXMLContours_DeleteAllBorder }
    function Get_Number_Record: LongWord;
    function Get_OldOrdinate(Index: Integer): IXMLTOrdinate;
    procedure Set_Number_Record(Value: LongWord);
    function Add: IXMLTOrdinate;
    function Insert(const Index: Integer): IXMLTOrdinate;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLContours_DeleteAllBorderList }

  TXMLContours_DeleteAllBorderList = class(TXMLNodeCollection, IXMLContours_DeleteAllBorderList)
  protected
    { IXMLContours_DeleteAllBorderList }
    function Add: IXMLContours_DeleteAllBorder;
    function Insert(const Index: Integer): IXMLContours_DeleteAllBorder;

    function Get_Item(Index: Integer): IXMLContours_DeleteAllBorder;
  end;

{ TXMLRelatedParcels }

  TXMLRelatedParcels = class(TXMLNodeCollection, IXMLRelatedParcels)
  protected
    { IXMLRelatedParcels }
    function Get_ParcelNeighbours(Index: Integer): IXMLRelatedParcels_ParcelNeighbours;
    function Add: IXMLRelatedParcels_ParcelNeighbours;
    function Insert(const Index: Integer): IXMLRelatedParcels_ParcelNeighbours;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLRelatedParcels_ParcelNeighbours }

  TXMLRelatedParcels_ParcelNeighbours = class(TXMLNode, IXMLRelatedParcels_ParcelNeighbours)
  private
    FParcelNeighbour: IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbourList;
  protected
    { IXMLRelatedParcels_ParcelNeighbours }
    function Get_Definition: UnicodeString;
    function Get_ParcelNeighbour: IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbourList;
    procedure Set_Definition(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour }

  TXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour = class(TXMLNode, IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour)
  protected
    { IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour }
    function Get_Cadastral_Number: UnicodeString;
    function Get_OwnerNeighbours: IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours;
    procedure Set_Cadastral_Number(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLRelatedParcels_ParcelNeighbours_ParcelNeighbourList }

  TXMLRelatedParcels_ParcelNeighbours_ParcelNeighbourList = class(TXMLNodeCollection, IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbourList)
  protected
    { IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbourList }
    function Add: IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour;
    function Insert(const Index: Integer): IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour;

    function Get_Item(Index: Integer): IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour;
  end;

{ TXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours }

  TXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours = class(TXMLNode, IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours)
  private
    FOwnerNeighbour: IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbourList;
  protected
    { IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours }
    function Get_NameRight: UnicodeString;
    function Get_OwnerNeighbour: IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbourList;
    procedure Set_NameRight(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbour }

  TXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbour = class(TXMLNode, IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbour)
  protected
    { IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbour }
    function Get_NameOwner: UnicodeString;
    function Get_ContactAddress: UnicodeString;
    function Get_Documents: IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbour_Documents;
    procedure Set_NameOwner(Value: UnicodeString);
    procedure Set_ContactAddress(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbourList }

  TXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbourList = class(TXMLNodeCollection, IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbourList)
  protected
    { IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbourList }
    function Add: IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbour;
    function Insert(const Index: Integer): IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbour;

    function Get_Item(Index: Integer): IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbour;
  end;

{ TXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbour_Documents }

  TXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbour_Documents = class(TXMLNodeCollection, IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbour_Documents)
  protected
    { IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbour_Documents }
    function Get_Document(Index: Integer): IXMLTDocument;
    function Add: IXMLTDocument;
    function Insert(const Index: Integer): IXMLTDocument;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLSTD_MP_Package_SpecifyParcel_ExistEZ }

  TXMLSTD_MP_Package_SpecifyParcel_ExistEZ = class(TXMLNode, IXMLSTD_MP_Package_SpecifyParcel_ExistEZ)
  protected
    { IXMLSTD_MP_Package_SpecifyParcel_ExistEZ }
    function Get_ExistEZParcels: IXMLTExistEZParcel;
    function Get_ExistEZEntryParcels: IXMLSTD_MP_Package_SpecifyParcel_ExistEZ_ExistEZEntryParcels;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTExistEZParcel }

  TXMLTExistEZParcel = class(TXMLNode, IXMLTExistEZParcel)
  protected
    { IXMLTExistEZParcel }
    function Get_CadastralNumber: UnicodeString;
    function Get_CadastralBlock: UnicodeString;
    function Get_Inner_CadastralNumbers: IXMLTExistEZParcel_Inner_CadastralNumbers;
    function Get_Area: IXMLTAreaNew;
    function Get_SubParcels: IXMLTExistParcel_SubParcels; //IXMLTExistEZParcel_SubParcels;
    function Get_Composition_EZ: IXMLTExistEZParcel_Composition_EZ;
    function Get_Area_In_GKN: LongWord;
    function Get_Delta_Area: LongWord;
    function Get_Min_Area: IXMLTArea_without_Innccuracy;
    function Get_Max_Area: IXMLTArea_without_Innccuracy;
    function Get_Note: UnicodeString;
    function Get_RelatedParcels: IXMLRelatedParcels;
    procedure Set_CadastralNumber(Value: UnicodeString);
    procedure Set_CadastralBlock(Value: UnicodeString);
    procedure Set_Area_In_GKN(Value: LongWord);
    procedure Set_Delta_Area(Value: LongWord);
    procedure Set_Note(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTExistEZParcel_Inner_CadastralNumbers }

  TXMLTExistEZParcel_Inner_CadastralNumbers = class(TXMLNode, IXMLTExistEZParcel_Inner_CadastralNumbers)
  private
    FCadastralNumber: IXMLTExistEZParcel_Inner_CadastralNumbers_CadastralNumberList;
    FNumber: IXMLString_List;
  protected
    { IXMLTExistEZParcel_Inner_CadastralNumbers }
    function Get_CadastralNumber: IXMLTExistEZParcel_Inner_CadastralNumbers_CadastralNumberList;
    function Get_Number: IXMLString_List;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTExistEZParcel_SubParcels }

  TXMLTExistEZParcel_SubParcels = class(TXMLNode, IXMLTExistEZParcel_SubParcels)
  private
    FFormSubParcel: IXMLTExistEZParcel_SubParcels_FormSubParcelList;
    FExistSubParcel: IXMLTExistEZParcel_SubParcels_ExistSubParcelList;
    FInvariableSubParcel: IXMLTExistEZParcel_SubParcels_InvariableSubParcelList;
  protected
    { IXMLTExistEZParcel_SubParcels }
    function Get_FormSubParcel: IXMLTExistEZParcel_SubParcels_FormSubParcelList;
    function Get_ExistSubParcel: IXMLTExistEZParcel_SubParcels_ExistSubParcelList;
    function Get_InvariableSubParcel: IXMLTExistEZParcel_SubParcels_InvariableSubParcelList;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTExistEZParcel_SubParcels_FormSubParcel }

  TXMLTExistEZParcel_SubParcels_FormSubParcel = class(TXMLTSubParcel, IXMLTExistEZParcel_SubParcels_FormSubParcel)
  protected
    { IXMLTExistEZParcel_SubParcels_FormSubParcel }
    function Get_Definition: UnicodeString;
    procedure Set_Definition(Value: UnicodeString);
  end;

{ TXMLTExistEZParcel_SubParcels_FormSubParcelList }

  TXMLTExistEZParcel_SubParcels_FormSubParcelList = class(TXMLNodeCollection, IXMLTExistEZParcel_SubParcels_FormSubParcelList)
  protected
    { IXMLTExistEZParcel_SubParcels_FormSubParcelList }
    function Add: IXMLTExistEZParcel_SubParcels_FormSubParcel;
    function Insert(const Index: Integer): IXMLTExistEZParcel_SubParcels_FormSubParcel;

    function Get_Item(Index: Integer): IXMLTExistEZParcel_SubParcels_FormSubParcel;
  end;

{ TXMLTExistEZParcel_SubParcels_ExistSubParcel }

  TXMLTExistEZParcel_SubParcels_ExistSubParcel = class(TXMLTSubParcel, IXMLTExistEZParcel_SubParcels_ExistSubParcel)
  protected
    { IXMLTExistEZParcel_SubParcels_ExistSubParcel }
    function Get_Number_Record: LongWord;
    procedure Set_Number_Record(Value: LongWord);
  end;

{ TXMLTExistEZParcel_SubParcels_ExistSubParcelList }

  TXMLTExistEZParcel_SubParcels_ExistSubParcelList = class(TXMLNodeCollection, IXMLTExistEZParcel_SubParcels_ExistSubParcelList)
  protected
    { IXMLTExistEZParcel_SubParcels_ExistSubParcelList }
    function Add: IXMLTExistEZParcel_SubParcels_ExistSubParcel;
    function Insert(const Index: Integer): IXMLTExistEZParcel_SubParcels_ExistSubParcel;

    function Get_Item(Index: Integer): IXMLTExistEZParcel_SubParcels_ExistSubParcel;
  end;

{ TXMLTExistEZParcel_SubParcels_InvariableSubParcel }

  TXMLTExistEZParcel_SubParcels_InvariableSubParcel = class(TXMLNode, IXMLTExistEZParcel_SubParcels_InvariableSubParcel)
  protected
    { IXMLTExistEZParcel_SubParcels_InvariableSubParcel }
    function Get_Number_Record: LongWord;
    function Get_SubParcel_Realty: Boolean;
    function Get_Area: IXMLTExistEZParcel_SubParcels_InvariableSubParcel_Area;
    function Get_Encumbrance: IXMLTEncumbrance;
    function Get_Contours: IXMLTExistEZParcel_SubParcels_InvariableSubParcel_Contours;
    procedure Set_Number_Record(Value: LongWord);
    procedure Set_SubParcel_Realty(Value: Boolean);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTExistEZParcel_SubParcels_InvariableSubParcelList }

  TXMLTExistEZParcel_SubParcels_InvariableSubParcelList = class(TXMLNodeCollection, IXMLTExistEZParcel_SubParcels_InvariableSubParcelList)
  protected
    { IXMLTExistEZParcel_SubParcels_InvariableSubParcelList }
    function Add: IXMLTExistEZParcel_SubParcels_InvariableSubParcel;
    function Insert(const Index: Integer): IXMLTExistEZParcel_SubParcels_InvariableSubParcel;

    function Get_Item(Index: Integer): IXMLTExistEZParcel_SubParcels_InvariableSubParcel;
  end;

{ TXMLTExistEZParcel_SubParcels_InvariableSubParcel_Area }

  TXMLTExistEZParcel_SubParcels_InvariableSubParcel_Area = class(TXMLNode, IXMLTExistEZParcel_SubParcels_InvariableSubParcel_Area)
  protected
    { IXMLTExistEZParcel_SubParcels_InvariableSubParcel_Area }
    function Get_Area: LongWord;
    function Get_Unit_: UnicodeString;
    procedure Set_Area(Value: LongWord);
    procedure Set_Unit_(Value: UnicodeString);
  end;

{ TXMLTExistEZParcel_SubParcels_InvariableSubParcel_Contours }

  TXMLTExistEZParcel_SubParcels_InvariableSubParcel_Contours = class(TXMLNodeCollection, IXMLTExistEZParcel_SubParcels_InvariableSubParcel_Contours)
  protected
    { IXMLTExistEZParcel_SubParcels_InvariableSubParcel_Contours }
    function Get_Contour(Index: Integer): IXMLTExistEZParcel_SubParcels_InvariableSubParcel_Contours_Contour;
    function Add: IXMLTExistEZParcel_SubParcels_InvariableSubParcel_Contours_Contour;
    function Insert(const Index: Integer): IXMLTExistEZParcel_SubParcels_InvariableSubParcel_Contours_Contour;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTExistEZParcel_SubParcels_InvariableSubParcel_Contours_Contour }

  TXMLTExistEZParcel_SubParcels_InvariableSubParcel_Contours_Contour = class(TXMLNode, IXMLTExistEZParcel_SubParcels_InvariableSubParcel_Contours_Contour)
  protected
    { IXMLTExistEZParcel_SubParcels_InvariableSubParcel_Contours_Contour }
    function Get_Number: UnicodeString;
    function Get_Area: IXMLTArea_Contour;
    procedure Set_Number(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTExistEZParcel_Composition_EZ }

  TXMLTExistEZParcel_Composition_EZ = class(TXMLNode, IXMLTExistEZParcel_Composition_EZ)
  protected
    { IXMLTExistEZParcel_Composition_EZ }
    function Get_InsertEntryParcels: IXMLTExistEZParcel_Composition_EZ_InsertEntryParcels;
    function Get_DeleteEntryParcels: IXMLTExistEZParcel_Composition_EZ_DeleteEntryParcels;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTExistEZParcel_Composition_EZ_InsertEntryParcels }

  TXMLTExistEZParcel_Composition_EZ_InsertEntryParcels = class(TXMLNodeCollection, IXMLTExistEZParcel_Composition_EZ_InsertEntryParcels)
  protected
    { IXMLTExistEZParcel_Composition_EZ_InsertEntryParcels }
    function Get_InsertEntryParcel(Index: Integer): IXMLTExistEZParcel_Composition_EZ_InsertEntryParcels_InsertEntryParcel;
    function Add: IXMLTExistEZParcel_Composition_EZ_InsertEntryParcels_InsertEntryParcel;
    function Insert(const Index: Integer): IXMLTExistEZParcel_Composition_EZ_InsertEntryParcels_InsertEntryParcel;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTExistEZParcel_Composition_EZ_InsertEntryParcels_InsertEntryParcel }

  TXMLTExistEZParcel_Composition_EZ_InsertEntryParcels_InsertEntryParcel = class(TXMLNode, IXMLTExistEZParcel_Composition_EZ_InsertEntryParcels_InsertEntryParcel)
  protected
    { IXMLTExistEZParcel_Composition_EZ_InsertEntryParcels_InsertEntryParcel }
    function Get_ExistEntryParcel: IXMLTExistEZParcel_Composition_EZ_InsertEntryParcels_InsertEntryParcel_ExistEntryParcel;
    function Get_NewEntryParcel: IXMLTNewEZEntryParcel;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTExistEZParcel_Composition_EZ_InsertEntryParcels_InsertEntryParcel_ExistEntryParcel }

  TXMLTExistEZParcel_Composition_EZ_InsertEntryParcels_InsertEntryParcel_ExistEntryParcel = class(TXMLNode, IXMLTExistEZParcel_Composition_EZ_InsertEntryParcels_InsertEntryParcel_ExistEntryParcel)
  protected
    { IXMLTExistEZParcel_Composition_EZ_InsertEntryParcels_InsertEntryParcel_ExistEntryParcel }
    function Get_CadastralNumber: UnicodeString;
    procedure Set_CadastralNumber(Value: UnicodeString);
  end;

{ TXMLTNewEZEntryParcel }

  TXMLTNewEZEntryParcel = class(TXMLNode, IXMLTNewEZEntryParcel)
  protected
    { IXMLTNewEZEntryParcel }
    function Get_Name: UnicodeString;
    function Get_Definition: UnicodeString;
    function Get_Area: IXMLTAreaNew;
    function Get_Entity_Spatial: IXMLEntity_Spatial;
    function Get_Encumbrance: IXMLTEncumbrance;
    procedure Set_Name(Value: UnicodeString);
    procedure Set_Definition(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTExistEZParcel_Composition_EZ_DeleteEntryParcels }

  TXMLTExistEZParcel_Composition_EZ_DeleteEntryParcels = class(TXMLNodeCollection, IXMLTExistEZParcel_Composition_EZ_DeleteEntryParcels)
  protected
    { IXMLTExistEZParcel_Composition_EZ_DeleteEntryParcels }
    function Get_DeleteEntryParcel(Index: Integer): IXMLTExistEZParcel_Composition_EZ_DeleteEntryParcels_DeleteEntryParcel;
    function Add: IXMLTExistEZParcel_Composition_EZ_DeleteEntryParcels_DeleteEntryParcel;
    function Insert(const Index: Integer): IXMLTExistEZParcel_Composition_EZ_DeleteEntryParcels_DeleteEntryParcel;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTExistEZParcel_Composition_EZ_DeleteEntryParcels_DeleteEntryParcel }

  TXMLTExistEZParcel_Composition_EZ_DeleteEntryParcels_DeleteEntryParcel = class(TXMLNode, IXMLTExistEZParcel_Composition_EZ_DeleteEntryParcels_DeleteEntryParcel)
  protected
    { IXMLTExistEZParcel_Composition_EZ_DeleteEntryParcels_DeleteEntryParcel }
    function Get_CadastralNumber: UnicodeString;
    procedure Set_CadastralNumber(Value: UnicodeString);
  end;

{ TXMLSTD_MP_Package_SpecifyParcel_ExistEZ_ExistEZEntryParcels }

  TXMLSTD_MP_Package_SpecifyParcel_ExistEZ_ExistEZEntryParcels = class(TXMLNodeCollection, IXMLSTD_MP_Package_SpecifyParcel_ExistEZ_ExistEZEntryParcels)
  protected
    { IXMLSTD_MP_Package_SpecifyParcel_ExistEZ_ExistEZEntryParcels }
    function Get_ExistEZEntryParcel(Index: Integer): IXMLTExistEZEntryParcel;
    function Add: IXMLTExistEZEntryParcel;
    function Insert(const Index: Integer): IXMLTExistEZEntryParcel;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTExistEZEntryParcel }

  TXMLTExistEZEntryParcel = class(TXMLNode, IXMLTExistEZEntryParcel)
  protected
    { IXMLTExistEZEntryParcel }
    function Get_CadastralNumber: UnicodeString;
    function Get_Area: IXMLTAreaNew;
    function Get_Area_In_GKN: LongWord;
    function Get_Encumbrance: IXMLTEncumbrance;
    function Get_Entity_Spatial: IXMLEntity_Spatial;
    function Get_Note: UnicodeString;
    procedure Set_CadastralNumber(Value: UnicodeString);
    procedure Set_Area_In_GKN(Value: LongWord);
    procedure Set_Note(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLSTD_MP_Package_SpecifyParcel_SpecifyRelatedParcel }

  TXMLSTD_MP_Package_SpecifyParcel_SpecifyRelatedParcel = class(TXMLTSpecifyRelatedParcel, IXMLSTD_MP_Package_SpecifyParcel_SpecifyRelatedParcel)
  protected
    { IXMLSTD_MP_Package_SpecifyParcel_SpecifyRelatedParcel }
  end;

{ TXMLSTD_MP_Package_SpecifyParcel_SpecifyRelatedParcelList }

  TXMLSTD_MP_Package_SpecifyParcel_SpecifyRelatedParcelList = class(TXMLNodeCollection, IXMLSTD_MP_Package_SpecifyParcel_SpecifyRelatedParcelList)
  protected
    { IXMLSTD_MP_Package_SpecifyParcel_SpecifyRelatedParcelList }
    function Add: IXMLTSpecifyRelatedParcel;//IXMLSTD_MP_Package_SpecifyParcel_SpecifyRelatedParcel;
    function Insert(const Index: Integer): IXMLTSpecifyRelatedParcel;//IXMLSTD_MP_Package_SpecifyParcel_SpecifyRelatedParcel;

    function Get_Item(Index: Integer): IXMLTSpecifyRelatedParcel;//IXMLSTD_MP_Package_SpecifyParcel_SpecifyRelatedParcel;
  end;

{ TXMLTNewSubParcel }

  TXMLTNewSubParcel = class(TXMLNode, IXMLTNewSubParcel)
  protected
    { IXMLTNewSubParcel }
    function Get_Definition: UnicodeString;
    function Get_SubParcel_Realty: Boolean;
    function Get_CadastralNumber_Parcel: UnicodeString;
    function Get_Area: IXMLTArea;
    function Get_Encumbrance: IXMLTEncumbrance;
    {$IFNDEF disableIXMLTNewSubParcel_Entity_Spatial}
    function Get_Entity_Spatial: IXMLTNewSubParcel_Entity_Spatial;
    {$ELSE}
    function Get_Entity_Spatial: IXMLEntity_Spatial;
    {$ENDIF}
    {$IFNDEF disableIXMLTNewSubParcel_Contours}
    function Get_Contours: IXMLTNewSubParcel_Contours;
    {$ELSE}
    function Get_Contours: IXMLTSubParcel_Contours;
    {$ENDIF}
    procedure Set_Definition(Value: UnicodeString);
    procedure Set_SubParcel_Realty(Value: Boolean);
    procedure Set_CadastralNumber_Parcel(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTNewSubParcelList }

  TXMLTNewSubParcelList = class(TXMLNodeCollection, IXMLTNewSubParcelList)
  protected
    { IXMLTNewSubParcelList }
    function Add: IXMLTNewSubParcel;
    function Insert(const Index: Integer): IXMLTNewSubParcel;

    function Get_Item(Index: Integer): IXMLTNewSubParcel;
  end;

  {$IFNDEF  disableIXMLTNewSubParcel_Entity_Spatial}
{ TXMLTNewSubParcel_Entity_Spatial }

  TXMLTNewSubParcel_Entity_Spatial = class(TXMLNodeCollection, IXMLTNewSubParcel_Entity_Spatial)
  protected
    { IXMLTNewSubParcel_Entity_Spatial }
    function Get_Ent_Sys: UnicodeString;
    function Get_Spatial_Element(Index: Integer): IXMLTSPATIAL_ELEMENT;
    procedure Set_Ent_Sys(Value: UnicodeString);
    function Add: IXMLTSPATIAL_ELEMENT;
    function Insert(const Index: Integer): IXMLTSPATIAL_ELEMENT;
  public
    procedure AfterConstruction; override;
  end;
  {$ENDIF}

  {$IFNDEF disableIXMLTNewSubParcel_Contours}
{ TXMLTNewSubParcel_Contours }

  TXMLTNewSubParcel_Contours = class(TXMLNodeCollection, IXMLTNewSubParcel_Contours)
  protected
    { IXMLTNewSubParcel_Contours }
    function Get_Contour(Index: Integer): IXMLTNewSubParcel_Contours_Contour;
    function Add: IXMLTNewSubParcel_Contours_Contour;
    function Insert(const Index: Integer): IXMLTNewSubParcel_Contours_Contour;
  public
    procedure AfterConstruction; override;
  end;
  {$ENDIF}

{ TXMLTNewSubParcel_Contours_Contour }

  TXMLTNewSubParcel_Contours_Contour = class(TXMLNode, IXMLTNewSubParcel_Contours_Contour)
  protected
    { IXMLTNewSubParcel_Contours_Contour }
    function Get_Number: UnicodeString;
    function Get_Area: IXMLTArea_Contour;
    function Get_Entity_Spatial: IXMLTNewSubParcel_Contours_Contour_Entity_Spatial;
    procedure Set_Number(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTNewSubParcel_Contours_Contour_Entity_Spatial }

  TXMLTNewSubParcel_Contours_Contour_Entity_Spatial = class(TXMLNodeCollection, IXMLTNewSubParcel_Contours_Contour_Entity_Spatial)
  protected
    { IXMLTNewSubParcel_Contours_Contour_Entity_Spatial }
    function Get_Ent_Sys: UnicodeString;
    function Get_Spatial_Element(Index: Integer): IXMLTSPATIAL_ELEMENT;
    procedure Set_Ent_Sys(Value: UnicodeString);
    function Add: IXMLTSPATIAL_ELEMENT;
    function Insert(const Index: Integer): IXMLTSPATIAL_ELEMENT;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLSTD_MP_Package_SpecifyParcelsApproximal }

  TXMLSTD_MP_Package_SpecifyParcelsApproximal = class(TXMLNode, IXMLSTD_MP_Package_SpecifyParcelsApproximal)
  protected
    { IXMLSTD_MP_Package_SpecifyParcelsApproximal }
    function Get_ExistParcel: IXMLTExistParcel;
    function Get_ExistEZ: IXMLSTD_MP_Package_SpecifyParcelsApproximal_ExistEZ;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLSTD_MP_Package_SpecifyParcelsApproximalList }

  TXMLSTD_MP_Package_SpecifyParcelsApproximalList = class(TXMLNodeCollection, IXMLSTD_MP_Package_SpecifyParcelsApproximalList)
  protected
    { IXMLSTD_MP_Package_SpecifyParcelsApproximalList }
    function Add: IXMLSTD_MP_Package_SpecifyParcelsApproximal;
    function Insert(const Index: Integer): IXMLSTD_MP_Package_SpecifyParcelsApproximal;

    function Get_Item(Index: Integer): IXMLSTD_MP_Package_SpecifyParcelsApproximal;
  end;

{ TXMLSTD_MP_Package_SpecifyParcelsApproximal_ExistEZ }

  TXMLSTD_MP_Package_SpecifyParcelsApproximal_ExistEZ = class(TXMLNode, IXMLSTD_MP_Package_SpecifyParcelsApproximal_ExistEZ)
  private
    FExistEZEntryParcels: IXMLSTD_MP_Package_SpecifyParcelsApproximal_ExistEZ_ExistEZEntryParcelsList;
  protected
    { IXMLSTD_MP_Package_SpecifyParcelsApproximal_ExistEZ }
    function Get_ExistEZParcels: IXMLTExistEZParcel;
    function Get_ExistEZEntryParcels: IXMLSTD_MP_Package_SpecifyParcelsApproximal_ExistEZ_ExistEZEntryParcelsList;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLSTD_MP_Package_SpecifyParcelsApproximal_ExistEZ_ExistEZEntryParcels }

  TXMLSTD_MP_Package_SpecifyParcelsApproximal_ExistEZ_ExistEZEntryParcels = class(TXMLNode, IXMLSTD_MP_Package_SpecifyParcelsApproximal_ExistEZ_ExistEZEntryParcels)
  protected
    { IXMLSTD_MP_Package_SpecifyParcelsApproximal_ExistEZ_ExistEZEntryParcels }
    function Get_ExistEZEntryParcel: IXMLTExistEZEntryParcel;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLSTD_MP_Package_SpecifyParcelsApproximal_ExistEZ_ExistEZEntryParcelsList }

  TXMLSTD_MP_Package_SpecifyParcelsApproximal_ExistEZ_ExistEZEntryParcelsList = class(TXMLNodeCollection, IXMLSTD_MP_Package_SpecifyParcelsApproximal_ExistEZ_ExistEZEntryParcelsList)
  protected
    { IXMLSTD_MP_Package_SpecifyParcelsApproximal_ExistEZ_ExistEZEntryParcelsList }
    function Add: IXMLSTD_MP_Package_SpecifyParcelsApproximal_ExistEZ_ExistEZEntryParcels;
    function Insert(const Index: Integer): IXMLSTD_MP_Package_SpecifyParcelsApproximal_ExistEZ_ExistEZEntryParcels;

    function Get_Item(Index: Integer): IXMLSTD_MP_Package_SpecifyParcelsApproximal_ExistEZ_ExistEZEntryParcels;
  end;

{ TXMLCoord_Systems }

  TXMLCoord_Systems = class(TXMLNode, IXMLCoord_Systems)
  protected
    { IXMLCoord_Systems }
    function Get_Coord_System: IXMLCoord_System;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLCoord_System }

  TXMLCoord_System = class(TXMLNode, IXMLCoord_System)
  protected
    { IXMLCoord_System }
    function Get_Name: UnicodeString;
    function Get_Cs_Id: UnicodeString;
    procedure Set_Name(Value: UnicodeString);
    procedure Set_Cs_Id(Value: UnicodeString);
  end;

{ TXMLSTD_MP_Input_Data }

  TXMLSTD_MP_Input_Data = class(TXMLNode, IXMLSTD_MP_Input_Data)
  protected
    { IXMLSTD_MP_Input_Data }
    function Get_Documents: IXMLSTD_MP_Input_Data_Documents;
    function Get_Geodesic_Bases: IXMLSTD_MP_Input_Data_Geodesic_Bases;
    function Get_Means_Survey: IXMLSTD_MP_Input_Data_Means_Survey;
    function Get_Realty: IXMLSTD_MP_Input_Data_Realty;
    function Get_SubParcels: IXMLSTD_MP_Input_Data_SubParcels;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLSTD_MP_Input_Data_Documents }

  TXMLSTD_MP_Input_Data_Documents = class(TXMLNodeCollection, IXMLSTD_MP_Input_Data_Documents)
  protected
    { IXMLSTD_MP_Input_Data_Documents }
    function Get_Document(Index: Integer): IXMLSTD_MP_Input_Data_Documents_Document;
    function Add: IXMLSTD_MP_Input_Data_Documents_Document;
    function Insert(const Index: Integer): IXMLSTD_MP_Input_Data_Documents_Document;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLSTD_MP_Input_Data_Documents_Document }

  TXMLSTD_MP_Input_Data_Documents_Document = class(TXMLTDocument, IXMLSTD_MP_Input_Data_Documents_Document)
  protected
    { IXMLSTD_MP_Input_Data_Documents_Document }
    function Get_Scale: UnicodeString;
    function Get_Date_Create: UnicodeString;
    function Get_Date_Update: UnicodeString;
    procedure Set_Scale(Value: UnicodeString);
    procedure Set_Date_Create(Value: UnicodeString);
    procedure Set_Date_Update(Value: UnicodeString);
  end;

{ TXMLSTD_MP_Input_Data_Geodesic_Bases }

  TXMLSTD_MP_Input_Data_Geodesic_Bases = class(TXMLNodeCollection, IXMLSTD_MP_Input_Data_Geodesic_Bases)
  protected
    { IXMLSTD_MP_Input_Data_Geodesic_Bases }
    function Get_Geodesic_Base(Index: Integer): IXMLSTD_MP_Input_Data_Geodesic_Bases_Geodesic_Base;
    function Add: IXMLSTD_MP_Input_Data_Geodesic_Bases_Geodesic_Base;
    function Insert(const Index: Integer): IXMLSTD_MP_Input_Data_Geodesic_Bases_Geodesic_Base;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLSTD_MP_Input_Data_Geodesic_Bases_Geodesic_Base }

  TXMLSTD_MP_Input_Data_Geodesic_Bases_Geodesic_Base = class(TXMLNode, IXMLSTD_MP_Input_Data_Geodesic_Bases_Geodesic_Base)
  protected
    { IXMLSTD_MP_Input_Data_Geodesic_Bases_Geodesic_Base }
    function Get_PName: UnicodeString;
    function Get_PKind: UnicodeString;
    function Get_PKlass: UnicodeString;
    function Get_OrdX: UnicodeString;
    function Get_OrdY: UnicodeString;
    procedure Set_PName(Value: UnicodeString);
    procedure Set_PKind(Value: UnicodeString);
    procedure Set_PKlass(Value: UnicodeString);
    procedure Set_OrdX(Value: UnicodeString);
    procedure Set_OrdY(Value: UnicodeString);
  end;

{ TXMLSTD_MP_Input_Data_Means_Survey }

  TXMLSTD_MP_Input_Data_Means_Survey = class(TXMLNodeCollection, IXMLSTD_MP_Input_Data_Means_Survey)
  protected
    { IXMLSTD_MP_Input_Data_Means_Survey }
    function Get_Means_Survey(Index: Integer): IXMLSTD_MP_Input_Data_Means_Survey_Means_Survey;
    function Add: IXMLSTD_MP_Input_Data_Means_Survey_Means_Survey;
    function Insert(const Index: Integer): IXMLSTD_MP_Input_Data_Means_Survey_Means_Survey;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLSTD_MP_Input_Data_Means_Survey_Means_Survey }

  TXMLSTD_MP_Input_Data_Means_Survey_Means_Survey = class(TXMLNode, IXMLSTD_MP_Input_Data_Means_Survey_Means_Survey)
  protected
    { IXMLSTD_MP_Input_Data_Means_Survey_Means_Survey }
    function Get_Name: UnicodeString;
    function Get_Certificate: UnicodeString;
    function Get_Certificate_Verification: UnicodeString;
    procedure Set_Name(Value: UnicodeString);
    procedure Set_Certificate(Value: UnicodeString);
    procedure Set_Certificate_Verification(Value: UnicodeString);
  end;

{ TXMLSTD_MP_Input_Data_Realty }

  TXMLSTD_MP_Input_Data_Realty = class(TXMLNodeCollection, IXMLSTD_MP_Input_Data_Realty)
  protected
    { IXMLSTD_MP_Input_Data_Realty }
    function Get_OKS(Index: Integer): IXMLSTD_MP_Input_Data_Realty_OKS;
    function Add: IXMLSTD_MP_Input_Data_Realty_OKS;
    function Insert(const Index: Integer): IXMLSTD_MP_Input_Data_Realty_OKS;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLSTD_MP_Input_Data_Realty_OKS }

  TXMLSTD_MP_Input_Data_Realty_OKS = class(TXMLNode, IXMLSTD_MP_Input_Data_Realty_OKS)
  protected
    { IXMLSTD_MP_Input_Data_Realty_OKS }
    function Get_CadastralNumber: UnicodeString;
    function Get_CadastralNumber_OtherNumber: UnicodeString;
    function Get_Name_OKS: UnicodeString;
    procedure Set_CadastralNumber(Value: UnicodeString);
    procedure Set_CadastralNumber_OtherNumber(Value: UnicodeString);
    procedure Set_Name_OKS(Value: UnicodeString);
  end;

{ TXMLSTD_MP_Input_Data_SubParcels }

  TXMLSTD_MP_Input_Data_SubParcels = class(TXMLNodeCollection, IXMLSTD_MP_Input_Data_SubParcels)
  protected
    { IXMLSTD_MP_Input_Data_SubParcels }
    function Get_SubParcel(Index: Integer): IXMLSTD_MP_Input_Data_SubParcels_SubParcel;
    function Add: IXMLSTD_MP_Input_Data_SubParcels_SubParcel;
    function Insert(const Index: Integer): IXMLSTD_MP_Input_Data_SubParcels_SubParcel;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLSTD_MP_Input_Data_SubParcels_SubParcel }

  TXMLSTD_MP_Input_Data_SubParcels_SubParcel = class(TXMLNode, IXMLSTD_MP_Input_Data_SubParcels_SubParcel)
  protected
    { IXMLSTD_MP_Input_Data_SubParcels_SubParcel }
    function Get_CadastralNumber: UnicodeString;
    function Get_Number_Record: UnicodeString;
    procedure Set_CadastralNumber(Value: UnicodeString);
    procedure Set_Number_Record(Value: UnicodeString);
  end;

{ TXMLSTD_MP_Survey }

  TXMLSTD_MP_Survey = class(TXMLNodeCollection, IXMLSTD_MP_Survey)
  protected
    { IXMLSTD_MP_Survey }
    function Get_AppliedFile(Index: Integer): IXMLSTD_MP_Survey_AppliedFile;
    function Add: IXMLSTD_MP_Survey_AppliedFile;
    function Insert(const Index: Integer): IXMLSTD_MP_Survey_AppliedFile;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLSTD_MP_Survey_AppliedFile }

  TXMLSTD_MP_Survey_AppliedFile = class(TXMLTAppliedFile, IXMLSTD_MP_Survey_AppliedFile)
  protected
    { IXMLSTD_MP_Survey_AppliedFile }
  end;

{ TXMLSTD_MP_Scheme_Geodesic_Plotting }

  TXMLSTD_MP_Scheme_Geodesic_Plotting = class(TXMLNodeCollection, IXMLSTD_MP_Scheme_Geodesic_Plotting)
  protected
    { IXMLSTD_MP_Scheme_Geodesic_Plotting }
    function Get_AppliedFile(Index: Integer): IXMLSTD_MP_Survey_AppliedFile;
    function Add: IXMLSTD_MP_Survey_AppliedFile;
    function Insert(const Index: Integer): IXMLSTD_MP_Survey_AppliedFile;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLSTD_MP_Scheme_Geodesic_Plotting_AppliedFile }

  TXMLSTD_MP_Scheme_Geodesic_Plotting_AppliedFile = class(TXMLTAppliedFile, IXMLSTD_MP_Scheme_Geodesic_Plotting_AppliedFile)
  protected
    { IXMLSTD_MP_Scheme_Geodesic_Plotting_AppliedFile }
  end;

{ TXMLSTD_MP_Scheme_Disposition_Parcels }

  TXMLSTD_MP_Scheme_Disposition_Parcels = class(TXMLNodeCollection, IXMLSTD_MP_Scheme_Disposition_Parcels)
  protected
    { IXMLSTD_MP_Scheme_Disposition_Parcels }
    function Get_AppliedFile(Index: Integer): IXMLSTD_MP_Survey_AppliedFile;
    function Add: IXMLSTD_MP_Survey_AppliedFile;
    function Insert(const Index: Integer): IXMLSTD_MP_Survey_AppliedFile;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLSTD_MP_Scheme_Disposition_Parcels_AppliedFile }

  TXMLSTD_MP_Scheme_Disposition_Parcels_AppliedFile = class(TXMLTAppliedFile, IXMLSTD_MP_Scheme_Disposition_Parcels_AppliedFile)
  protected
    { IXMLSTD_MP_Scheme_Disposition_Parcels_AppliedFile }
  end;

{ TXMLSTD_MP_Diagram_Parcels_SubParcels }

  TXMLSTD_MP_Diagram_Parcels_SubParcels = class(TXMLNodeCollection, IXMLSTD_MP_Diagram_Parcels_SubParcels)
  protected
    { IXMLSTD_MP_Diagram_Parcels_SubParcels }
    function Get_AppliedFile(Index: Integer): IXMLSTD_MP_Survey_AppliedFile;
    function Add: IXMLSTD_MP_Survey_AppliedFile;
    function Insert(const Index: Integer): IXMLSTD_MP_Survey_AppliedFile;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLSTD_MP_Diagram_Parcels_SubParcels_AppliedFile }

  TXMLSTD_MP_Diagram_Parcels_SubParcels_AppliedFile = class(TXMLTAppliedFile, IXMLSTD_MP_Diagram_Parcels_SubParcels_AppliedFile)
  protected
    { IXMLSTD_MP_Diagram_Parcels_SubParcels_AppliedFile }
  end;

{ TXMLSTD_MP_Agreement_Document }

  TXMLSTD_MP_Agreement_Document = class(TXMLNodeCollection, IXMLSTD_MP_Agreement_Document)
  protected
    { IXMLSTD_MP_Agreement_Document }
    function Get_AppliedFile(Index: Integer): IXMLSTD_MP_Survey_AppliedFile;
    function Add: IXMLSTD_MP_Survey_AppliedFile;
    function Insert(const Index: Integer): IXMLSTD_MP_Survey_AppliedFile;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLSTD_MP_Agreement_Document_AppliedFile }

  TXMLSTD_MP_Agreement_Document_AppliedFile = class(TXMLTAppliedFile, IXMLSTD_MP_Agreement_Document_AppliedFile)
  protected
    { IXMLSTD_MP_Agreement_Document_AppliedFile }
  end;

{ TXMLSTD_MP_NodalPointSchemes }

  TXMLSTD_MP_NodalPointSchemes = class(TXMLNodeCollection, IXMLSTD_MP_NodalPointSchemes)
  protected
    { IXMLSTD_MP_NodalPointSchemes }
    function Get_NodalPointScheme(Index: Integer): IXMLSTD_MP_NodalPointSchemes_NodalPointScheme;
    function Add: IXMLSTD_MP_NodalPointSchemes_NodalPointScheme;
    function Insert(const Index: Integer): IXMLSTD_MP_NodalPointSchemes_NodalPointScheme;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLSTD_MP_NodalPointSchemes_NodalPointScheme }

  TXMLSTD_MP_NodalPointSchemes_NodalPointScheme = class(TXMLNodeCollection, IXMLSTD_MP_NodalPointSchemes_NodalPointScheme)
  protected
    { IXMLSTD_MP_NodalPointSchemes_NodalPointScheme }
    function Get_Definition: UnicodeString;
    function Get_AppliedFile(Index: Integer): IXMLSTD_MP_Survey_AppliedFile;
    procedure Set_Definition(Value: UnicodeString);
    function Add: IXMLSTD_MP_Survey_AppliedFile;
    function Insert(const Index: Integer): IXMLSTD_MP_Survey_AppliedFile;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLSTD_MP_NodalPointSchemes_NodalPointScheme_AppliedFile }

  TXMLSTD_MP_NodalPointSchemes_NodalPointScheme_AppliedFile = class(TXMLTAppliedFile, IXMLSTD_MP_NodalPointSchemes_NodalPointScheme_AppliedFile)
  protected
    { IXMLSTD_MP_NodalPointSchemes_NodalPointScheme_AppliedFile }
  end;

{ TXMLSTD_MP_Appendix }

  TXMLSTD_MP_Appendix = class(TXMLNodeCollection, IXMLSTD_MP_Appendix)
  protected
    { IXMLSTD_MP_Appendix }
    function Get_Document(Index: Integer): IXMLTDocument;
    function Add: IXMLTDocument;
    function Insert(const Index: Integer): IXMLTDocument;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLString_List }

  TXMLString_List = class(TXMLNodeCollection, IXMLString_List)
  protected
    { IXMLString_List }
    function Add(const Value: UnicodeString): IXMLNode;
    function Insert(const Index: Integer; const Value: UnicodeString): IXMLNode;

    function Get_Item(Index: Integer): UnicodeString;
  end;

{ TXMLTChangeParcel_Inner_CadastralNumbers_CadastralNumberList }

  TXMLTChangeParcel_Inner_CadastralNumbers_CadastralNumberList = class(TXMLNodeCollection, IXMLTChangeParcel_Inner_CadastralNumbers_CadastralNumberList)
  protected
    { IXMLTChangeParcel_Inner_CadastralNumbers_CadastralNumberList }
    function Add(const Value: UnicodeString): IXMLNode;
    function Insert(const Index: Integer; const Value: UnicodeString): IXMLNode;

    function Get_Item(Index: Integer): UnicodeString;
  end;

{ TXMLTExistEZParcel_Inner_CadastralNumbers_CadastralNumberList }

  TXMLTExistEZParcel_Inner_CadastralNumbers_CadastralNumberList = class(TXMLNodeCollection, IXMLTExistEZParcel_Inner_CadastralNumbers_CadastralNumberList)
  protected
    { IXMLTExistEZParcel_Inner_CadastralNumbers_CadastralNumberList }
    function Add(const Value: UnicodeString): IXMLNode;
    function Insert(const Index: Integer; const Value: UnicodeString): IXMLNode;

    function Get_Item(Index: Integer): UnicodeString;
  end;

{ TXMLTExistParcel_Inner_CadastralNumbers_CadastralNumberList }

{  TXMLTExistParcel_Inner_CadastralNumbers_CadastralNumberList = class(TXMLNodeCollection, IXMLTExistParcel_Inner_CadastralNumbers_CadastralNumberList)
  protected
    { IXMLTExistParcel_Inner_CadastralNumbers_CadastralNumberList }
 {   function Add(const Value: UnicodeString): IXMLNode;
    function Insert(const Index: Integer; const Value: UnicodeString): IXMLNode;

    function Get_Item(Index: Integer): UnicodeString;
  end;

{ TXMLTInner_CadastralNumbers_CadastralNumberList }

  TXMLTInner_CadastralNumbers_CadastralNumberList = class(TXMLNodeCollection, IXMLTInner_CadastralNumbers_CadastralNumberList)
  protected
    { IXMLTInner_CadastralNumbers_CadastralNumberList }
    function Add(const Value: UnicodeString): IXMLNode;
    function Insert(const Index: Integer; const Value: UnicodeString): IXMLNode;

    function Get_Item(Index: Integer): UnicodeString;
  end;

  { TXMLTInner_CadastralNumbers_CadastralNumberList }


{ TXMLTProviding_Pass_CadastralNumbers_CadastralNumberList }

  TXMLTProviding_Pass_CadastralNumbers_CadastralNumberList = class(TXMLNodeCollection, IXMLTProviding_Pass_CadastralNumbers_CadastralNumberList)
  protected
    { IXMLTProviding_Pass_CadastralNumbers_CadastralNumberList }
    function Add(const Value: UnicodeString): IXMLNode;
    function Insert(const Index: Integer; const Value: UnicodeString): IXMLNode;

    function Get_Item(Index: Integer): UnicodeString;
  end;

function GetSTD_MP(Doc: IXMLDocument): IXMLSTD_MP;
function LoadSTD_MP(const FileName: string): IXMLSTD_MP;
function NewSTD_MP: IXMLSTD_MP;

const
  TargetNamespace = '';

var
  STD_MP_WorkDir: string = '';


implementation
function GetSTD_MP(Doc: IXMLDocument): IXMLSTD_MP;
begin
  Result := Doc.GetDocBinding('STD_MP', TXMLSTD_MP, TargetNamespace)
    as IXMLSTD_MP;
end;

function LoadSTD_MP(const FileName: string): IXMLSTD_MP;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('STD_MP', TXMLSTD_MP,
    TargetNamespace) as IXMLSTD_MP;
end;

function NewSTD_MP: IXMLSTD_MP;
begin
  Result := NewXMLDocument.GetDocBinding('STD_MP', TXMLSTD_MP, TargetNamespace)
    as IXMLSTD_MP;
end;

{ TXMLSTD_MP }

procedure TXMLSTD_MP.AfterConstruction;
begin
  RegisterChildNode('eDocument', TXMLSTD_MP_eDocument);
  RegisterChildNode('Title', TXMLSTD_MP_Title);
  RegisterChildNode('Package', TXMLSTD_MP_Package);
  RegisterChildNode('Coord_Systems', TXMLCoord_Systems);
  RegisterChildNode('Input_Data', TXMLSTD_MP_Input_Data);
  RegisterChildNode('Survey', TXMLSTD_MP_Survey);
  RegisterChildNode('Scheme_Geodesic_Plotting', TXMLSTD_MP_Scheme_Geodesic_Plotting);
  RegisterChildNode('Scheme_Disposition_Parcels', TXMLSTD_MP_Scheme_Disposition_Parcels);
  RegisterChildNode('Diagram_Parcels_SubParcels', TXMLSTD_MP_Diagram_Parcels_SubParcels);
  RegisterChildNode('Agreement_Document', TXMLSTD_MP_Agreement_Document);
  RegisterChildNode('NodalPointSchemes', TXMLSTD_MP_NodalPointSchemes);
  RegisterChildNode('Appendix', TXMLSTD_MP_Appendix);
  inherited;
end;

function TXMLSTD_MP.Get_EDocument: IXMLSTD_MP_eDocument;
begin
  Result := ChildNodes['eDocument'] as IXMLSTD_MP_eDocument;
end;

function TXMLSTD_MP.Get_Title: IXMLSTD_MP_Title;
begin
  Result := ChildNodes['Title'] as IXMLSTD_MP_Title;
end;

function TXMLSTD_MP.Get_Package: IXMLSTD_MP_Package;
begin
  Result := ChildNodes['Package'] as IXMLSTD_MP_Package;
end;

function TXMLSTD_MP.Get_Coord_Systems: IXMLCoord_Systems;
begin
  Result := ChildNodes['Coord_Systems'] as IXMLCoord_Systems;
end;

function TXMLSTD_MP.Get_Input_Data: IXMLSTD_MP_Input_Data;
begin
  Result := ChildNodes['Input_Data'] as IXMLSTD_MP_Input_Data;
end;

function TXMLSTD_MP.Get_Survey: IXMLSTD_MP_Survey;
begin
  Result := ChildNodes['Survey'] as IXMLSTD_MP_Survey;
end;

function TXMLSTD_MP.Get_Conclusion: UnicodeString;
begin
  Result := ChildNodes['Conclusion'].Text;
end;

procedure TXMLSTD_MP.Set_Conclusion(Value: UnicodeString);
begin
  ChildNodes['Conclusion'].NodeValue := Value;
end;

function TXMLSTD_MP.Get_Scheme_Geodesic_Plotting: IXMLSTD_MP_Scheme_Geodesic_Plotting;
begin
  Result := ChildNodes['Scheme_Geodesic_Plotting'] as IXMLSTD_MP_Scheme_Geodesic_Plotting;
end;

function TXMLSTD_MP.Get_Scheme_Disposition_Parcels: IXMLSTD_MP_Scheme_Disposition_Parcels;
begin
  Result := ChildNodes['Scheme_Disposition_Parcels'] as IXMLSTD_MP_Scheme_Disposition_Parcels;
end;

function TXMLSTD_MP.Get_Diagram_Parcels_SubParcels: IXMLSTD_MP_Diagram_Parcels_SubParcels;
begin
  Result := ChildNodes['Diagram_Parcels_SubParcels'] as IXMLSTD_MP_Diagram_Parcels_SubParcels;
end;

function TXMLSTD_MP.Get_Agreement_Document: IXMLSTD_MP_Agreement_Document;
begin
  Result := ChildNodes['Agreement_Document'] as IXMLSTD_MP_Agreement_Document;
end;

function TXMLSTD_MP.Get_NodalPointSchemes: IXMLSTD_MP_NodalPointSchemes;
begin
  Result := ChildNodes['NodalPointSchemes'] as IXMLSTD_MP_NodalPointSchemes;
end;

function TXMLSTD_MP.Get_Appendix: IXMLSTD_MP_Appendix;
begin
  Result := ChildNodes['Appendix'] as IXMLSTD_MP_Appendix;
end;

{ TXMLSTD_MP_eDocument }

function TXMLSTD_MP_eDocument.Get_CodeType: UnicodeString;
begin
  Result := AttributeNodes['CodeType'].Text;
end;

procedure TXMLSTD_MP_eDocument.Set_CodeType(Value: UnicodeString);
begin
  SetAttribute('CodeType', Value);
end;

function TXMLSTD_MP_eDocument.Get_Version: UnicodeString;
begin
  Result := AttributeNodes['Version'].Text;
end;

procedure TXMLSTD_MP_eDocument.Set_Version(Value: UnicodeString);
begin
  SetAttribute('Version', Value);
end;

function TXMLSTD_MP_eDocument.Get_GUID: UnicodeString;
begin
  Result := AttributeNodes['GUID'].Text;
end;

procedure TXMLSTD_MP_eDocument.Set_GUID(Value: UnicodeString);
begin
  SetAttribute('GUID', Value);
end;

{ TXMLSTD_MP_Title }

procedure TXMLSTD_MP_Title.AfterConstruction;
begin
  RegisterChildNode('Contractor', TXMLSTD_MP_Title_Contractor);
  RegisterChildNode('Client', TXMLSTD_MP_Title_Client);
  FClient := CreateCollection(TXMLSTD_MP_Title_ClientList, IXMLSTD_MP_Title_Client, 'Client') as IXMLSTD_MP_Title_ClientList;
  inherited;
end;

function TXMLSTD_MP_Title.Get_Contractor: IXMLSTD_MP_Title_Contractor;
begin
  Result := ChildNodes['Contractor'] as IXMLSTD_MP_Title_Contractor;
end;

function TXMLSTD_MP_Title.Get_Purpose: UnicodeString;
begin
  Result := ChildNodes['Purpose'].Text;
end;

procedure TXMLSTD_MP_Title.Set_Purpose(Value: UnicodeString);
begin
  ChildNodes['Purpose'].NodeValue := Value;
end;

function TXMLSTD_MP_Title.Get_Reason: UnicodeString;
begin
  Result := ChildNodes['Reason'].Text;
end;

procedure TXMLSTD_MP_Title.Set_Reason(Value: UnicodeString);
begin
  ChildNodes['Reason'].NodeValue := Value;
end;

function TXMLSTD_MP_Title.Get_Client: IXMLSTD_MP_Title_ClientList;
begin
  Result := FClient;
end;

{ TXMLSTD_MP_Title_Contractor }

procedure TXMLSTD_MP_Title_Contractor.AfterConstruction;
begin
  RegisterChildNode('FIO', TXMLTFIO);
  inherited;
end;

function TXMLSTD_MP_Title_Contractor.Get_Date: UnicodeString;
begin
  Result := AttributeNodes['Date'].Text;
end;

procedure TXMLSTD_MP_Title_Contractor.Set_Date(Value: UnicodeString);
begin
  SetAttribute('Date', Value);
end;

function TXMLSTD_MP_Title_Contractor.Get_FIO: IXMLTFIO;
begin
  Result := ChildNodes['FIO'] as IXMLTFIO;
end;

function TXMLSTD_MP_Title_Contractor.Get_N_Certificate: UnicodeString;
begin
  Result := ChildNodes['N_Certificate'].Text;
end;

procedure TXMLSTD_MP_Title_Contractor.Set_N_Certificate(Value: UnicodeString);
begin
  ChildNodes['N_Certificate'].NodeValue := Value;
end;

function TXMLSTD_MP_Title_Contractor.Get_Telephone: UnicodeString;
begin
  Result := ChildNodes['Telephone'].Text;
end;

procedure TXMLSTD_MP_Title_Contractor.Set_Telephone(Value: UnicodeString);
begin
  ChildNodes['Telephone'].NodeValue := Value;
end;

function TXMLSTD_MP_Title_Contractor.Get_Address: UnicodeString;
begin
  Result := ChildNodes['Address'].Text;
end;

procedure TXMLSTD_MP_Title_Contractor.Set_Address(Value: UnicodeString);
begin
  ChildNodes['Address'].NodeValue := Value;
end;

function TXMLSTD_MP_Title_Contractor.Get_E_mail: UnicodeString;
begin
  Result := ChildNodes['E_mail'].Text;
end;

procedure TXMLSTD_MP_Title_Contractor.Set_E_mail(Value: UnicodeString);
begin
  ChildNodes['E_mail'].NodeValue := Value;
end;

function TXMLSTD_MP_Title_Contractor.Get_Organization: UnicodeString;
begin
  Result := ChildNodes['Organization'].Text;
end;

procedure TXMLSTD_MP_Title_Contractor.Set_Organization(Value: UnicodeString);
begin
  ChildNodes['Organization'].NodeValue := Value;
end;

{ TXMLTFIO }

function TXMLTFIO.Get_Surname: UnicodeString;
begin
  Result := ChildNodes['Surname'].Text;
end;

procedure TXMLTFIO.Set_Surname(Value: UnicodeString);
begin
  ChildNodes['Surname'].NodeValue := Value;
end;

function TXMLTFIO.Get_First: UnicodeString;
begin
  Result := ChildNodes['First'].Text;
end;

procedure TXMLTFIO.Set_First(Value: UnicodeString);
begin
  ChildNodes['First'].NodeValue := Value;
end;

function TXMLTFIO.Get_Patronymic: UnicodeString;
begin
  Result := ChildNodes['Patronymic'].Text;
end;

procedure TXMLTFIO.Set_Patronymic(Value: UnicodeString);
begin
  ChildNodes['Patronymic'].NodeValue := Value;
end;

{ TXMLSTD_MP_Title_Client }

procedure TXMLSTD_MP_Title_Client.AfterConstruction;
begin
  RegisterChildNode('Person', TXMLSTD_MP_Title_Client_Person);
  RegisterChildNode('Organization', TXMLSTD_MP_Title_Client_Organization);
  RegisterChildNode('Foreign_Organization', TXMLSTD_MP_Title_Client_Foreign_Organization);
  RegisterChildNode('Governance', TXMLSTD_MP_Title_Client_Governance);
  inherited;
end;

function TXMLSTD_MP_Title_Client.Get_Date: UnicodeString;
begin
  Result := AttributeNodes['Date'].Text;
end;

procedure TXMLSTD_MP_Title_Client.Set_Date(Value: UnicodeString);
begin
  SetAttribute('Date', Value);
end;

function TXMLSTD_MP_Title_Client.Get_Person: IXMLSTD_MP_Title_Client_Person;
begin
  Result := ChildNodes['Person'] as IXMLSTD_MP_Title_Client_Person;
end;

function TXMLSTD_MP_Title_Client.Get_Organization: IXMLSTD_MP_Title_Client_Organization;
begin
  Result := ChildNodes['Organization'] as IXMLSTD_MP_Title_Client_Organization;
end;

function TXMLSTD_MP_Title_Client.Get_Foreign_Organization: IXMLSTD_MP_Title_Client_Foreign_Organization;
begin
  Result := ChildNodes['Foreign_Organization'] as IXMLSTD_MP_Title_Client_Foreign_Organization;
end;

function TXMLSTD_MP_Title_Client.Get_Governance: IXMLSTD_MP_Title_Client_Governance;
begin
  Result := ChildNodes['Governance'] as IXMLSTD_MP_Title_Client_Governance;
end;

{ TXMLSTD_MP_Title_ClientList }

function TXMLSTD_MP_Title_ClientList.Add: IXMLSTD_MP_Title_Client;
begin
  Result := AddItem(-1) as IXMLSTD_MP_Title_Client;
end;

function TXMLSTD_MP_Title_ClientList.Insert(const Index: Integer): IXMLSTD_MP_Title_Client;
begin
  Result := AddItem(Index) as IXMLSTD_MP_Title_Client;
end;

function TXMLSTD_MP_Title_ClientList.Get_Item(Index: Integer): IXMLSTD_MP_Title_Client;
begin
  Result := List[Index] as IXMLSTD_MP_Title_Client;
end;

{ TXMLSTD_MP_Title_Client_Person }

procedure TXMLSTD_MP_Title_Client_Person.AfterConstruction;
begin
  RegisterChildNode('FIO', TXMLTFIO);
  inherited;
end;

function TXMLSTD_MP_Title_Client_Person.Get_FIO: IXMLTFIO;
begin
  Result := ChildNodes['FIO'] as IXMLTFIO;
end;

{ TXMLSTD_MP_Title_Client_Organization }

procedure TXMLSTD_MP_Title_Client_Organization.AfterConstruction;
begin
  RegisterChildNode('Agent', TXMLSTD_MP_Title_Client_Organization_Agent);
  inherited;
end;

function TXMLSTD_MP_Title_Client_Organization.Get_Name: UnicodeString;
begin
  Result := ChildNodes['Name'].Text;
end;

procedure TXMLSTD_MP_Title_Client_Organization.Set_Name(Value: UnicodeString);
begin
  ChildNodes['Name'].NodeValue := Value;
end;

function TXMLSTD_MP_Title_Client_Organization.Get_Agent: IXMLSTD_MP_Title_Client_Organization_Agent;
begin
  Result := ChildNodes['Agent'] as IXMLSTD_MP_Title_Client_Organization_Agent;
end;

{ TXMLSTD_MP_Title_Client_Organization_Agent }

function TXMLSTD_MP_Title_Client_Organization_Agent.Get_Appointment: UnicodeString;
begin
  Result := ChildNodes['Appointment'].Text;
end;

procedure TXMLSTD_MP_Title_Client_Organization_Agent.Set_Appointment(Value: UnicodeString);
begin
  ChildNodes['Appointment'].NodeValue := Value;
end;

{ TXMLSTD_MP_Title_Client_Foreign_Organization }

procedure TXMLSTD_MP_Title_Client_Foreign_Organization.AfterConstruction;
begin
  RegisterChildNode('Agent', TXMLSTD_MP_Title_Client_Organization_Agent); //TODO
  inherited;
end;

function TXMLSTD_MP_Title_Client_Foreign_Organization.Get_Name: UnicodeString;
begin
  Result := ChildNodes['Name'].Text;
end;

procedure TXMLSTD_MP_Title_Client_Foreign_Organization.Set_Name(Value: UnicodeString);
begin
  ChildNodes['Name'].NodeValue := Value;
end;

function TXMLSTD_MP_Title_Client_Foreign_Organization.Get_Country: UnicodeString;
begin
  Result := ChildNodes['Country'].Text;
end;

procedure TXMLSTD_MP_Title_Client_Foreign_Organization.Set_Country(Value: UnicodeString);
begin
  ChildNodes['Country'].NodeValue := Value;
end;

function TXMLSTD_MP_Title_Client_Foreign_Organization.Get_Agent: IXMLSTD_MP_Title_Client_Organization_Agent;
begin
  Result := ChildNodes['Agent'] as IXMLSTD_MP_Title_Client_Organization_Agent;
end;

{ TXMLSTD_MP_Title_Client_Foreign_Organization_Agent }

function TXMLSTD_MP_Title_Client_Foreign_Organization_Agent.Get_Appointment: UnicodeString;
begin
  Result := ChildNodes['Appointment'].Text;
end;

procedure TXMLSTD_MP_Title_Client_Foreign_Organization_Agent.Set_Appointment(Value: UnicodeString);
begin
  ChildNodes['Appointment'].NodeValue := Value;
end;

{ TXMLSTD_MP_Title_Client_Governance }

procedure TXMLSTD_MP_Title_Client_Governance.AfterConstruction;
begin
  RegisterChildNode('Agent', TXMLSTD_MP_Title_Client_Organization_Agent);
  inherited;
end;

function TXMLSTD_MP_Title_Client_Governance.Get_Name: UnicodeString;
begin
  Result := ChildNodes['Name'].Text;
end;

procedure TXMLSTD_MP_Title_Client_Governance.Set_Name(Value: UnicodeString);
begin
  ChildNodes['Name'].NodeValue := Value;
end;

function TXMLSTD_MP_Title_Client_Governance.Get_Agent: IXMLSTD_MP_Title_Client_Organization_Agent;
begin
  Result := ChildNodes['Agent'] as IXMLSTD_MP_Title_Client_Organization_Agent;
end;

{ TXMLSTD_MP_Title_Client_Governance_Agent }

function TXMLSTD_MP_Title_Client_Governance_Agent.Get_Appointment: UnicodeString;
begin
  Result := ChildNodes['Appointment'].Text;
end;

procedure TXMLSTD_MP_Title_Client_Governance_Agent.Set_Appointment(Value: UnicodeString);
begin
  ChildNodes['Appointment'].NodeValue := Value;
end;

{ TXMLSTD_MP_Package }

procedure TXMLSTD_MP_Package.AfterConstruction;
begin
  RegisterChildNode('FormParcels', TXMLSTD_MP_Package_FormParcels);
  RegisterChildNode('SpecifyParcel', TXMLSTD_MP_Package_SpecifyParcel);
  RegisterChildNode('NewSubParcel', TXMLTNewSubParcel);
  RegisterChildNode('SpecifyParcelsApproximal', TXMLSTD_MP_Package_SpecifyParcelsApproximal);
  FNewSubParcel := CreateCollection(TXMLTNewSubParcelList, IXMLTNewSubParcel, 'NewSubParcel') as IXMLTNewSubParcelList;
  FSpecifyParcelsApproximal := CreateCollection(TXMLSTD_MP_Package_SpecifyParcelsApproximalList, IXMLSTD_MP_Package_SpecifyParcelsApproximal, 'SpecifyParcelsApproximal') as IXMLSTD_MP_Package_SpecifyParcelsApproximalList;
  inherited;
end;

function TXMLSTD_MP_Package.Get_FormParcels: IXMLSTD_MP_Package_FormParcels;
begin
  Result := ChildNodes['FormParcels'] as IXMLSTD_MP_Package_FormParcels;
end;

function TXMLSTD_MP_Package.Get_SpecifyParcel: IXMLSTD_MP_Package_SpecifyParcel;
begin
  Result := ChildNodes['SpecifyParcel'] as IXMLSTD_MP_Package_SpecifyParcel;
end;

function TXMLSTD_MP_Package.Get_NewSubParcel: IXMLTNewSubParcelList;
begin
  Result := FNewSubParcel;
end;

function TXMLSTD_MP_Package.Get_SpecifyParcelsApproximal: IXMLSTD_MP_Package_SpecifyParcelsApproximalList;
begin
  Result := FSpecifyParcelsApproximal;
end;

{ TXMLSTD_MP_Package_FormParcels }

procedure TXMLSTD_MP_Package_FormParcels.AfterConstruction;
begin
  RegisterChildNode('NewParcel', TXMLTNewParcel);
  RegisterChildNode('ChangeParcel', TXMLTChangeParcel);
  //RegisterChildNode('SpecifyRelatedParcel', TXMLSTD_MP_Package_FormParcels_SpecifyRelatedParcel);
 // RegisterChildNode('SpecifyRelatedParcel', TXMLSTD_MP_Package_SpecifyParcel_SpecifyRelatedParcel);
  FNewParcel := CreateCollection(TXMLTNewParcelList, IXMLTNewParcel, 'NewParcel') as IXMLTNewParcelList;
  FChangeParcel := CreateCollection(TXMLTChangeParcelList, IXMLTChangeParcel, 'ChangeParcel') as IXMLTChangeParcelList;
  //FSpecifyRelatedParcel := CreateCollection(TXMLSTD_MP_Package_FormParcels_SpecifyRelatedParcelList, IXMLSTD_MP_Package_FormParcels_SpecifyRelatedParcel, 'SpecifyRelatedParcel') as IXMLSTD_MP_Package_FormParcels_SpecifyRelatedParcelList;
  //FSpecifyRelatedParcel := CreateCollection(TXMLSTD_MP_Package_SpecifyParcel_SpecifyRelatedParcelList,
  //                          IXMLSTD_MP_Package_SpecifyParcel_SpecifyRelatedParcelList, 'SpecifyRelatedParcel') as IXMLSTD_MP_Package_SpecifyParcel_SpecifyRelatedParcelList;
   RegisterChildNode('SpecifyRelatedParcel', TXMLSTD_MP_Package_SpecifyParcel_SpecifyRelatedParcel);
  FSpecifyRelatedParcel := CreateCollection(TXMLSTD_MP_Package_SpecifyParcel_SpecifyRelatedParcelList, IXMLSTD_MP_Package_SpecifyParcel_SpecifyRelatedParcel, 'SpecifyRelatedParcel') as IXMLSTD_MP_Package_SpecifyParcel_SpecifyRelatedParcelList;
  inherited;
end;

function TXMLSTD_MP_Package_FormParcels.Get_Method: UnicodeString;
begin
  Result := AttributeNodes['Method'].Text;
end;

procedure TXMLSTD_MP_Package_FormParcels.Set_Method(Value: UnicodeString);
begin
  SetAttribute('Method', Value);
end;

function TXMLSTD_MP_Package_FormParcels.Get_NewParcel: IXMLTNewParcelList;
begin
  Result := FNewParcel;
end;

function TXMLSTD_MP_Package_FormParcels.Get_ChangeParcel: IXMLTChangeParcelList;
begin
  Result := FChangeParcel;
end;

//function TXMLSTD_MP_Package_FormParcels.Get_SpecifyRelatedParcel: IXMLSTD_MP_Package_FormParcels_SpecifyRelatedParcelList;
function TXMLSTD_MP_Package_FormParcels.Get_SpecifyRelatedParcel: IXMLSTD_MP_Package_SpecifyParcel_SpecifyRelatedParcelList;
begin
  Result := FSpecifyRelatedParcel;
end;

{ TXMLTNewParcel }

procedure TXMLTNewParcel.AfterConstruction;
begin
  RegisterChildNode('Prev_CadastralNumbers', TXMLTNewParcel_Prev_CadastralNumbers);
  RegisterChildNode('Providing_Pass_CadastralNumbers', TXMLTProviding_Pass_CadastralNumbers);
  RegisterChildNode('Inner_CadastralNumbers', TXMLTInner_CadastralNumbers);
  RegisterChildNode('Area', TXMLTArea); //TXMLTAreaNew
  RegisterChildNode('Location', TXMLTNewParcel_Location);
  RegisterChildNode('Category', TXMLTCategory);
  RegisterChildNode('NaturalObject', TXMLTNatural_Object);
  RegisterChildNode('Utilization', TXMLTUtilization);
  RegisterChildNode('SubParcels', TXMLTNewParcel_SubParcels);
  {$ifndef disableIXMLTNewParcel_Contours}
  RegisterChildNode('Contours', TXMLTNewParcel_Contours);
  {$else}
  RegisterChildNode('Contours', TXMLContours);
  {$endif}


  RegisterChildNode('Entity_Spatial', TXMLEntity_Spatial);
  RegisterChildNode('Min_Area', TXMLTArea_without_Innccuracy);
  RegisterChildNode('Max_Area', TXMLTArea_without_Innccuracy);
  inherited;
end;

function TXMLTNewParcel.Get_Definition: UnicodeString;
begin
  Result := AttributeNodes['Definition'].Text;
end;

procedure TXMLTNewParcel.Set_Definition(Value: UnicodeString);
begin
  SetAttribute('Definition', Value);
end;

function TXMLTNewParcel.Get_CadastralBlock: UnicodeString;
begin
  Result := ChildNodes['CadastralBlock'].Text;
end;

procedure TXMLTNewParcel.Set_CadastralBlock(Value: UnicodeString);
begin
  ChildNodes['CadastralBlock'].NodeValue := Value;
end;

function TXMLTNewParcel.Get_Prev_CadastralNumbers: IXMLTNewParcel_Prev_CadastralNumbers;
begin
  Result := ChildNodes['Prev_CadastralNumbers'] as IXMLTNewParcel_Prev_CadastralNumbers;
end;

function TXMLTNewParcel.Get_Providing_Pass_CadastralNumbers: IXMLTProviding_Pass_CadastralNumbers;
begin
  Result := ChildNodes['Providing_Pass_CadastralNumbers'] as IXMLTProviding_Pass_CadastralNumbers;
end;

function TXMLTNewParcel.Get_Inner_CadastralNumbers: IXMLTInner_CadastralNumbers;
begin
  Result := ChildNodes['Inner_CadastralNumbers'] as IXMLTInner_CadastralNumbers;
end;

function TXMLTNewParcel.Get_Area: IXMLTArea;
begin
  Result := ChildNodes['Area'] as IXMLTArea;
end;

function TXMLTNewParcel.Get_Location: IXMLTNewParcel_Location;
begin
  Result := ChildNodes['Location'] as IXMLTNewParcel_Location;
end;

function TXMLTNewParcel.Get_Category: IXMLTCategory;
begin
  Result := ChildNodes['Category'] as IXMLTCategory;
end;

function TXMLTNewParcel.Get_NaturalObject: IXMLTNatural_Object;
begin
  Result := ChildNodes['NaturalObject'] as IXMLTNatural_Object;
end;

function TXMLTNewParcel.Get_Utilization: IXMLTUtilization;
begin
  Result := ChildNodes['Utilization'] as IXMLTUtilization;
end;

function TXMLTNewParcel.Get_SubParcels: IXMLTNewParcel_SubParcels;
begin
  Result := ChildNodes['SubParcels'] as IXMLTNewParcel_SubParcels;
end;
{$ifndef disableIXMLTNewParcel_Contours}
function TXMLTNewParcel.Get_Contours: IXMLTNewParcel_Contours;
begin
  Result := ChildNodes['Contours'] as IXMLTNewParcel_Contours;
end;
{$else}
function TXMLTNewParcel.Get_Contours: IXMLContours;
begin
  Result := ChildNodes['Contours'] as IXMLContours;
end;
{$endif}


function TXMLTNewParcel.Get_Entity_Spatial: IXMLEntity_Spatial;
begin
  Result := ChildNodes['Entity_Spatial'] as IXMLEntity_Spatial;
end;

function TXMLTNewParcel.Get_Min_Area: IXMLTArea_without_Innccuracy;
begin
  Result := ChildNodes['Min_Area'] as IXMLTArea_without_Innccuracy;
end;

function TXMLTNewParcel.Get_Max_Area: IXMLTArea_without_Innccuracy;
begin
  Result := ChildNodes['Max_Area'] as IXMLTArea_without_Innccuracy;
end;

function TXMLTNewParcel.Get_Note: UnicodeString;
begin
  Result := ChildNodes['Note'].Text;
end;

procedure TXMLTNewParcel.Set_Note(Value: UnicodeString);
begin
  ChildNodes['Note'].NodeValue := Value;
end;

{ TXMLTNewParcelList }

function TXMLTNewParcelList.Add: IXMLTNewParcel;
begin
  Result := AddItem(-1) as IXMLTNewParcel;
end;

function TXMLTNewParcelList.Insert(const Index: Integer): IXMLTNewParcel;
begin
  Result := AddItem(Index) as IXMLTNewParcel;
end;

function TXMLTNewParcelList.Get_Item(Index: Integer): IXMLTNewParcel;
begin
  Result := List[Index] as IXMLTNewParcel;
end;

{ TXMLTNewParcel_Prev_CadastralNumbers }

procedure TXMLTNewParcel_Prev_CadastralNumbers.AfterConstruction;
begin
  ItemTag := 'CadastralNumber';
  ItemInterface := IXMLNode;
  inherited;
end;

function TXMLTNewParcel_Prev_CadastralNumbers.Get_CadastralNumber(Index: Integer): UnicodeString;
begin
  Result := List[Index].Text;
end;

function TXMLTNewParcel_Prev_CadastralNumbers.Add(const CadastralNumber: UnicodeString): IXMLNode;
begin
  Result := AddItem(-1);
  Result.NodeValue := CadastralNumber;
end;

function TXMLTNewParcel_Prev_CadastralNumbers.Insert(const Index: Integer; const CadastralNumber: UnicodeString): IXMLNode;
begin
  Result := AddItem(Index);
  Result.NodeValue := CadastralNumber;
end;

{ TXMLTProviding_Pass_CadastralNumbers }

procedure TXMLTProviding_Pass_CadastralNumbers.AfterConstruction;
begin
  RegisterChildNode('Documents', TXMLTProviding_Pass_CadastralNumbers_Documents);
  FCadastralNumber := CreateCollection(TXMLTProviding_Pass_CadastralNumbers_CadastralNumberList, IXMLNode, 'CadastralNumber') as IXMLTProviding_Pass_CadastralNumbers_CadastralNumberList;
  FDefinition := CreateCollection(TXMLString_List, IXMLNode, 'Definition') as IXMLString_List;
  inherited;
end;

function TXMLTProviding_Pass_CadastralNumbers.Get_CadastralNumber: IXMLTProviding_Pass_CadastralNumbers_CadastralNumberList;
begin
  Result := FCadastralNumber;
end;

function TXMLTProviding_Pass_CadastralNumbers.Get_Definition: IXMLString_List;
begin
  Result := FDefinition;
end;

function TXMLTProviding_Pass_CadastralNumbers.Get_Other: UnicodeString;
begin
  Result := ChildNodes['Other'].Text;
end;

procedure TXMLTProviding_Pass_CadastralNumbers.Set_Other(Value: UnicodeString);
begin
  ChildNodes['Other'].NodeValue := Value;
end;

function TXMLTProviding_Pass_CadastralNumbers.Get_Documents: IXMLTProviding_Pass_CadastralNumbers_Documents;
begin
  Result := ChildNodes['Documents'] as IXMLTProviding_Pass_CadastralNumbers_Documents;
end;

{ TXMLTProviding_Pass_CadastralNumbers_Documents }

procedure TXMLTProviding_Pass_CadastralNumbers_Documents.AfterConstruction;
begin
  RegisterChildNode('Document', TXMLTDocument);
  ItemTag := 'Document';
  ItemInterface := IXMLTDocument;
  inherited;
end;

function TXMLTProviding_Pass_CadastralNumbers_Documents.Get_Document(Index: Integer): IXMLTDocument;
begin
  Result := List[Index] as IXMLTDocument;
end;

function TXMLTProviding_Pass_CadastralNumbers_Documents.Add: IXMLTDocument;
begin
  Result := AddItem(-1) as IXMLTDocument;
end;

function TXMLTProviding_Pass_CadastralNumbers_Documents.Insert(const Index: Integer): IXMLTDocument;
begin
  Result := AddItem(Index) as IXMLTDocument;
end;

{ TXMLTDocument }

procedure TXMLTDocument.AfterConstruction;
begin
  RegisterChildNode('Duration', TXMLDuration);
  RegisterChildNode('AppliedFiles', TXMLAppliedFiles);
  inherited;
end;

function TXMLTDocument.Get_Code_Document: UnicodeString;
begin
  Result := ChildNodes['Code_Document'].Text;
end;

procedure TXMLTDocument.Set_Code_Document(Value: UnicodeString);
begin
  ChildNodes['Code_Document'].NodeValue := Value;
end;

function TXMLTDocument.Get_Name: UnicodeString;
begin
  Result := ChildNodes['Name'].Text;
end;

procedure TXMLTDocument.Set_Name(Value: UnicodeString);
begin
  ChildNodes['Name'].NodeValue := Value;
end;

function TXMLTDocument.Get_Series: UnicodeString;
begin
  Result := ChildNodes['Series'].Text;
end;

procedure TXMLTDocument.Set_Series(Value: UnicodeString);
begin
  ChildNodes['Series'].NodeValue := Value;
end;

function TXMLTDocument.Get_Number: UnicodeString;
begin
  Result := ChildNodes['Number'].Text;
end;

procedure TXMLTDocument.Set_Number(Value: UnicodeString);
begin
  ChildNodes['Number'].NodeValue := Value;
end;

function TXMLTDocument.Get_Date: UnicodeString;
begin
  Result := ChildNodes['Date'].Text;
end;

procedure TXMLTDocument.Set_Date(Value: UnicodeString);
begin
  ChildNodes['Date'].NodeValue := Value;
end;

function TXMLTDocument.Get_IssueOrgan: UnicodeString;
begin
  Result := ChildNodes['IssueOrgan'].Text;
end;

procedure TXMLTDocument.Set_IssueOrgan(Value: UnicodeString);
begin
  ChildNodes['IssueOrgan'].NodeValue := Value;
end;

function TXMLTDocument.Get_NumberReg: UnicodeString;
begin
  Result := ChildNodes['NumberReg'].Text;
end;

procedure TXMLTDocument.Set_NumberReg(Value: UnicodeString);
begin
  ChildNodes['NumberReg'].NodeValue := Value;
end;

function TXMLTDocument.Get_DateReg: UnicodeString;
begin
  Result := ChildNodes['DateReg'].Text;
end;

procedure TXMLTDocument.Set_DateReg(Value: UnicodeString);
begin
  ChildNodes['DateReg'].NodeValue := Value;
end;

function TXMLTDocument.Get_Duration: IXMLDuration;
begin
  Result := ChildNodes['Duration'] as IXMLDuration;
end;

function TXMLTDocument.Get_Register: UnicodeString;
begin
  Result := ChildNodes['Register'].Text;
end;

procedure TXMLTDocument.Set_Register(Value: UnicodeString);
begin
  ChildNodes['Register'].NodeValue := Value;
end;

function TXMLTDocument.Get_Desc: UnicodeString;
begin
  Result := ChildNodes['Desc'].Text;
end;

procedure TXMLTDocument.Set_Desc(Value: UnicodeString);
begin
  ChildNodes['Desc'].NodeValue := Value;
end;

function TXMLTDocument.Get_IssueOrgan_Code: UnicodeString;
begin
  Result := ChildNodes['IssueOrgan_Code'].Text;
end;

procedure TXMLTDocument.Set_IssueOrgan_Code(Value: UnicodeString);
begin
  ChildNodes['IssueOrgan_Code'].NodeValue := Value;
end;

function TXMLTDocument.Get_AppliedFiles: IXMLAppliedFiles;
begin
  Result := ChildNodes['AppliedFiles'] as IXMLAppliedFiles;
end;

{ TXMLDuration }

function TXMLDuration.Get_Started: UnicodeString;
begin
  Result := ChildNodes['Started'].Text;
end;

procedure TXMLDuration.Set_Started(Value: UnicodeString);
begin
  ChildNodes['Started'].NodeValue := Value;
end;

function TXMLDuration.Get_Stopped: UnicodeString;
begin
  Result := ChildNodes['Stopped'].Text;
end;

procedure TXMLDuration.Set_Stopped(Value: UnicodeString);
begin
  ChildNodes['Stopped'].NodeValue := Value;
end;

{ TXMLAppliedFiles }

procedure TXMLAppliedFiles.AfterConstruction;
begin
  RegisterChildNode('AppliedFile', TXMLTAppliedFile);
  ItemTag := 'AppliedFile';
  ItemInterface := IXMLTAppliedFile;
  inherited;
end;

function TXMLAppliedFiles.Get_AppliedFile(Index: Integer): IXMLTAppliedFile;
begin
  Result := List[Index] as IXMLTAppliedFile;
end;

function TXMLAppliedFiles.Add: IXMLTAppliedFile;
begin
  Result := AddItem(-1) as IXMLTAppliedFile;
end;

function TXMLAppliedFiles.Insert(const Index: Integer): IXMLTAppliedFile;
begin
  Result := AddItem(Index) as IXMLTAppliedFile;
end;

{ TXMLTAppliedFile }

function TXMLTAppliedFile.Get_Kind: UnicodeString;
begin
  Result := AttributeNodes['Kind'].Text;
end;

procedure TXMLTAppliedFile.Set_Kind(Value: UnicodeString);
begin
  SetAttribute('Kind', Value);
end;

function TXMLTAppliedFile.Get_Name: UnicodeString;
begin
  Result := AttributeNodes['Name'].Text;
end;

procedure TXMLTAppliedFile.Set_Name(Value: UnicodeString);
begin
  SetAttribute('Name', Value);
end;

{ TXMLTInner_CadastralNumbers }

procedure TXMLTInner_CadastralNumbers.AfterConstruction;
begin
  FCadastralNumber := CreateCollection(TXMLTInner_CadastralNumbers_CadastralNumberList, IXMLNode, 'CadastralNumber') as IXMLTInner_CadastralNumbers_CadastralNumberList;
  FNumber := CreateCollection(TXMLString_List, IXMLNode, 'Number') as IXMLString_List;
  inherited;
end;

function TXMLTInner_CadastralNumbers.Get_CadastralNumber: IXMLTInner_CadastralNumbers_CadastralNumberList;
begin
  Result := FCadastralNumber;
end;

function TXMLTInner_CadastralNumbers.Get_Number: IXMLString_List;
begin
  Result := FNumber;
end;

{ TXMLTAreaNew }

function TXMLTAreaNew.Get_Area: LongWord;
begin
  Result := ChildNodes['Area'].NodeValue;
end;

procedure TXMLTAreaNew.Set_Area(Value: LongWord);
begin
  ChildNodes['Area'].NodeValue := Value;
end;

function TXMLTAreaNew.Get_Unit_: UnicodeString;
begin
  Result := ChildNodes['Unit'].Text;
end;

procedure TXMLTAreaNew.Set_Unit_(Value: UnicodeString);
begin
  ChildNodes['Unit'].NodeValue := Value;
end;

function TXMLTAreaNew.Get_Innccuracy: LongWord;
begin
  Result := ChildNodes['Innccuracy'].NodeValue;
end;

procedure TXMLTAreaNew.Set_Innccuracy(Value: LongWord);
begin
  ChildNodes['Innccuracy'].NodeValue := Value;
end;

{ TXMLTAddress }

procedure TXMLTAddress.AfterConstruction;
begin
  RegisterChildNode('District', TXMLDistrict);
  RegisterChildNode('City', TXMLCity);
  RegisterChildNode('Urban_District', TXMLUrban_District);
  RegisterChildNode('Soviet_Village', TXMLSoviet_Village);
  RegisterChildNode('Locality', TXMLLocality);
  RegisterChildNode('Street', TXMLStreet);
  RegisterChildNode('Level1', TXMLLevel1);
  RegisterChildNode('Level2', TXMLLevel2);
  RegisterChildNode('Level3', TXMLLevel3);
  RegisterChildNode('Apartment', TXMLApartment);
  inherited;
end;

function TXMLTAddress.Get_Code_OKATO: UnicodeString;
begin
  Result := ChildNodes['Code_OKATO'].Text;
end;

procedure TXMLTAddress.Set_Code_OKATO(Value: UnicodeString);
begin
  ChildNodes['Code_OKATO'].NodeValue := Value;
end;

function TXMLTAddress.Get_Code_KLADR: UnicodeString;
begin
  Result := ChildNodes['Code_KLADR'].Text;
end;

procedure TXMLTAddress.Set_Code_KLADR(Value: UnicodeString);
begin
  ChildNodes['Code_KLADR'].NodeValue := Value;
end;

function TXMLTAddress.Get_Postal_Code: UnicodeString;
begin
  Result := ChildNodes['Postal_Code'].Text;
end;

procedure TXMLTAddress.Set_Postal_Code(Value: UnicodeString);
begin
  ChildNodes['Postal_Code'].NodeValue := Value;
end;

function TXMLTAddress.Get_Region: UnicodeString;
begin
  Result := ChildNodes['Region'].Text;
end;

procedure TXMLTAddress.Set_Region(Value: UnicodeString);
begin
  ChildNodes['Region'].NodeValue := Value;
end;

function TXMLTAddress.Get_District: IXMLDistrict;
begin
  Result := ChildNodes['District'] as IXMLDistrict;
end;

function TXMLTAddress.Get_City: IXMLCity;
begin
  Result := ChildNodes['City'] as IXMLCity;
end;

function TXMLTAddress.Get_Urban_District: IXMLUrban_District;
begin
  Result := ChildNodes['Urban_District'] as IXMLUrban_District;
end;

function TXMLTAddress.Get_Soviet_Village: IXMLSoviet_Village;
begin
  Result := ChildNodes['Soviet_Village'] as IXMLSoviet_Village;
end;

function TXMLTAddress.Get_Locality: IXMLLocality;
begin
  Result := ChildNodes['Locality'] as IXMLLocality;
end;

function TXMLTAddress.Get_Street: IXMLStreet;
begin
  Result := ChildNodes['Street'] as IXMLStreet;
end;

function TXMLTAddress.Get_Level1: IXMLLevel1;
begin
  Result := ChildNodes['Level1'] as IXMLLevel1;
end;

function TXMLTAddress.Get_Level2: IXMLLevel2;
begin
  Result := ChildNodes['Level2'] as IXMLLevel2;
end;

function TXMLTAddress.Get_Level3: IXMLLevel3;
begin
  Result := ChildNodes['Level3'] as IXMLLevel3;
end;

function TXMLTAddress.Get_Apartment: IXMLApartment;
begin
  Result := ChildNodes['Apartment'] as IXMLApartment;
end;

function TXMLTAddress.Get_Other: UnicodeString;
begin
  Result := ChildNodes['Other'].Text;
end;

procedure TXMLTAddress.Set_Other(Value: UnicodeString);
begin
  ChildNodes['Other'].NodeValue := Value;
end;

function TXMLTAddress.Get_Note: UnicodeString;
begin
  Result := ChildNodes['Note'].Text;
end;

procedure TXMLTAddress.Set_Note(Value: UnicodeString);
begin
  ChildNodes['Note'].NodeValue := Value;
end;

{ TXMLDistrict }

function TXMLDistrict.Get_Name: UnicodeString;
begin
  Result := AttributeNodes['Name'].Text;
end;

procedure TXMLDistrict.Set_Name(Value: UnicodeString);
begin
  SetAttribute('Name', Value);
end;

function TXMLDistrict.Get_Type_: UnicodeString;
begin
  Result := AttributeNodes['Type'].Text;
end;

procedure TXMLDistrict.Set_Type_(Value: UnicodeString);
begin
  SetAttribute('Type', Value);
end;

{ TXMLCity }

function TXMLCity.Get_Name: UnicodeString;
begin
  Result := AttributeNodes['Name'].Text;
end;

procedure TXMLCity.Set_Name(Value: UnicodeString);
begin
  SetAttribute('Name', Value);
end;

function TXMLCity.Get_Type_: UnicodeString;
begin
  Result := AttributeNodes['Type'].Text;
end;

procedure TXMLCity.Set_Type_(Value: UnicodeString);
begin
  SetAttribute('Type', Value);
end;

{ TXMLUrban_District }

function TXMLUrban_District.Get_Name: UnicodeString;
begin
  Result := AttributeNodes['Name'].Text;
end;

procedure TXMLUrban_District.Set_Name(Value: UnicodeString);
begin
  SetAttribute('Name', Value);
end;

function TXMLUrban_District.Get_Type_: UnicodeString;
begin
  Result := AttributeNodes['Type'].Text;
end;

procedure TXMLUrban_District.Set_Type_(Value: UnicodeString);
begin
  SetAttribute('Type', Value);
end;

{ TXMLSoviet_Village }

function TXMLSoviet_Village.Get_Name: UnicodeString;
begin
  Result := AttributeNodes['Name'].Text;
end;

procedure TXMLSoviet_Village.Set_Name(Value: UnicodeString);
begin
  SetAttribute('Name', Value);
end;

function TXMLSoviet_Village.Get_Type_: UnicodeString;
begin
  Result := AttributeNodes['Type'].Text;
end;

procedure TXMLSoviet_Village.Set_Type_(Value: UnicodeString);
begin
  SetAttribute('Type', Value);
end;

{ TXMLLocality }

function TXMLLocality.Get_Name: UnicodeString;
begin
  Result := AttributeNodes['Name'].Text;
end;

procedure TXMLLocality.Set_Name(Value: UnicodeString);
begin
  SetAttribute('Name', Value);
end;

function TXMLLocality.Get_Type_: UnicodeString;
begin
  Result := AttributeNodes['Type'].Text;
end;

procedure TXMLLocality.Set_Type_(Value: UnicodeString);
begin
  SetAttribute('Type', Value);
end;

{ TXMLStreet }

function TXMLStreet.Get_Name: UnicodeString;
begin
  Result := AttributeNodes['Name'].Text;
end;

procedure TXMLStreet.Set_Name(Value: UnicodeString);
begin
  SetAttribute('Name', Value);
end;

function TXMLStreet.Get_Type_: UnicodeString;
begin
  Result := AttributeNodes['Type'].Text;
end;

procedure TXMLStreet.Set_Type_(Value: UnicodeString);
begin
  SetAttribute('Type', Value);
end;

{ TXMLLevel1 }

function TXMLLevel1.Get_Type_: UnicodeString;
begin
  Result := AttributeNodes['Type'].Text;
end;

procedure TXMLLevel1.Set_Type_(Value: UnicodeString);
begin
  SetAttribute('Type', Value);
end;

function TXMLLevel1.Get_Value: UnicodeString;
begin
  Result := AttributeNodes['Value'].Text;
end;

procedure TXMLLevel1.Set_Value(Value: UnicodeString);
begin
  SetAttribute('Value', Value);
end;

{ TXMLLevel2 }

function TXMLLevel2.Get_Type_: UnicodeString;
begin
  Result := AttributeNodes['Type'].Text;
end;

procedure TXMLLevel2.Set_Type_(Value: UnicodeString);
begin
  SetAttribute('Type', Value);
end;

function TXMLLevel2.Get_Value: UnicodeString;
begin
  Result := AttributeNodes['Value'].Text;
end;

procedure TXMLLevel2.Set_Value(Value: UnicodeString);
begin
  SetAttribute('Value', Value);
end;

{ TXMLLevel3 }

function TXMLLevel3.Get_Type_: UnicodeString;
begin
  Result := AttributeNodes['Type'].Text;
end;

procedure TXMLLevel3.Set_Type_(Value: UnicodeString);
begin
  SetAttribute('Type', Value);
end;

function TXMLLevel3.Get_Value: UnicodeString;
begin
  Result := AttributeNodes['Value'].Text;
end;

procedure TXMLLevel3.Set_Value(Value: UnicodeString);
begin
  SetAttribute('Value', Value);
end;

{ TXMLApartment }

function TXMLApartment.Get_Type_: UnicodeString;
begin
  Result := AttributeNodes['Type'].Text;
end;

procedure TXMLApartment.Set_Type_(Value: UnicodeString);
begin
  SetAttribute('Type', Value);
end;

function TXMLApartment.Get_Value: UnicodeString;
begin
  Result := AttributeNodes['Value'].Text;
end;

procedure TXMLApartment.Set_Value(Value: UnicodeString);
begin
  SetAttribute('Value', Value);
end;

{ TXMLTNewParcel_Location }

procedure TXMLTNewParcel_Location.AfterConstruction;
begin
  RegisterChildNode('Document', TXMLTDocument);
  inherited;
end;

function TXMLTNewParcel_Location.Get_Document: IXMLTDocument;
begin
  Result := ChildNodes['Document'] as IXMLTDocument;
end;

{ TXMLTCategory }

procedure TXMLTCategory.AfterConstruction;
begin
  RegisterChildNode('DocCategory', TXMLTDocument);
  inherited;
end;

function TXMLTCategory.Get_Category: UnicodeString;
begin
  Result := AttributeNodes['Category'].Text;
end;

procedure TXMLTCategory.Set_Category(Value: UnicodeString);
begin
  SetAttribute('Category', Value);
end;

function TXMLTCategory.Get_DocCategory: IXMLTDocument;
begin
  Result := ChildNodes['DocCategory'] as IXMLTDocument;
end;

{ TXMLTNatural_Object }

function TXMLTNatural_Object.Get_Name: UnicodeString;
begin
  Result := ChildNodes['Name'].Text;
end;

procedure TXMLTNatural_Object.Set_Name(Value: UnicodeString);
begin
  ChildNodes['Name'].NodeValue := Value;
end;

function TXMLTNatural_Object.Get_ForestUse: UnicodeString;
begin
  Result := ChildNodes['ForestUse'].Text;
end;

procedure TXMLTNatural_Object.Set_ForestUse(Value: UnicodeString);
begin
  ChildNodes['ForestUse'].NodeValue := Value;
end;

function TXMLTNatural_Object.Get_Type_ProtectiveForest: UnicodeString;
begin
  Result := ChildNodes['Type_ProtectiveForest'].Text;
end;

procedure TXMLTNatural_Object.Set_Type_ProtectiveForest(Value: UnicodeString);
begin
  ChildNodes['Type_ProtectiveForest'].NodeValue := Value;
end;

{ TXMLTUtilization }

procedure TXMLTUtilization.AfterConstruction;
begin
  RegisterChildNode('DocUtilization', TXMLTDocument);
  inherited;
end;

function TXMLTUtilization.Get_Utilization: UnicodeString;
begin
  Result := AttributeNodes['Utilization'].Text;
end;

procedure TXMLTUtilization.Set_Utilization(Value: UnicodeString);
begin
  SetAttribute('Utilization', Value);
end;

function TXMLTUtilization.Get_ByDoc: UnicodeString;
begin
  Result := AttributeNodes['ByDoc'].Text;
end;

procedure TXMLTUtilization.Set_ByDoc(Value: UnicodeString);
begin
  SetAttribute('ByDoc', Value);
end;

function TXMLTUtilization.Get_AdditionalName: UnicodeString;
begin
  Result := AttributeNodes['AdditionalName'].Text;
end;

procedure TXMLTUtilization.Set_AdditionalName(Value: UnicodeString);
begin
  SetAttribute('AdditionalName', Value);
end;

function TXMLTUtilization.Get_DocUtilization: IXMLTDocument;
begin
  Result := ChildNodes['DocUtilization'] as IXMLTDocument;
end;

{ TXMLTNewParcel_SubParcels }

procedure TXMLTNewParcel_SubParcels.AfterConstruction;
begin
  RegisterChildNode('FormSubParcel', TXMLTNewParcel_SubParcels_FormSubParcel);
  ItemTag := 'FormSubParcel';
  ItemInterface := IXMLTNewParcel_SubParcels_FormSubParcel;
  inherited;
end;

function TXMLTNewParcel_SubParcels.Get_FormSubParcel(Index: Integer): IXMLTNewParcel_SubParcels_FormSubParcel;
begin
  Result := List[Index] as IXMLTNewParcel_SubParcels_FormSubParcel;
end;

function TXMLTNewParcel_SubParcels.Add: IXMLTNewParcel_SubParcels_FormSubParcel;
begin
  Result := AddItem(-1) as IXMLTNewParcel_SubParcels_FormSubParcel;
end;

function TXMLTNewParcel_SubParcels.Insert(const Index: Integer): IXMLTNewParcel_SubParcels_FormSubParcel;
begin
  Result := AddItem(Index) as IXMLTNewParcel_SubParcels_FormSubParcel;
end;

{ TXMLTSubParcel }

procedure TXMLTSubParcel.AfterConstruction;
begin
  RegisterChildNode('Area', TXMLTArea);
  RegisterChildNode('Encumbrance', TXMLTEncumbrance);
  RegisterChildNode('Entity_Spatial', TXMLEntity_Spatial);
  RegisterChildNode('Contours', TXMLTSubParcel_Contours);
  inherited;
end;

function TXMLTSubParcel.Get_SubParcel_Realty: Boolean;
begin
  Result := AttributeNodes['SubParcel_Realty'].NodeValue;
end;

procedure TXMLTSubParcel.Set_SubParcel_Realty(Value: Boolean);
begin
  SetAttribute('SubParcel_Realty', Value);
end;

function TXMLTSubParcel.Get_Area: IXMLTArea;
begin
  Result := ChildNodes['Area'] as IXMLTArea;
end;

function TXMLTSubParcel.Get_Encumbrance: IXMLTEncumbrance;
begin
  Result := ChildNodes['Encumbrance'] as IXMLTEncumbrance;
end;

function TXMLTSubParcel.Get_Entity_Spatial: IXMLEntity_Spatial;
begin
  Result := ChildNodes['Entity_Spatial'] as IXMLEntity_Spatial;
end;

function TXMLTSubParcel.Get_Contours: IXMLTSubParcel_Contours;
begin
  Result := ChildNodes['Contours'] as IXMLTSubParcel_Contours;
end;

{ TXMLTArea }

function TXMLTArea.Get_Area: UnicodeString;
begin
  Result := ChildNodes['Area'].Text;
end;

procedure TXMLTArea.Set_Area(Value: UnicodeString);
begin
  ChildNodes['Area'].Text := Value;
end;

function TXMLTArea.Get_Unit_: UnicodeString;
begin
  Result := ChildNodes['Unit'].Text;
end;

procedure TXMLTArea.Set_Unit_(Value: UnicodeString);
begin
  ChildNodes['Unit'].NodeValue := Value;
end;

function TXMLTArea.Get_Innccuracy: LongWord;
begin
  Result := ChildNodes['Innccuracy'].NodeValue;
end;

procedure TXMLTArea.Set_Innccuracy(Value: LongWord);
begin
  ChildNodes['Innccuracy'].NodeValue := Value;
end;

{ TXMLTEncumbrance }

procedure TXMLTEncumbrance.AfterConstruction;
begin
  RegisterChildNode('Documents', TXMLTEncumbrance_Documents);
  inherited;
end;

function TXMLTEncumbrance.Get_Name: UnicodeString;
begin
  Result := ChildNodes['Name'].Text;
end;

procedure TXMLTEncumbrance.Set_Name(Value: UnicodeString);
begin
  ChildNodes['Name'].NodeValue := Value;
end;

function TXMLTEncumbrance.Get_Type_: UnicodeString;
begin
  Result := ChildNodes['Type'].Text;
end;

procedure TXMLTEncumbrance.Set_Type_(Value: UnicodeString);
begin
  ChildNodes['Type'].NodeValue := Value;
end;

function TXMLTEncumbrance.Get_CadastralNumber_Restriction: UnicodeString;
begin
  Result := ChildNodes['CadastralNumber_Restriction'].Text;
end;

procedure TXMLTEncumbrance.Set_CadastralNumber_Restriction(Value: UnicodeString);
begin
  ChildNodes['CadastralNumber_Restriction'].NodeValue := Value;
end;

function TXMLTEncumbrance.Get_Documents: IXMLTEncumbrance_Documents;
begin
  Result := ChildNodes['Documents'] as IXMLTEncumbrance_Documents;
end;

{ TXMLTEncumbrance_Documents }

procedure TXMLTEncumbrance_Documents.AfterConstruction;
begin
  RegisterChildNode('Document', TXMLTDocument);
  ItemTag := 'Document';
  ItemInterface := IXMLTDocument;
  inherited;
end;

function TXMLTEncumbrance_Documents.Get_Document(Index: Integer): IXMLTDocument;
begin
  Result := List[Index] as IXMLTDocument;
end;

function TXMLTEncumbrance_Documents.Add: IXMLTDocument;
begin
  Result := AddItem(-1) as IXMLTDocument;
end;

function TXMLTEncumbrance_Documents.Insert(const Index: Integer): IXMLTDocument;
begin
  Result := AddItem(Index) as IXMLTDocument;
end;

{ TXMLEntity_Spatial }

procedure TXMLEntity_Spatial.AfterConstruction;
begin
  RegisterChildNode('Spatial_Element', TXMLTSPATIAL_ELEMENT_OLD_NEW);
  RegisterChildNode('Borders', TXMLEntity_Spatial_Borders);
  FSpatial_Element := CreateCollection(TXMLTSPATIAL_ELEMENT_OLD_NEWList, IXMLTSPATIAL_ELEMENT, 'Spatial_Element') as IXMLTSPATIAL_ELEMENTList;
  inherited;
end;

function TXMLEntity_Spatial.Get_Ent_Sys: UnicodeString;
begin
  Result := AttributeNodes['Ent_Sys'].Text;
end;

procedure TXMLEntity_Spatial.Set_Ent_Sys(Value: UnicodeString);
begin
  SetAttribute('Ent_Sys', Value);
end;

function TXMLEntity_Spatial.Get_Spatial_Element: IXMLTSPATIAL_ELEMENTList;
begin
  Result := FSpatial_Element;
end;

function TXMLEntity_Spatial.Get_Borders: IXMLEntity_Spatial_Borders;
begin
  Result := ChildNodes['Borders'] as IXMLEntity_Spatial_Borders;
end;

{ TXMLTSPATIAL_ELEMENT_OLD_NEW }

procedure TXMLTSPATIAL_ELEMENT_OLD_NEW.AfterConstruction;
begin
  RegisterChildNode('Spelement_Unit', TXMLTSPELEMENT_UNIT_OLD_NEW);
  ItemTag := 'Spelement_Unit';
  ItemInterface := IXMLTSPELEMENT_UNIT;
  inherited;
end;

function TXMLTSPATIAL_ELEMENT_OLD_NEW.Get_Spelement_Unit(Index: Integer): IXMLTSPELEMENT_UNIT;
begin
  Result := List[Index] as IXMLTSPELEMENT_UNIT;
end;

function TXMLTSPATIAL_ELEMENT_OLD_NEW.Add: IXMLTSPELEMENT_UNIT;
begin
  Result := AddItem(-1) as IXMLTSPELEMENT_UNIT;
end;

function TXMLTSPATIAL_ELEMENT_OLD_NEW.Insert(const Index: Integer): IXMLTSPELEMENT_UNIT;
begin
  Result := AddItem(Index) as IXMLTSPELEMENT_UNIT;
end;

{ TXMLTSPATIAL_ELEMENT_OLD_NEWList }

function TXMLTSPATIAL_ELEMENT_OLD_NEWList.Add: IXMLTSPATIAL_ELEMENT;
begin
  Result := AddItem(-1) as IXMLTSPATIAL_ELEMENT;
end;

function TXMLTSPATIAL_ELEMENT_OLD_NEWList.Insert(const Index: Integer): IXMLTSPATIAL_ELEMENT;
begin
  Result := AddItem(Index) as IXMLTSPATIAL_ELEMENT;
end;

function TXMLTSPATIAL_ELEMENT_OLD_NEWList.Get_Item(Index: Integer): IXMLTSPATIAL_ELEMENT;
begin
  Result := List[Index] as IXMLTSPATIAL_ELEMENT;
end;

{ TXMLTSPELEMENT_UNIT_OLD_NEW }

procedure TXMLTSPELEMENT_UNIT_OLD_NEW.AfterConstruction;
begin
  RegisterChildNode('NewOrdinate', TXMLTOrdinate);
  RegisterChildNode('OldOrdinate', TXMLTOrdinate);
  inherited;
end;

function TXMLTSPELEMENT_UNIT_OLD_NEW.Get_Type_Unit: UnicodeString;
begin
  Result := AttributeNodes['Type_Unit'].Text;
end;

procedure TXMLTSPELEMENT_UNIT_OLD_NEW.Set_Type_Unit(Value: UnicodeString);
begin
  SetAttribute('Type_Unit', Value);
end;

function TXMLTSPELEMENT_UNIT_OLD_NEW.Get_NewOrdinate: IXMLTOrdinate;
begin
  Result := ChildNodes['NewOrdinate'] as IXMLTOrdinate;
end;

function TXMLTSPELEMENT_UNIT_OLD_NEW.Get_OldOrdinate: IXMLTOrdinate;
begin
  Result := ChildNodes['OldOrdinate'] as IXMLTOrdinate;
end;

{ TXMLTOrdinate }

function TXMLTOrdinate.Get_X: UnicodeString;
begin
  Result := AttributeNodes[WideString('X')].Text;
end;

procedure TXMLTOrdinate.Set_X(Value: UnicodeString);
begin
  SetAttribute(WideString('X'), Value);
end;

function TXMLTOrdinate.Get_Y: UnicodeString;
begin
  Result := AttributeNodes[WideString('Y')].Text;
end;

procedure TXMLTOrdinate.Set_Y(Value: UnicodeString);
begin
  SetAttribute(WideString('Y'), Value);
end;

function TXMLTOrdinate.Get_Num_Geopoint: LongWord;
begin
  Result := AttributeNodes['Num_Geopoint'].NodeValue;
end;

procedure TXMLTOrdinate.Set_Num_Geopoint(Value: LongWord);
begin
  SetAttribute('Num_Geopoint', Value);
end;

function TXMLTOrdinate.Get_Geopoint_Zacrep: UnicodeString;
begin
  Result := AttributeNodes['Geopoint_Zacrep'].Text;
end;

procedure TXMLTOrdinate.Set_Geopoint_Zacrep(Value: UnicodeString);
begin
  SetAttribute('Geopoint_Zacrep', Value);
end;

function TXMLTOrdinate.Get_Delta_Geopoint: UnicodeString;
begin
  Result := AttributeNodes['Delta_Geopoint'].Text;
end;

procedure TXMLTOrdinate.Set_Delta_Geopoint(Value: UnicodeString);
begin
  SetAttribute('Delta_Geopoint', Value);
end;

function TXMLTOrdinate.Get_Point_Pref: UnicodeString;
begin
  Result := AttributeNodes['Point_Pref'].Text;
end;

procedure TXMLTOrdinate.Set_Point_Pref(Value: UnicodeString);
begin
  SetAttribute('Point_Pref', Value);
end;

{ TXMLEntity_Spatial_Borders }

procedure TXMLEntity_Spatial_Borders.AfterConstruction;
begin
  RegisterChildNode('Border', TXMLEntity_Spatial_Borders_Border);
  ItemTag := 'Border';
  ItemInterface := IXMLEntity_Spatial_Borders_Border;
  inherited;
end;

function TXMLEntity_Spatial_Borders.Get_Border(Index: Integer): IXMLEntity_Spatial_Borders_Border;
begin
  Result := List[Index] as IXMLEntity_Spatial_Borders_Border;
end;

function TXMLEntity_Spatial_Borders.Add: IXMLEntity_Spatial_Borders_Border;
begin
  Result := AddItem(-1) as IXMLEntity_Spatial_Borders_Border;
end;

function TXMLEntity_Spatial_Borders.Insert(const Index: Integer): IXMLEntity_Spatial_Borders_Border;
begin
  Result := AddItem(Index) as IXMLEntity_Spatial_Borders_Border;
end;

{ TXMLTBorder }

procedure TXMLTBorder.AfterConstruction;
begin
  RegisterChildNode('Edge', TXMLTBorder_Edge);
  inherited;
end;

function TXMLTBorder.Get_Spatial: Integer;
begin
  Result := AttributeNodes['Spatial'].NodeValue;
end;

procedure TXMLTBorder.Set_Spatial(Value: Integer);
begin
  SetAttribute('Spatial', Value);
end;

function TXMLTBorder.Get_Point1: Integer;
begin
  Result := AttributeNodes['Point1'].NodeValue;
end;

procedure TXMLTBorder.Set_Point1(Value: Integer);
begin
  SetAttribute('Point1', Value);
end;

function TXMLTBorder.Get_Point2: Integer;
begin
  Result := AttributeNodes['Point2'].NodeValue;
end;

procedure TXMLTBorder.Set_Point2(Value: Integer);
begin
  SetAttribute('Point2', Value);
end;

function TXMLTBorder.Get_ByDef: Boolean;
begin
  Result := AttributeNodes['ByDef'].NodeValue;
end;

procedure TXMLTBorder.Set_ByDef(Value: Boolean);
begin
  SetAttribute('ByDef', Value);
end;

function TXMLTBorder.Get_Edge: IXMLTBorder_Edge;
begin
  Result := ChildNodes['Edge'] as IXMLTBorder_Edge;
end;

{ TXMLTBorder_Edge }

function TXMLTBorder_Edge.Get_Length: UnicodeString;
begin
  Result := ChildNodes['Length'].Text;
end;

procedure TXMLTBorder_Edge.Set_Length(Value: UnicodeString);
begin
  ChildNodes['Length'].NodeValue := Value;
end;

function TXMLTBorder_Edge.Get_Definition: UnicodeString;
begin
  Result := ChildNodes['Definition'].Text;
end;

procedure TXMLTBorder_Edge.Set_Definition(Value: UnicodeString);
begin
  ChildNodes['Definition'].NodeValue := Value;
end;

{ TXMLEntity_Spatial_Borders_Border }

{ TXMLTSubParcel_Contours }

procedure TXMLTSubParcel_Contours.AfterConstruction;
begin
  RegisterChildNode('Contour', TXMLTSubParcel_Contours_Contour);
  ItemTag := 'Contour';
  ItemInterface := IXMLTSubParcel_Contours_Contour;
  inherited;
end;

function TXMLTSubParcel_Contours.Get_Contour(Index: Integer): IXMLTSubParcel_Contours_Contour;
begin
  Result := List[Index] as IXMLTSubParcel_Contours_Contour;
end;

function TXMLTSubParcel_Contours.Add: IXMLTSubParcel_Contours_Contour;
begin
  Result := AddItem(-1) as IXMLTSubParcel_Contours_Contour;
end;

function TXMLTSubParcel_Contours.Insert(const Index: Integer): IXMLTSubParcel_Contours_Contour;
begin
  Result := AddItem(Index) as IXMLTSubParcel_Contours_Contour;
end;

{ TXMLTSubParcel_Contours_Contour }

procedure TXMLTSubParcel_Contours_Contour.AfterConstruction;
begin
  RegisterChildNode('Area', TXMLTArea);//TODO:L:
  RegisterChildNode('Entity_Spatial', TXMLEntity_Spatial); //TODO:L:
  inherited;
end;

function TXMLTSubParcel_Contours_Contour.Get_Number: UnicodeString;
begin
  Result := AttributeNodes['Number'].Text;
end;

procedure TXMLTSubParcel_Contours_Contour.Set_Number(Value: UnicodeString);
begin
  SetAttribute('Number', Value);
end;

function TXMLTSubParcel_Contours_Contour.Get_Area: IXMLTArea; //TODO:L:
begin
  Result := ChildNodes['Area'] as IXMLTArea;
end;

function TXMLTSubParcel_Contours_Contour.Get_Entity_Spatial: IXMLEntity_Spatial; //TODO:L:
begin
  Result := ChildNodes['Entity_Spatial'] as IXMLEntity_Spatial;
end;

{ TXMLTArea_Contour }

function TXMLTArea_Contour.Get_Area: UnicodeString;
begin
  Result := ChildNodes['Area'].Text;
end;

procedure TXMLTArea_Contour.Set_Area(Value: UnicodeString);
begin
  ChildNodes['Area'].NodeValue := Value;
end;

function TXMLTArea_Contour.Get_Unit_: UnicodeString;
begin
  Result := ChildNodes['Unit'].Text;
end;

procedure TXMLTArea_Contour.Set_Unit_(Value: UnicodeString);
begin
  ChildNodes['Unit'].NodeValue := Value;
end;

function TXMLTArea_Contour.Get_Innccuracy: LongWord;
begin
  Result := ChildNodes['Innccuracy'].NodeValue;
end;

procedure TXMLTArea_Contour.Set_Innccuracy(Value: LongWord);
begin
  ChildNodes['Innccuracy'].NodeValue := Value;
end;

{ TXMLTSubParcel_Contours_Contour_Entity_Spatial }

procedure TXMLTSubParcel_Contours_Contour_Entity_Spatial.AfterConstruction;
begin
  RegisterChildNode('Spatial_Element', TXMLTSPATIAL_ELEMENT_OLD_NEW);
  ItemTag := 'Spatial_Element';
  ItemInterface := IXMLTSPATIAL_ELEMENT;
  inherited;
end;

function TXMLTSubParcel_Contours_Contour_Entity_Spatial.Get_Ent_Sys: UnicodeString;
begin
  Result := AttributeNodes['Ent_Sys'].Text;
end;

procedure TXMLTSubParcel_Contours_Contour_Entity_Spatial.Set_Ent_Sys(Value: UnicodeString);
begin
  SetAttribute('Ent_Sys', Value);
end;

function TXMLTSubParcel_Contours_Contour_Entity_Spatial.Get_Spatial_Element(Index: Integer): IXMLTSPATIAL_ELEMENT;
begin
  Result := List[Index] as IXMLTSPATIAL_ELEMENT;
end;

function TXMLTSubParcel_Contours_Contour_Entity_Spatial.Add: IXMLTSPATIAL_ELEMENT;
begin
  Result := AddItem(-1) as IXMLTSPATIAL_ELEMENT;
end;

function TXMLTSubParcel_Contours_Contour_Entity_Spatial.Insert(const Index: Integer): IXMLTSPATIAL_ELEMENT;
begin
  Result := AddItem(Index) as IXMLTSPATIAL_ELEMENT;
end;

{ TXMLTNewParcel_SubParcels_FormSubParcel }

function TXMLTNewParcel_SubParcels_FormSubParcel.Get_Definition: UnicodeString;
begin
  Result := AttributeNodes['Definition'].Text;
end;

procedure TXMLTNewParcel_SubParcels_FormSubParcel.Set_Definition(Value: UnicodeString);
begin
  SetAttribute('Definition', Value);
end;

{$ifndef disableIXMLTNewParcel_Contours}
{ TXMLTNewParcel_Contours }

procedure TXMLTNewParcel_Contours.AfterConstruction;
begin
  RegisterChildNode('NewContour', TXMLTNewParcel_Contours_NewContour);
  ItemTag := 'NewContour';
  ItemInterface := IXMLTNewParcel_Contours_NewContour;
  inherited;
end;

function TXMLTNewParcel_Contours.Get_NewContour(Index: Integer): IXMLTNewParcel_Contours_NewContour;
begin
  Result := List[Index] as IXMLTNewParcel_Contours_NewContour;
end;

function TXMLTNewParcel_Contours.Add: IXMLTNewParcel_Contours_NewContour;
begin
  Result := AddItem(-1) as IXMLTNewParcel_Contours_NewContour;
end;

function TXMLTNewParcel_Contours.Insert(const Index: Integer): IXMLTNewParcel_Contours_NewContour;
begin
  Result := AddItem(Index) as IXMLTNewParcel_Contours_NewContour;
end;
{$endif}

{ TXMLTContour }

procedure TXMLTContour.AfterConstruction;
begin
  RegisterChildNode('Area', TXMLTArea); //TODO:L:
  RegisterChildNode('Entity_Spatial', TXMLEntity_Spatial);
  inherited;
end;

function TXMLTContour.Get_Area: IXMLTArea;  //TODO:L:
begin
  Result := ChildNodes['Area'] as IXMLTArea;
end;

function TXMLTContour.Get_Entity_Spatial: IXMLEntity_Spatial;
begin
  Result := ChildNodes['Entity_Spatial'] as IXMLEntity_Spatial;
end;

{ TXMLTNewParcel_Contours_NewContour }

procedure TXMLTNewParcel_Contours_NewContour.AfterConstruction;
begin
  RegisterChildNode('Providing_Pass_CadastralNumbers', TXMLTProviding_Pass_CadastralNumbers);
  inherited;
end;

function TXMLTNewParcel_Contours_NewContour.Get_Definition: UnicodeString;
begin
  Result := AttributeNodes['Definition'].Text;
end;

procedure TXMLTNewParcel_Contours_NewContour.Set_Definition(Value: UnicodeString);
begin
  SetAttribute('Definition', Value);
end;

function TXMLTNewParcel_Contours_NewContour.Get_Providing_Pass_CadastralNumbers: IXMLTProviding_Pass_CadastralNumbers;
begin
  Result := ChildNodes['Providing_Pass_CadastralNumbers'] as IXMLTProviding_Pass_CadastralNumbers;
end;

{ TXMLTArea_without_Innccuracy }

function TXMLTArea_without_Innccuracy.Get_Area: LongWord;
begin
  Result := ChildNodes['Area'].NodeValue;
end;

procedure TXMLTArea_without_Innccuracy.Set_Area(Value: LongWord);
begin
  ChildNodes['Area'].NodeValue := Value;
end;

function TXMLTArea_without_Innccuracy.Get_Unit_: UnicodeString;
begin
  Result := ChildNodes['Unit'].Text;
end;

procedure TXMLTArea_without_Innccuracy.Set_Unit_(Value: UnicodeString);
begin
  ChildNodes['Unit'].NodeValue := Value;
end;

{ TXMLTChangeParcel }

procedure TXMLTChangeParcel.AfterConstruction;
begin
  RegisterChildNode('Providing_Pass_CadastralNumbers', TXMLTProviding_Pass_CadastralNumbers);
  RegisterChildNode('Inner_CadastralNumbers', TXMLTChangeParcel_Inner_CadastralNumbers);
  {$IFNDEF disableIXMLTChangeParcel_SubParcels}
  RegisterChildNode('SubParcels', TXMLTChangeParcel_SubParcels);
  {$ELSE}
  RegisterChildNode('SubParcels', TXMLTExistParcel_SubParcels);
  {$ENDIF}
  RegisterChildNode('DeleteEntryParcels', TXMLTChangeParcel_DeleteEntryParcels);
  inherited;
end;

function TXMLTChangeParcel.Get_CadastralNumber: UnicodeString;
begin
  Result := AttributeNodes['CadastralNumber'].Text;
end;

procedure TXMLTChangeParcel.Set_CadastralNumber(Value: UnicodeString);
begin
  SetAttribute('CadastralNumber', Value);
end;

function TXMLTChangeParcel.Get_CadastralBlock: UnicodeString;
begin
  Result := ChildNodes['CadastralBlock'].Text;
end;

procedure TXMLTChangeParcel.Set_CadastralBlock(Value: UnicodeString);
begin
  ChildNodes['CadastralBlock'].NodeValue := Value;
end;

function TXMLTChangeParcel.Get_Providing_Pass_CadastralNumbers: IXMLTProviding_Pass_CadastralNumbers;
begin
  Result := ChildNodes['Providing_Pass_CadastralNumbers'] as IXMLTProviding_Pass_CadastralNumbers;
end;

function TXMLTChangeParcel.Get_Inner_CadastralNumbers: IXMLTChangeParcel_Inner_CadastralNumbers;
begin
  Result := ChildNodes['Inner_CadastralNumbers'] as IXMLTChangeParcel_Inner_CadastralNumbers;
end;

{$IFNDEF disableIXMLTChangeParcel_SubParcels}
function TXMLTChangeParcel.Get_SubParcels: IXMLTChangeParcel_SubParcels;
begin
  Result := ChildNodes['SubParcels'] as IXMLTChangeParcel_SubParcels;
end;
{$ELSE}
function TXMLTChangeParcel.Get_SubParcels: IXMLTExistParcel_SubParcels;
begin
  Result := ChildNodes['SubParcels'] as IXMLTExistParcel_SubParcels;
end;
{$ENDIF}

function TXMLTChangeParcel.Get_DeleteEntryParcels: IXMLTChangeParcel_DeleteEntryParcels;
begin
  Result := ChildNodes['DeleteEntryParcels'] as IXMLTChangeParcel_DeleteEntryParcels;
end;

function TXMLTChangeParcel.Get_Note: UnicodeString;
begin
  Result := ChildNodes['Note'].Text;
end;

procedure TXMLTChangeParcel.Set_Note(Value: UnicodeString);
begin
  ChildNodes['Note'].NodeValue := Value;
end;

{ TXMLTChangeParcelList }

function TXMLTChangeParcelList.Add: IXMLTChangeParcel;
begin
  Result := AddItem(-1) as IXMLTChangeParcel;
end;

function TXMLTChangeParcelList.Insert(const Index: Integer): IXMLTChangeParcel;
begin
  Result := AddItem(Index) as IXMLTChangeParcel;
end;

function TXMLTChangeParcelList.Get_Item(Index: Integer): IXMLTChangeParcel;
begin
  Result := List[Index] as IXMLTChangeParcel;
end;

{ TXMLTChangeParcel_Inner_CadastralNumbers }

procedure TXMLTChangeParcel_Inner_CadastralNumbers.AfterConstruction;
begin
  FCadastralNumber := CreateCollection(TXMLTChangeParcel_Inner_CadastralNumbers_CadastralNumberList, IXMLNode, 'CadastralNumber') as IXMLTChangeParcel_Inner_CadastralNumbers_CadastralNumberList;
  FNumber := CreateCollection(TXMLString_List, IXMLNode, 'Number') as IXMLString_List;
  inherited;
end;

function TXMLTChangeParcel_Inner_CadastralNumbers.Get_CadastralNumber: IXMLTChangeParcel_Inner_CadastralNumbers_CadastralNumberList;
begin
  Result := FCadastralNumber;
end;

function TXMLTChangeParcel_Inner_CadastralNumbers.Get_Number: IXMLString_List;
begin
  Result := FNumber;
end;

{$IFNDEF  disableIXMLTChangeParcel_SubParcels}

{ TXMLTChangeParcel_SubParcels }

procedure TXMLTChangeParcel_SubParcels.AfterConstruction;
begin
  RegisterChildNode('FormSubParcel', TXMLTChangeParcel_SubParcels_FormSubParcel);
  RegisterChildNode('ExistSubParcel', TXMLTChangeParcel_SubParcels_ExistSubParcel);
  RegisterChildNode('InvariableSubParcel', TXMLTChangeParcel_SubParcels_InvariableSubParcel);
  FFormSubParcel := CreateCollection(TXMLTChangeParcel_SubParcels_FormSubParcelList, IXMLTChangeParcel_SubParcels_FormSubParcel, 'FormSubParcel') as IXMLTChangeParcel_SubParcels_FormSubParcelList;
  FExistSubParcel := CreateCollection(TXMLTChangeParcel_SubParcels_ExistSubParcelList, IXMLTChangeParcel_SubParcels_ExistSubParcel, 'ExistSubParcel') as IXMLTChangeParcel_SubParcels_ExistSubParcelList;
  FInvariableSubParcel := CreateCollection(TXMLTChangeParcel_SubParcels_InvariableSubParcelList, IXMLTChangeParcel_SubParcels_InvariableSubParcel, 'InvariableSubParcel') as IXMLTChangeParcel_SubParcels_InvariableSubParcelList;
  inherited;
end;

function TXMLTChangeParcel_SubParcels.Get_FormSubParcel: IXMLTChangeParcel_SubParcels_FormSubParcelList;
begin
  Result := FFormSubParcel;
end;

function TXMLTChangeParcel_SubParcels.Get_ExistSubParcel: IXMLTChangeParcel_SubParcels_ExistSubParcelList;
begin
  Result := FExistSubParcel;
end;

function TXMLTChangeParcel_SubParcels.Get_InvariableSubParcel: IXMLTChangeParcel_SubParcels_InvariableSubParcelList;
begin
  Result := FInvariableSubParcel;
end;
{$ENDIF}

{ TXMLTChangeParcel_SubParcels_FormSubParcel }

function TXMLTChangeParcel_SubParcels_FormSubParcel.Get_Definition: UnicodeString;
begin
  Result := AttributeNodes['Definition'].Text;
end;

procedure TXMLTChangeParcel_SubParcels_FormSubParcel.Set_Definition(Value: UnicodeString);
begin
  SetAttribute('Definition', Value);
end;

{ TXMLTChangeParcel_SubParcels_FormSubParcelList }

function TXMLTChangeParcel_SubParcels_FormSubParcelList.Add: IXMLTChangeParcel_SubParcels_FormSubParcel;
begin
  Result := AddItem(-1) as IXMLTChangeParcel_SubParcels_FormSubParcel;
end;

function TXMLTChangeParcel_SubParcels_FormSubParcelList.Insert(const Index: Integer): IXMLTChangeParcel_SubParcels_FormSubParcel;
begin
  Result := AddItem(Index) as IXMLTChangeParcel_SubParcels_FormSubParcel;
end;

function TXMLTChangeParcel_SubParcels_FormSubParcelList.Get_Item(Index: Integer): IXMLTChangeParcel_SubParcels_FormSubParcel;
begin
  Result := List[Index] as IXMLTChangeParcel_SubParcels_FormSubParcel;
end;

{ TXMLTChangeParcel_SubParcels_ExistSubParcel }

function TXMLTChangeParcel_SubParcels_ExistSubParcel.Get_Number_Record: LongWord;
begin
  Result := AttributeNodes['Number_Record'].NodeValue;
end;

procedure TXMLTChangeParcel_SubParcels_ExistSubParcel.Set_Number_Record(Value: LongWord);
begin
  SetAttribute('Number_Record', Value);
end;

function TXMLTChangeParcel_SubParcels_ExistSubParcel.Get_CadastralNumber_EntryParcel: UnicodeString;
begin
  Result := AttributeNodes['CadastralNumber_EntryParcel'].Text;
end;

procedure TXMLTChangeParcel_SubParcels_ExistSubParcel.Set_CadastralNumber_EntryParcel(Value: UnicodeString);
begin
  SetAttribute('CadastralNumber_EntryParcel', Value);
end;

{ TXMLTChangeParcel_SubParcels_ExistSubParcelList }

function TXMLTChangeParcel_SubParcels_ExistSubParcelList.Add: IXMLTChangeParcel_SubParcels_ExistSubParcel;
begin
  Result := AddItem(-1) as IXMLTChangeParcel_SubParcels_ExistSubParcel;
end;

function TXMLTChangeParcel_SubParcels_ExistSubParcelList.Insert(const Index: Integer): IXMLTChangeParcel_SubParcels_ExistSubParcel;
begin
  Result := AddItem(Index) as IXMLTChangeParcel_SubParcels_ExistSubParcel;
end;

function TXMLTChangeParcel_SubParcels_ExistSubParcelList.Get_Item(Index: Integer): IXMLTChangeParcel_SubParcels_ExistSubParcel;
begin
  Result := List[Index] as IXMLTChangeParcel_SubParcels_ExistSubParcel;
end;

{ TXMLTChangeParcel_SubParcels_InvariableSubParcel }

procedure TXMLTChangeParcel_SubParcels_InvariableSubParcel.AfterConstruction;
begin
  RegisterChildNode('Area', TXMLTChangeParcel_SubParcels_InvariableSubParcel_Area);
  RegisterChildNode('Encumbrance', TXMLTEncumbrance);
  RegisterChildNode('Contours', TXMLTChangeParcel_SubParcels_InvariableSubParcel_Contours);
  inherited;
end;

function TXMLTChangeParcel_SubParcels_InvariableSubParcel.Get_Number_Record: LongWord;
begin
  Result := AttributeNodes['Number_Record'].NodeValue;
end;

procedure TXMLTChangeParcel_SubParcels_InvariableSubParcel.Set_Number_Record(Value: LongWord);
begin
  SetAttribute('Number_Record', Value);
end;

function TXMLTChangeParcel_SubParcels_InvariableSubParcel.Get_SubParcel_Realty: Boolean;
begin
  Result := AttributeNodes['SubParcel_Realty'].NodeValue;
end;

procedure TXMLTChangeParcel_SubParcels_InvariableSubParcel.Set_SubParcel_Realty(Value: Boolean);
begin
  SetAttribute('SubParcel_Realty', Value);
end;

function TXMLTChangeParcel_SubParcels_InvariableSubParcel.Get_Area: IXMLTChangeParcel_SubParcels_InvariableSubParcel_Area;
begin
  Result := ChildNodes['Area'] as IXMLTChangeParcel_SubParcels_InvariableSubParcel_Area;
end;

function TXMLTChangeParcel_SubParcels_InvariableSubParcel.Get_Encumbrance: IXMLTEncumbrance;
begin
  Result := ChildNodes['Encumbrance'] as IXMLTEncumbrance;
end;

function TXMLTChangeParcel_SubParcels_InvariableSubParcel.Get_Contours: IXMLTChangeParcel_SubParcels_InvariableSubParcel_Contours;
begin
  Result := ChildNodes['Contours'] as IXMLTChangeParcel_SubParcels_InvariableSubParcel_Contours;
end;

{ TXMLTChangeParcel_SubParcels_InvariableSubParcelList }

function TXMLTChangeParcel_SubParcels_InvariableSubParcelList.Add: IXMLTChangeParcel_SubParcels_InvariableSubParcel;
begin
  Result := AddItem(-1) as IXMLTChangeParcel_SubParcels_InvariableSubParcel;
end;

function TXMLTChangeParcel_SubParcels_InvariableSubParcelList.Insert(const Index: Integer): IXMLTChangeParcel_SubParcels_InvariableSubParcel;
begin
  Result := AddItem(Index) as IXMLTChangeParcel_SubParcels_InvariableSubParcel;
end;

function TXMLTChangeParcel_SubParcels_InvariableSubParcelList.Get_Item(Index: Integer): IXMLTChangeParcel_SubParcels_InvariableSubParcel;
begin
  Result := List[Index] as IXMLTChangeParcel_SubParcels_InvariableSubParcel;
end;

{ TXMLTChangeParcel_SubParcels_InvariableSubParcel_Area }

function TXMLTChangeParcel_SubParcels_InvariableSubParcel_Area.Get_Area: LongWord;
begin
  Result := ChildNodes['Area'].NodeValue;
end;

procedure TXMLTChangeParcel_SubParcels_InvariableSubParcel_Area.Set_Area(Value: LongWord);
begin
  ChildNodes['Area'].NodeValue := Value;
end;

function TXMLTChangeParcel_SubParcels_InvariableSubParcel_Area.Get_Unit_: UnicodeString;
begin
  Result := ChildNodes['Unit'].Text;
end;

procedure TXMLTChangeParcel_SubParcels_InvariableSubParcel_Area.Set_Unit_(Value: UnicodeString);
begin
  ChildNodes['Unit'].NodeValue := Value;
end;

{ TXMLTChangeParcel_SubParcels_InvariableSubParcel_Contours }

procedure TXMLTChangeParcel_SubParcels_InvariableSubParcel_Contours.AfterConstruction;
begin
  RegisterChildNode('Contour', TXMLTChangeParcel_SubParcels_InvariableSubParcel_Contours_Contour);
  ItemTag := 'Contour';
  ItemInterface := IXMLTChangeParcel_SubParcels_InvariableSubParcel_Contours_Contour;
  inherited;
end;

function TXMLTChangeParcel_SubParcels_InvariableSubParcel_Contours.Get_Contour(Index: Integer): IXMLTChangeParcel_SubParcels_InvariableSubParcel_Contours_Contour;
begin
  Result := List[Index] as IXMLTChangeParcel_SubParcels_InvariableSubParcel_Contours_Contour;
end;

function TXMLTChangeParcel_SubParcels_InvariableSubParcel_Contours.Add: IXMLTChangeParcel_SubParcels_InvariableSubParcel_Contours_Contour;
begin
  Result := AddItem(-1) as IXMLTChangeParcel_SubParcels_InvariableSubParcel_Contours_Contour;
end;

function TXMLTChangeParcel_SubParcels_InvariableSubParcel_Contours.Insert(const Index: Integer): IXMLTChangeParcel_SubParcels_InvariableSubParcel_Contours_Contour;
begin
  Result := AddItem(Index) as IXMLTChangeParcel_SubParcels_InvariableSubParcel_Contours_Contour;
end;

{ TXMLTChangeParcel_SubParcels_InvariableSubParcel_Contours_Contour }

procedure TXMLTChangeParcel_SubParcels_InvariableSubParcel_Contours_Contour.AfterConstruction;
begin
  RegisterChildNode('Area', TXMLTArea_Contour);
  inherited;
end;

function TXMLTChangeParcel_SubParcels_InvariableSubParcel_Contours_Contour.Get_Number: UnicodeString;
begin
  Result := AttributeNodes['Number'].Text;
end;

procedure TXMLTChangeParcel_SubParcels_InvariableSubParcel_Contours_Contour.Set_Number(Value: UnicodeString);
begin
  SetAttribute('Number', Value);
end;

function TXMLTChangeParcel_SubParcels_InvariableSubParcel_Contours_Contour.Get_Area: IXMLTArea_Contour;
begin
  Result := ChildNodes['Area'] as IXMLTArea_Contour;
end;

{ TXMLTChangeParcel_DeleteEntryParcels }

procedure TXMLTChangeParcel_DeleteEntryParcels.AfterConstruction;
begin
  RegisterChildNode('DeleteEntryParcel', TXMLTChangeParcel_DeleteEntryParcels_DeleteEntryParcel);
  ItemTag := 'DeleteEntryParcel';
  ItemInterface := IXMLTChangeParcel_DeleteEntryParcels_DeleteEntryParcel;
  inherited;
end;

function TXMLTChangeParcel_DeleteEntryParcels.Get_DeleteEntryParcel(Index: Integer): IXMLTChangeParcel_DeleteEntryParcels_DeleteEntryParcel;
begin
  Result := List[Index] as IXMLTChangeParcel_DeleteEntryParcels_DeleteEntryParcel;
end;

function TXMLTChangeParcel_DeleteEntryParcels.Add: IXMLTChangeParcel_DeleteEntryParcels_DeleteEntryParcel;
begin
  Result := AddItem(-1) as IXMLTChangeParcel_DeleteEntryParcels_DeleteEntryParcel;
end;

function TXMLTChangeParcel_DeleteEntryParcels.Insert(const Index: Integer): IXMLTChangeParcel_DeleteEntryParcels_DeleteEntryParcel;
begin
  Result := AddItem(Index) as IXMLTChangeParcel_DeleteEntryParcels_DeleteEntryParcel;
end;

{ TXMLTChangeParcel_DeleteEntryParcels_DeleteEntryParcel }

function TXMLTChangeParcel_DeleteEntryParcels_DeleteEntryParcel.Get_CadastralNumber: UnicodeString;
begin
  Result := AttributeNodes['CadastralNumber'].Text;
end;

procedure TXMLTChangeParcel_DeleteEntryParcels_DeleteEntryParcel.Set_CadastralNumber(Value: UnicodeString);
begin
  SetAttribute('CadastralNumber', Value);
end;

{ TXMLTSpecifyRelatedParcel }

procedure TXMLTSpecifyRelatedParcel.AfterConstruction;
begin
  RegisterChildNode('AllBorder', TXMLTSpecifyRelatedParcel_AllBorder);
  RegisterChildNode('ChangeBorder', TXMLTChangeBorder);
  RegisterChildNode('Contours', TXMLContours);
  RegisterChildNode('DeleteAllBorder', TXMLTSpecifyRelatedParcel_DeleteAllBorder);
  RegisterChildNode('ExistSubParcels', TXMLTSpecifyRelatedParcel_ExistSubParcels);
  FChangeBorder := CreateCollection(TXMLTSpecifyRelatedParcel_ChangeBorderList, IXMLTSpecifyRelatedParcel_ChangeBorder, 'ChangeBorder') as IXMLTSpecifyRelatedParcel_ChangeBorderList;
  FDeleteAllBorder := CreateCollection(TXMLTSpecifyRelatedParcel_DeleteAllBorderList, IXMLTSpecifyRelatedParcel_DeleteAllBorder, 'DeleteAllBorder') as IXMLTSpecifyRelatedParcel_DeleteAllBorderList;
  inherited;
end;

function TXMLTSpecifyRelatedParcel.Get_CadastralNumber: UnicodeString;
begin
  Result := AttributeNodes['CadastralNumber'].Text;
end;

procedure TXMLTSpecifyRelatedParcel.Set_CadastralNumber(Value: UnicodeString);
begin
  SetAttribute('CadastralNumber', Value);
end;

function TXMLTSpecifyRelatedParcel.Get_Number_Record: LongWord;
begin
  Result := AttributeNodes['Number_Record'].NodeValue;
end;

procedure TXMLTSpecifyRelatedParcel.Set_Number_Record(Value: LongWord);
begin
  SetAttribute('Number_Record', Value);
end;

function TXMLTSpecifyRelatedParcel.Get_AllBorder: IXMLTSpecifyRelatedParcel_AllBorder;
begin
  Result := ChildNodes['AllBorder'] as IXMLTSpecifyRelatedParcel_AllBorder;
end;

function TXMLTSpecifyRelatedParcel.Get_ChangeBorder: IXMLTSpecifyRelatedParcel_ChangeBorderList;
begin
  Result := FChangeBorder;
end;

function TXMLTSpecifyRelatedParcel.Get_Contours: IXMLContours;
begin
  Result := ChildNodes['Contours'] as IXMLContours;
end;

function TXMLTSpecifyRelatedParcel.Get_DeleteAllBorder: IXMLTSpecifyRelatedParcel_DeleteAllBorderList;
begin
  Result := FDeleteAllBorder;
end;

function TXMLTSpecifyRelatedParcel.Get_ExistSubParcels: IXMLTSpecifyRelatedParcel_ExistSubParcels;
begin
  Result := ChildNodes['ExistSubParcels'] as IXMLTSpecifyRelatedParcel_ExistSubParcels;
end;

{ TXMLTSpecifyRelatedParcel_AllBorder }

procedure TXMLTSpecifyRelatedParcel_AllBorder.AfterConstruction;
begin
  RegisterChildNode('Entity_Spatial', TXMLEntity_Spatial);
  inherited;
end;

function TXMLTSpecifyRelatedParcel_AllBorder.Get_Entity_Spatial: IXMLEntity_Spatial;
begin
  Result := ChildNodes['Entity_Spatial'] as IXMLEntity_Spatial;
end;

{ TXMLTSpecifyRelatedParcel_ChangeBorder }

procedure TXMLTSpecifyRelatedParcel_ChangeBorder.AfterConstruction;
begin
  RegisterChildNode('OldOrdinate', TXMLTOrdinate);
  RegisterChildNode('NewOrdinate', TXMLTOrdinate);
  inherited;
end;

function TXMLTSpecifyRelatedParcel_ChangeBorder.Get_OldOrdinate: IXMLTOrdinate;
begin
  Result := ChildNodes['OldOrdinate'] as IXMLTOrdinate;
end;

function TXMLTSpecifyRelatedParcel_ChangeBorder.Get_NewOrdinate: IXMLTOrdinate;
begin
  Result := ChildNodes['NewOrdinate'] as IXMLTOrdinate;
end;

{ TXMLTSpecifyRelatedParcel_ChangeBorderList }

function TXMLTSpecifyRelatedParcel_ChangeBorderList.Add: IXMLTChangeBorder;
begin
  Result := AddItem(-1) as IXMLTChangeBorder;
end;

function TXMLTSpecifyRelatedParcel_ChangeBorderList.Insert(const Index: Integer): IXMLTChangeBorder;
begin
  Result := AddItem(Index) as IXMLTChangeBorder;
end;

function TXMLTSpecifyRelatedParcel_ChangeBorderList.Get_Item(Index: Integer): IXMLTChangeBorder;
begin
  Result := List[Index] as IXMLTChangeBorder;
end;

{ TXMLTSpecifyRelatedParcel_Contours }

procedure TXMLTSpecifyRelatedParcel_Contours.AfterConstruction;
begin
  RegisterChildNode('NewContour', TXMLTSpecifyRelatedParcel_Contours_NewContour);
  ItemTag := 'NewContour';
  ItemInterface := IXMLTSpecifyRelatedParcel_Contours_NewContour;
  inherited;
end;

function TXMLTSpecifyRelatedParcel_Contours.Get_NewContour(Index: Integer): IXMLTSpecifyRelatedParcel_Contours_NewContour;
begin
  Result := List[Index] as IXMLTSpecifyRelatedParcel_Contours_NewContour;
end;

function TXMLTSpecifyRelatedParcel_Contours.Add: IXMLTSpecifyRelatedParcel_Contours_NewContour;
begin
  Result := AddItem(-1) as IXMLTSpecifyRelatedParcel_Contours_NewContour;
end;

function TXMLTSpecifyRelatedParcel_Contours.Insert(const Index: Integer): IXMLTSpecifyRelatedParcel_Contours_NewContour;
begin
  Result := AddItem(Index) as IXMLTSpecifyRelatedParcel_Contours_NewContour;
end;

{ TXMLTSpecifyRelatedParcel_Contours_NewContour }

procedure TXMLTSpecifyRelatedParcel_Contours_NewContour.AfterConstruction;
begin
  RegisterChildNode('Providing_Pass_CadastralNumbers', TXMLTProviding_Pass_CadastralNumbers);
  inherited;
end;

function TXMLTSpecifyRelatedParcel_Contours_NewContour.Get_Definition: UnicodeString;
begin
  Result := AttributeNodes['Definition'].Text;
end;

procedure TXMLTSpecifyRelatedParcel_Contours_NewContour.Set_Definition(Value: UnicodeString);
begin
  SetAttribute('Definition', Value);
end;

function TXMLTSpecifyRelatedParcel_Contours_NewContour.Get_Providing_Pass_CadastralNumbers: IXMLTProviding_Pass_CadastralNumbers;
begin
  Result := ChildNodes['Providing_Pass_CadastralNumbers'] as IXMLTProviding_Pass_CadastralNumbers;
end;

{ TXMLTSpecifyRelatedParcel_DeleteAllBorder }

procedure TXMLTSpecifyRelatedParcel_DeleteAllBorder.AfterConstruction;
begin
  RegisterChildNode('OldOrdinate', TXMLTOrdinate);
  inherited;
end;

function TXMLTSpecifyRelatedParcel_DeleteAllBorder.Get_OldOrdinate: IXMLTOrdinate;
begin
  Result := ChildNodes['OldOrdinate'] as IXMLTOrdinate;
end;

{ TXMLTSpecifyRelatedParcel_DeleteAllBorderList }

function TXMLTSpecifyRelatedParcel_DeleteAllBorderList.Add: IXMLTSpecifyRelatedParcel_DeleteAllBorder;
begin
  Result := AddItem(-1) as IXMLTSpecifyRelatedParcel_DeleteAllBorder;
end;

function TXMLTSpecifyRelatedParcel_DeleteAllBorderList.Insert(const Index: Integer): IXMLTSpecifyRelatedParcel_DeleteAllBorder;
begin
  Result := AddItem(Index) as IXMLTSpecifyRelatedParcel_DeleteAllBorder;
end;

function TXMLTSpecifyRelatedParcel_DeleteAllBorderList.Get_Item(Index: Integer): IXMLTSpecifyRelatedParcel_DeleteAllBorder;
begin
  Result := List[Index] as IXMLTSpecifyRelatedParcel_DeleteAllBorder;
end;

{ TXMLTSpecifyRelatedParcel_ExistSubParcels }

procedure TXMLTSpecifyRelatedParcel_ExistSubParcels.AfterConstruction;
begin
  RegisterChildNode('ExistSubParcel', TXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel);
  ItemTag := 'ExistSubParcel';
  ItemInterface := IXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel;
  inherited;
end;

function TXMLTSpecifyRelatedParcel_ExistSubParcels.Get_ExistSubParcel(Index: Integer): IXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel;
begin
  Result := List[Index] as IXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel;
end;

function TXMLTSpecifyRelatedParcel_ExistSubParcels.Add: IXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel;
begin
  Result := AddItem(-1) as IXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel;
end;

function TXMLTSpecifyRelatedParcel_ExistSubParcels.Insert(const Index: Integer): IXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel;
begin
  Result := AddItem(Index) as IXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel;
end;

{ TXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel }

procedure TXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel.AfterConstruction;
begin
  RegisterChildNode('Entity_Spatial', TXMLEntity_Spatial);
  RegisterChildNode('Contours', TXMLTSubParcel_Contours);
  inherited;
end;

function TXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel.Get_Number_Record: LongWord;
begin
  Result := AttributeNodes['Number_Record'].NodeValue;
end;

procedure TXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel.Set_Number_Record(Value: LongWord);
begin
  SetAttribute('Number_Record', Value);
end;

function TXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel.Get_Entity_Spatial: IXMLEntity_Spatial;
begin
  Result := ChildNodes['Entity_Spatial'] as IXMLEntity_Spatial;
end;

function TXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel.Get_Contours: IXMLTSubParcel_Contours;
begin
  Result := ChildNodes['Contours'] as IXMLTSubParcel_Contours;
end;

{ TXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Entity_Spatial }

procedure TXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Entity_Spatial.AfterConstruction;
begin
  RegisterChildNode('Spatial_Element', TXMLTSPATIAL_ELEMENT_OLD_NEW);
  ItemTag := 'Spatial_Element';
  ItemInterface := IXMLTSPATIAL_ELEMENT;
  inherited;
end;

function TXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Entity_Spatial.Get_Ent_Sys: UnicodeString;
begin
  Result := AttributeNodes['Ent_Sys'].Text;
end;

procedure TXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Entity_Spatial.Set_Ent_Sys(Value: UnicodeString);
begin
  SetAttribute('Ent_Sys', Value);
end;

function TXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Entity_Spatial.Get_Spatial_Element(Index: Integer): IXMLTSPATIAL_ELEMENT;
begin
  Result := List[Index] as IXMLTSPATIAL_ELEMENT;
end;

function TXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Entity_Spatial.Add: IXMLTSPATIAL_ELEMENT;
begin
  Result := AddItem(-1) as IXMLTSPATIAL_ELEMENT;
end;

function TXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Entity_Spatial.Insert(const Index: Integer): IXMLTSPATIAL_ELEMENT;
begin
  Result := AddItem(Index) as IXMLTSPATIAL_ELEMENT;
end;

{ TXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Contours }

procedure TXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Contours.AfterConstruction;
begin
  RegisterChildNode('Contour', TXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Contours_Contour);
  ItemTag := 'Contour';
  ItemInterface := IXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Contours_Contour;
  inherited;
end;

function TXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Contours.Get_Contour(Index: Integer): IXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Contours_Contour;
begin
  Result := List[Index] as IXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Contours_Contour;
end;

function TXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Contours.Add: IXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Contours_Contour;
begin
  Result := AddItem(-1) as IXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Contours_Contour;
end;

function TXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Contours.Insert(const Index: Integer): IXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Contours_Contour;
begin
  Result := AddItem(Index) as IXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Contours_Contour;
end;

{ TXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Contours_Contour }

procedure TXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Contours_Contour.AfterConstruction;
begin
  RegisterChildNode('Entity_Spatial', TXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Contours_Contour_Entity_Spatial);
  inherited;
end;

function TXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Contours_Contour.Get_Number: LongWord;
begin
  Result := AttributeNodes['Number'].NodeValue;
end;

procedure TXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Contours_Contour.Set_Number(Value: LongWord);
begin
  SetAttribute('Number', Value);
end;

function TXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Contours_Contour.Get_Entity_Spatial: IXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Contours_Contour_Entity_Spatial;
begin
  Result := ChildNodes['Entity_Spatial'] as IXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Contours_Contour_Entity_Spatial;
end;

{ TXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Contours_Contour_Entity_Spatial }

procedure TXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Contours_Contour_Entity_Spatial.AfterConstruction;
begin
  RegisterChildNode('Spatial_Element', TXMLTSPATIAL_ELEMENT_OLD_NEW);
  ItemTag := 'Spatial_Element';
  ItemInterface := IXMLTSPATIAL_ELEMENT;
  inherited;
end;

function TXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Contours_Contour_Entity_Spatial.Get_Ent_Sys: UnicodeString;
begin
  Result := AttributeNodes['Ent_Sys'].Text;
end;

procedure TXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Contours_Contour_Entity_Spatial.Set_Ent_Sys(Value: UnicodeString);
begin
  SetAttribute('Ent_Sys', Value);
end;

function TXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Contours_Contour_Entity_Spatial.Get_Spatial_Element(Index: Integer): IXMLTSPATIAL_ELEMENT;
begin
  Result := List[Index] as IXMLTSPATIAL_ELEMENT;
end;

function TXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Contours_Contour_Entity_Spatial.Add: IXMLTSPATIAL_ELEMENT;
begin
  Result := AddItem(-1) as IXMLTSPATIAL_ELEMENT;
end;

function TXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel_Contours_Contour_Entity_Spatial.Insert(const Index: Integer): IXMLTSPATIAL_ELEMENT;
begin
  Result := AddItem(Index) as IXMLTSPATIAL_ELEMENT;
end;

{ TXMLSTD_MP_Package_FormParcels_SpecifyRelatedParcel }

{ TXMLSTD_MP_Package_FormParcels_SpecifyRelatedParcelList }

function TXMLSTD_MP_Package_FormParcels_SpecifyRelatedParcelList.Add: IXMLSTD_MP_Package_FormParcels_SpecifyRelatedParcel;
begin
  Result := AddItem(-1) as IXMLSTD_MP_Package_FormParcels_SpecifyRelatedParcel;
end;

function TXMLSTD_MP_Package_FormParcels_SpecifyRelatedParcelList.Insert(const Index: Integer): IXMLSTD_MP_Package_FormParcels_SpecifyRelatedParcel;
begin
  Result := AddItem(Index) as IXMLSTD_MP_Package_FormParcels_SpecifyRelatedParcel;
end;

function TXMLSTD_MP_Package_FormParcels_SpecifyRelatedParcelList.Get_Item(Index: Integer): IXMLSTD_MP_Package_FormParcels_SpecifyRelatedParcel;
begin
  Result := List[Index] as IXMLSTD_MP_Package_FormParcels_SpecifyRelatedParcel;
end;

{ TXMLSTD_MP_Package_SpecifyParcel }

procedure TXMLSTD_MP_Package_SpecifyParcel.AfterConstruction;
begin
  RegisterChildNode('ExistParcel', TXMLTExistParcel);
  RegisterChildNode('ExistEZ', TXMLSTD_MP_Package_SpecifyParcel_ExistEZ);
  RegisterChildNode('SpecifyRelatedParcel', TXMLSTD_MP_Package_SpecifyParcel_SpecifyRelatedParcel);
  FSpecifyRelatedParcel := CreateCollection(TXMLSTD_MP_Package_SpecifyParcel_SpecifyRelatedParcelList, IXMLSTD_MP_Package_SpecifyParcel_SpecifyRelatedParcel, 'SpecifyRelatedParcel') as IXMLSTD_MP_Package_SpecifyParcel_SpecifyRelatedParcelList;
  inherited;
end;

function TXMLSTD_MP_Package_SpecifyParcel.Get_ExistParcel: IXMLTExistParcel;
begin
 Result := ChildNodes['ExistParcel'] as IXMLTExistParcel;
end;

function TXMLSTD_MP_Package_SpecifyParcel.Get_ExistEZ: IXMLSTD_MP_Package_SpecifyParcel_ExistEZ;
begin
  Result := ChildNodes['ExistEZ'] as IXMLSTD_MP_Package_SpecifyParcel_ExistEZ;
end;

function TXMLSTD_MP_Package_SpecifyParcel.Get_SpecifyRelatedParcel: IXMLSTD_MP_Package_SpecifyParcel_SpecifyRelatedParcelList;
begin
  Result := FSpecifyRelatedParcel;
end;

{ TXMLTExistParcel }

procedure TXMLTExistParcel.AfterConstruction;
begin
  RegisterChildNode('Inner_CadastralNumbers', TXMLTInner_CadastralNumbers);
  RegisterChildNode('Area', TXMLTArea);
  RegisterChildNode('SubParcels', TXMLTExistParcel_SubParcels);
  RegisterChildNode('Entity_Spatial', TXMLEntity_Spatial);
  RegisterChildNode('Contours', TXMLContours);
  RegisterChildNode('Min_Area', TXMLTArea_without_Innccuracy);
  RegisterChildNode('Max_Area', TXMLTArea_without_Innccuracy);
  RegisterChildNode('RelatedParcels', TXMLRelatedParcels);
  inherited;
end;

function TXMLTExistParcel.Get_CadastralNumber: UnicodeString;
begin
  Result := AttributeNodes['CadastralNumber'].Text;
end;

procedure TXMLTExistParcel.Set_CadastralNumber(Value: UnicodeString);
begin
  SetAttribute('CadastralNumber', Value);
end;

function TXMLTExistParcel.Get_CadastralBlock: UnicodeString;
begin
  Result := ChildNodes['CadastralBlock'].Text;
end;

procedure TXMLTExistParcel.Set_CadastralBlock(Value: UnicodeString);
begin
  ChildNodes['CadastralBlock'].NodeValue := Value;
end;

function TXMLTExistParcel.Get_Inner_CadastralNumbers: IXMLTInner_CadastralNumbers;
begin
  Result := ChildNodes['Inner_CadastralNumbers'] as IXMLTInner_CadastralNumbers;
end;

function TXMLTExistParcel.Get_Area: IXMLTArea;
begin
  Result := ChildNodes['Area'] as IXMLTArea;
end;

function TXMLTExistParcel.Get_SubParcels: IXMLTExistParcel_SubParcels;
begin
  Result := ChildNodes['SubParcels'] as IXMLTExistParcel_SubParcels;
end;

function TXMLTExistParcel.Get_Entity_Spatial: IXMLEntity_Spatial;
begin
  Result := ChildNodes['Entity_Spatial'] as IXMLEntity_Spatial;
end;

function TXMLTExistParcel.Get_Contours: IXMLContours;
begin
  Result := ChildNodes['Contours'] as IXMLContours;
end;

function TXMLTExistParcel.Get_Area_In_GKN: LongWord;
begin
  Result := ChildNodes['Area_In_GKN'].NodeValue;
end;

procedure TXMLTExistParcel.Set_Area_In_GKN(Value: LongWord);
begin
  ChildNodes['Area_In_GKN'].NodeValue := Value;
end;

function TXMLTExistParcel.Get_Delta_Area: LongWord;
begin
  Result := ChildNodes['Delta_Area'].NodeValue;
end;

procedure TXMLTExistParcel.Set_Delta_Area(Value: LongWord);
begin
  ChildNodes['Delta_Area'].NodeValue := Value;
end;

function TXMLTExistParcel.Get_Min_Area: IXMLTArea_without_Innccuracy;
begin
  Result := ChildNodes['Min_Area'] as IXMLTArea_without_Innccuracy;
end;

function TXMLTExistParcel.Get_Max_Area: IXMLTArea_without_Innccuracy;
begin
  Result := ChildNodes['Max_Area'] as IXMLTArea_without_Innccuracy;
end;

function TXMLTExistParcel.Get_Note: UnicodeString;
begin
  Result := ChildNodes['Note'].Text;
end;

procedure TXMLTExistParcel.Set_Note(Value: UnicodeString);
begin
  ChildNodes['Note'].NodeValue := Value;
end;

function TXMLTExistParcel.Get_RelatedParcels: IXMLRelatedParcels;
begin
  Result := ChildNodes['RelatedParcels'] as IXMLRelatedParcels;
end;

{ TXMLTExistParcel_Inner_CadastralNumbers }

{procedure TXMLTExistParcel_Inner_CadastralNumbers.AfterConstruction;
begin
  FCadastralNumber := CreateCollection(TXMLTExistParcel_Inner_CadastralNumbers_CadastralNumberList, IXMLNode, 'CadastralNumber') as IXMLTExistParcel_Inner_CadastralNumbers_CadastralNumberList;
  FNumber := CreateCollection(TXMLString_List, IXMLNode, 'Number') as IXMLString_List;
  inherited;
end;}

{function TXMLTExistParcel_Inner_CadastralNumbers.Get_CadastralNumber: IXMLTExistParcel_Inner_CadastralNumbers_CadastralNumberList;
begin
  Result := FCadastralNumber;
end;

function TXMLTExistParcel_Inner_CadastralNumbers.Get_Number: IXMLString_List;
begin
  Result := FNumber;
end;

{ TXMLTExistParcel_SubParcels }

procedure TXMLTExistParcel_SubParcels.AfterConstruction;
begin
  RegisterChildNode('FormSubParcel', TXMLTExistParcel_SubParcels_FormSubParcel);
  RegisterChildNode('ExistSubParcel', TXMLTExistParcel_SubParcels_ExistSubParcel);
  RegisterChildNode('InvariableSubParcel', TXMLTExistParcel_SubParcels_InvariableSubParcel);
  FFormSubParcel := CreateCollection(TXMLTExistParcel_SubParcels_FormSubParcelList, IXMLTExistParcel_SubParcels_FormSubParcel, 'FormSubParcel') as IXMLTExistParcel_SubParcels_FormSubParcelList;
  FExistSubParcel := CreateCollection(TXMLTExistParcel_SubParcels_ExistSubParcelList, IXMLTExistParcel_SubParcels_ExistSubParcel, 'ExistSubParcel') as IXMLTExistParcel_SubParcels_ExistSubParcelList;
  FInvariableSubParcel := CreateCollection(TXMLTExistParcel_SubParcels_InvariableSubParcelList, IXMLTExistParcel_SubParcels_InvariableSubParcel, 'InvariableSubParcel') as IXMLTExistParcel_SubParcels_InvariableSubParcelList;
  inherited;
end;

function TXMLTExistParcel_SubParcels.Get_FormSubParcel: IXMLTExistParcel_SubParcels_FormSubParcelList;
begin
  Result := FFormSubParcel;
end;

function TXMLTExistParcel_SubParcels.Get_ExistSubParcel: IXMLTExistParcel_SubParcels_ExistSubParcelList;
begin
  Result := FExistSubParcel;
end;

function TXMLTExistParcel_SubParcels.Get_InvariableSubParcel: IXMLTExistParcel_SubParcels_InvariableSubParcelList;
begin
  Result := FInvariableSubParcel;
end;

{ TXMLTExistParcel_SubParcels_FormSubParcel }

function TXMLTExistParcel_SubParcels_FormSubParcel.Get_Definition: UnicodeString;
begin
  Result := AttributeNodes['Definition'].Text;
end;

procedure TXMLTExistParcel_SubParcels_FormSubParcel.Set_Definition(Value: UnicodeString);
begin
  SetAttribute('Definition', Value);
end;

{ TXMLTExistParcel_SubParcels_FormSubParcelList }

function TXMLTExistParcel_SubParcels_FormSubParcelList.Add: IXMLTExistParcel_SubParcels_FormSubParcel;
begin
  Result := AddItem(-1) as IXMLTExistParcel_SubParcels_FormSubParcel;
end;

function TXMLTExistParcel_SubParcels_FormSubParcelList.Insert(const Index: Integer): IXMLTExistParcel_SubParcels_FormSubParcel;
begin
  Result := AddItem(Index) as IXMLTExistParcel_SubParcels_FormSubParcel;
end;

function TXMLTExistParcel_SubParcels_FormSubParcelList.Get_Item(Index: Integer): IXMLTExistParcel_SubParcels_FormSubParcel;
begin
  Result := List[Index] as IXMLTExistParcel_SubParcels_FormSubParcel;
end;

{ TXMLTExistParcel_SubParcels_ExistSubParcel }

function TXMLTExistParcel_SubParcels_ExistSubParcel.Get_Number_Record: LongWord;
begin
  Result := AttributeNodes['Number_Record'].NodeValue;
end;

procedure TXMLTExistParcel_SubParcels_ExistSubParcel.Set_Number_Record(Value: LongWord);
begin
  SetAttribute('Number_Record', Value);
end;

{ TXMLTExistParcel_SubParcels_ExistSubParcelList }

function TXMLTExistParcel_SubParcels_ExistSubParcelList.Add: IXMLTExistParcel_SubParcels_ExistSubParcel;
begin
  Result := AddItem(-1) as IXMLTExistParcel_SubParcels_ExistSubParcel;
end;

function TXMLTExistParcel_SubParcels_ExistSubParcelList.Insert(const Index: Integer): IXMLTExistParcel_SubParcels_ExistSubParcel;
begin
  Result := AddItem(Index) as IXMLTExistParcel_SubParcels_ExistSubParcel;
end;

function TXMLTExistParcel_SubParcels_ExistSubParcelList.Get_Item(Index: Integer): IXMLTExistParcel_SubParcels_ExistSubParcel;
begin
  Result := List[Index] as IXMLTExistParcel_SubParcels_ExistSubParcel;
end;

{ TXMLTExistParcel_SubParcels_InvariableSubParcel }

procedure TXMLTExistParcel_SubParcels_InvariableSubParcel.AfterConstruction;
begin
  RegisterChildNode('Area', TXMLTExistParcel_SubParcels_InvariableSubParcel_Area);
  RegisterChildNode('Encumbrance', TXMLTEncumbrance);
  RegisterChildNode('Contours', TXMLTExistParcel_SubParcels_InvariableSubParcel_Contours);
  inherited;
end;

function TXMLTExistParcel_SubParcels_InvariableSubParcel.Get_Number_Record: LongWord;
begin
  Result := AttributeNodes['Number_Record'].NodeValue;
end;

procedure TXMLTExistParcel_SubParcels_InvariableSubParcel.Set_Number_Record(Value: LongWord);
begin
  SetAttribute('Number_Record', Value);
end;

function TXMLTExistParcel_SubParcels_InvariableSubParcel.Get_SubParcel_Realty: Boolean;
begin
  Result := AttributeNodes['SubParcel_Realty'].NodeValue;
end;

procedure TXMLTExistParcel_SubParcels_InvariableSubParcel.Set_SubParcel_Realty(Value: Boolean);
begin
  SetAttribute('SubParcel_Realty', Value);
end;

function TXMLTExistParcel_SubParcels_InvariableSubParcel.Get_Area: IXMLTExistParcel_SubParcels_InvariableSubParcel_Area;
begin
  Result := ChildNodes['Area'] as IXMLTExistParcel_SubParcels_InvariableSubParcel_Area;
end;

function TXMLTExistParcel_SubParcels_InvariableSubParcel.Get_Encumbrance: IXMLTEncumbrance;
begin
  Result := ChildNodes['Encumbrance'] as IXMLTEncumbrance;
end;

function TXMLTExistParcel_SubParcels_InvariableSubParcel.Get_Contours: IXMLTExistParcel_SubParcels_InvariableSubParcel_Contours;
begin
  Result := ChildNodes['Contours'] as IXMLTExistParcel_SubParcels_InvariableSubParcel_Contours;
end;

{ TXMLTExistParcel_SubParcels_InvariableSubParcelList }

function TXMLTExistParcel_SubParcels_InvariableSubParcelList.Add: IXMLTExistParcel_SubParcels_InvariableSubParcel;
begin
  Result := AddItem(-1) as IXMLTExistParcel_SubParcels_InvariableSubParcel;
end;

function TXMLTExistParcel_SubParcels_InvariableSubParcelList.Insert(const Index: Integer): IXMLTExistParcel_SubParcels_InvariableSubParcel;
begin
  Result := AddItem(Index) as IXMLTExistParcel_SubParcels_InvariableSubParcel;
end;

function TXMLTExistParcel_SubParcels_InvariableSubParcelList.Get_Item(Index: Integer): IXMLTExistParcel_SubParcels_InvariableSubParcel;
begin
  Result := List[Index] as IXMLTExistParcel_SubParcels_InvariableSubParcel;
end;

{ TXMLTExistParcel_SubParcels_InvariableSubParcel_Area }

function TXMLTExistParcel_SubParcels_InvariableSubParcel_Area.Get_Area: LongWord;
begin
  Result := ChildNodes['Area'].NodeValue;
end;

procedure TXMLTExistParcel_SubParcels_InvariableSubParcel_Area.Set_Area(Value: LongWord);
begin
  ChildNodes['Area'].NodeValue := Value;
end;

function TXMLTExistParcel_SubParcels_InvariableSubParcel_Area.Get_Unit_: UnicodeString;
begin
  Result := ChildNodes['Unit'].Text;
end;

procedure TXMLTExistParcel_SubParcels_InvariableSubParcel_Area.Set_Unit_(Value: UnicodeString);
begin
  ChildNodes['Unit'].NodeValue := Value;
end;

{ TXMLTExistParcel_SubParcels_InvariableSubParcel_Contours }

procedure TXMLTExistParcel_SubParcels_InvariableSubParcel_Contours.AfterConstruction;
begin
  RegisterChildNode('Contour', TXMLTExistParcel_SubParcels_InvariableSubParcel_Contours_Contour);
  ItemTag := 'Contour';
  ItemInterface := IXMLTExistParcel_SubParcels_InvariableSubParcel_Contours_Contour;
  inherited;
end;

function TXMLTExistParcel_SubParcels_InvariableSubParcel_Contours.Get_Contour(Index: Integer): IXMLTExistParcel_SubParcels_InvariableSubParcel_Contours_Contour;
begin
  Result := List[Index] as IXMLTExistParcel_SubParcels_InvariableSubParcel_Contours_Contour;
end;

function TXMLTExistParcel_SubParcels_InvariableSubParcel_Contours.Add: IXMLTExistParcel_SubParcels_InvariableSubParcel_Contours_Contour;
begin
  Result := AddItem(-1) as IXMLTExistParcel_SubParcels_InvariableSubParcel_Contours_Contour;
end;

function TXMLTExistParcel_SubParcels_InvariableSubParcel_Contours.Insert(const Index: Integer): IXMLTExistParcel_SubParcels_InvariableSubParcel_Contours_Contour;
begin
  Result := AddItem(Index) as IXMLTExistParcel_SubParcels_InvariableSubParcel_Contours_Contour;
end;

{ TXMLTExistParcel_SubParcels_InvariableSubParcel_Contours_Contour }

procedure TXMLTExistParcel_SubParcels_InvariableSubParcel_Contours_Contour.AfterConstruction;
begin
  RegisterChildNode('Area', TXMLTArea);
  inherited;
end;

function TXMLTExistParcel_SubParcels_InvariableSubParcel_Contours_Contour.Get_Number: UnicodeString;
begin
  Result := AttributeNodes['Number'].Text;
end;

procedure TXMLTExistParcel_SubParcels_InvariableSubParcel_Contours_Contour.Set_Number(Value: UnicodeString);
begin
  SetAttribute('Number', Value);
end;

function TXMLTExistParcel_SubParcels_InvariableSubParcel_Contours_Contour.Get_Area: IXMLTArea;
begin
  Result := ChildNodes['Area'] as IXMLTArea;
end;

{ TXMLContours }

procedure TXMLContours.AfterConstruction;
begin
  RegisterChildNode('NewContour', TXMLContours_NewContour);
  RegisterChildNode('ExistContour', TXMLContours_ExistContour);
  RegisterChildNode('DeleteAllBorder', TXMLContours_DeleteAllBorder);
  FNewContour := CreateCollection(TXMLContours_NewContourList, IXMLContours_NewContour, 'NewContour') as IXMLContours_NewContourList;
  FExistContour := CreateCollection(TXMLContours_ExistContourList, IXMLContours_ExistContour, 'ExistContour') as IXMLContours_ExistContourList;
  FDeleteAllBorder := CreateCollection(TXMLContours_DeleteAllBorderList, IXMLContours_DeleteAllBorder, 'DeleteAllBorder') as IXMLContours_DeleteAllBorderList;
  inherited;
end;

function TXMLContours.Get_NewContour: IXMLContours_NewContourList;
begin
  Result := FNewContour;
end;

function TXMLContours.Get_ExistContour: IXMLContours_ExistContourList;
begin
  Result := FExistContour;
end;

function TXMLContours.Get_DeleteAllBorder: IXMLContours_DeleteAllBorderList;
begin
  Result := FDeleteAllBorder;
end;

{ TXMLContours_NewContour }

procedure TXMLContours_NewContour.AfterConstruction;
begin
  RegisterChildNode('Providing_Pass_CadastralNumbers', TXMLTProviding_Pass_CadastralNumbers);
  inherited;
end;

function TXMLContours_NewContour.Get_Definition: UnicodeString;
begin
  Result := AttributeNodes['Definition'].Text;
end;

procedure TXMLContours_NewContour.Set_Definition(Value: UnicodeString);
begin
  SetAttribute('Definition', Value);
end;

function TXMLContours_NewContour.Get_Providing_Pass_CadastralNumbers: IXMLTProviding_Pass_CadastralNumbers;
begin
  Result := ChildNodes['Providing_Pass_CadastralNumbers'] as IXMLTProviding_Pass_CadastralNumbers;
end;

{ TXMLContours_NewContourList }

function TXMLContours_NewContourList.Add: IXMLContours_NewContour;
begin
  Result := AddItem(-1) as IXMLContours_NewContour;
end;

function TXMLContours_NewContourList.Insert(const Index: Integer): IXMLContours_NewContour;
begin
  Result := AddItem(Index) as IXMLContours_NewContour;
end;

function TXMLContours_NewContourList.Get_Item(Index: Integer): IXMLContours_NewContour;
begin
  Result := List[Index] as IXMLContours_NewContour;
end;

{ TXMLContours_ExistContour }

function TXMLContours_ExistContour.Get_Number_Record: LongWord;
begin
  Result := AttributeNodes['Number_Record'].NodeValue;
end;

procedure TXMLContours_ExistContour.Set_Number_Record(Value: LongWord);
begin
  SetAttribute('Number_Record', Value);
end;

{ TXMLContours_ExistContourList }

function TXMLContours_ExistContourList.Add: IXMLContours_ExistContour;
begin
  Result := AddItem(-1) as IXMLContours_ExistContour;
end;

function TXMLContours_ExistContourList.Insert(const Index: Integer): IXMLContours_ExistContour;
begin
  Result := AddItem(Index) as IXMLContours_ExistContour;
end;

function TXMLContours_ExistContourList.Get_Item(Index: Integer): IXMLContours_ExistContour;
begin
  Result := List[Index] as IXMLContours_ExistContour;
end;


{ TXMLContours_DeleteAllBorder }

procedure TXMLContours_DeleteAllBorder.AfterConstruction;
begin
  RegisterChildNode('OldOrdinate', TXMLTOrdinate);
  ItemTag := 'OldOrdinate';
  ItemInterface := IXMLTOrdinate;
  inherited;
end;

function TXMLContours_DeleteAllBorder.Get_Number_Record: LongWord;
begin
  Result := AttributeNodes['Number_Record'].NodeValue;
end;

procedure TXMLContours_DeleteAllBorder.Set_Number_Record(Value: LongWord);
begin
  SetAttribute('Number_Record', Value);
end;

function TXMLContours_DeleteAllBorder.Get_OldOrdinate(Index: Integer): IXMLTOrdinate;
begin
  Result := List[Index] as IXMLTOrdinate;
end;

function TXMLContours_DeleteAllBorder.Add: IXMLTOrdinate;
begin
  Result := AddItem(-1) as IXMLTOrdinate;
end;

function TXMLContours_DeleteAllBorder.Insert(const Index: Integer): IXMLTOrdinate;
begin
  Result := ChildNodes['OldOrdinate'] as IXMLTOrdinate;
end;

{ TXMLContours_DeleteAllBorderList }

function TXMLContours_DeleteAllBorderList.Add: IXMLContours_DeleteAllBorder;
begin
  Result := AddItem(-1) as IXMLContours_DeleteAllBorder;
end;

function TXMLContours_DeleteAllBorderList.Insert(const Index: Integer): IXMLContours_DeleteAllBorder;
begin
  Result := AddItem(Index) as IXMLContours_DeleteAllBorder;
end;

function TXMLContours_DeleteAllBorderList.Get_Item(Index: Integer): IXMLContours_DeleteAllBorder;
begin
  Result := List[Index] as IXMLContours_DeleteAllBorder;
end;

{ TXMLRelatedParcels }

procedure TXMLRelatedParcels.AfterConstruction;
begin
  RegisterChildNode('ParcelNeighbours', TXMLRelatedParcels_ParcelNeighbours);
  ItemTag := 'ParcelNeighbours';
  ItemInterface := IXMLRelatedParcels_ParcelNeighbours;
  inherited;
end;

function TXMLRelatedParcels.Get_ParcelNeighbours(Index: Integer): IXMLRelatedParcels_ParcelNeighbours;
begin
  Result := List[Index] as IXMLRelatedParcels_ParcelNeighbours;
end;

function TXMLRelatedParcels.Add: IXMLRelatedParcels_ParcelNeighbours;
begin
  Result := AddItem(-1) as IXMLRelatedParcels_ParcelNeighbours;
end;

function TXMLRelatedParcels.Insert(const Index: Integer): IXMLRelatedParcels_ParcelNeighbours;
begin
  Result := AddItem(Index) as IXMLRelatedParcels_ParcelNeighbours;
end;

{ TXMLRelatedParcels_ParcelNeighbours }

procedure TXMLRelatedParcels_ParcelNeighbours.AfterConstruction;
begin
  RegisterChildNode('ParcelNeighbour', TXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour);
  FParcelNeighbour := CreateCollection(TXMLRelatedParcels_ParcelNeighbours_ParcelNeighbourList, IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour, 'ParcelNeighbour') as IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbourList;
  inherited;
end;

function TXMLRelatedParcels_ParcelNeighbours.Get_Definition: UnicodeString;
begin
  Result := ChildNodes['Definition'].Text;
end;

procedure TXMLRelatedParcels_ParcelNeighbours.Set_Definition(Value: UnicodeString);
begin
  ChildNodes['Definition'].NodeValue := Value;
end;

function TXMLRelatedParcels_ParcelNeighbours.Get_ParcelNeighbour: IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbourList;
begin
  Result := FParcelNeighbour;
end;

{ TXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour }

procedure TXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour.AfterConstruction;
begin
  RegisterChildNode('OwnerNeighbours', TXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours);
  inherited;
end;

function TXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour.Get_Cadastral_Number: UnicodeString;
begin
  Result := ChildNodes['Cadastral_Number'].Text;
end;

procedure TXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour.Set_Cadastral_Number(Value: UnicodeString);
begin
  ChildNodes['Cadastral_Number'].NodeValue := Value;
end;

function TXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour.Get_OwnerNeighbours: IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours;
begin
  Result := ChildNodes['OwnerNeighbours'] as IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours;
end;

{ TXMLRelatedParcels_ParcelNeighbours_ParcelNeighbourList }

function TXMLRelatedParcels_ParcelNeighbours_ParcelNeighbourList.Add: IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour;
begin
  Result := AddItem(-1) as IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour;
end;

function TXMLRelatedParcels_ParcelNeighbours_ParcelNeighbourList.Insert(const Index: Integer): IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour;
begin
  Result := AddItem(Index) as IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour;
end;

function TXMLRelatedParcels_ParcelNeighbours_ParcelNeighbourList.Get_Item(Index: Integer): IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour;
begin
  Result := List[Index] as IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour;
end;

{ TXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours }

procedure TXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours.AfterConstruction;
begin
  RegisterChildNode('OwnerNeighbour', TXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbour);
  FOwnerNeighbour := CreateCollection(TXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbourList, IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbour, 'OwnerNeighbour') as IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbourList;
  inherited;
end;

function TXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours.Get_NameRight: UnicodeString;
begin
  Result := ChildNodes['NameRight'].Text;
end;

procedure TXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours.Set_NameRight(Value: UnicodeString);
begin
  ChildNodes['NameRight'].NodeValue := Value;
end;

function TXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours.Get_OwnerNeighbour: IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbourList;
begin
  Result := FOwnerNeighbour;
end;

{ TXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbour }

procedure TXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbour.AfterConstruction;
begin
  RegisterChildNode('Documents', TXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbour_Documents);
  inherited;
end;

function TXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbour.Get_NameOwner: UnicodeString;
begin
  Result := ChildNodes['NameOwner'].Text;
end;

procedure TXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbour.Set_NameOwner(Value: UnicodeString);
begin
  ChildNodes['NameOwner'].NodeValue := Value;
end;

function TXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbour.Get_ContactAddress: UnicodeString;
begin
  Result := ChildNodes['ContactAddress'].Text;
end;

procedure TXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbour.Set_ContactAddress(Value: UnicodeString);
begin
  ChildNodes['ContactAddress'].NodeValue := Value;
end;

function TXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbour.Get_Documents: IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbour_Documents;
begin
  Result := ChildNodes['Documents'] as IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbour_Documents;
end;

{ TXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbourList }

function TXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbourList.Add: IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbour;
begin
  Result := AddItem(-1) as IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbour;
end;

function TXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbourList.Insert(const Index: Integer): IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbour;
begin
  Result := AddItem(Index) as IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbour;
end;

function TXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbourList.Get_Item(Index: Integer): IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbour;
begin
  Result := List[Index] as IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbour;
end;

{ TXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbour_Documents }

procedure TXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbour_Documents.AfterConstruction;
begin
  RegisterChildNode('Document', TXMLTDocument);
  ItemTag := 'Document';
  ItemInterface := IXMLTDocument;
  inherited;
end;

function TXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbour_Documents.Get_Document(Index: Integer): IXMLTDocument;
begin
  Result := List[Index] as IXMLTDocument;
end;

function TXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbour_Documents.Add: IXMLTDocument;
begin
  Result := AddItem(-1) as IXMLTDocument;
end;

function TXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbour_Documents.Insert(const Index: Integer): IXMLTDocument;
begin
  Result := AddItem(Index) as IXMLTDocument;
end;

{ TXMLSTD_MP_Package_SpecifyParcel_ExistEZ }

procedure TXMLSTD_MP_Package_SpecifyParcel_ExistEZ.AfterConstruction;
begin
  RegisterChildNode('ExistEZParcels', TXMLTExistEZParcel);
  RegisterChildNode('ExistEZEntryParcels', TXMLSTD_MP_Package_SpecifyParcel_ExistEZ_ExistEZEntryParcels);
  inherited;
end;

function TXMLSTD_MP_Package_SpecifyParcel_ExistEZ.Get_ExistEZParcels: IXMLTExistEZParcel;
begin
  Result := ChildNodes['ExistEZParcels'] as IXMLTExistEZParcel;
end;

function TXMLSTD_MP_Package_SpecifyParcel_ExistEZ.Get_ExistEZEntryParcels: IXMLSTD_MP_Package_SpecifyParcel_ExistEZ_ExistEZEntryParcels;
begin
  Result := ChildNodes['ExistEZEntryParcels'] as IXMLSTD_MP_Package_SpecifyParcel_ExistEZ_ExistEZEntryParcels;
end;


{ TXMLTExistEZParcel }

procedure TXMLTExistEZParcel.AfterConstruction;
begin
  RegisterChildNode('Inner_CadastralNumbers', TXMLTExistEZParcel_Inner_CadastralNumbers);
  RegisterChildNode('Area', TXMLTAreaNew);
  RegisterChildNode('SubParcels', TXMLTExistParcel_SubParcels);  // TXMLTExistEZParcel_SubParcels);
  RegisterChildNode('Composition_EZ', TXMLTExistEZParcel_Composition_EZ);
  RegisterChildNode('Min_Area', TXMLTArea_without_Innccuracy);
  RegisterChildNode('Max_Area', TXMLTArea_without_Innccuracy);
  RegisterChildNode('RelatedParcels', TXMLRelatedParcels);
  inherited;
end;

function TXMLTExistEZParcel.Get_CadastralNumber: UnicodeString;
begin
  Result := AttributeNodes['CadastralNumber'].Text;
end;

procedure TXMLTExistEZParcel.Set_CadastralNumber(Value: UnicodeString);
begin
  SetAttribute('CadastralNumber', Value);
end;

function TXMLTExistEZParcel.Get_CadastralBlock: UnicodeString;
begin
  Result := ChildNodes['CadastralBlock'].Text;
end;

procedure TXMLTExistEZParcel.Set_CadastralBlock(Value: UnicodeString);
begin
  ChildNodes['CadastralBlock'].NodeValue := Value;
end;

function TXMLTExistEZParcel.Get_Inner_CadastralNumbers: IXMLTExistEZParcel_Inner_CadastralNumbers;
begin
  Result := ChildNodes['Inner_CadastralNumbers'] as IXMLTExistEZParcel_Inner_CadastralNumbers;
end;

function TXMLTExistEZParcel.Get_Area: IXMLTAreaNew;
begin
  Result := ChildNodes['Area'] as IXMLTAreaNew;
end;

function TXMLTExistEZParcel.Get_SubParcels:IXMLTExistParcel_SubParcels; //IXMLTExistEZParcel_SubParcels;
begin
  Result := ChildNodes['SubParcels'] as IXMLTExistParcel_SubParcels; //IXMLTExistEZParcel_SubParcels;
end;

function TXMLTExistEZParcel.Get_Composition_EZ: IXMLTExistEZParcel_Composition_EZ;
begin
  Result := ChildNodes['Composition_EZ'] as IXMLTExistEZParcel_Composition_EZ;
end;

function TXMLTExistEZParcel.Get_Area_In_GKN: LongWord;
begin
  Result := ChildNodes['Area_In_GKN'].NodeValue;
end;

procedure TXMLTExistEZParcel.Set_Area_In_GKN(Value: LongWord);
begin
  ChildNodes['Area_In_GKN'].NodeValue := Value;
end;

function TXMLTExistEZParcel.Get_Delta_Area: LongWord;
begin
  Result := ChildNodes['Delta_Area'].NodeValue;
end;

procedure TXMLTExistEZParcel.Set_Delta_Area(Value: LongWord);
begin
  ChildNodes['Delta_Area'].NodeValue := Value;
end;

function TXMLTExistEZParcel.Get_Min_Area: IXMLTArea_without_Innccuracy;
begin
  Result := ChildNodes['Min_Area'] as IXMLTArea_without_Innccuracy;
end;

function TXMLTExistEZParcel.Get_Max_Area: IXMLTArea_without_Innccuracy;
begin
  Result := ChildNodes['Max_Area'] as IXMLTArea_without_Innccuracy;
end;

function TXMLTExistEZParcel.Get_Note: UnicodeString;
begin
  Result := ChildNodes['Note'].Text;
end;

procedure TXMLTExistEZParcel.Set_Note(Value: UnicodeString);
begin
  ChildNodes['Note'].NodeValue := Value;
end;

function TXMLTExistEZParcel.Get_RelatedParcels: IXMLRelatedParcels;
begin
  Result := ChildNodes['RelatedParcels'] as IXMLRelatedParcels;
end;

{ TXMLTExistEZParcel_Inner_CadastralNumbers }

procedure TXMLTExistEZParcel_Inner_CadastralNumbers.AfterConstruction;
begin
  FCadastralNumber := CreateCollection(TXMLTExistEZParcel_Inner_CadastralNumbers_CadastralNumberList, IXMLNode, 'CadastralNumber') as IXMLTExistEZParcel_Inner_CadastralNumbers_CadastralNumberList;
  FNumber := CreateCollection(TXMLString_List, IXMLNode, 'Number') as IXMLString_List;
  inherited;
end;

function TXMLTExistEZParcel_Inner_CadastralNumbers.Get_CadastralNumber: IXMLTExistEZParcel_Inner_CadastralNumbers_CadastralNumberList;
begin
  Result := FCadastralNumber;
end;

function TXMLTExistEZParcel_Inner_CadastralNumbers.Get_Number: IXMLString_List;
begin
  Result := FNumber;
end;

{ TXMLTExistEZParcel_SubParcels }

procedure TXMLTExistEZParcel_SubParcels.AfterConstruction;
begin
  RegisterChildNode('FormSubParcel', TXMLTExistEZParcel_SubParcels_FormSubParcel);
  RegisterChildNode('ExistSubParcel', TXMLTExistEZParcel_SubParcels_ExistSubParcel);
  RegisterChildNode('InvariableSubParcel', TXMLTExistEZParcel_SubParcels_InvariableSubParcel);
  FFormSubParcel := CreateCollection(TXMLTExistEZParcel_SubParcels_FormSubParcelList, IXMLTExistEZParcel_SubParcels_FormSubParcel, 'FormSubParcel') as IXMLTExistEZParcel_SubParcels_FormSubParcelList;
  FExistSubParcel := CreateCollection(TXMLTExistEZParcel_SubParcels_ExistSubParcelList, IXMLTExistEZParcel_SubParcels_ExistSubParcel, 'ExistSubParcel') as IXMLTExistEZParcel_SubParcels_ExistSubParcelList;
  FInvariableSubParcel := CreateCollection(TXMLTExistEZParcel_SubParcels_InvariableSubParcelList, IXMLTExistEZParcel_SubParcels_InvariableSubParcel, 'InvariableSubParcel') as IXMLTExistEZParcel_SubParcels_InvariableSubParcelList;
  inherited;
end;

function TXMLTExistEZParcel_SubParcels.Get_FormSubParcel: IXMLTExistEZParcel_SubParcels_FormSubParcelList;
begin
  Result := FFormSubParcel;
end;

function TXMLTExistEZParcel_SubParcels.Get_ExistSubParcel: IXMLTExistEZParcel_SubParcels_ExistSubParcelList;
begin
  Result := FExistSubParcel;
end;

function TXMLTExistEZParcel_SubParcels.Get_InvariableSubParcel: IXMLTExistEZParcel_SubParcels_InvariableSubParcelList;
begin
  Result := FInvariableSubParcel;
end;

{ TXMLTExistEZParcel_SubParcels_FormSubParcel }

function TXMLTExistEZParcel_SubParcels_FormSubParcel.Get_Definition: UnicodeString;
begin
  Result := AttributeNodes['Definition'].Text;
end;

procedure TXMLTExistEZParcel_SubParcels_FormSubParcel.Set_Definition(Value: UnicodeString);
begin
  SetAttribute('Definition', Value);
end;

{ TXMLTExistEZParcel_SubParcels_FormSubParcelList }

function TXMLTExistEZParcel_SubParcels_FormSubParcelList.Add: IXMLTExistEZParcel_SubParcels_FormSubParcel;
begin
  Result := AddItem(-1) as IXMLTExistEZParcel_SubParcels_FormSubParcel;
end;

function TXMLTExistEZParcel_SubParcels_FormSubParcelList.Insert(const Index: Integer): IXMLTExistEZParcel_SubParcels_FormSubParcel;
begin
  Result := AddItem(Index) as IXMLTExistEZParcel_SubParcels_FormSubParcel;
end;

function TXMLTExistEZParcel_SubParcels_FormSubParcelList.Get_Item(Index: Integer): IXMLTExistEZParcel_SubParcels_FormSubParcel;
begin
  Result := List[Index] as IXMLTExistEZParcel_SubParcels_FormSubParcel;
end;

{ TXMLTExistEZParcel_SubParcels_ExistSubParcel }

function TXMLTExistEZParcel_SubParcels_ExistSubParcel.Get_Number_Record: LongWord;
begin
  Result := AttributeNodes['Number_Record'].NodeValue;
end;

procedure TXMLTExistEZParcel_SubParcels_ExistSubParcel.Set_Number_Record(Value: LongWord);
begin
  SetAttribute('Number_Record', Value);
end;

{ TXMLTExistEZParcel_SubParcels_ExistSubParcelList }

function TXMLTExistEZParcel_SubParcels_ExistSubParcelList.Add: IXMLTExistEZParcel_SubParcels_ExistSubParcel;
begin
  Result := AddItem(-1) as IXMLTExistEZParcel_SubParcels_ExistSubParcel;
end;

function TXMLTExistEZParcel_SubParcels_ExistSubParcelList.Insert(const Index: Integer): IXMLTExistEZParcel_SubParcels_ExistSubParcel;
begin
  Result := AddItem(Index) as IXMLTExistEZParcel_SubParcels_ExistSubParcel;
end;

function TXMLTExistEZParcel_SubParcels_ExistSubParcelList.Get_Item(Index: Integer): IXMLTExistEZParcel_SubParcels_ExistSubParcel;
begin
  Result := List[Index] as IXMLTExistEZParcel_SubParcels_ExistSubParcel;
end;

{ TXMLTExistEZParcel_SubParcels_InvariableSubParcel }

procedure TXMLTExistEZParcel_SubParcels_InvariableSubParcel.AfterConstruction;
begin
  RegisterChildNode('Area', TXMLTExistEZParcel_SubParcels_InvariableSubParcel_Area);
  RegisterChildNode('Encumbrance', TXMLTEncumbrance);
  RegisterChildNode('Contours', TXMLTExistEZParcel_SubParcels_InvariableSubParcel_Contours);
  inherited;
end;

function TXMLTExistEZParcel_SubParcels_InvariableSubParcel.Get_Number_Record: LongWord;
begin
  Result := AttributeNodes['Number_Record'].NodeValue;
end;

procedure TXMLTExistEZParcel_SubParcels_InvariableSubParcel.Set_Number_Record(Value: LongWord);
begin
  SetAttribute('Number_Record', Value);
end;

function TXMLTExistEZParcel_SubParcels_InvariableSubParcel.Get_SubParcel_Realty: Boolean;
begin
  Result := AttributeNodes['SubParcel_Realty'].NodeValue;
end;

procedure TXMLTExistEZParcel_SubParcels_InvariableSubParcel.Set_SubParcel_Realty(Value: Boolean);
begin
  SetAttribute('SubParcel_Realty', Value);
end;

function TXMLTExistEZParcel_SubParcels_InvariableSubParcel.Get_Area: IXMLTExistEZParcel_SubParcels_InvariableSubParcel_Area;
begin
  Result := ChildNodes['Area'] as IXMLTExistEZParcel_SubParcels_InvariableSubParcel_Area;
end;

function TXMLTExistEZParcel_SubParcels_InvariableSubParcel.Get_Encumbrance: IXMLTEncumbrance;
begin
  Result := ChildNodes['Encumbrance'] as IXMLTEncumbrance;
end;

function TXMLTExistEZParcel_SubParcels_InvariableSubParcel.Get_Contours: IXMLTExistEZParcel_SubParcels_InvariableSubParcel_Contours;
begin
  Result := ChildNodes['Contours'] as IXMLTExistEZParcel_SubParcels_InvariableSubParcel_Contours;
end;

{ TXMLTExistEZParcel_SubParcels_InvariableSubParcelList }

function TXMLTExistEZParcel_SubParcels_InvariableSubParcelList.Add: IXMLTExistEZParcel_SubParcels_InvariableSubParcel;
begin
  Result := AddItem(-1) as IXMLTExistEZParcel_SubParcels_InvariableSubParcel;
end;

function TXMLTExistEZParcel_SubParcels_InvariableSubParcelList.Insert(const Index: Integer): IXMLTExistEZParcel_SubParcels_InvariableSubParcel;
begin
  Result := AddItem(Index) as IXMLTExistEZParcel_SubParcels_InvariableSubParcel;
end;

function TXMLTExistEZParcel_SubParcels_InvariableSubParcelList.Get_Item(Index: Integer): IXMLTExistEZParcel_SubParcels_InvariableSubParcel;
begin
  Result := List[Index] as IXMLTExistEZParcel_SubParcels_InvariableSubParcel;
end;

{ TXMLTExistEZParcel_SubParcels_InvariableSubParcel_Area }

function TXMLTExistEZParcel_SubParcels_InvariableSubParcel_Area.Get_Area: LongWord;
begin
  Result := ChildNodes['Area'].NodeValue;
end;

procedure TXMLTExistEZParcel_SubParcels_InvariableSubParcel_Area.Set_Area(Value: LongWord);
begin
  ChildNodes['Area'].NodeValue := Value;
end;

function TXMLTExistEZParcel_SubParcels_InvariableSubParcel_Area.Get_Unit_: UnicodeString;
begin
  Result := ChildNodes['Unit'].Text;
end;

procedure TXMLTExistEZParcel_SubParcels_InvariableSubParcel_Area.Set_Unit_(Value: UnicodeString);
begin
  ChildNodes['Unit'].NodeValue := Value;
end;

{ TXMLTExistEZParcel_SubParcels_InvariableSubParcel_Contours }

procedure TXMLTExistEZParcel_SubParcels_InvariableSubParcel_Contours.AfterConstruction;
begin
  RegisterChildNode('Contour', TXMLTExistEZParcel_SubParcels_InvariableSubParcel_Contours_Contour);
  ItemTag := 'Contour';
  ItemInterface := IXMLTExistEZParcel_SubParcels_InvariableSubParcel_Contours_Contour;
  inherited;
end;

function TXMLTExistEZParcel_SubParcels_InvariableSubParcel_Contours.Get_Contour(Index: Integer): IXMLTExistEZParcel_SubParcels_InvariableSubParcel_Contours_Contour;
begin
  Result := List[Index] as IXMLTExistEZParcel_SubParcels_InvariableSubParcel_Contours_Contour;
end;

function TXMLTExistEZParcel_SubParcels_InvariableSubParcel_Contours.Add: IXMLTExistEZParcel_SubParcels_InvariableSubParcel_Contours_Contour;
begin
  Result := AddItem(-1) as IXMLTExistEZParcel_SubParcels_InvariableSubParcel_Contours_Contour;
end;

function TXMLTExistEZParcel_SubParcels_InvariableSubParcel_Contours.Insert(const Index: Integer): IXMLTExistEZParcel_SubParcels_InvariableSubParcel_Contours_Contour;
begin
  Result := AddItem(Index) as IXMLTExistEZParcel_SubParcels_InvariableSubParcel_Contours_Contour;
end;

{ TXMLTExistEZParcel_SubParcels_InvariableSubParcel_Contours_Contour }

procedure TXMLTExistEZParcel_SubParcels_InvariableSubParcel_Contours_Contour.AfterConstruction;
begin
  RegisterChildNode('Area', TXMLTArea_Contour);
  inherited;
end;

function TXMLTExistEZParcel_SubParcels_InvariableSubParcel_Contours_Contour.Get_Number: UnicodeString;
begin
  Result := AttributeNodes['Number'].Text;
end;

procedure TXMLTExistEZParcel_SubParcels_InvariableSubParcel_Contours_Contour.Set_Number(Value: UnicodeString);
begin
  SetAttribute('Number', Value);
end;

function TXMLTExistEZParcel_SubParcels_InvariableSubParcel_Contours_Contour.Get_Area: IXMLTArea_Contour;
begin
  Result := ChildNodes['Area'] as IXMLTArea_Contour;
end;

{ TXMLTExistEZParcel_Composition_EZ }

procedure TXMLTExistEZParcel_Composition_EZ.AfterConstruction;
begin
  RegisterChildNode('InsertEntryParcels', TXMLTExistEZParcel_Composition_EZ_InsertEntryParcels);
  RegisterChildNode('DeleteEntryParcels', TXMLTExistEZParcel_Composition_EZ_DeleteEntryParcels);
  inherited;
end;

function TXMLTExistEZParcel_Composition_EZ.Get_InsertEntryParcels: IXMLTExistEZParcel_Composition_EZ_InsertEntryParcels;
begin
  Result := ChildNodes['InsertEntryParcels'] as IXMLTExistEZParcel_Composition_EZ_InsertEntryParcels;
end;

function TXMLTExistEZParcel_Composition_EZ.Get_DeleteEntryParcels: IXMLTExistEZParcel_Composition_EZ_DeleteEntryParcels;
begin
  Result := ChildNodes['DeleteEntryParcels'] as IXMLTExistEZParcel_Composition_EZ_DeleteEntryParcels;
end;

{ TXMLTExistEZParcel_Composition_EZ_InsertEntryParcels }

procedure TXMLTExistEZParcel_Composition_EZ_InsertEntryParcels.AfterConstruction;
begin
  RegisterChildNode('InsertEntryParcel', TXMLTExistEZParcel_Composition_EZ_InsertEntryParcels_InsertEntryParcel);
  ItemTag := 'InsertEntryParcel';
  ItemInterface := IXMLTExistEZParcel_Composition_EZ_InsertEntryParcels_InsertEntryParcel;
  inherited;
end;

function TXMLTExistEZParcel_Composition_EZ_InsertEntryParcels.Get_InsertEntryParcel(Index: Integer): IXMLTExistEZParcel_Composition_EZ_InsertEntryParcels_InsertEntryParcel;
begin
  Result := List[Index] as IXMLTExistEZParcel_Composition_EZ_InsertEntryParcels_InsertEntryParcel;
end;

function TXMLTExistEZParcel_Composition_EZ_InsertEntryParcels.Add: IXMLTExistEZParcel_Composition_EZ_InsertEntryParcels_InsertEntryParcel;
begin
  Result := AddItem(-1) as IXMLTExistEZParcel_Composition_EZ_InsertEntryParcels_InsertEntryParcel;
end;

function TXMLTExistEZParcel_Composition_EZ_InsertEntryParcels.Insert(const Index: Integer): IXMLTExistEZParcel_Composition_EZ_InsertEntryParcels_InsertEntryParcel;
begin
  Result := AddItem(Index) as IXMLTExistEZParcel_Composition_EZ_InsertEntryParcels_InsertEntryParcel;
end;

{ TXMLTExistEZParcel_Composition_EZ_InsertEntryParcels_InsertEntryParcel }

procedure TXMLTExistEZParcel_Composition_EZ_InsertEntryParcels_InsertEntryParcel.AfterConstruction;
begin
  RegisterChildNode('ExistEntryParcel', TXMLTExistEZParcel_Composition_EZ_InsertEntryParcels_InsertEntryParcel_ExistEntryParcel);
  RegisterChildNode('NewEntryParcel', TXMLTNewEZEntryParcel);
  inherited;
end;

function TXMLTExistEZParcel_Composition_EZ_InsertEntryParcels_InsertEntryParcel.Get_ExistEntryParcel: IXMLTExistEZParcel_Composition_EZ_InsertEntryParcels_InsertEntryParcel_ExistEntryParcel;
begin
  Result := ChildNodes['ExistEntryParcel'] as IXMLTExistEZParcel_Composition_EZ_InsertEntryParcels_InsertEntryParcel_ExistEntryParcel;
end;

function TXMLTExistEZParcel_Composition_EZ_InsertEntryParcels_InsertEntryParcel.Get_NewEntryParcel: IXMLTNewEZEntryParcel;
begin
  Result := ChildNodes['NewEntryParcel'] as IXMLTNewEZEntryParcel;
end;

{ TXMLTExistEZParcel_Composition_EZ_InsertEntryParcels_InsertEntryParcel_ExistEntryParcel }

function TXMLTExistEZParcel_Composition_EZ_InsertEntryParcels_InsertEntryParcel_ExistEntryParcel.Get_CadastralNumber: UnicodeString;
begin
  Result := AttributeNodes['CadastralNumber'].Text;
end;

procedure TXMLTExistEZParcel_Composition_EZ_InsertEntryParcels_InsertEntryParcel_ExistEntryParcel.Set_CadastralNumber(Value: UnicodeString);
begin
  SetAttribute('CadastralNumber', Value);
end;

{ TXMLTNewEZEntryParcel }

procedure TXMLTNewEZEntryParcel.AfterConstruction;
begin
  RegisterChildNode('Area', TXMLTAreaNew);
  RegisterChildNode('Entity_Spatial', TXMLEntity_Spatial);
  RegisterChildNode('Encumbrance', TXMLTEncumbrance);
  inherited;
end;

function TXMLTNewEZEntryParcel.Get_Name: UnicodeString;
begin
  Result := AttributeNodes['Name'].Text;
end;

procedure TXMLTNewEZEntryParcel.Set_Name(Value: UnicodeString);
begin
  SetAttribute('Name', Value);
end;

function TXMLTNewEZEntryParcel.Get_Definition: UnicodeString;
begin
  Result := AttributeNodes['Definition'].Text;
end;

procedure TXMLTNewEZEntryParcel.Set_Definition(Value: UnicodeString);
begin
  SetAttribute('Definition', Value);
end;

function TXMLTNewEZEntryParcel.Get_Area: IXMLTAreaNew;
begin
  Result := ChildNodes['Area'] as IXMLTAreaNew;
end;

function TXMLTNewEZEntryParcel.Get_Entity_Spatial: IXMLEntity_Spatial;
begin
  Result := ChildNodes['Entity_Spatial'] as IXMLEntity_Spatial;
end;

function TXMLTNewEZEntryParcel.Get_Encumbrance: IXMLTEncumbrance;
begin
  Result := ChildNodes['Encumbrance'] as IXMLTEncumbrance;
end;

{ TXMLTExistEZParcel_Composition_EZ_DeleteEntryParcels }

procedure TXMLTExistEZParcel_Composition_EZ_DeleteEntryParcels.AfterConstruction;
begin
  RegisterChildNode('DeleteEntryParcel', TXMLTExistEZParcel_Composition_EZ_DeleteEntryParcels_DeleteEntryParcel);
  ItemTag := 'DeleteEntryParcel';
  ItemInterface := IXMLTExistEZParcel_Composition_EZ_DeleteEntryParcels_DeleteEntryParcel;
  inherited;
end;

function TXMLTExistEZParcel_Composition_EZ_DeleteEntryParcels.Get_DeleteEntryParcel(Index: Integer): IXMLTExistEZParcel_Composition_EZ_DeleteEntryParcels_DeleteEntryParcel;
begin
  Result := List[Index] as IXMLTExistEZParcel_Composition_EZ_DeleteEntryParcels_DeleteEntryParcel;
end;

function TXMLTExistEZParcel_Composition_EZ_DeleteEntryParcels.Add: IXMLTExistEZParcel_Composition_EZ_DeleteEntryParcels_DeleteEntryParcel;
begin
  Result := AddItem(-1) as IXMLTExistEZParcel_Composition_EZ_DeleteEntryParcels_DeleteEntryParcel;
end;

function TXMLTExistEZParcel_Composition_EZ_DeleteEntryParcels.Insert(const Index: Integer): IXMLTExistEZParcel_Composition_EZ_DeleteEntryParcels_DeleteEntryParcel;
begin
  Result := AddItem(Index) as IXMLTExistEZParcel_Composition_EZ_DeleteEntryParcels_DeleteEntryParcel;
end;

{ TXMLTExistEZParcel_Composition_EZ_DeleteEntryParcels_DeleteEntryParcel }

function TXMLTExistEZParcel_Composition_EZ_DeleteEntryParcels_DeleteEntryParcel.Get_CadastralNumber: UnicodeString;
begin
  Result := AttributeNodes['CadastralNumber'].Text;
end;

procedure TXMLTExistEZParcel_Composition_EZ_DeleteEntryParcels_DeleteEntryParcel.Set_CadastralNumber(Value: UnicodeString);
begin
  SetAttribute('CadastralNumber', Value);
end;

{ TXMLSTD_MP_Package_SpecifyParcel_ExistEZ_ExistEZEntryParcels }

procedure TXMLSTD_MP_Package_SpecifyParcel_ExistEZ_ExistEZEntryParcels.AfterConstruction;
begin
  RegisterChildNode('ExistEZEntryParcel', TXMLTExistEZEntryParcel);
  ItemTag := 'ExistEZEntryParcel';
  ItemInterface := IXMLTExistEZEntryParcel;
  inherited;
end;

function TXMLSTD_MP_Package_SpecifyParcel_ExistEZ_ExistEZEntryParcels.Get_ExistEZEntryParcel(Index: Integer): IXMLTExistEZEntryParcel;
begin
  Result := List[Index] as IXMLTExistEZEntryParcel;
end;

function TXMLSTD_MP_Package_SpecifyParcel_ExistEZ_ExistEZEntryParcels.Add: IXMLTExistEZEntryParcel;
begin
  Result := AddItem(-1) as IXMLTExistEZEntryParcel;
end;

function TXMLSTD_MP_Package_SpecifyParcel_ExistEZ_ExistEZEntryParcels.Insert(const Index: Integer): IXMLTExistEZEntryParcel;
begin
  Result := AddItem(Index) as IXMLTExistEZEntryParcel;
end;

{ TXMLTExistEZEntryParcel }

procedure TXMLTExistEZEntryParcel.AfterConstruction;
begin
  RegisterChildNode('Area', TXMLTAreaNew);
  RegisterChildNode('Encumbrance', TXMLTEncumbrance);
  RegisterChildNode('Entity_Spatial', TXMLEntity_Spatial);
  inherited;
end;

function TXMLTExistEZEntryParcel.Get_CadastralNumber: UnicodeString;
begin
  Result := AttributeNodes['CadastralNumber'].Text;
end;

procedure TXMLTExistEZEntryParcel.Set_CadastralNumber(Value: UnicodeString);
begin
  SetAttribute('CadastralNumber', Value);
end;

function TXMLTExistEZEntryParcel.Get_Area: IXMLTAreaNew;
begin
  Result := ChildNodes['Area'] as IXMLTAreaNew;
end;

function TXMLTExistEZEntryParcel.Get_Area_In_GKN: LongWord;
begin
  Result := ChildNodes['Area_In_GKN'].NodeValue;
end;

procedure TXMLTExistEZEntryParcel.Set_Area_In_GKN(Value: LongWord);
begin
  ChildNodes['Area_In_GKN'].NodeValue := Value;
end;

function TXMLTExistEZEntryParcel.Get_Encumbrance: IXMLTEncumbrance;
begin
  Result := ChildNodes['Encumbrance'] as IXMLTEncumbrance;
end;

function TXMLTExistEZEntryParcel.Get_Entity_Spatial: IXMLEntity_Spatial;
begin
  Result := ChildNodes['Entity_Spatial'] as IXMLEntity_Spatial;
end;

function TXMLTExistEZEntryParcel.Get_Note: UnicodeString;
begin
  Result := ChildNodes['Note'].Text;
end;

procedure TXMLTExistEZEntryParcel.Set_Note(Value: UnicodeString);
begin
  ChildNodes['Note'].NodeValue := Value;
end;

{ TXMLSTD_MP_Package_SpecifyParcel_SpecifyRelatedParcel }

{ TXMLSTD_MP_Package_SpecifyParcel_SpecifyRelatedParcelList }

function TXMLSTD_MP_Package_SpecifyParcel_SpecifyRelatedParcelList.Add: IXMLTSpecifyRelatedParcel;
begin
  Result := AddItem(-1) as IXMLSTD_MP_Package_SpecifyParcel_SpecifyRelatedParcel;
end;

function TXMLSTD_MP_Package_SpecifyParcel_SpecifyRelatedParcelList.Insert(const Index: Integer): IXMLTSpecifyRelatedParcel;
begin
  Result := AddItem(Index) as IXMLTSpecifyRelatedParcel;
end;

function TXMLSTD_MP_Package_SpecifyParcel_SpecifyRelatedParcelList.Get_Item(Index: Integer): IXMLTSpecifyRelatedParcel;
begin
  Result := List[Index] as IXMLTSpecifyRelatedParcel;
end;

{ TXMLTNewSubParcel }

procedure TXMLTNewSubParcel.AfterConstruction;
begin
  RegisterChildNode('Area', TXMLTArea);
  RegisterChildNode('Encumbrance', TXMLTEncumbrance);
  {$IFNDEF disableIXMLTNewSubParcel_Entity_Spatial}
  RegisterChildNode('Entity_Spatial', TXMLTNewSubParcel_Entity_Spatial);
  {$ELSE}
  RegisterChildNode('Entity_Spatial', TXMLEntity_Spatial);
  {$ENDIF}
  {$IFNDEF disableIXMLTNewSubParcel_Contours}
  RegisterChildNode('Contours', TXMLTNewSubParcel_Contours);
  {$ELSE}
  RegisterChildNode('Contours',TXMLTSubParcel_Contours);
  {$ENDIF}
  inherited;
end;

function TXMLTNewSubParcel.Get_Definition: UnicodeString;
begin
  Result := AttributeNodes['Definition'].Text;
end;

procedure TXMLTNewSubParcel.Set_Definition(Value: UnicodeString);
begin
  SetAttribute('Definition', Value);
end;

function TXMLTNewSubParcel.Get_SubParcel_Realty: Boolean;
begin
  Result := AttributeNodes['SubParcel_Realty'].NodeValue;
end;

procedure TXMLTNewSubParcel.Set_SubParcel_Realty(Value: Boolean);
begin
  SetAttribute('SubParcel_Realty', Value);
end;

function TXMLTNewSubParcel.Get_CadastralNumber_Parcel: UnicodeString;
begin
  Result := ChildNodes['CadastralNumber_Parcel'].Text;
end;

procedure TXMLTNewSubParcel.Set_CadastralNumber_Parcel(Value: UnicodeString);
begin
  ChildNodes['CadastralNumber_Parcel'].NodeValue := Value;
end;

function TXMLTNewSubParcel.Get_Area: IXMLTArea;
begin
  Result := ChildNodes['Area'] as IXMLTArea;
end;

function TXMLTNewSubParcel.Get_Encumbrance: IXMLTEncumbrance;
begin
  Result := ChildNodes['Encumbrance'] as IXMLTEncumbrance;
end;

{$IFNDEF  disableIXMLTNewSubParcel_Entity_Spatial}
function TXMLTNewSubParcel.Get_Entity_Spatial: IXMLTNewSubParcel_Entity_Spatial;
begin
  Result := ChildNodes['Entity_Spatial'] as IXMLTNewSubParcel_Entity_Spatial;
end;
{$ELSE}
function TXMLTNewSubParcel.Get_Entity_Spatial: IXMLEntity_Spatial;
begin
  Result := ChildNodes['Entity_Spatial'] as IXMLEntity_Spatial;
end;
{$ENDIF}

{$IFNDEF disableIXMLTNewSubParcel_Contours}
function TXMLTNewSubParcel.Get_Contours: IXMLTNewSubParcel_Contours;
begin
  Result := ChildNodes['Contours'] as IXMLTNewSubParcel_Contours;
end;
{$ELSE}
function TXMLTNewSubParcel.Get_Contours: IXMLTSubParcel_Contours;
begin
  Result := ChildNodes['Contours'] as IXMLTSubParcel_Contours;
end;
{$ENDIF}

{ TXMLTNewSubParcelList }

function TXMLTNewSubParcelList.Add: IXMLTNewSubParcel;
begin
  Result := AddItem(-1) as IXMLTNewSubParcel;
end;

function TXMLTNewSubParcelList.Insert(const Index: Integer): IXMLTNewSubParcel;
begin
  Result := AddItem(Index) as IXMLTNewSubParcel;
end;

function TXMLTNewSubParcelList.Get_Item(Index: Integer): IXMLTNewSubParcel;
begin
  Result := List[Index] as IXMLTNewSubParcel;
end;

{$IFNDEF disableIXMLTNewSubParcel_Entity_Spatial}
{ TXMLTNewSubParcel_Entity_Spatial }

procedure TXMLTNewSubParcel_Entity_Spatial.AfterConstruction;
begin
  RegisterChildNode('Spatial_Element', TXMLTSPATIAL_ELEMENT_OLD_NEW);
  ItemTag := 'Spatial_Element';
  ItemInterface := IXMLTSPATIAL_ELEMENT;
  inherited;
end;

function TXMLTNewSubParcel_Entity_Spatial.Get_Ent_Sys: UnicodeString;
begin
  Result := AttributeNodes['Ent_Sys'].Text;
end;

procedure TXMLTNewSubParcel_Entity_Spatial.Set_Ent_Sys(Value: UnicodeString);
begin
  SetAttribute('Ent_Sys', Value);
end;

function TXMLTNewSubParcel_Entity_Spatial.Get_Spatial_Element(Index: Integer): IXMLTSPATIAL_ELEMENT;
begin
  Result := List[Index] as IXMLTSPATIAL_ELEMENT;
end;

function TXMLTNewSubParcel_Entity_Spatial.Add: IXMLTSPATIAL_ELEMENT;
begin
  Result := AddItem(-1) as IXMLTSPATIAL_ELEMENT;
end;

function TXMLTNewSubParcel_Entity_Spatial.Insert(const Index: Integer): IXMLTSPATIAL_ELEMENT;
begin
  Result := AddItem(Index) as IXMLTSPATIAL_ELEMENT;
end;

{$ENDIF}
{$IFNDEF disableIXMLTNewSubParcel_Contours}
{ TXMLTNewSubParcel_Contours }

procedure TXMLTNewSubParcel_Contours.AfterConstruction;
begin
  RegisterChildNode('Contour', TXMLTNewSubParcel_Contours_Contour);
  ItemTag := 'Contour';
  ItemInterface := IXMLTNewSubParcel_Contours_Contour;
  inherited;
end;

function TXMLTNewSubParcel_Contours.Get_Contour(Index: Integer): IXMLTNewSubParcel_Contours_Contour;
begin
  Result := List[Index] as IXMLTNewSubParcel_Contours_Contour;
end;

function TXMLTNewSubParcel_Contours.Add: IXMLTNewSubParcel_Contours_Contour;
begin
  Result := AddItem(-1) as IXMLTNewSubParcel_Contours_Contour;
end;

function TXMLTNewSubParcel_Contours.Insert(const Index: Integer): IXMLTNewSubParcel_Contours_Contour;
begin
  Result := AddItem(Index) as IXMLTNewSubParcel_Contours_Contour;
end;
{$ENDIF}
{ TXMLTNewSubParcel_Contours_Contour }

procedure TXMLTNewSubParcel_Contours_Contour.AfterConstruction;
begin
  RegisterChildNode('Area', TXMLTArea_Contour);
  RegisterChildNode('Entity_Spatial', TXMLTNewSubParcel_Contours_Contour_Entity_Spatial);
  inherited;
end;

function TXMLTNewSubParcel_Contours_Contour.Get_Number: UnicodeString;
begin
  Result := AttributeNodes['Number'].Text;
end;

procedure TXMLTNewSubParcel_Contours_Contour.Set_Number(Value: UnicodeString);
begin
  SetAttribute('Number', Value);
end;

function TXMLTNewSubParcel_Contours_Contour.Get_Area: IXMLTArea_Contour;
begin
  Result := ChildNodes['Area'] as IXMLTArea_Contour;
end;

function TXMLTNewSubParcel_Contours_Contour.Get_Entity_Spatial: IXMLTNewSubParcel_Contours_Contour_Entity_Spatial;
begin
  Result := ChildNodes['Entity_Spatial'] as IXMLTNewSubParcel_Contours_Contour_Entity_Spatial;
end;

{ TXMLTNewSubParcel_Contours_Contour_Entity_Spatial }

procedure TXMLTNewSubParcel_Contours_Contour_Entity_Spatial.AfterConstruction;
begin
  RegisterChildNode('Spatial_Element', TXMLTSPATIAL_ELEMENT_OLD_NEW);
  ItemTag := 'Spatial_Element';
  ItemInterface := IXMLTSPATIAL_ELEMENT;
  inherited;
end;

function TXMLTNewSubParcel_Contours_Contour_Entity_Spatial.Get_Ent_Sys: UnicodeString;
begin
  Result := AttributeNodes['Ent_Sys'].Text;
end;

procedure TXMLTNewSubParcel_Contours_Contour_Entity_Spatial.Set_Ent_Sys(Value: UnicodeString);
begin
  SetAttribute('Ent_Sys', Value);
end;

function TXMLTNewSubParcel_Contours_Contour_Entity_Spatial.Get_Spatial_Element(Index: Integer): IXMLTSPATIAL_ELEMENT;
begin
  Result := List[Index] as IXMLTSPATIAL_ELEMENT;
end;

function TXMLTNewSubParcel_Contours_Contour_Entity_Spatial.Add: IXMLTSPATIAL_ELEMENT;
begin
  Result := AddItem(-1) as IXMLTSPATIAL_ELEMENT;
end;

function TXMLTNewSubParcel_Contours_Contour_Entity_Spatial.Insert(const Index: Integer): IXMLTSPATIAL_ELEMENT;
begin
  Result := AddItem(Index) as IXMLTSPATIAL_ELEMENT;
end;

{ TXMLSTD_MP_Package_SpecifyParcelsApproximal }

procedure TXMLSTD_MP_Package_SpecifyParcelsApproximal.AfterConstruction;
begin
  RegisterChildNode('ExistParcel', TXMLTExistParcel);
  RegisterChildNode('ExistEZ', TXMLSTD_MP_Package_SpecifyParcelsApproximal_ExistEZ);
  inherited;
end;

function TXMLSTD_MP_Package_SpecifyParcelsApproximal.Get_ExistParcel: IXMLTExistParcel;
begin
  Result := ChildNodes['ExistParcel'] as IXMLTExistParcel;
end;

function TXMLSTD_MP_Package_SpecifyParcelsApproximal.Get_ExistEZ: IXMLSTD_MP_Package_SpecifyParcelsApproximal_ExistEZ;
begin
  Result := ChildNodes['ExistEZ'] as IXMLSTD_MP_Package_SpecifyParcelsApproximal_ExistEZ;
end;

{ TXMLSTD_MP_Package_SpecifyParcelsApproximalList }

function TXMLSTD_MP_Package_SpecifyParcelsApproximalList.Add: IXMLSTD_MP_Package_SpecifyParcelsApproximal;
begin
  Result := AddItem(-1) as IXMLSTD_MP_Package_SpecifyParcelsApproximal;
end;

function TXMLSTD_MP_Package_SpecifyParcelsApproximalList.Insert(const Index: Integer): IXMLSTD_MP_Package_SpecifyParcelsApproximal;
begin
  Result := AddItem(Index) as IXMLSTD_MP_Package_SpecifyParcelsApproximal;
end;

function TXMLSTD_MP_Package_SpecifyParcelsApproximalList.Get_Item(Index: Integer): IXMLSTD_MP_Package_SpecifyParcelsApproximal;
begin
  Result := List[Index] as IXMLSTD_MP_Package_SpecifyParcelsApproximal;
end;

{ TXMLSTD_MP_Package_SpecifyParcelsApproximal_ExistEZ }

procedure TXMLSTD_MP_Package_SpecifyParcelsApproximal_ExistEZ.AfterConstruction;
begin
  RegisterChildNode('ExistEZParcels', TXMLTExistEZParcel);
  RegisterChildNode('ExistEZEntryParcels', TXMLSTD_MP_Package_SpecifyParcelsApproximal_ExistEZ_ExistEZEntryParcels);
  FExistEZEntryParcels := CreateCollection(TXMLSTD_MP_Package_SpecifyParcelsApproximal_ExistEZ_ExistEZEntryParcelsList, IXMLSTD_MP_Package_SpecifyParcelsApproximal_ExistEZ_ExistEZEntryParcels, 'ExistEZEntryParcels') as IXMLSTD_MP_Package_SpecifyParcelsApproximal_ExistEZ_ExistEZEntryParcelsList;
  inherited;
end;

function TXMLSTD_MP_Package_SpecifyParcelsApproximal_ExistEZ.Get_ExistEZParcels: IXMLTExistEZParcel;
begin
  Result := ChildNodes['ExistEZParcels'] as IXMLTExistEZParcel;
end;

function TXMLSTD_MP_Package_SpecifyParcelsApproximal_ExistEZ.Get_ExistEZEntryParcels: IXMLSTD_MP_Package_SpecifyParcelsApproximal_ExistEZ_ExistEZEntryParcelsList;
begin
  Result := FExistEZEntryParcels;
end;

{ TXMLSTD_MP_Package_SpecifyParcelsApproximal_ExistEZ_ExistEZEntryParcels }

procedure TXMLSTD_MP_Package_SpecifyParcelsApproximal_ExistEZ_ExistEZEntryParcels.AfterConstruction;
begin
  RegisterChildNode('ExistEZEntryParcel', TXMLTExistEZEntryParcel);
  inherited;
end;

function TXMLSTD_MP_Package_SpecifyParcelsApproximal_ExistEZ_ExistEZEntryParcels.Get_ExistEZEntryParcel: IXMLTExistEZEntryParcel;
begin
  Result := ChildNodes['ExistEZEntryParcel'] as IXMLTExistEZEntryParcel;
end;

{ TXMLSTD_MP_Package_SpecifyParcelsApproximal_ExistEZ_ExistEZEntryParcelsList }

function TXMLSTD_MP_Package_SpecifyParcelsApproximal_ExistEZ_ExistEZEntryParcelsList.Add: IXMLSTD_MP_Package_SpecifyParcelsApproximal_ExistEZ_ExistEZEntryParcels;
begin
  Result := AddItem(-1) as IXMLSTD_MP_Package_SpecifyParcelsApproximal_ExistEZ_ExistEZEntryParcels;
end;

function TXMLSTD_MP_Package_SpecifyParcelsApproximal_ExistEZ_ExistEZEntryParcelsList.Insert(const Index: Integer): IXMLSTD_MP_Package_SpecifyParcelsApproximal_ExistEZ_ExistEZEntryParcels;
begin
  Result := AddItem(Index) as IXMLSTD_MP_Package_SpecifyParcelsApproximal_ExistEZ_ExistEZEntryParcels;
end;

function TXMLSTD_MP_Package_SpecifyParcelsApproximal_ExistEZ_ExistEZEntryParcelsList.Get_Item(Index: Integer): IXMLSTD_MP_Package_SpecifyParcelsApproximal_ExistEZ_ExistEZEntryParcels;
begin
  Result := List[Index] as IXMLSTD_MP_Package_SpecifyParcelsApproximal_ExistEZ_ExistEZEntryParcels;
end;

{ TXMLCoord_Systems }

procedure TXMLCoord_Systems.AfterConstruction;
begin
  RegisterChildNode('Coord_System', TXMLCoord_System);
  inherited;
end;

function TXMLCoord_Systems.Get_Coord_System: IXMLCoord_System;
begin
  Result := ChildNodes['Coord_System'] as IXMLCoord_System;
end;

{ TXMLCoord_System }

function TXMLCoord_System.Get_Name: UnicodeString;
begin
  Result := AttributeNodes['Name'].Text;
end;

procedure TXMLCoord_System.Set_Name(Value: UnicodeString);
begin
  SetAttribute('Name', Value);
end;

function TXMLCoord_System.Get_Cs_Id: UnicodeString;
begin
  Result := AttributeNodes['Cs_Id'].Text;
end;

procedure TXMLCoord_System.Set_Cs_Id(Value: UnicodeString);
begin
  SetAttribute('Cs_Id', Value);
end;

{ TXMLSTD_MP_Input_Data }

procedure TXMLSTD_MP_Input_Data.AfterConstruction;
begin
  RegisterChildNode('Documents', TXMLSTD_MP_Input_Data_Documents);
  RegisterChildNode('Geodesic_Bases', TXMLSTD_MP_Input_Data_Geodesic_Bases);
  RegisterChildNode('Means_Survey', TXMLSTD_MP_Input_Data_Means_Survey);
  RegisterChildNode('Realty', TXMLSTD_MP_Input_Data_Realty);
  RegisterChildNode('SubParcels', TXMLSTD_MP_Input_Data_SubParcels);
  inherited;
end;

function TXMLSTD_MP_Input_Data.Get_Documents: IXMLSTD_MP_Input_Data_Documents;
begin
  Result := ChildNodes['Documents'] as IXMLSTD_MP_Input_Data_Documents;
end;

function TXMLSTD_MP_Input_Data.Get_Geodesic_Bases: IXMLSTD_MP_Input_Data_Geodesic_Bases;
begin
  Result := ChildNodes['Geodesic_Bases'] as IXMLSTD_MP_Input_Data_Geodesic_Bases;
end;

function TXMLSTD_MP_Input_Data.Get_Means_Survey: IXMLSTD_MP_Input_Data_Means_Survey;
begin
  Result := ChildNodes['Means_Survey'] as IXMLSTD_MP_Input_Data_Means_Survey;
end;

function TXMLSTD_MP_Input_Data.Get_Realty: IXMLSTD_MP_Input_Data_Realty;
begin
  Result := ChildNodes['Realty'] as IXMLSTD_MP_Input_Data_Realty;
end;

function TXMLSTD_MP_Input_Data.Get_SubParcels: IXMLSTD_MP_Input_Data_SubParcels;
begin
  Result := ChildNodes['SubParcels'] as IXMLSTD_MP_Input_Data_SubParcels;
end;

{ TXMLSTD_MP_Input_Data_Documents }

procedure TXMLSTD_MP_Input_Data_Documents.AfterConstruction;
begin
  RegisterChildNode('Document', TXMLSTD_MP_Input_Data_Documents_Document);
  ItemTag := 'Document';
  ItemInterface := IXMLSTD_MP_Input_Data_Documents_Document;
  inherited;
end;

function TXMLSTD_MP_Input_Data_Documents.Get_Document(Index: Integer): IXMLSTD_MP_Input_Data_Documents_Document;
begin
  Result := List[Index] as IXMLSTD_MP_Input_Data_Documents_Document;
end;

function TXMLSTD_MP_Input_Data_Documents.Add: IXMLSTD_MP_Input_Data_Documents_Document;
begin
  Result := AddItem(-1) as IXMLSTD_MP_Input_Data_Documents_Document;
end;

function TXMLSTD_MP_Input_Data_Documents.Insert(const Index: Integer): IXMLSTD_MP_Input_Data_Documents_Document;
begin
  Result := AddItem(Index) as IXMLSTD_MP_Input_Data_Documents_Document;
end;

{ TXMLSTD_MP_Input_Data_Documents_Document }

function TXMLSTD_MP_Input_Data_Documents_Document.Get_Scale: UnicodeString;
begin
  Result := ChildNodes['Scale'].Text;
end;

procedure TXMLSTD_MP_Input_Data_Documents_Document.Set_Scale(Value: UnicodeString);
begin
  ChildNodes['Scale'].NodeValue := Value;
end;

function TXMLSTD_MP_Input_Data_Documents_Document.Get_Date_Create: UnicodeString;
begin
  Result := ChildNodes['Date_Create'].Text;
end;

procedure TXMLSTD_MP_Input_Data_Documents_Document.Set_Date_Create(Value: UnicodeString);
begin
  ChildNodes['Date_Create'].NodeValue := Value;
end;

function TXMLSTD_MP_Input_Data_Documents_Document.Get_Date_Update: UnicodeString;
begin
  Result := ChildNodes['Date_Update'].Text;
end;

procedure TXMLSTD_MP_Input_Data_Documents_Document.Set_Date_Update(Value: UnicodeString);
begin
  ChildNodes['Date_Update'].NodeValue := Value;
end;

{ TXMLSTD_MP_Input_Data_Geodesic_Bases }

procedure TXMLSTD_MP_Input_Data_Geodesic_Bases.AfterConstruction;
begin
  RegisterChildNode('Geodesic_Base', TXMLSTD_MP_Input_Data_Geodesic_Bases_Geodesic_Base);
  ItemTag := 'Geodesic_Base';
  ItemInterface := IXMLSTD_MP_Input_Data_Geodesic_Bases_Geodesic_Base;
  inherited;
end;

function TXMLSTD_MP_Input_Data_Geodesic_Bases.Get_Geodesic_Base(Index: Integer): IXMLSTD_MP_Input_Data_Geodesic_Bases_Geodesic_Base;
begin
  Result := List[Index] as IXMLSTD_MP_Input_Data_Geodesic_Bases_Geodesic_Base;
end;

function TXMLSTD_MP_Input_Data_Geodesic_Bases.Add: IXMLSTD_MP_Input_Data_Geodesic_Bases_Geodesic_Base;
begin
  Result := AddItem(-1) as IXMLSTD_MP_Input_Data_Geodesic_Bases_Geodesic_Base;
end;

function TXMLSTD_MP_Input_Data_Geodesic_Bases.Insert(const Index: Integer): IXMLSTD_MP_Input_Data_Geodesic_Bases_Geodesic_Base;
begin
  Result := AddItem(Index) as IXMLSTD_MP_Input_Data_Geodesic_Bases_Geodesic_Base;
end;

{ TXMLSTD_MP_Input_Data_Geodesic_Bases_Geodesic_Base }

function TXMLSTD_MP_Input_Data_Geodesic_Bases_Geodesic_Base.Get_PName: UnicodeString;
begin
  Result := ChildNodes['PName'].Text;
end;

procedure TXMLSTD_MP_Input_Data_Geodesic_Bases_Geodesic_Base.Set_PName(Value: UnicodeString);
begin
  ChildNodes['PName'].NodeValue := Value;
end;

function TXMLSTD_MP_Input_Data_Geodesic_Bases_Geodesic_Base.Get_PKind: UnicodeString;
begin
  Result := ChildNodes['PKind'].Text;
end;

procedure TXMLSTD_MP_Input_Data_Geodesic_Bases_Geodesic_Base.Set_PKind(Value: UnicodeString);
begin
  ChildNodes['PKind'].NodeValue := Value;
end;

function TXMLSTD_MP_Input_Data_Geodesic_Bases_Geodesic_Base.Get_PKlass: UnicodeString;
begin
  Result := ChildNodes['PKlass'].Text;
end;

procedure TXMLSTD_MP_Input_Data_Geodesic_Bases_Geodesic_Base.Set_PKlass(Value: UnicodeString);
begin
  ChildNodes['PKlass'].NodeValue := Value;
end;

function TXMLSTD_MP_Input_Data_Geodesic_Bases_Geodesic_Base.Get_OrdX: UnicodeString;
begin
  Result := ChildNodes['OrdX'].Text;
end;

procedure TXMLSTD_MP_Input_Data_Geodesic_Bases_Geodesic_Base.Set_OrdX(Value: UnicodeString);
begin
  ChildNodes['OrdX'].NodeValue := Value;
end;

function TXMLSTD_MP_Input_Data_Geodesic_Bases_Geodesic_Base.Get_OrdY: UnicodeString;
begin
  Result := ChildNodes['OrdY'].Text;
end;

procedure TXMLSTD_MP_Input_Data_Geodesic_Bases_Geodesic_Base.Set_OrdY(Value: UnicodeString);
begin
  ChildNodes['OrdY'].NodeValue := Value;
end;

{ TXMLSTD_MP_Input_Data_Means_Survey }

procedure TXMLSTD_MP_Input_Data_Means_Survey.AfterConstruction;
begin
  RegisterChildNode('Means_Survey', TXMLSTD_MP_Input_Data_Means_Survey_Means_Survey);
  ItemTag := 'Means_Survey';
  ItemInterface := IXMLSTD_MP_Input_Data_Means_Survey_Means_Survey;
  inherited;
end;

function TXMLSTD_MP_Input_Data_Means_Survey.Get_Means_Survey(Index: Integer): IXMLSTD_MP_Input_Data_Means_Survey_Means_Survey;
begin
  Result := List[Index] as IXMLSTD_MP_Input_Data_Means_Survey_Means_Survey;
end;

function TXMLSTD_MP_Input_Data_Means_Survey.Add: IXMLSTD_MP_Input_Data_Means_Survey_Means_Survey;
begin
  Result := AddItem(-1) as IXMLSTD_MP_Input_Data_Means_Survey_Means_Survey;
end;

function TXMLSTD_MP_Input_Data_Means_Survey.Insert(const Index: Integer): IXMLSTD_MP_Input_Data_Means_Survey_Means_Survey;
begin
  Result := AddItem(Index) as IXMLSTD_MP_Input_Data_Means_Survey_Means_Survey;
end;

{ TXMLSTD_MP_Input_Data_Means_Survey_Means_Survey }

function TXMLSTD_MP_Input_Data_Means_Survey_Means_Survey.Get_Name: UnicodeString;
begin
  Result := ChildNodes['Name'].Text;
end;

procedure TXMLSTD_MP_Input_Data_Means_Survey_Means_Survey.Set_Name(Value: UnicodeString);
begin
  ChildNodes['Name'].NodeValue := Value;
end;

function TXMLSTD_MP_Input_Data_Means_Survey_Means_Survey.Get_Certificate: UnicodeString;
begin
  Result := ChildNodes['Certificate'].Text;
end;

procedure TXMLSTD_MP_Input_Data_Means_Survey_Means_Survey.Set_Certificate(Value: UnicodeString);
begin
  ChildNodes['Certificate'].NodeValue := Value;
end;

function TXMLSTD_MP_Input_Data_Means_Survey_Means_Survey.Get_Certificate_Verification: UnicodeString;
begin
  Result := ChildNodes['Certificate_Verification'].Text;
end;

procedure TXMLSTD_MP_Input_Data_Means_Survey_Means_Survey.Set_Certificate_Verification(Value: UnicodeString);
begin
  ChildNodes['Certificate_Verification'].NodeValue := Value;
end;

{ TXMLSTD_MP_Input_Data_Realty }

procedure TXMLSTD_MP_Input_Data_Realty.AfterConstruction;
begin
  RegisterChildNode('OKS', TXMLSTD_MP_Input_Data_Realty_OKS);
  ItemTag := 'OKS';
  ItemInterface := IXMLSTD_MP_Input_Data_Realty_OKS;
  inherited;
end;

function TXMLSTD_MP_Input_Data_Realty.Get_OKS(Index: Integer): IXMLSTD_MP_Input_Data_Realty_OKS;
begin
  Result := List[Index] as IXMLSTD_MP_Input_Data_Realty_OKS;
end;

function TXMLSTD_MP_Input_Data_Realty.Add: IXMLSTD_MP_Input_Data_Realty_OKS;
begin
  Result := AddItem(-1) as IXMLSTD_MP_Input_Data_Realty_OKS;
end;

function TXMLSTD_MP_Input_Data_Realty.Insert(const Index: Integer): IXMLSTD_MP_Input_Data_Realty_OKS;
begin
  Result := AddItem(Index) as IXMLSTD_MP_Input_Data_Realty_OKS;
end;

{ TXMLSTD_MP_Input_Data_Realty_OKS }

function TXMLSTD_MP_Input_Data_Realty_OKS.Get_CadastralNumber: UnicodeString;
begin
  Result := ChildNodes['CadastralNumber'].Text;
end;

procedure TXMLSTD_MP_Input_Data_Realty_OKS.Set_CadastralNumber(Value: UnicodeString);
begin
  ChildNodes['CadastralNumber'].NodeValue := Value;
end;

function TXMLSTD_MP_Input_Data_Realty_OKS.Get_CadastralNumber_OtherNumber: UnicodeString;
begin
  Result := ChildNodes['CadastralNumber_OtherNumber'].Text;
end;

procedure TXMLSTD_MP_Input_Data_Realty_OKS.Set_CadastralNumber_OtherNumber(Value: UnicodeString);
begin
  ChildNodes['CadastralNumber_OtherNumber'].NodeValue := Value;
end;

function TXMLSTD_MP_Input_Data_Realty_OKS.Get_Name_OKS: UnicodeString;
begin
  Result := ChildNodes['Name_OKS'].Text;
end;

procedure TXMLSTD_MP_Input_Data_Realty_OKS.Set_Name_OKS(Value: UnicodeString);
begin
  ChildNodes['Name_OKS'].NodeValue := Value;
end;

{ TXMLSTD_MP_Input_Data_SubParcels }

procedure TXMLSTD_MP_Input_Data_SubParcels.AfterConstruction;
begin
  RegisterChildNode('SubParcel', TXMLSTD_MP_Input_Data_SubParcels_SubParcel);
  ItemTag := 'SubParcel';
  ItemInterface := IXMLSTD_MP_Input_Data_SubParcels_SubParcel;
  inherited;
end;

function TXMLSTD_MP_Input_Data_SubParcels.Get_SubParcel(Index: Integer): IXMLSTD_MP_Input_Data_SubParcels_SubParcel;
begin
  Result := List[Index] as IXMLSTD_MP_Input_Data_SubParcels_SubParcel;
end;

function TXMLSTD_MP_Input_Data_SubParcels.Add: IXMLSTD_MP_Input_Data_SubParcels_SubParcel;
begin
  Result := AddItem(-1) as IXMLSTD_MP_Input_Data_SubParcels_SubParcel;
end;

function TXMLSTD_MP_Input_Data_SubParcels.Insert(const Index: Integer): IXMLSTD_MP_Input_Data_SubParcels_SubParcel;
begin
  Result := AddItem(Index) as IXMLSTD_MP_Input_Data_SubParcels_SubParcel;
end;

{ TXMLSTD_MP_Input_Data_SubParcels_SubParcel }

function TXMLSTD_MP_Input_Data_SubParcels_SubParcel.Get_CadastralNumber: UnicodeString;
begin
  Result := ChildNodes['CadastralNumber'].Text;
end;

procedure TXMLSTD_MP_Input_Data_SubParcels_SubParcel.Set_CadastralNumber(Value: UnicodeString);
begin
  ChildNodes['CadastralNumber'].NodeValue := Value;
end;

function TXMLSTD_MP_Input_Data_SubParcels_SubParcel.Get_Number_Record: UnicodeString;
begin
  Result := ChildNodes['Number_Record'].Text;
end;

procedure TXMLSTD_MP_Input_Data_SubParcels_SubParcel.Set_Number_Record(Value: UnicodeString);
begin
  ChildNodes['Number_Record'].NodeValue := Value;
end;

{ TXMLSTD_MP_Survey }

procedure TXMLSTD_MP_Survey.AfterConstruction;
begin
  RegisterChildNode('AppliedFile', TXMLSTD_MP_Survey_AppliedFile);
  ItemTag := 'AppliedFile';
  ItemInterface := IXMLSTD_MP_Survey_AppliedFile;
  inherited;
end;

function TXMLSTD_MP_Survey.Get_AppliedFile(Index: Integer): IXMLSTD_MP_Survey_AppliedFile;
begin
  Result := List[Index] as IXMLSTD_MP_Survey_AppliedFile;
end;

function TXMLSTD_MP_Survey.Add: IXMLSTD_MP_Survey_AppliedFile;
begin
  Result := AddItem(-1) as IXMLSTD_MP_Survey_AppliedFile;
end;

function TXMLSTD_MP_Survey.Insert(const Index: Integer): IXMLSTD_MP_Survey_AppliedFile;
begin
  Result := AddItem(Index) as IXMLSTD_MP_Survey_AppliedFile;
end;

{ TXMLSTD_MP_Survey_AppliedFile }

{ TXMLSTD_MP_Scheme_Geodesic_Plotting }

procedure TXMLSTD_MP_Scheme_Geodesic_Plotting.AfterConstruction;
begin
  RegisterChildNode('AppliedFile', TXMLSTD_MP_Survey_AppliedFile);
  ItemTag := 'AppliedFile';
  ItemInterface := IXMLSTD_MP_Survey_AppliedFile;
  inherited;
end;

function TXMLSTD_MP_Scheme_Geodesic_Plotting.Get_AppliedFile(Index: Integer): IXMLSTD_MP_Survey_AppliedFile;
begin
  Result := List[Index] as IXMLSTD_MP_Survey_AppliedFile;
end;

function TXMLSTD_MP_Scheme_Geodesic_Plotting.Add: IXMLSTD_MP_Survey_AppliedFile;
begin
  Result := AddItem(-1) as IXMLSTD_MP_Survey_AppliedFile;
end;

function TXMLSTD_MP_Scheme_Geodesic_Plotting.Insert(const Index: Integer): IXMLSTD_MP_Survey_AppliedFile;
begin
  Result := AddItem(Index) as IXMLSTD_MP_Survey_AppliedFile;
end;

{ TXMLSTD_MP_Scheme_Geodesic_Plotting_AppliedFile }

{ TXMLSTD_MP_Scheme_Disposition_Parcels }

procedure TXMLSTD_MP_Scheme_Disposition_Parcels.AfterConstruction;
begin
  RegisterChildNode('AppliedFile', TXMLSTD_MP_Survey_AppliedFile);
  ItemTag := 'AppliedFile';
  ItemInterface := IXMLSTD_MP_Survey_AppliedFile;
  inherited;
end;

function TXMLSTD_MP_Scheme_Disposition_Parcels.Get_AppliedFile(Index: Integer): IXMLSTD_MP_Survey_AppliedFile;
begin
  Result := List[Index] as IXMLSTD_MP_Survey_AppliedFile;
end;

function TXMLSTD_MP_Scheme_Disposition_Parcels.Add: IXMLSTD_MP_Survey_AppliedFile;
begin
  Result := AddItem(-1) as IXMLSTD_MP_Survey_AppliedFile;
end;

function TXMLSTD_MP_Scheme_Disposition_Parcels.Insert(const Index: Integer): IXMLSTD_MP_Survey_AppliedFile;
begin
  Result := AddItem(Index) as IXMLSTD_MP_Survey_AppliedFile;
end;

{ TXMLSTD_MP_Scheme_Disposition_Parcels_AppliedFile }

{ TXMLSTD_MP_Diagram_Parcels_SubParcels }

procedure TXMLSTD_MP_Diagram_Parcels_SubParcels.AfterConstruction;
begin
  RegisterChildNode('AppliedFile', TXMLSTD_MP_Survey_AppliedFile);
  ItemTag := 'AppliedFile';
  ItemInterface := IXMLSTD_MP_Survey_AppliedFile;
  inherited;
end;

function TXMLSTD_MP_Diagram_Parcels_SubParcels.Get_AppliedFile(Index: Integer): IXMLSTD_MP_Survey_AppliedFile;
begin
  Result := List[Index] as IXMLSTD_MP_Survey_AppliedFile;
end;

function TXMLSTD_MP_Diagram_Parcels_SubParcels.Add: IXMLSTD_MP_Survey_AppliedFile;
begin
  Result := AddItem(-1) as IXMLSTD_MP_Survey_AppliedFile;
end;

function TXMLSTD_MP_Diagram_Parcels_SubParcels.Insert(const Index: Integer): IXMLSTD_MP_Survey_AppliedFile;
begin
  Result := AddItem(Index) as IXMLSTD_MP_Survey_AppliedFile;
end;

{ TXMLSTD_MP_Diagram_Parcels_SubParcels_AppliedFile }

{ TXMLSTD_MP_Agreement_Document }

procedure TXMLSTD_MP_Agreement_Document.AfterConstruction;
begin
  RegisterChildNode('AppliedFile', TXMLSTD_MP_Survey_AppliedFile);
  ItemTag := 'AppliedFile';
  ItemInterface := IXMLSTD_MP_Survey_AppliedFile;
  inherited;
end;

function TXMLSTD_MP_Agreement_Document.Get_AppliedFile(Index: Integer): IXMLSTD_MP_Survey_AppliedFile;
begin
  Result := List[Index] as IXMLSTD_MP_Survey_AppliedFile;
end;

function TXMLSTD_MP_Agreement_Document.Add: IXMLSTD_MP_Survey_AppliedFile;
begin
  Result := AddItem(-1) as IXMLSTD_MP_Survey_AppliedFile;
end;

function TXMLSTD_MP_Agreement_Document.Insert(const Index: Integer): IXMLSTD_MP_Survey_AppliedFile;
begin
  Result := AddItem(Index) as IXMLSTD_MP_Survey_AppliedFile;
end;

{ TXMLSTD_MP_Agreement_Document_AppliedFile }

{ TXMLSTD_MP_NodalPointSchemes }

procedure TXMLSTD_MP_NodalPointSchemes.AfterConstruction;
begin
  RegisterChildNode('NodalPointScheme', TXMLSTD_MP_NodalPointSchemes_NodalPointScheme);
  ItemTag := 'NodalPointScheme';
  ItemInterface := IXMLSTD_MP_NodalPointSchemes_NodalPointScheme;
  inherited;
end;

function TXMLSTD_MP_NodalPointSchemes.Get_NodalPointScheme(Index: Integer): IXMLSTD_MP_NodalPointSchemes_NodalPointScheme;
begin
  Result := List[Index] as IXMLSTD_MP_NodalPointSchemes_NodalPointScheme;
end;

function TXMLSTD_MP_NodalPointSchemes.Add: IXMLSTD_MP_NodalPointSchemes_NodalPointScheme;
begin
  Result := AddItem(-1) as IXMLSTD_MP_NodalPointSchemes_NodalPointScheme;
end;

function TXMLSTD_MP_NodalPointSchemes.Insert(const Index: Integer): IXMLSTD_MP_NodalPointSchemes_NodalPointScheme;
begin
  Result := AddItem(Index) as IXMLSTD_MP_NodalPointSchemes_NodalPointScheme;
end;

{ TXMLSTD_MP_NodalPointSchemes_NodalPointScheme }

procedure TXMLSTD_MP_NodalPointSchemes_NodalPointScheme.AfterConstruction;
begin
  RegisterChildNode('AppliedFile', TXMLSTD_MP_Survey_AppliedFile);
  ItemTag := 'AppliedFile';
  ItemInterface := IXMLSTD_MP_Survey_AppliedFile;
  inherited;
end;

function TXMLSTD_MP_NodalPointSchemes_NodalPointScheme.Get_Definition: UnicodeString;
begin
  Result := AttributeNodes['Definition'].Text;
end;

procedure TXMLSTD_MP_NodalPointSchemes_NodalPointScheme.Set_Definition(Value: UnicodeString);
begin
  SetAttribute('Definition', Value);
end;

function TXMLSTD_MP_NodalPointSchemes_NodalPointScheme.Get_AppliedFile(Index: Integer): IXMLSTD_MP_Survey_AppliedFile;
begin
  Result := List[Index] as IXMLSTD_MP_Survey_AppliedFile;
end;

function TXMLSTD_MP_NodalPointSchemes_NodalPointScheme.Add: IXMLSTD_MP_Survey_AppliedFile;
begin
  Result := AddItem(-1) as IXMLSTD_MP_Survey_AppliedFile;
end;

function TXMLSTD_MP_NodalPointSchemes_NodalPointScheme.Insert(const Index: Integer): IXMLSTD_MP_Survey_AppliedFile;
begin
  Result := AddItem(Index) as IXMLSTD_MP_Survey_AppliedFile;
end;

{ TXMLSTD_MP_NodalPointSchemes_NodalPointScheme_AppliedFile }

{ TXMLSTD_MP_Appendix }

procedure TXMLSTD_MP_Appendix.AfterConstruction;
begin
  RegisterChildNode('Document', TXMLTDocument);
  ItemTag := 'Document';
  ItemInterface := IXMLTDocument;
  inherited;
end;

function TXMLSTD_MP_Appendix.Get_Document(Index: Integer): IXMLTDocument;
begin
  Result := List[Index] as IXMLTDocument;
end;

function TXMLSTD_MP_Appendix.Add: IXMLTDocument;
begin
  Result := AddItem(-1) as IXMLTDocument;
end;

function TXMLSTD_MP_Appendix.Insert(const Index: Integer): IXMLTDocument;
begin
  Result := AddItem(Index) as IXMLTDocument;
end;

{ TXMLString_List }

function TXMLString_List.Add(const Value: UnicodeString): IXMLNode;
begin
  Result := AddItem(-1);
  Result.NodeValue := Value;
end;

function TXMLString_List.Insert(const Index: Integer; const Value: UnicodeString): IXMLNode;
begin
  Result := AddItem(Index);
  Result.NodeValue := Value;
end;

function TXMLString_List.Get_Item(Index: Integer): UnicodeString;
begin
  Result := List[Index].NodeValue;
end;

{ TXMLTChangeParcel_Inner_CadastralNumbers_CadastralNumberList }

function TXMLTChangeParcel_Inner_CadastralNumbers_CadastralNumberList.Add(const Value: UnicodeString): IXMLNode;
begin
  Result := AddItem(-1);
  Result.NodeValue := Value;
end;

function TXMLTChangeParcel_Inner_CadastralNumbers_CadastralNumberList.Insert(const Index: Integer; const Value: UnicodeString): IXMLNode;
begin
  Result := AddItem(Index);
  Result.NodeValue := Value;
end;

function TXMLTChangeParcel_Inner_CadastralNumbers_CadastralNumberList.Get_Item(Index: Integer): UnicodeString;
begin
  Result := List[Index].NodeValue;
end;

{ TXMLTExistEZParcel_Inner_CadastralNumbers_CadastralNumberList }

function TXMLTExistEZParcel_Inner_CadastralNumbers_CadastralNumberList.Add(const Value: UnicodeString): IXMLNode;
begin
  Result := AddItem(-1);
  Result.NodeValue := Value;
end;

function TXMLTExistEZParcel_Inner_CadastralNumbers_CadastralNumberList.Insert(const Index: Integer; const Value: UnicodeString): IXMLNode;
begin
  Result := AddItem(Index);
  Result.NodeValue := Value;
end;

function TXMLTExistEZParcel_Inner_CadastralNumbers_CadastralNumberList.Get_Item(Index: Integer): UnicodeString;
begin
  Result := List[Index].NodeValue;
end;

{ TXMLTExistParcel_Inner_CadastralNumbers_CadastralNumberList }


{function TXMLTExistParcel_Inner_CadastralNumbers_CadastralNumberList.Add(const Value: UnicodeString): IXMLNode;
begin
  Result := AddItem(-1);
  Result.NodeValue := Value;
end;

function TXMLTExistParcel_Inner_CadastralNumbers_CadastralNumberList.Insert(const Index: Integer; const Value: UnicodeString): IXMLNode;
begin
  Result := AddItem(Index);
  Result.NodeValue := Value;
end;

function TXMLTExistParcel_Inner_CadastralNumbers_CadastralNumberList.Get_Item(Index: Integer): UnicodeString;
begin
  Result := List[Index].NodeValue;
end;

{ TXMLTInner_CadastralNumbers_CadastralNumberList }

function TXMLTInner_CadastralNumbers_CadastralNumberList.Add(const Value: UnicodeString): IXMLNode;
begin
  Result := AddItem(-1);
  Result.NodeValue := Value;
end;

function TXMLTInner_CadastralNumbers_CadastralNumberList.Insert(const Index: Integer; const Value: UnicodeString): IXMLNode;
begin
  Result := AddItem(Index);
  Result.NodeValue := Value;
end;

function TXMLTInner_CadastralNumbers_CadastralNumberList.Get_Item(Index: Integer): UnicodeString;
begin
  Result := List[Index].NodeValue;
end;
{ TXMLTInner_CadastralNumbers_CadastralNumberList }

{ TXMLTProviding_Pass_CadastralNumbers_CadastralNumberList }

function TXMLTProviding_Pass_CadastralNumbers_CadastralNumberList.Add(const Value: UnicodeString): IXMLNode;
begin
  Result := AddItem(-1);
  Result.NodeValue := Value;
end;

function TXMLTProviding_Pass_CadastralNumbers_CadastralNumberList.Insert(const Index: Integer; const Value: UnicodeString): IXMLNode;
begin
  Result := AddItem(Index);
  Result.NodeValue := Value;
end;

function TXMLTProviding_Pass_CadastralNumbers_CadastralNumberList.Get_Item(Index: Integer): UnicodeString;
begin
  Result := List[Index].NodeValue;
end;

{ TXMLTExistParcelList }


{ TXMLTChangeBorder }

procedure TXMLTChangeBorder.AfterConstruction;
begin
        RegisterChildNode('OldOrdinate', TXMLTOrdinate);
        RegisterChildNode('NewOrdinate', TXMLTOrdinate);
        inherited;

end;

function TXMLTChangeBorder.Get_NewOrdinate: IXMLTOrdinate;
begin
          Result := ChildNodes['NewOrdinate'] as IXMLTOrdinate;
end;

function TXMLTChangeBorder.Get_OldOrdinate: IXMLTOrdinate;
begin
        Result := ChildNodes['OldOrdinate'] as IXMLTOrdinate;
end;

end.


