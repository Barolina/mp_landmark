unit fmSelectionList;

{ �����  ���������� }

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ButtonGroup, StdCtrls, ActnList, Vcl.ExtCtrls,
  unRussLang, JvExStdCtrls, JvListBox;

type
  TfrmSelectionList = class(TForm)
    pnl_Selection: TPanel;
    btnOKBtn: TButton;
    lstListType: TJvListBox;
  private
    CallForm: THandle;
    { Private declarations }
  public
    { Public declarations }
    function ListValueShow(Caption: String; valList: TStringList): integer;
  end;

var
  frmSelectionList: TfrmSelectionList;

implementation

{$R *.dfm}

function TfrmSelectionList.ListValueShow(Caption: String;
  valList: TStringList): integer;
begin
  if not Assigned(frmSelectionList) then
    Application.CreateForm(TfrmSelectionList, frmSelectionList);
  Result := -1;
  if (valList = nil) or (valList.Count <= 0) then
    raise Exception.Create(rsNoSelectedValues);
  frmSelectionList.Caption := Caption;
  frmSelectionList.lstListType.Clear;
  frmSelectionList.lstListType.Items := valList;
  frmSelectionList.lstListType.ItemIndex := 0;
  frmSelectionList.ShowModal;
  if frmSelectionList.ModalResult = mrOk then
    Result := frmSelectionList.lstListType.ItemIndex;
end;

end.
