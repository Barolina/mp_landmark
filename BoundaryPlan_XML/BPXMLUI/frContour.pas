unit frContour;

{                             NewContour
  ExistParcel -> Contours - > ExistContour
                              DeleteAllBorder
}
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ComCtrls,

  FrArea, frEntitySpatial, frDeleteAllBorder,  STD_MP, ActnList, XMLIntf,
  System.Actions, unRussLang, JvExControls, JvGradient, JvgPage;

type
  TFrameContour = class(TFrame)
    Attribute_Panel: TPanel;
    Attribute_DefOrNumPP_Edit: TEdit;
    Attribute_DefOrNumPP_Label: TLabel;
    ActionList: TActionList;

    ActionChange_Attribute_DefOrNumPP: TAction;
    ActionChange_ExistingOrNew: TAction;
    jvgpgcntrlNewParcelsPageControl: TJvgPageControl;
    tsEnSp_Attribut: TTabSheet;
    jvgrdntEntity_Spatial: TJvGradient;
    tsArea: TTabSheet;
    pnl2: TPanel;
    pnlArea: TPanel;
    procedure Attribute_DefOrNumPP_EditKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Attribute_NumberPP_EditKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure ActionChange_Attribute_DefOrNumPPExecute(Sender: TObject);
  private
    { Private declarations }
    FXmlNode            : IXMLNode;
    FArea_frame         : TFrameArea;
    FEntitySpatial_frame: TFrameEntity_Spatial;
    FOldOrdinat         : TFrameDeleteAllBorder;
    FisTypeContour      : integer; {� ��  �������� ������ - �� ������� -  ��� ������� }

    FOnChange_Attribute_DefOrNum: TNotifyEvent;

    function getNewContour: IXMLContours_NewContour; //����� ������ ��������������� �������
    function getExistContour: IXMLContours_ExistContour; //������������ (����������, ����������) ������
    function getDeleteAllBorder : IXMLContours_DeleteAllBorder; //���������� ������� ������� ��������������� ������� (���������� �������)


    procedure Data_attribute_write;
    procedure Data_attribute_read;

    procedure Data_DoRead;
    procedure Data_DoWrite;
  protected
    procedure SetContour(const aContour: IXMLContours_NewContour);
    procedure SetContourExist (const aContour : IXMLContours_ExistContour);
    procedure SetContourDeleteAllBorder (const aContour : IXMLContours_DeleteAllBorder);

    procedure InitializationUI;
  public
    { Public declarations }
    constructor Create(Owner: TComponent); override;
    function IsAssignedContour: boolean;

    procedure AsAttributeContour(LockUserChange: Boolean=False);

    procedure DataRefeshOrdinate;
    procedure SetVisibleTypeCOntoursForm(flag : boolean);

    property XmlContour: IXMLContours_NewContour read getNewContour write SetContour;
    property XmlContourExist : IXMLContours_ExistContour read getExistContour write SetContourExist;
    property XmlContourDeleteAllBorder : IXMLContours_DeleteAllBorder read getDeleteAllBorder write SetContourDeleteAllBorder;

    property OnChange_Attribute_DefOnNum: TNotifyEvent read FOnChange_Attribute_DefOrNum write FOnChange_Attribute_DefOrNum;   end;

implementation

uses MsXMLAPI;
resourcestring
   cnstDefinition    = 'Definition';
   cnstNumber_Record = 'Number_Record';
   cnstTxtDefinition = '�����������: ';
   cnstTxtNumberRecord = '������� �����: ';
   cnstArea          = 'Area';
   cnstEntity_Spatial  = 'Entity_Spatial';
   cnstOldOrdinate    = 'OldOrdinate';


{$R *.dfm}

{ TFrameContour }

procedure TFrameContour.ActionChange_Attribute_DefOrNumPPExecute(
  Sender: TObject);
begin
  if not self.IsAssignedContour then begin
      self.Attribute_DefOrNumPP_Edit.Text:= '';
      raise Exception.Create(ExMSG_NotAssignedROOT);
  end;
  Data_attribute_write;
  if Assigned(self.FOnChange_Attribute_DefOrNum) then
 self.FOnChange_Attribute_DefOrNum(self);
end;

procedure TFrameContour.AsAttributeContour(LockUserChange: Boolean);
var
 _Def: string;
begin
 //���� ���� ������� ������
 if self.IsAssignedContour then
 begin
   case FisTypeContour  of
   0:  begin
         self.Attribute_DefOrNumPP_Label.Caption:= cnstTxtDefinition;
        _Def:=  MsXml_ReadAttribute(self.FXmlNode,cnstDefinition);
        MsXml_RemoveAttribute(self.FXmlNode,cnstDefinition);
        if _Def<>'' then begin
         self.FXmlNode.Attributes[cnstDefinition]:= _Def;
         self.Attribute_DefOrNumPP_Edit.Text:= _Def;
        end else
         self.Attribute_DefOrNumPP_Edit.Text:= _Def;
   end;
   1,2:  begin
         self.Attribute_DefOrNumPP_Label.Caption:= cnstTxtNumberRecord;
        _Def:=  MsXml_ReadAttribute(self.FXmlNode,cnstNumber_Record);
        MsXml_RemoveAttribute(self.FXmlNode,cnstNumber_Record);
        if _Def<>'' then begin
         self.FXmlNode.Attributes[cnstNumber_Record]:= strtoint(_Def);
         self.Attribute_DefOrNumPP_Edit.Text:= _Def;
        end else
         self.Attribute_DefOrNumPP_Edit.Text:= _Def;
      end;
   end;
 end;
end;


procedure TFrameContour.Attribute_DefOrNumPP_EditKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
 if key = VK_RETURN then
  if (Sender is TCustomEdit) then
   TCustomEdit(Sender).Parent.SetFocus;
end;

procedure TFrameContour.Attribute_NumberPP_EditKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
 if key = VK_RETURN then
  if (Sender is TCustomEdit) then
   TCustomEdit(Sender).Parent.SetFocus;
end;

constructor TFrameContour.Create(Owner: TComponent);
begin
  inherited;
  DataRefeshOrdinate;
  FisTypeContour := 0; //�����  ����� �����
end;

procedure TFrameContour.InitializationUI;
begin
 if self.IsAssignedContour then begin
   self.Data_DoRead;
   FOldOrdinat.Visible := false;
   case FisTypeContour of
    0,1: begin
       self.FArea_frame.XmlArea:= (self.FXmlNode.ChildNodes.Nodes[cnstArea] as IXMLTArea);
       self.FEntitySpatial_frame.EntitySpatial:= (self.FXmlNode.ChildNodes.Nodes[cnstEntity_Spatial]  as IXMLEntity_Spatial);
       self.FOldOrdinat.Visible:= false;
       self.FEntitySpatial_frame.Visible:= true;
   end;
   2  : begin
       self.FOldOrdinat.XMLContour := (Self.FXmlNode as IXMLContours_DeleteAllBorder);
       self.FEntitySpatial_frame.Visible:= false;
       self.FOldOrdinat.Visible := true;
   end
   else raise Exception.Create('��� �� ����� �� ��� � ContourExistParcel_Init');
   end;
 end else
 begin
   self.Attribute_DefOrNumPP_Edit.Text:= '';
   self.FArea_frame.XmlArea:= nil;
   self.FEntitySpatial_frame.EntitySpatial:= nil;
   self.FOldOrdinat.XMLContour := nil;
 end;
end;

function TFrameContour.IsAssignedContour: boolean;
begin
 Result := self.FXmlNode <> nil;
end;

procedure TFrameContour.SetContour(const aContour: IXMLContours_NewContour);
begin
 if FXmlNode = aContour  then Exit
 else
 begin
   FisTypeContour :=  0;
   self.FXmlNode:= aContour;
   self.InitializationUI;
 end;
end;

procedure TFrameContour.SetContourDeleteAllBorder(
  const aContour: IXMLContours_DeleteAllBorder);
begin
 if FXmlNode = aContour  then Exit
 else
 begin
   FisTypeContour := 2;
   self.FXmlNode:= aContour;
   self.InitializationUI;
 end;
end;

procedure TFrameContour.SetContourExist(
  const aContour: IXMLContours_ExistContour);
begin
 if FXmlNode = aContour  then Exit
 else
 begin
   FisTypeContour := 1;
   self.FXmlNode:= aContour;
   self.InitializationUI;
 end;
end;


procedure TFrameContour.SetVisibleTypeCOntoursForm(flag: boolean);
begin
   self.FArea_frame.Visible := flag;
   self.FOldOrdinat.Visible :=  not flag;
end;

procedure TFrameContour.DataRefeshOrdinate;
begin
    if not Assigned(FArea_frame) then
    begin
        self.FArea_frame:= TFrameArea.Create(self.pnlArea);
        self.FArea_frame.Parent:= self.pnlArea;
        self.FArea_frame.Align:= alClient;
    end;
    if not Assigned(FEntitySpatial_frame) then
    begin
       self.FEntitySpatial_frame:= TFrameEntity_Spatial.Create(self.tsEnSp_Attribut);
       self.FEntitySpatial_frame.Parent:= self.tsEnSp_Attribut;
       self.FEntitySpatial_frame.Align:= alClient;
    end;
   if not Assigned(FOldOrdinat) then
   begin
    self.FOldOrdinat := TFrameDeleteAllBorder.Create(self.tsEnSp_Attribut);
    self.FOldOrdinat.Parent := self.tsEnSp_Attribut;
    self.FOldOrdinat.Align := alClient;
   end;
end;


procedure TFrameContour.Data_attribute_read;
begin
   self.AsAttributeContour(true);
end;

procedure TFrameContour.Data_attribute_write;
begin
 case FisTypeContour of
  0   :self.FXmlNode.Attributes[cnstDefinition]    :=  self.Attribute_DefOrNumPP_Edit.Text;
  1,2 :self.FXmlNode.Attributes[cnstNumber_Record] :=  strtoint(self.Attribute_DefOrNumPP_Edit.Text);
  else
    raise Exception.Create('Error : attribute is TypeConour?');
 end;
end;

procedure TFrameContour.Data_DoRead;
begin
 Data_attribute_read;
end;

procedure TFrameContour.Data_DoWrite;
begin
 Data_attribute_write;
end;

function TFrameContour.getDeleteAllBorder: IXMLContours_DeleteAllBorder;
begin
 if (self.FXmlNode<> nil) then
  Result:= IXMLContours_DeleteAllBorder(self.FXmlNode)
 else
  Result:= nil;
end;

function TFrameContour.getExistContour: IXMLContours_ExistContour;
begin
 if (self.FXmlNode<> nil) then
  Result:= IXMLContours_ExistContour(self.FXmlNode)
 else
  Result:= nil;
end;

function TFrameContour.getNewContour: IXMLContours_NewContour;
begin
 if (self.FXmlNode<> nil) then
  Result:= IXMLContours_NewContour(self.FXmlNode)
 else
  Result:= nil;
end;

end.

