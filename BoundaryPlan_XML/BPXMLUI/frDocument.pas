unit frDocument;

{
  TODO^
   ������������ ��� ��� ����� Encumbrance ���  �  ��� �����
   Input_Data, �� � ���������������  ������
}
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ButtonGroup, StdCtrls, ActnList,

  STD_MP, XMLIntf, Vcl.ImgList, Vcl.ExtCtrls,
  Vcl.Mask,frAttributesSTD_MP, fmAllDocumentsMain,unRussLang, JvExMask,
  JvToolEdit, JvMaskEdit, JvCheckedMaskEdit, JvDatePickerEdit;

type
  TFrameDocument = class(TFrame)
    FileOpenDialog: TOpenDialog;
    ImageList: TImageList;
    lbCode_Document: TLabel;
    cpgDocument: TCategoryPanelGroup;
    cpAttributDoc: TCategoryPanel;
    lbNumberDoc: TLabel;
    edNumber: TMaskEdit;
    cpDopolnitelno: TCategoryPanel;
    lbDateDoc: TLabel;
    edDate: TMaskEdit;
    lbAvtorDoc: TLabel;
    edIssueOrgan: TMaskEdit;
    lbNumberReg: TLabel;
    edNumberReg: TMaskEdit;
    lbDataReg: TLabel;
    edDateReg: TMaskEdit;
    edSeries: TMaskEdit;
    lbSeriesDoc: TLabel;
    edName: TMaskEdit;
    lbNameDoc: TLabel;
    lbStartedData: TLabel;
    edStarted: TMaskEdit;
    Label1: TLabel;
    edStopped: TMaskEdit;
    lbRegister: TLabel;
    edRegister: TMaskEdit;
    lbDesc: TLabel;
    edDesc: TMaskEdit;
    lbCode: TLabel;
    edIssueOrgan_Code: TMaskEdit;
    cpAppliedFields: TCategoryPanel;
    pnlObazat: TPanel;
    pnlDopol: TPanel;
    btnDocument: TButtonedEdit;
    procedure edNumberExit(Sender: TObject);
    procedure edDateExit(Sender: TObject);
    procedure edIssueOrganExit(Sender: TObject);
    procedure edNumberRegExit(Sender: TObject);
    procedure edDateRegExit(Sender: TObject);
    procedure edNameExit(Sender: TObject);
    procedure edSeriesExit(Sender: TObject);
    procedure edStartedExit(Sender: TObject);
    procedure edStoppedExit(Sender: TObject);
    procedure edRegisterExit(Sender: TObject);
    procedure edDescExit(Sender: TObject);
    procedure edIssueOrgan_CodeExit(Sender: TObject);
    procedure cpAppliedFieldsExpand(Sender: TObject);
    procedure btnDocumentRightButtonClick(Sender: TObject);

  private
    { Private declarations }

    FXMlDocument      : IXMLTDocument;
    FAppleidFile_frame: TFrameAttributedSTD_MP;

  protected
    procedure InitializationUI;
    procedure SetDocument(const aDocument : IXMLTDocument);

    procedure OnEdMaskExit(Sender : Tobject);
    procedure UI_WriteTo(const aDocument: IXMLTDocument; Sender : TObject);
    procedure UI_ReadOf(const aDocument: IXMLTDocument;Sender: TObject);

  public
    constructor Create(Owner: TComponent); override;
    { Public declarations }
    procedure RemoveLoadFiles();
    function IsAssignedDocument: boolean;
    property XMlDOcument                   : IXMLTDocument
                                             read FXMlDocument
                                             write SetDocument;
    property AppliedFile_frame  : TFrameAttributedSTD_MP read FAppleidFile_frame;
  end;


implementation
uses  MsXMLAPI;
resourcestring
    cnstName   = 'Name';
    cnstSeries ='Series';
    cnstNumber ='Number';
    cnstDate   ='Date';
    cnstIssueOrgan  = 'IssueOrgan';
    cnstNumberReg   ='NumberReg';
    cnstDateReg     ='DateReg';
    cnstStarted     = 'Started';
    cnstStopped     = 'Stopped';
    cnstRegister    = 'Register';
    cnstDesc        = 'Desc';
    cnstIssueOrgan_Code ='IssueOrgan_Code';

    {$R *.dfm}
    
{ TFrameAppliedFile }

///   ������� �� ������ ��� ������� �������
///   �.�. Ed
///   -- ��� �������
function GetPieceStr(str : string) : string;
begin
   Result := Copy(str,3,Length(str)-1);
end;

procedure TFrameDocument.OnEdMaskExit(Sender : Tobject);
begin
 if not self.IsAssignedDocument then raise Exception.Create(ExMSG_NotAssignedROOT);
 self.UI_WriteTo(self.FXMlDocument, Sender);
end;

procedure TFrameDocument.edDateExit(Sender: TObject);
begin
  OnEdMaskExit(Sender);
end;

procedure TFrameDocument.edDateRegExit(Sender: TObject);
begin
  OnEdMaskExit(Sender);
end;

procedure TFrameDocument.edDescExit(Sender: TObject);
begin
  OnEdMaskExit(Sender);
end;

procedure TFrameDocument.edIssueOrganExit(Sender: TObject);
begin
  OnEdMaskExit(Sender);
end;

procedure TFrameDocument.edIssueOrgan_CodeExit(Sender: TObject);
begin
  OnEdMaskExit(Sender);
end;

procedure TFrameDocument.edNameExit(Sender: TObject);
begin
  OnEdMaskExit(Sender);
end;

procedure TFrameDocument.edNumberExit(Sender: TObject);
begin
  OnEdMaskExit(Sender);
end;

procedure TFrameDocument.edNumberRegExit(Sender: TObject);
begin
  OnEdMaskExit(Sender);
end;

procedure TFrameDocument.edRegisterExit(Sender: TObject);
begin
  OnEdMaskExit(Sender);
end;

procedure TFrameDocument.edSeriesExit(Sender: TObject);
begin
  OnEdMaskExit(Sender);
end;

procedure TFrameDocument.edStartedExit(Sender: TObject);
begin
  OnEdMaskExit(Sender);
end;

procedure TFrameDocument.edStoppedExit(Sender: TObject);
begin
  OnEdMaskExit(Sender);
end;

procedure TFrameDocument.InitializationUI;
var  i  : integer;
begin
if self.IsAssignedDocument then {������������ Contrl}
 begin
   self.btnDocument.Text:= FormAllDocumentsMain.Documents.Key2Value(FXMlDocument.Code_Document);
   for I := 0 to pnlObazat.ControlCount-1 do
    if pnlObazat.Controls[I] is TMaskEdit then
      self.UI_ReadOf(self.FXMlDocument,TMaskEdit(pnlObazat.Controls[I]));

   for I := 0 to pnlDopol.ControlCount-1 do
    if pnlDopol.Controls[I] is TMaskEdit then
      self.UI_ReadOf(self.FXMlDocument,TMaskEdit(pnlDopol.Controls[I]));
   FAppleidFile_frame.XMLApplidFiles := FXMlDocument.AppliedFiles;
   FAppleidFile_frame.Visible := true;
 end
 else
 begin
   for I := 0 to pnlObazat.ControlCount-1 do
    if pnlObazat.Controls[I] is TMaskEdit then
     TMaskEdit(pnlObazat.Controls[I]).Text := '';
   for I := 0 to pnlDopol.ControlCount-1 do
    if pnlDopol.Controls[I] is TMaskEdit then
     TMaskEdit(pnlDopol.Controls[I]).Text := '';
   FAppleidFile_frame.XMLApplidFiles := nil;
   FAppleidFile_frame.Visible :=false;
 end;
end;

function TFrameDocument.IsAssignedDocument: boolean;
begin
  Result := FXMlDocument <> nil;
end;

procedure TFrameDocument.RemoveLoadFiles;
begin
  if not IsAssignedDocument  then Exit;
  if  (FXMlDocument.AppliedFiles <> nil) and  (FXMlDocument.AppliedFiles.Count > 0)  then
  begin
    FAppleidFile_frame.ListAppleField.SelectAll;
    FAppleidFile_frame.DeleteSelAppliedFile;
  end;  
end;

procedure TFrameDocument.btnDocumentRightButtonClick(Sender: TObject);
begin
 FormAllDocumentsMain.Key:=  FXMlDocument.Code_Document;
 FormAllDocumentsMain.ShowModal;
 if FormAllDocumentsMain.ModalResult=mrOk then  begin
  FXMlDocument.Code_Document:= FormAllDocumentsMain.Key;
  self.btnDocument.Text:= FormAllDocumentsMain.Value;
 end;
end;

procedure TFrameDocument.cpAppliedFieldsExpand(Sender: TObject);
var
  i: integer;
  CP: TCategoryPanel;
  flag : boolean;
begin
{
  for i := 0 to  cpgDocument.ControlCount -1 do
  begin
    CP := TCategoryPanel(cpgDocument.Controls[i]);
    CP.Collapsed := not (CP = (Sender as TCategoryPanel));
  end;
  cpgDocument.Realign;
  }
end;

constructor TFrameDocument.Create(Owner: TComponent);
begin
 inherited;
 self.FAppleidFile_frame:= TFrameAttributedSTD_MP.Create(cpAppliedFields);
 self.FAppleidFile_frame.Parent:= cpAppliedFields;
 self.FAppleidFile_frame.Align:= alClient;
 self.FAppleidFile_frame.Visible:= False;
end;

procedure TFrameDocument.SetDocument(const aDocument: IXMLTDocument);
begin
 if self.FXMlDocument=aDocument then EXIT;
 self.FXMlDocument:= aDocument;
 self.InitializationUI;
end;


procedure TFrameDocument.UI_ReadOf(const aDocument: IXMLTDocument; Sender: TObject);
var nameEdit : string;
    duration : IXMLDuration;
    resultXML: string;
    valueDate: TDateTime;
    frmt : TFormatSettings;
begin
   frmt.DateSeparator := '-';
   frmt.ShortDateFormat := 'yyyy-mm-dd';

   nameEdit := GetPieceStr((Sender as TMaskEdit).Name);
   duration := aDocument.Duration;

   if (nameEdit = cnstStarted) or (nameEdit = cnstStopped)  then
   begin
      if duration <> nil then
        begin
           if (nameEdit = cnstStarted)  then resultXML := MsXml_ReadChildNodeValue(duration,cnstStarted)
           else resultXML := MsXml_ReadChildNodeValue(duration,cnstStopped)
        end;
   end
   else {��� �����}
   resultXML := MsXml_ReadChildNodeValue(aDocument,GetPieceStr((Sender as TMaskEdit).Name));

   if (nameEdit = cnstDate ) or (nameEdit=  cnstStarted) or (nameEdit=  cnstStopped) or (nameEdit=cnstDateReg) then
   begin
       if TryStrToDate(resultXML,valueDate,frmt) then
        (Sender as TMaskEdit).Text := FormatDateTime('dd-mm-yyyy',StrToDate(resultXML,frmt))
        else  (Sender as TMaskEdit).Clear;
   end
   else (Sender as TMaskEdit).Text := resultXML;
end;


procedure TFrameDocument.UI_WriteTo(const aDocument: IXMLTDocument; Sender : TObject);
var str : string;
    nameEd : string;
    frmt : TFormatSettings;

    function UpDateDate(str : string) : string;
    begin
       if (str  <>  '  -  -    ') and (str <>  '') then
        Result := FormatDateTime('yyyy-mm-dd',StrToDate(str,frmt))
      else
        Result  := '';
    end;

begin
 frmt.DateSeparator := '-';
 frmt.ShortDateFormat := 'dd-mm-yyyy';

 str  := (Sender as TMaskEdit).Text;
 nameEd := GetPieceStr((Sender as TMaskEdit).Name);
 if nameEd = cnstName then
      aDocument.Name :=  str else
 if nameEd = cnstSeries then
     aDocument.Series := str else
 if nameEd = cnstNumber then
     aDocument.Number := str else
 if nameEd = cnstDate then
        aDocument.Date := UpDateDate(str) else
 if nameEd  = cnstIssueOrgan then
     aDocument.IssueOrgan := str else
 if nameEd= cnstNumberReg then
     aDocument.NumberReg := str else
 if nameEd =  cnstDateReg then
    aDocument.DateReg := UpDateDate(str) else
 if nameEd=  cnstStarted then
    aDocument.Duration.Started := UpDateDate(str) else
 if nameEd = cnstStopped then
    aDocument.Duration.Stopped := UpDateDate(str) else
 if nameEd = cnstRegister then
    aDocument.Register  := str else
 if nameEd =cnstDesc then
    aDocument.Desc := str else
 if nameEd = cnstIssueOrgan_Code then
    aDocument.IssueOrgan_Code := str;
end;

end.
