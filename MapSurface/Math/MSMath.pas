{*******************************************************}
{      |----------------------------------------|       }
{      |-(c) 2009-2010 ������������ ����������.-|       }
{      |-        ���������� ���������.         -|       }
{      |-          kras_ai@mail.ru             -|       }
{      |----------------------------------------|       }
{*******************************************************}
{
 ������ ������ ����� �������� � ���� �������������� ��� ������
 ��� ��������� ������������ �� ���������� MsMath. ������ ����������
 ���� ������� ���-�� � ������� �� ��������� �������:
 1.MapProcs.pas
 2.msApprox.pas
 3.Mscommon.pas
 4.MSCONV.PAS
 5.MSELL.PAS
 6.Msfilter.pas
 7.msMath.pas
 8.MSMATHEX.PAS
 9.MSMEMMAN.PAS
 10.MSNEW.PAS
 11.MSPROC.PAS
 12.MSTRACE.PAS
 13.MsTypes.pas
}
unit MSMath;
interface
uses
 MSType,
 MSMType_,
 MSMCommon,
 MSMCommon_;
type
 
 TRelationtKind = (rkOUT,rkIN,rkINTERSEC);
 
 TLineKind = (lkLine,lkInterval);
 
 TModeCurve = (mcClosed,mcNoClosed);
 
 TDistKind = (dkLine,dkPointA,dkPointB);
 
 TDirectionCurveKind = (dcWithHourArrow,dcWithOutHourArrow);
function msmPointInInterval(A,B,P: TMSMPoint): Boolean;
function msmPointInRect(A,B,P: TMSMPoint): Boolean;
function msmGetDistPointToLine(P,A,B : TMSMPoint; Mode: TLineKind; var DistKind: TDistKind) : double;
function msmGetExtentArea(Points: TPointsLogic): TMSMRect;
function msmGetProjection( A,B,P : TMSMPoint) : TMSMPoint;
procedure msmGetParallel(Base:TPointsLogic; var ResP:TPointsLogic; Dist,BStep,EStep:double; BConnect, EConnect:boolean);
function msmGetRelationLines(A1,B1,A2,B2 : TMSMPoint; Mode : TLineKind; var I1,I2 : TMSMPoint ) : Boolean;
function msmGetRalationLineVsRect(A,B: TMSMPoint;  Mode : TLineKind; P1,P2: TMSMPoint;  var I1,I2 :TMSMPoint ) : TRelationtKind;
function msmRelationPointVsArea(Point: TMSMPoint; Points: TPointsLogic; ControlExtent: Boolean = True): boolean;
function msmNearestSegment(Point: TMSMPoint; Points: TPointsLogic; ClosePoints: Boolean; var PointProjection: TMSMPoint ): Integer;
function msmRelationSegmentVsSegments(A,B: TMSMPoint; Points: TPointsLogic; ModeCurve: TModeCurve): TRelationtKind;
function msmGetAreaOf(Points: TPointsLogic) : double;
function msmDirectionOf(Points: TPointsLogic): TDirectionCurveKind;
procedure msmReDirectCurve(Points: TPointsLogic);
function msmDirectCurveTo(Points: TPointsLogic; Direct:TDirectionCurveKind): boolean;
procedure msmGetWeightCenterOf(Points: TPointsLogic; var PointWC: TMSMPoint);
function msmRelationCurveVsCurve(Points1:TPointsLogic;ModeCurve1: TModeCurve;Points2:TPointsLogic;ModeCurve2: TModeCurve): TRelationtKind;
function msmRelationCurveVsRect(Points: TPointsLogic;ModeCurve: TModeCurve;Rect: TMSMRect): TRelationtKind;
implementation
{------------------------------------------------------------------------------}
function msmPointInInterval( A,B,P : TMSMPoint ) : Boolean;
begin
 Result:= msmcIsZero(A.GetDistance(P.x,P.y)) or msmcIsZero(B.GetDistance(P.x,P.y)) or
          msmcIsZero(abs(msmcAngleBetween(P.x,P.y,A.x,A.y,P.x,P.y,B.x,B.y) - 180));
end;
{------------------------------------------------------------------------------}
function msmPointInRect( A,B,P : TMSMPoint ) : Boolean;
var
 ContainX : Boolean;
begin
 Result := False;
 if msmcisLE(A.X,B.X) then
    ContainX := msmcisLE(A.X , P.X) and msmcisLE(P.X , B.X)
 else
    ContainX := msmcisLE(B.X , P.X) and msmcisLE(P.X , A.X);
 if ContainX then
    if msmcisLE(A.Y , B.Y) then
       Result := msmcisLE(A.Y , P.Y) and msmcisLE(P.Y , B.Y)
    else
       Result := msmcisLE(B.Y , P.Y) and msmcisLE(P.Y , A.Y)
end;
{------------------------------------------------------------------------------}
function msmGetRelationLines(A1,B1,A2,B2 : TMSMPoint; Mode : TLineKind; var I1,I2   : TMSMPoint ) : Boolean;
var
 u1,u2, x,y : double;
  function IntersectionInterval : Boolean;
  begin
     Result := True;
     if Mode = lkLine then begin
        I1.DoAssign(A1);
        if B2.IsEqualPos(A1.x,a1.y)
           then I2.DoAssign(A2)
           else I2.DoAssign(B2);
        Exit;
     end;
     if msmPointInRect( A1,B1,B2 ) then begin
        I1.DoAssign(B2);
        if msmPointInRect( A1,B1,A2 )
           then I2.DoAssign(A2)
           else if msmPointInRect( A2,B2,A1 )
              then I2.DoAssign(A1)
              else I2.DoAssign(B1)
     end else
        if msmPointInRect( A1,B1,A2 ) then begin
           I1.DoAssign(A2);
           if msmPointInRect( A2,B2,A1 )
              then I2.DoAssign(A1)
              else I2.DoAssign(B1)
        end else
           if msmPointInRect( A2,B2,A1 ) then begin
              I1.DoAssign(A1);
              I2.DoAssign(B1);
           end else
              Result := False;
  end;
var
  vmrIsVertical1,vmrIsVertical2 : boolean;
  vmrTangens1,vmrTangens2 : double;
begin
  Result := False;
  if A1.IsEqualPos(B1.x,B1.y) then begin
     I1.DoAssign(A1);
     I2.DoAssign(I1);
     Result := A2.IsEqualPos(B2.x,B2.y) and A2.IsEqualPos(A1.x,A1.y);
     Exit;
  end;
  if A2.IsEqualPos(B2.x,B2.y) then begin
     I1.DoAssign(A2);
     I2.DoAssign(I1);
     Result := msmPointInInterval(A1,B1,I1);
     Exit;
  end;
  vmrIsVertical1 := msmcisEqual(A1.x,B1.x);
  vmrIsVertical2 := msmcisEqual(A2.x,B2.x);
  if not vmrIsVertical1 then vmrTangens1 := (B1.y-A1.y) / (B1.x-A1.x);
  if not vmrIsVertical2 then vmrTangens2 := (B2.y-A2.y) / (B2.x-A2.x);
  if vmrIsVertical1 then begin
     if vmrIsVertical2 then begin
        Result := msmcisEqual(A2.x,A1.x) and IntersectionInterval;
        exit;
     end else begin
        u2 := A2.y-vmrTangens2*A2.x;
        x  := A1.x;
        y  := vmrTangens2*x+u2;
        I1.x:= x;
        I1.y:= y;
        I2.DoAssign(I1);
        Result := (Mode = lkLine) or msmPointInRect( A1,B1,I1 ) and msmPointInRect( A2,B2,I1 );
        exit;
     end;
  end;
  if vmrIsVertical2 then begin
     u1 := A1.y-vmrTangens1*A1.x;
     x  := A2.x;
     y  := vmrTangens1*x+u1;
     I1.x:= x;
     I1.y:= y;
     I2.DoAssign(I1);
     Result := (Mode = lkLine) or msmPointInRect( A1,B1,I1 ) and msmPointInRect( A2,B2,I1 );
     exit;
  end;
  u1 := A1.y-vmrTangens1*A1.x;
  u2 := A2.y-vmrTangens2*A2.x;
  if msmcisEqual(vmrTangens1,vmrTangens2) then
     Result := msmcisEqual(u1,u2) and IntersectionInterval
  else begin
     x  := (u2-u1) / (vmrTangens1-vmrTangens2);
     y  := vmrTangens1*x+u1;
     I1.x:= x;
     I1.y:= y;
     I2.DoAssign(I1);
     Result := (Mode = lkLine) or
               (msmPointInRect( A1,B1,I1 ) or msmPointInInterval( A1,B1,I1 )) and
               (msmPointInRect( A2,B2,I1 ) or msmPointInInterval( A2,B2,I1 ));
  end;
end;   { GetCommonPoints }
{------------------------------------------------------------------------------}
procedure msmGetParallel(Base:TPointsLogic;
                           var ResP:TPointsLogic;
                           Dist,BStep,EStep:double;
                           BConnect,EConnect:boolean);
var
 ib,ip{,tc}:integer;
 l,da,sa,BAngle,EAngle:double;
 AA,A,B: TMSMPoint;
 R1,R2,R3,R4,I1,I2:TMSMPoint;
 Loop:boolean;
 BCount:integer;
 RCount:integer;
 procedure AddPoint(x,y:double); begin
  inc(RCount);
  SetLength(ResP,RCount);
  ResP[RCount-1]:= TMSMPoint.Create(x,y);
 end;
begin
 R1:= TMSMPoint.Create(0,0);
 R2:= TMSMPoint.Create(0,0);
 R3:= TMSMPoint.Create(0,0);
 R4:= TMSMPoint.Create(0,0);
 I1:= TMSMPoint.Create(0,0);
 I2:= TMSMPoint.Create(0,0);
 BCount:= length(Base);
 if BCount<=1 then Exit;
 Loop:=Base[0].IsEqualPos(Base[BCount-1].x,Base[BCount-1].y);
 if Loop and (BCount=2) then Exit;
 BAngle:=Arctan(BStep/abs(Dist));
 EAngle:=Arctan(EStep/abs(Dist));
 try
  RCount:=0;
  if BConnect and not Loop then
     AddPoint(Base[0].x,Base[0].y);
  if (not Loop) then begin
   sa:=-BAngle;
   l:=Dist/Cos(sa); if l<0 then sa:=-sa;
   da:=msmcLineAngle(Base[0].x,Base[0].y,Base[1].x,Base[1].y)*Pi/180+Pi/2;
   AddPoint(Base[0].x+l*Cos(da+sa),Base[0].y+l*Sin(da+sa));
  end;
  if Loop then ib:=0 else ib:=1;
   for ip := ib to BCount-2 do begin
     if ip>0 then
      AA:=Base[ip-1]
     else
      if Loop then
       AA:=Base[BCount - 2]
      else
       AA:=Base[BCount-1];
     A:=Base[ip];
     B:=Base[ip+1];
     da:=msmcLineAngle(AA.x,AA.y,A.x,A.y)*Pi/180+Pi/2;
     sa:=Pi/4;
     l:=Dist/Cos(sa); if l<0 then sa:=-sa;
     R1.x:=AA.x+l*Cos(da+sa);
     R1.y:=AA.y+l*Sin(da+sa);
     R2.x:=A.x+l*Cos(da-sa);
     R2.y:=A.y+l*Sin(da-sa);
     da:=msmcLineAngle(A.x,A.y,B.x,B.y)*Pi/180+Pi/2;
     R3.x:=A.x+l*Cos(da+sa);
     R3.y:=A.y+l*Sin(da+sa);
     R4.x:=B.x+l*Cos(da-sa);
     R4.y:=B.y+l*Sin(da-sa);
     if msmGetRelationLines(R1,R2,R3,R4,lkInterval,I1,I2) then
       AddPoint(I1.x,I1.y)
     else begin
       AddPoint(R2.x,R2.y);
       AddPoint(R3.x,R3.y);
      end;
   end; { for ip }
  if (not Loop) then begin
    sa:=-EAngle;
    l:=Dist/Cos(sa); if l<0 then sa:=-sa;
    da:=msmcLineAngle(Base[BCount-2].x,Base[BCount-2].y,Base[BCount-1].x,Base[BCount-1].y)*Pi/180+Pi/2;
    AddPoint(Base[BCount-1].x+l*Cos(da-sa),Base[BCount-1].y+l*Sin(da-sa));
  end else
   AddPoint(ResP[0].x,ResP[0].y);
  if EConnect and not Loop then
    AddPoint(Base[BCount-1].x,Base[BCount-1].y);
  
 finally
  R1.free; R2.free;
  R3.free; R4.free;
  I1.free; I2.free;
 end;
end;
{------------------------------------------------------------------------------}
function msmGetDistPointToLine(P,A,B : TMSMPoint; Mode: TLineKind; var DistKind: TDistKind) : double;
var
 C : TMSMPoint;
 d1,d2 : double;
begin
 if A.IsEqualPos(B.x,B.y) then  begin
  Result := P.GetDistance(A.x,A.y);
  DistKind:= dkLine;
  exit;
 end;
 C := msmGetProjection( A,B,P);
 if Mode = lkLine then begin
  d1:= P.GetDistance(C.x,C.y);
  DistKind:= dkLine;
 end else
  if msmPointInRect(A,B,C) and not (a.IsEqualPos(C.x,C.y) or b.IsEqualPos(C.x,c.y)) then begin
   d1 := P.GetDistance(C.x,C.y);
   DistKind:= dkLine;
  end else begin
   d1 := A.GetDistance(P.x,P.y);
   d2 := B.GetDistance(P.x,P.y);
   DistKind:= dkPointA;
   if d1 > d2 then begin
    d1 := d2;
    DistKind:= dkPointB;
   end;
  end;
 c.Free;
 Result := d1;
end;
{------------------------------------------------------------------------------}
function msmGetProjection( A,B,P : TMSMPoint) : TMSMPoint;
var
  u1,u2,x,y : double;
  vmrTangens1: double;
  vmrTangens2: double;
begin
 if msmcisEqual(A.X,B.X) then
   Result:= TMSMPoint.Create( A.X,P.Y )
  else
   if msmcisEqual(A.Y,B.Y) then
    Result:= TMSMPoint.Create( P.X,A.Y )
   else begin
    vmrTangens1 := (B.Y-A.Y) / (B.X-A.X);
    vmrTangens2 := -1 / vmrTangens1;
    u1 := A.Y - vmrTangens1 * A.X;
    u2 := P.Y - vmrTangens2 * P.X;
    x  := (u1-u2) / (vmrTangens2-vmrTangens1);
    y  := vmrTangens1 * x + u1;
    Result:= TMSMPoint.Create( x,y );
   end;
end;
{------------------------------------------------------------------------------}
function msmGetRalationLineVsRect(A,B: TMSMPoint;
                       Mode : TLineKind;
                      P1,P2: TMSMPoint;
                      var I1,I2 :TMSMPoint ) : TRelationtKind;
var
 intersCount : integer;
 IP1,IP2: TMSMPoint;
 IP: array[1..2] of TMSMPoint;
 x,y,r: double;
 procedure IntersectionAnalyse;
   begin
      Result := rkINTERSEC;
      if msmPointInRect( A,B,IP1 ) then
         if msmPointInRect( A,B,IP2 ) then
            if msmPointInRect( A,IP1,IP2 ) then begin
               I1.DoAssign(IP2);
               I2.DoAssign(IP1);
            end else begin
               I1.DoAssign(IP1);
               I2.DoAssign(IP2);
            end
         else
            if A.IsEqualPos(IP1.x,IP1.y) then
               if msmPointInRect(IP1,IP2,B) then begin
                  I1.DoAssign(IP1);
                  I2.DoAssign(B);
               end else begin
                  I1.DoAssign(A);
                  I2.DoAssign(IP1);
               end
            else
               if msmPointInRect(IP1,IP2,A ) then begin
                  I1.DoAssign(A);
                  I2.DoAssign(IP1);
               end else begin
                  I1.DoAssign(IP1);
                  I2.DoAssign(B);
               end
      else  {  not mrContains( A,B,IP[1] ) }
         if msmPointInRect( A,B,IP2 ) then
            if A.IsEqualPos(IP2.x,IP2.y) then
               if msmPointInRect(IP1,IP2,B ) then begin
                  I1.DoAssign(IP2);
                  I2.DoAssign(B);
               end else begin
                  I1.DoAssign(A);
                  I2.DoAssign(IP2);
               end
            else
               if msmPointInRect(IP1,IP2,A ) then begin
                  I1.DoAssign(A);
                  I2.DoAssign(IP2);
               end else begin
                  I1.DoAssign(IP2);
                  I2.DoAssign(B);
               end
         else { not mrContains( A,B,IP[2] ) }
            if msmPointInRect(IP1,IP2,A ) then begin
               I1.DoAssign(A);
               I2.DoAssign(B);
               Result := rkIN;
            end else
               Result := rkOUT;
   end; { IntersectionAnalyse }
var
   vmrTangens : double;
begin {  mrLineInRect }
 IP1:= TMSMPoint.Create(0,0);
 IP2:= TMSMPoint.Create(0,0);
 TRY
 IP[1]:= IP1; IP[2]:= IP2;
 if msmcisEqual(A.x,B.x) then
   begin
    if (A.x < P1.x) or (A.x > P2.x) then
     begin
      Result := rkOUT;
      Exit;
     end;
    IP1.x:= A.x; IP1.y:= P1.y;
    IP2.x:= A.x; IP2.y:= P2.y;
   end
 else
  if msmcisEqual(A.y,B.y) then
   begin
    if (A.y < P1.y) or (A.y > P2.y) then
     begin
      Result := rkOUT;
      Exit;
     end;
    IP1.x:= P1.x; IP1.y:= A.y;
    IP2.x:= P2.x; IP2.y:= A.y;
   end
  else
   begin
    IP1.y := P1.y + 1;  { for third if }
    intersCount := 0;
    vmrTangens  := (B.y - A.y) / (B.x - A.x);
    r := A.y - vmrTangens * A.x;
    y := vmrTangens * P1.x + r;
    if msmcisLE(P1.y , y) and msmcisGE(P2.y , y) then
     begin
      inc(intersCount);
      IP[intersCount].x:= P1.x;
      IP[intersCount].y:= y;
     end;
    y := vmrTangens * P2.x + r;
    if msmcisLE(P1.y , y) and msmcisGE(P2.y , y) then
     begin
      inc( intersCount );
      IP[intersCount].x:= P2.x;
      IP[intersCount].y:= y;
     end;
    if intersCount < 2 then
     begin
      if A.y <> B.y then
        if IP[1].y <> P1.y then
         begin
          x := (P1.y - r) / vmrTangens;
          if msmcisLE(P1.x , x) and msmcisGE(P2.x , x) then
           begin
            inc( intersCount );
            IP[intersCount].x:= x;
            IP[intersCount].y:= P1.y;
           end;
         end;
     end;
    if (intersCount < 2) and (A.y <> B.y) then
     begin
      x := (P2.y - r) / vmrTangens;
         if msmcisLE(P1.x , x) and msmcisGE(P2.x , x) then
          begin
           inc( intersCount );
           IP[intersCount].x:= x;
           IP[intersCount].y:=P2.y;
          end;
     end;
    if intersCount < 2 then
     begin
      Result := rkOUT;
      Exit;
     end;
   end;
   if Mode <> lkLine
     then IntersectionAnalyse;
 FINALLY
  IP1.free;
  IP2.free;
 END;
end;
{------------------------------------------------------------------------------}
function msmRelationPointVsArea(Point: TMSMPoint; Points: TPointsLogic; ControlExtent: Boolean) : Boolean;
var
 TempPointPoly: TPointsLogic;
 b1 : boolean;
 b2 : boolean;
 n: integer;
 I : integer;
 vExtent: TMSMRect;
begin
 Result := False;
 if Length(Points)<3 then exit;
 if ControlExtent then begin
  vExtent:= msmGetExtentArea(Points);
  vExtent.DoCheckOrientation;
  if (vExtent.GetSize = 0) or not(vExtent.IsContainsPoint(Point.x,Point.y)) then begin
   vExtent.Free;
   exit;
  end else
   vExtent.Free;
 end;
 TRY
  
  SetLength(TempPointPoly,length(Points));
  for I := 0 to High(Points) do
   TempPointPoly[i]:= Points[i].getCopy;
  
  if not TempPointPoly[high(TempPointPoly)].IsEqualPos(TempPointPoly[0].x,TempPointPoly[0].y) then begin
   SetLength(TempPointPoly,length(TempPointPoly)+1);
   TempPointPoly[high(TempPointPoly)]:= tempPointPoly[0].getCopy;
  end;
  i := 0;
  repeat
   b1 := Point.Y > tempPointPoly[i].Y;
   b2 := Point.Y <= tempPointPoly[i+1].Y;
   if  not (b1 and  not b2 or b2 and  not b1) then begin
    if Point.x-tempPointPoly[i].x < (Point.Y-tempPointPoly[i].y)*
                                    (tempPointPoly[i+1].x - tempPointPoly[i].x)/
                                    (tempPointPoly[i+1].Y - tempPointPoly[i].Y)
    then Result:= not Result;
   end;
   i := i+1;
  until  not (i<=high(TempPointPoly)-1);
 FINALLY
  for I := 0 to High(TempPointPoly) do
    TempPointPoly[i].Free;
  SetLength(TempPointPoly,0);
  TempPointPoly:= nil;
 END;
end;
{------------------------------------------------------------------------------}
function msmGetExtentArea(Points: TPointsLogic): TMSMRect;
var
 i: Integer;
 resRect: TMSMRect;
 MinPoint,MaxPoint: TMSMPoint;
begin
 resRect:= TMSMRect.Create(0,0,0,0);
 if Length(Points)<>0 then begin
   MinPoint:= resRect.Apoint;
   MaxPoint:= resRect.Bpoint;
   MinPoint.DoAssign(Points[0]);
   MaxPoint.DoAssign(Points[0]);
   for I := 0 to high(Points) do begin
    if MinPoint.x > Points[i].x then MinPoint.X:= Points[i].x;
    if MinPoint.y > Points[i].y then MinPoint.y:= Points[i].y;
    if MaxPoint.x < Points[i].x then MaxPoint.X:= Points[i].x;
    if MaxPoint.y < Points[i].y then MaxPoint.y:= Points[i].y;
   end;
  end;
 Result:= resRect;
end;
{------------------------------------------------------------------------------}
function msmRelationSegmentVsSegments(A,B: TMSMPoint; Points: TPointsLogic; ModeCurve: TModeCurve): TRelationtKind;
var
 vExtent: TMSMRect;
 vI1,vI2: TMSMPoint;
 WhereA,WhereB: Boolean;
 i: integer;
begin
 Result:= rkOUT;
 if Length(Points)=0 then Exit;
 try
  vExtent:= msmGetExtentArea(Points);
  vExtent.DoCheckOrientation;
  vI1:= TMSMPoint.Create(0,0);
  vI2:= TMSMPoint.Create(0,0);
  WhereA:= False; WhereB:= false;
  
  if (ModeCurve = mcClosed) and (Length(Points)>2) then begin
   WhereA:= msmRelationPointVsArea(a,points,true);
   WhereB:= msmRelationPointVsArea(b,points,true);
   if WhereA xor WhereB then
    Result:= rkINTERSEC;
  end;
  if Result = rkOUT then begin
   
   for I := 0 to high(Points) - 1 do
    if msmGetRelationLines(a,b,points[i],points[I+1],lkInterval,vi1,vi2) then begin
     Result:= rkINTERSEC;
     Break;
   end;
   
   if (ModeCurve = mcClosed) and (Result = rkOUT) and (Length(Points)>2) then
    if msmGetRelationLines(a,b,points[high(Points)],points[0],lkInterval,vi1,vi2) then
     Result:= rkINTERSEC;
  end;
  
  if (Result = rkOUT) and WhereA and WhereB then
   Result:= rkIN;
 finally
  vExtent.Free;
  vI1.Free;
  vI2.Free;
 end;
end;
{------------------------------------------------------------------------------}
function msmRelationCurveVsCurve(Points1:TPointsLogic;ModeCurve1: TModeCurve;Points2:TPointsLogic;ModeCurve2: TModeCurve): TRelationtKind;
var
 vExtent1,vExtent2: TMSMRect;
 v1,v2,v3,v4: TMSMPoint;
 vRelationt: TRelationtKind;
 vRelationtIn,vRelationtOut: Boolean;
 i: Integer;
 bl: Boolean;
begin
 Result:= rkOUT;
 vExtent1:= nil;
 vExtent2:= nil;
 v1:= nil; v2:= nil;
 v3:= nil; v4:= nil;
 if (Length(Points1)<3) or (Length(Points2)<3) then exit;
 try
  vExtent1:= msmGetExtentArea(Points1);
  vExtent1.DoCheckOrientation;
  vExtent2:= msmGetExtentArea(Points2);
  vExtent2.DoCheckOrientation;
  if vExtent1.IsIntersection(vExtent2) then begin
    vRelationt:= rkOUT;
    vRelationtIn:= False;
    vRelationtOut:= False;
    for I := 0 to high(Points1) - 1 do begin
     vRelationt:=msmRelationSegmentVsSegments(Points1[i],Points1[i+1],points2,ModeCurve2);
     case vRelationt of
      rkOUT: if vRelationtIn then break else vRelationtOut:= true;
      rkIN: if vRelationtOut then break else vRelationtIn:= true;
      rkINTERSEC: Break;
     end;
    end;
    if (ModeCurve1 = mcClosed) and (vRelationt = rkOUT) then
      vRelationt:=msmRelationSegmentVsSegments(Points1[0],Points1[high(Points1)],points2,ModeCurve2);
    case vRelationt of
     rkOUT: if vRelationtIn then vRelationt:= rkINTERSEC;
     rkIN: if vRelationtOut then vRelationt:= rkINTERSEC;
    end;
   Result:= vRelationt;
  end;
 finally
  vExtent1.Free;
  vExtent2.Free;
  v1.Free; v2.Free;
  v3.Free; v4.Free;
 end;
end;
{------------------------------------------------------------------------------}
function msmRelationCurveVsRect(Points: TPointsLogic;ModeCurve: TModeCurve;Rect: TMSMRect): TRelationtKind;
var
 RectVertex: TPointsLogic;
 vrect: TMSMRect;
begin
 try
  Result:= rkOUT;
  SetLength(RectVertex,4);
  vrect:= TMSMRect.Create(Rect.Apoint.x,Rect.Apoint.y,Rect.Bpoint.x,Rect.Bpoint.y);
  vrect.DoCheckOrientation;
  RectVertex[0]:= vRect.Apoint.GetCopy;
  RectVertex[1]:= TMSMPoint.Create(vRect.Apoint.x,vRect.Bpoint.y);
  RectVertex[2]:= vRect.Bpoint.GetCopy;
  RectVertex[3]:= TMSMPoint.Create(vRect.Bpoint.x,vRect.Apoint.y);
  result:= msmRelationCurveVsCurve(points,ModeCurve,RectVertex,mcClosed);
 finally
  vrect.Free;
  RectVertex[0].Free;
  RectVertex[1].Free;
  RectVertex[2].Free;
  RectVertex[3].Free;
 end;
end;
{------------------------------------------------------------------------------}
function msmNearestSegment(Point: TMSMPoint; 
                           Points: TPointsLogic; 
                           ClosePoints: Boolean; 
                           var PointProjection: TMSMPoint ): Integer;
 var
  CurPoint,_CurPoint: TMSMPoint;
  CurLength,_CurLength: Double;
  CurIndex,_I,_II: Integer;
  CurDistKind,_CurDistKind: TDistKind;
 begin
  Result:= -1;
  if Length(Points)<2 then Exit;
  
  CurPoint:= msmGetProjection(Points[0],Points[1],Point);
  CurLength:= msmGetDistPointToLine(Point,Points[0],Points[1],lkInterval,CurDistKind);
  CurIndex:= 0;
  
  for _I := 1 to high(Points) do begin
   if (_I = High(Points)) then begin 
    if not ClosePoints or (Length(Points)<3) then Continue; 
    _II:= 0;
   end else
   _II:= _I+1;
   _CurPoint:= msmGetProjection(Points[_I],Points[_II],Point);
   _CurLength:= msmGetDistPointToLine(Point,Points[_I],Points[_II],lkInterval,_CurDistKind);
   if (_CurLength<CurLength) and (_CurDistKind=dkLine) then begin
    CurPoint.DoAssign(_CurPoint);
    CurLength:= _CurLength;
    CurIndex:= _i;
    CurDistKind:=dkLine;
   end;
   _CurPoint.Free;
  end;
  if CurDistKind=dkLine then begin
   if Assigned(PointProjection) then
     PointProjection.DoAssign(CurPoint);
   Result:= CurIndex;
  end;
  CurPoint.Free;
end;
{------------------------------------------------------------------------------}
function msmGetAreaOf(Points: TPointsLogic) : double;
var
 s : double;
 i : integer;
begin
 Result:= 0.0;
 if Length(Points)<3 then exit;
 s := 0.0;
 for i := 0 to high(Points)-1 do
  s := s + 0.5*( Points[i+1].X-Points[i].X )*( Points[i+1].Y+Points[i].Y );
 result := s + 0.5*( Points[0].X-Points[high(Points)].X )*( Points[0].Y+Points[high(Points)].Y );
end;
{------------------------------------------------------------------------------}
function msmDirectionOf(Points: TPointsLogic): TDirectionCurveKind;
begin
 if msmGetAreaOf(Points) < 0 then
  Result:= dcWithHourArrow
 else
  result:= dcWithOutHourArrow;
end;
{------------------------------------------------------------------------------}
procedure msmReDirectCurve(Points: TPointsLogic);
var
 P: TMSMPoint;
 i  : integer;
begin
 if Length(Points)<3 then exit;
 p:= TMSMPoint.Create(0,0);
 for i := 0 to (length(Points) div 2)-1 do begin
  P.DoAssign(Points[i]);
  Points[i].DoAssign(Points[High(Points)-i]);
  Points[High(Points)-i].DoAssign(P);
 end;
 p.Free;
end;
{------------------------------------------------------------------------------}
function msmDirectCurveTo(Points: TPointsLogic; Direct:TDirectionCurveKind): boolean;
begin
 result:= false;
 if Length(Points)<3 then exit;
 if msmDirectionOf(Points) <> Direct then begin
  msmReDirectCurve(Points);
  Result:= true;
 end;
end;
{------------------------------------------------------------------------------}
procedure msmGetWeightCenterOf(Points: TPointsLogic; var PointWC: TMSMPoint);
var
 xs,ys : double;
 i : integer;
begin
 xs := 0.0;
 ys := 0.0;
 if Length(Points)=0 then exit;
 for i := 0 to high(Points) do begin
  xs := xs + Points[i].x;
  ys := ys + Points[i].y;
 end;
 if Assigned(PointWC) then begin
  PointWC.x:=  xs/length(Points);
  PointWC.y:=  ys/Length(Points);
 end;
end;
{------------------------------------------------------------------------------}
end.
