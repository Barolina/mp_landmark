unit fmAllDocuments;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Buttons, Vcl.ComCtrls, Vcl.StdCtrls,
  Vcl.ExtCtrls;

type
  TformAllDocuments = class(TForm)
    pnl1: TPanel;
    edtSearch: TEdit;
    tvDocuments: TTreeView;
    btnOk: TSpeedButton;
    btnCancel: TSpeedButton;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  formAllDocuments: TformAllDocuments;

implementation

{$R *.dfm}

end.
