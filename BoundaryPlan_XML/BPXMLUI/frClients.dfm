object FrameClients: TFrameClients
  Left = 0
  Top = 0
  Width = 588
  Height = 276
  TabOrder = 0
  object PanelTopControl: TPanel
    Left = 0
    Top = 0
    Width = 51
    Height = 276
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 0
    object StatuseSubParcel_Lab: TLabel
      Left = 248
      Top = 8
      Width = 60
      Height = 13
      Caption = '                    '
    end
    object SubParcels_Actions_ButtonGroup: TButtonGroup
      Left = 0
      Top = 0
      Width = 51
      Height = 26
      Align = alTop
      BevelInner = bvNone
      BevelOuter = bvNone
      BorderStyle = bsNone
      ButtonHeight = 25
      ButtonWidth = 25
      DockSite = True
      DoubleBuffered = True
      Images = formContainer.ilCommands
      Items = <
        item
          Caption = '+'
          Hint = #13#10#1044#1086#1073#1072#1074#1080#1090#1100' '#1079#1072#1082#1072#1079#1095#1080#1082#1072' '#1082#1072#1076'. '#1088#1072#1073'.'#13#10
          ImageIndex = 0
          OnClick = SubParcels_Actions_ButtonGroupItems0Click
        end
        item
          Caption = '-'
          Hint = #13#10#1044#1086#1073#1072#1074#1080#1090#1100' '#1079#1072#1082#1072#1079#1095#1080#1082#1072' '#1082#1072#1076'. '#1088#1072#1073'.'#13#10
          ImageIndex = 1
          OnClick = SubParcels_Actions_ButtonGroupItems1Click
        end>
      ParentDoubleBuffered = False
      ShowHint = True
      TabOrder = 0
    end
    object cbClientList: TListBox
      Left = 0
      Top = 26
      Width = 51
      Height = 250
      Hint = #1057#1087#1080#1089#1086#1082' '#1082#1083#1080#1077#1085#1090#1086#1074
      Style = lbOwnerDrawVariable
      Align = alClient
      BevelEdges = [beLeft, beRight, beBottom]
      BevelKind = bkFlat
      BorderStyle = bsNone
      Color = 15921123
      ItemHeight = 13
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = cbClientListChange
    end
  end
  object Panel: TPanel
    Left = 51
    Top = 0
    Width = 537
    Height = 276
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
  end
end
