unit frSubParContours;
{
  ������� �������������� �����
}
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ActnList, ButtonGroup, ExtCtrls,
  VCL.clipbrd,  Vcl.ComCtrls, Vcl.ToolWin, JvExComCtrls, JvToolBar,
  ImgList, System.Actions,

  STD_MP,
  fmContainer, unRussLang,
  FrPreloader,
  frSubParContour;
  
type
  TFrameSubParContours = class(TFrame)
    actlst: TActionList;
    Contours_List_ListBox: TListBox;
    actContrours_AddNew: TAction;
    actContours_DeleteCurrent: TAction;
    actClipBrd_Paste: TAction;
    actClipBrd_Copy: TAction;
    actContour_next: TAction;
    actContour_prev: TAction;
    actValid: TAction;
    pnlAttribute_Panel: TPanel;
    pnl1: TPanel;
    pnlAction: TPanel;
    btnEdCurContour: TButtonedEdit;
    cbbContoursType: TComboBox;
    jvtlbr2: TJvToolBar;
    btnContour_prev1: TToolButton;
    btnContour_next1: TToolButton;
    btnContrours_AddNew1: TToolButton;
    btnContours_DeleteCurrent1: TToolButton;
    btnClipBrd_Copy1: TToolButton;
    btnClipBrd_Paste1: TToolButton;
    btnValid1: TToolButton;
    pnl2: TPanel;
    pnl3: TPanel;

    procedure actContrours_AddNewExecute(Sender: TObject);
    procedure actContours_DeleteCurrentExecute(Sender: TObject);
    procedure Action_Contours_SetCurrentExecute(Sender: TObject);
        procedure Contours_List_ListBoxMouseLeave(Sender: TObject);
    procedure btnEdCurContourRightButtonClick(Sender: TObject);
    procedure Contours_List_ListBoxKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure btnEdCurContourKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure btnEdCurContourExit(Sender: TObject);

    procedure cbbContoursTypeChange(Sender: TObject);
    procedure actClipBrd_CopyExecute(Sender: TObject);
    procedure actClipBrd_PasteExecute(Sender: TObject);
    procedure actContour_nextExecute(Sender: TObject);
    procedure actContour_prevExecute(Sender: TObject);
    procedure actValidExecute(Sender: TObject);
    procedure btnEdCurContourDblClick(Sender: TObject);
  public type
   TContourKind = (cUndefine,cNewContour);

  private
   { Private declarations }
   FXmlContours            : IXMLTSubParcel_Contours;                           
   FContour_frame          : TFrameSubParContour;
   
   procedure ContourNewListDorefresh(A,B : Integer);
   procedure Contours_List_DoRefresh(A: Integer = -1; B: Integer = -1);

   procedure Contours_List_DoSetCurrentItem(IndexItem: integer); 

   procedure Contours_List_DoUpDate;
   procedure Contours_List_DoAddNewItem;
   procedure Contours_List_DoDeleteItem(IndexItem: Integer);

   procedure OnChange_Attribute_DefinitionOrNumberPP(sender: Tobject);
   procedure _setContourKind(aValue: TContourKind);
  protected
   procedure InitializationUI;
   procedure SetContours(const aContours: IXMLTSubParcel_Contours);

   procedure SetActivContourUIIndex(index: integer);
   function  GetActivContourUIIndex: integer;

   procedure setActivContourKind(aValue: TContourKind);
   function  getActivContourKind: TContourKind;
  public
   { Public declarations }
   constructor Create(Owner: TComponent); override;

   function IsAssignedContours: Boolean;

   procedure getAsText(aContours: TContourKind; var OutputText: string);
   procedure setAsText(aContours: TContourKind; InputText: string);

   procedure ContoursList_Show;
   procedure ContoursList_Hide;

   property ActivContourUIIndex: integer read getActivContourUIIndex write setActivContourUIIndex;
   property ActivContourKind: TContourKind read getActivContourKind write setActivContourKind;

   property Contour: TFrameSubParContour read FContour_frame;
   property XmlContours: IXMLTSubParcel_Contours read FXmlContours write SetContours;
  end;

implementation

uses MsXMLAPI, SurveyingPlan.DataFormat, SurveyingPlan.Xml.Utils,
  SurveyingPlan.Xml.Types, fmLog;

{$R *.dfm}

{ TFrameContours }

procedure TFrameSubParContours.actClipBrd_CopyExecute(Sender: TObject);
var
 _st: string;
begin
 self.getAsText(self.ActivContourKind,_st);
 Clipboard.Open;
 TRY
  Clipboard.AsText:= _st;
 FINALLY
  Clipboard.Close;
 END;
end;

procedure TFrameSubParContours.actClipBrd_PasteExecute(Sender: TObject);
var
 _st: string;
begin
 Clipboard.Open;
 TRY
     TFrLoader.Inctance.RunAnimation := true;
     TRY
     _st:= Clipboard.AsText;
     FINALLY
      Clipboard.Close;
     END;
     self.setAsText(self.ActivContourKind,_st);
 finally    
     TFrLoader.Inctance.RunAnimation  := false;
 end;
end;

procedure TFrameSubParContours.actContour_nextExecute(Sender: TObject);
begin
 self.ActivContourUIIndex:= self.ActivContourUIIndex+1;
end;

procedure TFrameSubParContours.actContour_prevExecute(Sender: TObject);
begin
self.ActivContourUIIndex:= self.ActivContourUIIndex-1;
end;

procedure TFrameSubParContours.actContours_DeleteCurrentExecute(Sender: TObject);
begin
 if not IsAssignedContours then EXIT;
 if self.Contours_List_ListBox.ItemIndex=-1 then EXIT;
 if MessageDlg('������� ������: '+'��������� �����'+'?',mtInformation,mbYesNo,-1) = mrNo then Exit;

 self.Contours_List_DoDeleteItem(self.Contours_List_ListBox.ItemIndex);
 self.SetActivContourUIIndex(self.Contours_List_ListBox.ItemIndex);
 if not self.FContour_frame.IsAssignedContour then
  self.SetActivContourUIIndex(0);
end;

procedure TFrameSubParContours.Action_Contours_SetCurrentExecute(Sender: TObject);
begin
 self.ActivContourUIIndex:= self.Contours_List_ListBox.ItemIndex;
end;

procedure TFrameSubParContours.actContrours_AddNewExecute(Sender: TObject);
begin
 if not self.IsAssignedContours then EXIT;
 self.Contours_List_DoAddNewItem;
 self.SetActivContourUIIndex(self.Contours_List_ListBox.Count-1);
end;

procedure TFrameSubParContours.actValidExecute(Sender: TObject);
var
 _st: string;
 _mcontour: TSpMContour;
 _log: TStringBuilder;
 _dlog: TformLog;
begin
 _mcontour:= TSpMContour.Create;
 if self.ActivContourKind = cNewContour then
    TSpConvert.MContour.Convert(self.FXmlContours,_mcontour);
 _log:= TStringBuilder.Create;
 _dlog:= TformLog.Create(nil);
 if not TSpValidator.Validate(_mcontour,_log) then
  _dlog.TextLog:= _log.ToString
 else
  _dlog.TextLog:= '������ �� ����������';
 _dlog.ShowModal;
 _log.Free;
 _dlog.Free;


end;

procedure TFrameSubParContours.btnEdCurContourDblClick(Sender: TObject);
var
    nameZU : string;
    index  : integer;
begin
 nameZU:= InputBox('�������� ����������� ��� ������� ����� ������� �����','����������� ��� ������� ����� ������� �����','');
 if nameZU = ''  then Exit;
 index := Self.Contours_List_ListBox.ItemIndex;
 if index = -1  then Exit;
 case Self.ActivContourKind of
   cUndefine: ;
   cNewContour: FXmlContours.Contour[index].Number := nameZU;
 end;
 Self.Contours_List_ListBox.Items[index] := nameZU;
 self.SetActivContourUIIndex(index);
end;

procedure TFrameSubParContours.btnEdCurContourExit(Sender: TObject);
var
 _index: integer;
begin
 if TryStrToInt(self.btnEdCurContour.Text,_index) then
  self.ActivContourUIIndex:= _index-1
 else
  self.ActivContourUIIndex:= self.Contours_List_ListBox.ItemIndex;
end;

procedure TFrameSubParContours.btnEdCurContourKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if key = VK_RETURN then
  self.SetFocus;
end;

procedure TFrameSubParContours.btnEdCurContourRightButtonClick(Sender: TObject);
begin
 if self.Contours_List_ListBox.Visible then
  self.ContoursList_Hide
 else
  self.ContoursList_Show;
end;

procedure TFrameSubParContours.ContourNewListDorefresh(A,B : Integer);
var
 _Contour: IXMLTSubParcel_Contours_Contour;
 _i,_j: Integer;
 _ItemName: string;
begin
 if self.Contours_List_ListBox.Items.Count = 0 then EXIT;

 if (A<0) or (a>=self.Contours_List_ListBox.Items.Count)  then
  a:= 0;

 if (B<A) or (B>=self.Contours_List_ListBox.Items.Count)  then
  B:= self.Contours_List_ListBox.Items.Count-1;

 _j:= self.Contours_List_ListBox.ItemIndex;
 for _i := A to B do begin
  _Contour:= IXMLTSubParcel_Contours_Contour(pointer(self.Contours_List_ListBox.Items.Objects[_i]));
 _ItemName:= MsXml_ReadAttribute(_Contour,cnstAtrNumber);
  self.Contours_List_ListBox.Items.Strings[_i]:= _ItemName;
 end;
 self.Contours_List_ListBox.ItemIndex:= _j;
end;

procedure TFrameSubParContours.Contours_List_DoRefresh(A, B: Integer);
begin
  if self.ActivContourKind = cNewContour  then ContourNewListDorefresh(A,B);
end;

procedure TFrameSubParContours.Contours_List_DoSetCurrentItem(IndexItem: integer);
var
 _contour: IXMLTSubParcel_Contours_Contour;
begin
  if self.ActivContourKind =cNewContour then 
  begin
       _contour:= IXMLTSubParcel_Contours_Contour(pointer(self.Contours_List_ListBox.Items.Objects[IndexItem]));
        self.FContour_frame.XmlContour:= _contour;
  end;
end;

procedure TFrameSubParContours.SetActivContourUIIndex(index: integer);
begin
 if (index>-1) and (index<self.Contours_List_ListBox.Count) then begin
  self.Contours_List_DoSetCurrentItem(index);
  self.Contours_List_ListBox.ItemIndex:= index;
  if Self.ActivContourKind = cNewContour  then
    self.btnEdCurContour.Text:= self.FXmlContours.Contour[index].Number;
  self.FContour_frame.Visible:= True;
 end else begin
  self.FContour_frame.Visible:= false;
  self.Contours_List_ListBox.ItemIndex:= -1;
  self.btnEdCurContour.Text:='';
  if self.ActivContourKind = cNewContour then
    self.FContour_frame.XmlContour := nil;
 end;
end;

constructor TFrameSubParContours.Create(Owner: TComponent);
begin
 inherited;
 self.FContour_frame:= TFrameSubParContour.Create(self.pnl3);
 self.FContour_frame.OnChange_Attribute_DefOnNum:= self.OnChange_Attribute_DefinitionOrNumberPP;
 self.FContour_frame.Parent:= self.pnl3;
 self.FContour_frame.Align:= alClient;
 self.FContour_frame.Visible:= False;
end;

function TFrameSubParContours.GetActivContourUIIndex: integer;
begin
 Result:= self.Contours_List_ListBox.ItemIndex;
end;

procedure TFrameSubParContours.getAsText(aContours: TContourKind; var OutputText: string);
var
 _mcontour: TSpMContour;
begin
 _mcontour:= TSpMContour.Create;
 TRY
  case aContours of
    cUndefine: ;
    cNewContour: TSpConvert.MContour.Convert(self.FXmlContours,_mcontour);
  end;
  OutputText:= DataFormat.MContour.Text.Build(_mcontour);
 FINALLY
  _mcontour.Free;
 END;
end;

function TFrameSubParContours.getActivContourKind: TContourKind;
begin
 if self.cbbContoursType.ItemIndex in [0..2] then
  Result:= TContourKind(self.cbbContoursType.ItemIndex+1)
 else
  Result:= cUndefine;
end;

procedure TFrameSubParContours.InitializationUI;
begin
 self.Contours_List_ListBox.Clear;
 if self.IsAssignedContours then begin
  self.Contours_List_DoUpDate;
  self.SetActivContourUIIndex(0);
 end;
end;

function TFrameSubParContours.IsAssignedContours: Boolean;
begin
 Result:= self.FXmlContours<>nil;
end;

procedure TFrameSubParContours.OnChange_Attribute_DefinitionOrNumberPP(sender: tobject);
begin
 self.Contours_List_DoRefresh(self.Contours_List_ListBox.ItemIndex,self.Contours_List_ListBox.ItemIndex);
end;

procedure TFrameSubParContours.setAsText(aContours: TContourKind;InputText: string);
var
 _mcontour: TSpMContour;
 _log: TStringBuilder;
 _dlog: TformLog;
begin
 _mcontour:= nil;
 _log:= TStringBuilder.Create;
 TRY
  _mcontour:= DataFormat.MContour.Text.Parse(InputText);
  _mcontour._checkDirection;
  if not TSpValidator.Validate(_mcontour,_log) then begin
  _dlog:= TformLog.Create(nil);
  _dlog.TextLog:= _log.ToString;
  FreeAndNil(_log);
  _dlog.ShowModal;
  if MessageDlg('������������ ��������� ������?',mtConfirmation,mbYesNo,-1) = mrNo then begin
   _dlog.Free;
   EXIT;
  end;
  _dlog.Free;
 end;
 FreeAndNil(_log);

  case aContours of
    cUndefine: ;
    cNewContour: TSpConvert.MContour.Convert(_mcontour,self.FXmlContours);
  end;

 FINALLY
  _mcontour.Free;
  _log.Free;
 END;
 InitializationUI;
end;

procedure TFrameSubParContours.SetContours(const aContours: IXMLTSubParcel_Contours);
begin
 if self.FXmlContours=aContours then EXIT;
 self.SetActivContourUIIndex(-1);
 self.FXmlContours:= aContours;
 self.InitializationUI;
end;

procedure TFrameSubParContours.setActivContourKind(aValue: TContourKind);
begin
  self._setContourKind(aValue);
  if self.IsAssignedContours then 
  begin
   if aValue = cNewContour then
   begin
      self.FContour_frame.SetVisibleTypeCOntoursForm(true);
      self.FContour_frame.AsAttributeContour(true);
   end;
   self.Contours_List_DoUpDate;
  end;
end;

procedure TFrameSubParContours._setContourKind(aValue: TContourKind);
begin
 case aValue of
   cUndefine: self.cbbContoursType.ItemIndex:=-1;
   cNewContour: self.cbbContoursType.ItemIndex:=0;
 end;
end;

procedure TFrameSubParContours.cbbContoursTypeChange(Sender: TObject);
begin
  self.setActivContourKind(self.getActivContourKind);
end;

procedure TFrameSubParContours.ContoursList_Hide;
begin
 self.Contours_List_ListBox.Visible:= False;
end;

procedure TFrameSubParContours.ContoursList_Show;
begin
 self.Contours_List_ListBox.Parent:= nil;
 self.Contours_List_ListBox.Parent:= pnl2;
 self.Contours_List_ListBox.Visible:= true;
 self.Contours_List_ListBox.SetFocus;
end;

procedure TFrameSubParContours.Contours_List_DoAddNewItem;
var
 _contour: IXMLTSubParcel_Contours_Contour;
 _ItemName: string;
begin
 if self.ActivContourKind = cNewContour  then
  begin
     _contour:= self.FXmlContours.Add;
     _ItemName:= IntToStr(self.FXmlContours.Count);
     _contour.Entity_Spatial.Spatial_Element.Add;
     self.Contours_List_ListBox.AddItem(_ItemName,pointer(_contour));
  end;
end;


procedure TFrameSubParContours.Contours_List_DoDeleteItem(IndexItem: Integer);
var
 _contour: IXMLTSubParcel_Contours_Contour;
 _index: Integer;
begin
 if self.ActivContourKind = cNewContour then
 begin
     _contour:= IXMLTSubParcel_Contours_Contour(pointer(self.Contours_List_ListBox.Items.Objects[IndexItem]));
     if self.FContour_frame.XmlContour=_contour then
      self.SetActivContourUIIndex(IndexItem-1);
     self.Contours_List_ListBox.Items.Delete(IndexItem);
     self.FXmlContours.Remove(_contour);
     _contour:= nil;
 end;
end;

procedure TFrameSubParContours.Contours_List_DoUpDate;
var
 _i, _index: Integer;
 _Contour: IXMLTSubParcel_Contours_Contour;
 _ItemName: string;
begin
 {���������� ��� ������� }
 if self.ActivContourKind = cUndefine then
 if FXmlContours.ChildNodes.FindNode(cnstXmlNewContour) <> nil  then
   self.ActivContourKind := cNewContour;
 _index:= self.Contours_List_ListBox.ItemIndex;
 self.Contours_List_ListBox.Clear;
 if self.ActivContourKind = cNewContour then
     for _i := 0 to self.FXmlContours.Count-1 do begin
      _Contour:= self.FXmlContours.Contour[_i];
     _ItemName:= MsXml_ReadAttribute(_Contour,cnstAtrNumber);
      _Contour.Number := _ItemName;
      self.Contours_List_ListBox.AddItem(_ItemName,pointer(_Contour));
     end;
 self.SetActivContourUIIndex(_index);
 if not self.FContour_frame.IsAssignedContour then
 self.SetActivContourUIIndex(0);
end;

procedure TFrameSubParContours.Contours_List_ListBoxKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
//  case key of
//   VK_ESCAPE: self.Contours_List_ListBox.Visible:= false;
//   VK_RETURN: self.Contours_List_ListBox.Visible:= false;
//  end;

end;

procedure TFrameSubParContours.Contours_List_ListBoxMouseLeave(Sender: TObject);
begin
// self.Contours_List_ListBox.Visible:= false;
end;

end.
