unit frSubParcelsNewParcel;
{
  NewParcelParcel -> SubParcels
}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ActnList, ButtonGroup, StdCtrls, ExtCtrls,

  STD_MP, frSubParcel, BpXML_different, ImgList, BPCommon, System.Actions,
  Vcl.CheckLst, Vcl.Buttons, Vcl.ComCtrls, Vcl.ToolWin,
  unRussLang,FrTypeParcel;

type

  TFrameSubParcelsNewparcel = class(TFrame)
    PanelSubParcelFrame: TPanel;
    ActionList: TActionList;
    Action_AddNewSubParcel: TAction;
    Action_DeleteSubParcel: TAction;
    Action_SelectSubParcel: TAction;
    ImageList: TImageList;
    StatuseSubParcel_Lab: TLabel;
    pnlSubParcel: TPanel;
    Splitter1: TSplitter;
    Splitter2: TSplitter;
    Panel: TPanel;
    SubParcels_Actions_ButtonGroup: TButtonGroup;
    BHint: TBalloonHint;
    lbListChZU: TComboBox;
    GroupBox1: TGroupBox;

    procedure Action_AddNewSubParcelExecute(Sender: TObject);
    procedure Action_DeleteSubParcelExecute(Sender: TObject);
    procedure Action_DelAllSubParcelsExecute(Sender: TObject);
    procedure Action_SelectSubParcelExecute(Sender: TObject);
    procedure SubParcels_ComboBoxChange(Sender: TObject);

    procedure SubParcelExistAddNew;
    procedure lbListChZU_Click(Sender: TObject);
    procedure lbTypeSybParcels_MouseLeave(Sender: TObject);
    procedure lbDocumentsClick(Sender: TObject);
    procedure tBtnFormParcelClick(Sender: TObject);
    procedure tlbtnExistparcelClick(Sender: TObject);
    procedure tlbtnInvariableParcelClick(Sender: TObject);
  private
    { Private declarations }
   FXmlSubParcels  : IXMLTNewParcel_SubParcels;
   FSubParcel_Frame: TFrameSubParcel;

   isTypeLandSubParcel  : integer;// �������� ��������������(�� ���������)  ��� ������

   procedure Data_SubParcel_DoAddNew;

   {Delete_________________________________________}
   procedure Data_FormSub_DoDelete(index : integer);
   procedure Data_ExistSub_DoDelete(index : integer);
   procedure Data_InvariableSub_DoDelete(index : integer);
   procedure Data_SubParcel_DoDelete(index: Integer);

   {Update_________________________________________}
   procedure FormsubParcelList_DoUpDate;
   procedure ExistParcelList_DoUpDate;
   procedure InvariableParcelList_DoUpDate;
   procedure SubParcelsList_DoUpDate;

   procedure SubParcelsList_DoRefresh(A: Integer = -1; B: Integer = -1);

   procedure OnChange_Attribute_DefinitionOrNUmberPP(Sender: TObject);
   procedure OnUpdateAttributeSubParcelForm(index : integer);
   function  GetIndexButtonTool() : integer;
   procedure RefreshNameControl();

   procedure FormSubParcelList_SetCurrent(index: Integer);
   procedure ExsistSubParcelList_SetCurrent(index : Integer);
   procedure InvariableSubparcelList_SetCurrent(index : Integer);
  protected
   procedure SetSubParcels(const aSubParcels: IXMLTNewParcel_SubParcels);
   procedure InitializationUI;
  public
    { Public declarations }
   constructor Create(Owner: TComponent); override;
   function IsAssigned_SubParcels: Boolean;
   procedure SubParcelsList_UpDate;

   procedure SubParcelsList_SetCurrent(index: Integer);

   property XmlSubParcels: IXMLTNewParcel_SubParcels read FXmlSubParcels write SetSubParcels;
   property SubParcel: TFrameSubParcel read FSubParcel_Frame;
  end;

implementation

uses MsXMLAPI;

const
     cnstFormSubParcel       = 'FormSubParcel';
     cnstExistSubParcel      = 'ExistSubParcel';
     cnstInvariableSubParcel = 'InvariableSubParcel';
     cnstNameFormSubParcel   = '��������� ';
     cnstNameExistSubParcel  = '����������, ���������� ';
     cnstNameInvariableSubParcel  = '������������ ';
{$R *.dfm}

{ TFrameSubParcels }

function TFrameSubParcelsNewparcel.GetIndexButtonTool() : integer;
begin
end;

procedure TFrameSubParcelsNewparcel.Action_AddNewSubParcelExecute(Sender: TObject);
begin
 if not self.IsAssigned_SubParcels then EXIT;
if GetIndexButtonTool = 2 then

 begin
  isTypeLandSubParcel := -1;
  Self.Data_SubParcel_DoAddNew;
  self.SubParcelsList_SetCurrent(self.lbListChZU.ItemIndex);
 end
 else
 begin
       isTypeLandSubParcel := FrTypeParcel.frmTypeParcel.GetTypeObject;
        Self.Data_SubParcel_DoAddNew;
        self.SubParcelsList_SetCurrent(self.lbListChZU.ItemIndex);
 end;
end;

procedure TFrameSubParcelsNewparcel.Action_DelAllSubParcelsExecute(Sender: TObject);
begin
 if not IsAssigned_SubParcels then EXIT;
 self.FXmlSubParcels.Clear;
 self.SubParcelsList_DoUpDate;
end;

procedure TFrameSubParcelsNewparcel.Action_DeleteSubParcelExecute(Sender: TObject);
begin
 if not self.FSubParcel_Frame.IsAssignedSubParcel then EXIT;
  if (lbListChZU.ItemIndex > -1) then
    if MessageDlg('������� ��������� �������?', mtConfirmation,
      [mbYes, mbNo], -1) = mrYes then
    begin
        self.Data_SubParcel_DoDelete(self.lbListChZU.ItemIndex);
        self.SubParcelsList_SetCurrent(self.lbListChZU.ItemIndex);
        if not self.FSubParcel_Frame.IsAssignedSubParcel then
        self.SubParcelsList_SetCurrent(0);
    end;
end;

procedure TFrameSubParcelsNewparcel.Action_SelectSubParcelExecute(Sender: TObject);
begin
 self.SubParcelsList_SetCurrent(self.lbListChZU.ItemIndex);
end;

constructor TFrameSubParcelsNewparcel.Create(Owner: TComponent);
begin
  inherited;
  self.InitializationUI;
  self.FSubParcel_Frame:= TFrameSubParcel.Create(self.PanelSubParcelFrame);
  self.FSubParcel_Frame.IsTypeSubParcel := 0 ; //FormSubParcel �����������
  self.FSubParcel_Frame.Parent:= self.PanelSubParcelFrame;
  self.FSubParcel_Frame.Align:= alClient;
  self.FSubParcel_Frame.OnChange_Attribute_DefOnNum:= self.OnChange_Attribute_DefinitionOrNUmberPP;
  self.FSubParcel_Frame.Visible:= false;
end;

procedure TFrameSubParcelsNewparcel.InitializationUI;
begin
 self.lbListChZU.Clear;
    //--���������� ��� SubParcel
 if FXmlSubParcels  = nil    then Exit;
 RefreshNameControl;
end;


procedure TFrameSubParcelsNewparcel.InvariableParcelList_DoUpDate;
begin
end;

procedure TFrameSubParcelsNewparcel.InvariableSubparcelList_SetCurrent(index: Integer);
begin
end;

function TFrameSubParcelsNewparcel.IsAssigned_SubParcels: Boolean;
begin
 Result:= self.FXmlSubParcels<>nil;
end;

procedure TFrameSubParcelsNewparcel.lbDocumentsClick(Sender: TObject);
begin
  self.SubParcelsList_DoUpDate;
end;

procedure TFrameSubParcelsNewparcel.lbListChZU_Click(Sender: TObject);
begin
 self.SubParcelsList_SetCurrent(self.lbListChZU.ItemIndex);
end;

procedure TFrameSubParcelsNewparcel.lbTypeSybParcels_MouseLeave(Sender: TObject);
  var xy : TPoint;
begin
end;

procedure TFrameSubParcelsNewparcel.OnChange_Attribute_DefinitionOrNUmberPP(Sender: TObject);
begin
 self.SubParcelsList_DoRefresh(self.lbListChZU.ItemIndex, self.lbListChZU.ItemIndex);
end;

procedure TFrameSubParcelsNewparcel.OnUpdateAttributeSubParcelForm(index: integer);
begin
 case index  of
   0 : begin
         StatuseSubParcel_Lab.Caption := '�������� �������������� ������';
         isTypeLandSubParcel := 0;
   end;
   1 : begin
         //FSubParcel_Frame.IsTypeLandSubParcel := 1;
         StatuseSubParcel_Lab.Caption:= '������� �������������� �����';
         isTypeLandSubParcel := 1;
   end;
   2 : begin
          isTypeLandSubParcel := -1; //���  ������ ���� ���������� �������
          //FSubParcel_Frame.IsTypeLandSubParcel := -1;
          StatuseSubParcel_Lab.Caption := '';
   end;
 end;
end;

procedure TFrameSubParcelsNewparcel.RefreshNameControl;
begin
end;
procedure TFrameSubParcelsNewparcel.SetSubParcels(const aSubParcels: IXMLTNewParcel_SubParcels);
begin
 self.SubParcelsList_SetCurrent(-1);
 self.FXmlSubParcels:= aSubParcels;
 self.InitializationUI;
 if self.IsAssigned_SubParcels then
  self.SubParcelsList_DoUpDate;
end;


procedure TFrameSubParcelsNewparcel.SubParcelsList_SetCurrent(index: Integer);
begin
 if (index>-1) and (index<self.lbListChZU.Items.Count) then
 begin
      FormSubParcelList_SetCurrent(index);
      self.FSubParcel_Frame.Visible:= true;
      self.lbListChZU.ItemIndex:= index;
  end
 else
 begin
    self.FSubParcel_Frame.Visible:= false;
    self.FSubParcel_Frame.XmlSubParcel:= nil;
    self.lbListChZU.ItemIndex:= -1;
 end;
end;


procedure TFrameSubParcelsNewparcel.SubParcelExistAddNew;
var
 _SubParcelExist: IXMLTNewParcel_SubParcels_FormSubParcel;
 Entity_Spatial : IXMLEntity_Spatial;
 _ItemName: string;
begin
     _SubParcelExist:= self.FXmlSubParcels.Add;
     //���� ��������������� ��� ����� �����
      _ItemName:= IntToStr(self.FXmlSubParcels.Count);
     //����� ��������� ������� ��������������� ��������� or Contours
      if isTypeLandSubParcel = 0   then
       // _SubParcelExist.Entity_Spatial.Spatial_Element.Add
       Entity_Spatial := _SubParcelExist.Entity_Spatial
      else
       _SubParcelExist.Contours.Add.Number := '1';
     _SubParcelExist.SubParcel_Realty := false;
     _SubParcelExist.Definition := _ItemName;
      self.lbListChZU.AddItem(_ItemName,pointer(_SubParcelExist));
      self.lbListChZU.ItemIndex := self.lbListChZU.Items.Count -1;
      Entity_Spatial := nil;
end;

procedure TFrameSubParcelsNewparcel.SubParcelsList_DoRefresh(A: Integer = -1; B: Integer = -1);
var
 _SubParcel: IXMLTNewParcel_SubParcels_FormSubParcel;
 _i,_j: Integer;
 _ItemName: string;
begin
 if self.lbListChZU.Items.Count = 0 then EXIT;

 if (A<0) or (a>=self.lbListChZU.Items.Count)  then
  a:= 0;

 if (B<A) or (B>=self.lbListChZU.Items.Count)  then
  B:= self.lbListChZU.Items.Count-1;
 _j:= self.lbListChZU.ItemIndex;

       for _i := A to B do
       begin
           _SubParcel := IXMLTNewParcel_SubParcels_FormSubParcel
           (pointer(self.lbListChZU.Items.Objects[_i]));
          _ItemName:=  MsXml_ReadAttribute(_SubParcel,cnstAtrDefinition);
          self.lbListChZU.Items.Strings[_i]:= _ItemName;
       end;
 self.lbListChZU.ItemIndex:= _j;
end;

procedure TFrameSubParcelsNewparcel.SubParcelsList_DoUpDate;
var
 _index: Integer;
 _i: Integer;
 _SubParcel: IXMLTNewParcel_SubParcels_FormSubParcel;
 _ItemName: string;
begin
 _index := lbListChZU.ItemIndex;
  FormsubParcelList_DoUpDate
end;


procedure TFrameSubParcelsNewparcel.SubParcelsList_UpDate;
begin
 if self.IsAssigned_SubParcels then
  self.SubParcelsList_DoUpDate;
end;


procedure TFrameSubParcelsNewparcel.SubParcels_ComboBoxChange(Sender: TObject);
begin
 // isTypeSubparcels := lbTypeSybParcels.ItemIndex;
  self.SubParcelsList_DoUpDate;
end;

procedure TFrameSubParcelsNewparcel.tBtnFormParcelClick(Sender: TObject);
begin
  self.SubParcelsList_DoUpDate;
end;

procedure TFrameSubParcelsNewparcel.tlbtnExistparcelClick(Sender: TObject);
begin
  self.SubParcelsList_DoUpDate;
end;

procedure TFrameSubParcelsNewparcel.tlbtnInvariableParcelClick(Sender: TObject);
begin
  self.SubParcelsList_DoUpDate;
end;

procedure TFrameSubParcelsNewparcel.Data_ExistSub_DoDelete(index: integer);
var
 _SubParcelExist: IXMLTNewParcel_SubParcels_FormSubParcel;
begin
  _SubParcelExist:= IXMLTNewParcel_SubParcels_FormSubParcel
                   (pointer(self.lbListChZU.Items.Objects[index]));
   if self.FSubParcel_Frame.XmlNewParcelSubParcel=_SubParcelExist then
   self.SubParcelsList_SetCurrent(index-1);
   self.lbListChZU.Items.Delete(index);
   self.FXmlSubParcels.Remove(_SubParcelExist);
  _SubParcelExist:= nil;
end;

procedure TFrameSubParcelsNewparcel.Data_FormSub_DoDelete(index: integer);
var
 _SubParcel: IXMLTNewParcel_SubParcels_FormSubParcel;
begin
   _SubParcel:= IXMLTNewParcel_SubParcels_FormSubParcel
               (pointer(self.lbListChZU.Items.Objects[index]));
   if self.FSubParcel_Frame.XmlNewParcelSubParcel=_SubParcel then
        self.SubParcelsList_SetCurrent(index-1);
   self.lbListChZU.Items.Delete(index);
   self.FXmlSubParcels.Remove(_SubParcel);
   _SubParcel:= nil;
end;

procedure TFrameSubParcelsNewparcel.Data_InvariableSub_DoDelete(index: integer);
begin
end;

procedure TFrameSubParcelsNewparcel.Data_SubParcel_DoAddNew;
var
 _SubParcel: IXMLTNewParcel_SubParcels_FormSubParcel;
 _Contour : IXMLTSubParcel_Contours_Contour;
 _Entity_Spatial : IXMLEntity_Spatial;
 _ItemName: string;
begin
         _SubParcel:= self.FXmlSubParcels.Add;
         //���� ��������������� ��� ����� �����
          _ItemName:= cBpDefinitionPart+IntToStr(self.FXmlSubParcels.Count);
          _SubParcel.Definition := _ItemName;
          if isTypeLandSubParcel = 0  then
             _Entity_Spatial := _SubParcel.Entity_Spatial
          else
          begin
              _Contour:= _SubParcel.Contours.Add;
              _Contour.Number :='1';
          end;
         _SubParcel.SubParcel_Realty := false;
          self.lbListChZU.AddItem(_ItemName,pointer(_SubParcel));
          self.lbListChZU.ItemIndex := self.lbListChZU.Items.Count -1;
          _Entity_Spatial := nil;
end;

procedure TFrameSubParcelsNewparcel.Data_SubParcel_DoDelete(index: Integer);
var
 _SubParcel: IXMLTNewParcel_SubParcels_FormSubParcel;
begin
 if FXmlSubParcels.ChildNodes.Get(index).NodeName = cnstFormSubParcel then
     Data_FormSub_DoDelete(index)
end;

procedure TFrameSubParcelsNewparcel.ExistParcelList_DoUpDate;
var
 _i: Integer;
 _SubParcelExist: IXMLTNewParcel_SubParcels_FormSubParcel;
 _ItemName: string;
begin
 for _i := 0 to self.FXmlSubParcels.Count-1 do
 begin
     _SubParcelExist:= self.FXmlSubParcels.FormSubParcel[_i];
    _ItemName:= MsXml_ReadAttribute(_SubParcelExist,cnstAtrNumber_Record);
    _SubParcelExist.Definition  := _ItemName;
    _SubParcelExist.SubParcel_Realty := _SubParcelExist.SubParcel_Realty;
     self.lbListChZU.AddItem(_ItemName,pointer(_SubParcelExist));
 end
end;

procedure TFrameSubParcelsNewparcel.ExsistSubParcelList_SetCurrent(index: Integer);
begin
  OnUpdateAttributeSubParcelForm(0);
  if FXmlSubParcels.FormSubParcel[index].ChildNodes.FindNode('Contours') <> nil  then
     OnUpdateAttributeSubParcelForm(1);
  self.FSubParcel_Frame.XmlNewParcelSubParcel:= FXmlSubParcels.FormSubParcel[index];
end;

procedure TFrameSubParcelsNewparcel.FormsubParcelList_DoUpDate;
var _i : Integer;
    _SubParcel  : IXMLTNewParcel_SubParcels_FormSubParcel;
    _ItemName   : string;
    _index      : Integer;
begin
 self.lbListChZU.Items.Clear;
 _index := lbListChZU.ItemIndex;
 for _i := 0 to self.FXmlSubParcels.Count-1 do
 begin
     _SubParcel:= self.FXmlSubParcels.FormSubParcel[_i];
     _ItemName:= MsXml_ReadAttribute(_SubParcel,cnstAtrDefinition);
     _SubParcel.Definition := _ItemName;
     _SubParcel.SubParcel_Realty  := _SubParcel.SubParcel_Realty;
      self.lbListChZU.AddItem(_ItemName,pointer(_SubParcel));
 end;
 self.SubParcelsList_SetCurrent(_index-1);
 if not self.FSubParcel_Frame.IsAssignedSubParcel then
  self.SubParcelsList_SetCurrent(0);
end;

procedure TFrameSubParcelsNewparcel.FormSubParcelList_SetCurrent(index: Integer);
begin
   OnUpdateAttributeSubParcelForm(0);
   if FXmlSubParcels.FormSubParcel[index].ChildNodes.FindNode('Contours') <> nil  then
     OnUpdateAttributeSubParcelForm(1);
   self.FSubParcel_Frame.XmlNewParcelSubParcel:= FXmlSubParcels.FormSubParcel[index];
end;

end.
