<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="text" version="1.0" encoding="UTF-8" indent="yes"/>
<xsl:strip-space elements="*"/>

 <xsl:template match="Region_Cadastr_Vidimus_KV">
     <xsl:apply-templates select="Package" />
 </xsl:template>
 
 <!--Менять местами X Y  (0 - меняем  ; 1 - нет )--> 
<xsl:param name="IsTypeXY" select="0" />
 <!-- =======================================================================================-->
 
<xsl:template match="Package">
	<xsl:text>Version 2 &#10;</xsl:text> 
	<xsl:text>Delimiter "," &#10;</xsl:text>
	<xsl:text>CoordSys Nonearth Units "m" Bounds (0,0) (100000000,100000000) &#10;</xsl:text>
	<xsl:text>Columns 3 &#10;</xsl:text>
	<xsl:text>KN CHAR(255) &#10;</xsl:text>
	<xsl:text>Area CHAR(255) &#10;</xsl:text>
	<xsl:text>Address CHAR(255) &#10;</xsl:text>
	<xsl:text>Data &#10;</xsl:text>
   <xsl:apply-templates  select="//Parcel//Entity_Spatial"/>
 </xsl:template>
 
<!-- ===================================================================================== -->

<!--пространственная сущьность данных-->
 <xsl:template match="Entity_Spatial">
      <xsl:text>REGION </xsl:text>
      <xsl:value-of select="count(child::Spatial_Element)"/>
      <xsl:text>&#10;</xsl:text>
	  <xsl:apply-templates select="Spatial_Element"/>
      <xsl:text>Pen (1,2,0) &#10;</xsl:text>
      <xsl:text>Brush(2,65535)&#10;</xsl:text>
 </xsl:template>

<!-- элемент  когнтура -->
<!-- ======================================================================================= -->
<xsl:template match="Spatial_Element" name="Spatila_Element">
    <xsl:value-of select="count(child::Spelement_Unit)" />     
    <xsl:text>&#10;</xsl:text>
    <xsl:choose>
        <xsl:when test="$IsTypeXY=0">
             <xsl:call-template name="GetYX"/>
        </xsl:when>
        <xsl:otherwise>
            <xsl:call-template name="GetXY"/>
        </xsl:otherwise>
    </xsl:choose>  
</xsl:template>  

<!-- =======================================================================================-->
<!--запрос координат-->
<xsl:template name = "GetXY">
 <xsl:for-each select="current()/child::Spelement_Unit"> 
          <xsl:for-each select="child::*[1]"> <!--TODO: пока что ...-->
               <xsl:value-of select="concat(@X,' ',@Y,'&#10;')"/>
          </xsl:for-each>
     </xsl:for-each>
</xsl:template> 

<xsl:template name = "GetYX">
 <xsl:for-each select="current()/child::Spelement_Unit"> 
          <xsl:for-each select="child::*[1]"> <!--TODO: пока что ...-->
               <xsl:value-of select="concat(@Y,' ',@X,'&#10;')"/>
          </xsl:for-each>
     </xsl:for-each>
</xsl:template>    
 <!-- ================================================================================= -->     
      
</xsl:stylesheet>