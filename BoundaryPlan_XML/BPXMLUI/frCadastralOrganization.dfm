object FrameCadastralOrganization: TFrameCadastralOrganization
  Left = 0
  Top = 0
  Width = 600
  Height = 143
  TabOrder = 0
  object Label1: TLabel
    Left = 8
    Top = 2
    Width = 115
    Height = 13
    Caption = #1053#1072#1079#1074#1072#1085#1080#1077' '#1086#1088#1075#1072#1085#1080#1079#1072#1094#1080#1080
  end
  object Label2: TLabel
    Left = 365
    Top = 2
    Width = 27
    Height = 13
    Caption = #1054#1043#1056#1053
  end
  object Label3: TLabel
    Left = 475
    Top = 2
    Width = 21
    Height = 13
    Caption = #1048#1053#1053
  end
  object Label4: TLabel
    Left = 8
    Top = 47
    Width = 44
    Height = 13
    Caption = #1058#1077#1083#1077#1092#1086#1085
  end
  object Label5: TLabel
    Left = 221
    Top = 47
    Width = 28
    Height = 13
    Caption = 'E-mail'
  end
  object Label6: TLabel
    Left = 8
    Top = 93
    Width = 31
    Height = 13
    Caption = #1040#1076#1088#1077#1089
  end
  object Bevel1: TBevel
    Left = 88
    Top = 43
    Width = 409
    Height = 2
    Shape = bsTopLine
  end
  object Bevel2: TBevel
    Left = 88
    Top = 90
    Width = 409
    Height = 2
    Shape = bsTopLine
  end
  object Name_Edit: TEdit
    Left = 8
    Top = 18
    Width = 341
    Height = 21
    Color = 16376249
    MaxLength = 255
    TabOrder = 0
    OnExit = Name_EditExit
    OnKeyUp = Name_EditKeyUp
  end
  object CodeORGN_Edit: TEdit
    Left = 365
    Top = 16
    Width = 92
    Height = 21
    Color = 15269864
    MaxLength = 13
    TabOrder = 1
    OnExit = CodeORGN_EditExit
    OnKeyUp = CodeORGN_EditKeyUp
  end
  object INN_Edit: TEdit
    Left = 475
    Top = 18
    Width = 94
    Height = 21
    Color = 15269864
    MaxLength = 10
    TabOrder = 2
    OnExit = INN_EditExit
    OnKeyUp = INN_EditKeyUp
  end
  object Telephone_Edit: TEdit
    Left = 8
    Top = 63
    Width = 193
    Height = 21
    Color = 15269864
    MaxLength = 50
    TabOrder = 3
    OnExit = Telephone_EditExit
    OnKeyUp = Telephone_EditKeyUp
  end
  object EMail_Edit: TEdit
    Left = 221
    Top = 63
    Width = 252
    Height = 21
    Color = 15269864
    MaxLength = 100
    TabOrder = 4
    OnExit = EMail_EditExit
    OnKeyUp = EMail_EditKeyUp
  end
  object Address_Edit: TEdit
    Left = 8
    Top = 109
    Width = 561
    Height = 21
    Color = 15269864
    MaxLength = 250
    TabOrder = 5
    OnExit = Address_EditExit
    OnKeyUp = Address_EditKeyUp
  end
end
