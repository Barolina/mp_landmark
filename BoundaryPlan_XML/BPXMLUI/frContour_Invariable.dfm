object FrameContourInvariable: TFrameContourInvariable
  Left = 0
  Top = 0
  Width = 657
  Height = 315
  TabOrder = 0
  object Attribute_Panel: TPanel
    Left = 0
    Top = 0
    Width = 657
    Height = 49
    Align = alTop
    BevelOuter = bvNone
    ParentColor = True
    TabOrder = 0
    object Attribute_DefOrNumPP_Label: TLabel
      Left = 7
      Top = 6
      Width = 282
      Height = 13
      Caption = #1054#1073#1086#1079#1085#1072#1095#1077#1085#1080#1077' '#1080#1083#1080' '#1091#1095#1077#1090#1085#1099#1081' '#1085#1086#1084#1077#1088' '#1082#1086#1085#1090#1091#1088#1072' '#1095#1072#1089#1090#1080':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Attribute_DefOrNumPP_Edit: TEdit
      Left = 5
      Top = 20
      Width = 284
      Height = 21
      BevelKind = bkFlat
      BorderStyle = bsNone
      Color = 15921123
      TabOrder = 0
      OnExit = ActionChange_Attribute_DefOrNumPPExecute
      OnKeyUp = Attribute_DefOrNumPP_EditKeyUp
    end
  end
  object Area_Panel: TPanel
    Left = 0
    Top = 49
    Width = 657
    Height = 266
    Align = alClient
    BevelOuter = bvNone
    ParentColor = True
    TabOrder = 1
  end
  object ActionList: TActionList
    Left = 600
    Top = 256
    object ActionChange_Attribute_DefOrNumPP: TAction
      Caption = 'ActionChange_Attribute_DefOrNumPP'
      OnExecute = ActionChange_Attribute_DefOrNumPPExecute
    end
    object ActionChange_ExistingOrNew: TAction
      Caption = 'ActionChange_ExistingOrNew'
    end
  end
end
