object FrameSubParContour: TFrameSubParContour
  Left = 0
  Top = 0
  Width = 657
  Height = 315
  Color = clBtnFace
  ParentBackground = False
  ParentColor = False
  TabOrder = 0
  object New1: TJvgPageControl
    AlignWithMargins = True
    Left = 3
    Top = 3
    Width = 651
    Height = 309
    ActivePage = ts2
    Align = alClient
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clBlue
    Font.Height = -15
    Font.Name = 'Tahoma'
    Font.Style = [fsBold, fsUnderline, fsStrikeOut]
    MultiLine = True
    ParentFont = False
    ParentShowHint = False
    ShowHint = False
    Style = tsFlatButtons
    TabOrder = 0
    TabStop = False
    TabStyle.Borders = [fsdLeft, fsdTop, fsdRight, fsdBottom]
    TabStyle.BevelInner = bvNone
    TabStyle.BevelOuter = bvNone
    TabStyle.Bold = False
    TabStyle.BackgrColor = clBtnFace
    TabStyle.Font.Charset = RUSSIAN_CHARSET
    TabStyle.Font.Color = clNavy
    TabStyle.Font.Height = -12
    TabStyle.Font.Name = 'Tahoma'
    TabStyle.Font.Style = [fsUnderline]
    TabStyle.CaptionHAlign = fhaCenter
    TabStyle.Gradient.FromColor = clBtnFace
    TabStyle.Gradient.ToColor = clSilver
    TabStyle.Gradient.Active = True
    TabStyle.Gradient.Orientation = fgdVertical
    TabSelectedStyle.Borders = [fsdLeft, fsdTop, fsdRight, fsdBottom]
    TabSelectedStyle.BevelInner = bvNone
    TabSelectedStyle.BevelOuter = bvNone
    TabSelectedStyle.Bold = False
    TabSelectedStyle.BackgrColor = clBtnFace
    TabSelectedStyle.Font.Charset = RUSSIAN_CHARSET
    TabSelectedStyle.Font.Color = clNavy
    TabSelectedStyle.Font.Height = -12
    TabSelectedStyle.Font.Name = 'Tahoma'
    TabSelectedStyle.Font.Style = [fsBold]
    TabSelectedStyle.CaptionHAlign = fhaCenter
    TabSelectedStyle.Gradient.FromColor = clWhite
    TabSelectedStyle.Gradient.ToColor = 15977886
    TabSelectedStyle.Gradient.Active = True
    TabSelectedStyle.Gradient.Orientation = fgdVertical
    DrawGlyphsOption = fwoTile
    Options = [ftoAutoFontDirection, ftoExcludeGlyphs, ftoTabColorAsGradientFrom, ftoTabColorAsGradientTo]
    object ts1: TTabSheet
      Caption = #1055#1083#1086#1097#1072#1076#1100' '#1082#1086#1085#1090#1091#1088#1072' '#1095#1072#1089#1090#1080
      object pnl1: TPanel
        Left = 0
        Top = 0
        Width = 643
        Height = 65
        Align = alTop
        Anchors = []
        BevelEdges = []
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object lblAttribute_DefOrNumPP_Label: TLabel
          AlignWithMargins = True
          Left = 4
          Top = 16
          Width = 279
          Height = 13
          Caption = #1054#1073#1086#1079#1085#1072#1095#1077#1085#1080#1077' '#1080#1083#1080' '#1091#1095#1077#1090#1085#1099#1081' '#1085#1086#1084#1077#1088' '#1082#1086#1085#1090#1091#1088#1072' '#1095#1072#1089#1090#1080
          Color = clWhite
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          WordWrap = True
        end
        object edtAttribute_DefOrNumPP_Edit: TEdit
          Left = 4
          Top = 35
          Width = 295
          Height = 22
          Hint = #1048#1079#1084#1077#1085#1080#1090#1100'  '#1091#1095#1077#1090#1085#1099#1081' '#1085#1086#1084#1077#1088' ('#1086#1073#1086#1079#1085#1072#1095#1077#1085#1080#1077') '#1095#1072#1089#1090#1080' '#1047#1059
          BevelKind = bkFlat
          BorderStyle = bsNone
          Color = 15921123
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          TextHint = #1048#1079#1084#1077#1085#1080#1090#1100'  '#1091#1095#1077#1090#1085#1099#1081' '#1085#1086#1084#1077#1088' ('#1086#1073#1086#1079#1085#1072#1095#1077#1085#1080#1077') '#1095#1072#1089#1090#1080' '#1047#1059
          StyleElements = []
          OnExit = ActionChange_Attribute_DefOrNumPPExecute
        end
      end
    end
    object ts2: TTabSheet
      Caption = #1054#1087#1080#1089#1072#1085#1080#1077' '#1084#1077#1089#1090#1086#1087#1086#1083#1086#1078#1077#1085#1080#1103' '#1075#1088#1072#1085#1080#1094
      ImageIndex = 1
      Touch.ParentTabletOptions = False
      Touch.TabletOptions = []
    end
  end
  object ActionList: TActionList
    Left = 600
    Top = 256
    object ActionChange_Attribute_DefOrNumPP: TAction
      Caption = 'ActionChange_Attribute_DefOrNumPP'
      OnExecute = ActionChange_Attribute_DefOrNumPPExecute
    end
    object ActionChange_ExistingOrNew: TAction
      Caption = 'ActionChange_ExistingOrNew'
    end
  end
  object jvpgmngr1: TJvPageManager
    UseHistory = True
    Left = 416
    Top = 136
  end
end
