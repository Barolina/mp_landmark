unit frNewParcel;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, Mask, ComCtrls, Buttons, Grids,

  frEntitySpatial, XMLDoc, XMLIntf, STD_MP, ActnList, ButtonGroup,
  PlatformDefaultStyleActnCtrls, ActnMan, PageControlAnimate,
  {MSClipBoard,} frPrevCadastralNumbers, frArea, frAddress, BpXML_different,
  frNaturalObject, frUtilizationList,

  ImgList,
  System.Actions, frInnerCadastralNumbers,
  frProvCadastralNumbers, frAreaDelta, frContours, unRussLang,frSubParcels, frSubParcel,
  JvExStdCtrls, JvCombobox
  ,frCategory;

type
  TFrameNewParcel = class(TFrame)
    Label3: TLabel;
    frameAddress: TLabel;
    NewParcelsPageControl: TPageControl;
    PrevCadNums_TabSheet: TTabSheet;
    ProvPassCadNums_TabSheet: TTabSheet;
    Area_TabSheet: TTabSheet;
    Location_TabSheet: TTabSheet;
    Category_TabSheet: TTabSheet;
    NaturalObject_TabSheet: TTabSheet;
    Utilization_TabSheet: TTabSheet;
    SubParcels_TabSheet: TTabSheet;
    Coordinats_TabSheet: TTabSheet;
    InnerCadNums_TabSheet: TTabSheet;
    Panel_Navigation: TPanel;
    SpeedButton_Nav_Back: TSpeedButton;
    SpeedButton_Nav_Next: TSpeedButton;
    ComboBox_Nav_GoTo: TComboBox;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;

    NaturalObject_IsFill_CheckBox: TCheckBox;
    NaturalObject_LabelCaption: TLabel;
    Utilization_IsFill_LabelCaption: TLabel;
    SubParcels_LabelCaption: TLabel;
    OrdinatesPageControl: TPageControl;
    Contours_TabSheet: TTabSheet;
    EntitySpatial_TabSheet: TTabSheet;
    PrevCadNums_IsFill_CheckBox: TCheckBox;
    ProvPassCadNums_IsFill_CheckBox: TCheckBox;
    InnerCadNums_IsFill_CheckBox: TCheckBox;
    Area_IsFill_CheckBox: TCheckBox;
    Location_IsFill_CheckBox: TCheckBox;
    Category_IsFill_CheckBox: TCheckBox;
    CheckBox4: TCheckBox;
    SubParcels_IsFill_CheckBox: TCheckBox;
    Coordinats_Label_Caption: TLabel;
    Coordinats_IsFill_CheckBox: TCheckBox;

    ActionManager_FormParcels: TActionList;
    ActionXML_Category_Write: TAction;
    Action_PrevCadNums_IsFill: TAction;
    Action_ProvPassCadNums_IsFil: TAction;
    Action_InnerCadNums_IsFill: TAction;
    Action_LocationAddress_IsFill: TAction;
    Action_NaturalObject_IsFill: TAction;
    Action_SubParcels_IsFill: TAction;
    Action_Encumbrance_IsFill: TAction;
    pnlNewParcel: TPanel;

    procedure SpeedButton_Nav_BackClick(Sender: TObject);
    procedure SpeedButton_Nav_NextClick(Sender: TObject);
    procedure ComboBox_Nav_GoToChange(Sender: TObject);



    procedure ActionXML_Category_WriteExecute(Sender: TObject);
    procedure Action_PrevCadNums_IsFillExecute(Sender: TObject);
    procedure Action_ProvPassCadNums_IsFilExecute(Sender: TObject);
    procedure Action_InnerCadNums_IsFillExecute(Sender: TObject);
    procedure Action_LocationAddress_IsFillExecute(Sender: TObject);
    procedure Action_NaturalObject_IsFillExecute(Sender: TObject);
    procedure Action_SubParcels_IsFillExecute(Sender: TObject);

    procedure Attributes_Definition_EditKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Attributes_AdditionalName_EditKeyUp(Sender: TObject;
      var Key: Word; Shift: TShiftState);
    procedure medtCadastralBlock_value_MaskEditKeyUp(Sender: TObject;
      var Key: Word; Shift: TShiftState);
    procedure NewParcelsPageControlChange(Sender: TObject);
    procedure Label16Click(Sender: TObject);
    procedure Label17Click(Sender: TObject);
    procedure Label18Click(Sender: TObject);
    procedure NaturalObject_LabelCaptionClick(Sender: TObject);
    procedure SubParcels_LabelCaptionClick(Sender: TObject);




  private
    FXmlNewParcel          : IXMLTNewParcel; // ������� ��������� �������

    FPrevCadNums_frame     : TFramePrevSCadastralNumbers; // �������� ��
    FProvPassCadlNums_frame: TFrameProvCadastralNumber;
    // �� ��� ����������� ������� � �������
    FInnerCadNums_frame    : TFrameInnerCadastralNumbers; // �������� ��
    FArea_frame            : TFrameArea; // �������
    FAreaDelta_frame       : TFrameAreaDelta; // �������������� ���������
    FAddress_frame         : TFrameAddress; // �����
    FNaturalObject_frame   : TFrameNaturalObject; // ���� ���
    FUtilizationList_frame : TFrameUtilizationList; // ������������� �������
    FSubParcels_frame      : TFrameSubParcels; // ����� ��
    FContours_frame        : TFrameContours; // ���� ��������������
    FEntitySpatial_frame   : TFrameEntity_Spatial; // ���� �������������
    FCategory_frame              : TFrameCategory;

    FOnChangeDefinition: TNotifyEvent;

    procedure InitializationUI; // ���������������� ����������


    // ���������
    procedure UI_Category_Read;
    procedure UI_Category_Write;

    // ��������� ������� NewParcel
    procedure SetNewParcel(const aNewParcel: IXMLTNewParcel);


    procedure SetType(aValue: integer); //0 - ���������������� 1-�������������� �������
    function GetType: integer;
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    procedure SetActivTab(index: integer; UseAnimate: Boolean = false);

    function IsAssigned_NewParcel: Boolean;

    property XmlNewParcel: IXMLTNewParcel read FXmlNewParcel write SetNewParcel;
    property TypeNewParcel: integer read GetType write SetType;
    Property OnChangeDefinition: TNotifyEvent read FOnChangeDefinition write FOnChangeDefinition;
  end;

implementation

uses MsXMLAPI;

procedure _HiddenChangeCheckBoxSate(ChBox: TCheckBox; NewSate: TCheckBoxState);
var
  _OnClick: TNotifyEvent;
begin
  if ChBox.State = NewSate then
    EXIT;
  _OnClick := ChBox.OnClick;
  ChBox.OnClick := nil;
  ChBox.State := NewSate;
  ChBox.OnClick := _OnClick;
end;

{$R *.dfm}
{ TFrameNewparcel }

procedure TFrameNewParcel.ActionXML_Category_WriteExecute(Sender: TObject);
begin
  if not self.IsAssigned_NewParcel then
    raise Exception.Create(ExMSG_NotAssignedROOT);
  self.UI_Category_Write;
end;

procedure TFrameNewParcel.Action_InnerCadNums_IsFillExecute(Sender: TObject);
begin
  if not self.InnerCadNums_IsFill_CheckBox.Checked  then
  begin
    if self.FXmlNewParcel.ChildNodes.FindNode(cnstXmlInner_CadastralNumbers)<>nil then
      if MessageDlg('������� ������: '+Label18.Caption+'?',mtInformation,mbYesNo,-1) = mrYes then
        begin
          MsXml_RemoveChildNode(self.FXmlNewParcel, 'Inner_CadastralNumbers');
          self.FInnerCadNums_frame.XmlCadastralNumbers := nil;
          self.FInnerCadNums_frame.Visible := false;
        end else self.InnerCadNums_IsFill_CheckBox.Checked := True;
  end
  else
  begin
    self.FInnerCadNums_frame.XmlCadastralNumbers :=
      self.FXmlNewParcel.Inner_CadastralNumbers;
    self.FInnerCadNums_frame.Visible := true;
  end
end;

procedure TFrameNewParcel.Action_LocationAddress_IsFillExecute(Sender: TObject);
begin
  if self.Location_IsFill_CheckBox.Checked then
  begin
    self.FAddress_frame.XmlAddress :=
      (self.FXmlNewParcel.Location as IXMLTAddress);
    self.FAddress_frame.Visible := true;
  end
  else
  begin
    MsXml_RemoveChildNode(self.FXmlNewParcel.Location, 'Address');
    self.FAddress_frame.XmlAddress := nil;
    self.FAddress_frame.Visible := false;
  end;
end;

procedure TFrameNewParcel.Action_NaturalObject_IsFillExecute(Sender: TObject);
begin
  if not NaturalObject_IsFill_CheckBox.Checked then
  begin
     if FNaturalObject_frame.XmlNaturalObject <> nil then
        if MessageDlg('������� ������: '+NaturalObject_LabelCaption.Caption+'?',mtInformation,mbYesNo,-1) = mrYes then
        begin
           MsXml_RemoveChildNode(self.FXmlNewParcel, 'NaturalObject');
           self.FNaturalObject_frame.XmlNaturalObject := nil;
           self.FNaturalObject_frame.Visible := false;
        end else NaturalObject_IsFill_CheckBox.Checked := True;
  end
  else
  begin
    self.FNaturalObject_frame.XmlNaturalObject := self.FXmlNewParcel.NaturalObject;
    self.FNaturalObject_frame.Visible := true;
  end
end;

procedure TFrameNewParcel.Action_PrevCadNums_IsFillExecute(Sender: TObject);
begin
  if not PrevCadNums_IsFill_CheckBox.Checked then
  begin
     if FPrevCadNums_frame.XmlCadastralNumbers <> nil then
       if MessageDlg('������� ������: '+Label16.Caption+'?',mtInformation,mbYesNo,-1) = mrYes then
          begin
            MsXml_RemoveChildNode(self.FXmlNewParcel, 'Prev_CadastralNumbers');
            self.FPrevCadNums_frame.XmlCadastralNumbers := nil;
            self.FPrevCadNums_frame.Visible := false;
          end
       ELSE PrevCadNums_IsFill_CheckBox.Checked := true;
  end
  else
  begin
    self.FPrevCadNums_frame.XmlCadastralNumbers :=
      self.FXmlNewParcel.Prev_CadastralNumbers;
    self.FPrevCadNums_frame.Visible := true;
  end
end;

procedure TFrameNewParcel.Action_ProvPassCadNums_IsFilExecute(Sender: TObject);
begin
  if not  self.ProvPassCadNums_IsFill_CheckBox.Checked then
  begin
    if FProvPassCadlNums_frame.XmlCadastralNumbers <> nil then
      if MessageDlg('������� ������: '+Label17.Caption+'?',mtInformation,mbYesNo,-1) = mrYes then
        begin
          MsXml_RemoveChildNode(self.FXmlNewParcel,
            'Providing_Pass_CadastralNumbers');
          self.FProvPassCadlNums_frame.XmlCadastralNumbers := nil;
          self.FProvPassCadlNums_frame.Visible := false;
        end else self.ProvPassCadNums_IsFill_CheckBox.Checked := True;
  end
  else
  begin
    self.FProvPassCadlNums_frame.XmlCadastralNumbers :=
      self.FXmlNewParcel.Providing_Pass_CadastralNumbers;
    self.FProvPassCadlNums_frame.Visible := true;
  end
end;

procedure TFrameNewParcel.Action_SubParcels_IsFillExecute(Sender: TObject);
begin
  if not SubParcels_IsFill_CheckBox.Checked then
  begin
     if FSubParcels_frame.XmlNewSubParcels <> nil then
       if MessageDlg('������� ������: '+SubParcels_LabelCaption.Caption+'?',mtInformation,mbYesNo,-1) = mrYes then
       begin
          MsXml_RemoveChildNode(self.FXmlNewParcel, 'SubParcels');
          self.FSubParcels_frame.XmlNewSubParcels := nil;
          self.FSubParcels_frame.Visible := false;
       end else SubParcels_IsFill_CheckBox.Checked := True;
  end
  else
  begin
    self.FSubParcels_frame.XmlNewSubParcels := self.FXmlNewParcel.SubParcels;
    self.FSubParcels_frame.Visible := true;
    Self.FSubParcels_frame.ActivSubParcelKind := TSubParcelKind.cNewFormSubParcel;
  end
end;

procedure TFrameNewParcel.Attributes_AdditionalName_EditKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key = VK_RETURN then
    if (Sender is TCustomEdit) then
      TCustomEdit(Sender).Parent.SetFocus;
end;

procedure TFrameNewParcel.Attributes_Definition_EditKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key = VK_RETURN then
    if (Sender is TCustomEdit) then
      TCustomEdit(Sender).Parent.SetFocus;
end;

procedure TFrameNewParcel.medtCadastralBlock_value_MaskEditKeyUp
  (Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if Key = VK_RETURN then
    if (Sender is TCustomEdit) then
      TCustomEdit(Sender).Parent.SetFocus;
end;

procedure TFrameNewParcel.ComboBox_Nav_GoToChange(Sender: TObject);
begin
  self.SetActivTab(self.ComboBox_Nav_GoTo.ItemIndex, false);
end;

constructor TFrameNewParcel.Create(AOwner: TComponent);
begin
  inherited;

  // NA
  self.FPrevCadNums_frame := TFramePrevSCadastralNumbers.Create
    (self.PrevCadNums_TabSheet);
  self.FPrevCadNums_frame.Parent := self.PrevCadNums_TabSheet;
  self.FPrevCadNums_frame.Align := alClient;

  // NA
  self.FProvPassCadlNums_frame := TFrameProvCadastralNumber.Create
    (self.ProvPassCadNums_TabSheet);
  self.FProvPassCadlNums_frame.Parent := self.ProvPassCadNums_TabSheet;
  self.FProvPassCadlNums_frame.Align := alClient;

  // NA
  self.FInnerCadNums_frame := TFrameInnerCadastralNumbers.Create
    (self.InnerCadNums_TabSheet);
  self.FInnerCadNums_frame.Parent := self.InnerCadNums_TabSheet;
  self.FInnerCadNums_frame.Align := alClient;

  self.FArea_frame := TFrameArea.Create(self.Area_TabSheet);
  self.FArea_frame.Parent := self.Area_TabSheet;
  self.FArea_frame.Align := alTop;
  self.FArea_frame.Visible := true;

  self.FAreaDelta_frame := TFrameAreaDelta.Create(self.Area_TabSheet);
  self.FAreaDelta_frame.Parent := self.Area_TabSheet;
  self.FAreaDelta_frame.Align := alClient;
  self.FAreaDelta_frame.Visible := true;
  { ������ �� ������� ��� �����������  ���� ��� ����� }
  self.FAreaDelta_frame.Area_In_GKN_MaskEdit.Visible := false;
  self.FAreaDelta_frame.Area_In_GKN_Label_Caption.Visible := false;
  self.FAreaDelta_frame.Delta_Area_MaskEdit.Visible := false;
  self.FAreaDelta_frame.Delta_Area_Label_Caption.Visible := false;

  self.FAddress_frame := TFrameAddress.Create(self.Location_TabSheet);
  self.FAddress_frame.Parent := self.Location_TabSheet;
  self.FAddress_frame.Align := alClient;
  self.FAddress_frame.Visible := True;

  self.FCategory_frame := TFrameCategory.Create
    (self.Category_TabSheet);
  self.FCategory_frame.Parent := self.Category_TabSheet;
  self.FCategory_frame.Align := alClient;


  self.FNaturalObject_frame := TFrameNaturalObject.Create
    (self.NaturalObject_TabSheet);
  self.FNaturalObject_frame.Parent := self.NaturalObject_TabSheet;
  self.FNaturalObject_frame.Align := alClient;

  self.FUtilizationList_frame := TFrameUtilizationList.Create
    (self.Utilization_TabSheet);
  self.FUtilizationList_frame.Parent := self.Utilization_TabSheet;
  self.FUtilizationList_frame.Align := alClient;

  self.FSubParcels_frame := TFrameSubParcels.Create(self.SubParcels_TabSheet);
  self.FSubParcels_frame.Parent := self.SubParcels_TabSheet;
  self.FSubParcels_frame.Align := alClient;
  Self.FSubParcels_frame.cbbContoursType.Enabled := False;

  self.FContours_frame := TFrameContours.Create(self.Contours_TabSheet);
  self.FContours_frame.ActivContourKind:= TFrameContours.TContourKind.cNewContour;
  self.FContours_frame.tlbTypeSubparcels.Visible:= false;

  self.FContours_frame.Parent := self.Contours_TabSheet;
  self.FContours_frame.Align := alClient;
  // ����� �������� � ��������� ��� � ����� ������������� :)
  self.FContours_frame.Contour.AsAttributeContour;

  self.FEntitySpatial_frame := TFrameEntity_Spatial.Create
    (self.EntitySpatial_TabSheet);
  self.FEntitySpatial_frame.Parent := self.EntitySpatial_TabSheet;
  self.FEntitySpatial_frame.Align := alClient;
end;

function TFrameNewParcel.GetType: integer;
begin
 Result:= -1;
 if Xml_IsExistChildNode(self.FXmlNewParcel,cnstXmlContours) then
  Result:= 1
 else
  if Xml_IsExistChildNode(self.FXmlNewParcel,cnstXmlEntity_Spatial) then
    Result:= 0;
end;

procedure TFrameNewParcel.InitializationUI;
var
  _node: IXMLNode;
begin
  if self.IsAssigned_NewParcel then
  begin
    self.NewParcelsPageControl.Visible := true;
    self.Panel_Navigation.Visible := true;


    _node := self.FXmlNewParcel.ChildNodes.FindNode('Prev_CadastralNumbers');
    if _node <> nil then
    begin
      self.FPrevCadNums_frame.XmlCadastralNumbers :=
      self.FXmlNewParcel.Prev_CadastralNumbers;
      self.FPrevCadNums_frame.Visible := true;
      _HiddenChangeCheckBoxSate(self.PrevCadNums_IsFill_CheckBox, cbChecked);
    end
    else
    begin
      self.FPrevCadNums_frame.XmlCadastralNumbers := nil;
      self.FPrevCadNums_frame.Visible := false;
      _HiddenChangeCheckBoxSate(self.PrevCadNums_IsFill_CheckBox, cbUnchecked);
    end;

    _node := self.FXmlNewParcel.ChildNodes.FindNode('Providing_Pass_CadastralNumbers');
    if _node <> nil then
    begin
      self.FProvPassCadlNums_frame.XmlCadastralNumbers :=
        self.FXmlNewParcel.Providing_Pass_CadastralNumbers;
      self.FProvPassCadlNums_frame.Visible := true;
      _HiddenChangeCheckBoxSate(self.ProvPassCadNums_IsFill_CheckBox,
        cbChecked);
    end
    else
    begin
      self.FProvPassCadlNums_frame.XmlCadastralNumbers := nil;
      self.FProvPassCadlNums_frame.Visible := false;
      _HiddenChangeCheckBoxSate(self.ProvPassCadNums_IsFill_CheckBox,
        cbUnchecked);
    end;

    _node := self.FXmlNewParcel.ChildNodes.FindNode('Inner_CadastralNumbers');
    if _node <> nil then
    begin
      self.FInnerCadNums_frame.XmlCadastralNumbers :=
        self.FXmlNewParcel.Inner_CadastralNumbers;
      self.FInnerCadNums_frame.Visible := true;
      _HiddenChangeCheckBoxSate(self.InnerCadNums_IsFill_CheckBox, cbChecked);
    end
    else
    begin
      self.FInnerCadNums_frame.XmlCadastralNumbers := nil;
      self.FInnerCadNums_frame.Visible := false;
      _HiddenChangeCheckBoxSate(self.InnerCadNums_IsFill_CheckBox, cbUnchecked);
    end;

    FArea_frame.XmlArea := FXmlNewParcel.Area;
    FAreaDelta_frame.XmlNewParcel := FXmlNewParcel;

    self.FAddress_frame.XmlAddress:= (self.FXmlNewParcel.Location as IXMLTAddress);

    _node := self.FXmlNewParcel.ChildNodes.FindNode('NaturalObject');
    if _node <> nil then
    begin
      self.FNaturalObject_frame.XmlNaturalObject :=
        self.FXmlNewParcel.NaturalObject;
      self.FNaturalObject_frame.Visible := true;
      _HiddenChangeCheckBoxSate(self.NaturalObject_IsFill_CheckBox, cbChecked);
    end
    else
    begin
      self.FNaturalObject_frame.XmlNaturalObject := nil;
      self.FNaturalObject_frame.Visible := false;
      _HiddenChangeCheckBoxSate(self.NaturalObject_IsFill_CheckBox,
        cbUnchecked);
    end;

    self.FUtilizationList_frame.XmlUtilizationList :=
      self.FXmlNewParcel.Utilization;

    self.FCategory_frame.XmlCategory :=  self.FXmlNewParcel.Category;


    _node := self.FXmlNewParcel.ChildNodes.FindNode('SubParcels');
    if _node <> nil then
    begin
      self.FSubParcels_frame.XmlNewSubParcels := self.FXmlNewParcel.SubParcels;
      self.FSubParcels_frame.Visible := true;
      _HiddenChangeCheckBoxSate(self.SubParcels_IsFill_CheckBox, cbChecked);
    end
    else
    begin
      self.FSubParcels_frame.XmlNewSubParcels := nil;
      self.FSubParcels_frame.Visible := false;
      _HiddenChangeCheckBoxSate(self.SubParcels_IsFill_CheckBox, cbUnchecked);
    end;

    self.OrdinatesPageControl.ActivePage := nil;
    _node := self.FXmlNewParcel.ChildNodes.FindNode('Contours');
    if _node <> nil then
    begin
      self.FContours_frame.XmlContours := self.FXmlNewParcel.Contours;
      // self.FContours_frame.DefinitionParent:= _ReadAttribute(self.FXmlNewParcel,'Definition');
      self.OrdinatesPageControl.ActivePage := self.Contours_TabSheet;

    end
    else
      self.FContours_frame.XmlContours := nil;

    _node := self.FXmlNewParcel.ChildNodes.FindNode('Entity_Spatial');
    if _node <> nil then
    begin
      self.FEntitySpatial_frame.EntitySpatial :=
        self.FXmlNewParcel.Entity_Spatial;
      self.OrdinatesPageControl.ActivePage := self.EntitySpatial_TabSheet;

    end
    else
      self.FEntitySpatial_frame.EntitySpatial := nil;

  end
  else
  begin
    self.NewParcelsPageControl.Visible := false;
    self.Panel_Navigation.Visible := false;
    self.FPrevCadNums_frame.XmlCadastralNumbers := nil;
    self.FProvPassCadlNums_frame.XmlCadastralNumbers := nil;
    self.FInnerCadNums_frame.XmlCadastralNumbers := nil;

    self.FArea_frame.XmlArea := nil;
    self.FAreaDelta_frame.XmlNewParcel := nil;
    self.FAddress_frame.XmlAddress := nil;
    self.FNaturalObject_frame.XmlNaturalObject := nil;
    self.FUtilizationList_frame.XmlUtilizationList := nil;
    self.FCategory_frame.XmlCategory := nil;
    self.FSubParcels_frame.XmlNewSubParcels := nil;
    self.FContours_frame.XmlContours := nil;
    self.FEntitySpatial_frame.EntitySpatial := nil;
  end;

end;

function TFrameNewParcel.IsAssigned_NewParcel: Boolean;
begin
  Result := self.FXmlNewParcel <> nil;
end;

procedure TFrameNewParcel.Label16Click(Sender: TObject);
begin
  Self.PrevCadNums_IsFill_CheckBox.Checked := not Self.PrevCadNums_IsFill_CheckBox.Checked;
end;

procedure TFrameNewParcel.Label17Click(Sender: TObject);
begin
 Self.ProvPassCadNums_IsFill_CheckBox.Checked := not self.ProvPassCadNums_IsFill_CheckBox.Checked;
end;

procedure TFrameNewParcel.Label18Click(Sender: TObject);
begin
 self.InnerCadNums_IsFill_CheckBox.Checked := not self.InnerCadNums_IsFill_CheckBox.Checked;
end;

procedure TFrameNewParcel.NaturalObject_LabelCaptionClick(Sender: TObject);
begin
 self.NaturalObject_IsFill_CheckBox.Checked := not self.NaturalObject_IsFill_CheckBox.Checked;
end;

procedure TFrameNewParcel.NewParcelsPageControlChange(Sender: TObject);
begin
  self.SpeedButton_Nav_Back.Visible :=
    self.NewParcelsPageControl.ActivePageIndex = 0;
  self.SpeedButton_Nav_Next.Visible :=
    self.NewParcelsPageControl.ActivePageIndex = self.NewParcelsPageControl.
    PageCount - 1;

end;

procedure TFrameNewParcel.SetActivTab(index: integer; UseAnimate: Boolean = false);
begin
  if (index < 0) or (index > self.NewParcelsPageControl.PageCount - 1) then  EXIT;

  if UseAnimate then   pcaSetActivTab(self.NewParcelsPageControl,self.NewParcelsPageControl.Pages[index])
  else
    self.NewParcelsPageControl.ActivePageIndex := index;

  self.ComboBox_Nav_GoTo.ItemIndex :=
    self.NewParcelsPageControl.ActivePageIndex;
  self.SpeedButton_Nav_Back.Enabled := self.ComboBox_Nav_GoTo.ItemIndex <> 0;
  self.SpeedButton_Nav_Next.Enabled := self.ComboBox_Nav_GoTo.ItemIndex <>
    self.ComboBox_Nav_GoTo.Items.Count - 1;
end;

procedure TFrameNewParcel.SetNewParcel(const aNewParcel: IXMLTNewParcel);
begin
  if self.FXmlNewParcel = aNewParcel then
    EXIT;
  self.FXmlNewParcel := aNewParcel;
  self.InitializationUI;
end;

procedure TFrameNewParcel.SetType(aValue: integer);
begin
    case aValue of
      0:
        begin // �������������� -> ����������������
          MsXml_RemoveChildNode(self.FXmlNewParcel,cnstXmlContours);
          self.FContours_frame.XmlContours := nil;
          self.FEntitySpatial_frame.EntitySpatial := self.FXmlNewParcel.Entity_Spatial;
          self.OrdinatesPageControl.ActivePage := self.EntitySpatial_TabSheet;
        end;
      1:
        begin // ���������������� -> ��������������
          MsXml_RemoveChildNode(self.FXmlNewParcel,cnstXmlEntity_Spatial);
          self.FEntitySpatial_frame.EntitySpatial := nil;
          self.FContours_frame.XmlContours := self.FXmlNewParcel.Contours;
          self.OrdinatesPageControl.ActivePage := self.Contours_TabSheet;
        end;
     else
      raise Exception.Create(ExMsg_Data_Invalid);
    end;


end;

procedure TFrameNewParcel.SpeedButton_Nav_BackClick(Sender: TObject);
begin
  self.SetActivTab(self.ComboBox_Nav_GoTo.ItemIndex - 1,false);
end;

procedure TFrameNewParcel.SpeedButton_Nav_NextClick(Sender: TObject);
begin
  self.SetActivTab(self.ComboBox_Nav_GoTo.ItemIndex + 1, false);
end;


procedure TFrameNewParcel.SubParcels_LabelCaptionClick(Sender: TObject);
begin
self.SubParcels_IsFill_CheckBox.Checked := not self.SubParcels_IsFill_CheckBox.Checked;
end;

procedure TFrameNewParcel.UI_Category_Read;
begin
end;

procedure TFrameNewParcel.UI_Category_Write;
begin
end;


end.
