unit frChangeSubParcels;
{
  FormParcels - > ChangeSubParcels - �������� �� ���������� ��������� �������� � �� ������
}
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ActnList, ButtonGroup, ExtCtrls,
  VCL.clipbrd,
  frSubparcel,STD_MP, BpXML_different, ImgList, BPCommon, System.Actions,
  fmContainer, unRussLang, Vcl.Menus, Vcl.ComCtrls,
  Vcl.Buttons,FrTypeParcel, Xml.XMLIntf;

type
  TFrameChangeSubParcels = class(TFrame)
    Contours_List_ButtonGroup: TButtonGroup;
    actlst: TActionList;
    Contours_List_ListBox: TListBox;
    actContrours_AddNew: TAction;
    actContours_DeleteCurrent: TAction;
    cbbContoursType: TComboBox;
    actClipBrd_Paste: TAction;
    actClipBrd_Copy: TAction;
    pnlControls: TPanel;
    btnEdCurContour: TButtonedEdit;
    actContour_next: TAction;
    actContour_prev: TAction;
    actValid: TAction;

    procedure actContrours_AddNewExecute(Sender: TObject);
    procedure actContours_DeleteCurrentExecute(Sender: TObject);
    procedure Action_Contours_SetCurrentExecute(Sender: TObject);
        procedure Contours_List_ListBoxMouseLeave(Sender: TObject);
    procedure btnEdCurContourRightButtonClick(Sender: TObject);
    procedure Contours_List_ListBoxKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure btnEdCurContourKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure btnEdCurContourExit(Sender: TObject);

    procedure cbbContoursTypeChange(Sender: TObject);
    procedure actClipBrd_CopyExecute(Sender: TObject);
    procedure actClipBrd_PasteExecute(Sender: TObject);
    procedure actContour_nextExecute(Sender: TObject);
    procedure actContour_prevExecute(Sender: TObject);
    procedure actValidExecute(Sender: TObject);
  private
   { Private declarations }
   FXmlChangeSubParcel : IXMLTExistParcel_SubParcels;
   FSubParcel_frame          : TFrameSubParcel;

   procedure SubParcelNewListDorefresh(A,B : Integer);
   procedure SubParcelExistListDorefresh(A,B : Integer);
   procedure SubParcelInvariableListDorefresh(A,B : Integer);
   procedure SubParcels_List_DoRefresh(A: Integer = -1; B: Integer = -1);

   procedure SubParcels_List_DoSetCurrentItem(IndexItem: integer);

   procedure SubParcels_List_DoUpDate;
   procedure SubParcels_List_DoAddNewItem(indexType : integer); //Entity_Spatial ot Contours
   procedure SubParcels_List_DoDeleteItem(IndexItem: Integer);

   procedure OnChange_Attribute_DefinitionOrNumberPP(sender: Tobject);
   procedure _setSubParcelKind(aValue: TSubParcelKind);
  protected
  
   procedure InitializationUI;
   procedure SetChangeParcel (const aSubParces : IXMLTExistParcel_SubParcels);

   procedure SetActivSubParcelUIIndex(index: integer);
   function  GetActivSubParcelUIIndex: integer;

   procedure setActivSubParcelKind(aValue: TSubParcelKind);
   function  getActivSubParcelKind: TSubParcelKind;
  public
   { Public declarations }
   constructor Create(Owner: TComponent); override;

   function IsAssignedSubParcels: Boolean;

   procedure getAsText(aSubParcels: TSubParcelKind; var OutputText: string);
   procedure setAsText(aSubParcels: TSubParcelKind; InputText: string);

   procedure SubParcelsList_Show;
   procedure SubParcelsList_Hide;

   property ActivSubparcelUIIndex: integer read GetActivSubParcelUIIndex write SetActivSubParcelUIIndex;
   property ActivSubParcelKind: TSubParcelKind read getActivSubParcelKind write setActivSubParcelKind;

   property SubParcel: TFrameSubParcel read FSubParcel_frame;
   property XmlChangeSubParcels: IXMLTExistParcel_SubParcels read FXmlChangeSubParcel write SetChangeParcel;

  end;

implementation

uses MsXMLAPI, SurveyingPlan.DataFormat, SurveyingPlan.Xml.Utils,
  SurveyingPlan.Xml.Types, fmLog;

{$R *.dfm}

{ TFrameContours }

procedure TFrameChangeSubParcels.actClipBrd_CopyExecute(Sender: TObject);
var
 _st: string;
begin
 self.getAsText(self.ActivSubParcelKind,_st);
 Clipboard.Open;
 TRY
  Clipboard.AsText:= _st;
 FINALLY
  Clipboard.Close;
 END;
end;

procedure TFrameChangeSubParcels.actClipBrd_PasteExecute(Sender: TObject);
var
 _st: string;
begin
 Clipboard.Open;
 TRY
 _st:= Clipboard.AsText;
 FINALLY
  Clipboard.Close;
 END;
 if FSubParcel_frame.IsTypeLandSubParcel = 0  then   self.setAsText(self.ActivSubParcelKind,_st)
 else MessageDlg('�� ������ ������ ��������� ������ �� ��������������! ',mtInformation,[mbOK],-1);
end;

procedure TFrameChangeSubParcels.actContour_nextExecute(Sender: TObject);
begin
 self.ActivSubparcelUIIndex:= self.ActivSubparcelUIIndex+1;
end;

procedure TFrameChangeSubParcels.actContour_prevExecute(Sender: TObject);
begin
self.ActivSubparcelUIIndex:= self.ActivSubparcelUIIndex-1;
end;

procedure TFrameChangeSubParcels.actContours_DeleteCurrentExecute(Sender: TObject);
begin
 if not IsAssignedSubParcels then EXIT;
 if self.Contours_List_ListBox.ItemIndex=-1 then EXIT;
 self.SubParcels_List_DoDeleteItem(self.Contours_List_ListBox.ItemIndex);
 self.SetActivSubParcelUIIndex(self.Contours_List_ListBox.ItemIndex);
 if not self.FSubParcel_frame.IsAssignedSubParcel then
  self.SetActivSubParcelUIIndex(0);
end;

procedure TFrameChangeSubParcels.Action_Contours_SetCurrentExecute(Sender: TObject);
begin
 self.ActivSubparcelUIIndex:= self.Contours_List_ListBox.ItemIndex;
 self.Contours_List_ListBox.Visible:= false;
end;

procedure TFrameChangeSubParcels.actContrours_AddNewExecute(Sender: TObject);
begin
 if not self.IsAssignedSubParcels then EXIT;
 if self.ActivSubParcelKind <>  cInvariableSubParcel    then
   FSubParcel_frame.IsTypeLandSubParcel := frmTypeParcel.GetTypeObject
 else FSubParcel_frame.IsTypeLandSubParcel := -1;
 self.SubParcels_List_DoAddNewItem(FSubParcel_frame.IsTypeLandSubParcel);
 self.SetActivSubParcelUIIndex(self.Contours_List_ListBox.Count-1);
end;

procedure TFrameChangeSubParcels.actValidExecute(Sender: TObject);
var
 _st: string;
 _msubParcel: TSpMSubParcel;
 _log: TStringBuilder;
 _dlog: TformLog;
begin
 _msubParcel:= TSpMSubParcel.Create;

// case self.ActivSubParcelKind of
//   cFormSubParcel:  TSpConvert.MSubParcel.Convert(FXmlChangeSubParcel.FormSubParcel,_msubParcel);
//   cExistSubParcel: TSpConvert.MSubParcel.Convert(FXmlChangeSubParcel.ExistSubParcel,_msubParcel);
//   cInvariableSubParcel: TSpConvert.MSubParcel.Convert(FXmlChangeSubParcel.InvariableSubParcel,_msubParcel);
// end;
 _log:= TStringBuilder.Create;
 _dlog:= TformLog.Create(nil);
 if not TSpValidator.Validate(_msubParcel,_log) then
  _dlog.TextLog:= _log.ToString
 else
  _dlog.TextLog:= '������ �� ����������';
 _dlog.ShowModal;
 _log.Free;
 _dlog.Free;
end;

procedure TFrameChangeSubParcels.btnEdCurContourExit(Sender: TObject);
var
 _index: integer;
begin
 if TryStrToInt(self.btnEdCurContour.Text,_index) then
  self.ActivSubparcelUIIndex:= _index-1
 else
  self.ActivSubparcelUIIndex:= self.Contours_List_ListBox.ItemIndex;
end;

procedure TFrameChangeSubParcels.btnEdCurContourKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if key = VK_RETURN then
  self.SetFocus;
end;

procedure TFrameChangeSubParcels.btnEdCurContourRightButtonClick(Sender: TObject);
begin
 if self.Contours_List_ListBox.Visible then
  self.SubParcelsList_Hide
 else
  self.SubParcelsList_Show;
end;

procedure TFrameChangeSubParcels.SubParcelInvariableListDorefresh(A,
  B: Integer);
var
 _SubParcel: IXMLTChangeParcel_SubParcels_InvariableSubParcel;
 _i,_j: Integer;
 _ItemName: string;
 xmlNode : IXMLNode;
begin
 if self.Contours_List_ListBox.Items.Count = 0 then EXIT;
 if (A<0) or (a>=self.Contours_List_ListBox.Items.Count)  then a:= 0;

 if (B<A) or (B>=self.Contours_List_ListBox.Items.Count)  then  B:= self.Contours_List_ListBox.Items.Count-1;

 _j:= self.Contours_List_ListBox.ItemIndex;
 for _i := A to B do 
 begin
  xmlNode := IXMLNode(pointer(self.Contours_List_ListBox.Items.Objects[_i]));
  _ItemName := MsXml_ReadAttribute(xmlNode,cnstAtrNumber_Record);
  self.Contours_List_ListBox.Items.Strings[_i]:= _ItemName;
 end;
 self.Contours_List_ListBox.ItemIndex:= _j;
end;

procedure TFrameChangeSubParcels.SubParcelExistListDorefresh(A, B: Integer);
var
 _SubPacel: IXMLTChangeParcel_SubParcels_ExistSubParcel;
 _i,_j: Integer;
 _ItemName: string;
 xmlNode : IXMLNode;
begin
 if self.Contours_List_ListBox.Items.Count = 0 then EXIT;
 if (A<0) or (a>=self.Contours_List_ListBox.Items.Count)  then  a:= 0;
 if (B<A) or (B>=self.Contours_List_ListBox.Items.Count)  then  B:= self.Contours_List_ListBox.Items.Count-1;

 _j:= self.Contours_List_ListBox.ItemIndex;
 for _i := A to B do 
 begin
  xmlNode:= IXMLNode(pointer(self.Contours_List_ListBox.Items.Objects[_i]));
  _ItemName:= MsXml_ReadAttribute(xmlNode,cnstAtrNumber_Record);
  self.Contours_List_ListBox.Items.Strings[_i]:= _ItemName;
 end;
 self.Contours_List_ListBox.ItemIndex:= _j;
end;

procedure TFrameChangeSubParcels.SubParcelNewListDorefresh(A,B : Integer);
var
 _SubParcel: IXMLTChangeParcel_SubParcels_FormSubParcel;
 _i,_j: Integer;
 _ItemName: string;
 xmlNode : IXMLNode;
begin
 if self.Contours_List_ListBox.Items.Count = 0 then EXIT;
 if (A<0) or (a>=self.Contours_List_ListBox.Items.Count)  then  a:= 0;
 if (B<A) or (B>=self.Contours_List_ListBox.Items.Count)  then  B:= self.Contours_List_ListBox.Items.Count-1;

 _j:= self.Contours_List_ListBox.ItemIndex;
 for _i := A to B do 
 begin
  xmlNode:= IXMLNode(pointer(self.Contours_List_ListBox.Items.Objects[_i]));
 _ItemName:= MsXml_ReadAttribute(xmlNode,cnstAtrDefinition);
  self.Contours_List_ListBox.Items.Strings[_i]:= _ItemName;
 end;
 self.Contours_List_ListBox.ItemIndex:= _j;
end;

procedure TFrameChangeSubParcels.SubParcels_List_DoRefresh(A, B: Integer);
begin
  case self.ActivSubParcelKind of
    cFormSubParcel : SubParcelNewListDorefresh(A,B);
    cExistSubParcel : SubParcelExistListDorefresh(A,B);
    cInvariableSubParcel : SubParcelInvariableListDorefresh(A,B);
  end;
end;

procedure TFrameChangeSubParcels.SubParcels_List_DoSetCurrentItem(IndexItem: integer);
begin
  case self.ActivSubParcelKind of
    cFormSubParcel :
        self.FSubParcel_frame.XmlChangeSubParcel:=  IXMLTChangeParcel_SubParcels_FormSubParcel(pointer(self.Contours_List_ListBox.Items.Objects[IndexItem]));
    cExistSubParcel : 
        self.FSubParcel_frame.XmlChangeExistSubParcel:=  IXMLTChangeParcel_SubParcels_ExistSubParcel(pointer(self.Contours_List_ListBox.Items.Objects[IndexItem]));
    cInvariableSubParcel :
         self.FSubParcel_frame.XmlChangeInvariableSubParcel:= IXMLTChangeParcel_SubParcels_InvariableSubParcel(pointer(self.Contours_List_ListBox.Items.Objects[IndexItem]));
  end;
end;

procedure TFrameChangeSubParcels.SetActivSubParcelUIIndex(index: integer);
begin
 if (index>-1) and (index<self.Contours_List_ListBox.Count) then
 begin
    self.SubParcels_List_DoSetCurrentItem(index);
    self.Contours_List_ListBox.ItemIndex:= index;
    case self.ActivSubParcelKind of
      cFormSubParcel: self.btnEdCurContour.Text:= FXmlChangeSubParcel.FormSubParcel[index].Definition;
      cExistSubParcel: self.btnEdCurContour.Text:= IntToStr(FXmlChangeSubParcel.ExistSubParcel[index].Number_Record);
      cInvariableSubParcel: self.btnEdCurContour.Text:= IntToStr(FXmlChangeSubParcel.InvariableSubParcel[index].Number_Record);
    end;
    self.FSubParcel_frame.Visible:= True;
 end else
 begin
    self.FSubParcel_frame.Visible:= false;
    self.Contours_List_ListBox.ItemIndex:= -1;
    self.btnEdCurContour.Text:='';
    case self.ActivSubParcelKind of
      cFormSubParcel : self.FSubParcel_frame.XmlChangeSubParcel:= nil;
      cExistSubParcel : self.FSubParcel_frame.XmlChangeExistSubParcel := nil;
      cInvariableSubParcel : self.FSubParcel_frame.XmlChangeInvariableSubParcel := nil;
    end;
 end;
end;

constructor TFrameChangeSubParcels.Create(Owner: TComponent);
begin
 inherited;
 self.FSubParcel_frame:= TFrameSubParcel.Create(self);
 self.FSubParcel_frame.OnChange_Attribute_DefOnNum:= self.OnChange_Attribute_DefinitionOrNumberPP;
 self.FSubParcel_frame.Parent:= self;
 self.FSubParcel_frame.Align:= alClient;
 self.FSubParcel_frame.Visible:= False;
end;

function TFrameChangeSubParcels.GetActivSubParcelUIIndex: integer;
begin
 Result:= self.Contours_List_ListBox.ItemIndex;
end;

procedure TFrameChangeSubParcels.getAsText(aSubParcels: TSubParcelKind; var OutputText: string);
var
 _msubParcel: TSpMSubParcel;
begin
 _msubParcel:= TSpMSubParcel.Create;
 TRY
  case aSubParcels of
    cUndefine: ;
 //    cFormSubParcel: TSpConvert.MSubParcel.Convert(FXmlSubParcels.FormSubParcel,_msubParcel);
 //   cExistSubParcel: TSpConvert.MSubParcel.Convert(FXmlSubParcels.ExistSubParcel,_msubParcel);
 //   cInvariableSubParcel: TSpConvert.MSubParcel.Convert(FXmlSubParcels.InvariableSubParcel,_msubParcel);
  end;
  OutputText:= DataFormat.MSubParcel.Text.Build(_msubParcel);
 FINALLY
  _msubParcel.Free;
 END;
end;

function TFrameChangeSubParcels.getActivSubParcelKind: TSubParcelKind;
begin
 if self.cbbContoursType.ItemIndex in [0..2] then
  Result:= TSubParcelKind(self.cbbContoursType.ItemIndex+1)
 else
  Result:= cUndefine;
end;

procedure TFrameChangeSubParcels.InitializationUI;
begin
 FSubParcel_frame.IsTypeLandSubParcel := 0; //����� ����� EntitySpatial
 self.Contours_List_ListBox.Clear;
 if self.IsAssignedSubParcels then
 begin
  self.SubParcels_List_DoUpDate;
  self.SetActivSubParcelUIIndex(0);
 end;
end;

function TFrameChangeSubParcels.IsAssignedSubParcels: Boolean;
begin
   Result := self.FXmlChangeSubParcel <> nil
end;

procedure TFrameChangeSubParcels.OnChange_Attribute_DefinitionOrNumberPP(sender: tobject);
begin
 self.SubParcels_List_DoRefresh(self.Contours_List_ListBox.ItemIndex,self.Contours_List_ListBox.ItemIndex);
end;

procedure TFrameChangeSubParcels.setAsText(aSubParcels: TSubParcelKind;InputText: string);
var
 _msubParcel: TSpMSubParcel;
 _log: TStringBuilder;
 _dlog: TformLog;
begin
 _msubParcel:= nil;
 _log:= TStringBuilder.Create;
 TRY
  _msubParcel:= DataFormat.MSubParcel.Text.Parse(InputText);
  _msubParcel._checkDirection;

  if not TSpValidator.Validate(_msubParcel,_log) then
  begin
    _dlog:= TformLog.Create(nil);
    _dlog.TextLog:= _log.ToString;
    FreeAndNil(_log);
    _dlog.ShowModal;
    if MessageDlg('������������ ��������� ������?',mtConfirmation,mbYesNo,-1) = mrNo then begin
     _dlog.Free;
     EXIT;
  end;
  _dlog.Free;
 end;
 FreeAndNil(_log);
  { �������� �������� Contours or EntitySpatial }

  case aSubParcels of
    cUndefine: ;
//    cFormSubParcel: TSpConvert.MSubParcel.Convert(_msubParcel,FXmlSubParcels.FormSubParcel);
//    cExistSubParcel: TSpConvert.MSubParcel.Convert(_msubParcel,FXmlSubParcels.ExistSubParcel);
//    cInvariableSubParcel: TSpConvert.MSubParcel.Convert(_msubParcel,FXmlSubParcels.InvariableSubParcel);
  end;

 FINALLY
  _msubParcel.Free;
  _log.Free;
 END;
 InitializationUI;
end;

procedure TFrameChangeSubParcels.SetChangeParcel(const aSubParces: IXMLTExistParcel_SubParcels);
begin
 if self.FXmlChangeSubParcel=aSubParces then EXIT;
 self.SetActivSubParcelUIIndex(-1);
 self.FXmlChangeSubParcel:= aSubParces;
 self.InitializationUI;
end;


procedure TFrameChangeSubParcels.setActivSubParcelKind(aValue: TSubParcelKind);
begin
  self._setSubParcelKind(aValue);
  if self.IsAssignedSubParcels then
  begin
        self.FSubParcel_frame.ShowFrames(aValue, (FSubParcel_frame.IsTypeLandSubParcel = 0));
        self.FSubParcel_frame.AsAttributeSubParcel(true);
  end;
   self.SubParcels_List_DoUpDate;
end;

procedure TFrameChangeSubParcels._setSubParcelKind(aValue: TSubParcelKind);
begin
 case aValue of
   cUndefine: self.cbbContoursType.ItemIndex:=-1;
   cFormSubParcel: self.cbbContoursType.ItemIndex:=0;
   cExistSubParcel: self.cbbContoursType.ItemIndex:=1;
   cInvariableSubParcel: self.cbbContoursType.ItemIndex:=2;
 end;
end;

procedure TFrameChangeSubParcels.cbbContoursTypeChange(Sender: TObject);
begin
  self.setActivSubParcelKind(self.getActivSubParcelKind);
end;

procedure TFrameChangeSubParcels.SubParcelsList_Hide;
begin
 self.Contours_List_ListBox.Visible:= False;
end;

procedure TFrameChangeSubParcels.SubParcelsList_Show;
begin
 self.Contours_List_ListBox.Parent:= nil;
 self.Contours_List_ListBox.Parent:= self;
 self.Contours_List_ListBox.Visible:= true;
 self.Contours_List_ListBox.SetFocus;
end;

procedure TFrameChangeSubParcels.SubParcels_List_DoAddNewItem;
var
 _subPacel: IXMLTChangeParcel_SubParcels_FormSubParcel;
 _subParcelExist : IXMLTChangeParcel_SubParcels_ExistSubParcel;
 _subParcelDeleteAllBorder : IXMLTChangeParcel_SubParcels_InvariableSubParcel;
 _ItemName: string;
begin
  case self.ActivSubParcelKind  of
  cFormSubParcel:
  begin
     _subPacel:= FXmlChangeSubParcel.FormSubParcel.Add;
     _ItemName:= IntToStr(FXmlChangeSubParcel.FormSubParcel.Count);
     _subPacel.Definition := _ItemName;
     case indexType  of
       0 :  _subPacel.Entity_Spatial.Spatial_Element.Add;
       1 :  _subPacel.Contours.Add;
     end;
     _subPacel.SubParcel_Realty := False;
     _subPacel.Definition:= _ItemName;
     self.Contours_List_ListBox.AddItem(_ItemName,pointer(_subPacel));
  end;
  cExistSubParcel:
  begin
     _subParcelExist:= FXmlChangeSubParcel.ExistSubParcel.Add;
     _ItemName:= IntToStr(FXmlChangeSubParcel.ExistSubParcel.Count);
     _subParcelExist.Number_Record := strtoint(_ItemName);
     _subParcelExist.Entity_Spatial.Spatial_Element.Add;
      _subParcelExist.Number_Record:= strtoint(_ItemName);
     self.Contours_List_ListBox.AddItem(_ItemName,pointer(_subParcelExist));
  end;
  cInvariableSubParcel:  begin
     _subParcelDeleteAllBorder:= FXmlChangeSubParcel.InvariableSubParcel.Add;
     _ItemName:= IntToStr(FXmlChangeSubParcel.InvariableSubParcel.Count);
     _subParcelDeleteAllBorder.Number_Record:= strtoint(_ItemName);
      self.Contours_List_ListBox.AddItem(_ItemName,pointer(_subParcelDeleteAllBorder));
  end;
  end;
end;


procedure TFrameChangeSubParcels.SubParcels_List_DoDeleteItem(IndexItem: Integer);
var
 _subParcel: IXMLTChangeParcel_SubParcels_FormSubParcel;
 _subParcelExist : IXMLTChangeParcel_SubParcels_ExistSubParcel;
 _subParcelDeleteAllBorder : IXMLTChangeParcel_SubParcels_InvariableSubParcel;
 _index: Integer;
begin
 case self.ActivSubParcelKind of
 cFormSubParcel:
 begin
     _subParcel:= IXMLTChangeParcel_SubParcels_FormSubParcel(pointer(self.Contours_List_ListBox.Items.Objects[IndexItem]));
     if self.FSubParcel_frame.XmlChangeSubParcel=_subParcel then
      self.SetActivSubParcelUIIndex(IndexItem-1);
     self.Contours_List_ListBox.Items.Delete(IndexItem);
     FXmlChangeSubParcel.FormSubParcel.Remove(_subParcel);
     _subParcel:= nil;
 end;
 cExistSubParcel:
 begin
     _subParcelExist:= IXMLTChangeParcel_SubParcels_ExistSubParcel(pointer(self.Contours_List_ListBox.Items.Objects[IndexItem]));
     if self.FSubParcel_frame.XmlChangeExistSubParcel=_subParcelExist then
      self.SetActivSubParcelUIIndex(IndexItem-1);
     self.Contours_List_ListBox.Items.Delete(IndexItem);
     FXmlChangeSubParcel.ExistSubParcel.Remove(_subParcelExist);
     _subParcelExist:= nil;
 end;
 cInvariableSubParcel:
 begin
     _subParcelDeleteAllBorder:= IXMLTChangeParcel_SubParcels_InvariableSubParcel(pointer(self.Contours_List_ListBox.Items.Objects[IndexItem]));
     if self.FSubParcel_frame.XmlChangeInvariableSubParcel=_subParcelDeleteAllBorder then
      self.SetActivSubParcelUIIndex(IndexItem-1);
     self.Contours_List_ListBox.Items.Delete(IndexItem);
     FXmlChangeSubParcel.InvariableSubParcel.Remove(_subParcelDeleteAllBorder);
     _subParcelDeleteAllBorder:= nil;
 end;
 end;
end;

procedure TFrameChangeSubParcels.SubParcels_List_DoUpDate;
var
 _i, _index: Integer;
 _subPacel: IXMLTChangeParcel_SubParcels_FormSubParcel;
 _subParcelExist : IXMLTChangeParcel_SubParcels_ExistSubParcel;
 _subParcelDeleteAllBorder : IXMLTChangeParcel_SubParcels_InvariableSubParcel;
 _ItemName: string;
 subParcel_Realty : Boolean;
 xmlNode : IXMLNode;
 xmlNodeLst : IXMLNodeList;

 procedure  SetActivTypeSubParcelKind();
 begin
   if FXmlChangeSubParcel <> nil then
   begin
     if self.ActivSubParcelKind = cUndefine then
         if FXmlChangeSubParcel.ChildNodes.FindNode(cnstXmlFormSubParcel) <> nil  then
           self.ActivSubParcelKind := cFormSubParcel
         else
           if FXmlChangeSubParcel.ChildNodes.FindNode(cnstXmlExistSubParcel) <> nil then
             self.ActivSubParcelKind := cExistSubParcel
           else
             if FXmlChangeSubParcel.ChildNodes.FindNode(cnstXmlInvariableSubParcel) <> nil then
              self.ActivSubParcelKind := cInvariableSubParcel;
   end
 end;

begin
 SetActivTypeSubParcelKind;
 FSubParcel_frame.Visible := (self.ActivSubParcelKind <> cInvariableSubParcel);
 _index:= self.Contours_List_ListBox.ItemIndex;
 self.Contours_List_ListBox.Clear;
 case self.ActivSubParcelKind of
 cFormSubParcel:
       for _i := 0 to FXmlChangeSubParcel.FormSubParcel.Count-1 do begin
        _subPacel:= FXmlChangeSubParcel.FormSubParcel.Items[_i];
        _ItemName:= MsXml_ReadAttribute(_subPacel,cnstAtrDefinition);
        self.Contours_List_ListBox.AddItem(_ItemName,pointer(_subPacel));
       end;
 cExistSubParcel:
       for _i := 0 to FXmlChangeSubParcel.ExistSubParcel.Count-1 do begin
        _subparcelExist:= FXmlChangeSubParcel.ExistSubParcel.Items[_i];
       _ItemName:= MsXml_ReadAttribute(_subparcelExist,cnstAtrNumber_Record);
        self.Contours_List_ListBox.AddItem(_ItemName,pointer(_subparcelExist));
       end;
 cInvariableSubParcel:
       for _i := 0 to FXmlChangeSubParcel.InvariableSubParcel.Count-1 do begin
        _subParcelDeleteAllBorder:= FXmlChangeSubParcel.InvariableSubParcel.Items[_i];
       _ItemName:= MsXml_ReadAttribute(_subParcelDeleteAllBorder,cnstAtrNumber_Record);
        self.Contours_List_ListBox.AddItem(_ItemName,pointer(_subParcelDeleteAllBorder));
       end;
 end;
 self.SetActivSubParcelUIIndex(_index);
 if not self.FSubParcel_frame.IsAssignedSubParcel then
 self.SetActivSubParcelUIIndex(0);
end;

procedure TFrameChangeSubParcels.Contours_List_ListBoxKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  case key of
   VK_ESCAPE: self.Contours_List_ListBox.Visible:= false;
   VK_RETURN: self.Contours_List_ListBox.Visible:= false;
  end;

end;

procedure TFrameChangeSubParcels.Contours_List_ListBoxMouseLeave(Sender: TObject);
begin
 self.Contours_List_ListBox.Visible:= false;
end;
end.
