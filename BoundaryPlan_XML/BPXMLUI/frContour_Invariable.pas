unit frContour_Invariable;

{
  IXMLTExistParcel_SubParcels_InvariableSubParcel_Contours_Contour
}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ComCtrls,

  FrArea, STD_MP, ActnList, XMLIntf,
  System.Actions,unRussLang;

type
  TFrameContourInvariable = class(TFrame)
    ActionList: TActionList;

    ActionChange_Attribute_DefOrNumPP: TAction;
    ActionChange_ExistingOrNew: TAction;
    Attribute_Panel: TPanel;
    Attribute_DefOrNumPP_Label: TLabel;
    Attribute_DefOrNumPP_Edit: TEdit;
    Area_Panel: TPanel;
    procedure ActionChange_Attribute_DefOrNumPPExecute(Sender: TObject);
    procedure Attribute_DefOrNumPP_EditKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Attribute_NumberPP_EditKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
    FArea_frame          :  TFrameArea;
    FXmlContourInvariable: IXMLTExistParcel_SubParcels_InvariableSubParcel_Contours_Contour;

    FOnChange_Attribute_DefOrNum: TNotifyEvent;

    procedure Data_attribute_NumberPP_write;
    procedure Data_attribute_NumberPP_read;

    procedure Data_DoRead;
    procedure Data_DoWrite;
    procedure Data_DoReadInvariable;
    procedure Data_DoWriteInvariable;


  protected
    procedure SetContourInvariable (const aContourInvariable  : IXMLTExistParcel_SubParcels_InvariableSubParcel_Contours_Contour);
    procedure InitializationUIInvariable;
  public
    { Public declarations }
    constructor Create(Owner: TComponent); override;
    function IsAssignedContourIvariable: boolean;
    procedure AsExistingContour();

    property XmlContourInvariable : IXMLTExistParcel_SubParcels_InvariableSubParcel_Contours_Contour
            read FXmlContourInvariable write SetContourInvariable;
    property OnChange_Attribute_DefOnNum: TNotifyEvent read FOnChange_Attribute_DefOrNum write FOnChange_Attribute_DefOrNum;
   end;

implementation

uses MsXMLAPI;
 const
  _ExistingContour = 1;
  _NewContour = 0;

{$R *.dfm}

{ TFrameContour }

procedure TFrameContourInvariable.ActionChange_Attribute_DefOrNumPPExecute(Sender: TObject);
begin
 if not self.IsAssignedContourIvariable then begin
  self.Attribute_DefOrNumPP_Edit.Text:= '';
  raise Exception.Create(ExMSG_NotAssignedROOT);
 end;
 self.Data_attribute_NumberPP_write;

 if Assigned(self.FOnChange_Attribute_DefOrNum) then
  self.FOnChange_Attribute_DefOrNum(self);
end;

procedure TFrameContourInvariable.AsExistingContour();
var
 _Def: string;
begin
 //�������������� ����������
 self.Attribute_DefOrNumPP_Label.Caption:= '����������� ��� ������� ����� ������� �����:';
  //���� ���� ������� ������
 if self.IsAssignedContourIvariable then begin
  _Def:=  MsXml_ReadAttribute(self.FXmlContourInvariable,'Number');
  MsXml_RemoveAttribute(self.FXmlContourInvariable,'Number');
  if _Def<>'' then begin
   FXmlContourInvariable.Attributes['Number'] := _Def;
   self.Attribute_DefOrNumPP_Edit.Text:= _Def;
  end else
   self.Attribute_DefOrNumPP_Edit.Text:= self.FXmlContourInvariable.Number;
 end;
end;


procedure TFrameContourInvariable.Attribute_DefOrNumPP_EditKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
 if key = VK_RETURN then
  if (Sender is TCustomEdit) then
   TCustomEdit(Sender).Parent.SetFocus;
end;

procedure TFrameContourInvariable.Attribute_NumberPP_EditKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
 if key = VK_RETURN then
  if (Sender is TCustomEdit) then
   TCustomEdit(Sender).Parent.SetFocus;
end;

constructor TFrameContourInvariable.Create(Owner: TComponent);
begin
  inherited;
  self.FArea_frame:= TFrameArea.Create(self.Area_Panel);
  self.FArea_frame.Parent:= self.Area_Panel;
  self.FArea_frame.Align:= alClient;
end;


procedure TFrameContourInvariable.InitializationUIInvariable;
begin
 if self.IsAssignedContourIvariable then begin
   self.Data_DoReadInvariable;
   self.FArea_frame.XmlArea:= self.FXmlContourInvariable.Area;
 end else begin
   self.Attribute_DefOrNumPP_Edit.Text:= '';
   self.FArea_frame.XmlArea:= nil;
 end;
end;


function TFrameContourInvariable.IsAssignedContourIvariable: boolean;
begin
 Result:= self.FXmlContourInvariable<>nil;
end;


procedure TFrameContourInvariable.SetContourInvariable(
  const aContourInvariable: IXMLTExistParcel_SubParcels_InvariableSubParcel_Contours_Contour);
begin
 if self.FXmlContourInvariable = aContourInvariable then EXIT;
 self.FXmlContourInvariable:= aContourInvariable;
 self.InitializationUIInvariable;
end;


procedure TFrameContourInvariable.Data_attribute_NumberPP_read;
var
 _node: IXMLNode;
begin
 _node:= self.FXmlContourInvariable.AttributeNodes.FindNode('Number');
 if (_node<>nil) and (_node.NodeValue<>Null) then
  self.AsExistingContour()
end;

procedure TFrameContourInvariable.Data_attribute_NumberPP_write;
begin
 self.FXmlContourInvariable.Number:= self.Attribute_DefOrNumPP_Edit.Text;
end;

procedure TFrameContourInvariable.Data_DoRead;
begin
 self.Data_attribute_NumberPP_read;
end;

procedure TFrameContourInvariable.Data_DoReadInvariable;
var
 _node: IXMLNode;
begin
 _node:= self.FXmlContourInvariable.AttributeNodes.FindNode('Number');
 if (_node<>nil) and (_node.NodeValue<>Null) then
  self.AsExistingContour()
end;

procedure TFrameContourInvariable.Data_DoWrite;
begin
 self.Data_attribute_NumberPP_write;
end;

procedure TFrameContourInvariable.Data_DoWriteInvariable;
begin
 self.FXmlContourInvariable.Number:= self.Attribute_DefOrNumPP_Edit.Text;
end;

end.

