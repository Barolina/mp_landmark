unit GlbCfg;

interface
 uses

  System.Win.Registry,
  Winapi.Windows,

  System.Classes,
  System.SysUtils,
  System.JSON;
const

//******************����� � ����������� � ���������������� �����****************
 Const_ProxySetting = 'Proxy';
 Const_BugServer = 'BugServer';

 //-----------------------------------------------------------------------------
type
 TMSCfg = class
 private
  FModel:TJSONObject;
  function getItem(aName: string): TJSONObject;
 public
  constructor Create(const aFile: string);
  destructor Destroy; override;

  procedure SaveToFile(const aFile: string);

  property Item[Name:string]: TJSONObject read getItem;
end;

implementation

{TMSCfg}

constructor TMSCfg.Create(const aFile: string);
var
 _strr: TStreamReader;
begin
if aFile<>'' then begin
 _strr:= TStreamReader.Create(aFile,TEncoding.UTF8);
 self.FModel:= TJSONObject(TJSONObject.ParseJSONValue(_strr.ReadToEnd));
 _strr.Free;
end else
 self.FModel:= TJSONObject.Create;
end;

destructor TMSCfg.Destroy;
begin
 self.FModel.Free;
 inherited;
end;

function TMSCfg.getItem(aName: string): TJSONObject;
begin
 if Assigned(self.FModel) then
  Result:= TJSONObject(self.FModel.Get(aName).JsonValue)
 else
  Result:= nil;
end;

procedure TMSCfg.SaveToFile(const aFile: string);
var
 _strmw: TStreamWriter;
begin
 _strmw:= TStreamWriter.Create(aFile,FALSE,TEncoding.UTF8);
 _strmw.Write(self.FModel.ToString);
 _strmw.Flush;
 _strmw.Free;
end;
end.
