unit UnInternet;
 ///
 ///  ��������� ���������� � ����������
 ///

interface

uses
  System.SysUtils, System.Classes,
  Vcl.Controls, Vcl.Forms, Vcl.StdCtrls, Vcl.ExtCtrls, JvRadioGroup,
  Config, JvMaskEdit, Vcl.Mask, JvExMask, JvToolEdit, JvExExtCtrls;

type
  TFrInternet = class(TForm)
    jvrdgrpType: TJvRadioGroup;
    mskPort: TJvMaskEdit;
    lblPort: TLabel;
    lbl1: TLabel;
    edtIP: TEdit;
    procedure jvrdgrpTypeClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure HostExit(Sender: TObject);
    procedure PortExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
  strict private 
    class var instConnect : TFrInternet;   
  public
    { Public declarations }
    class function insConnection : TFrInternet; static;
  end;


implementation
uses BpXML_different;
{$R *.dfm}



procedure TFrInternet.FormCreate(Sender: TObject);
var host : string;
    port : integer;
begin

end;

class function TFrInternet.insConnection: TFrInternet;
begin
  if not Assigned(instConnect) then
    Application.CreateForm(TFrInternet, instConnect);  
  Result := instConnect;  
end;

///
/// <summary>
///     �������� �������� ������ - �������
/// </summary>
///
procedure TFrInternet.jvrdgrpTypeClick(Sender: TObject);
var host : string;
    port : integer;
begin
  TConfig.Section<TProxyOptions>.isProxy := self.jvrdgrpType.ItemIndex;

  self.edtIP.Enabled := jvrdgrpType.ItemIndex= 1;
  self.mskPort.Enabled := jvrdgrpType.ItemIndex= 1;
  if jvrdgrpType.ItemIndex = 1 then
  begin
      self.edtIP.Text := TConfig.Section<TProxyOptions>.Proxy;
      self.mskPort.Text := IntToStr(TConfig.Section<TProxyOptions>.Port);
      if self.edtIP.Text = ''  then
      begin
         GetSettingsProxy(host, port);
         
         self.edtIP.Text := host;
         self.mskPort.Text := IntToStr(port);
      end;
  end;
end;

procedure TFrInternet.FormShow(Sender: TObject);
begin
  jvrdgrpType.ItemIndex := TConfig.Section<TProxyOptions>.isProxy;
  if self.jvrdgrpType.ItemIndex = 1  then  self.jvrdgrpTypeClick(Sender);
end;

procedure TFrInternet.HostExit(Sender: TObject);
begin
  if edtIP.Text = '' then Exit;
  TConfig.Section<TProxyOptions>.Proxy := edtIP.Text;
end;

procedure TFrInternet.PortExit(Sender: TObject);
var port : Integer;
begin
   if (mskPort.Text = '')or (not TryStrToInt(mskPort.Text,port))  then Exit;
   TConfig.Section<TProxyOptions>.Port :=  port;
end;

end.
