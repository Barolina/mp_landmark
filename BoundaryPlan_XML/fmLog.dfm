object formLog: TformLog
  Left = 0
  Top = 0
  BorderStyle = bsSizeToolWin
  Caption = #1046#1091#1088#1085#1072#1083' '#1089#1086#1073#1097#1077#1085#1080#1103
  ClientHeight = 254
  ClientWidth = 424
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  object pnlBottom: TPanel
    Left = 0
    Top = 226
    Width = 424
    Height = 28
    Align = alBottom
    TabOrder = 0
    object btnClose: TButton
      Left = 350
      Top = 1
      Width = 73
      Height = 26
      Align = alRight
      Caption = #1047#1072#1082#1088#1099#1090#1100
      TabOrder = 0
      OnClick = btnCloseClick
    end
    object btng: TButtonGroup
      Left = 1
      Top = 1
      Width = 80
      Height = 26
      Align = alLeft
      BorderStyle = bsNone
      Items = <
        item
          Action = actSaveToFile
        end>
      ShowHint = True
      TabOrder = 1
    end
  end
  object RichEdit: TRichEdit
    Left = 0
    Top = 0
    Width = 424
    Height = 226
    Align = alClient
    BevelKind = bkFlat
    BorderStyle = bsNone
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Lines.Strings = (
      '')
    ParentFont = False
    TabOrder = 1
    Zoom = 100
  end
  object actlst: TActionList
    Left = 16
    Top = 8
    object actSaveToFile: TAction
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1074' '#1092#1072#1081#1083
      Hint = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1074' '#1092#1072#1081#1083
      OnExecute = actSaveToFileExecute
    end
  end
end
