object FrameContractor: TFrameContractor
  Left = 0
  Top = 0
  Width = 597
  Height = 239
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  ParentFont = False
  TabOrder = 0
  object pnlFIO: TPanel
    Left = 0
    Top = 0
    Width = 597
    Height = 110
    Align = alTop
    BevelOuter = bvNone
    Color = clWhite
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
  end
  object pnlContractor: TPanel
    Left = 0
    Top = 110
    Width = 597
    Height = 129
    Align = alClient
    BevelOuter = bvNone
    Color = clWhite
    TabOrder = 1
    object Label_Organization: TLabel
      Left = 9
      Top = 44
      Width = 139
      Height = 13
      Hint = 
        #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1102#1088#1080'-'#1076#1080#1095#1077#1089#1082#1086#1075#1086' '#1083#1080#1094#1072', '#1077#1089#1083#1080' '#1082#1072#1076#1072#1089#1090#1088#1086#1074#1099#1081' '#1080#1085#1078#1077'-'#1085#1077#1088' '#1103#1074#1083#1103#1077 +
        #1090#1089#1103' '#1088#1072#1073#1086#1090'-'#1085#1080#1082#1086#1084' '#1102#1088#1080#1076#1080#1095#1077#1089#1082#1086#1075#1086' '#1083#1080#1094#1072
      Caption = #1053#1072#1079#1074#1072#1085#1080#1077' '#1086#1088#1075#1072#1085#1080#1079#1072#1094#1080#1080
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      Font.Quality = fqClearTypeNatural
      ParentFont = False
    end
    object Label1: TLabel
      Left = 244
      Top = -2
      Width = 105
      Height = 13
      Caption = #1044#1072#1090#1072' '#1087#1088#1086#1074#1077#1076#1077#1085#1080#1103
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      Font.Quality = fqClearType
      ParentFont = False
    end
    object Label4: TLabel
      Left = 9
      Top = -2
      Width = 200
      Height = 13
      Caption = #8470' '#1082#1074#1072#1083#1080#1092#1080#1082#1072#1094#1080#1086#1085#1085#1086#1075#1086' '#1072#1090#1090#1077#1089#1090#1072#1090#1072
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Pitch = fpFixed
      Font.Style = [fsBold]
      Font.Quality = fqClearType
      ParentFont = False
    end
    object Label6: TLabel
      Left = 376
      Top = -2
      Width = 53
      Height = 13
      Caption = #1058#1077#1083#1077#1092#1086#1085
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      Font.Quality = fqClearTypeNatural
      ParentFont = False
    end
    object Label7: TLabel
      Left = 376
      Top = 41
      Width = 61
      Height = 13
      Caption = 'E-mail '#1072#1076#1088#1077#1089
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Font.Quality = fqClearTypeNatural
      ParentFont = False
    end
    object Label8: TLabel
      Left = 10
      Top = 88
      Width = 101
      Height = 13
      Caption = #1040#1076#1088#1077#1089' '#1076#1083#1103' '#1089#1074#1103#1079#1080
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      Font.Quality = fqClearTypeNatural
      ParentFont = False
    end
    object Address_Edit: TEdit
      Left = 8
      Top = 102
      Width = 579
      Height = 21
      BevelKind = bkFlat
      BorderStyle = bsNone
      Color = 15921123
      MaxLength = 250
      TabOrder = 4
      TextHint = #1040#1076#1088#1077#1089' '#1076#1083#1103' '#1089#1074#1103#1079#1080
      StyleElements = []
      OnExit = Address_EditExit
      OnKeyUp = Address_EditKeyUp
    end
    object Email_Edit: TEdit
      Left = 376
      Top = 59
      Width = 211
      Height = 21
      BevelKind = bkFlat
      BorderStyle = bsNone
      Color = clWhite
      MaxLength = 100
      TabOrder = 3
      TextHint = 'E-mail '#1072#1076#1088#1077#1089
      StyleElements = []
      OnExit = Email_EditExit
      OnKeyUp = Email_EditKeyUp
    end
    object NCertificate_Edit: TEdit
      Left = 8
      Top = 13
      Width = 214
      Height = 22
      BevelKind = bkFlat
      BorderStyle = bsNone
      Color = 15921123
      MaxLength = 50
      TabOrder = 0
      TextHint = #1053#1086#1084#1077#1088' '#1082#1074#1072#1083#1080#1092#1080#1082#1072#1094#1080#1086#1085#1085#1086#1075#1086' '#1072#1090#1090#1077#1089#1090#1072#1090#1072
      StyleElements = []
      OnExit = NCertificate_EditExit
      OnKeyUp = NCertificate_EditKeyUp
    end
    object Organization_Edit: TEdit
      Left = 8
      Top = 59
      Width = 341
      Height = 21
      Hint = #1053#1072#1079#1074#1072#1085#1080#1077' '#1086#1088#1075#1072#1085#1080#1079#1072#1094#1080#1080
      BevelKind = bkFlat
      BorderStyle = bsNone
      Color = 15921123
      MaxLength = 255
      ParentShowHint = False
      ShowHint = False
      TabOrder = 2
      TextHint = #1053#1072#1079#1074#1072#1085#1080#1077' '#1086#1088#1075#1072#1085#1080#1079#1072#1094#1080#1080
      StyleElements = []
      OnExit = Organization_EditExit
      OnKeyUp = Organization_EditKeyUp
    end
    object Telephone_Edit: TEdit
      Left = 376
      Top = 14
      Width = 211
      Height = 21
      BevelKind = bkFlat
      BorderStyle = bsNone
      Color = 15921123
      MaxLength = 50
      TabOrder = 1
      TextHint = #1058#1077#1083#1077#1092#1086#1085
      StyleElements = []
      OnExit = Telephone_EditExit
      OnKeyUp = Telephone_EditKeyUp
    end
    object jvdtpckrdtDate: TJvDatePickerEdit
      Left = 243
      Top = 13
      Width = 106
      Height = 21
      AllowNoDate = True
      BorderStyle = bsNone
      Checked = True
      Color = 15921123
      DateFormat = 'yyyy-MM-dd'
      DateSeparator = '-'
      BevelKind = bkFlat
      StoreDateFormat = True
      TabOrder = 5
      OnExit = jvdtpckrdtDateExit
    end
  end
end
