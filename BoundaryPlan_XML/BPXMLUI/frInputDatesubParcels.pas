unit frInputDatesubParcels;
    {
    �������� � ������ ��������, ���������� ��� ���������� ��������� ��������
    IXMLSTD_MP_Input_Data_SubParcels
    }
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ButtonGroup, StdCtrls, ActnList,Vcl.Menus, Vcl.ImgList, Vcl.ExtCtrls, Vcl.Grids,

  STD_MP, JvgStringGrid, JvExGrids, JvStringGrid;

const cnstWCadNum =318;
      cnstWNumber_Record  = 409;
      cnstWNN = 35;
      cnstColCount = 3;
      cnstNN = 0;
      cnstNCadNum= 1;
      cnstNNumber_Record = 2;
  
type
  THackGrid = class(TStringGrid);

type
  TFrameInputDateSubParcels = class(TFrame)
    Panel: TPanel;
    stringGridDate: TStringGrid;
    poMenuTable: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    pnlLabelTabl: TPanel;
    lblCadNum: TLabel;
    lblNumber_Record: TLabel;
    splCadNUm: TSplitter;
    splNumberRecord: TSplitter;
    lblN: TLabel;
    spl4: TSplitter;
    procedure SubParcels_Actions_ButtonGroupItems0Click(Sender: TObject);
    procedure stringGridDateSetEditText(Sender: TObject; ACol, ARow: Integer;
      const Value: string);
    procedure stringGridDateKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SubParcels_Actions_ButtonGroupItems1Click(Sender: TObject);
    procedure splNumberRecordMoved(Sender: TObject);
    procedure splCadNUmMoved(Sender: TObject);
  private
    { Private declarations }
     FXmlSubParcels: IXMLSTD_MP_Input_Data_SubParcels;
     FXMLSubParcel : IXMLSTD_MP_Input_Data_SubParcels_SubParcel;

     procedure UI_ReadOf(const aSubParcel: IXMLSTD_MP_Input_Data_SubParcels);
     procedure Data_SubParcel_DoAddNew;
     procedure Data_SubParcel_DoDelete;

  protected
     procedure SetSubParcels(const aSubParcels: IXMLSTD_MP_Input_Data_SubParcels);
     procedure InitializationUI;

  public
      { Public declarations }
     procedure AddRowStrGrid();
     procedure DelRowStrGrid(aRow : integer);
    
     constructor Create(Owner: TComponent); override;
     function   IsAssigned_SubParcels: Boolean;
     property XmlSubParcels: IXMLSTD_MP_Input_Data_SubParcels read FXmlSubParcels write SetSubParcels;
  end;

implementation


{$R *.dfm}

{ TFrameInputDateSubParcels}

procedure TFrameInputDateSubParcels.AddRowStrGrid;
var rowCount  : integer;
begin
  rowCount := stringGridDate.RowCount;
  StringGridDate.RowCount := rowCount +1;
  stringGridDate.Cells[0,rowCount] := IntToStr(rowCount+1);
end;

constructor TFrameInputDateSubParcels.Create(Owner: TComponent);
begin
  inherited;
  FXmlSubParcels := nil;
  FXMLSubParcel :=  nil;
  {setting ^)}
  stringGridDate.ColCount := cnstColCount;
  stringGridDate.ColWidths[cnstNN] := cnstWNN; 
  stringGridDate.ColWidths[cnstNCadNum] :=cnstWCadNum; 
  stringGridDate.ColWidths[cnstNNumber_Record] :=cnstWNumber_Record; 
  lblCadNum.Width := cnstWCadNum; 
  lblNumber_Record.Width := cnstWNumber_Record;
  lblN.Width := cnstWNN;

  self.InitializationUI;
end;

procedure TFrameInputDateSubParcels.Data_SubParcel_DoAddNew;
begin
  if not self.IsAssigned_SubParcels then EXIT;
  AddRowStrGrid;
  FXMLSubParcel := FXmlSubParcels.Add;
end;

procedure TFrameInputDateSubParcels.Data_SubParcel_DoDelete;
var FArow : integer;
begin
   FArow := StringGridDate.Row;
   if (MessageDlg('������� ������ � '+IntToStr(FArow+1)+'?', mtInformation, mbYesNo,-1) = mrYes )
      and (FArow >=0) and (FXmlSubParcels.Count > FArow) then
   begin
     FXMLSubParcel := FXmlSubParcels.SubParcel[FArow];
     FXmlSubParcels.Remove(FXMLSubParcel);
     FXMLSubParcel := nil;
     DelRowStrGrid(FArow);
   end;
end;

procedure TFrameInputDateSubParcels.DelRowStrGrid(aRow: integer);
begin
  if aRow < 0 then Exit;
  THackGrid(stringGridDate).Rows[aRow].Clear;
  if aRow > 0 then THackGrid(stringGridDate).DeleteRow(aRow)
end;

procedure TFrameInputDateSubParcels.InitializationUI;
begin
  stringGridDate.RowCount := 1;
  THackGrid(stringGridDate).Rows[0].Clear;
  stringGridDate.Cells[cnstNN,0] := '1';
  if  IsAssigned_SubParcels then   UI_ReadOf(FXmlSubParcels);
end;


function TFrameInputDateSubParcels.IsAssigned_SubParcels: Boolean;
begin
 Result := FXmlSubParcels <> nil;
end;

procedure TFrameInputDateSubParcels.SetSubParcels(
  const aSubParcels: IXMLSTD_MP_Input_Data_SubParcels);
begin
 if FXmlSubParcels = aSubParcels then Exit;
 FXmlSubParcels := aSubParcels;
 InitializationUI;
end;

procedure TFrameInputDateSubParcels.splCadNUmMoved(Sender: TObject);
begin
 self.stringGridDate.ColWidths[cnstNCadNum] := lblCadNum.Width;
end;

procedure TFrameInputDateSubParcels.splNumberRecordMoved(Sender: TObject);
begin
 self.stringGridDate.ColWidths[cnstNNumber_Record] := lblNumber_Record.Width;
end;

procedure TFrameInputDateSubParcels.stringGridDateKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var i : integer;
begin
  if (Key = VK_DELETE) and (ssCtrl in Shift) then Data_SubParcel_DoDelete;
  if (Key = VK_INSERT) and (ssCtrl in Shift )then Data_SubParcel_DoAddNew;
end;

procedure  TFrameInputDateSubParcels.stringGridDateSetEditText(Sender: TObject; ACol,
  ARow: Integer; const Value: string);
begin
  if not IsAssigned_SubParcels then Exit;
  if Value = '' then Exit;
  
  if ARow  >= 0 then
  begin
    if FXmlSubParcels.Count > ARow then FXMLSubParcel := FXmlSubParcels.SubParcel[ARow]
    else  FXMLSubParcel := FXmlSubParcels.Add;
    case ACol of
      cnstNCadNum : FXMLSubParcel.CadastralNumber := Value; 
      cnstNNumber_Record : FXMLSubParcel.Number_Record := Value;
    end;
  end;
end;

procedure TFrameInputDateSubParcels.SubParcels_Actions_ButtonGroupItems0Click(
  Sender: TObject);                                                             
begin
 Data_SubParcel_DoAddNew;
end;

procedure TFrameInputDateSubParcels.SubParcels_Actions_ButtonGroupItems1Click(
  Sender: TObject);
begin
 Data_SubParcel_DoDelete
end;

procedure TFrameInputDateSubParcels.UI_ReadOf(const aSubParcel: IXMLSTD_MP_Input_Data_SubParcels);
var  i  : integer;
begin
  StringGridDate.RowCount := aSubParcel.Count;
  for I := 0 to aSubParcel.Count -1 do
  begin
      FXMLSubParcel := aSubParcel.SubParcel[i];
      stringGridDate.Cells[cnstNN,i] := IntToStr(i+1);
      stringGridDate.Cells[cnstNCadNum,i] := FXMLSubParcel.CadastralNumber;
      stringGridDate.Cells[cnstNNumber_Record,i] := FXMLSubParcel.Number_Record;
  end;
  FXMLSubParcel := nil;
end;


end.
