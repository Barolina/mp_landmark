{*******************************************************}
{      |----------------------------------------|       }
{      |- (c) 2010-2011 ���������� ���������.  -|       }
{      |-          kras_ai@mail.ru             -|       }
{      |----------------------------------------|       }
{*******************************************************}

// 11.09.12 -�������� �����- function TMSMPoints.GetItemByShift(BaseIndex, ShiftValue: integer): TMSMPoint;

{$define testTMSMPoints}
unit MSMType_;

interface

 uses
  {$ifdef testTMSMPoints}
   System.Generics.Collections,
  {$endif}
  System.Classes,
  System.Contnrs,
  System.Types,

  math;

 type
  TMSMBase = class abstract(TObject);

//�����-------------------------------------------------------------------------
  TMSMPointData = record
   X: Double;
   Y: Double;
   Caption: string;
  end;

  TMSMPoint = class(TMSMBase)
   private
    FX: Double;
    FY: Double;
    FCaption: string;
   protected
    procedure DoSetX(Value: Double);
    procedure DoSetY(Value: Double);
    procedure DoSetCaption(Value: string);

    procedure SetX(Value: Double); virtual;
    procedure SetY(Value: Double); virtual;
    procedure SetCaption(Value: string); virtual;
   public
    constructor Create(x,y: Double);
    function GetCopy: TMSMPoint;

    function IsEqualPos(x,y: Double): boolean; overload;
    function IsEqualPos(aPoint: TMSMPoint): boolean; overload;
    function GetDistance(x,y: Double): Double; overload;
    function GetDistance(aPoint:TMSMPoint): Double; overload;

    procedure DoMove(Dx,Dy: Double);
    procedure DoAssign(sP: TMSMPoint);

    procedure GetData(var Value: TMSMPointData);
    procedure SetData(Value: TMSMPointData);

    property X: Double read FX write SetX;
    property Y: Double read FY write SetY;
    property Caption: String read FCaption write SetCaption;
  end;
//------------------------------------------------------------------------------

//�������-----------------------------------------------------------------------
  TMSMSegmentData = record
    A: TMSMPointData;
    B: TMSMPointData;
   end;

  TMSMSegment = class(TMSMBase)
   private
    FA: TMSMPoint;
    FB: TMSMPoint;
   protected
    procedure SetA(Value: TMSMPoint);
    procedure SetB(Value: TMSMPoint);
    function GetA: TMSMPoint;
    function GetB: TMSMPoint;
   public
    constructor Create(Ax,Ay,Bx,By: double);
    destructor Destroy; override;
    function GetCopy: TMSMSegment;

    procedure GetData(var Value: TMSMSegmentData);
    procedure SetData(Value: TMSMSegmentData);

    property A: TMSMPoint read GetA;
    property B: TMSMPoint read GetB;
   end;
//------------------------------------------------------------------------------


//������������� �������---------------------------------------------------------
  TMSMRectData = Record
   A: TMSMPointData;
   B: TMSMPointData;
  end;

  TMSMRect = class(TMSMBase)
    private
     FA: TMSMPoint;
     FB: TMSMPoint;
    public
     constructor Create(Ax,Ay,Bx,By: double);
     destructor Destroy; override;
     function GetCopy: TMSMRect;

     function IsContainsPoint(x,y:double): boolean; overload; //�������������� ����� ������������� �������
     function IsContainsPoint(P: TMSMPoint): boolean; overload;
     function IsContainsRect(x1,y1,x2,y2: Double): boolean; overload; //true ���� ������ ��� ���������!!!
     function IsContainsRect(R: TMSMRect): boolean; overload; //true ���� ������ ��� ���������!!!
     function IsIntersection(x1,y1,x2,y2: Double): Boolean; overload; //����������, �������, ����������, ���������
     function IsIntersection(R: TMSMRect): Boolean; overload;

     function GetSize: double;//������ ���������
     function GetSideX: double;// ������ ������� �� X (���� �� ������������ �� ��. ��. ������������)
     function GetSideY: double;// ������ ������� �� Y (���� �� ������������ �� ��. ��. ������������)
     function GetCentreX: double;//���������� X ����� ������ ��������������
     function GetCentreY: double;//���������� Y ����� ������ ��������������
     function GetCentrePoint: TMSMPoint;

     procedure DoEnlarge(Value: Double); //������ � ������ ���������� �� Value*2 ������������ ������
     function DoCheckOrientation: boolean; //�������� � ���� Ax<Bx,Ay<By (���������� ��������������). ���� true �� ���������� ���� ����������
     procedure DoAssign(R: TMSMRect);//�������� ��������

     property Apoint: TMSMPoint read FA;
     property Bpoint: TMSMPoint read FB;
   end;
//------------------------------------------------------------------------------


//������ �����------------------------------------------------------------------
  {$ifdef testTMSMPoints}
  TMSMPointsCustom<T: TMSMPoint> = class(TObjectList<T>)
  public
    function GetItemByShift(BaseIndex: Integer; ShiftValue: integer): T;
    procedure Assign(Collection:TMSMPointsCustom<T>);
    //���������� ������ ������� �������. ���� ������� �� ������� �� ����������� ������������ -1
    function IndexOfAdjacent(Item: T): integer;
    function IndexOfAdjacentItem(Item: T; ADirection: TDirection): integer;
  end;
  TMSMPoints = class(TMSMPointsCustom<TMSMPoint>);
  //TMSMPoints = TMSMPointsCustom;
  {$else}
  TMSMPoints = class(TObjectList)
  protected
    function GetItem(Index: Integer): TMSMPoint; inline;
    procedure SetItem(Index: Integer; AObject: TMSMPoint); inline;
  public
    function Add(AObject: TMSMPoint): Integer; inline;
    function Extract(Item: TMSMPoint): TMSMPoint; inline;
    function ExtractItem(Item: TMSMPoint; Direction: TDirection): TMSMPoint; inline;
    function Remove(AObject: TMSMPoint): Integer; overload; inline;
    function RemoveItem(AObject: TMSMPoint; ADirection: TDirection): Integer; inline;
    function IndexOf(AObject: TMSMPoint): Integer; inline;
    function IndexOfItem(AObject: TMSMPoint; ADirection: TDirection): Integer; inline;
    procedure Insert(Index: Integer; AObject: TMSMPoint); inline;
    function First: TMSMPoint; inline;
    function Last: TMSMPoint; inline;
    //������� ������������� ������ ����� ��� ������. ����������� ����������
    //����� ����������� �� ��������� �������� ShifValue ������������ BaseIndex.
    //�������� ����� ���� ��� ������������� ��� � �������������
    function GetItemByShift(BaseIndex: Integer; ShiftValue: integer): TMSMPoint; inline;

    //���������� ������ ������� �������. ���� ������� �� ������� �� ����������� ������������ -1
    function IndexOfAdjacent(Item: TMSMPoint): integer; inline;
    function IndexOfAdjacentItem(Item: TMSMPoint; ADirection: TDirection): integer; inline;

    property Items[Index: Integer]: TMSMPoint read GetItem write SetItem; default;
  end;
  {$endif}
//------------------------------------------------------------------------------

implementation

 uses
  MSMCommon_;

{ TMSPointR }

function TMSMPoint.IsEqualPos(aPoint: TMSMPoint): boolean;
begin
 Result:= self.IsEqualPos(aPoint.X,aPoint.Y);
end;


procedure TMSMPoint.SetCaption(Value: string);
begin
 self.DoSetCaption(Value);
end;

procedure TMSMPoint.SetData(Value: TMSMPointData);
begin
 self.SetX(Value.X);
 self.SetY(Value.Y);
 self.SetCaption(Value.Caption);
end;

procedure TMSMPoint.SetX(Value: Double);
begin
 self.DoSetX(Value);
end;

procedure TMSMPoint.SetY(Value: Double);
begin
 self.DoSetY(Value);
end;

procedure TMSMPoint.DoSetCaption(Value: string);
begin
 self.FCaption:= Value;
end;

procedure TMSMPoint.DoSetX(value: double);
begin
 self.FX:= Value;
end;

procedure TMSMPoint.DoSetY(value: double);
begin
 self.FY:= Value;
end;

function TMSMPoint.IsEqualPos(x, y: Double): boolean;
begin
 Result:= msmcIsEqual(self.x,x) and msmcIsEqual(self.y,y);
end;

constructor TMSMPoint.Create(x, y: Double);
begin
 self.SetX(x);
 self.SetY(y);
end;

function TMSMPoint.GetDistance(x, y: Double): Double;
begin
 try
  Result:= msmcGetDistanceBetweenPoints(self.x,self.y,x,y);
 except
  Result:= 0;
 end;
end;

function TMSMPoint.getCopy: TMSMPoint;
begin
 Result:=  TMSMPoint.create(self.X,self.Y);
 //Result.Caption:= self.FCaption;
end;

procedure TMSMPoint.GetData(var Value: TMSMPointData);
begin
 Value.X:= self.X;
 Value.Y:= self.Y;
 Value.Caption:= self.Caption;
end;

function TMSMPoint.GetDistance(aPoint: TMSMPoint): Double;
begin
 Result:= self.GetDistance(aPoint.X,aPoint.Y);
end;

procedure TMSMPoint.DoAssign(sP: TMSMPoint);
begin
 self.SetX(sP.X);
 self.SetY(sP.Y);
// self.Caption:= sP.Caption;
end;

procedure TMSMPoint.DoMove(Dx, Dy: Double);
begin
 self.SetX(self.x+Dx);
 self.SetY(self.y+Dy);
end;


{ TMSRectR }

procedure TMSMRect.DoAssign(R: TMSMRect);
begin
 self.FA.DoAssign(R.Apoint);
 self.FB.DoAssign(R.Bpoint);
end;

function TMSMRect.DoCheckOrientation: boolean;
var
 temp: double;
begin
 Result:= false;
  if self.FA.X>self.FB.X then begin
   temp:= self.FA.X;
   self.FA.X:= self.FB.X;
   self.FB.X:=temp;
   Result:= true;
  end;

  if self.FA.Y>self.FB.Y then begin
   temp:= self.FA.Y;
   self.FA.Y:= self.FB.Y;
   self.FB.Y:= temp;
   Result:= true;
  end;
end;

constructor TMSMRect.Create(AX, AY, BX, BY: double);
begin
 self.FA:= TMSMPoint.create(Ax,Ay);
 self.FB:= TMSMPoint.create(BX,BY);
end;

destructor TMSMRect.Destroy;
begin
 self.FA.Free;
 self.FB.Free;
 inherited;
end;

procedure TMSMRect.DoEnlarge(Value: Double);
begin
 if Value = 0 then Exit;

 if msmcIsLE(Self.FA.X,Self.FB.X) then begin
  self.FA.X:= self.FA.X-Value;
  self.FB.X:= self.FB.X+Value;
 end else begin
  self.FA.X:= self.FA.X+Value;
  self.FB.X:= self.FB.X-Value;
 end;

 if msmcIsLE(self.FA.Y,self.FB.Y) then begin
  self.FA.Y:= self.FA.Y-Value;
  self.FB.Y:= self.FB.Y+Value;
 end else begin
  self.FA.Y:= self.FA.Y+Value;
  self.FB.Y:= self.FB.Y-Value;
 end;
end;


function TMSMRect.GetCentrePoint: TMSMPoint;
begin
 Result:= TMSMPoint.Create(self.GetCentreX,self.GetCentreY);
end;

function TMSMRect.GetCentreX: double;
begin
 Result:= (self.FA.X+self.FB.X)/2;
end;

function TMSMRect.GetCentreY: double;
begin
 Result:= (self.FA.Y+self.FB.Y)/2;
end;

function TMSMRect.GetCopy: TMSMRect;
begin
 Result:= TMSMRect.Create(self.Apoint.X,self.Apoint.Y,self.Bpoint.X,self.Bpoint.Y);
end;

function TMSMRect.getSideX: double;
begin
 result:= self.FB.X-self.FA.X;
end;

function TMSMRect.getSideY: double;
begin
  result:= self.FB.Y-self.FA.Y;
end;

function TMSMRect.getSize: double;
begin
 result:= self.FA.GetDistance(self.FB.X,self.FB.Y);
end;

function TMSMRect.IsContainsRect(x1,y1,x2,y2: Double): boolean;
begin
 Result:= msmcIsRectInRect(x1,y1,x2,y2,self.FA.X,self.FA.Y,self.FB.X,self.FB.Y);
end;

function TMSMRect.IsContainsPoint(x, y: double): boolean;
begin
 Result:= msmcIsPointInRect(self.FA.X,self.FA.Y,self.FB.X,self.FB.Y,x,y);
end;

function TMSMRect.IsIntersection(x1,y1,x2,y2: Double): Boolean;
begin
 Result:= msmcIsIntersectRect(self.FA.X,self.FA.Y,self.FB.X,self.FB.Y,x1,y1,x2,y2);
end;

function TMSMRect.IsContainsPoint(P: TMSMPoint): boolean;
begin
 Result:= self.IsContainsPoint(p.X,p.Y);
end;

function TMSMRect.IsContainsRect(R: TMSMRect): boolean;
begin
 Result:= self.IsContainsRect(R.Apoint.X,R.Apoint.Y,R.Bpoint.X,R.Bpoint.Y)
end;

function TMSMRect.IsIntersection(R: TMSMRect): Boolean;
begin
 Result:= self.IsIntersection(R.Apoint.X,R.Apoint.Y,R.Bpoint.X,R.Bpoint.Y);
end;

{$REGION  'TMSMPoints'}
{$IFDEF testTMSMPoints} {$else}
function TMSMPoints.Add(AObject: TMSMPoint): Integer;
begin
 Result:= inherited Add(AObject);
end;

function TMSMPoints.Extract(Item: TMSMPoint): TMSMPoint;
begin
 Result:= TMSMPoint( inherited Extract(Item));
end;

function TMSMPoints.ExtractItem(Item: TMSMPoint; Direction: TDirection): TMSMPoint;
begin
 Result:= TMSMPoint (inherited ExtractItem(Item,Direction));
end;

function TMSMPoints.First: TMSMPoint;
begin
 Result:= TMSMPoint( inherited First);
end;

function TMSMPoints.GetItem(Index: Integer): TMSMPoint;
begin
 Result:= TMSMPoint( inherited GetItem(Index));
end;

function TMSMPoints.GetItemByShift(BaseIndex, ShiftValue: integer): TMSMPoint;
begin
 if self.Count=0 then
   self.Error('����������� ������� � ������',BaseIndex);

 if ShiftValue=0 then begin
  Result:= self.GetItem(BaseIndex);
  EXIT;
 end;

 ShiftValue:= (ShiftValue + BaseIndex) mod self.Count;
 if ShiftValue<0 then
  ShiftValue:= self.Count+ShiftValue;

 Result:= self.GetItem(ShiftValue)
end;

function TMSMPoints.IndexOf(AObject: TMSMPoint): Integer;
begin
 Result:= inherited IndexOf(AObject);
end;

function TMSMPoints.IndexOfAdjacent(Item: TMSMPoint): integer;
begin
 Result:= self.IndexOfAdjacentItem(Item,FromBeginning);
end;

function TMSMPoints.IndexOfAdjacentItem(Item: TMSMPoint;  ADirection: TList.TDirection): integer;
begin
 case ADirection of
   FromBeginning: begin
                   for Result:= 0 to self.Count - 1 do
                    if self.GetItem(Result).IsEqualPos(item) then
                    EXIT;
                   Result:= -1;
                  end;
   FromEnd:begin
            for Result:= self.Count - 1 downto 0 do
             if self.GetItem(Result).IsEqualPos(item) then
              EXIT;
            Result:= -1;
           end;
 end;
end;

function TMSMPoints.IndexOfItem(AObject: TMSMPoint; ADirection: TDirection): Integer;
begin
 Result:= inherited IndexOfItem(AObject,ADirection);
end;

procedure TMSMPoints.Insert(Index: Integer; AObject: TMSMPoint);
begin
 inherited Insert(Index, AObject);
end;

function TMSMPoints.Last: TMSMPoint;
begin
  result:= TMSMPoint( inherited Last);
end;

function TMSMPoints.Remove(AObject: TMSMPoint): Integer;
begin
 Result:= inherited Remove(AObject);
end;

function TMSMPoints.RemoveItem(AObject: TMSMPoint; ADirection: TDirection): Integer;
begin
 result:= inherited RemoveItem(AObject,ADirection);
end;

procedure TMSMPoints.SetItem(Index: Integer; AObject: TMSMPoint);
begin
 inherited SetItem(Index,AObject);
end;
{$EndIf}
{$ENDREGION}

{ TMSMSegment }

constructor TMSMSegment.Create(Ax, Ay, Bx, By: double);
begin
 self.FA:= TMSMPoint.create(Ax,Ay);
 self.FB:= TMSMPoint.create(BX,BY);
end;

destructor TMSMSegment.Destroy;
begin
 self.FA.Free;
 self.FB.Free;
 inherited;
end;

function TMSMSegment.GetA: TMSMPoint;
begin
 Result:= self.FA;
end;

function TMSMSegment.GetB: TMSMPoint;
begin
 Result:= self.FB;
end;

function TMSMSegment.GetCopy: TMSMSegment;
begin
 Result:= TMSMSegment.Create(self.FA.X,self.FA.Y,self.FB.X,self.FB.Y);
end;

procedure TMSMSegment.GetData(var Value: TMSMSegmentData);
begin
 self.FA.GetData(Value.A);
 self.FB.GetData(Value.B);
end;

procedure TMSMSegment.SetA(Value: TMSMPoint);
begin
 self.FA.Free;
 self.FA:= Value;
end;

procedure TMSMSegment.SetB(Value: TMSMPoint);
begin
 self.FB.Free;
 self.FB:= Value;
end;

procedure TMSMSegment.SetData(Value: TMSMSegmentData);
begin
 self.FA.SetData(Value.A);
 self.FB.SetData(Value.B);
end;

{$ifdef testTMSMPoints}
{$REGION 'TMSMPointsCustom<T>'}

procedure TMSMPointsCustom<T>.Assign(Collection:TMSMPointsCustom<T>);
begin
 self.Clear;
 self.AddRange(Collection);
end;

function TMSMPointsCustom<T>.GetItemByShift(BaseIndex, ShiftValue: integer): T;
begin
  if self.Count=0 then
   self.Error('����������� ������� � ������',BaseIndex);

 if ShiftValue=0 then begin
  Result:= self.Items[BaseIndex];
  EXIT;
 end;

 ShiftValue:= (ShiftValue + BaseIndex) mod self.Count;
 if ShiftValue<0 then
  ShiftValue:= self.Count+ShiftValue;

 Result:= self.Items[ShiftValue]
end;

function TMSMPointsCustom<T>.IndexOfAdjacent(Item: T): integer;
begin
 Result:= self.IndexOfAdjacentItem(Item,FromBeginning);
end;

function TMSMPointsCustom<T>.IndexOfAdjacentItem(Item: T;
  ADirection: TDirection): integer;
begin
case ADirection of
   FromBeginning: begin
                   for Result:= 0 to self.Count - 1 do
                    if self.Items[Result].IsEqualPos(item) then
                    EXIT;
                   Result:= -1;
                  end;
   FromEnd:begin
            for Result:= self.Count - 1 downto 0 do
             if self.Items[Result].IsEqualPos(item) then
              EXIT;
            Result:= -1;
           end;
 end;
end;
{$ENDREGION}
{$endif}

end.

