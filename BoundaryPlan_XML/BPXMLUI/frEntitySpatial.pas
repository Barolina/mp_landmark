unit frEntitySpatial;

interface
uses
 system.classes,
 System.SysUtils,

 vcl.controls,

 Vcl.Forms,

 fmspVCLEditor, STD_MP,
 SurveyingPlan.Xml.Types,
 SurveyingPlan.Xml.Utils,
 Dialogs,
 Variants;

 type

  TFrameEntity_Spatial = class(TFrame)
    procedure FrameExit(Sender: TObject);
  private
   FEs: IXMLEntity_Spatial;
   FEditor: TFormEntitySpatial;
  protected
   procedure setES(Es: IXMLEntity_Spatial);
   function getEs: IXMLEntity_Spatial;
   procedure _doSave(Sender: TObject);
  public
   constructor Create(AOwner: TComponent); override;
   function FlushChanged(Log: TStringBuilder = nil; IgnoreValidate: boolean = false): boolean;
   property EntitySpatial: IXMLEntity_Spatial read getES write setEs;
  end;

implementation

uses fmLog;
{$R *.dfm}
{ TFrameEntity_Spatial }

constructor TFrameEntity_Spatial.Create(AOwner: TComponent);
begin
  inherited;
 self.FEditor:= TFormEntitySpatial.Create(nil);
 self.FEditor.Parent:= self;
 self.FEditor.Align:= alClient;
 self.FEditor.OnNeedSave:= self._doSave;
 self.FEditor.Show;
end;

function TFrameEntity_Spatial.FlushChanged(Log: TStringBuilder; IgnoreValidate: boolean): boolean;
var
 _test: TSpContour;
 fl : Boolean;
begin
 Result:= false;
 if not IgnoreValidate then
   if not self.FEditor.isValidate(Log) then EXIT;

 _test:= TSpContour.Create(true);
 self.FEditor.WriteData(_test);
 {��� ������� ���� ������� �� ���������� Borders}
 try
   if ((FEs.ParentNode<> nil ) and (Length(FEs.ParentNode.NodeName)>0)) and
       ((FEs.ParentNode <> nil) and ((FEs.ParentNode.NodeName = 'Contour') or
          {�������� ������ ������������ ������ ����������� �������� ������� (� �.�. �������������� ������)}
       ((FEs.ParentNode.NodeName ='ExistSubParcel')and (FEs.ParentNode.ParentNode.NodeName = 'ExistSubParcels')) or
       (FEs.ParentNode.NodeName = 'NewSubParcel'))) then
       TSpConvert.Convert(_test,self.FEs,false)
    else TSpConvert.Convert(_test,self.FEs);
 except
   begin
     TSpConvert.Convert(_test,self.FEs,false);
  //TODO: ERROR   raise Exception.Create('FEs.ParentNode.NodeName = nil; ')  at ReturnAddress;
   end;
 end;

 Result:= true;
  _test.Free;
  
end;

procedure TFrameEntity_Spatial.FrameExit(Sender: TObject);
begin
 self.FlushChanged(nil,TRUE);
end;

function TFrameEntity_Spatial.getEs: IXMLEntity_Spatial;
begin
 Result:= self.FEs;
end;

procedure TFrameEntity_Spatial.setES(Es: IXMLEntity_Spatial);
var
 _temp: TSpContour;
begin
 if self.FEs=es then EXIT;
 TRY
  if self.FEs<>nil then
   self.FlushChanged(nil,TRUE);
 self.FEs:= es;
 if Es=nil then begin
  self.FEditor.ClearALL;        
  EXIT;
 end;

 _temp:= TSpContour.Create;
 TSpConvert.Convert(es,_temp);
 self.FEditor.ReadData(_temp);
 _temp.Free;

 EXCEPT
 self.FEs:= es;
 if Es=nil then begin
  self.FEditor.ClearALL;
  EXIT;
 end;

 _temp:= TSpContour.Create;
 TSpConvert.Convert(es,_temp);
 self.FEditor.ReadData(_temp);
 _temp.Free;
  //TODO:ERROR{$ifdef DEBUG}raise Exception.Create('EntitySpatial flush changes') at ReturnAddress;{$endif}
 END;


end;

procedure TFrameEntity_Spatial._doSave(Sender: TObject);
var
 _log: TStringBuilder;
 _dlog: TformLog;
begin
 _log:= TStringBuilder.Create;

 if self.FlushChanged(_log,false) then begin
  _log.Free;
  EXIT;
 end;

 _dlog:= TformLog.Create(nil);
 _dlog.TextLog:= _log.ToString;
 _dlog.ShowModal;
 _dlog.Free;

 _log.Free;
end;

end.
