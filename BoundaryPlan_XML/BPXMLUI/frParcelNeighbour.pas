unit frParcelNeighbour;

{
�������� � ������ � ���������������� �������� �������
}
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ActnList, ButtonGroup, ExtCtrls,

  STD_MP, ImgList, System.Actions,frOwnerNeighbours,XMLIntf;

type
  TFrameParcelNeighbour = class(TFrame)
    ActionList: TActionList;
    Action_Contrours_AddNew: TAction;
    Action_Contours_DeleteCurrent: TAction;
    Action_Contours_SetCurrent: TAction;


  private
   { Private declarations }
   FXmlParcelNeighbour            : IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour;
   FOwnerNeighbourr_frames        : TFrameOwnerNeighbours;

  protected
   procedure InitializationUI;
   procedure SetContoursInvariable(const aContours: IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour);
  public
   { Public declarations }
   constructor Create(Owner: TComponent); override;

   function IsAssignedContours: Boolean;

   property OwnerNeighbours: TFrameOwnerNeighbours read FOwnerNeighbourr_frames;
   property XmlOwnerNeighbours: IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour read FXmlParcelNeighbour write SetContoursInvariable;

  end;

implementation



{$R *.dfm}


constructor TFrameParcelNeighbour.Create(Owner: TComponent);
begin
 inherited;
 self.FOwnerNeighbourr_frames:= TFrameOwnerNeighbours.Create(self);
 self.FOwnerNeighbourr_frames.Parent:= self;
 self.FOwnerNeighbourr_frames.Align:= alClient;
end;


procedure TFrameParcelNeighbour.InitializationUI;
begin
 if self.IsAssignedContours then
  FOwnerNeighbourr_frames.XmlOwnerNeighbours := FXmlParcelNeighbour.OwnerNeighbours
 else
  FOwnerNeighbourr_frames.XmlOwnerNeighbours := nil;
end;


function TFrameParcelNeighbour.IsAssignedContours: Boolean;
begin
 Result:= self.FXmlParcelNeighbour<>nil;
end;

procedure TFrameParcelNeighbour.SetContoursInvariable(
  const aContours: IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour);
begin
 if self.FXmlParcelNeighbour=aContours then EXIT;
 begin
     self.FXmlParcelNeighbour := aContours;
     self.InitializationUI;
 end;
end;


end.

