unit ProxyCfg;

interface

uses
  WinAPI.Windows,
  system.Classes, System.SysUtils,
  System.JSON;

type

TMgisCfgProxyServer = class
  private
    FJSONWProxy:TJSONObject;

    function getPassword: string;
    procedure setPassword(const aValue: string);
    function getPort: integer;
    procedure setPort(const aValue: integer);
    function getIP: string;
    procedure setIP(const aValue: string);
    function getLogin:string;
    procedure setLogin(const aValue:string);
    function getName: string;
    procedure SetName(const Value: string);

  public
    constructor Create(const aM�del:TJSONObject);
    function GetProxy:string;

    property Name:string read getName write SetName;
    property IP: string read getIP write setIP;
    property Port: integer read getPort write setPort;

    property Login: string read getLogin write setLogin;
    property Password: string read getPassword write setPassword;
end;


implementation

{ TmsConnectConfig }

constructor TMgisCfgProxyServer.Create(const aM�del:TJSONObject);
begin
 Self.FJSONWProxy:= aM�del;
end;

function TMgisCfgProxyServer.getPassword: string;
var
 _jp: TJSONPair;
begin
 _jp:= self.FJSONWProxy.Get('Password');
 if Assigned(_jp) then  result:= _jp.JsonValue.Value;
end;

function TMgisCfgProxyServer.getPort: integer;
var
 _jp: TJSONPair;
begin
 _jp:= self.FJSONWProxy.Get('Port');
 if Assigned(_jp) then  result:= StrToInt(_jp.JsonValue.Value);
end;

function TMgisCfgProxyServer.GetProxy: string;
begin
  self.FJSONWProxy.ToString;
end;

function TMgisCfgProxyServer.getIP: string;
var
 _jp: TJSONPair;
begin
 _jp:= self.FJSONWProxy.Get('IP');
 if Assigned(_jp) then  result:= _jp.JsonValue.Value;
end;

function TMgisCfgProxyServer.getLogin: string;
var
 _jp: TJSONPair;
begin
 _jp:= self.FJSONWProxy.Get('Login');
 if Assigned(_jp) then  result:= _jp.JsonValue.Value;
end;

function TMgisCfgProxyServer.getName: string;
var
  _jp:TJSONPair;
begin
 _jp:= self.FJSONWProxy.Get('Name');
 if Assigned(_jp) then  result:= _jp.JsonValue.Value;
end;

procedure TMgisCfgProxyServer.setPassword(const aValue: string);
begin
  self.FJSONWProxy.RemovePair('Password');
  self.FJSONWProxy.AddPair('Password',aValue);
end;

procedure TMgisCfgProxyServer.setPort(const aValue: integer);
begin
  self.FJSONWProxy.RemovePair('Port');
  self.FJSONWProxy.AddPair('Port',IntToStr(aValue));
end;

procedure TMgisCfgProxyServer.setIP(const aValue: string);
begin
  self.FJSONWProxy.RemovePair('IP');
  self.FJSONWProxy.AddPair('IP',aValue);
end;

procedure TMgisCfgProxyServer.setLogin(const aValue: string);
begin
  self.FJSONWProxy.RemovePair('Login');
  self.FJSONWProxy.AddPair('Login',aValue);
end;

procedure TMgisCfgProxyServer.SetName(const Value: string);
begin
  self.FJSONWProxy.RemovePair('Name');
  self.FJSONWProxy.AddPair('Name',Value);
end;

end.
