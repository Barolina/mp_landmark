object FormBPXML_FormParcels: TFormBPXML_FormParcels
  Left = 0
  Top = 0
  ParentCustomHint = False
  BorderStyle = bsSingle
  Caption = '(XML) '#1054#1073#1088#1072#1079#1086#1074#1072#1085#1080#1077' '#1047#1059' '#1084#1077#1090#1086#1076#1086#1084' '
  ClientHeight = 489
  ClientWidth = 976
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  PixelsPerInch = 96
  TextHeight = 13
  object lblFormParcels: TLabel
    Left = 544
    Top = -16
    Width = 83
    Height = 13
    Caption = #1054#1073#1088#1072#1079#1086#1074#1072#1085#1080#1077' '#1047#1059
    Visible = False
  end
  object jvPageControl_FP: TJvgPageControl
    Left = 0
    Top = 0
    Width = 976
    Height = 489
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    ActivePage = ts_SpecifyRelatedParcel
    Align = alClient
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    ParentShowHint = False
    ShowHint = False
    Style = tsFlatButtons
    TabOrder = 0
    TabStop = False
    StyleElements = []
    TabStyle.Borders = []
    TabStyle.BevelInner = bvNone
    TabStyle.BevelOuter = bvNone
    TabStyle.Bold = False
    TabStyle.BackgrColor = clInactiveBorder
    TabStyle.Font.Charset = RUSSIAN_CHARSET
    TabStyle.Font.Color = clBlack
    TabStyle.Font.Height = -12
    TabStyle.Font.Name = 'Arial'
    TabStyle.Font.Style = [fsBold]
    TabStyle.CaptionHAlign = fhaCenter
    TabStyle.Gradient.ToColor = clSilver
    TabStyle.Gradient.Active = True
    TabStyle.Gradient.Orientation = fgdHorizontal
    TabSelectedStyle.Borders = [fsdLeft, fsdTop, fsdRight, fsdBottom]
    TabSelectedStyle.BevelInner = bvNone
    TabSelectedStyle.BevelOuter = bvNone
    TabSelectedStyle.Bold = False
    TabSelectedStyle.BackgrColor = 15921123
    TabSelectedStyle.Font.Charset = ANSI_CHARSET
    TabSelectedStyle.Font.Color = clBlack
    TabSelectedStyle.Font.Height = -12
    TabSelectedStyle.Font.Name = 'Arial'
    TabSelectedStyle.Font.Style = [fsBold]
    TabSelectedStyle.CaptionHAlign = fhaCenter
    TabSelectedStyle.Gradient.FromColor = 15921123
    TabSelectedStyle.Gradient.ToColor = clSkyBlue
    TabSelectedStyle.Gradient.Active = True
    TabSelectedStyle.Gradient.Orientation = fgdRightBias
    Wallpaper.FillCaptions = False
    Wallpaper.Tile = False
    Wallpaper.IncludeBevels = False
    LookLikeButtons = True
    Options = []
    object ts_NewParcels: TTabSheet
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -9
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ImageIndex = 1
      ParentFont = False
      TabVisible = False
      Touch.ParentTabletOptions = False
      Touch.TabletOptions = []
      object Panel_ControlNewParcel: TPanel
        Left = 0
        Top = 0
        Width = 968
        Height = 48
        Align = alTop
        Anchors = []
        BevelEdges = [beLeft, beRight]
        BevelKind = bkFlat
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object Label2: TLabel
          Left = 12
          Top = 4
          Width = 141
          Height = 13
          Caption = #1057#1087#1080#1089#1086#1082' '#1086#1073#1088#1072#1079#1091#1077#1084#1099#1093' '#1047#1059
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lblCadastralBlock: TLabel
          Left = 365
          Top = 4
          Width = 155
          Height = 13
          Caption = #1050#1074#1072#1088#1090#1072#1083' '#1084#1077#1089#1090#1086#1087#1086#1083#1086#1078#1077#1085#1080#1103
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object LabelName: TLabel
          Left = 547
          Top = 4
          Width = 148
          Height = 13
          Caption = #1058#1080#1087' '#1079#1077#1084#1077#1083#1100#1085#1086#1075#1086' '#1091#1095#1072#1089#1090#1082#1072
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object NewParcels_ComboBox: TComboBox
          Left = 12
          Top = 20
          Width = 200
          Height = 22
          BevelKind = bkFlat
          Style = csOwnerDrawFixed
          Color = 15921123
          TabOrder = 0
          TextHint = #1054#1073#1086#1079#1085#1072#1095#1077#1085#1080#1077' '#1047#1059
          OnChange = Action_NewParcels_SetCurrentExecute
        end
        object ButtonGroup1: TButtonGroup
          Left = 211
          Top = 19
          Width = 128
          Height = 26
          BevelInner = bvNone
          BevelOuter = bvNone
          BorderStyle = bsNone
          ButtonHeight = 25
          ButtonWidth = 25
          ButtonOptions = []
          Images = formContainer.ilCommands
          Items = <
            item
              Action = ActionXML_NewParcel_SetCurrentBack
              Hint = #13#10#1055#1088#1077#1076#1099#1076#1091#1097#1080#1081' '#1086#1073#1088#1072#1079#1091#1077#1084#1099#1081' '#1047#1059#13#10
              ImageIndex = 7
            end
            item
              Action = ActionXML_NewParcel_SetCurrentNext
              Hint = #13#10#1057#1083#1077#1076#1091#1102#1097#1080#1081' '#1086#1073#1088#1072#1079#1091#1077#1084#1099#1081' '#1047#1059#13#10
              ImageIndex = 8
            end
            item
              Action = ActionXML_NewParcel_AddNew
              Hint = #13#10#1044#1086#1073#1072#1074#1080#1090#1100' '#1085#1086#1074#1099#1081'  '#1086#1073#1088#1072#1079#1091#1077#1084#1099#1081' '#1047#1059#13#10
            end
            item
              Action = ActionXML_NewParcel_DelCurrent
              Hint = #13#10#1059#1076#1072#1083#1080#1090#1100' '#1074#1099#1073#1088#1072#1085#1085#1099#1081' '#1086#1073#1088#1072#1079#1091#1077#1084#1099#1081' '#1047#1059#13#10
            end
            item
              Caption = #1048#1079#1084#1077#1085#1080#1090#1100' '#1086#1073#1086#1079#1085#1072#1095#1077#1085#1080#1077
              Hint = #13#10#1048#1079#1084#1077#1085#1080#1090#1100'  '#1086#1073#1086#1079#1085#1072#1095#1077#1085#1080#1077#13#10
              ImageIndex = 9
              OnClick = ButtonGroup1Items5Click
            end>
          ShowHint = True
          TabOrder = 1
        end
        object medtFormParcel_CadastralBlock: TMaskEdit
          Left = 362
          Top = 21
          Width = 178
          Height = 21
          BevelKind = bkFlat
          BorderStyle = bsNone
          Color = 15921123
          TabOrder = 2
          Text = ''
          TextHint = #1050#1074#1072#1088#1090#1072#1083' '#1084#1077#1089#1090#1086#1087#1086#1083#1086#1078#1077#1085#1080#1103
          OnExit = medtFormParcel_CadastralBlockExit
          OnKeyUp = medtFormParcel_CadastralBlockKeyUp
        end
        object cbbFormParcel_Type: TComboBox
          Left = 546
          Top = 20
          Width = 196
          Height = 22
          BevelKind = bkFlat
          Style = csOwnerDrawFixed
          Color = 15921123
          TabOrder = 3
          TextHint = #1058#1080#1087' '#1079#1077#1084#1077#1083#1100#1085#1086#1075#1086' '#1091#1095#1072#1089#1090#1082#1072
          OnChange = cbbFormParcel_TypeChange
          Items.Strings = (
            #1047#1077#1084#1083#1077#1087#1086#1083#1100#1079#1086#1074#1072#1085#1080#1077
            #1052#1085#1086#1075#1086#1082#1086#1085#1090#1091#1088#1085#1099#1081' '#1091#1095#1072#1089#1090#1086#1082)
        end
      end
    end
    object ts_ChangeParcels: TTabSheet
      Hint = 
        '|'#1042' '#1089#1083#1091#1095#1072#1077', '#1077#1089#1083#1080' '#1084#1077#1078#1077#1074#1086#1081' '#1087#1083#1072#1085' '#1086#1092#1086#1088#1084#1083#1103#1077#1090#1089#1103' '#1074' '#1088#1077#1079#1091#1083#1100#1090#1072#1090#1077#13#10' '#1082#1072#1076#1072#1089#1090#1088#1086 +
        #1074#1099#1093' '#1088#1072#1073#1086#1090' '#1087#1086' '#1086#1073#1088#1072#1079#1086#1074#1072#1085#1080#1102' '#1079#1077#1084#1077#1083#1100#1085#1099#1093' '#1091#1095#1072#1089#1090#1082#1086#1074' '#1080#1079' '#1088#1072#1085#1077#1077#13#10' '#1091#1095#1090#1077#1085#1085#1086#1075#1086 +
        ' '#1077#1076#1080#1085#1086#1075#1086' '#1079#1077#1084#1083#1077#1087#1086#1083#1100#1079#1086#1074#1072#1085#1080#1103' '#1080' '#1087#1088#1080' '#1101#1090#1086#1084' '#1085#1086#1074#1099#1077' '#1079#1077#1084#1077#1083#1100#1085#1099#1077#13#10' '#1091#1095#1072#1089#1090#1082#1080' '#1086 +
        #1073#1088#1072#1079#1086#1074#1072#1085#1099' '#1074' '#1088#1077#1079#1091#1083#1100#1090#1072#1090#1077' '#1088#1072#1079#1076#1077#1083#1072' '#1079#1077#1084#1077#1083#1100#1085#1099#1093' '#1091#1095#1072#1089#1090#1082#1086#1074','#13#10#1074#1093#1086#1076#1103#1097#1080#1093' '#1074' '#1089 +
        #1086#1089#1090#1072#1074' '#1077#1076#1080#1085#1086#1075#1086' '#1079#1077#1084#1083#1077#1087#1086#1083#1100#1079#1086#1074#1072#1085#1080#1103', '#1090#1072#1082#1080#1077' '#1074#1093#1086#1076#1103#1097#1080#1077' '#1074#13#10#1089#1086#1089#1090#1072#1074' '#1077#1076#1080#1085#1086#1075#1086 +
        ' '#1079#1077#1084#1083#1077#1087#1086#1083#1100#1079#1086#1074#1072#1085#1080#1103' '#1079#1077#1084#1077#1083#1100#1085#1099#1077' '#1091#1095#1072#1089#1090#1082#1080#13#10' '#1089#1086#1093#1088#1072#1085#1103#1102#1090#1089#1103' '#1074' '#1080#1079#1084#1077#1085#1077#1085#1085#1099#1093' '#1075 +
        #1088#1072#1085#1080#1094#1072#1093' '#13#10'('#1080#1079#1084#1077#1085#1077#1085#1085#1099#1077' '#1086#1073#1086#1089#1086#1073#1083#1077#1085#1085#1099#1077' '#1080#1083#1080' '#1091#1089#1083#1086#1074#1085#1099#1077' '#1079#1077#1084#1077#1083#1100#1085#1099#1077' '#1091#1095#1072#1089#1090#1082 +
        #1080').'#13#10' '#1042' '#1076#1072#1085#1085#1086#1084' '#1089#1083#1091#1095#1072#1077' '#1089#1074#1077#1076#1077#1085#1080#1103' '#1086' '#1090#1072#1082#1080#1093' '#1080#1079#1084#1077#1085#1077#1085#1085#1099#1093' '#1086#1073#1086#1089#1086#1073#1083#1077#1085#1085#1099#1093#13#10 +
        ' '#1080#1083#1080' '#1091#1089#1083#1086#1074#1085#1099#1093' '#1079#1077#1084#1077#1083#1100#1085#1099#1093' '#1091#1095#1072#1089#1090#1082#1072#1093' '#1074#1082#1083#1102#1095#1072#1102#1090#1089#1103' '#1074' '#1088#1077#1082#1074#1080#1079#1080#1090' '#171'2'#187' '#1080' '#171'3'#187 +
        ' '#1088#1072#1079#1076#1077#1083#1072#13#10#171#1057#1074#1077#1076#1077#1085#1080#1103' '#1086#1073' '#1080#1079#1084#1077#1085#1077#1085#1085#1099#1093' '#1079#1077#1084#1077#1083#1100#1085#1099#1093' '#1091#1095#1072#1089#1090#1082#1072#1093' '#1080' '#1080#1093' '#1095#1072#1089#1090#1103#1093 +
        #187'. '#13#10#1055#1088#1080' '#1101#1090#1086#1084' '#1074#1084#1077#1089#1090#1086' '#1091#1095#1077#1090#1085#1086#1075#1086' '#1085#1086#1084#1077#1088#1072' '#1095#1072#1089#1090#1080' '#1091#1082#1072#1079#1099#1074#1072#1077#1090#1089#1103' '#1082#1072#1076#1072#1089#1090#1088#1086#1074 +
        #1099#1081' '#1085#1086#1084#1077#1088#13#10' '#1090#1072#1082#1086#1075#1086' '#1079#1077#1084#1077#1083#1100#1085#1086#1075#1086' '#1091#1095#1072#1089#1090#1082#1072', '#1074' '#1082#1072#1095#1077#1089#1090#1074#1077' '#1093#1072#1088#1072#1082#1090#1077#1088#1080#1089#1090#1080#1082#1080' ' +
        #1095#1072#1089#1090#1080' '#13#10#1091#1082#1072#1079#1099#1074#1072#1077#1090#1089#1103' '#1085#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1079#1077#1084#1077#1083#1100#1085#1086#1075#1086' '#1091#1095#1072#1089#1090#1082#1072' '#13#10'('#171#1086#1073#1086#1089#1086#1073#1083#1077 +
        #1085#1085#1099#1081' '#1079#1077#1084#1077#1083#1100#1085#1099#1081' '#1091#1095#1072#1089#1090#1086#1082#187' '#1080#1083#1080' '#171#1091#1089#1083#1086#1074#1085#1099#1081' '#1079#1077#1084#1077#1083#1100#1085#1099#1081' '#1091#1095#1072#1089#1090#1086#1082#187').'
      Caption = '       '
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabVisible = False
      object pnlControl_ChangeParcel: TPanel
        Left = 0
        Top = 0
        Width = 968
        Height = 41
        Align = alTop
        BevelEdges = [beLeft, beRight]
        BevelKind = bkFlat
        BevelOuter = bvNone
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 0
        object lbl1: TLabel
          Left = 12
          Top = 3
          Width = 144
          Height = 13
          Caption = #1057#1087#1080#1089#1086#1082' '#1080#1079#1084#1077#1085#1103#1077#1084#1099#1093' '#1047#1059
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lbl2: TLabel
          Left = 292
          Top = 3
          Width = 155
          Height = 13
          Caption = #1050#1074#1072#1088#1090#1072#1083' '#1084#1077#1089#1090#1086#1087#1086#1083#1086#1078#1077#1085#1080#1103
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object cbbChangeParcelItems_ComboBox: TComboBox
          Left = 12
          Top = 16
          Width = 200
          Height = 22
          BevelKind = bkFlat
          Style = csOwnerDrawFixed
          Color = 15921123
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          TextHint = #1050#1072#1076#1072#1089#1090#1088#1086#1074#1099#1081' '#1085#1086#1084#1077#1088' '#1047#1059
          OnChange = ActionCP_SetCurrentExecute
        end
        object btnChangeParcels_ButtonGroup: TButtonGroup
          Left = 216
          Top = 15
          Width = 53
          Height = 26
          BevelInner = bvNone
          BevelOuter = bvNone
          BorderStyle = bsNone
          ButtonHeight = 25
          ButtonWidth = 25
          Images = formContainer.ilCommands
          Items = <
            item
              Action = ActionCP_AddNew
              Hint = #13#10#1044#1086#1073#1072#1074#1080#1090#1100' '#1047#1059#13#10
              ImageIndex = 0
            end
            item
              Hint = #13#10#1059#1076#1072#1083#1080#1090#1100' '#1047#1059#13#10
              ImageIndex = 1
              OnClick = ActionCP_DelCurrentExecute
            end>
          ShowHint = True
          TabOrder = 1
        end
        object medtChangeParCadBlock: TMaskEdit
          Left = 289
          Top = 16
          Width = 178
          Height = 21
          Hint = #1050#1074#1072#1088#1090#1072#1083' '#1084#1077#1089#1090#1086#1087#1086#1083#1086#1078#1077#1085#1080#1103
          BevelKind = bkFlat
          BorderStyle = bsNone
          Color = 15921123
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          Text = ''
          TextHint = #1050#1074#1072#1088#1090#1072#1083' '#1084#1077#1089#1090#1086#1087#1086#1083#1086#1078#1077#1085#1080#1103
          OnExit = medtChangeParCadBlockExit
          OnKeyUp = medtFormParcel_CadastralBlockKeyUp
        end
      end
    end
    object ts_SpecifyRelatedParcel: TTabSheet
      ImageIndex = 2
      TabVisible = False
    end
  end
  object ActionManager_FormParcels: TActionManager
    Left = 657
    Top = 91
    StyleName = 'Platform Default'
    object Action_NewParcels_SetCurrent: TAction
      Category = 'NewParcel'
      Caption = #1057#1076#1077#1083#1072#1090#1100' '#1090#1077#1082#1091#1097#1080#1084' '#1074#1099#1073#1088#1072#1085#1085#1099#1081' '#1086#1073#1088#1072#1079#1091#1077#1084#1099#1081' '#1047#1059
      OnExecute = Action_NewParcels_SetCurrentExecute
    end
    object ActionXML_NewParcel_AddNew: TAction
      Category = 'NewParcel'
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1085#1086#1074#1099#1081'  '#1086#1073#1088#1072#1079#1091#1077#1084#1099#1081' '#1047#1059
      Hint = #1044#1086#1073#1072#1074#1080#1090#1100' '#1085#1086#1074#1099#1081'  '#1086#1073#1088#1072#1079#1091#1077#1084#1099#1081' '#1047#1059
      ImageIndex = 0
      OnExecute = ActionXML_NewParcel_AddNewExecute
    end
    object ActionXML_NewParcel_DelCurrent: TAction
      Category = 'NewParcel'
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1074#1099#1073#1088#1072#1085#1085#1099#1081' '#1086#1073#1088#1072#1079#1091#1077#1084#1099#1081' '#1047#1059
      Hint = #1059#1076#1072#1083#1080#1090#1100' '#1074#1099#1073#1088#1072#1085#1085#1099#1081' '#1086#1073#1088#1072#1079#1091#1077#1084#1099#1081' '#1047#1059
      ImageIndex = 1
      OnExecute = ActionXML_NewParcel_DelCurrentExecute
    end
    object ActionCP_AddNew: TAction
      Category = 'ChangeParcel'
      Caption = 'ActionCP_AddNew'
      OnExecute = ActionCP_AddNewExecute
    end
    object ActionCP_DelCurrent: TAction
      Category = 'ChangeParcel'
      Caption = 'ActionCP_DelCurrent'
      OnExecute = ActionCP_DelCurrentExecute
    end
    object ActionCP_SetCurrent: TAction
      Category = 'ChangeParcel'
      Caption = 'ActionCP_SetCurrent'
      OnExecute = ActionCP_SetCurrentExecute
    end
    object ActionXML_NewParcel_SetCurrentNext: TAction
      Category = 'NewParcel'
      Caption = #1057#1083#1077#1076#1091#1102#1097#1080#1081' '#1086#1073#1088#1072#1079#1091#1077#1084#1099#1081' '#1047#1059
      Hint = #1057#1083#1077#1076#1091#1102#1097#1080#1081' '#1086#1073#1088#1072#1079#1091#1077#1084#1099#1081' '#1047#1059
      ImageIndex = 3
      OnExecute = ActionXML_NewParcel_SetCurrentNextExecute
    end
    object ActionXML_NewParcel_SetCurrentBack: TAction
      Category = 'NewParcel'
      Caption = #1055#1088#1077#1076#1099#1076#1091#1097#1080#1081' '#1086#1073#1088#1072#1079#1091#1077#1084#1099#1081' '#1047#1059
      Hint = #1055#1088#1077#1076#1099#1076#1091#1097#1080#1081' '#1086#1073#1088#1072#1079#1091#1077#1084#1099#1081' '#1047#1059
      ImageIndex = 2
      OnExecute = ActionXML_NewParcel_SetCurrentBackExecute
    end
    object ActionXML_NewParcel_GroupEdit: TAction
      Category = 'NewParcel'
      Caption = #1059#1089#1090#1072#1085#1086#1074#1082#1072' '#1075#1088#1091#1087#1086#1074#1099#1093' '#1079#1085#1072#1095#1077#1085#1080#1081
      ImageIndex = 4
      OnExecute = ActionXML_NewParcel_GroupEditExecute
    end
    object actEditDeifinition: TAction
      Category = 'NewParcel'
      Caption = #1048#1079#1084#1077#1085#1080#1090#1100' '#1086#1073#1086#1079#1085#1072#1095#1077#1085#1080#1077
      Hint = #1048#1079#1084#1077#1085#1080#1090#1100' '#1086#1073#1086#1079#1085#1072#1095#1077#1085#1080#1077
      OnExecute = ButtonGroup1Items5Click
    end
  end
end
