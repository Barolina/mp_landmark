unit frDataDocuments;

{
   ������ ����� Documemnts
}
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ActnList, ButtonGroup, ExtCtrls,

  STD_MP, ImgList,
  System.Actions, frDocument,XMLIntf,fmContainer,FrPreloader,frAttributesSTD_MP,
  RegularExpressions,unRussLang, System.IOUtils, Vcl.ComCtrls, Vcl.ToolWin,
  JvExComCtrls, JvToolBar,MsXmlApi, JvComponentBase, JvBalloonHint, JvgHint,
  JvHint;

type
  TFrameDataDocuments = class(TFrame)
    PanelNodalPointSchemes_List: TPanel;
    ActionList: TActionList;
    Documents_List_ListBox: TListBox;
    Action_Contrours_AddNew: TAction;
    Action_NodalPointSchemes_DeleteCurrent: TAction;
    Action_NodalPointSchemes_SetCurrent: TAction;
    actImportAll: TAction;
    dlgFileOpenDialog: TOpenDialog;
    jvtlbrElemeny: TJvToolBar;
    btnAdd: TToolButton;
    btnDel: TToolButton;
    btnimport: TToolButton;
    spl1: TSplitter;
    actHint: TAction;
    blnhnt1: TBalloonHint;

    procedure Action_Contrours_AddNewExecute(Sender: TObject);
    procedure Action_NodalPointSchemes_DeleteCurrentExecute(Sender: TObject);
    procedure Action_NodalPointSchemes_SetCurrentExecute(Sender: TObject);
    procedure actImportAllExecute(Sender: TObject);
    procedure btnimportMouseEnter(Sender: TObject);
  private
   { Private declarations }
     FDocument_frame: TFrameDocument;


     FXmlInputDataDocuments    : IXMLSTD_MP_Input_Data_Documents;
     FXmlAppendix              : IXMLSTD_MP_Appendix;
     FXmlEncumbrance           : IXMLTEncumbrance_Documents;
     FXmlProvidingPassCadNum   : IXMLTProviding_Pass_CadastralNumbers_Documents;
     FXmlOwnerNeighbour        : IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbour_Documents;
     {
     //����������   �������������� ���������  ��� ����� Documents
      ��� �������� ��� InputDate
     }
     FIsTypeXMLDocument        : integer;
     FXMLNodeCollection        : IXMLNodeCollection;
     FXMlDocNodeColl           : IXMLNodeCollection;

     procedure AppendixListDoSetCurrentItem(indexItem : integer);
     procedure AppendixLIstUpdateDoUpdate;
     procedure AppendixListDoAddItem;
     procedure AppendixListDoDeleteItem(indexItem : integer);

  protected
     procedure InitializationUI;
     procedure SetAppendix(const aAppendix: IXMLSTD_MP_Appendix);
     procedure SetInputDataDocuments(const aInputDataDocuments  : IXMLSTD_MP_Input_Data_Documents);
     procedure SetEncumbranceDocuments(const aEncumbranceDocuments : IXMLTEncumbrance_Documents);
     procedure SetProvidingPassCadNum (const aProvidingPassCadNum : IXMLTProviding_Pass_CadastralNumbers_Documents);
     procedure SetOwnerNeighbour (const aOwnerNeighbour : IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbour_Documents );

     procedure AppendixSetCurrent(index : integer);
  public
    { Public declarations }
     constructor Create(Owner: TComponent); override;
     function IsAssignedAppendix: Boolean;
     property Documents: TFrameDocument read FDocument_frame;

     property XmlAppendix: IXMLSTD_MP_Appendix read FXmlAppendix write SetAppendix;
     property XmlInputDataDocuments : IXMLSTD_MP_Input_Data_Documents read FXmlInputDataDocuments
              write  SetInputDataDocuments;
     property XmlEncumbrance : IXMLTEncumbrance_Documents read FXmlEncumbrance
              write SetEncumbranceDocuments;
     property XmlProvidingPassCadNum : IXMLTProviding_Pass_CadastralNumbers_Documents
              read FXmlProvidingPassCadNum
              write SetProvidingPassCadNum;
     property XmlOwnerNeighbour      : IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbour_Documents
              read FXmlOwnerNeighbour
              write SetOwnerNeighbour;
  end;

implementation
 resourcestring
     rsDefaultCodeDocument = '558221990000';


{$R *.dfm}

{ TFrameNodalPointSchemes }

procedure TFrameDataDocuments.Action_NodalPointSchemes_DeleteCurrentExecute(Sender: TObject);
begin
 if not IsAssignedAppendix then EXIT;
 if self.Documents_List_ListBox.ItemIndex=-1 then EXIT;

 self.AppendixListDoDeleteItem(self.Documents_List_ListBox.ItemIndex);
 self.AppendixSetCurrent(self.Documents_List_ListBox.ItemIndex);
 if not self.FDocument_frame.IsAssignedDocument then
  self.AppendixSetCurrent(0);
end;

procedure TFrameDataDocuments.Action_NodalPointSchemes_SetCurrentExecute(Sender: TObject);
begin
 self.AppendixSetCurrent(self.Documents_List_ListBox.ItemIndex);
end;

procedure TFrameDataDocuments.AppendixListDoAddItem;
var
    itemName : string;
    document : IXMLNode;
begin
   document := FXMLNodeCollection.AddChild('Document');
   itemName := inttostr(FXMLNodeCollection.Count);
   Documents_List_ListBox.AddItem(itemName, pointer(document));
end;

procedure TFrameDataDocuments.AppendixListDoDeleteItem(indexItem: integer);
var
   document : IXMLNode;
begin
  document := IXMLNode(pointer(self.Documents_List_ListBox.Items.Objects[indexItem]));
  if FDocument_frame.XMlDOcument = document then
      AppendixListDoSetCurrentItem(indexItem-1);
  FDocument_frame.RemoveLoadFiles;    
  Documents_List_ListBox.Items.Delete(indexItem);
  FXMLNodeCollection.Remove(document);  
  document := nil;
end;

procedure TFrameDataDocuments.AppendixListDoSetCurrentItem(indexItem: integer);
var
 document_ : IXMLTDocument;
 number : string;
begin
 document_ := (IXMLTDocument(pointer(Documents_List_ListBox.Items.Objects[indexItem])) as IXMLTDocument);
 FDocument_frame.XMlDOcument := document_;
 try
   number := document_.Number;
   self.Documents_List_ListBox.Items[indexItem] := IntToStr(indexItem+1)  + ' ('+number+')';
 except
   number := '';
 end;
end;

procedure TFrameDataDocuments.AppendixLIstUpdateDoUpdate;
var
  i,index : integer;
  document : IXMLNode;
  {$region 'GetNameDoc'}
  function GetNameDoc () :string;
  begin
     Result := '';
     try
        Result := MsXml_ReadChildNodeValue(document,'Name')+' '+MsXml_ReadChildNodeValue(document,'Number');
     except
     end;
  end;
  {$ENDREGION}
begin
   index := Documents_List_ListBox.ItemIndex;
   Documents_List_ListBox.Clear;
   for i := 0 to FXMLNodeCollection.Count-1 do
   begin
      document := FXMLNodeCollection.Nodes[i];
      Documents_List_ListBox.AddItem(IntToStr(i+1)+' ('+GetNameDoc+')',pointer(document));//inttostr(i+1)
      AppendixSetCurrent(i);
   end;

   if not FDocument_frame.IsAssignedDocument then    self.AppendixSetCurrent(0);
end;

procedure TFrameDataDocuments.AppendixSetCurrent(index: integer);
begin
 if (index>-1) and (index<self.Documents_List_ListBox.Count) then begin
  self.AppendixListDoSetCurrentItem(index);
  self.Documents_List_ListBox.ItemIndex:= index;
  self.FDocument_frame.Visible:= True;
 end else begin
  self.FDocument_frame.Visible:= false;
  self.Documents_List_ListBox.ItemIndex:= -1;
  self.FDocument_frame.XMlDOcument:= nil;
 end;

end;

procedure TFrameDataDocuments.btnimportMouseEnter(Sender: TObject);
begin
end;

///
///  ������ ���������� ��������
///  � ������������ ������� ������ ���������� �����
///  ������������ ����� ������ ����� ������������ ������
///  "������������ ���������";"�����";"���� ��������� � ���������".pdf
///
procedure TFrameDataDocuments.actImportAllExecute(Sender: TObject);
var namePDf  : string;
    i : Integer;
    nameDoc, NumberDoc, DataDoc : string;
    fl_LoadDoc : boolean;
    {$region 'GetElementName'}
    function GetElementName (str : string)  : string; //������ ����� �� �������
    var
      Matchs: TMatchCollection;
      Match: TMatch;
      _date : string;
      frmt : TFormatSettings;
    begin
      try
         fl_LoadDoc := true;
         nameDoc := ''; NumberDoc := ''; DataDoc := '';
         str := str.Replace(' ;',';').Replace('; ',';');
         Matchs := TRegEx.Matches(str, regNameDocuments);
         if Matchs.Count = 0  then  Exit;
         Match := Matchs.Item[0];
         nameDoc := Match.Groups.Item[1].Value;
         NumberDoc := Match.Groups.Item[2].Value;
         frmt.DateSeparator := '-';
         frmt.ShortDateFormat := 'dd-mm-yyyy';
         _date  := Match.Groups.Item[3].Value;
         _date := _date.Replace('.','-').Replace(' ','');
         DataDoc :=  FormatDateTime('yyyy-mm-dd',StrToDate(_date));
      except
        fl_LoadDoc := False;
      end;
    end;
    {$ENDREGION}
begin
 ///�������� ��������� ���������� �  �������������� �������
 if not self.IsAssignedAppendix then EXIT;
 if not dlgFileOpenDialog.Execute then Exit;
 try
   TFrLoader.Inctance.RunAnimation := True;
   fl_LoadDoc := True;
   for i := 0 to dlgFileOpenDialog.Files.Count -1 do
   begin
       Action_Contrours_AddNewExecute(Sender);
       FDocument_frame.AppliedFile_frame.ImportDocument(dlgFileOpenDialog.Files[i]);
       FDocument_frame.XMlDOcument.Code_Document := rsDefaultCodeDocument;
       GetElementName(TPath.GetFileNameWithoutExtension(dlgFileOpenDialog.Files[i]));
       if DataDoc <> '' then  FDocument_frame.XMlDOcument.Date := DataDoc;
       if NumberDoc <> '' then
       begin
         FDocument_frame.XMlDOcument.Number := NumberDoc;
         Documents_List_ListBox.Items[Documents_List_ListBox.ItemIndex] := Documents_List_ListBox.Items[Documents_List_ListBox.ItemIndex]+' ('+NumberDoc+')';
       end;
   end;
   self.AppendixSetCurrent(self.Documents_List_ListBox.Count-1);
   if not fl_LoadDoc  then raise Exception.Create('�� ��� �������� ������ ������� ����������!');
finally
  TFrLoader.Inctance.RunAnimation := false;
end;
end;

procedure TFrameDataDocuments.Action_Contrours_AddNewExecute(Sender: TObject);
begin
 if not self.IsAssignedAppendix then EXIT;
 self.AppendixListDoAddItem;
 self.AppendixSetCurrent(self.Documents_List_ListBox.Count-1);
end;


constructor TFrameDataDocuments.Create(Owner: TComponent);
begin
 inherited;
 self.FDocument_frame:= TFrameDocument.Create(self);
 self.FDocument_frame.Parent:= self;
 self.FDocument_frame.Align:= alClient;
 self.FDocument_frame.Visible:= False;
end;

procedure TFrameDataDocuments.InitializationUI;
begin
 self.Documents_List_ListBox.Clear;
 if self.IsAssignedAppendix then begin
  self.AppendixLIstUpdateDoUpdate;
  self.AppendixSetCurrent(0);
 end
end;

function TFrameDataDocuments.IsAssignedAppendix: Boolean;
begin
 Result:= self.FXMLNodeCollection<>nil;
end;

procedure TFrameDataDocuments.SetAppendix(const aAppendix: IXMLSTD_MP_Appendix);
begin
 if self.FXMLNodeCollection=aAppendix then EXIT;
 FIsTypeXMLDocument := 0;
 self.AppendixSetCurrent(-1);
 self.FXMLNodeCollection:= aAppendix;
 self.InitializationUI;
end;

procedure TFrameDataDocuments.SetEncumbranceDocuments(
  const aEncumbranceDocuments: IXMLTEncumbrance_Documents);
begin
  if self.FXmlEncumbrance = aEncumbranceDocuments then EXIT;
  FIsTypeXMLDocument := 0;
  self.AppendixSetCurrent(-1);
  self.FXMLNodeCollection := aEncumbranceDocuments;
  self.InitializationUI;
end;

procedure TFrameDataDocuments.SetInputDataDocuments(
  const aInputDataDocuments: IXMLSTD_MP_Input_Data_Documents);
begin
 if self.FXmlInputDataDocuments=aInputDataDocuments then EXIT;
 FIsTypeXMLDocument := 1;
 self.AppendixSetCurrent(-1);
 self.FXMLNodeCollection:= aInputDataDocuments;
 self.InitializationUI;
end;

procedure TFrameDataDocuments.SetOwnerNeighbour(
  const aOwnerNeighbour: IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbour_Documents);
begin
 if FXmlOwnerNeighbour = aOwnerNeighbour then Exit;
 FIsTypeXMLDocument := 0;
 self.AppendixSetCurrent(-1);
 FXMLNodeCollection := aOwnerNeighbour;
 self.InitializationUI;
end;

procedure TFrameDataDocuments.SetProvidingPassCadNum(
  const aProvidingPassCadNum: IXMLTProviding_Pass_CadastralNumbers_Documents);
begin
 if FXmlProvidingPassCadNum = aProvidingPassCadNum then Exit;
 FIsTypeXMLDocument := 0;
 self.AppendixSetCurrent(-1);
 FXMLNodeCollection := aProvidingPassCadNum;
 self.InitializationUI;
end;

end.
