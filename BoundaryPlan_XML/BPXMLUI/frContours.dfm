object FrameContours: TFrameContours
  Left = 0
  Top = 0
  Width = 660
  Height = 276
  TabOrder = 0
  object jvgrphdr1: TJvGroupHeader
    Left = 0
    Top = 58
    Width = 660
    Height = 17
    Align = alTop
    Alignment = taCenter
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 12615680
    Font.Height = -12
    Font.Name = 'Arial'
    Font.Pitch = fpFixed
    Font.Style = []
    Font.Quality = fqClearType
    ParentFont = False
    BevelOptions.Height = 1
    BevelOptions.Style = bsShape
    Transparent = True
    ExplicitLeft = -163
    ExplicitTop = 60
    ExplicitWidth = 823
  end
  object pnlControls: TPanel
    Left = 0
    Top = 30
    Width = 660
    Height = 28
    Align = alTop
    BevelEdges = [beBottom]
    BevelOuter = bvNone
    Caption = 'pnlControls'
    ShowCaption = False
    TabOrder = 1
    object cbbContoursType: TComboBox
      Left = 408
      Top = 0
      Width = 226
      Height = 22
      Hint = #1040#1082#1090#1080#1074#1085#1099#1081' '#1090#1080#1087' '#1082#1086#1085#1090#1091#1088#1072
      Align = alLeft
      BevelKind = bkFlat
      Style = csOwnerDrawFixed
      Color = 15921123
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      TextHint = #1040#1082#1090#1080#1074#1085#1099#1081' '#1090#1080#1087' '#1082#1086#1085#1090#1091#1088#1072
      Visible = False
      StyleElements = []
      OnChange = cbbContoursTypeChange
      Items.Strings = (
        #1054#1073#1088#1072#1079#1091#1077#1084#1099#1077' '#1082#1086#1085#1090#1091#1088#1072' '#1084#1085#1086#1075#1086#1082#1086#1085#1090#1091#1088#1085#1086#1075#1086' '#1091#1095#1072#1089#1090#1082#1072
        #1059#1090#1086#1095#1085#1103#1077#1084#1099#1077' '#1089#1091#1097#1077#1089#1090#1074#1091#1102#1097#1080#1077' '#1082#1086#1085#1090#1091#1088#1072' '#1084#1085#1086#1075#1086#1082#1086#1085#1090#1091#1088#1085#1086#1075#1086' '#1091#1095#1072#1089#1090#1082#1072
        
          #1048#1089#1082#1083#1102#1095#1077#1085#1080#1077' '#1075#1088#1072#1085#1080#1094#1099' '#1082#1086#1085#1090#1091#1088#1072' '#1084#1085#1086#1075#1086#1082#1086#1085#1090#1091#1088#1085#1086#1075#1086' '#1091#1095#1072#1089#1090#1082#1072' ('#1080#1089#1082#1083#1102#1095#1077#1085#1080#1077' '#1082 +
          #1086#1085#1090#1091#1088#1072')')
    end
    object btnEdCurContour: TButtonedEdit
      Left = 0
      Top = 0
      Width = 167
      Height = 28
      Align = alLeft
      Alignment = taCenter
      BevelEdges = [beLeft, beTop, beRight]
      BevelInner = bvNone
      BevelKind = bkFlat
      BorderStyle = bsNone
      Color = 15921123
      Images = formContainer.ilCommands
      RightButton.ImageIndex = 6
      RightButton.Visible = True
      TabOrder = 0
      OnExit = btnEdCurContourExit
      OnKeyDown = btnEdCurContourKeyDown
      OnRightButtonClick = btnEdCurContourRightButtonClick
    end
    object jvtlbr2: TJvToolBar
      Left = 167
      Top = 0
      Width = 241
      Height = 28
      ParentCustomHint = False
      Align = alLeft
      ButtonHeight = 24
      ButtonWidth = 24
      Caption = 'jvtlbr1'
      Color = 15711412
      DoubleBuffered = False
      DrawingStyle = dsGradient
      EdgeInner = esNone
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      GradientEndColor = 15197907
      GradientStartColor = clWhite
      Images = formContainer.ilCommands
      List = True
      GradientDirection = gdHorizontal
      ParentColor = False
      ParentDoubleBuffered = False
      ParentFont = False
      ParentShowHint = False
      AllowTextButtons = True
      ShowHint = True
      TabOrder = 1
      HintColor = 13551291
      object btnContour_prev1: TToolButton
        Left = 0
        Top = 0
        ParentCustomHint = False
        Action = actContour_prev
        Marked = True
        ParentShowHint = False
        ShowHint = True
      end
      object btnContour_next1: TToolButton
        Left = 24
        Top = 0
        ParentCustomHint = False
        Action = actContour_next
        ParentShowHint = False
        ShowHint = True
      end
      object btnContrours_AddNew1: TToolButton
        Left = 48
        Top = 0
        Hint = #13#10#1044#1086#1073#1072#1074#1080#1090#1100' '#1082#1086#1085#1090#1091#1088#13#10
        ParentCustomHint = False
        Action = actContrours_AddNew
        ParentShowHint = False
        ShowHint = True
      end
      object btnContours_DeleteCurrent1: TToolButton
        Left = 72
        Top = 0
        Hint = #13#10#1059#1076#1072#1083#1080#1090#1100' '#1074#1099#1073#1088#1072#1085#1085#1099#1081' '#1082#1086#1085#1090#1091#1088#13#10
        ParentCustomHint = False
        Action = actContours_DeleteCurrent
        ParentShowHint = False
        ShowHint = True
      end
      object btnClipBrd_Copy1: TToolButton
        Left = 96
        Top = 0
        ParentCustomHint = False
        Action = actClipBrd_Copy
        ParentShowHint = False
        ShowHint = True
      end
      object btnClipBrd_Paste1: TToolButton
        Left = 120
        Top = 0
        Hint = #13#10#1048#1084#1087#1086#1088#1090' '#1076#1072#1085#1085#1099#1093#13#10
        ParentCustomHint = False
        Action = actClipBrd_Paste
        ParentShowHint = False
        ShowHint = True
      end
      object btnValid1: TToolButton
        Left = 144
        Top = 0
        Hint = #13#10#1055#1088#1086#1074#1077#1088#1082#1072' '#1082#1086#1088#1088#1077#1082#1090#1085#1086#1089#1090#1080#13#10
        ParentCustomHint = False
        Action = actValid
        ParentShowHint = False
        ShowHint = True
      end
      object btn1: TToolButton
        Left = 168
        Top = 0
        Width = 25
        ParentCustomHint = False
        Caption = 'btn1'
        ImageIndex = 5
        ParentShowHint = False
        ShowHint = True
        Style = tbsSeparator
      end
      object btnEntity_Spatial: TToolButton
        Left = 193
        Top = 0
        Hint = #13#10#1050#1086#1086#1088#1076#1080#1085#1072#1090#1085#1086#1077' '#1086#1087#1080#1089#1072#1085#1080#1077#13#10
        ParentCustomHint = False
        Caption = 'btnEntity_Spatial'
        Grouped = True
        ImageIndex = 19
        ParentShowHint = False
        ShowHint = True
        Style = tbsCheck
        OnClick = btnEntity_SpatialClick
      end
      object btnArea: TToolButton
        Left = 217
        Top = 0
        Hint = #13#10#1055#1083#1086#1097#1072#1076#1100'/'#1054#1073#1086#1079#1085#1072#1095#1077#1085#1080#1077#13#10
        ParentCustomHint = False
        Caption = 'btnArea'
        Grouped = True
        ImageIndex = 21
        ParentShowHint = False
        ShowHint = True
        Style = tbsCheck
        OnClick = btnAreaClick
      end
    end
  end
  object Contours_List_ListBox: TListBox
    Left = 0
    Top = 60
    Width = 167
    Height = 209
    Style = lbOwnerDrawVariable
    BevelEdges = [beLeft, beRight, beBottom]
    BevelKind = bkFlat
    BorderStyle = bsNone
    Color = 15921123
    ItemHeight = 13
    TabOrder = 2
    Visible = False
    OnClick = Action_Contours_SetCurrentExecute
    OnKeyDown = Contours_List_ListBoxKeyDown
    OnMouseLeave = Contours_List_ListBoxMouseLeave
  end
  object tlbTypeSubparcels: TToolBar
    Left = 0
    Top = 0
    Width = 660
    Height = 30
    ParentCustomHint = False
    Anchors = []
    ButtonHeight = 23
    ButtonWidth = 206
    DoubleBuffered = False
    DragCursor = crHandPoint
    DrawingStyle = dsGradient
    EdgeInner = esNone
    EdgeOuter = esNone
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = 12615680
    Font.Height = -12
    Font.Name = 'Arial'
    Font.Style = [fsBold, fsUnderline]
    Font.Quality = fqClearType
    GradientEndColor = clSilver
    GradientStartColor = 15395562
    GradientDirection = gdHorizontal
    ParentDoubleBuffered = False
    ParentFont = False
    ParentShowHint = False
    ShowCaptions = True
    ShowHint = False
    TabOrder = 0
    TabStop = True
    Transparent = True
    StyleElements = []
    object btnNewContour: TToolButton
      Left = 0
      Top = 0
      Hint = 
        #13#10#1047#1072#1087#1086#1083#1085#1103#1077#1090#1089#1103' '#1074' '#1086#1090#1085#1086#1096#1077#1085#1080#1080' '#1086#1073#1088#1072#1079#1091#1077#1084#1099#1093' '#1095#1072#1089#1090#1077#1081' '#1047#1059#13#10' ('#1074' '#1090'.'#1095'. '#1084#1085#1086#1075#1086#1082#1086 +
        #1085#1090#1091#1088#1085#1099#1093') '#13#10
      AutoSize = True
      Caption = #1054#1073#1088#1072#1079#1091#1077#1084#1099#1077' '#1050#1052#1047#1059
      Grouped = True
      ImageIndex = 0
      ParentShowHint = False
      ShowHint = False
      Style = tbsCheck
      OnClick = tlbTypeSubparcelsClick
    end
    object btnExistContour: TToolButton
      Tag = 1
      Left = 120
      Top = 0
      Hint = 
        #13#10#1047#1072#1087#1086#1083#1085#1103#1077#1090#1089#1103' '#1074' '#1086#1090#1085#1086#1096#1077#1085#1080#1080' '#1095#1072#1089#1090#1077#1081' '#1047#1059#13#10' ('#1074' '#1090'.'#1095'. '#1084#1085#1086#1075#1086#1082#1086#1085#1090#1091#1088#1085#1099#1093'), '#1082 +
        #1086#1090#1086#1088#1099#1077' '#1089#1086#1093#1088#1072#1085#1103#1102#1090#1089#1103' '#1074#13#10' '#1091#1090#1086#1095#1085#1103#1077#1084#1099#1093' '#1075#1088#1072#1085#1080#1094#1072#1093' '
      AutoSize = True
      Caption = #1059#1090#1086#1095#1085#1103#1077#1084#1099#1077' '#1089#1091#1097#1091#1089#1090#1074#1091#1102#1097#1080#1077'  '#1050#1052#1047#1059
      Grouped = True
      ImageIndex = 2
      ParentShowHint = False
      ShowHint = False
      Style = tbsCheck
      OnClick = tlbTypeSubparcelsClick
    end
    object btnDeleteAllBorder: TToolButton
      Tag = 2
      Left = 330
      Top = 0
      Hint = 
        #13#10#1047#1072#1087#1086#1083#1085#1103#1077#1090#1089#1103' '#1074' '#1086#1090#1085#1086#1096#1077#1085#1080#1080' '#1095#1072#1089#1090#1077#1081' '#1047#1059#13#10' ('#1074' '#1090'.'#1095'. '#1084#1085#1086#1075#1086#1082#1086#1085#1090#1091#1088#1085#1099#1093'), '#1082 +
        #1086#1090#1086#1088#1099#1077' '#1089#1086#1093#1088#1072#1085#1103#1102#1090#1089#1103' '#1074' '#1087#1088#1077#1078#1085#1080#1093#13#10' '#1075#1088#1072#1085#1080#1094#1072#1093' '
      AutoSize = True
      Caption = #1048#1089#1082#1083#1102#1095#1077#1085#1080#1077' '#1075#1088#1072#1085#1080#1094#1099' '#1050#1052#1047#1059
      Grouped = True
      ImageIndex = 1
      ParentShowHint = False
      ShowHint = False
      Style = tbsCheck
      OnClick = tlbTypeSubparcelsClick
    end
  end
  object actlst: TActionList
    Images = formContainer.ilCommands
    Left = 88
    Top = 104
    object actContrours_AddNew: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1082#1086#1085#1090#1091#1088
      Hint = #1044#1086#1073#1072#1074#1080#1090#1100' '#1082#1086#1085#1090#1091#1088
      ImageIndex = 0
      OnExecute = actContrours_AddNewExecute
    end
    object actContours_DeleteCurrent: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1074#1099#1073#1088#1072#1085#1085#1099#1081' '#1082#1086#1085#1090#1091#1088
      Hint = #1059#1076#1072#1083#1080#1090#1100' '#1074#1099#1073#1088#1072#1085#1085#1099#1081' '#1082#1086#1085#1090#1091#1088
      ImageIndex = 1
      OnExecute = actContours_DeleteCurrentExecute
    end
    object actClipBrd_Paste: TAction
      Caption = 'actClipBrd_Paste'
      ImageIndex = 2
      OnExecute = actClipBrd_PasteExecute
    end
    object actClipBrd_Copy: TAction
      Caption = 'actClipBrd_Copy'
      ImageIndex = 3
      OnExecute = actClipBrd_CopyExecute
    end
    object actContour_next: TAction
      Caption = 'actContour_next'
      ImageIndex = 8
      OnExecute = actContour_nextExecute
    end
    object actContour_prev: TAction
      Caption = 'actContour_prev'
      ImageIndex = 7
      OnExecute = actContour_prevExecute
    end
    object actValid: TAction
      Caption = 'actValid'
      ImageIndex = 4
      OnExecute = actValidExecute
    end
  end
end
