unit edGrid;

interface
 uses
  Windows, Classes, Grids, graphics, Controls, Dialogs, Messages, Clipbrd, SysUtils, Forms,
  MSMCommon_ ;

 Type

  TEdTypeEdit = set of (teNumber,teArithmetic);

  TEdInplaceEdit = class(TInplaceEdit)
   private
    FAcceptSymbol: string;
    FAcceptSymbolOnly: Boolean;

    procedure SetAcceptSymbolOnly(value: Boolean);
    procedure SetAcceptSymbol(value: string);

    procedure WMPaste(var Message: TMessage); message WM_PASTE;
   protected

   public
     constructor Create(AOwner: TComponent); override;
     property AcceptSymbolOnly: Boolean Read FAcceptSymbolOnly write SetAcceptSymbolOnly;
     property AcceptSymbol: string read FAcceptSymbol write SetAcceptSymbol;
  end;

  TEdStringGrid = class(TStringGrid)
   private
    FDeletingRow: boolean;
    FAcceptKeys: string;
    FReadOnly: boolean;
   protected
    procedure DrawCell(ACol, ARow: Longint; ARect: TRect; AState: TGridDrawState); override;
    function CreateEditor: TInplaceEdit; override;
    function SelectCell(ACol, ARow: Longint): Boolean; override;
    function CanEditAcceptKey(Key: Char): Boolean; override;
    procedure SetEditText(ACol, ARow: Longint; const Value: string); override;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState;
      X, Y: Integer); override;
    procedure WMPaint(var Message: TWMPaint); message WM_PAINT;
    procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
   public
    constructor Create(AOwner: TComponent); override;
    procedure InsertRow(Index:integer);
    procedure DeleteRow(ARow: Longint); override;
    function AddRow:integer;
    function IsSelectedCell(ACol,ARow: integer): Boolean;
    property AcceptKeys: string read FAcceptKeys write FAcceptKeys;
    property DeletingRow: boolean read FDeletingRow;
    property InplaceEditReadOnly: Boolean read FReadOnly write FReadOnly;

   end;


implementation

{ TEdStringGrid }

function TEdStringGrid.AddRow: integer;
begin
   Result:=RowCount;
  InsertRow(RowCount);
end;

function TEdStringGrid.CanEditAcceptKey(Key: Char): Boolean;
begin
 Result:= (self.FAcceptKeys='') or (pos(UpCase(key),self.FAcceptKeys)<>0);
end;

constructor TEdStringGrid.Create(AOwner: TComponent);
begin
 inherited;
 self.FDeletingRow:= false;
 self.DefaultDrawing:= True;
 self.FReadOnly:=false;
end;

function TEdStringGrid.CreateEditor: TInplaceEdit;
begin
 result:= TEdInplaceEdit.Create(Self);
 Result.ReadOnly:= self.FReadOnly;
end;


procedure TEdStringGrid.DeleteRow(ARow: Integer);
var
 _Row: integer;
begin
 self.FDeletingRow:= true;
 inherited;
 if ARow<self.RowCount then
  self.Row:= arow;
 self.FDeletingRow:= false;
end;

procedure TEdStringGrid.DrawCell(ACol, ARow: Integer; ARect: TRect; AState: TGridDrawState);
var
  Hold: Integer;
  _rect: TRect;
 _st: string;
 _cl: TColor;
begin
  _rect:= arect;
  _st:= self.Cells[acol,arow];
{
  _cl:= self.Canvas.Brush.Color;
  if ARow = self.Row then
   self.Canvas.Brush.Color:= $00F3E2D5;
  self.Canvas.FillRect(_rect);
  self.Canvas.TextRect(_rect,_st,[tfVerticalCenter,tfCenter]);
 self.Canvas.Brush.Color:= _cl;
}
self.Canvas.TextRect(_rect,_st,[tfVerticalCenter,tfCenter]);
  if Assigned(OnDrawCell) then begin
   if UseRightToLeftAlignment then begin
    ARect.Left := ClientWidth - ARect.Left;
    ARect.Right := ClientWidth - ARect.Right;
    Hold := ARect.Left;
    ARect.Left := ARect.Right;
    ARect.Right := Hold;
    ChangeGridOrientation(False);
   end;
   OnDrawCell(Self, ACol, ARow, ARect, AState);
   if UseRightToLeftAlignment then ChangeGridOrientation(True);
  end;

end;


procedure TEdStringGrid.InsertRow(Index: integer);
begin
  RowCount:=RowCount+1;
  MoveRow(RowCount-1,Index);
  Rows[Index].Clear;
end;

function TEdStringGrid.IsSelectedCell(ACol, ARow: integer): Boolean;
var
 _gr: TGridRect;
begin
 _gr:= self.Selection;
 Result:= msmcIsPointInRect(_gr.Left,_gr.Top,_gr.Right,_gr.Bottom,ACol,ARow);
end;

procedure TEdStringGrid.MouseDown(Button: TMouseButton; Shift: TShiftState; X,
  Y: Integer);
begin
  HideEditor;
  inherited;
end;

procedure TEdStringGrid.MouseMove(Shift: TShiftState; X, Y: Integer);
var
 t: TGridCoord;
begin
  inherited;
 //t:= self.MouseCoord(x,y);
 //(self.Parent.Parent as tform).Caption:= IntToStr(t.X)+' '+IntToStr(t.Y);
end;

function TEdStringGrid.SelectCell(ACol, ARow: Integer): Boolean;
var
 _rect: trect;
 _st: string;
begin
 result:= inherited;

 {
 if (self.FSelRow>0) and (self.FSelRow<self.RowCount) then begin
  self.Canvas.Font.Color:= self.Font.Color;
  self.CellRect(0,Arow,_rect);
  self.Canvas.TextRect(_rect,_st,[tfVerticalCenter,tfCenter]);
 end;

 if (ACol=0) and (ARow=self.FSelRow) then
  self.Canvas.Font.Color:= clred
 else
  self.Canvas.Font.Color:= self.Font.Color;
 self.CellRect(0,Arow,_rect);
 self.Canvas.TextRect(_rect,_st,[tfVerticalCenter,tfCenter]);
 }
end;

procedure TEdStringGrid.SetEditText(ACol, ARow: Integer; const Value: string);
begin
 //if CompareText(Value,self.cells[ACol,ARow])<> 0 then
  inherited;
end;

procedure TEdStringGrid.WMPaint(var Message: TWMPaint);
var
 _UpRect: trect;
 _FCRect,_r: trect;
begin
{
 GetUpdateRect(self.Handle,_UpRect,false);
 _FCRect:= self.CellRect(self.Col,self.Row);
 IntersectRect(_r,_UpRect,_FCRect);
 if not IsRectEmpty(_r) then
  self.InvalidateRow(self.Row);}
   inherited;
end;

{ TEdInplaceEdit }

constructor TEdInplaceEdit.Create(AOwner: TComponent);
begin
  inherited;
  self.Alignment:= taCenter;
 // self.NumbersOnly:= True;
end;

procedure TEdInplaceEdit.SetAcceptSymbol(value: string);
begin
 self.FAcceptSymbol:= value;
end;

procedure TEdInplaceEdit.SetAcceptSymbolOnly(value: Boolean);
begin
 self.FAcceptSymbolOnly:= value;
end;



procedure TEdInplaceEdit.WMPaste(var Message: TMessage);
var
  Value: string;
  Str: string;
  _i: Integer;
begin  {
  Clipboard.Open;
   Value := Clipboard.AsText;
  Clipboard.Clear;
  for _I := 1 to Length(Value) do
   if (self.Grid as TEdStringGrid).CanEditAcceptKey(value[_i]) then
  Clipboard.AsText:= Clipboard.AsText+Value[_i];
  Clipboard.Close; }
  inherited;
end;
end.
