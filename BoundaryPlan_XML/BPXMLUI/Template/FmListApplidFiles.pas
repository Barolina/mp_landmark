unit FmListApplidFiles;

///
///  AppledFiles -  �������� ini ����� 
///

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ButtonGroup, StdCtrls, ActnList,
  Vcl.ExtCtrls, Vcl.Grids, Vcl.ValEdit,

  BpXML_different,unRussLang, Vcl.ComCtrls, JvExComCtrls, JvListView,
  JvgStringGrid, JvStringHolder, JvListBox, JvExStdCtrls, JvHtControls,
  JvBackgrounds;

type
  TFrmListAppliedFiles = class(TForm)
    lstAplpiedFiles: TValueListEditor;
    btnCloe: TButton;
    pnlClose: TPanel;
    procedure lstAplpiedFilesDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
  private
    { Private declarations }    
  public
    procedure ReadIntoGrid(aSection: string; const aGrid: TStringGrid);
    { Public declarations }
    procedure ShowListAplpiedFiles();
  end;

var
  FrmListAppliedFiles: TFrmListAppliedFiles;

implementation
uses Winapi.SHFolder;

{$R *.dfm}
procedure TFrmListAppliedFiles.lstAplpiedFilesDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
  var Index : Integer;
const
  clPaleGreen = TColor($CCFFCC);
  clPaleRed =   TColor($CCCCFF);
  clPaleWhite = TColor($FFFFFF);
begin
//
    if (ACol = 1) and (ARow > 0) then
    begin
        if ( lstAplpiedFiles.Cells[ACol,aRow] = '['+rsLoadFile+']') then
             lstAplpiedFiles.Canvas.Brush.Color := clPaleGreen
        else
            if (lstAplpiedFiles.Cells[ACol,aRow] = '['+rsNotLoadFile+']') then
                lstAplpiedFiles.Canvas.Brush.Color := clPaleRed
            else
                lstAplpiedFiles.Canvas.Brush.Color := clPaleWhite;
        lstAplpiedFiles.Canvas.FillRect(Rect);
        lstAplpiedFiles.Canvas.TextOut(Rect.Left + 1, Rect.Top + 1, lstAplpiedFiles.Cells[Acol, Arow]);
    end;
end;

{TODO: ��� �������}
procedure TFrmListAppliedFiles.ReadIntoGrid(aSection: string; const aGrid: TStringGrid);
var
  SL: TStringList;
  i: Integer;
begin
  SL := TStringList.Create;
  try
      aGrid.ColCount := 2;
      TAppliedFileDirect.AppliedFileDirect.ReadSectionValues(aSection,SL);
      aGrid.RowCount := SL.Count;
      for i := 0 to SL.Count - 1 do
      begin
        aGrid.Cells[0,i] := SL.Names[i];
        aGrid.Cells[1,i] := SL.ValueFromIndex[i];
      end;
  finally
    SL.Free;
  end;
end;

procedure TFrmListAppliedFiles.ShowListAplpiedFiles;
begin
  if not  FileExists(STD_MP_TEMPDIR+'\'+cnstLogLoadFile) then
  begin
    MessageDlg(rsNoDetetectedLoadFile, mtInformation,[mbOK],-1);
    Exit;
  end;
  if not Assigned(FrmListAppliedFiles) then   Application.CreateForm(TFrmListAppliedFiles,FrmListAppliedFiles);
  FrmListAppliedFiles.Caption := rsNameFmApliedFiles;


  FrmListAppliedFiles.lstAplpiedFiles.Strings.LoadFromFile(STD_MP_TEMPDIR+'\'+cnstLogLoadFile);
  FrmListAppliedFiles.ShowModal;     
end;

end.
