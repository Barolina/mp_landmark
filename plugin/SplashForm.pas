unit SplashForm;

interface
uses 
 Windows,PNGImage,Forms,Graphics,
 System.Classes, System.IOUtils; 

type
  TSpashForm = class(TForm)
  public
   constructor Create;
  end;

  TSplash<T: TForm,constructor>=class
    private
      FImage:TPNGImage;
      FForm: T;
      procedure ToLayeredWindow;
    public
     constructor Create(aPNGData: TStream); overload;
     constructor Create(const aPNGFile: string); overload;
     destructor Destroy; override;
     procedure Show(StayOnTop:Boolean);
     procedure Close;
  end;

procedure CreatePremultipliedBitmap(DstBitMap:TBitmap;SrcPngImage:TPNGImage);

implementation
procedure CreatePremultipliedBitmap(DstBitMap:TBitmap;SrcPngImage:TPNGImage);
type
  TRGBTripleArray = ARRAY[Word] of TRGBTriple;
  pRGBTripleArray = ^TRGBTripleArray;
  TRGBAArray = array[Word] of TRGBQuad;
  PRGBAArray = ^TRGBAArray;
var x,y:integer;
    TripleAlpha:double;
    pColor:pRGBTripleArray;
    pAlpha:PByteArray;
    pBmp:pRGBAArray ;
begin
  DstBitMap.Height := SrcPngImage.Height;
  DstBitMap.Width := SrcPngImage.Width;
  DstBitMap.PixelFormat := pf32bit;
  for y := 0 to SrcPngImage.Height-1 do
    begin
    pAlpha := SrcPngImage.AlphaScanline[y];
    pColor := SrcPngImage.Scanline[y];
    pBmp := DstBitMap.ScanLine[y];
    for x := 0 to SrcPngImage.Width-1 do
      begin
        pBmp[x].rgbReserved:=pAlpha[x];
        // ����������� � ������ ������
        TripleAlpha := pBmp[x].rgbReserved / 255;
        pBmp[x].rgbBlue  := byte(trunc(pColor[x].rgbtBlue*TripleAlpha));
        pBmp[x].rgbGreen := byte(trunc(pColor[x].rgbtGreen*TripleAlpha));
        pBmp[x].rgbRed   := byte(trunc(pColor[x].rgbtRed*TripleAlpha));
      end;
    end;
end;

constructor TSplash<T>.Create(aPNGData: TStream);
begin
  self.FForm:= T.create;
  self.FImage:= TPNGImage.Create;
  self.FImage.LoadFromStream(aPNGData);
end;

constructor TSplash<T>.Create(const aPNGFile: string);
var
  _stream: TMemoryStream;
begin
 if TFile.Exists(aPNGFile) then begin
  _stream:= TMemoryStream.Create;
  _stream.LoadFromFile(aPNGFile);
  self.Create(_stream);
  _stream.Free;
 end else begin
  self.FForm:= T.create;
  self.FImage:= TPNGImage.Create;
 end;
end;

destructor TSplash<T>.Destroy;
begin
 self.Close;
 self.FImage.Free;
 self.FForm.free;
 inherited;
end;

procedure TSplash<T>.ToLayeredWindow;
var BitMap:TBitMap;
    bf:TBlendFunction;
    BitmapSize: TSize;
    BitmapPos:Tpoint;
begin
  {�������� ���������� ������� �����}
  BitMap:=TBitMap.Create;
  CreatePremultipliedBitmap(Bitmap,FImage);
  {�������� BlendFunction}
  with bf do
  begin
    BlendOp := AC_SRC_OVER;
    BlendFlags := 0;
    AlphaFormat := AC_SRC_ALPHA;
    SourceConstantAlpha := 255;
  end;
  {�������� ������� BitMap}
  BitmapSize.cx := Bitmap.Width;
  BitmapSize.cy := Bitmap.Height;
  {�������� ���������� BitMap}
  BitmapPos.X := 0;
  BitmapPos.Y := 0;
  {��������� �������� ����� ����}
  SetWindowLong(self.FForm.Handle, GWL_EXSTYLE,
    GetWindowLong(self.FForm.Handle, GWL_EXSTYLE)+ WS_EX_LAYERED);
  {����������� ���� � �������� ���� }
  UpdateLayeredWindow(
    self.FForm.Handle,
    0,
    nil,//pos
    @BitmapSize,//size
    bitmap.Canvas.Handle,//src
    @BitmapPos,//pptsrc
    0,
    @bf,
    ULW_ALPHA
  );
  BitMap.Free;
end;


procedure TSplash<T>.Show(StayOnTop:Boolean);
begin
  {������� ���� � ������� �����������}

  self.FForm.BorderStyle := bsNone;
   self.FForm.width := FImage.Width;
   self.FForm.Height := FImage.Height;
   self.FForm.position := poScreenCenter;
    if StayOnTop then self.FForm.formstyle := fsStayOnTop;

  {������ �� ���� �������� ����}
  self.ToLayeredWindow;
  //  self.SplashForm.InsertControl(TEdit.Create(nil));
  {����������}
  self.FForm.Show;
end;


procedure TSplash<T>.Close;
begin
  {������}
  self.FForm.Close;
end;

{ TSpashForm }

constructor TSpashForm.Create;
begin
 inherited Create(nil)
end;

end.

