unit frAllBorders;

{
  �������� ������ ������������ ����� ����������� �������� �������
  (� �.�. �������������� �����)

  AllBorder

}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ComCtrls,

  STD_MP, frEntitySpatial,XMLIntf, ActnList,
  System.Actions;
type
  TFrameAllBorders = class(TFrame)
    ActionList: TActionList;
    ActionChange_DefOrNumPP: TAction;
    ActionChange_SubParcelRealty: TAction;
    ActionChange_ExistingOrNewSubParcel: TAction;
    Panel1: TPanel;

  private
    { Private declarations }
    FXmlAllBorder   : IXMLTSpecifyRelatedParcel_AllBorder;
    FEntity_Spatila : TFrameEntity_Spatial;

  protected
   procedure InitializationUI; 
   procedure SetAllBorder( const aAllBorder: IXMLTSpecifyRelatedParcel_AllBorder);
  public
    { Public declarations }
    constructor Create(Owner: TComponent); override;
    function IsAssignedAllBorder: Boolean;

    property XmlAllBorder: IXMLTSpecifyRelatedParcel_AllBorder read FXmlAllBorder write SetAllBorder;
  end;

implementation



{$R *.dfm}

{ TFrameAllBorders }

constructor TFrameAllBorders.Create(Owner: TComponent);
begin
 inherited;
  FXmlAllBorder := nil;
  self.FEntity_Spatila := TFrameEntity_Spatial.Create(self.Panel1);
  self.FEntity_Spatila.Parent :=  self.Panel1;
  self.FEntity_Spatila.Align := alClient;
  self.FEntity_Spatila.Visible := true;
end;

procedure TFrameAllBorders.InitializationUI;
begin
 if self.IsAssignedAllBorder then
 begin
   FEntity_Spatila.EntitySpatial := FXmlAllBorder.Entity_Spatial;
   FEntity_Spatila.Visible := true;
 end
 else
 FEntity_Spatila.EntitySpatial := nil;
end;

function TFrameAllBorders.IsAssignedAllBorder: Boolean;
begin
  Result:= self.FXmlAllBorder<>nil;
end;

procedure TFrameAllBorders.SetAllBorder;
begin
  if self.FXmlAllBorder = aAllBorder  then Exit;
  self.FXmlAllBorder := aAllBorder;
  InitializationUI
end;


end.
