object FrameChangeSubParcels: TFrameChangeSubParcels
  Left = 0
  Top = 0
  Width = 823
  Height = 293
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Tahoma'
  Font.Style = []
  ParentFont = False
  TabOrder = 0
  object pnlControls: TPanel
    Left = 0
    Top = 0
    Width = 823
    Height = 28
    Align = alTop
    Caption = 'pnlControls'
    ShowCaption = False
    TabOrder = 0
    object Contours_List_ButtonGroup: TButtonGroup
      Left = 217
      Top = 1
      Width = 185
      Height = 26
      Align = alLeft
      BevelInner = bvNone
      BevelOuter = bvNone
      BorderStyle = bsNone
      Images = formContainer.ilCommands
      Items = <
        item
          Action = actContour_prev
          Caption = #1055#1088#1077#1076#1099#1076#1091#1097#1080#1081' '#1082#1086#1085#1090#1091#1088
          Hint = #1055#1088#1077#1076#1099#1076#1091#1097#1080#1081' '#1082#1086#1085#1090#1091#1088
        end
        item
          Action = actContour_next
          Caption = #1057#1083#1077#1076#1091#1102#1097#1080#1081' '#1082#1086#1085#1090#1091#1088
          Hint = #1057#1083#1077#1076#1091#1102#1097#1080#1081' '#1082#1086#1085#1090#1091#1088
        end
        item
          Action = actContrours_AddNew
        end
        item
          Action = actContours_DeleteCurrent
        end
        item
          Action = actClipBrd_Copy
          Caption = #1057#1082#1086#1087#1080#1088#1086#1074#1072#1090#1100' '#1076#1072#1085#1085#1099#1077' '#1074' '#1073#1091#1092#1077#1088' '#1086#1073#1084#1077#1085#1072
          Hint = #1057#1082#1086#1087#1080#1088#1086#1074#1072#1090#1100' '#1076#1072#1085#1085#1099#1077' '#1074' '#1073#1091#1092#1077#1088' '#1086#1073#1084#1077#1085#1072
        end
        item
          Action = actClipBrd_Paste
          Caption = #1042#1089#1090#1072#1074#1080#1090#1100' '#1076#1072#1085#1085#1099#1077' '#1080#1079' '#1073#1091#1092#1077#1088' '#1086#1073#1084#1077#1085#1072
          Hint = #1042#1089#1090#1072#1074#1080#1090#1100' '#1076#1072#1085#1085#1099#1077' '#1080#1079' '#1073#1091#1092#1077#1088' '#1086#1073#1084#1077#1085#1072
        end
        item
          Action = actValid
          Caption = #1055#1088#1086#1074#1077#1088#1082#1072' '#1082#1086#1088#1088#1077#1082#1090#1085#1086#1089#1090#1080' '#1082#1086#1086#1088#1076#1080#1085#1072#1090#1085#1086#1075#1086' '#1086#1087#1080#1089#1072#1085#1080#1103
          Hint = #1055#1088#1086#1074#1077#1088#1082#1072' '#1082#1086#1088#1088#1077#1082#1090#1085#1086#1089#1090#1080' '#1082#1086#1086#1088#1076#1080#1085#1072#1090#1085#1086#1075#1086' '#1086#1087#1080#1089#1072#1085#1080#1103
        end>
      ShowHint = True
      TabOrder = 0
    end
    object cbbContoursType: TComboBox
      Left = 402
      Top = 1
      Width = 351
      Height = 22
      Hint = #1040#1082#1090#1080#1074#1085#1099#1081' '#1090#1080#1087' '#1082#1086#1085#1090#1091#1088#1072
      Align = alLeft
      Style = csDropDownList
      Color = 15921123
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      TextHint = #1040#1082#1090#1080#1074#1085#1099#1081' '#1090#1080#1087' '#1082#1086#1085#1090#1091#1088#1072
      StyleElements = []
      OnChange = cbbContoursTypeChange
      Items.Strings = (
        #1054#1073#1088#1072#1079#1091#1077#1084#1072#1103' '#1095#1072#1089#1090#1100' '#1091#1095#1072#1089#1090#1082#1072
        #1057#1091#1097#1077#1089#1090#1074#1091#1102#1097#1072#1103' ('#1091#1090#1086#1095#1085#1103#1077#1084#1072#1103', '#1080#1079#1084#1077#1085#1103#1077#1084#1072#1103')  '#1095#1072#1089#1090#1100' '#1091#1095#1072#1089#1090#1082#1072
        #1057#1091#1097#1077#1089#1090#1074#1091#1102#1097#1072#1103' ('#1085#1077#1080#1079#1084#1077#1085#1103#1077#1084#1072#1103') '#1095#1072#1089#1090#1100' '#1091#1095#1072#1089#1090#1082#1072)
    end
    object btnEdCurContour: TButtonedEdit
      Left = 1
      Top = 1
      Width = 216
      Height = 26
      Align = alLeft
      Alignment = taCenter
      Color = 15921123
      Images = formContainer.ilCommands
      RightButton.ImageIndex = 6
      RightButton.Visible = True
      TabOrder = 2
      OnExit = btnEdCurContourExit
      OnKeyDown = btnEdCurContourKeyDown
      OnRightButtonClick = btnEdCurContourRightButtonClick
      ExplicitHeight = 22
    end
  end
  object Contours_List_ListBox: TListBox
    Left = 1
    Top = 25
    Width = 216
    Height = 209
    Style = lbOwnerDrawVariable
    BevelInner = bvNone
    BevelOuter = bvNone
    Color = 15921123
    ItemHeight = 13
    TabOrder = 1
    Visible = False
    OnClick = Action_Contours_SetCurrentExecute
    OnKeyDown = Contours_List_ListBoxKeyDown
    OnMouseLeave = Contours_List_ListBoxMouseLeave
  end
  object actlst: TActionList
    Images = formContainer.ilCommands
    Left = 224
    Top = 38
    object actContrours_AddNew: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1082#1086#1085#1090#1091#1088
      Hint = #1044#1086#1073#1072#1074#1080#1090#1100' '#1082#1086#1085#1090#1091#1088
      ImageIndex = 0
      OnExecute = actContrours_AddNewExecute
    end
    object actContours_DeleteCurrent: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1074#1099#1073#1088#1072#1085#1085#1099#1081' '#1082#1086#1085#1090#1091#1088
      Hint = #1059#1076#1072#1083#1080#1090#1100' '#1074#1099#1073#1088#1072#1085#1085#1099#1081' '#1082#1086#1085#1090#1091#1088
      ImageIndex = 1
      OnExecute = actContours_DeleteCurrentExecute
    end
    object actClipBrd_Paste: TAction
      Caption = 'actClipBrd_Paste'
      ImageIndex = 2
      OnExecute = actClipBrd_PasteExecute
    end
    object actClipBrd_Copy: TAction
      Caption = 'actClipBrd_Copy'
      ImageIndex = 3
      OnExecute = actClipBrd_CopyExecute
    end
    object actContour_next: TAction
      Caption = 'actContour_next'
      ImageIndex = 8
      OnExecute = actContour_nextExecute
    end
    object actContour_prev: TAction
      Caption = 'actContour_prev'
      ImageIndex = 7
      OnExecute = actContour_prevExecute
    end
    object actValid: TAction
      Caption = 'actValid'
      ImageIndex = 4
      OnExecute = actValidExecute
    end
  end
end
