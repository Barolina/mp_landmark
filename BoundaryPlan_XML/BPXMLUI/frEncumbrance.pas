unit frEncumbrance;

{
  �������������� �����
}


interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, StdCtrls, ActnList,

  STD_MP, BpXML_different, XMLIntf, System.Actions, Vcl.ExtCtrls,
  frDataDocuments, JvExStdCtrls, JvGroupBox;

type
  TFrameEncumbrance = class(TFrame)
    Label1: TLabel;
    Type_ComboBox: TComboBox;
    Name_Edit: TEdit;
    Label2: TLabel;
    ActionList: TActionList;
    Action_Type_Write: TAction;
    Action_Name_Write: TAction;
    Panel1: TPanel;
    pnlDocuments: TJvGroupBox;
    pnl1: TPanel;

    procedure Action_Type_WriteExecute(Sender: TObject);
    procedure Action_Name_WriteExecute(Sender: TObject);
    procedure Name_EditKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
    FXmlEncumbrance : IXMLTEncumbrance;
    FDocument_frame : TFrameDataDocuments;
  protected
    procedure InitializationUI;
    procedure SetEncumbrance(const aEncumbrance: IXMLTEncumbrance);
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;

    //�������� ���������������� ������ � �����������
    function UI_name_IsEmpty: boolean;
    function UI_type_IsValid: boolean;

    procedure UI_name_WriteTo(const aEncumbrance: IXMLTEncumbrance);
    procedure UI_type_WriteTo(const aEncumbrance: IXMLTEncumbrance);
    procedure UI_name_ReadOf(const aEncumbrance: IXMLTEncumbrance);
    procedure UI_type_ReadOf(const aEncumbrance: IXMLTEncumbrance);

    procedure UI_WriteTo(const aEncumbrance: IXMLTEncumbrance);
    procedure UI_ReadOf(const aEncumbrance: IXMLTEncumbrance);

    function IsAssignedEncumbrance:boolean;
    property Encumbrance: IXMLTEncumbrance read FXmlEncumbrance write SetEncumbrance;
  end;

implementation

uses MsXMLAPI;

const
 ExMSG_Data_name = '������ ��������� "���������� �����������"';
 ExMSG_Data_type = '������ ��������� "��� �����������"';

{$R *.dfm}

{ TFrameEncumbrance }

procedure TFrameEncumbrance.Action_Name_WriteExecute(Sender: TObject);
begin
 if self.IsAssignedEncumbrance then
  self.UI_name_WriteTo(self.FXmlEncumbrance);
end;

procedure TFrameEncumbrance.Action_Type_WriteExecute(Sender: TObject);
begin
 if self.IsAssignedEncumbrance then
  self.UI_type_WriteTo(self.FXmlEncumbrance);
end;

constructor TFrameEncumbrance.Create(AOwner: TComponent);
begin
 inherited;
 TSTD_MP_ListofIndexGenerate.BpXml_LoadDictionary(STD_MP_WorkDir+'dEncumbrances.xsd',self.Type_ComboBox.Items);
 FDocument_frame := TFrameDataDocuments.Create(pnlDocuments);
 FDocument_frame.Parent := pnlDocuments;
 FDocument_frame.Align := alClient;
// self.InitializationUI;
end;

procedure TFrameEncumbrance.InitializationUI;
begin
 self.Name_Edit.Text:= '';
 self.Type_ComboBox.ItemIndex:= -1;
 if self.IsAssignedEncumbrance then
 begin
  self.UI_ReadOf(self.FXmlEncumbrance);
  FDocument_frame.XmlEncumbrance := Self.FXmlEncumbrance.Documents;
 end
 else
   FDocument_frame.XmlEncumbrance := nil;
end;

function TFrameEncumbrance.IsAssignedEncumbrance: boolean;
begin
 Result:= self.FXmlEncumbrance<>nil;
end;

procedure TFrameEncumbrance.Name_EditKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if key = VK_RETURN then
  if (Sender is TCustomEdit) then
   TCustomEdit(Sender).Parent.SetFocus;
end;

procedure TFrameEncumbrance.SetEncumbrance(const aEncumbrance: IXMLTEncumbrance);
begin
 if self.FXmlEncumbrance=aEncumbrance then EXIT;
 self.FXmlEncumbrance:= aEncumbrance;
 self.InitializationUI;
end;

procedure TFrameEncumbrance.UI_ReadOf(const aEncumbrance: IXMLTEncumbrance);
begin
 self.UI_name_ReadOf(aEncumbrance);
 self.UI_type_ReadOf(aEncumbrance);
end;

procedure TFrameEncumbrance.UI_WriteTo(const aEncumbrance: IXMLTEncumbrance);
begin
 self.UI_name_WriteTo(aEncumbrance);
 self.UI_type_WriteTo(aEncumbrance);
end;

procedure TFrameEncumbrance.UI_name_ReadOf(const aEncumbrance: IXMLTEncumbrance);
var
 _node: IXMLNode;
begin
 _node:= aEncumbrance.ChildNodes.FindNode('Name');
 if _node<>nil then
  self.Name_Edit.Text:= _node.Text
 else
  self.Name_Edit.Text:= '';
end;

procedure TFrameEncumbrance.UI_name_WriteTo(const aEncumbrance: IXMLTEncumbrance);
begin
 if not self.UI_name_IsEmpty then
  aEncumbrance.Name:= self.Name_Edit.Text
 else
  MsXml_RemoveChildNode(aEncumbrance,'Name');
end;

function TFrameEncumbrance.UI_name_IsEmpty: boolean;
begin
 Result:= self.Name_Edit.Text='';
end;

function TFrameEncumbrance.UI_type_IsValid: boolean;
begin
 Result:= self.Type_ComboBox.ItemIndex<>-1;
end;

procedure TFrameEncumbrance.UI_type_ReadOf( const aEncumbrance: IXMLTEncumbrance);
begin
 self.Type_ComboBox.ItemIndex:= TSTD_MP_ListofIndexGenerate.BpXml_FindItemByCode(self.Type_ComboBox.Items,aEncumbrance.Type_);
end;

procedure TFrameEncumbrance.UI_type_WriteTo(const aEncumbrance: IXMLTEncumbrance);
var
 _item: TBpXml_DictionaryItem;
 _index: Integer;
begin
 if not self.UI_type_IsValid then
  raise Exception.Create(ExMSG_Data_type);

 _index:= self.Type_ComboBox.ItemIndex;
 _item:= TBpXml_DictionaryItem(self.Type_ComboBox.Items.Objects[_index]);
 aEncumbrance.Type_:= _item.Code;
end;



end.
