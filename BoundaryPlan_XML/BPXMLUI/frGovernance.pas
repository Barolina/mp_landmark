unit frGovernance;
{

  ���������� ����������� ����
  IXMLSTD_MP_Title_Client_Organization
}
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls,

  STD_MP, frAgent, XMLIntf,unRussLang;

type
  TFrameGovernance = class(TFrame)
    Label_Name: TLabel;
    pnlAgent: TPanel;
    mmoName: TMemo;

    procedure Name_EditExit(Sender: TObject);
    procedure Name_EditKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
    FXmlOrganisation : IXMLSTD_MP_Title_Client_Governance;

    FAgent           : TFrameAgent;
    function IsAssignedOrganisation: boolean;

 protected
   procedure InitializationUI;
   procedure SeTitleOrganisation(const aOrganisation: IXMLSTD_MP_Title_Client_Governance);
  public
   { Public declarations }
   constructor Create(AOwner: TComponent); override;

   procedure UI_Name_ReadOf(const aOrganisation: IXMLSTD_MP_Title_Client_Governance);
   procedure UI_ReadOf(const aOrganisation: IXMLSTD_MP_Title_Client_Governance);

   procedure UI_Name_WriteTo(const aOrganisation: IXMLSTD_MP_Title_Client_Governance);
   procedure UI_WriteTo(const aOrganisation: IXMLSTD_MP_Title_Client_Governance);

   property XMLTitleOrganisation:IXMLSTD_MP_Title_Client_Governance read FXmlOrganisation write SeTitleOrganisation;
  end;

implementation

uses MsXmlApi;
{$R *.dfm}


{ TFrameOrgabisation }


constructor TFrameGovernance.Create(AOwner: TComponent);
begin
  inherited;
  self.FAgent:= TFrameAgent.Create(self.pnlAgent);
  self.FAgent.Parent:= self.pnlAgent;
  self.FAgent.Align:= alClient;
end;

procedure TFrameGovernance.InitializationUI;
begin
 if self.IsAssignedOrganisation then
 begin
  self.UI_ReadOf(self.FXmlOrganisation);
  self.FAgent.XMLAgent := FXmlOrganisation.Agent;
 end
 else begin
  self.mmoName.Clear;
  FAgent.XMLAgent := nil;
 end;
end;

procedure TFrameGovernance.Name_EditExit(Sender: TObject);
begin
 if not self.IsAssignedOrganisation then raise Exception.Create(ExMSG_NotAssignedROOT);
 self.UI_Name_WriteTo(self.FXmlOrganisation);
end;

procedure TFrameGovernance.Name_EditKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if key = VK_RETURN then
  if (Sender is TCustomEdit) then
   TCustomEdit(Sender).Parent.SetFocus;
end;

function TFrameGovernance.IsAssignedOrganisation: boolean;
begin
 Result:= self.FXmlOrganisation<>nil;
end;

procedure TFrameGovernance.SeTitleOrganisation(const aOrganisation: IXMLSTD_MP_Title_Client_Governance);
begin
 if self.FXmlOrganisation=aOrganisation then EXIT;
 self.FXmlOrganisation:= aOrganisation;
 self.InitializationUI;
end;

procedure TFrameGovernance.UI_Name_ReadOf(
  const aOrganisation: IXMLSTD_MP_Title_Client_Governance);
begin
  mmoName.Lines.Add(MsXml_ReadChildNodeValue(aOrganisation,'Name'));
end;

procedure TFrameGovernance.UI_Name_WriteTo;
var
 _st: string;
begin
 _st:= self.mmoName.Text;
 if _st<>'' then
  aOrganisation.Name:= _st
 else
  MsXml_RemoveChildNode(aOrganisation,'Name');
end;


procedure TFrameGovernance.UI_ReadOf;
begin
 self.UI_Name_ReadOf(aOrganisation);
end;

procedure TFrameGovernance.UI_WriteTo;
begin
 self.UI_WriteTo(aOrganisation);
end;

end.
