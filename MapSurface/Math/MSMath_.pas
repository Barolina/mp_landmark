{*******************************************************}
{      |----------------------------------------|       }
{      |- (c) 2010-2011 ���������� ���������.  -|       }
{      |-          kras_ai@mail.ru             -|       }
{      |----------------------------------------|       }
{*******************************************************}

//11.09.12
// ��������� procedure msmGetParallelEx(Source,Dest: TMSMPoints; Dist:double)
{.$define testTMSMPoints}
unit MSMath_;

interface

uses
 Math,
 MSMType_,
// MSMIType_,
 MSMCommon_,
 SysUtils;

type

 //��� ��������
 TLineType = (ltLine,ltInterval);

 //��� ���������� (���������� ��...)
 TDistKind = (dkLine, dkPointA, dkPointB);

 //��� ����������� ������ (������������)
 TDirectionCurveKind = (dcWithHourArrow,dcWithOutHourArrow);

//==============================================================================
//                                �����
//==============================================================================

//���������� �� ����� A �� B (�������� ��� msmcGetDistanceBetweenPoints)
{OK}function msmGetDistanceBetweenPoints(A,B:TMSMPoint):Double;

//���������� �� P1 �� P2 ������ Limit (���������� ��������) (�������� ��� msmcIsDistanceLessThen)
{OK}function msmIsDistanceLessThen(P1,P2 : TMSMPoint; Limit : double): Boolean;
{OK}function msmIsDistanceLessThenA(P1,P2 : TMSMPoint; Limit : double; var Dist: double) : Boolean;

//������� ����� P ������ ����� C �� ���� Angle � ��������.
//��������� ������������ � vP; (�������� ��� msmcDoRotatePoint)
//����� vP=C ��� vP=P
{OK}procedure msmGetRotatePoint(C,P,vP:TMSMPoint; Angle: double);

//����� �������� ����� P �� ������ A-B  (�������� ��� msmcProjectionPointToLine)
//����� vP=A,B,P �� vP<>nil
{OK}procedure msmGetProjectionPointToLine(A,B,P,vP: TMSMPoint);

//���������� �� ����� P �� (�������/�����) AB
{OK}function msmGetDistancePointToLine(P,A,B : TMSMPoint; LineType: TLineType; var DistKind: TDistKind; vP: TMSMPoint = nil): double;

//�������������� ����� P ������������� ������ A-B (�������� ��� msmcPointInRect)
//���������� ������� �������� �� ������
{OK}function msmIsPointInRect(R1,R2,P: TMSMPoint): Boolean;

//�������������� ����� P ����� A-B (�������� ��� msmcPointInLine �  msmcPointInInterval)
{OK}function msmIsPointInLine(A,B: TMSMPoint; LineType: TLineType; P: TMSMPoint): Boolean;

//����������� �� ����� P ��������� ������������� ������� Points
type
 TRelationPoint = Cardinal;
const
 TIsPoint_OUT: TRelationPoint = $00000000;
 TIsPoint_IN: TRelationPoint = $10000000;
 TIsPoint_VERTEX: TRelationPoint = $30000000;
 TIsPoint_SEGMENT: TRelationPoint = $40000000;

{OK}function msmIsPointInArea(Point: TMSMPoint; Area: TMSMPoints; ControlExtent: Boolean = True): TRelationPoint;

//�� �������
function _msmIsPointInArea_Fast(Point: TMSMPoint; Area: TMSMPoints; ControlExtent: Boolean = True): TRelationPoint;

//����������� �� ����� P ������ Points
{OK}function msmIsPointInCurve(Point: TMSMPoint; Curve: TMSMPoints; AsClosed: boolean; ControlExtent: Boolean = True): TRelationPoint;

//==============================================================================
//                                �����
//==============================================================================

//���� ����� ���� OX � �������� ( ��������,������ ),
//����������� ������ ������� ������� � �������� [0..360)
//���� S (eq) E �� result=0;
//(�� �� ���������� ������������� ����� � ������������� ������� ���������)
//(�������� ��� msmcGetAngleLine)
function msmGetAngleLine(A,B: TMSMPoint): double;

//���� (� ��������) ����� ����� ��������� ( ������� )
//����������� �� ������ ������ �� ������ ������ ������� �������
//(�������� ��� msmcGetAngleBetweenLines)
function msmGetAngleBetweenLines(A1,B1,A2,B2: TMSMPoint): double;

//����������� �������� �� ����� ������������ (�������� ��� msmcIsParallelLine)
{OK}function msmIsParallelLine(A1,B1,A2,B2: TMSMPoint): Boolean;

//����������� �������� �� ������� A1B1 � A2B2 ������������� (�������� ��� msmcIsCollinearlLine)
{OK}function msmIsCollinearlLine(A1,B1,A2,B2: TMSMPoint): Boolean;

//��������� � vA,vB ������� ������������ A1,B1 � ������������ �� ������ A2,B2
//(�������� ��� msmcDoCheckCollinearlLine)
function msmGetCollinearlLine(A1,B1,A2,B2,vA,vB: TMSMPoint): Boolean;

//�������� ���������� ������ � ������� AB
//(�������� ��� msmcGetCentrePointLine)
procedure msmGetCentrePointLine(A,B,vC: TMSMPoint);

//��������� ��������� ����� ����� ���� ����� (������ ��� ��������).
//����� ����� (�����) �������� ���������� I1I2. � ������ ����� ����� ������� "������������" ����� �� A1,B1 � A2,B2.
//I1,I2 - ����� ���� nil; ����� �������� ��������� �� ������������.
//��������� �������������� �������� ������������� ����� � �����(�)
{OK}function msmGetCommonPointsLines(A1,B1,A2,B2 : TMSMPoint; LineType : TLineType; vI1,vI2: TMSMPoint): boolean;

//������� �� ����� ����� ����� (������.) � ������������� �������???  ������� ���  msmGetCommonPointsLinesAndRect
{OK}function msmIsCommonPointsLinesAndRect(A,B: TMSMPoint; LineType: TLineType; R1,R2: TMSMPoint): boolean;


type
 TMSMCommonPoints =
 (
  cpNo, //������ ���������� ����� �����
  cpIn, //����� ����� ���������� ������ ����������
  cpIntersec, //����� ����� � ��� ����� ���������� ������������
  cpOut
 );

//��������� ������� ( ������ ) A-B � ������������� ������� P1-P2;
//Mode = LkLine ��� LkInterval;
//vI1,vI2 - ����� ����������� (������� ��������� ����� ���� nil, ����� �������� ��������� �� ������������).
//vI1,vI2 - ����������� � A,B :)
//���������� �1�2 �������� �� �����
//���� ���� ����� �� "�������" p1p2 ����� ��������� ������������
//No - ��� ����� �����;
//In - ����� ����� vI1,vI2 ����������� ������� ���������� A,B � ������������� ������� (� ������������� ����� A,B); (������ � ������ LkInterval)
//Intersec - ����� ����� vI1,vI2 ������ �������� ���������� � ���������� ��������� ������������� �������� �����.
{OK}function msmGetCommonPointsLinesAndRect(A,B: TMSMPoint; LineType: TLineType; R1,R2,vI1,vI2 :TMSMPoint): TMSMCommonPoints;

//������� �� ����� ����� ����� � ������ (AsClosed = true - ����������� ���������� �������)
{OK}function msmIsCommonPointsLinesAndCurve(A,B: TMSMPoint; LineType: TLineType; Curve: TMSMPoints; AsClosed: Boolean): boolean;

//�������� � rPoints ����� ����� ����� � ������ (AsClosed = true - ����������� ���������� �������).
//����� ����������� �� � � �. �������� ������ msmDoFilterPointsOnEquals
//Points=rPoints �����
{OK}function msmGetCommonPointsLinesAndCurve(A,B: TMSMPoint; LineType: TLineType; Curve: TMSMPoints; AsClosed: Boolean; vCPoints: TMSMPoints): boolean;


//���������� ��������� ����� ����� (���� ��� ����) ����� � ������ (AsClosed = true - ��������� ���������� �������)
//���� vCPoints = nil ����� ����� �� �����������.
//����� ����� ����� �������������� �������� smGetCommonPointsLinesAndCurve
//No - ��� ����� �����;
//In - A,B ���������� � ������� (������ � ������ Interval)
//Intersec - ����� ����� ����������.
{OK}function msmGetCommonPointsLinesAndCurveEx(A,B: TMSMPoint; LineType: TLineType; Curve: TMSMPoints; AsClosed: Boolean; vCPoints: TMSMPoints): TMSMCommonPoints;

type
 TMSMAreaBorder = (//��� ������� - ���������� ������� �� �������� ����� ��������� � ������ �� ��������� ��������� ����� �����
  abSimple,//������� ������� - �� ��� �������
  abOuter,//������ ������� - �� ��� ��.��.�������.
  abOut,// ��� ������
  abInner//���������� �������
 );
function msmIsCommonPointsLinesAndArea(A,B: TMSMPoint; LineType: TLineType; Area: TMSMPoints; AreaBorder: TMSMAreaBorder): TMSMCommonPoints;

//==============================================================================
//                        ������������� �������
//==============================================================================

//����������� ��������������� ���� ��������;
//���������� �������� �������� �� ������;
//(�������� ��� msmcIsEqualRects)
{OK}function msmIsEqualRects(A1,B1,A2,B2: TMSMPoint): Boolean;

//����������� ���������� �� ������� A1B1 � A2B2 (� ����� ��������� ����);
//���������� �������� �������� �� ������;
//(�������� ��� msmcIsRectInRect)
{OK}function msmIsRectInRect(A1,B1,A2,B2: TMSMPoint): Boolean;

//����������� ������ �� ����� ����������� (*�������) ������������� ��������
//���������� �������� �������� �� ������
//(�������� ��� msmcIsIntersectRect)
{OK}function msmIsIntersectRect(A1,B1,A2,B2: TMSMPoint): Boolean;

//��������� ����� ������ ���� ������������� ��������. ����� ���� ��������� � �����
{OK}function msmGetIntersectRects(A1,B1,A2,B2,vA,vB: TMSMPoint): Boolean;

//==============================================================================
//                        ������������� �������
//==============================================================================

//�������� ������������� (�������) ����������� ������ �����
procedure msmGetExtentArea(Points: TMSMPoints; vExtent: TMSMRect);

//�������� �������� ������� ��������������
//���� >0 �� ����� ������� ������� ���� <0 �� �� �������
function msmGetAreaOf(Points: TMSMPoints): double;

//�������� �������� ������� ��������������
//���� >0 �� ����� ������� ������� ���� <0 �� �� �������
function msmGetAreaOfEx(Points: TMSMPoints): real;

//�������� ����� ������ ����
procedure msmGetWeightCenterOf(Points: TMSMPoints; vPoint: TMSMPoint);


//==============================================================================
//                                ������
//==============================================================================

//�������� ����������� ������������� ������
function msmGetDirectionOf(Points: TMSMPoints): TDirectionCurveKind;

//�������� ����������� ��������������� ������
procedure msmReDirectCurve(Points: TMSMPoints);

//��������� ������ �� ���������� (true ���� ���� �������� ������������)
function msmDirectCurveTo(Points: TMSMPoints; Direct:TDirectionCurveKind): boolean;

//{*}����� �������, ������������ Base, ����������� �� ����������
//���� Source ��������� ������ ������� ������� dist > 0 ������, dist < 0 �����
//���� Source ��������� �� ������� ������� dist < 0 ������, dist > 0 �����
//����� Source=dest
{OK}procedure msmGetParallel(Source,Dest: TMSMPoints; Dist:double; AsClosed: boolean = false);

//11.09.12

//����� �������, ������������ Source, ����������� �� ����������
//--> dist > 0 ������, dist < 0 ����� (������������ ������� ���������� ����� � Source)
//!!!�������� ���� dist = 0 �� �������� ������� ������� ��
//--> ����� Source=dest
//--> AsClosed = ������ ��������������� ��� ��������� ������
//--> DistInCaption - ���� true �� �������� ������� ������� �� ��������� ����� � Dist ������������
//procedure msmGetParallelEx(Source: TMSMPoints; AsClosed: boolean; Dest: TMSMPoints; Dist:double; SkewAngle: double = 90; DirectSourceAsWithHour = false);
procedure msmGetParallelEx(Source:TMSMPoints; AsClosed: boolean; Dest: TMSMPoints; DistValues: array of double; SkewAngle: double);

function msmIsCommonPointsCurveAndArea(Curve: TMSMPoints; AsClosed: boolean; Area: TMSMPoints; AreaBorder: TMSMAreaBorder): TMSMCommonPoints;
//==============================================================================
//                                �������
//==============================================================================

//������ ������ ����� �� ����.
//���������� ������������ ���� "a" �/� ����������
//� ���� "�"<=angle �� ����� ��-�� �������� ���� ��������� �� ������.
//���� Dest<>nil � Dest<>Source �� ��������� ������ ������� ����������
//������������� � Dest, Source �������� ��� ���������.
//��������!!! ItemCopy = true �� � Dest ��������� ����� ����� �� Source!!!!!
//��������!!! ItemCopy = false �� � Dest ��������� ������ ����� �� Source!!!!!
//��������!!! ���� �������� � �������� �� [0..360)!!!!!
//��������!!! ��������� ����� ���� ����������� � ����� (� ����� � ����� :)
//��������!!! ������� �� ������������ ����������� ����������. �.�. ����� ����������
//��������� ����� ��������� ����� ���� �� ��������������� �������. (����� ������������ � ���� while)
//���� result = true �� ������ ��� �������� � ������ �����.
function msmDoFilterPointsOnAngle(Source: TMSMPoints; ClosedShape:boolean; Angle: Double; Dest: TMSMPoints=nil; ItemCopy: boolean = true): boolean;

//������ ������ �� ��������������� ������ ��������� �����.
//���� Dest<>nil � Dest<>Source �� ��������� ������ ������� ����������
//������������� � Dest, Source �������� ��� ���������.
//��������!!! ItemCopy = true �� � Dest ��������� ����� ����� �� Source!!!!!
//��������!!! ItemCopy = false �� � Dest ��������� ������ ����� �� Source!!!!!
//���� result = true �� ������ ��� �������� � ������ �����.
//�������� ������������ ������ �������� ������ ����������� �����
function msmDoFilterPointsOnEquals(Source: TMSMPoints; Dest: TMSMPoints=nil; ItemCopy: boolean = true): boolean;

//������ ������ �� ���������� �/� �������.
//-->���� ��������� �� source[i] �� source[i+1] < Dist �� ��������� i �������.
//-->���� Dest<>nil � Dest<>Source �� ��������� ������ ������� ����������
//-->������������� � Dest, Source �������� ��� ���������.
//-->��������!!! ItemCopy = true �� � Dest ��������� ����� ����� �� Source!!!!!
//-->��������!!! ItemCopy = false �� � Dest ��������� ������ ����� �� Source!!!!!
//-->��������!!! ��������� ����� ���� ����������� � �����
//-->���� result = true �� ������ ��� �������� � ������ �����.
//-->�������� ������������ ������ �������� ��������� ����������������� �������
//-->AsLoop ����������� ������ � ���������
function msmDoFilterPointsOnDist(Source: TMSMPoints; AsLoop: boolean; Dist: Double; Dest: TMSMPoints=nil; ItemCopy: boolean = true): boolean;

//������ ������ �� �����.
//������� <= horda �� ������� �������
//���� Dest<>nil � Dest<>Source �� ��������� ������ ������� ����������
//������������� � Dest, Source �������� ��� ���������.
//��������!!! ItemCopy = true �� � Dest ��������� ����� ����� �� Source!!!!!
//��������!!! ItemCopy = false �� � Dest ��������� ������ ����� �� Source!!!!!
//��������!!! ��������� ����� ���� ����������� � �����
//���� result = true �� ������ ��� �������� � ������ �����.
//����� ���������� ��������� ��� ����� �����.
function msmDoFilterPointsOnHorda(Source: TMSMPoints; ClosedShape:boolean; Horda: Double; Dest: TMSMPoints=nil; ItemCopy: boolean = true): boolean;

function msmDoFilterOnLoops(Source: TMSMPoints; Dest: TMSMPoints=nil; ItemCopy: boolean = true): boolean;

//==============================================================================
//                                ���������
//==============================================================================

// C�������� ����� �� ����������� �������� �������
type TMSMSortPointFunction=function(P:TMSMPoint):double;
procedure msmDoSortPoints(P:TMSMPoints;SFunc:TMSMSortPointFunction);
var
 //�����, ������������ ������� ���������� ����������. �� �������� CREATE,FREE �� ����������� �������� "� ���"
 msmSortMainPoint:TMSMPoint;
//������� ��� ����������:)
function msmSortPointOnDistance(P:TMSMPoint):double; {��������� �� ����� P }
//function spAscX(P:TDPoint):double; {�� ����������� ���������� X}
//function spAscY(P:TDPoint):double; {�� ����������� ���������� Y}

implementation

//==============================================================================
//                       ��� ����������� �������������
//==============================================================================

//13.09.2012 �������� ����������� �� ������ ��������
//�������� ����� ����� �������� ����������� �� ����� �����
//vI1,vI2 <> NIL,A1,B1,A2,B2!!!!!!!
//vI1,vI2 ����������� A1,B1
//Result = true ���� ���� ����� �����
//� ������ ��������� ��������� ��������������� ��� ������������� �������
//��������� (���� ����) ������������� ������ ������������ � ���������� �� �����������
//� ����������� ������������� � ��������� ��� ���������� ���������� ������� �������������� � � A1B1
function msmGetCommonPointsIntervalsAreInLineA(A1,B1,A2,B2,vI1,vI2: TMSMPoint): boolean;
var
 _t: Double;
begin
 Result:= msmGetIntersectRects(A1,B1,A2,B2,vI1,vI2);
 if not Result then Exit;

 {if msmcIsEqual(vI1.X,vI2.X) or msmcIsEqual(vI1.Y,vI2.Y) then begin //�������� ������������ vI1,vI2 � ����� ��� �����
  if A1.IsEqual(A2.X,A2.Y) or A1.IsEqual(B2.X,B2.Y) then begin vI1.DoAssign(A1); vI2.DoAssign(A1); end
  else
  if B1.IsEqual(A2.X,A2.Y) or B1.IsEqual(B2.X,B2.Y) then begin vI1.DoAssign(B1); vI2.DoAssign(B1); end
  else Result:= false;  //�� ������...:)
 end else} begin
  if not msmcIsParallelLine(A1.X,A1.Y,B1.X,B1.Y,vI1.X, vI1.Y,vI2.X,vI2.Y) then
   if msmcIsParallelLine(A1.X,A1.Y,B1.X,B1.Y, vI2.X,vI1.Y,vI1.X,vI2.Y) then begin
    _t:= vI1.X; vI1.X:= vI2.X; vI2.X:= _t;
   end else
   if msmcIsParallelLine(A1.X,A1.Y,B1.X,B1.Y,vI1.X, vI2.Y,vI2.X,vI1.Y) then begin
    _t:= vI1.Y; vI1.Y:= vI2.Y; vI2.Y:= _t;
   end
   else Result:= false;  //�� ������...:)

  if Result then
   if a1.GetDistance(vI2)<a1.GetDistance(vI1) then begin //������������� � A1B1
    _t:= vI1.X; vI1.X:= vI2.X; vI2.X:= _t;
    _t:= vI1.Y; vI1.Y:= vI2.Y; vI2.Y:= _t;
   end;
  end;
end;

//�������� ����� ����� �������� ����������� �� ����� �����
//vI1,vI2 <> NIL,A1,B1,A2,B2!!!!!!!
//vI1,vI2 ����������� A1,B1
//Result = true ���� ���� ����� �����
//������ �������� ��������� �������� ���� (����� �������...) ��������� ��������
function msmGetCommonPointsIntervalsAreInLineB(A1,B1,A2,B2,vI1,vI2: TMSMPoint): boolean;
begin
 Result:= true;
 if msmIsPointInRect( A1,B1,A2 ) then
  if msmIsPointInRect( A1,B1,B2 ) then
   if msmIsPointInRect( A1,A2,B2 ) then begin //������������� �����������
    vI1.DoAssign(B2); vI2.DoAssign(A2);
   end else begin
    vI1.DoAssign(A2); vI2.DoAssign(B2);
    end
  else {not msmPointInRect( A1,B1,B2 )}
    if A1.IsEqualPos(A2.X,A2.Y) then
     if msmIsPointInRect(A2,B2,B1) then begin
      vI1.DoAssign(A2); vI2.DoAssign(B1);
     end else begin
      vI1.DoAssign(A1); vI2.DoAssign(A2);
     end
    else
     if msmIsPointInRect(A2,B2,A1 ) then begin
      vI1.DoAssign(A1); vI2.DoAssign(A2);
     end else begin
      vI1.DoAssign(A2); vI2.DoAssign(B1);
     end
  else {not msmPointInRect( A1,B1,A2 )}
    if msmIsPointInRect( A1,B1,B2 ) then
    if A1.IsEqualPos(B2.X,B2.Y) then
     if msmIsPointInRect(A2,B2,B1 ) then begin
      vI1.DoAssign(B2); vI2.DoAssign(B1);
     end else begin
      vI1.DoAssign(A1); vI2.DoAssign(B2);
     end
    else
     if msmIsPointInRect(A2,B2,A1 ) then begin
      vI1.DoAssign(A1); vI2.DoAssign(B2);
     end else begin
      vI1.DoAssign(B2); vI2.DoAssign(B1);
     end
   else { not msmPointInRect( A1,B1,B2 ) }
    if msmIsPointInRect(A2,B2,A1 ) then begin
     vI1.DoAssign(A1); vI2.DoAssign(B1);
     //Result := "In";
    end else
     Result:= false;
end;


//==============================================================================
//==============================================================================



{------------------------------------------------------------------------------}
function msmIsPointInLine(A,B: TMSMPoint; LineType: TLineType; P: TMSMPoint): Boolean;
begin
 Result:= false;
 case LineType of
  ltLine: Result:= msmcIsPointInLine(A.x,A.y,B.x,B.y,P.x,P.y);
  ltInterval: Result:= msmcIsPointInInterval(A.x,A.y,B.x,B.y,P.x,P.y);
 end;
end;
{------------------------------------------------------------------------------}
function msmIsPointInRect(R1,R2,P:TMSMPoint):Boolean;
begin
 Result:= msmcIsPointInRect(R1.X,R1.Y,R2.X,R2.Y,P.X,P.Y);
end;
{------------------------------------------------------------------------------}
function msmIsIntersectRect(A1,B1,A2,B2: TMSMPoint): Boolean;
begin
 Result:= msmcIsIntersectRect(A1.x,A1.y,B1.x,B1.y,A2.x,A2.y,B2.x,B2.y);
end;
{------------------------------------------------------------------------------}
function msmGetIntersectRects(A1,B1,A2,B2,vA,vB: TMSMPoint): Boolean;
var
 _Ax,_Ay,_Bx,_By: Double;
begin
 Result:= msmcgetIntersectRects(A1.X,A1.Y,B1.X,B1.Y,A2.X,A2.Y,B2.X,B2.Y,_Ax,_Ay,_Bx,_By);
 if not Result then EXIT;
 vA.X:= _Ax;
 vA.Y:= _Ay;
 vB.X:= _Bx;
 vB.Y:= _By;
end;
{------------------------------------------------------------------------------}
function msmIsRectInRect(A1,B1,A2,B2: TMSMPoint): Boolean;
begin
 Result:= msmcIsRectInRect(A1.x,A1.y,B1.x,B1.y,A2.x,A2.y,B2.x,B2.y);
end;
{------------------------------------------------------------------------------}
function msmIsEqualRects(A1,B1,A2,B2: TMSMPoint): Boolean;
begin
 Result:= msmcIsEqualRects(A1.x,A1.y,B1.x,B1.y,A2.x,A2.y,B2.x,B2.y);
end;
{------------------------------------------------------------------------------}
function msmGetAngleLine(A,B: TMSMPoint): double;
begin
 Result:= msmcGetAngleLine(A.x,A.y,B.x,B.y);
end;
{------------------------------------------------------------------------------}
function msmGetAngleBetweenLines(A1,B1,A2,B2: TMSMPoint) : double;
begin
 Result:= msmcGetAngleBetweenLines(A1.X,A1.Y,B1.X,B1.y,A2.x,A2.y,B2.x,B2.y);
end;
{------------------------------------------------------------------------------}
function msmIsParallelLine(A1,B1,A2,B2: TMSMPoint): Boolean;
begin
 Result:= msmcIsParallelLine(A1.x,A1.y,B1.x,B1.y,A2.x,A2.y,B2.x,B2.y);
end;
{------------------------------------------------------------------------------}
function msmIsCollinearlLine(A1,B1,A2,B2: TMSMPoint): Boolean;
begin
 Result:= msmcIsCollinearlLine(A1.x,A1.y,B1.x,B1.y,A2.x,A2.y,B2.x,B2.y);
end;
{------------------------------------------------------------------------------}
function msmGetCollinearlLine(A1,B1,A2,B2,vA,vB: TMSMPoint): Boolean;
var
 _Ax,_Ay,_Bx,_By: double;
begin
 _Ax:= A2.X; _Ay:= A2.y;
 _Bx:= B2.x; _By:= B2.y;
 Result:= msmcDoCheckCollinearlLine(A1.x,A1.y,B1.x,B1.y,_Ax,_Ay,_Bx,_By);
 vA.X:=_Ax; vA.Y:=_Ay;
 vB.X:=_Bx; vB.Y:=_By;
end;
{------------------------------------------------------------------------------}
procedure msmGetCentrePointLine(A,B,vC: TMSMPoint);
var
 _x,_y: double;
begin
 msmcGetCentrePointLine(A.X,A.Y,B.X,B.Y,_x,_y);
 vC.X:= _X;
 vC.Y:= _Y;
end;
{------------------------------------------------------------------------------}
function msmGetCommonPointsLines(A1,B1,A2,B2 : TMSMPoint; LineType : TLineType; vI1,vI2: TMSMPoint): boolean;
var
 _u1,_u2,_x,_y: double;
 _I1,_I2: TMSMPoint;

  function IntersectionInterval: boolean;//� ������ ���� ����������� ������� �������
  begin
   Result:= True;
   if LineType = ltLine then begin //���� ����� �� �������� ������ �������
    _I1.DoAssign(A1);
    if B2.IsEqualPos(A1)
      then _I2.DoAssign(A2)
      else _I2.DoAssign(B2);
    EXIT;
   end;
   Result:= msmGetCommonPointsIntervalsAreInLineB(A1,B1,A2,B2,_i1,_i2);
   //Result:= msmGetCommonPointsIntervalsAreInLineA(A1,B1,A2,B2,_i1,_i2); �������� ����������� �� ������ ��������
  end;

var
  _IsVertical1,_IsVertical2 : boolean;
  _Tangens1,_Tangens2 : double;
begin
  Result:= false;
  _I1:= nil;
  _I2:= nil;
  _I1:= TMSMPoint.Create(0,0);
  _I2:= TMSMPoint.Create(0,0);
  TRY
   if A1.IsEqualPos(B1.x,B1.y) then begin //���� 1 ����� ��������� � �����
    _I1.DoAssign(A1); _I2.DoAssign(_I1);
    if A2.IsEqualPos(B2.x,B2.y) then //���� 2 ����� ��������� � �����
     Result:= A2.IsEqualPos(A1.x,A1.y)
    else
     Result:= msmIsPointInLine(A2,B2,LineType,_I1); //���� 2 ����� �� ��������� � �����
    EXIT;
   end;

   if A2.IsEqualPos(B2.x,B2.y) then begin //���� 2 ����� ��������� � ����� � ��� �������� �� 1 �����
    _I1.DoAssign(A2); _I2.DoAssign(_I1);
    if msmIsPointInLine(A1,B1,LineType,_I1) then
     Result:= true;
    EXIT;
   end;

   _IsVertical1 := msmcIsEqual(A1.x,B1.x); // ����� 1 || Oy
   _IsVertical2 := msmcIsEqual(A2.x,B2.x); // ����� 2 || Oy
   if not _IsVertical1 then _Tangens1 := (B1.y-A1.y) / (B1.x-A1.x) else _Tangens1:= 0; //���������� ���� ������� 1 ������
   if not _IsVertical2 then _Tangens2 := (B2.y-A2.y) / (B2.x-A2.x) else _Tangens2:= 0; //���������� ���� ������� 2 ������


   if _IsVertical1 then // ���� ����� 1 || Oy
    if _IsVertical2 then begin // ���� ����� 2 || Oy
     Result:= msmcIsEqual(A2.x,A1.x) and IntersectionInterval;
     EXIT;
    end else begin // ���� ����� 1 || Oy � 2 ���
     _u2:= A2.y-_Tangens2*A2.x;
     _x:= A1.x;
     _y:= _Tangens2*_x+_u2;
     _I1.x:=_x; _I1.y:=_y;
     _I2.DoAssign(_I1);
     Result:= (LineType = ltLine) or msmIsPointInRect( A1,B1,_I1 ) and msmIsPointInRect( A2,B2,_I1 );
     EXIT;
    end;

   if _IsVertical2 then begin // ���� ����� 2 || Oy � 1 ���
    _u1:= A1.y-_Tangens1*A1.x;
    _x:= A2.x;
    _y:= _Tangens1*_x+_u1;
    _I1.x:= _x; _I1.y:= _y;
    _I2.DoAssign(_I1);
    Result:= (LineType = ltLine) or msmIsPointInRect( A1,B1,_I1 ) and msmIsPointInRect( A2,B2,_I1);
    EXIT;
   end;

   if msmcIsEqual(RoundTo(_Tangens1,-5),RoundTo(_Tangens2,-5)) then begin// ���� ����������� ���� ������� �����

     _u1 := A1.y-RoundTo(_Tangens1,-5)*A1.x; // y1= _Tangens1*_x+_u1
     //_u2 := A2.y-RoundTo(_Tangens2,-5)*A2.x; // y2= _Tangens2*_x+_u2
     _u2 := A2.y-RoundTo(_Tangens1,-5)*A2.x; // y2= _Tangens2*_x+_u2
    if msmcIsEqual(RoundTo(_u1,-1),RoundTo(_u2,-1)) then Result:= IntersectionInterval
   end else begin //������ ���� ����� ����� - ����� �����������
    _u1 := A1.y-_Tangens1*A1.x; // y1= _Tangens1*_x+_u1
    _u2 := A2.y-_Tangens2*A2.x; // y2= _Tangens2*_x+_u2
    _x:= (_u2-_u1) / (_Tangens1-_Tangens2);
    _y:= _Tangens1*_x+_u1;
    _I1.x:=_x; _I1.y:=_y;
    _I2.DoAssign(_I1);
    Result:= (LineType = ltLine) or msmIsPointInRect( A1,B1,_I1 ) and msmIsPointInRect( A2,B2,_I1 );
   end;

 FINALLY
  if Assigned(vI1) then vI1.DoAssign(_I1);
  if Assigned(vI2) then vI2.DoAssign(_I2);
 _I1.Free;
 _I2.Free;
 _I1:= nil;
 _I2:= nil;
 END;

end;
{------------------------------------------------------------------------------}
procedure msmGetParallel(Source,Dest: TMSMPoints; Dist:double; AsClosed: boolean);
var
 _ib,_ip:integer;
 _l,_da,_sa:double;
 _AA,_A,_B,_R1,_R2,_R3,_R4,_I1,_I2:TMSMPoint;
 _Loop:boolean;
 _BCount:integer;
 //_RCount:integer;
 _Dest: TMSMPoints;
 _Source: TMSMPoints;

 procedure AddPoint(x,y:double);
 begin
 // if (_dest.Count=0) or (not _dest.Last.IsEqual(x,y)) then
   _Dest.Add(TMSMPoint.Create(x,y))
 end;

begin
  _BCount:= Source.Count;
 if (_BCount<2) or (AsClosed and (_BCount<3)) then Exit;
 _AA:= nil;_A:= nil;_B:= nil;_R1:= nil;_R2:= nil;_R3:= nil;_R4:= nil;_I1:= nil;_I2:= nil;_Dest:= nil;

 _R1:= TMSMPoint.Create(0,0);
 _R2:= TMSMPoint.Create(0,0);
 _R3:= TMSMPoint.Create(0,0);
 _R4:= TMSMPoint.Create(0,0);
 _I1:= TMSMPoint.Create(0,0);
 _I2:= TMSMPoint.Create(0,0);
 _Dest:= TMSMPoints.Create(FALSE);
 _Source:= TMSMPoints.Create(FALSE);

 _Source.Assign(Source);


 if AsClosed and not _Source.Last.IsEqualPos(_Source.First) then
  _Source.Add(_Source.First);
 _BCount:= _Source.Count;

 TRY
  _Loop:=_Source.First.IsEqualPos(_Source.Last.X,_Source.Last.Y);
  if _Loop and (_BCount=2) then Exit;

  //_RCount:=0;
  if (not _Loop) then begin
   _sa:=0;
   _l:=Dist/Cos(_sa); if _l<0 then _sa:=-_sa;
   _da:=msmcGetAngleLine(_Source.First.X,_Source.First.Y,_Source.Items[1].x,_Source.Items[1].Y)*Pi/180+Pi/2;
   AddPoint(_Source.First.X+_l*Cos(_da+_sa),_Source.First.Y+_l*Sin(_da+_sa));
  end;

  if _Loop then _ib:=0 else _ib:=1;
   for _ip := _ib to _BCount-2 do begin

    if _ip>0 then
     _AA:=_Source.Items[_ip-1]
    else
     if _Loop then
      _AA:=_Source.Items[_BCount - 2]
     else
      _AA:=_Source.Items[_BCount-1];


    _A:=_Source.Items[_ip];
    _B:=_Source.Items[_ip+1];
    _da:=msmcGetAngleLine(_AA.X,_AA.y,_A.x,_A.y)*Pi/180+Pi/2;
    _sa:=Pi/4;
    _l:=Dist/Cos(_sa); if _l<0 then _sa:=-_sa;
    _R1.X:=(_AA.x+_l*Cos(_da+_sa));
    _R1.Y:=(_AA.y+_l*Sin(_da+_sa));
    _R2.X:=(_A.x+_l*Cos(_da-_sa));
    _R2.Y:=(_A.y+_l*Sin(_da-_sa));
    _da:=msmcGetAngleLine(_A.x,_A.y,_B.x,_B.y)*Pi/180+Pi/2;
    _R3.X:=(_A.x+_l*Cos(_da+_sa));
    _R3.Y:=(_A.y+_l*Sin(_da+_sa));
    _R4.X:=(_B.x+_l*Cos(_da-_sa));
    _R4.Y:=(_B.y+_l*Sin(_da-_sa));

    if msmGetCommonPointsLines(_R1,_R2,_R3,_R4,ltInterval,_I1,_I2) then
      AddPoint(_I1.x,_I1.y)
    else begin
      AddPoint(_R2.x,_R2.y);
      AddPoint(_R3.x,_R3.y);
    end;
   end; { for _ip }

  if (not _Loop) then begin
    _sa:=0;
    _l:=Dist/Cos(_sa); if _l<0 then _sa:=-_sa;
    _da:=msmcGetAngleLine(_Source.Items[_BCount-2].X,_Source.Items[_BCount-2].Y,_Source.Items[_BCount-1].X,_Source.Items[_BCount-1].Y)*Pi/180+Pi/2;
    AddPoint(_Source.Items[_BCount-1].X+_l*Cos(_da-_sa),_Source.Items[_BCount-1].Y+_l*Sin(_da-_sa));
  end else
   AddPoint(_Dest.First.X,_Dest.First.Y);


  //msfDeleteTabs(_Dest,_RCount,TmpP,TCount);

  if AsClosed and _dest.Last.IsEqualPos(_Dest.First) then begin
   _a:= _Dest.Extract(_Dest.Last);
   _a.Free;
  end;

  Dest.Assign(_Dest);

 FINALLY
  _R1.free; _R2.free;
  _R3.free; _R4.free;
  _I1.free; _I2.free;
  _Dest.Free; _Source.Free;
//  _R1:= nil; _R2:= nil;
//  _R3:= nil; _R4:= nil;
//  _I1:= nil; _I2:= nil;
 END;
end;
{------------------------------------------------------------------------------}
{------------------------------------------------------------------------------}
procedure msmGetParallelEx(Source:TMSMPoints; AsClosed: boolean; Dest: TMSMPoints; DistValues: array of double; SkewAngle: double);
var
 _ib,_ip:integer;
 _l,_da,_sa:double;
 _A,_B,_C,_R1,_R2,_R3,_R4,_I1,_I2:TMSMPoint;
 _tempA,_tempB: TMSMPoint;
 _Loop:boolean;
 _BCount:integer;
 _Dest: TMSMPoints;

begin
 if (Source.Count=0) or (length(DistValues)=0) then Exit;
 _A:= nil;_B:= nil;_C:= nil;

 _R1:= TMSMPoint.Create(0,0);
 _R2:= TMSMPoint.Create(0,0);
 _R3:= TMSMPoint.Create(0,0);
 _R4:= TMSMPoint.Create(0,0);
 _I1:= TMSMPoint.Create(0,0);
 _I2:= TMSMPoint.Create(0,0);
 _Dest:= TMSMPoints.Create(FALSE);

 _BCount:= Source.Count;

 //���� ������ ��������������� ��� ����������� �� ������ ������ ���������������� ������
  if not AsClosed then begin
   _tempA:= Source.GetItemByShift(0,0);
   _tempB:= Source.GetItemByShift(0,1);
    _l:=DistValues[0]/Cos(0);
   _da:=msmcGetAngleLine(_tempA.X,_tempA.Y,_tempB.x,_tempB.Y)*PiDiv180+PiDiv2;
   _Dest.Add(TMSMPoint.Create(_tempA.X+_l*Cos(_da),_tempA.y+_l*Sin(_da)));
   _ib:= 1;
  end else
   _ib:= 0;

 for _ip := _ib  to _BCount-1-_ib do begin
  // ���������� ����� ����������� ����

   _A:= Source.GetItemByShift(_ip,-1); //����������
   _B:= Source.Items[_ip]; //������� ������� ����
   _C:= Source.GetItemByShift(_ip,+1); //���������

  //������� �� 45 �� �������
  //_sa:=PiDiv4;
   _sa:= SkewAngle/2*PiDiv180;

   //���� �������������� � ������ AB
  _da:=msmcGetAngleLine(_A.X,_A.y,_B.x,_B.y)*PiDiv180+PiDiv2;
  //��������� ����������� ����

  _l:=DistValues[min(high(DistValues),_ip)]/Cos(_sa);

  if _l<0 then _sa:=-_sa;
  //���������� ������� AR1 ����������� ���� � 135 �������� � �������� AB
  _R1.X:=(_A.x+_l*Cos(_da+_sa));
  _R1.Y:=(_A.y+_l*Sin(_da+_sa));
  //���������� ������� BR2 ����������� ���� � 45 �������� � �������� AB
  _R2.X:=(_B.x+_l*Cos(_da-_sa));
  _R2.Y:=(_B.y+_l*Sin(_da-_sa));

  _da:=msmcGetAngleLine(_B.x,_B.y,_C.x,_C.y)*PiDiv180+PiDiv2;
  _R3.X:=(_B.x+_l*Cos(_da+_sa));
  _R3.Y:=(_B.y+_l*Sin(_da+_sa));
  _R4.X:=(_C.x+_l*Cos(_da-_sa));
  _R4.Y:=(_C.y+_l*Sin(_da-_sa));

  if msmGetCommonPointsLines(_R1,_R2,_R3,_R4,ltInterval,_I1,_I2) then begin
   //� ���� ������ ��������������� �������� ������� �������, ���� ����������� ����
   msmGetCentrePointLine(_I1,_I2,_i1);
   _Dest.Add(TMSMPoint.Create(_I1.x,_I1.y))
  end else begin
   // � ���� ������ ������������� �������� �������� ����.
   //�.�. ���������� ��������� ���� ���� ����� ���������� ������������ ������������ �����
   _Dest.Add(TMSMPoint.Create(_R2.x,_R2.y));
   _Dest.Add(TMSMPoint.Create(_R3.x,_R3.y));
  end;
 end; { for _ip }

 //���� ������ ��������������� ��� ����������� �� ������ ���������� ���������������� ������
 if not AsClosed then begin
  _ip:= Source.Count-1;
  _tempA:= Source.GetItemByShift(_ip,-1);
  _tempB:= Source.GetItemByShift(_ip,0);
  _l:=DistValues[min(high(DistValues),_ip)]/Cos(0);
  _da:=msmcGetAngleLine(_tempA.X,_tempA.Y,_tempB.x,_tempB.Y)*PiDiv180+PiDiv2;
  _Dest.Add(TMSMPoint.Create(_tempB.X+_l*Cos(_da),_tempB.y+_l*Sin(_da)));
 end;

 //���������� ���������
 Dest.Assign(_Dest);

 _R1.free; _R2.free;
 _R3.free; _R4.free;
 _I1.free; _I2.free;
 _Dest.Free;
end;
{------------------------------------------------------------------------------}
{OK}function msmGetDistancePointToLine(P,A,B : TMSMPoint; LineType: TLineType; var DistKind: TDistKind; vP: TMSMPoint = nil): double;
var
 _C : TMSMPoint;
 _d1,_d2 : double;
begin
 _C:= nil;
 if A.IsEqualPos(B.x,B.y) then  begin //����� �������� � �����
  Result := P.GetDistance(A.X,A.Y);
  DistKind:= dkLine;
  if Assigned(vP) then begin vP.X:= A.X; vP.Y:= A.Y; end;
  exit;
 end;
 _C:= TMSMPoint.Create(0,0);
 msmGetProjectionPointToLine(A,B,P,_C); //���� �������� ����� �� �����
 if LineType = ltLine then begin //���� ��� ����� "������"
  _d1:= P.GetDistance(_C.X,_C.Y); //���������� ���������� �� ����� �� ��������� ����� ��������
  DistKind:= dkLine;
  if Assigned(vP) then begin vP.X:= _C.X; vP.Y:= _C.Y; end;
 end else
  //���� ��� ����� �������� � ��������� ����� �������� �� ��������� � ������� ��������� ��������
  if msmIsPointInRect(A,B,_C) and not (A.IsEqualPos(_C.X,_C.Y) or b.IsEqualPos(_C.X,_C.Y)) then begin
   _d1 := P.GetDistance(_C.X,_C.Y);
   DistKind:= dkLine;
   if Assigned(vP) then begin vP.X:= _C.X; vP.Y:= _C.Y; end;
  end else begin
   _d1 := A.GetDistance(P.X,P.Y);
   _d2 := B.GetDistance(P.X,P.Y);
   DistKind:= dkPointA;
   if Assigned(vP) then begin vP.X:= A.X; vP.Y:= A.Y; end;
   if _d1 > _d2 then begin
    _d1 := _d2;
    DistKind:= dkPointB;
    if Assigned(vP) then begin vP.X:= B.X; vP.Y:= B.Y; end;
   end;
  end;
 _C.Free;
 Result := _d1;
end;
{------------------------------------------------------------------------------}
function msmIsDistanceLessThen(P1,P2 : TMSMPoint; Limit : double) : Boolean;
begin
 result:= msmcIsDistanceLessThen(p1.x,p1.y,p2.x,p2.y,Limit);
end;
{------------------------------------------------------------------------------}
function msmIsDistanceLessThenA(P1,P2 : TMSMPoint; Limit : double; var Dist: double) : Boolean;
begin
 result:= msmcIsDistanceLessThenA(p1.x,p1.y,p2.x,p2.y,Limit,Dist);
end;
{------------------------------------------------------------------------------}
procedure msmGetRotatePoint(C,P,vP:TMSMPoint; Angle: double);
var
 _x,_y: Double;
begin
 msmcGetRotatePoint(C.x,C.y,P.x,P.y,Angle,_x,_y);
 vP.X:= _x; vP.Y:= _y;
end;
{------------------------------------------------------------------------------}
function msmGetDistanceBetweenPoints(A,B:TMSMPoint):Double;
begin
 Result:= msmcGetDistanceBetweenPoints(A.X,A.Y,B.X,B.Y);
end;
{------------------------------------------------------------------------------}
procedure msmGetProjectionPointToLine(A,B,P,vP:TMSMPoint);
var
 _x,_y: Double;
begin
 msmcGetProjectionPointToLine(A.x,A.y,B.x,B.y,P.x,P.y,_x,_y);
 vP.X:= _x; vP.Y:= _y;
end;
{------------------------------------------------------------------------------}
function msmGetCommonPointsLinesAndRect(A,B: TMSMPoint; LineType : TLineType; R1,R2,vI1,vI2 :TMSMPoint): TMSMCommonPoints;
var
 _intersCount : integer;
 _Rect: TMSMRect;
 _IP: array[1..2] of TMSMPoint;
 _I1,_I2: TMSMPoint;
 _x,_y,_r: double;
 _Tangens: double;

 //(1)-(4) ������� �������� ������ �� ����������� � ������ AB.
 //
 // y^           (4)
 //  |    |---------------|R2
 //  |    |               |
 //  | (1)|     (OLO)     |(2)
 //  |    |               |
 //  |    |---------------|
 //  |   R1      (3)
 //  |______________________>x
 //

//16.02.11
begin

//����� �� ��� ��������*=????
//if not msmIsCommonPointsLinesAndRect(A,B,LineType,R1,R2) then begin
// Result:= _rcOut;
// EXIT;
//end;
  _Rect:= nil;
 _IP[1]:= nil;
 _IP[2]:= nil;
 _I1:= nil;
 _I2:= nil;

 _I1:= TMSMPoint.Create(0,0); _I2:= TMSMPoint.Create(0,0);
 _IP[1]:= TMSMPoint.Create(0,0); _IP[2]:= TMSMPoint.Create(0,0);
 _Rect:= TMSMRect.Create(R1.x,R1.y,R2.x,R2.y);
 _Rect.DoCheckOrientation;
 TRY

 if (LineType = ltInterval) and msmcIsPointInRect(R1.x,R1.y,R2.x,R2.y,A.x,A.y) and
                            msmcIsPointInRect(R1.x,R1.y,R2.x,R2.y,B.x,B.y) then begin //�������� ���������� � ��.�������
  Result:= cpIn;
  _I1.X:=(_Rect.Apoint.X); _I1.Y:=(_Rect.Apoint.Y);
  _I2.X:=(_Rect.Bpoint.X); _I2.Y:= (_Rect.Bpoint.Y);
  EXIT;
 end else

  if msmcIsEqual(A.X,B.X) then begin //���� AB || Oy
   if not(msmcIsLE(_Rect.Apoint.X,A.X) and msmcIsLE(A.X,_Rect.Bpoint.X)) then begin //���� �� ��������� ��������������
    Result := cpNo;
    EXIT;
   end;
   //������ ����������� ������� 3 � 4, ������ || oY � ��� ����� ���������� � 1 � 2
   _IP[1].X:=(A.X); _IP[1].Y:=(_Rect.Apoint.Y);
   _IP[2].X:=(A.X); _IP[2].Y:=(_Rect.Bpoint.Y);
  end else

   if msmcIsEqual(A.Y,B.Y) then begin //���� AB || Ox
    if not(msmcIsLE(_Rect.Apoint.Y,A.Y) and msmcIsLE(A.Y,_Rect.Bpoint.Y)) then begin //���� �� ��������� ��������������
     Result := cpNo;
     EXIT;
    end;
    //������ ����������� ������� 1 � 2, ������ || Ox � ��� ����� ���������� � 3 � 4
    _IP[1].X:=(_Rect.Apoint.X); _IP[1].Y:=(A.Y);
    _IP[2].X:=(_Rect.Bpoint.X); _IP[2].Y:=(A.Y);
   end else //���� �� AB || Oy � �� AB || Ox

   BEGIN //����� ���������
    _intersCount := 0;
    _Tangens  := (B.Y - A.Y) / (B.X - A.X);
    _r := A.Y - _Tangens * A.X;

    //��������� ������ ����������� � 1 �������� ������������� �������
    _y := _Tangens * _Rect.Apoint.X + _r;
    if msmcIsLE(_Rect.Apoint.Y , _y) and msmcIsLE(_y,_Rect.Bpoint.Y) then begin
     inc(_intersCount);
     _IP[_intersCount].X:=(_Rect.Apoint.X);
     _IP[_intersCount].Y:=(_y);
    end;

    //��������� ������ ����������� � 2 �������� ������������� �������
    _y := _Tangens * _Rect.Bpoint.X + _r;
    if msmcIsLE(_Rect.Apoint.Y,_y) and msmcIsLE(_y,_Rect.Bpoint.Y) then begin
     inc( _intersCount );
     _IP[_intersCount].X:=(_Rect.Bpoint.x);
     _IP[_intersCount].Y:=(_y);
    end;

    if (_intersCount < 2) then begin
     //��������� ������ ����������� � 3 �������� ������������� �������
     _x := (_Rect.Apoint.Y - _r) / _Tangens;
     if msmcIsLE(_Rect.Apoint.X , _x) and msmcIsLE(_x,_Rect.Bpoint.X) then begin
      inc( _intersCount );
      _IP[_intersCount].X:=(_x);
      _IP[_intersCount].Y:=(_Rect.Apoint.Y);
     end;
    end;

    if (_intersCount < 2) {and (A.y <> B.y)} then begin
     //��������� ������ ����������� � 4 �������� ������������� �������
     _x := (_Rect.Bpoint.y - _r) / _Tangens;
     if msmcIsLE(_Rect.Apoint.x , _x) and msmcIsLE(_x,_Rect.Bpoint.X) then begin
      inc( _intersCount );
      _IP[_intersCount].X:=(_x);
      _IP[_intersCount].Y:=(_Rect.Bpoint.Y);
     end;
    end;

    if _intersCount < 2 then begin
     Result := cpNo;
     EXIT;
    end;
   END;

 Result:= cpIntersec; //intersect
 case LineType of
  ltLine: if not msmGetCollinearlLine(A,B,_IP[1],_IP[2],_I1,_I2) then Result:= cpNo; //cpNo - �� ����� �.�. A,B,_IP[1],_IP[2] ����� �� ����� ������
  ltInterval: if not msmGetCommonPointsIntervalsAreInLineB(A,B,_IP[1],_IP[2],_i1,_i2) then Result:= cpNo
 end; {CASE}

 FINALLY
  if Assigned(vI1) then vI1.DoAssign(_I1);
  if Assigned(vI2) then vI2.DoAssign(_I2);
  _I1.Free; _I2.Free;
  _IP[1].free; _IP[2].free;
  _Rect.Free;
  _I1:= nil; _I2:= nil;
  _IP[1]:= nil; _IP[2]:= nil;
  _Rect:= nil;
 END;

end;
{------------------------------------------------------------------------------}
function msmIsCommonPointsLinesAndArea(A,B: TMSMPoint; LineType: TLineType; Area: TMSMPoints; AreaBorder: TMSMAreaBorder): TMSMCommonPoints;

function _GetKindCp(ArBr: TMSMAreaBorder; Ap,Bp: TRelationPoint; var KindCp: TMSMCommonPoints): boolean;
begin
 Result:= true;
 case ArBr of //��������� ������� ������� �������� �����������
  abOut://��� ������� (IN-OUT = intersec)
   if ((AP = TIsPoint_IN) and (BP = TIsPoint_OUT)) or ((AP = TIsPoint_OUT) and (BP = TIsPoint_IN)) then
    KindCp:= cpIntersec
   else
    if (AP = TIsPoint_OUT) or (BP = TIsPoint_OUT) then KindCp:= cpNo else
    if (AP = TIsPoint_IN) or (BP = TIsPoint_IN) then KindCp:= cpIn else
     KindCp:= cpOut;

  abInner://� ���������� �������� (IN-OUT,VERTEX,SEGMENT = intersec)
   if ( (AP = TIsPoint_IN) and ((BP = TIsPoint_OUT) or (BP = TIsPoint_VERTEX) or (BP = TIsPoint_SEGMENT)) ) or
      ( (BP = TIsPoint_IN) and ((AP = TIsPoint_OUT) or (AP = TIsPoint_VERTEX) or (AP = TIsPoint_SEGMENT)) ) then KindCp:= cpIntersec else
     if (AP = TIsPoint_IN) and (Bp = TIsPoint_IN) then KindCp:= cpIn else KindCp:= cpNo;


  abOuter://� ������� �������� (OUT-IN,VERTEX,SEGMENT = intersec)
    if ( (AP = TIsPoint_OUT) and ((BP = TIsPoint_IN) or (BP = TIsPoint_VERTEX) or (BP = TIsPoint_SEGMENT)) ) or
       ( (BP = TIsPoint_OUT) and ((AP = TIsPoint_IN) or (AP = TIsPoint_VERTEX) or (AP = TIsPoint_SEGMENT)) ) then KindCp:= cpIntersec else
      if (AP = TIsPoint_OUT) and (ap = TIsPoint_OUT) then KindCp:= cpNo else KindCp:= cpIn;

  abSimple://� "������" �������� (VERTEX,SEGMENT ��� IN-OUT = intersec)
   if (AP = TIsPoint_VERTEX) or (BP = TIsPoint_VERTEX) or (AP = TIsPoint_SEGMENT) or (BP = TIsPoint_SEGMENT) then
    KindCp:= cpIntersec
   else
    if ((AP = TIsPoint_IN) and (BP = TIsPoint_OUT)) or ((BP = TIsPoint_OUT) and (AP = TIsPoint_IN)) then
     KindCp:= cpIntersec
     else
      if (AP = TIsPoint_IN) then KindCp:= cpIn else KindCp:= cpNo;
  end;
end;

var
 _Extent: TMSMRect;
 {_WhereA,_WhereB: boolean;}
 _AInP,_BInP: TRelationPoint;

 {_Acp,_Bcp: TMSMCommonPoints;}

 _I1,_I2: TMSMPoint;
 _I: Integer;
 _ps: TMSMPoints;
 _t: TMSMPoint;
 _isIn,_isNo,_isIntersec: boolean;

begin
 _Extent:= nil; _I1:= nil; _I2:= nil; _ps:= nil; _t:=nil;

 Result:=cpNo;

 if Area.Count<3 then EXIT;

 _Extent:= TMSMRect.Create(0,0,0,0);
 _I1:= TMSMPoint.Create(0,0);
 _I2:= TMSMPoint.Create(0,0);
 _t:= TMSMPoint.Create(0,0); //������ ��������� �����
TRY

 msmGetExtentArea(Area,_Extent);
 _Extent.DoCheckOrientation;//������������
 if not msmIsCommonPointsLinesAndRect(A,B,LineType,_Extent.Apoint,_Extent.Bpoint) then EXIT; //���� ��� ����� ����� ���� � ��������� �������

 _AInP:= (msmIsPointInArea(A,Area) and $FFFF0000);// ��������� ����� �
 if A.IsEqualPos(B) then begin //�������� �� ������������� � �����
  _GetKindCp(AreaBorder,_AInP,_AInP,Result);
  EXIT;
 end;
 _BInP:= (msmIsPointInArea(B,Area) and $FFFF0000);
 _GetKindCp(AreaBorder,_AInP,_BInP,Result);

 if (Result<>cpNo) or (Result<>cpOut) then begin
  //��������� ��������� ������� ����� "�������" � �������
  msmGetCentrePointLine(A,B,_t);
  _AInP:= (msmIsPointInArea(_t,Area) and $FFFF0000);
  _GetKindCp(AreaBorder,_AInP,_AInP,Result);
 end else begin //���� ������ ������� ���������
  _ps:= TMSMPoints.Create(TRUE);
  msmGetCommonPointsLinesAndCurve(A,B,LineType,Area,true,_ps); //�������� ������ ����� ����� ����� � ������� �������.

  if _ps.Count > 0 then
  if _ps.Count = 1 then begin
   //���� ����� ����� �.�. ����� ����� ������ ������� ����� �� �����
   //--A ��� B (��� ����� �������� ����� �� ������ ������� �������)
   case LineType of
    ltLine: _t:= _ps[0].GetCopy;
    ltInterval: if a.IsEqualPos(_ps[0]) then msmGetCentrePointLine(b,_ps[0],_t) else msmGetCentrePointLine(a,_ps[0],_t);
   end;
   _AInP:= msmIsPointInArea(_t,Area) and $FFFF0000;
   _GetKindCp(AreaBorder, _AInP,_AInP,Result);
  end else begin //���� ������ ����� ����� �����
   _isIn:= false;_isNo:= false;_isIntersec:= false;
   if LineType=ltInterval then begin
    if not a.IsEqualPos(_ps.First) then _ps.Insert(0,a.GetCopy); //��� ������������� ��������� ������ �
    if not b.IsEqualPos(_ps.Last) then _ps.Add(b.GetCopy); //��� ������������� ��������� ������ �
    //..��� ���������� ����� ��������� �������� ������� A..ps[0] � B..ps[count-1] � �������
   end;
   //�������� �� ���� �������� � ��������� �� ��������� � �������
   for _I := 0 to _ps.Count - 2 do begin
    msmGetCentrePointLine(_ps[_i],_ps[_i+1],_t);
    _AInP:= (msmIsPointInArea(_t,Area) and $FFFF0000);
    _GetKindCp(AreaBorder,_AInP,_AInP,Result);
    case Result of
     cpOut:;
     cpNo:begin _isNo:= true; if _isIn then begin Result:= cpIntersec; BREAK; end end;
     cpIn:begin _isIn:= true; if _isNo then begin Result:= cpIntersec; BREAK; end end;
     cpIntersec: Break;
    end;
   end;
  end;
 end;


 FINALLY
  if (LineType=ltLine) and (Result<>cpNo) and (Result<>cpOut) then Result:= cpIntersec;
  _ps.Free;
  _t.Free;
  _Extent.Free;
  _I1.Free;
  _I2.Free;
  _Extent:= nil;
  _I1:= nil;
  _I2:= nil;
 END;

end;
{------------------------------------------------------------------------------}
function msmIsCommonPointsCurveAndArea(Curve: TMSMPoints; AsClosed: boolean; Area: TMSMPoints; AreaBorder: TMSMAreaBorder): TMSMCommonPoints;
var
 _i: Integer;
 _res: TMSMCommonPoints;
 _IsNo,_isIn,_isIntersec: Boolean;
 _ExtentC,_ExtentSh: TMSMRect;
BEGIN
 _ExtentC:= nil;
 _ExtentSh:= nil;

 Result:= cpNo;

 if AsClosed then begin
  if Curve.Count<3 then EXIT;
 end else
 if Curve.Count<2 then EXIT;

 _ExtentC:= TMSMRect.Create(0,0,0,0);
 _ExtentSh:= TMSMRect.Create(0,0,0,0);
 msmGetExtentArea(Curve,_ExtentC);
 msmGetExtentArea(Area,_ExtentSh);

 if not _ExtentC.IsIntersection(_ExtentSh) then begin
  _ExtentC.Free;
  _ExtentSh.Free;
  EXIT;
 end;
 _ExtentC.Free;
 _ExtentSh.Free;

 _IsNo:= false;
 _isIn:= false;
 _isIntersec:= false;

 for _i := 0 to Curve.Count-2 do begin
  _res:= msmIsCommonPointsLinesAndArea(Curve[_i],Curve[_i+1],ltInterval,Area,AreaBorder);
  case _res of
   cpOut:;
   cpNo: begin _IsNo:= True; if _isIn then BREAK end;
   cpIn: begin _isIn:= True; if _IsNo then BREAK end;
   cpIntersec: begin _isIntersec:= True; BREAK end;
  end;
 end;

 if not _isIntersec then
  if AsClosed and not Curve.Last.IsEqualPos(Curve.First) then begin
  _res:= msmIsCommonPointsLinesAndArea(Curve.Last,Curve.First,ltInterval,Area,AreaBorder);
  case _res of
   cpOut:;
   cpNo: _IsNo:= True;
   cpIn: _isIn:= True;
   cpIntersec: _isIntersec:= True;
  end;
 end;

 if (_isIntersec or (_isIn and _isNo))then
  Result:= cpIntersec
 else
    if _isIn then
     Result:= cpIn
    else
     Result:= cpNo;
END;
{------------------------------------------------------------------------------}
function msmGetCommonPointsLinesAndCurve(A,B: TMSMPoint; LineType: TLineType; Curve: TMSMPoints; AsClosed: Boolean; vCPoints: TMSMPoints): boolean;
var
 _i:integer;
 _I1,_I2: TMSMPoint;
 _A,_B: TMSMPoint;
 _Extent: TMSMRect;
 _ps: TMSMPoints;

 procedure _AddPoint(p: TMSMPoint);
 begin
  if _ps.Count=0 then
    _ps.Add(TMSMPoint.Create(p.X,p.Y))
  else
   if not _ps.Last.IsEqualPos(p.x,p.Y) then
    _ps.Add(TMSMPoint.Create(p.X,p.Y))
 end;

begin
 _I1:= nil;_I2:= nil;
 _A:= nil;_B:= nil;
 _Extent:= nil;
 _ps:= nil;

 Result:= false;

 if Curve.Count<2 then Exit;
 //if Curve = vCPoints then Exit;

 _a:= TMSMPoint.Create(0,0);
 _b:= TMSMPoint.Create(0,0);
 _Extent:= TMSMRect.Create(0,0,0,0);
 msmGetExtentArea(Curve,_Extent);

 TRY
  if LineType = ltLine then begin
   if msmGetCommonPointsLinesAndRect(A,B,ltLine,_Extent.Apoint,_Extent.Bpoint,_A,_B) = cpNo then EXIT;
  end else begin
   if not msmIsCommonPointsLinesAndRect(A,B,LineType,_Extent.Apoint,_Extent.Bpoint) then EXIT;
   _A.DoAssign(A);
   _B.DoAssign(B);
  end;
  _ps:= TMSMPoints.Create(TRUE);

  _I1:= TMSMPoint.Create(0,0);
  _I2:= TMSMPoint.Create(0,0);

  for _I := 0 to Curve.Count - 2 do
   if msmGetCommonPointsLines(_A,_B,Curve[_I],Curve[_I+1],ltInterval,_I1,_I2) then
    begin _AddPoint(_I1); _AddPoint(_I2); end;

  if AsClosed and (Curve.Count>2) and not (Curve.Last.IsEqualPos(Curve.First)) then
   if msmGetCommonPointsLines(_A,_B,Curve.Last,Curve.First,ltInterval,_I1,_I2) then
    begin _AddPoint(_I1); _AddPoint(_I2); end;

  msmSortMainPoint.DoAssign(A);
  msmDoSortPoints(_ps,msmSortPointOnDistance);
  _ps.OwnsObjects:= true;
  msmDoFilterPointsOnEquals(_ps);
  _ps.OwnsObjects:= false;
  Result:= _ps.Count<>0;
  if Result then vCPoints.Assign(_ps);

   _I1.Free; _I2.Free;
  FINALLY
   _a.Free; _b.Free;
   _ps.Free;
   _Extent.Free;
  END;
end;
{------------------------------------------------------------------------------}
function msmGetCommonPointsLinesAndCurveEx(A,B: TMSMPoint; LineType: TLineType;
                                           Curve: TMSMPoints; AsClosed: Boolean; vCPoints: TMSMPoints): TMSMCommonPoints;
var
 _i: integer;
 _t: TMSMPoint;
 _CommonPoints: TMSMPoints;
 _r: TRelationPoint;
begin
 _t:= nil;
 _CommonPoints:= nil;

 if (vCPoints = nil) and (LineType = ltLine) then begin
  if msmIsCommonPointsLinesAndCurve(A,B,ltLine,Curve,AsClosed) then
   Result:= cpIntersec
  else
   Result:= cpNo;
  EXIT;
 end;

 Result:= cpNo;
 _CommonPoints:= TMSMPoints.Create(TRUE);
 if msmGetCommonPointsLinesAndCurve(A,B,LineType,Curve,AsClosed,_CommonPoints) then begin
  case LineType of
   ltLine: Result:= cpIntersec;
   ltInterval: begin
    Result:= cpIn;
    _t:= TMSMPoint.Create(0,0);
    if _CommonPoints.Count = 1 then
     Result:= cpIntersec
    else
     for _I := 0 to _CommonPoints.Count - 2 do begin
      msmGetCentrePointLine(_CommonPoints[_i],_CommonPoints[_i+1],_t);
      _r:= msmIsPointInArea(_t,Curve);
      if ((_r and $FFFF0000)=TIsPoint_IN) or ((_r and $FFFF0000)=TIsPoint_OUT) then begin
       Result:= cpIntersec;
       BREAK;
      end;
     end;
    _t.Free;
   end;
  end;{case}

  if Assigned(vCPoints) then begin
   vCPoints.Assign(_CommonPoints);
   _CommonPoints.OwnsObjects:= false;
  end;

 end;
 _CommonPoints.Free;
end;
{------------------------------------------------------------------------------}
function msmIsCommonPointsLinesAndCurve(A,B: TMSMPoint; LineType: TLineType; Curve: TMSMPoints; AsClosed: Boolean): boolean;
var
 _i:integer;
 _I1,_I2: TMSMPoint;

begin
 _I1:= nil;
 _I2:= nil;

 Result:= false;
 if Curve.Count<2 then Exit;

 _I1:= TMSMPoint.Create(0,0);
 _I2:= TMSMPoint.Create(0,0);
 TRY
  case LineType of
   ltLine: begin
    for _I := 0 to Curve.Count - 2 do
     if msmGetCommonPointsLines(A,B,Curve[_I],Curve[_I+1],ltLine,_I1,_I2) and
        msmIsIntersectRect(_I1,_I2,Curve[_I],Curve[_I+1]) then
        begin Result:= True; EXIT; end;

    if AsClosed and (Curve.Count>2) and not (Curve.Last.IsEqualPos(Curve.First)) then
     if msmGetCommonPointsLines(A,B,Curve.Last,Curve.First,ltLine,_I1,_I2) and
        msmIsIntersectRect(_I1,_I2,Curve.Last,Curve.First) then
        begin Result:= True; EXIT; end;
  end; {lkLine}

  ltInterval: begin
   for _I := 0 to Curve.Count - 2 do
    if msmGetCommonPointsLines(A,B,Curve[_I],Curve[_I+1],ltInterval,_I1,_I2) then
    begin Result:= True; EXIT; end;

   if AsClosed and (Curve.Count>2) and not (Curve.Last.IsEqualPos(Curve.First)) then
    if msmGetCommonPointsLines(A,B,Curve.Last,Curve.First,ltInterval,_I1,_I2) and
       msmIsIntersectRect(_I1,_I2,Curve.Last,Curve.First) then
       begin Result:= True; EXIT; end;
  end;{lkInterval}
 end;{case}

 FINALLY
  _I1.Free; _I2.Free;
 END;

end;
{------------------------------------------------------------------------------}
function msmIsCommonPointsLinesAndRect(A,B: TMSMPoint; LineType: TLineType; R1,R2: TMSMPoint): boolean;
var
 _rect: TMSMRect;
 _I1,_I2: TMSMPoint;
 _temp: Double;
begin
 _rect:= nil;
 _I1:= nil;
 _I2:= nil;

 _I1:= TMSMPoint.Create(0,0);
 _I2:= TMSMPoint.Create(0,0);
 _rect:= TMSMRect.Create(R1.X,R1.Y,R2.X,R2.Y);
 _rect.DoCheckOrientation;
 Result:= _rect.IsContainsPoint(a); //��������� ����� A
 if not Result then Result:= _rect.IsContainsPoint(b); //��������� ����� B
 if not Result then begin
  if msmGetCommonPointsLines(a,b,_rect.Apoint,_rect.Bpoint,LineType,_I1,_I2) then //�������� �� ��������� ����� � ������� ���������� ��.�������
   //����� ����� (�����) (���������� �����������) ��� (��������� �������� I1<>I2=> ����� ���������) ��� (������������� �������� ��. ����� �����������)
   Result:= (LineType=ltInterval) {or (not _I1.IsEqual(_I2))} or msmIsIntersectRect(_I1,_I2,_rect.Apoint,_rect.Bpoint)
 end;
 if not Result then begin
  //"�������� ����. ���������"
  _temp:= _rect.Apoint.Y;
  _rect.Apoint.Y:=(_rect.Bpoint.Y);
  _rect.Bpoint.Y:=(_temp);
  if msmGetCommonPointsLines(a,b,_rect.Apoint,_rect.Bpoint,LineType,_I1,_I2) then //�������� �� ��������� ����� � ����. ���������� ��.�������
   //����� ����� (�����) (���������� �����������) ��� (��������� �������� I1<>I2=> ����� ���������) ��� (������������� �������� ��. ����� �����������)
   Result:= (LineType=ltInterval) {or (not _I1.IsEqual(_I2))} or msmIsIntersectRect(_I1,_I2,_rect.Apoint,_rect.Bpoint)
 end;
 _I1.Free;
 _I2.Free;
 _rect.Free;
 _I1:= nil;
 _I2:= nil;
 _rect:= nil;
end;
{------------------------------------------------------------------------------}
function msmIsPointInArea(Point: TMSMPoint; Area: TMSMPoints; ControlExtent: Boolean = True): TRelationPoint;
var
 _I : integer;
 _Extent: TMSMRect;
 _Points: TMSMPoints;
 _InArea: boolean;
 _InCurve: TRelationPoint;
begin
//Result:= msmIsPointInArea_fast(Point,Area,ControlExtent);

 _Extent:= nil;
 _Points:= nil;

 Result := TIsPoint_OUT;

 //������3���������������
 if Area.Count<3 then Exit;

 //��������� � ������� ������������� �������
 if ControlExtent then begin
  _Extent:= TMSMRect.Create(0,0,0,0);
  msmGetExtentArea(Area,_Extent);
  _Extent.DoCheckOrientation;
  if (_Extent.GetSize = 0) or not(_Extent.IsContainsPoint(Point.X,Point.Y)) then begin
   _Extent.Free;
   exit;
  end;
  _Extent.Free;
 end;

 TRY
 //�������� �����
 _Points:= TMSMPoints.Create(false);
 _Points.Assign(Area);

 //��� ������������� �������� ������
 if not _Points.Last.IsEqualPos(_Points.First.X,_Points.First.Y) then
  _Points.Add(_Points.First);

 //��������� ��������� � ��������
 _InCurve:= msmIsPointInCurve(Point,_Points,false,false);
 //if Boolean(_InCurve shl 24) then begin //���� ���� ���������
 if LongBool(_InCurve) then begin //���� ���� ���������
  if (_InCurve and $FFFF0000) = TIsPoint_IN then  //� �������
   Result:= TIsPoint_SEGMENT+ (_InCurve and $0000FFFF);

  if (_InCurve and $FFFF0000) = TIsPoint_VERTEX then // � �������
   Result:= TIsPoint_VERTEX+ (_InCurve and $0000FFFF);

  EXIT;
 end;

 _I := 0; _InArea:= false;
 repeat
  //if  (Point.Y > _Points.Items[_I].Y)=msmcIsLE(Point.Y,_Points.Items[_I+1].Y) then
  if  (Point.Y > _Points.Items[_I].Y)=(Point.Y<=_Points.Items[_I+1].Y) then
   if (Point.X-_Points.Items[_I].X)<( (Point.Y-_Points.Items[_I].Y) * (_Points.Items[_I+1].X - _Points.Items[_I].X) / (_Points.Items[_I+1].Y - _Points.Items[_I].Y))
   then _InArea:= not _InArea;
   inc(_I);
 until  _I=_Points.Count-1;

 if _InArea then Result:= TIsPoint_IN;

 FINALLY
  _Points.Free;
  _Points:= nil;
 END;

end;
{------------------------------------------------------------------------------}
function _msmIsPointInArea_Fast(Point: TMSMPoint; Area: TMSMPoints; ControlExtent: Boolean = True) : TRelationPoint;
var
 _I : integer;
 _Extent: TMSMRect;
 _Points: TMSMPoints;
 _InArea: boolean;
{ _InCurve: TRelationPoint; }
 _ax,_ay,_bx,_by: TValueSign;
begin
 _Extent:= nil;
 _Points:= nil;

 Result := TIsPoint_OUT;

 //������3���������������
 if Area.Count<3 then Exit;

 //��������� � ������� ������������� �������
 if ControlExtent then begin
  _Extent:= TMSMRect.Create(0,0,0,0);
  msmGetExtentArea(Area,_Extent);
  _Extent.DoCheckOrientation;
  if (_Extent.GetSize = 0) or not(_Extent.IsContainsPoint(Point.X,Point.Y)) then begin
   _Extent.Free;
   exit;
  end;
  _Extent.Free;
 end;

 TRY
 //�������� �����
 _Points:= TMSMPoints.Create(false);
 _Points.Assign(Area);

 //��� ������������� �������� ������
 if not _Points.Last.IsEqualPos(_Points.First.X,_Points.First.Y) then
  _Points.Add(_Points.First);
 {
 //��������� ��������� � ��������
 _InCurve:= msmIsPointInCurve(Point,_Points,false,false);
 //if Boolean(_InCurve shl 24) then begin //���� ���� ���������
 if LongBool(_InCurve) then begin //���� ���� ���������
  if (_InCurve and $FFFF0000) = TIsPoint_IN then  //� �������
   Result:= TIsPoint_SEGMENT+ (_InCurve and $0000FFFF);

  if (_InCurve and $FFFF0000) = TIsPoint_VERTEX then // � �������
   Result:= TIsPoint_VERTEX+ (_InCurve and $0000FFFF);

  EXIT;
 end;
 }
 _I := 0; _InArea:= false;
 repeat
  _ax:= Sign(_Points.Items[_i].X-Point.X);
  _ay:= Sign(_Points.Items[_i].Y-Point.Y);
  inc(_i);
  _bx:= Sign(_Points.Items[_i].X-Point.X);
  _by:= Sign(_Points.Items[_i].Y-Point.Y);

  if (_ay=_by) or (_ax+_bx<0) then CONTINUE;



  _InArea:= not _InArea;

 until  _I=_Points.Count-1;

 if _InArea then Result:= TIsPoint_IN;

 FINALLY
  _Points.Free;
  _Points:= nil;
 END;

end;
{------------------------------------------------------------------------------}
function msmIsPointInCurve(Point: TMSMPoint; Curve: TMSMPoints; AsClosed: boolean; ControlExtent: Boolean = True): TRelationPoint;
var
 _I : integer;
 _Extent: TMSMRect;
begin
 _Extent:= nil;
 Result := TIsPoint_OUT;

 if Curve.Count<2 then Exit;

 //��������� � �������
 if ControlExtent then begin
  _Extent:= TMSMRect.Create(0,0,0,0);
  msmGetExtentArea(Curve,_Extent);
  _Extent.DoCheckOrientation;
  if (_Extent.GetSize = 0) or not(_Extent.IsContainsPoint(Point.X,Point.Y)) then begin
   _Extent.Free;
   exit;
  end;
  _Extent.Free;
 end;

 //�������� ��������� � �������� ������
 for _I := 0 to Curve.Count-2 do
  if msmcIsPointInInterval(Curve[_i].X,Curve[_i].y,Curve[_i+1].X,Curve[_i+1].y,Point.X,Point.y) then begin
   if msmcIsEqualP(Curve[_i].X,Curve[_i].y,Point.X,Point.y) then
    Result:= TIsPoint_VERTEX+ TRelationPoint(_I)
   else
    if msmcIsEqualP(Curve[_i+1].X,Curve[_i+1].y,Point.X,Point.y) then
     Result:= TIsPoint_VERTEX+TRelationPoint(_I+1)
    else
     Result:= TIsPoint_IN+TRelationPoint(_I);
   EXIT;
  end;

 //�������� ��������� � ���������� ������� ������
 if AsClosed and (Curve.Count>2) and not (Curve.Last.IsEqualPos(Curve.First)) then
  if msmcIsPointInInterval(Curve.Last.X,Curve.Last.y,Curve.First.X,Curve.First.y,Point.X,Point.y) then
   if msmcIsEqualP(Curve.Last.X,Curve.Last.y,Point.X,Point.y) then
    Result:= TIsPoint_VERTEX+TRelationPoint(Curve.Count-1)
   else
    if msmcIsEqualP(Curve.First.X,Curve.First.y,Point.X,Point.y) then
     Result:= TIsPoint_VERTEX+TRelationPoint(1)
    else
     Result:= TIsPoint_IN+TRelationPoint(1);

end;
{------------------------------------------------------------------------------}
procedure msmGetExtentArea(Points: TMSMPoints; vExtent: TMSMRect);
var
 i: Integer;
 MinPoint,MaxPoint: TMSMPoint;
begin
 MinPoint:= nil;
 MaxPoint:= nil;
 if Points.Count<>0 then begin
   MinPoint:= vExtent.Apoint;
   MaxPoint:= vExtent.Bpoint;
   MinPoint.DoAssign(Points.Items[0]);
   MaxPoint.DoAssign(Points.Items[0]);
   for I := 0 to Points.Count-1 do begin
    if MinPoint.x > Points.Items[i].x then MinPoint.X:=(Points.Items[i].X);
    if MinPoint.y > Points.Items[i].y then MinPoint.Y:=(Points.Items[i].Y);
    if MaxPoint.x < Points.Items[i].x then MaxPoint.X:=(Points.Items[i].X);
    if MaxPoint.y < Points.Items[i].y then MaxPoint.Y:=(Points.Items[i].Y);
   end;
  end;
end;
{------------------------------------------------------------------------------}
function msmGetAreaOf(Points: TMSMPoints) : double;
var
 _s : double;
 _i : integer;
begin
 Result:= 0.0;
 if Points.Count<3 then exit;
 _s := 0.0;
 for _i := 0 to Points.Count-2 do
  _s := _s + 0.5*( Points.Items[_i+1].X-Points.Items[_i].X)*( Points.Items[_i+1].Y+Points.Items[_i].Y);
 result := _s + 0.5*( Points.First.X-Points.Last.X)*( Points.First.Y+Points.Last.Y);
end;
{------------------------------------------------------------------------------}
function msmGetAreaOfEx(Points: TMSMPoints): real;
var
 _s : Real;
 _i : integer;
begin
 Result:= 0.0;
 if Points.Count<3 then exit;
 _s := 0.0;
 for _i := 0 to Points.Count-2 do
  _s := _s + 0.5*( Points.Items[_i+1].X-Points.Items[_i].X)*( Points.Items[_i+1].Y+Points.Items[_i].Y);
 result := _s + 0.5*( Points.First.X-Points.Last.X)*( Points.First.Y+Points.Last.Y);
end;
{------------------------------------------------------------------------------}
function msmGetDirectionOf(Points: TMSMPoints): TDirectionCurveKind;
begin
 if msmGetAreaOf(Points) < 0 then
  Result:= dcWithHourArrow
 else
  result:= dcWithOutHourArrow;
end;
{------------------------------------------------------------------------------}
procedure msmReDirectCurve(Points: TMSMPoints);
var
 _i: integer;
begin
 if Points.Count<3 then exit; //������ 3 ����� � 2, ������ ����� �������� �� �������� ����....
 for _i := 0 to (Points.Count div 2)-1 do
  Points.Exchange(_i,Points.Count-1-_i);
end;
{------------------------------------------------------------------------------}
function msmDirectCurveTo(Points: TMSMPoints; Direct:TDirectionCurveKind): boolean;
begin
 result:= false;
 if Points.Count<3 then exit;
 if msmGetDirectionOf(Points) <> Direct then begin
  msmReDirectCurve(Points);
  Result:= true;
 end;
end;
{------------------------------------------------------------------------------}
procedure msmGetWeightCenterOf(Points: TMSMPoints; vPoint: TMSMPoint);
var
 xs,ys: double;
 i: integer;
begin
 xs := 0.0; ys := 0.0;
 if Points.Count =0 then exit;
 for i := 0 to Points.Count-1 do begin
  xs := xs + Points.Items[i].X;
  ys := ys + Points.Items[i].Y;
 end;
 vPoint.X:=(xs/Points.Count);
 vPoint.Y:=(ys/Points.Count);
end;
{------------------------------------------------------------------------------}

//==============================================================================
//                                �������
//==============================================================================

function msmDoFilterPointsOnAngle(Source: TMSMPoints; ClosedShape:boolean; Angle: Double; Dest: TMSMPoints=nil; ItemCopy: boolean = true): boolean;
var
 _ControlSL: boolean;
 _Angle: Double;
 _i: integer;
 _temp: TMSMPoints;

 //�������� �������������� �������
 function _notValid : boolean;
  begin
   Result := msmcIsLE(abs(_Angle),Angle) or msmcIsLE(abs(180 - _Angle),Angle);
  end;

begin
 _temp:= nil;
 Result:= false;

 if (Source=nil) or (Source.Count<3) then EXIT;

 _ControlSL:= (Source=dest) or (Dest=nil);
 if _ControlSL then
  _temp:= Source
 else begin
  _temp:= TMSMPoints.Create(false);
  _temp.Assign(Source);
 end;

 _i:= 1;
 while _i<_temp.Count-1 do begin
  _Angle:= msmGetAngleBetweenLines(_temp.Items[_i],_temp.Items[_i-1],_temp.Items[_i],_temp.Items[_i+1]);
  if _notValid then begin
   _temp.Delete(_i);
   Result:= true;
  end else
   inc(_i);
 end;

 if ClosedShape and (_temp.Count>3) then begin
  _Angle:= msmGetAngleBetweenLines(_temp.Items[0],_temp.Items[1],_temp.Items[0],_temp.Items[_temp.Count-1]);
  if _notValid then begin
   //_temp.Delete(_temp.Count-1);
   _temp.Delete(0);//������, ��� ������� ������� ����
   Result:= true;
  end
 end;

 if Result then //��� �������� "�������" ������ ����� ������������ ������ �����������
   msmDoFilterPointsOnEquals(_temp);

 if not _ControlSL then begin
  if ItemCopy then begin
   dest.Clear;
   for _i := 0 to _temp.Count-1 do
    dest.Add(_temp.Items[_i].GetCopy);
  end else
   dest.Assign(_temp);
  _temp.Free;
 end;

end;
{------------------------------------------------------------------------------}
function msmDoFilterPointsOnEquals(Source: TMSMPoints; Dest: TMSMPoints=nil; ItemCopy: boolean = true): boolean;
var
 _ControlSL: boolean;
 _i: integer;
 _temp: TMSMPoints;
begin
 _temp:= nil;
  Result:= false;
 if (Source=nil) or (Source.Count<2) then EXIT;

 _ControlSL:= (Source=dest) or (Dest=nil); //���� ������ � �������� �������
 if _ControlSL then
  _temp:= Source
 else begin
  _temp:= TMSMPoints.Create(false);
  _temp.Assign(Source);
 end;

 _i:= 0;
 while _i<_temp.Count-1 do //�������� �������� ��������������� ������ ��������� �����
  if _temp.Items[_i].IsEqualPos(_temp.Items[_i+1]) then begin
   _temp.Delete(_i);
   Result:= true;
  end else
   inc(_i);

 if not _ControlSL then begin
  if ItemCopy then begin
   dest.Clear;
   for _i := 0 to _temp.Count-1 do
    dest.Add(_temp.Items[_i].GetCopy);
  end else
   dest.Assign(_temp);

 _temp.Free;
 end;

end;
{------------------------------------------------------------------------------}
function msmDoFilterPointsOnDist(Source: TMSMPoints; AsLoop: boolean; Dist: Double; Dest: TMSMPoints=nil; ItemCopy: boolean = true): boolean;
var
 _ControlSL: boolean;
 _i: integer;
 _temp: TMSMPoints;
begin
 _temp:= nil;
  Result:= false;
 if (Source=nil) or (Source.Count<2) then EXIT;

 _ControlSL:= (Source=dest) or (Dest=nil); //���� ������ � �������� �������
 if _ControlSL then
  _temp:= Source
 else begin
  _temp:= TMSMPoints.Create(false);
  _temp.Assign(Source);
 end;

 _i:= 0;
 while _i<_temp.Count-1 do
  if msmIsDistanceLessThen(_temp.Items[_i],_temp.Items[_i+1],Dist) then begin
   //_temp.Delete(_i);
   _temp.Delete(_i+1);
   Result:= true;
  end else
   inc(_i);

  if AsLoop then
   if msmIsDistanceLessThen(_temp.First,_temp.Last,dist) then begin
     _temp.Delete(_temp.Count-1);
     Result:= true;
   end;

 //if Result then
  //msmDoFilterPointsOnEquals(_temp,ClosedShape); �� ���� ����������� ����� �������� �� ���� ��������� :)

 if not _ControlSL then begin
  if ItemCopy then begin
   dest.Clear;
   for _i := 0 to _temp.Count-1 do begin
    dest.Add(_temp.Items[_i].GetCopy);
    Dest[_i].Caption:= _temp.Items[_i].Caption;
   end;
  end else
   dest.Assign(_temp);
  _temp.Free;
 end;

end;
{------------------------------------------------------------------------------}
function msmDoFilterPointsOnHorda(Source: TMSMPoints;ClosedShape:boolean; Horda: Double; Dest: TMSMPoints=nil; ItemCopy: boolean = true): boolean;
var
 _ControlSL: boolean;
 _i: integer;
 _temp: TMSMPoints;
 _horda: Double;
 _distkind: TDistKind;
begin
 _temp:= nil;
  Result:= false;
 if (Source=nil) or (Source.Count<3) then EXIT;

_ControlSL:= (Source=dest) or (Dest=nil); //���� ������ � �������� �������
 if _ControlSL then
  _temp:= Source
 else begin
  _temp:= TMSMPoints.Create(false);
  _temp.Assign(Source);
 end;

 _i:= 1;
 while _i<_temp.Count-1 do begin
   _horda:= msmGetDistancePointToLine(_temp.Items[_i],_temp.Items[_i-1],_temp.Items[_i+1],ltLine,_distkind);
  if msmcIsLE(_horda,Horda) then begin
   _temp.Delete(_i);
   Result:= true;
  end else
   inc(_i);
 end;

if ClosedShape then
 while _temp.Count>3 do begin
  _horda:= msmGetDistancePointToLine(_temp.Items[_temp.Count-1],_temp.Items[0],_temp.Items[_temp.Count-2],ltLine,_distkind);
  if msmcIsLE(_horda,Horda) then begin
   _temp.Delete(_temp.Count-1);
   Result:= true;
  end else
   break;
 end;

 if Result then
  msmDoFilterPointsOnEquals(_temp);

 if not _ControlSL then begin
  if ItemCopy then begin
   dest.Clear;
   for _i := 0 to _temp.Count-1 do
    dest.Add(_temp.Items[_i].GetCopy);
  end else
   dest.Assign(_temp);
  _temp.Free;
 end;

end;
{------------------------------------------------------------------------------}
function msmDoFilterOnLoops(Source: TMSMPoints; Dest: TMSMPoints=nil; ItemCopy: boolean = true): boolean;
var
 _ControlSL: boolean;
 _i,_j,_k: integer;
 _temp: TMSMPoints;
 _I1,_I2: TMSMPoint;
 _OnLine: boolean;
begin
 _temp:= nil;
  Result:= false;
 if (Source=nil) or (Source.Count<2) then EXIT;

 _ControlSL:= (Source=dest) or (Dest=nil); //���� ������ � �������� �������
 if _ControlSL then
  _temp:= Source
 else begin
  _temp:= TMSMPoints.Create(false);
  _temp.Assign(Source);
 end;

 Result:= msmDoFilterPointsOnEquals(_temp);
 if _temp.Count < 3 then exit;
 _i:= 0;
 _I1:= TMSMPoint.Create(0,0);
 _I2:= TMSMPoint.Create(0,0);
 while _i < _temp.Count do begin
  _j := _i + 1;
  while (_j < _temp.Count-1) do
   if msmGetCommonPointsLines(_temp[_i],_temp[_i + 1], _temp[_j],_temp[_j + 1],ltInterval,_I1,_I2 ) and
      not ( _I1.IsEqualPos(_I2) and ((_j = (_i + 1)) or _I1.IsEqualPos(_temp.Last){_temp.first}) )then begin
    _OnLine := not _I1.IsEqualPos(_temp[_i + 1]) and not _I2.IsEqualPos(_temp[_i + 1]);
    if _OnLine then begin
     inc(_i);
     _temp[_i].DoAssign(_I2);
    end;
    for _K := 1 to _j - _i do
     _temp.Delete(_j + 1);
    if _OnLine then dec(_i);
    _j := _i + 1;
   end else inc(_j);
    inc(_i);
  end; { while i < AllPts -1 }
 Result:= msmDoFilterPointsOnEquals(_temp);


 if not _ControlSL then begin
  if ItemCopy then begin
   dest.Clear;
   for _i := 0 to _temp.Count-1 do
    dest.Add(_temp.Items[_i].GetCopy);
  end else
   dest.Assign(_temp);

 _temp.Free;
 end;

end;
{------------------------------------------------------------------------------}


function msmSortPointOnDistance(P:TMSMPoint):double;
begin
 Result:= msmGetDistanceBetweenPoints(msmSortMainPoint,p);
end;

procedure msmDoSortPoints(P:TMSMPoints;SFunc:TMSMSortPointFunction);

 procedure _Sort(L,R: Integer);
  var
   _I,_J: Integer;
   _T: TMSMPoint;
  begin
   _T:= nil;
    repeat
      _I := L; _J := R;
      _T := P[(L + R) shr 1];
      repeat
        while SFunc(P[_I])<SFunc(_T) do Inc(_I);
        while SFunc(P[_J])>SFunc(_T) do Dec(_J);
        if _I <= _J then begin P.Exchange(_I, _J); Inc(_I); Dec(_J); end;
      until _I > _J;
      if L < _J then _Sort(L,_J);
      L := _I;
    until _I >= R;
  end;
begin
 if P.Count>1 then
  _Sort(0,P.Count-1);
end;

initialization
  msmSortMainPoint:= TMSMPoint.Create(0,0);
finalization
  msmSortMainPoint.Free;
end.
