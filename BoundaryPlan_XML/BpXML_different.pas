unit BpXML_different;

interface

uses

  Windows, Classes, Controls, XMLDoc, XMLIntf, SysUtils, std_mp,
  IOUtils, Dialogs,ShellAPI, Clipbrd,System.IniFiles,
  STDCtrls,

  Forms, fmSelectionList,unRussLang,RegularExpressions;

type
///
///  typ� MP - �����������
///
TTypeMP = (tpNewParcel,      {�����������}
           tpExistParcel,    {���������}
           tpExistEZParcel,  {��������� ��}
           tpNewSubParcel);  {����������� ������}
///
///  ��� ��������� ���������
///
TTypePackage = (tsNewParcel,tsChangeParcel,tsFPSpecifyRelatedParcel,             {�����������}
                tsExistParcel, tsSPpecifyRelatedParcel,                          {���������}
                tsExistEZParcels,tsExistEZEntryParcels,tsSPESpecifyRelatedParcel,{��}
                tsClients,tsContractor,tsDocumentation,tsConclusion,tsInputDate, {GeneralInforamtion}
                tsNewSubParcel);                                                 {NewSubparcel }
///
///{ ��� ������� ����� ������������ ������ }
///
TAppliedFileDirect = class (TIniFile)
  private
    class var clAppliedFileDirect: TAppliedFileDirect;
  public
    destructor Destroy; override;
    function  ReadSectionOpenIni(section,Ident : string)  : string;
    procedure WriteSectionOpenIni(section,Ident,value : string);
    function  DeleteKeyIni(section,ident : string): Boolean;
    class function AppliedFileDirect:TAppliedFileDirect; static;
end;

///
///{ �������� ����������  ���  ��������  �������� ���������� }
///
TSettingPathFile = class
  private
     class var  FTempFileName :  string;
  protected
     class procedure CreateTempPath; static;
  public
     class function  _TempPath: String;
     class procedure DeleteTempPath; static;
     class function  MakeTempPath : string; static;
end;

///
///  // ������� ������������� ������ ������� xsd
///
TBpXml_DictionaryItem = class
private
  FCode: string;
  FNote: string;
public
  property Code: string read FCode write FCode;
  property Note: string read FNote write FNote;
  function IsEqualCode(aCode: string): Boolean;
  function IsEqualNote(aNote: string): Boolean;
end;

///
/// ��������� �������
///
TSTD_MP_ListofIndexGenerate = class
   private
     class  procedure CompleteListType_SpecifyRelatedParcel(list : TStringList);
     class procedure CompleteListType_Client(list : TStringList);
     class procedure CompleteListMethodFormParcels(list : TStringList);
   public
     { �������� ������� Dest }
     class function BpXml_LoadDictionary(PathFile: string; Dest: TStrings): Integer; static;
     class function BpXml_FindItemByCode(ListItems: TStrings; aCode: string): Integer; static;
     class function BpXml_FindItemByNote(ListItems: TStrings; aNote: string): Integer; static;

     class function  IndexOfSelect_TypeSpecifyRelated   : Integer; static;
     class function  IndexOfSelect_TypeClient   : Integer;static;
     class function  IndexOfSelect_MethodFormParcels : integer; static;
     class function  IndexOfSelect_TypeSubParcel : Integer; static;
end;

TAttribute_SpelementUnit = record
  Type_Unit: string;
  Su_Nmb: Integer;
end;
TAttribute_Ordinate = record
  Ord_Nmb: Integer;
  Num_Geopoint: Integer;
  Delta_Geopoint: string;
  Point_Pref: string;
end;

///
///  <������>
///
function  GetClipBoardList(rgxStr : string; const  strText : string ='') : TStringList;    { ������ ������  }
function  CopyFileName(currentDir, fileName, targetDir: string) : Boolean;{����������� �����}
function  ClearDir(Dir: string ): boolean;
///
///  <Settings Proxy
///
procedure GetSettingsProxy(var Host : string; var Port : integer);

//TODO:
function GetSpecialFolderPath(folder: integer): string;
function FullRemoveDir(Dir: string; DeleteAllFilesAndFolders, StopIfNotAllDeleted, RemoveRoot: Boolean): Boolean;
Function MyRemoveDir(sDir : String) : Boolean;
function GetComputerNetName: string;

implementation

uses
  MSMCommon_, MSMath_,  System.Win.Registry, Winapi.SHFolder;



function GetComputerNetName: string;
var
  buffer: array[0..255] of char;
  size: dword;
begin
  size := 256;
  if GetComputerName(buffer, size) then
    Result := buffer
  else
    Result := ''
end;

Function MyRemoveDir(sDir : String) : Boolean;
 var
   iIndex: Integer;
   SearchRec: TSearchRec;
   sFileName: string;
 begin
   Result := False;
   sDir := sDir + '\*.*';
   iIndex := FindFirst(sDir, faAnyFile, SearchRec);

   while iIndex = 0 do
   begin
     sFileName := ExtractFileDir(sDir)+'\'+SearchRec.name;
     if SearchRec.Attr = faDirectory then
     begin
       if (SearchRec.name = '' ) and (SearchRec.name = '.') and
       (SearchRec.name = '..') then
         MyRemoveDir(sFileName);
     end
     else
     begin
       if SearchRec.Attr <> faArchive then
         FileSetAttr(sFileName, faArchive);
       if not DeleteFile(sFileName) then
         ShowMessage('Could NOT delete ' + sFileName);
     end;
     iIndex := FindNext(SearchRec);
   end;

   FindClose(SearchRec);

   Result := RemoveDir(ExtractFileDir(sDir));
end;

function FullRemoveDir(Dir: string; DeleteAllFilesAndFolders, StopIfNotAllDeleted, RemoveRoot: Boolean): Boolean;
{----------------------------------------------------------}
{---  �������� �� ������� �������� �� ����� ����������  ---}
{----------------------------------------------------------}
{| DeleteAllFilesAndFolders - ��������, ������������ ���- |}
{| ���� �� ��� �������� ������� ��������                  |}
{| StopIfNotAllDeleted - ��������, ����������� ���������  |}
{| �� ���������� ������� �������� ���� ���� �� ������ ���-|}
{| ���� ������                                            |}
{| RemoveRoot - ��������, ������������ �������� ��������� |}
{| ��������                                               |}
{----------------------------------------------------------}
var
  i: integer;
  SRec: TSearchRec;
  FN: string;
begin
  Result:=False;
  if not DirectoryExists(Dir) then exit;
  Result:=True;
  // ��������� ���� � ����� � ������ ����� - "��� ����� � ����������"
  Dir:=IncludeTrailingPathDelimiter(Dir);
  i:=FindFirst(Dir+'*', faAnyFile, SRec);
  try
    while i = 0 do
    begin
      // �������� ������ ���� � ����� ��� ����������
      FN:=Dir+SRec.Name;
      // ���� ��� ����������
      if SRec.Attr = faDirectory then
      begin
        // ����������� ����� ���� �� ������� � ������ �������� �����
        if (SRec.Name <> '') and (SRec.Name <> '.') and (SRec.Name <> '..') then
        begin
          if DeleteAllFilesAndFolders then FileSetAttr(FN, faArchive);
          Result:=FullRemoveDir(FN, DeleteAllFilesAndFolders, StopIfNotAllDeleted, True);
          if not Result and StopIfNotAllDeleted then exit
        end;
      end else // ����� ������� ����
      begin
        if DeleteAllFilesAndFolders then FileSetAttr(FN, faArchive);
        Result:=SysUtils.DeleteFile(FN);
        if not Result and StopIfNotAllDeleted then exit
      end;
      // ����� ��������� ���� ��� ����������
      i:=FindNext(SRec)
    end;
  finally
    SysUtils.FindClose(SRec)
  end;
  if not Result then exit;
  if RemoveRoot then // ���� ���������� ������� ������ - �������
    if not RemoveDir(Dir) then Result:=false
end;


function GetSpecialFolderPath(folder: integer): string;
const
      SHGFP_TYPE_CURRENT = 0;
var
  path: array [0..MAX_PATH] of char;
begin
  if SUCCEEDED(SHGetFolderPath(0,folder,0,SHGFP_TYPE_CURRENT,@path[0])) then
    Result := path
  else
    Result := '';
end;
{$REGION '�������� ����������'}
/// �������� 
function ClearDir(Dir: string ): boolean;
var
  isFound: boolean;
  sRec: TSearchRec;
begin
 try
   if not DirectoryExists(dir) then  Exit;

   Result := false;
   ChDir( Dir );
   if IOResult <> 0 then
   begin
      ShowMessage( '�� ���� ����� � �������: ' + Dir );
      Exit;
   end;
   if Dir[Length(Dir)] <> '\' then Dir := Dir + '\';
   isFound := FindFirst( Dir + '*.*', faAnyFile, sRec ) = 0;
   try
       while isFound do
       begin
       if ( sRec.Name <> '.' ) and ( sRec.Name <> '..' ) then
          if ( sRec.Attr and faDirectory ) = faDirectory then
          begin
             if not ClearDir( Dir + sRec.Name ) then Exit;
             if ( sRec.Name <> '.' ) and ( sRec.Name <> '..' ) then
                if ( Dir + sRec.Name ) <> Dir then
                begin
                   ChDir( '..' );
                   RmDir( Dir + sRec.Name );
                end;
          end
          else
             if not DeleteFile( Dir + sRec.Name ) then
             begin
                ShowMessage( '�� ���� ������� ����: ' + sRec.Name );
                Exit;
             end;
          isFound := FindNext( sRec ) = 0;
          Application.ProcessMessages;
       end;
   except on E : Exception do  
        raise Exception.Create(E.Message);
   end;
 finally
   FindClose( sRec );
   Result := IOResult = 0;
 end;
end;
{$ENDREGION}

///
///<param currentDir = "������"
///       fileName   = "���"
///       targetDir  = "����"> </param>
///
function  CopyFileName(currentDir, fileName, targetDir: string) : Boolean;
begin
  if not DirectoryExists(targetDir) then    ForceDirectories(targetDir);
  Result :=  CopyFile(PChar(currentDir), PChar(targetDir + '\' + fileName), true);
end;


/// <param   rgxStr = '���������� ��������'>
///          strText= '('' - �� ������ ) or '����� ��� �������''
/// </param>
function GetClipBoardList(rgxStr : string ;const strText : string ='') : TStringList;
var
  ClipbrdData: TStringList;
  i, j, RowCnt: Integer;
  key : string;
  Matchs: TMatchCollection;
  Match: TMatch;
  RegX : TRegEx;
begin
  if not Assigned(Result) then Result := TStringList.Create
  else Result.Clear;
  if not Clipboard.HasFormat(CF_TEXT) then Exit;
  ClipbrdData := TStringList.Create;
  RegX := TRegEx.Create(rgxStr,[roIgnoreCase,roMultiline]);
  try
    Clipboard.Open;
    { ���� ��  ������ ������ }
    if strText = '' then  ClipbrdData.Text := Clipboard.AsText
    else ClipbrdData.Text := strText;

    RowCnt := ClipbrdData.Count;
    for i := 0 to RowCnt - 1 do
    begin
      Matchs := Regx.Matches(ClipbrdData.Strings[I]);
      if  Matchs.Count = 0 then  Continue;
      for j := 0 to Matchs.Count -1 do
      begin
          Match := Matchs.Item[j];
          key  := Match.Groups.Item[0].Value;
          Result.Add(key);
      end;
    end;
  finally
    Clipboard.Close;
    FreeAndNil(ClipbrdData);
  end;
end;


{$REGION ' ��������� �������'}

 /// <param list="������ ��� ����������  ��  �����������">
 /// </param>
class procedure TSTD_MP_ListofIndexGenerate.CompleteListMethodFormParcels(list : TStringList);
begin
   BpXml_LoadDictionary(STD_MP_WorkDir+cnstdMethod,list);
end;

 /// <param > /// </param>
class function TSTD_MP_ListofIndexGenerate.IndexOfSelect_MethodFormParcels : integer;
var listType : TStringList;
begin
  try
    listType := TStringList.Create;
    CompleteListMethodFormParcels(listType);
    Result   := fmSelectionList.frmSelectionList.ListValueShow(rsNameMethodFormed,listType);
  finally
    FreeAndNil(listType);
  end;
end;
class function TSTD_MP_ListofIndexGenerate.IndexOfSelect_TypeSubParcel : integer;
var listType : TStringList;
begin
  try
    listType := TStringList.Create;
    CompleteListMethodFormParcels(listType);
    Result   := fmSelectionList.frmSelectionList.ListValueShow(rsNameMethodFormed,listType);
  finally
    FreeAndNil(listType);
  end;
end;


 /// <param > /// </param>
class function TSTD_MP_ListofIndexGenerate.IndexOfSelect_TypeSpecifyRelated   : Integer;
var listType : TStringList;
begin
  listType := TStringList.Create;
  try
    CompleteListType_SpecifyRelatedParcel(listType);
    Result   := fmSelectionList.frmSelectionList.ListValueShow(rsNameTypeSpecifyRelatedParcel,listType);
  finally
    FreeAndNil(listType);
  end;
end;

 /// <param list= '������ ��� ���������� SpecifyRelatedParcel' >
 /// </param>
class procedure TSTD_MP_ListofIndexGenerate.CompleteListType_SpecifyRelatedParcel(list : TStringList);
begin
   list.Add(rsAltAllBorder);
   list.Add(rsAltChangeBorder);
   list.Add(rsAltContours);
   list.Add(rsAltDeleteAllBorder);
end;

 /// <param list= '������ ��� ���������� Client' >
 /// </param>
class procedure TSTD_MP_ListofIndexGenerate.CompleteListType_Client(list : TStringList);
begin
   list.Add(rsAltPerson);
   list.Add(rsAltOrganization);
   list.Add(rsAltForeign_Organization);
   list.Add(rsAltGovernance);
end;

 /// <param > /// </param>
class function  TSTD_MP_ListofIndexGenerate.IndexOfSelect_TypeClient   : Integer;
var listType : TStringList;
begin
  listType := TStringList.Create;
  try
    CompleteListType_Client(listType);
    Result   := fmSelectionList.frmSelectionList.ListValueShow(rsNameTypeClients,listType);
  finally
    FreeAndNil(listType);
  end;
end;

class function TSTD_MP_ListofIndexGenerate.BpXml_LoadDictionary(PathFile: string; Dest: TStrings): Integer;
var
  _i: Integer;
  _Doc: IXMLDocument;
  _Node, _Enum: IXMLNode;
  _DicItem: TBpXml_DictionaryItem;
begin
  Result := 0;
  _Doc := TXMLDocument.Create(nil);
  _Doc.fileName := PathFile;
  _Doc.Active := true;
  _Node := _Doc.DocumentElement.ChildNodes.Nodes['simpleType'];
  _Node := _Node.ChildNodes.Nodes['restriction'];
  for _i := 0 to _Node.ChildNodes.Count - 1 do
  begin
    _DicItem := TBpXml_DictionaryItem.Create;
    _Enum := _Node.ChildNodes.Get(_i);
    _DicItem.Code := _Enum.Attributes['value'];
    _DicItem.Note := _Enum.ChildNodes.Nodes['annotation'].ChildNodes.Nodes
      ['documentation'].NodeValue;
    Dest.AddObject(_DicItem.Note, _DicItem);
    inc(Result);
  end;
  _Doc := nil;
end;

class function TSTD_MP_ListofIndexGenerate.BpXml_FindItemByCode(ListItems: TStrings; aCode: string): Integer;
var
  _i: Integer;
  _item: TBpXml_DictionaryItem;
begin
  Result := -1;
  for _i := 0 to ListItems.Count - 1 do
  begin
    _item := TBpXml_DictionaryItem(ListItems.Objects[_i]);
    if _item.IsEqualCode(aCode) then
    begin
      Result := _i;
      BREAK;
    end;
  end;
end;

class function TSTD_MP_ListofIndexGenerate.BpXml_FindItemByNote(ListItems: TStrings; aNote: string): Integer;
var
  _i: Integer;
  _item: TBpXml_DictionaryItem;
begin
  Result := -1;
  for _i := 0 to ListItems.Count - 1 do
  begin
    _item := TBpXml_DictionaryItem(ListItems.Objects[_i]);
    if _item.IsEqualNote(aNote) then
    begin
      Result := _i;
      BREAK;
    end;
  end;
end;
{$ENDREGION}

{$REGION '������� ������������� ������ ������� xsd'} 

function TBpXml_DictionaryItem.IsEqualCode(aCode: string): Boolean;
begin
  Result := CompareText(aCode, self.FCode) = 0;
end;

function TBpXml_DictionaryItem.IsEqualNote(aNote: string): Boolean;
begin
  Result := CompareText(aNote, self.FNote) = 0;
end;


class function TSettingPathFile._TempPath: String;
begin
  Result := GetEnvironmentVariable('Temp');
end;
{$ENDREGION}

{$REGION '��� ������� ����� ������������ ������'} 

class function TAppliedFileDirect.AppliedFileDirect: TAppliedFileDirect;
begin
   if not Assigned(clAppliedFileDirect) then
     clAppliedFileDirect  := TAppliedFileDirect.Create(STD_MP_TEMPDIR + '\'+cnstLogLoadFile);
   Result :=   clAppliedFileDirect;
end;

function TAppliedFileDirect.DeleteKeyIni(section, ident: string): Boolean; 
begin 
 Result := false;
 try
   self.clAppliedFileDirect.DeleteKey(section,ident);
   Result := true;
 except on E : Exception do
   raise Exception.Create(E.Message);
 end;  
end;

destructor TAppliedFileDirect.Destroy;
begin 
  inherited;
end;

function TAppliedFileDirect.ReadSectionOpenIni(section, Ident: string): string;
begin
   Result := self.clAppliedFileDirect.ReadString(section,Ident,'');
end;

procedure TAppliedFileDirect.WriteSectionOpenIni(section, Ident, value: string);
begin
   self.clAppliedFileDirect.WriteString(section,Ident,value);
end;
{$ENDREGION}

{$REGION '�������� ����������  ���  ��������  �������� ����������'}

class procedure TSettingPathFile.CreateTempPath;
var guid : TGuid;
    strGuid : string;
begin
  CreateGUID(guid);
  strGuid := GUIDToString(guid);
  Delete(strGuid, 1, 1);
  Delete(strGuid, length(strGuid), 1);
  FTempFileName := TSettingPathFile. _TempPath+'\'+strGuid;
  if not TDirectory.Exists(FTempFileName) then   TDirectory.CreateDirectory(FTempFileName);
end;

class procedure TSettingPathFile.DeleteTempPath;
begin
 try
  if (FTempFileName <> '') and (TDirectory.Exists(FTempFileName)) then
  begin
    FullRemoveDir(FTempFileName,True,False,true);
    FTempFileName := '';
  end;
  except on E: Exception do
    raise Exception.Create(E.Message);
 end;
end;


class function TSettingPathFile.MakeTempPath: string;
begin
  if FTempFileName = ''  then  TSettingPathFile.CreateTempPath;
  Result := FTempFileName;
end;
{$ENDREGION}

{$REGION '�������� ��������� ������'}
///
///  <Settings Proxy
///
procedure GetSettingsProxy(var Host : string; var Port : integer);
var Registry  : TRegistry;
    proxyStr  : string;
    indexPos  : Integer;
begin
  Host := ''; Port := 0;
  Registry := TRegistry.Create;
  try
     Registry.RootKey := HKEY_CURRENT_USER; 
     Registry.OpenKey(cnstKey,false);
     if Registry.ValueExists(cnstKeyName) then     
        proxyStr :=  Registry.ReadString(cnstKeyName)
     else  
       if Registry.ValueExists('AutoConfigURL') then  //��� Winsows7 � ���� �������� ������ �������� �����
           proxyStr := Registry.ReadString('AutoConfigURL');         
  finally
    Registry.Free;
  end;
  if proxyStr.IsEmpty then {������ �������� �� ���������}
  begin
    host := '192.168.2.120';
    port := 8080;
    Exit;
  end;
  indexPos :=  proxyStr.IndexOf(':');
  Host := proxyStr.Substring(0,indexPos);
  Port   := strtoint(proxyStr.Substring(indexPos+1, Length(proxyStr)));
end;
{$ENDREGION}

end.
