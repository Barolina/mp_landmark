unit Fm_SpecifyParcels;

{ 27.08.2012 - update
  Update on 14.06.2013   �� ����� ����� v_04 }

{$define newEditorEntitySpatial}

interface

uses
  Windows, Messages, SysUtils, Forms,
  Dialogs, StdCtrls, Menus,
  Mask,
{$IFDEF newEditorEntitySpatial}
{$ELSE}
  frEntitySpatial,
{$ENDIF}
{$IFDEF newEditorEntitySpatial}   {$ELSE} {$ENDIF}
  STD_MP,
  XMLDoc,
  XMLIntf,
  BpXML_different,
  frSpecifyRelatedsParcels,
  RibbonLunaStyleActnCtrls,
  Controls, System.Classes, ComCtrls, ExtCtrls,
  PlatformDefaultStyleActnCtrls, ActnList, ActnMan, Buttons,
  ImgList, ButtonGroup, System.Actions,
  frExistEZParcels,
  unRussLang,
  Fm_ExistParcel,
  frExistEZEntryParcel,
  CommCtrl,
  Graphics,
  fmContainer,
  System.RegularExpressions,
  SurveyingPlan.Xml.Types, SurveyingPlan.DataFormat, SurveyingPlan.Xml.Utils,
  fmLog,
  VCL.ClipBrd, JvgPage;

const
  // -- ���� ���������� �������
  cnstTypeEntity_Spatial = 0;
  cnstTypeContours = 1;

type
  TFormSpecifyParcels = class(TForm)
    tsExisParcel: TTabSheet;
    ActionManagerSpecifyParcel: TActionManager;
    Action_ExistParcels: TAction;
    Action_SpecifyRelatedParcels: TAction;
    ActionAddAtrExistParcel: TAction;
    Act_AddExistparcelCadastralNumber: TAction;
    PopupMenu: TPopupMenu;
    PMItem_SlowlyChangeSlide: TMenuItem;
    PMItem_QuickChangeSlide: TMenuItem;
    tsSpecifyRelatedParcel: TTabSheet;
    pnlExistParcelAttribute: TPanel;
    tsExistEzParcels: TTabSheet;
    tsExistEZEntryParcel: TTabSheet;
    pnlExistEZEParcel: TPanel;
    cbbExistEZEParcel: TComboBox;
    btnAddExistEZ: TButtonGroup;
    actlst: TActionList;
    actExistEZEntryParcel_AddNew: TAction;
    actExistEZEntryParcel_DelCurrent: TAction;
    actExistEZEntryParcel_PasteFromClipBrd: TAction;
    actExistEZEntryParcel_Validate: TAction;
    actExistEZEntryParcel_CheckDirections: TAction;
    jvSpecifyParcels: TJvgPageControl;
    lblSpecifyparcels: TLabel;
    lblSpecifyParcelsEZ: TLabel;
    lbl1: TLabel;
    procedure action_ExistParcelsExecute(Sender: TObject);
    procedure Action_SpecifyRelatedParcelsExecute(Sender: TObject);
    procedure meCadNumBlock_valueKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);

    procedure PMItem_SlowlyChangeSlideClick(Sender: TObject);
    procedure PMItem_QuickChangeSlideClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure edCadastralNumberKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cbbAttributes_NameKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure pgcSpecifyParcelsDrawTab(Control: TCustomTabControl;
      TabIndex: Integer; const Rect: TRect; Active: Boolean);
    procedure cbbExistEZEParcelChange(Sender: TObject);
    procedure actExistEZEntryParcel_AddNewExecute(Sender: TObject);
    procedure actExistEZEntryParcel_DelCurrentExecute(Sender: TObject);
    procedure actExistEZEntryParcel_PasteFromClipBrdExecute(Sender: TObject);
    procedure actExistEZEntryParcel_ValidateExecute(Sender: TObject);
    procedure actExistEZEntryParcel_CheckDirectionsExecute(Sender: TObject);
  private type
   TKind = (spUndefine,spExistParcel,spExistEZ);
  private
    FKind                  : TKind;
    FXMlSpecifyParcel      : IXMLSTD_MP_Package_SpecifyParcel;

    FExist_Parcel          : TfrmExistParcel; //����������������
    FSpecifyRelatedsParcels: TFrameSpecifyRelatedsParcels; //������� ��
    FExistEZParcels        : TFrameExistEZParcels; //������ ����������������
    FExistEZEntryPArcels   : TFrameExistEZEntryParcel; //�������� � ������ ����������������
    { Private declarations }


    procedure ExistEZEntryParcel_DoCreateNewItem(CadNum: string);
    procedure ExistEZEntry_DoDelete(index: Integer);
    procedure ExistEZEntry_DoSetCurrentItem(index: Integer);

    procedure SetSpecifyParcelsExistParcel(const aSpecifyParcels: IXMLSTD_MP_Package_SpecifyParcel);
    procedure SetSpecifyParcelsFExistEzParcel(const aSpecifyParcels: IXMLSTD_MP_Package_SpecifyParcel);
    procedure InitializationUI;
  public
    constructor Create(AOwner: TComponent); override;
    function IsAssigned_SpecifyParcels: Boolean;

    procedure ExistEzParcels_CreateExistParcel;
    procedure ExistEzParcels_Set;
    procedure ExistEzParcels_Delete;


    procedure ExistEZEntryParcel_CreateNewItem(CadNum: string);
    procedure ExistEZEntryParcel_UpDateList;
    procedure ExistEZEntryParcel_Delete(index: Integer);
    procedure ExistEZEntryParcel_SetCurrentItem(index: Integer);

    procedure SetPageControl(isType: TTypeMP);

    property XmlSpecifyParcels_ExistParcels: IXMLSTD_MP_Package_SpecifyParcel read FXMlSpecifyParcel write SetSpecifyParcelsExistParcel;
    property XmlSpecifyParcels_ExistEZParcels: IXMLSTD_MP_Package_SpecifyParcel  read FXMlSpecifyParcel write SetSpecifyParcelsFExistEzParcel;


  end;

var
  FormSpecifyParcels: TFormSpecifyParcels;

implementation

uses MsXMLAPI,
  SurveyingPlan.Common;

resourcestring  
 rsFCBEF5BF = '�������� ������ ����������� �� ��������� � ��.';
 rsC7F69575 = '������� ����������� ����� ����������� ��';
 rs91C7B3C6 = '��������� �������� "%s" �� ������������� ������� ������������ ������.';
 rs3DC23DDD = '�������� ������������ ������� ����������� ����������� �������.';
 rs2FAA072A = '������������ ��������� ������?';
 rs2FAA072W = '�������� ��������� ������ ����� ��������?';
 rsFAC0B33B = '������ �� ����������';
 rs7FCD7C89 = '�� ��������� ������ ����������� �����';
 rsOk       = '�������� ��������� �������.';
 
 {$R *.dfm}

procedure TFormSpecifyParcels.actExistEZEntryParcel_AddNewExecute(
  Sender: TObject);
var
  _CadNum: string;
begin
  _CadNum := InputBox(rsFCBEF5BF,rsC7F69575,'');
  if _CadNum = '' then EXIT;
  if not TRegEx.IsMatch(_CadNum,cnRegCadastralNumber) then begin
   MessageDlg(Format(rsFCBEF5BF,[_CadNum]),mtError,[mbOK],-1);
   EXIT;
  end;
 self.ExistEZEntryParcel_CreateNewItem(_CadNum);
end;

procedure TFormSpecifyParcels.actExistEZEntryParcel_DelCurrentExecute(
  Sender: TObject);
begin
 if MessageDlg('������� ������: '+'���������� ������������ ������'+'?',mtInformation,mbYesNo,-1) = mrNo then Exit;
 self.ExistEZEntryParcel_Delete(self.cbbExistEZEParcel.ItemIndex)
end;

procedure TFormSpecifyParcels.actExistEZEntryParcel_PasteFromClipBrdExecute(
  Sender: TObject);
var
 _st: string;
 _mcontour: TSpMContour;
 _log: TStringBuilder;
 _dlog: TformLog;
begin
 Clipboard.Open;
 TRY
  _st:= Clipboard.AsText;
 FINALLY
  Clipboard.Close;
 END;
 _mcontour:= DataFormat.MContour.Text.Parse(_st);
 _mcontour._checkDirection;
 _log:= TStringBuilder.Create;
 if not TSpValidator.Validate(_mcontour,_log) then begin
  _dlog:= TformLog.Create(nil);
  _dlog.TextLog:= _log.ToString;
  FreeAndNil(_log);
  _dlog.ShowModal;
  if MessageDlg(rs2FAA072A,mtConfirmation,mbYesNo,-1) = mrNo then begin
   _dlog.Free;
   EXIT;
  end;
  _dlog.Free;
 end;
 _log.Free;

 { TODO : ������� �� ������ ��������� �.�. ����������� ���� ���� ����
 ����������� ������� }
 if self.FXMlSpecifyParcel.ExistEZ.ExistEZEntryParcels.Count<>0 then
  case MessageDlg(rs2FAA072W,mtConfirmation,[mbYes,mbNo,mbCancel],-1) of
   mrYes: self.FXMlSpecifyParcel.ExistEZ.ExistEZEntryParcels.Clear;
   mrNo: ;
   mrCancel: EXIT;
  end;

 TSpConvert.MContour.Convert(_mcontour,self.FXMlSpecifyParcel.ExistEZ.ExistEZEntryParcels);
 self.ExistEZEntryParcel_UpDateList;
end;

procedure TFormSpecifyParcels.actExistEZEntryParcel_ValidateExecute(
  Sender: TObject);
var
 _st: string;
 _mcontour: TSpMContour;
 _log: TStringBuilder;
 _dlog: TformLog;
begin

 _mcontour:= TSpMContour.Create;
 TSpConvert.MContour.Convert(self.FXMlSpecifyParcel.ExistEZ.ExistEZEntryParcels,_mcontour);
 _log:= TStringBuilder.Create;
 _dlog:= TformLog.Create(nil);
 if not TSpValidator.Validate(_mcontour,_log) then
  _dlog.TextLog:= _log.ToString
 else
  _dlog.TextLog:= rsFAC0B33B;
 _dlog.ShowModal;
 _log.Free;
 _dlog.Free;

 
end;


procedure TFormSpecifyParcels.actExistEZEntryParcel_CheckDirectionsExecute(Sender: TObject);
var
 _mcontor: TSpMContour;
begin
 _mcontor:= TSpMContour.Create;
 TSpConvert.MContour.Convert(self.FXMlSpecifyParcel.ExistEZ.ExistEZEntryParcels,_mcontor);
 _mcontor._checkDirection;
 TSpConvert.MContour.Convert(_mcontor,self.FXMlSpecifyParcel.ExistEZ.ExistEZEntryParcels);
 _mcontor.Free;
 self.ExistEZEntryParcel_UpDateList;
 ShowMessage(rsOk);
end;

procedure TFormSpecifyParcels.action_ExistParcelsExecute(Sender: TObject);
begin
  jvSpecifyParcels.ActivePageIndex := 0;
end;

procedure TFormSpecifyParcels.Action_SpecifyRelatedParcelsExecute(Sender: TObject);
begin
  jvSpecifyParcels.ActivePageIndex := 1;
end;


procedure TFormSpecifyParcels.InitializationUI;
begin
 case self.FKind of
   spUndefine: begin
    self.FExist_Parcel.XmlSpecifyParcels_ExistParcels:= nil;
    self.FSpecifyRelatedsParcels.XmlSpecifyRelatedParcelList:= nil;
    self.FExistEZParcels.XmlExistEZParcel:= nil;
    self.FExistEZEntryPArcels.XmlNewParcel:= nil;
    self.ExistEZEntryParcel_UpDateList
   end;
   spExistParcel: begin
    SetPageControl(tpExistParcel);
    self.FExistEZParcels.XmlExistEZParcel:= nil;
    self.FExistEZEntryPArcels.XmlNewParcel:= nil;

    self.FExist_Parcel.XmlSpecifyParcels_ExistParcels:= self.FXMlSpecifyParcel.ExistParcel;
    self.FSpecifyRelatedsParcels.XmlSpecifyRelatedParcelList:= self.FXMlSpecifyParcel.SpecifyRelatedParcel;
    self.jvSpecifyParcels.ActivePage := self.tsExisParcel;
   end;
   spExistEZ: begin
    SetPageControl(tpExistEZParcel);
    self.FExist_Parcel.XmlSpecifyParcels_ExistParcels:= nil;
    FExistEZParcels.XmlExistEZParcel := FXMlSpecifyParcel.ExistEZ.ExistEZParcels;
    self.FSpecifyRelatedsParcels.XmlSpecifyRelatedParcelList:= FXMlSpecifyParcel.SpecifyRelatedParcel;
    self.ExistEZEntryParcel_UpDateList;
    self.jvSpecifyParcels.ActivePage := tsExistEzParcels;
   end;
 end;
end;

function TFormSpecifyParcels.IsAssigned_SpecifyParcels: Boolean;
begin
  Result := FXMlSpecifyParcel <> nil;
end;

procedure TFormSpecifyParcels.cbbAttributes_NameKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key = VK_RETURN then
    if (Sender is TComboBox) then
      TComboBox(Sender).Parent.SetFocus;
end;

procedure TFormSpecifyParcels.cbbExistEZEParcelChange(Sender: TObject);
begin
  self.ExistEZEntryParcel_SetCurrentItem(cbbExistEZEParcel.ItemIndex);
end;

procedure TFormSpecifyParcels.Button1Click(Sender: TObject);
begin
  ShowMessage(FXMlSpecifyParcel.XML);
end;

procedure TFormSpecifyParcels.meCadNumBlock_valueKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key = VK_RETURN then
    if (Sender is TCustomEdit) then
      TCustomEdit(Sender).Parent.SetFocus;
end;

procedure TFormSpecifyParcels.pgcSpecifyParcelsDrawTab
  (Control: TCustomTabControl; TabIndex: Integer; const Rect: TRect;
  Active: Boolean);
  var acan : TCanvas;
begin
end;

procedure TFormSpecifyParcels.PMItem_QuickChangeSlideClick(Sender: TObject);
begin
  self.PMItem_SlowlyChangeSlide.Checked := false;
  self.PMItem_QuickChangeSlide.Checked := true;
end;

procedure TFormSpecifyParcels.PMItem_SlowlyChangeSlideClick(Sender: TObject);
begin
  self.PMItem_SlowlyChangeSlide.Checked := true;
  self.PMItem_QuickChangeSlide.Checked := false;
end;

constructor TFormSpecifyParcels.Create(AOwner: TComponent);
begin
  inherited;
  // ���������� ��
  self.FSpecifyRelatedsParcels := TFrameSpecifyRelatedsParcels.Create(self.tsSpecifyRelatedParcel);
  self.FSpecifyRelatedsParcels.Parent := self.tsSpecifyRelatedParcel;
  self.FSpecifyRelatedsParcels.Align := alClient;
  self.FSpecifyRelatedsParcels.Visible := true;

  self.FExistEZParcels := TFrameExistEZParcels.Create(self.tsExistEzParcels);
  self.FExistEZParcels.Parent := self.tsExistEzParcels;
  self.FExistEZParcels.Align := alClient;
  self.FExistEZParcels.Visible := false;

  self.FExist_Parcel := TfrmExistParcel.Create(self.pnlExistParcelAttribute);
  self.FExist_Parcel.Parent := self.pnlExistParcelAttribute;
  self.FExist_Parcel.Align := alClient;
  self.FExist_Parcel.Visible := false;

  self.FExistEZEntryPArcels := TFrameExistEZEntryParcel.Create(self.tsExistEZEntryParcel);
  self.FExistEZEntryPArcels.Parent := self.tsExistEZEntryParcel;
  self.FExistEZEntryPArcels.Align := alClient;
  self.FExistEZEntryPArcels.Visible := false;

  self.FKind:= spUndefine;

end;

procedure TFormSpecifyParcels.SetPageControl;
begin
   FExist_Parcel.Visible := (isType = tpExistParcel);
   FExistEZParcels.Visible := (isType = tpExistEZParcel);
   FExistEZEntryPArcels.Visible := (isType = tpExistEZParcel);
end;

procedure TFormSpecifyParcels.edCadastralNumberKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key = VK_RETURN then
    if (Sender is TMaskEdit) then
      TMaskEdit(Sender).Parent.SetFocus;
end;

procedure TFormSpecifyParcels.ExistEZEntryParcel_CreateNewItem(CadNum: string);
begin
 if not self.IsAssigned_SpecifyParcels then EXIT;
  
 self.ExistEZEntryParcel_DoCreateNewItem(CadNum);
 self.ExistEZEntryParcel_SetCurrentItem(self.cbbExistEZEParcel.Items.Count - 1);
end;

procedure TFormSpecifyParcels.ExistEZEntryParcel_Delete(index: Integer);
begin
  if (index > -1) and (index < self.cbbExistEZEParcel.Items.Count) then
    if index = self.cbbExistEZEParcel.ItemIndex then
    begin
      self.ExistEZEntry_DoDelete(index);
      self.ExistEZEntryParcel_SetCurrentItem(index);
    end
    else
      self.ExistEZEntry_DoDelete(index);
end;

procedure TFormSpecifyParcels.ExistEZEntryParcel_DoCreateNewItem(CadNum: string);
var
  ExistEZEParcel: IXMLTExistEZEntryParcel;
  _i: integer;
begin
 for _i := 0 to self.FXMlSpecifyParcel.ExistEZ.ExistEZEntryParcels.Count-1 do 
  if CompareText(CadNum,self.FXMlSpecifyParcel.ExistEZ.ExistEZEntryParcels[_i].CadastralNumber) = 0 then
   raise Exception.Create(rs3DC23DDD);
   
 ExistEZEParcel := self.FXMlSpecifyParcel.ExistEZ.ExistEZEntryParcels.Add;
 ExistEZEParcel.CadastralNumber := CadNum;
 self.cbbExistEZEParcel.AddItem(CadNum, pointer(ExistEZEParcel));
end;

procedure TFormSpecifyParcels.ExistEZEntryParcel_SetCurrentItem(index: Integer);
begin
  if (index > -1) and (index < self.cbbExistEZEParcel.Items.Count) then
  begin
    self.cbbExistEZEParcel.ItemIndex := index;
    self.ExistEZEntry_DoSetCurrentItem(index);
    self.FExistEZEntryPArcels.Visible := true;
  end
  else
  begin
    self.FExistEZEntryPArcels.XmlNewParcel := nil;
    self.cbbExistEZEParcel.ItemIndex := -1;
    self.cbbExistEZEParcel.Text := '';
    self.FExistEZEntryPArcels.Visible := false;
  end;

end;

procedure TFormSpecifyParcels.ExistEZEntryParcel_UpDateList;
var
  _index, _i: Integer;
  EZ: IXMLTExistEZEntryParcel;
begin
  _index := self.cbbExistEZEParcel.ItemIndex;
  self.cbbExistEZEParcel.Clear;
  if self.IsAssigned_SpecifyParcels then begin
    for _i := 0 to self.FXMlSpecifyParcel.ExistEZ.ExistEZEntryParcels.Count - 1 do
    begin
      EZ := self.FXMlSpecifyParcel.ExistEZ.ExistEZEntryParcels.
        ExistEZEntryParcel[_i];
      self.cbbExistEZEParcel.AddItem(EZ.CadastralNumber, pointer(EZ));
    end;
  end;

  if _index < 0 then
    _index := 0;
  self.ExistEZEntryParcel_SetCurrentItem(_index);

end;

procedure TFormSpecifyParcels.ExistEZEntry_DoDelete(index: Integer);
var
  EZ: IXMLTExistEZEntryParcel;
begin
  EZ := IXMLTExistEZEntryParcel
    (pointer(self.cbbExistEZEParcel.Items.Objects[index]));
  self.FXMlSpecifyParcel.ExistEZ.ExistEZEntryParcels.Remove(EZ);
  self.cbbExistEZEParcel.Items.Delete(index);
  EZ := nil;
end;

procedure TFormSpecifyParcels.ExistEZEntry_DoSetCurrentItem(index: Integer);
begin
 try   
   self.FExistEZEntryPArcels.XmlNewParcel := IXMLTExistEZEntryParcel(pointer(self.cbbExistEZEParcel.Items.Objects[index]));
 except on E : Exception do
    ShowMessage(E.Message);
 end;
end;

procedure TFormSpecifyParcels.ExistEzParcels_CreateExistParcel;
begin
  if self.IsAssigned_SpecifyParcels then
    FExistEZParcels.XmlExistEZParcel :=
      FXMlSpecifyParcel.ExistEZ.ExistEZParcels;
end;

procedure TFormSpecifyParcels.ExistEzParcels_Delete;
begin
  if self.IsAssigned_SpecifyParcels then
    MsXml_ReadChildNodeValue(FXMlSpecifyParcel, cnstXmlExistEZ);
end;

procedure TFormSpecifyParcels.ExistEzParcels_Set;
begin
  if self.IsAssigned_SpecifyParcels then
    FExistEZParcels.XmlExistEZParcel :=
      FXMlSpecifyParcel.ExistEZ.ExistEZParcels;
end;

procedure TFormSpecifyParcels.SetSpecifyParcelsExistParcel(const aSpecifyParcels: IXMLSTD_MP_Package_SpecifyParcel);
begin
  if self.FKind=spExistEZ then self.SetSpecifyParcelsFExistEzParcel(NIL);
  if self.FXMlSpecifyParcel = aSpecifyParcels then EXIT;
  self.FXMlSpecifyParcel := aSpecifyParcels;
  if self.IsAssigned_SpecifyParcels then  self.FKind:= spExistParcel
  else   self.FKind:= spUndefine;
  self.InitializationUI;
end;

procedure TFormSpecifyParcels.SetSpecifyParcelsFExistEzParcel(const aSpecifyParcels: IXMLSTD_MP_Package_SpecifyParcel);
begin
  if self.FKind=spExistParcel then self.SetSpecifyParcelsExistParcel(NIL);
  if self.FXMlSpecifyParcel = aSpecifyParcels then EXIT;
  self.FXMlSpecifyParcel := aSpecifyParcels;
  if self.IsAssigned_SpecifyParcels then
   self.FKind:= spExistEZ
  else
   self.FKind:= spUndefine;
  self.InitializationUI;
end;



end.
