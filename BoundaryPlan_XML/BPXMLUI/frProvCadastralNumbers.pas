unit frProvCadastralNumbers;
{
�������� � ��������� ��������, ����������� ������� �������������� ������
}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ActnList, StdCtrls, Mask, Buttons, ButtonGroup,

  XMLIntf, std_mp,MSStringParser, {MSClipBoard,}vcl.clipbrd, ImgList, BPCommon, System.Actions,
  frDataDocuments, Vcl.ComCtrls, Vcl.ToolWin, unRussLang,fmContainer,
  JvComponentBase, JvBalloonHint;
type
  TFrameProvCadastralNumber = class(TFrame)
    ActionList: TActionList;
    CadNum_ButtonGroup_Actions: TButtonGroup;
    CadNum_ListCN_ListBox: TListBox;
    CadNum_NewCN_MaskEdit: TMaskEdit;
    Action_DoAdd: TAction;
    PanelCentre: TPanel;
    PanelLeft: TPanel;

    Action_CopySel: TAction;
    Action_DelSel: TAction;
    Action_Paste: TAction;
    pnlCadastral: TPanel;
    PageControl1: TPageControl;
    tsCadNum: TTabSheet;
    tsDocuments: TTabSheet;
    tsOther: TTabSheet;
    mmOther: TMemo;
    tlbProvCadastral: TToolBar;
    btnCadastralNumber: TToolButton;
    btnDefinition: TToolButton;
    btnOther: TToolButton;
    btnDocuments: TToolButton;
    actHint: TAction;
    procedure Action_DoAddExecute(Sender: TObject);
    procedure Action_CopySelExecute(Sender: TObject);
    procedure Action_DelSelExecute(Sender: TObject);
    procedure Action_PasteExecute(Sender: TObject);
    procedure RadGInnerCadastralNumberClick(Sender: TObject);
    procedure mmOtherExit(Sender: TObject);
  private
    { Private declarations }
    FXmlCadastralNumbers :  IXMLTProviding_Pass_CadastralNumbers;
    FDocuments_frame     :  TFrameDataDocuments;

    procedure Data_DoAdd;
    procedure Data_DoDeleteSelected;
    procedure Data_DoRead;
    procedure Data_DoWrite;

    procedure DeleteCadastralNumbers;
    procedure DeleteNumbers;
    procedure ReadCadastralNumbers;
    procedure ReadNumbers;
    procedure ReadOther;
    procedure WriteOther;
    procedure WriteCadastralNumbers;
    procedure WriteNumbers;
    procedure ReadDOcuments;

    function GetCoutCadastralNumber : integer;
    function GetCountDefinition :  integer;
    function GetCountDocuments : Integer;
    function GetCountOther  : Integer;
  protected
    procedure InitializationUI;
    procedure SetProviding_Pass_CadastralNumbers(const aCadastralNumbers: IXMLTProviding_Pass_CadastralNumbers);
    procedure UpdateProvidingCadNum(isType : integer);{}
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    procedure VisbleCurrentTabShet();

    function IsAssignedCadastralNumbers: Boolean;
    property XmlCadastralNumbers: IXMLTProviding_Pass_CadastralNumbers read FXmlCadastralNumbers write SetProviding_Pass_CadastralNumbers;
  end;

implementation
   

{$R *.dfm}

 function IsValidCadNum(value: string;  IsLand: Boolean): Boolean;
 var
  _sp: TMSStringParser;
 begin
  _sp:= TMSStringParser.Create(':  _');
  _sp.Parse(value);
  Result:= ((_sp.GetCountToken=4) and (IsLand)) or ((_sp.GetCountToken=3) and (not IsLand));
  if not Result then
   Result:= Length(_sp.GetToken(0))=2;
  if not Result then
   Result:= Length(_sp.GetToken(1))=2;
  if not Result then
   Result:= Length(_sp.GetToken(2))=7;
  if (not Result) and IsLand  then
   Result:= Length(_sp.GetToken(3))<>0;
  _sp.Free;
 end;

{ TFrameCadastralNumbersCollections }

procedure TFrameProvCadastralNumber.Action_CopySelExecute(Sender: TObject);
var
 _stl: TStringList;
 _i: Integer;
begin
 if not self.IsAssignedCadastralNumbers then raise Exception.Create(ExMSG_NotAssignedROOT);
 _stl:= TStringList.Create;
 for  _i:= 0 to self.CadNum_ListCN_ListBox.Count - 1 do
  if self.CadNum_ListCN_ListBox.Selected[_i] then
   _stl.Add(self.CadNum_ListCN_ListBox.Items[_i]);
 //ClipBrs_WriteStrings(_stl);
 _stl.Free;
end;

procedure TFrameProvCadastralNumber.Action_DelSelExecute(
  Sender: TObject);
begin
 if not self.IsAssignedCadastralNumbers then raise Exception.Create(ExMSG_NotAssignedROOT);
 self.Data_DoDeleteSelected;
end;

procedure TFrameProvCadastralNumber.Action_DoAddExecute(Sender: TObject);
begin
 if not self.IsAssignedCadastralNumbers then raise Exception.Create(ExMSG_NotAssignedROOT);
 self.Data_DoAdd;
end;

procedure TFrameProvCadastralNumber.Action_PasteExecute(Sender: TObject);
var
 _i: Integer;
 _stl: TStringList;
 _cnum: string;
begin
 if not self.IsAssignedCadastralNumbers then raise Exception.Create(ExMSG_NotAssignedROOT);

 if self.CadNum_ListCN_ListBox.Count<>0 then
   if MessageDlg('������� ��������� � ������ ������?',mtConfirmation,mbYesNo,-1)=mrYes then begin
     self.CadNum_ListCN_ListBox.SelectAll;
     Data_DoDeleteSelected;
   end;
 _stl:= TStringList.Create;
 //ClipBrd_ReadStrings(_stl);
 Clipboard.Open;
 _stl.Text:= Clipboard.AsText;
 Clipboard.Close;
 for _i := 0 to _stl.Count - 1 do begin
  _cnum:= _stl[_i];
  self.CadNum_NewCN_MaskEdit.Text:= _cnum;
  try
   self.Data_DoAdd;
  Except   raise Exception.Create('������ ������� :)');
  end;
 end;
 _stl.Free;
end;

constructor TFrameProvCadastralNumber.Create(AOwner: TComponent);
begin
 inherited;

 FDocuments_frame := TFrameDataDocuments.Create(tsDocuments);
 FDocuments_frame.Parent  := tsDocuments;
 FDocuments_frame.Align   := alClient;
 FDocuments_frame.Visible := true;

 self.InitializationUI;
end;

procedure TFrameProvCadastralNumber.InitializationUI;
begin
 self.CadNum_NewCN_MaskEdit.Text:= '';
 if self.IsAssignedCadastralNumbers then
  self.Data_DoRead
 else
 begin
  self.CadNum_ListCN_ListBox.Clear;
  FDocuments_frame.XmlProvidingPassCadNum := nil;
 end;
end;

function TFrameProvCadastralNumber.IsAssignedCadastralNumbers: Boolean;
begin
 Result:= self.FXmlCadastralNumbers<>nil;
end;

procedure TFrameProvCadastralNumber.mmOtherExit(Sender: TObject);
begin
  WriteOther;
end;

procedure TFrameProvCadastralNumber.DeleteNumbers;
var _i : integer;
begin
 for _i := self.CadNum_ListCN_ListBox.Count-1 downto 0 do
  if self.CadNum_ListCN_ListBox.Selected[_i] then begin
    FXmlCadastralNumbers.Definition.Delete(_i);
   self.CadNum_ListCN_ListBox.Items.Delete(_i);
  end;
end;


function TFrameProvCadastralNumber.GetCountDefinition: integer;
begin
  Result :=  FXmlCadastralNumbers.Definition.Count;
end;

function TFrameProvCadastralNumber.GetCountDocuments: Integer;
begin
  Result :=  FXmlCadastralNumbers.Documents.Count;
end;

function TFrameProvCadastralNumber.GetCountOther: Integer;
begin
  Result := 0;
  if (FXmlCadastralNumbers.Other <> '') then
   Result :=  1;
end;

function TFrameProvCadastralNumber.GetCoutCadastralNumber: integer;
begin
  Result := FXmlCadastralNumbers.CadastralNumber.Count;
end;

procedure TFrameProvCadastralNumber.RadGInnerCadastralNumberClick(Sender: TObject);
begin
  VisbleCurrentTabShet;
  UpdateProvidingCadNum((Sender as TToolButton).Index);
end;

procedure TFrameProvCadastralNumber.ReadCadastralNumbers;
var
 _i: Integer;
 _CadNum: string;
 _StVal: string;
begin
 CadNum_ListCN_ListBox.Clear;
 CadNum_NewCN_MaskEdit.Clear;
 self.CadNum_ListCN_ListBox.Clear;
 for _i := 0 to self.FXmlCadastralNumbers.CadastralNumber.Count-1 do begin
   _CadNum:= self.FXmlCadastralNumbers.CadastralNumber.Items[_i];
    _StVal:= _CadNum;
  self.CadNum_ListCN_ListBox.AddItem(_StVal,pointer(_CadNum));
  end;
  btnCadastralNumber.Caption := rsCadastralNumber +' '+ IntToStr(GetCoutCadastralNumber);
end;

procedure TFrameProvCadastralNumber.ReadDOcuments;
begin
  FDocuments_frame.XmlProvidingPassCadNum := FXmlCadastralNumbers.Documents;
  btnDocuments.Caption :=  rsDocuments+  ' '+ IntToStr(GetCountDocuments);
end;

procedure TFrameProvCadastralNumber.ReadNumbers;
var
 _i: Integer;
 _CadNum: string;
 _StVal: string;
begin
 CadNum_ListCN_ListBox.Clear;
 CadNum_NewCN_MaskEdit.Clear;
 self.CadNum_ListCN_ListBox.Clear;
 for _i := 0 to self.FXmlCadastralNumbers.Definition.Count-1 do begin
   _CadNum:= self.FXmlCadastralNumbers.Definition.Items[_i];
    _StVal:= _CadNum;
  self.CadNum_ListCN_ListBox.AddItem(_StVal,pointer(_CadNum));
  end;
 btnDefinition.Caption := rsDefinition +  ' '+ IntToStr(GetCountDefinition);
end;

procedure TFrameProvCadastralNumber.ReadOther;
begin
 mmOther.Clear;
 if FXmlCadastralNumbers.ChildNodes.FindNode(cnstXmlOther)  <> nil  then
   mmOther.Lines.Add(FXmlCadastralNumbers.Other);
  btnOther.Caption := rsOther +  ' '+ IntToStr(GetCountOther);
 end;

procedure TFrameProvCadastralNumber.SetProviding_Pass_CadastralNumbers(const aCadastralNumbers: IXMLTProviding_Pass_CadastralNumbers);
begin
 if self.FXmlCadastralNumbers=aCadastralNumbers then EXIT;
 self.FXmlCadastralNumbers:= aCadastralNumbers;
 self.InitializationUI;
end;


procedure TFrameProvCadastralNumber.UpdateProvidingCadNum(isType: integer);
begin
   VisbleCurrentTabShet;
   case isType of
    1 :  ReadNumbers;
    0 :  ReadCadastralNumbers;
    3 :  ReadDOcuments;
    2 :  ReadOther;
   end;
end;

procedure TFrameProvCadastralNumber.VisbleCurrentTabShet;
begin
 tsCadNum.Visible := (btnCadastralNumber.Down ) or (btnDefinition.Down);
 tsDocuments.Visible := btnDocuments.Down;
 tsOther.Visible := btnOther.Down;
end;

procedure TFrameProvCadastralNumber.WriteCadastralNumbers;
var
 _i: Integer;
 _CadNum: string;
 _StVal: string;
 _node: IXMLNode;
begin
 self.FXmlCadastralNumbers.CadastralNumber.Clear;
 for _i := 0 to self.CadNum_ListCN_ListBox.Count-1 do begin
     _node :=  FXmlCadastralNumbers.CadastralNumber.Add(FXmlCadastralNumbers.CadastralNumber.Items[_i]);
     self.CadNum_ListCN_ListBox.Items.Objects[_i]:= pointer(_node);
 end;
end;

procedure TFrameProvCadastralNumber.WriteNumbers;
var
 _i: Integer;
 _CadNum: string;
 _StVal: string;
 _node: IXMLNode;
begin
 self.FXmlCadastralNumbers.Definition.Clear;
 for _i := 0 to self.CadNum_ListCN_ListBox.Count-1 do begin
     _node :=  FXmlCadastralNumbers.Definition.Add(FXmlCadastralNumbers.Definition.Items[_i]);
     self.CadNum_ListCN_ListBox.Items.Objects[_i]:= pointer(_node);
 end;
end;

procedure TFrameProvCadastralNumber.WriteOther;
begin
  if not self.IsAssignedCadastralNumbers then raise Exception.Create(ExMSG_NotAssignedROOT);
  FXmlCadastralNumbers.Other := mmOther.Text;
end;

procedure TFrameProvCadastralNumber.Data_DoAdd;
var
 _cadnum: IXMLNode;
 _cn:string;
 xmllis : IXMLTInner_CadastralNumbers_CadastralNumberList;
begin
 try
  self.CadNum_NewCN_MaskEdit.ValidateEdit;
 except
  raise Exception.Create(Format(ExMsg_Data_Invalid,[self.CadNum_NewCN_MaskEdit.Hint]));
 end;

 _cn:= self.CadNum_NewCN_MaskEdit.Text;
 if _cn='' then EXIT;
 if not BPcCadNum_IsExistItemValue(_cn,[cnDistrict,cnMunicipality,cnBlock,cnNum]) then
  if MessageDlg('��������� ������ �� ������������� ������� ������������ ������ ��� ����� ������'+sLineBreak+
                '������������� ������������ ����� ������?',mtConfirmation,mbYesNo,-1)=mrNo then
                EXIT;

 if self.CadNum_ListCN_ListBox.Items.IndexOf(_cn)<>-1 then exit;

 if btnCadastralNumber.Down then
  begin
          if not IsValidCadNum(_cn,true) then
               raise Exception.Create(Format(ExMsg_Data_Invalid,[self.CadNum_NewCN_MaskEdit.Hint]));
          FXmlCadastralNumbers.CadastralNumber.Insert(FXmlCadastralNumbers.CadastralNumber.Count,self.CadNum_NewCN_MaskEdit.Text);
  end
  else if btnDefinition.Down then
          FXmlCadastralNumbers.Definition.Insert(FXmlCadastralNumbers.Definition.Count,self.CadNum_NewCN_MaskEdit.Text);
  self.CadNum_ListCN_ListBox.AddItem(self.CadNum_NewCN_MaskEdit.Text,pointer(_cadnum));
end;

procedure TFrameProvCadastralNumber.Data_DoDeleteSelected;
var
 _i:integer;
begin
  if btnCadastralNumber.Down then
     DeleteCadastralNumbers;
  if btnDefinition.Down then
     DeleteNumbers;
end;

procedure TFrameProvCadastralNumber.Data_DoRead;
var xmlNode : IXMLNode;
  I: Integer;
begin
{ ��� ���������� ��������� ���������� TToolbar }
 if FXmlCadastralNumbers.ChildNodes.FindNode(cnstXmlCadastralNumber) <>nil then
  btnCadastralNumber.Down :=  true
 else
 if FXmlCadastralNumbers.ChildNodes.FindNode(cnstXmlDefinition) <> nil then
   btnDefinition.Down := true
 else
 if FXmlCadastralNumbers.ChildNodes.FindNode(cnstXmlOther) <>nil  then
   btnOther.Down := True
 else
  if FXmlCadastralNumbers.ChildNodes.FindNode(cnstXmlDocuments) <> nil then
   btnDocuments.Down :=  true
  else
       tlbProvCadastral.Buttons[0].Down := true;

 for I := 0 to tlbProvCadastral.ButtonCount-1 do
   if tlbProvCadastral.Buttons[i].Down then  UpdateProvidingCadNum(i);

end;

procedure TFrameProvCadastralNumber.Data_DoWrite;
var
 _i: Integer;
 _CadNum: string;
 _StVal: string;
 _node: IXMLNode;
begin
 self.FXmlCadastralNumbers.CadastralNumber.Clear;
 for _i := 0 to self.CadNum_ListCN_ListBox.Count-1 do begin
     _node :=  FXmlCadastralNumbers.CadastralNumber.Add(FXmlCadastralNumbers.CadastralNumber.Items[_i]);
     self.CadNum_ListCN_ListBox.Items.Objects[_i]:= pointer(_node);
 end;
end;

procedure TFrameProvCadastralNumber.DeleteCadastralNumbers;
var _i : integer;
begin
 for _i := self.CadNum_ListCN_ListBox.Count-1 downto 0 do
  if self.CadNum_ListCN_ListBox.Selected[_i] then
  begin
    FXmlCadastralNumbers.CadastralNumber.Delete(_i);
    self.CadNum_ListCN_ListBox.Items.Delete(_i);
  end;

end;

end.
