<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
	<xsl:strip-space elements="*"/>
	<!--****************************************Elements****************************************-->
	<xsl:template match="STD_MP">
		<xsl:copy>
			<xsl:copy-of select="eDocument"/>
			<xsl:apply-templates select="Title"/>
			<xsl:apply-templates select="Package"/>
			<xsl:apply-templates select="Coord_Systems"/>
			<xsl:apply-templates select="Input_Data"/>
			<xsl:if test="count(Survey/child::*)>0">
				<xsl:apply-templates select="Survey"/>
			</xsl:if>
			<xsl:if test="Conclusion">
				<xsl:apply-templates select="Conclusion"/>
			</xsl:if>
			<xsl:if test="count(Scheme_Geodesic_Plotting/child::*)>0">
				<xsl:apply-templates select="Scheme_Geodesic_Plotting"/>
			</xsl:if>
			<xsl:if test="count(Scheme_Disposition_Parcels/child::*)>0">
				<xsl:apply-templates select="Scheme_Disposition_Parcels"/>
			</xsl:if>
			<xsl:apply-templates select="Diagram_Parcels_SubParcels"/>
			<xsl:if test="count(Agreement_Document/child::*)>0">
				<xsl:apply-templates select="Agreement_Document"/>
			</xsl:if>
			<xsl:if test="count(NodalPointSchemes/child::*)>0">
				<xsl:apply-templates select="NodalPointSchemes"/>
			</xsl:if>
			<xsl:if test="count(Appendix/child::*)>0">
				<xsl:apply-templates select="Appendix"/>
			</xsl:if>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Entity_Spatial">
		<xsl:copy>
			<xsl:copy-of select="@Ent_Sys"/>
			<xsl:for-each select="Spatial_Element">
				<xsl:if test="count(child::*) != 0">
					<xsl:copy>
						<xsl:call-template name="tSPATIAL_ELEMENT_OLD_NEW"/>
					</xsl:copy>
				</xsl:if>
			</xsl:for-each>
			<xsl:if test="Borders">
				<xsl:element name="Borders">
					<xsl:for-each select="Borders/Border">
						<xsl:copy>
							<xsl:call-template name="tBorder"/>
						</xsl:copy>
					</xsl:for-each>
				</xsl:element>
			</xsl:if>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="RelatedParcels">
		<xsl:if test="ParcelNeighbours">
			<xsl:copy>
				<xsl:for-each select="ParcelNeighbours">
					<xsl:copy>
						<xsl:if test="Definition != ''">
							<xsl:element name="Definition">
								<xsl:value-of select="normalize-space(Definition)"/>
							</xsl:element>
						</xsl:if>
						<xsl:for-each select="ParcelNeighbour">
							<xsl:copy>
								<xsl:if test="Cadastral_Number!=''">
									<xsl:element name="Cadastral_Number">
										<xsl:value-of select="normalize-space(Cadastral_Number)"/>
									</xsl:element>
								</xsl:if>
								<xsl:if test="OwnerNeighbours !='' and count(OwnerNeighbours/child::*)>0">
									<xsl:element name="OwnerNeighbours">
										<xsl:copy-of select="OwnerNeighbours/NameRight"/>
										<xsl:for-each select="OwnerNeighbours/OwnerNeighbour">
											<xsl:copy>
												<xsl:copy-of select="NameOwner"/>
												<xsl:copy-of select="ContactAddress"/>
												<xsl:if test="Documents != '' and count (Documents/child::*)>0">
													<xsl:apply-templates select="Documents" mode="Document"/>
												</xsl:if>
											</xsl:copy>
										</xsl:for-each>
									</xsl:element>
								</xsl:if>
							</xsl:copy>
						</xsl:for-each>
					</xsl:copy>
				</xsl:for-each>
			</xsl:copy>
		</xsl:if>
	</xsl:template>
	<xsl:template match="Contours">
		<xsl:copy>
			<xsl:for-each select="NewContour">
				<xsl:copy>
					<xsl:copy-of select="@Definition"/>
					<xsl:call-template name="tContour"/>
					<xsl:apply-templates select="Providing_Pass_CadastralNumbers" mode="tProviding_Pass_CadastralNumbers"/>
				</xsl:copy>
			</xsl:for-each>
			<xsl:for-each select="ExistContour">
				<xsl:copy>
					<xsl:copy-of select="@Number_Record"/>
					<xsl:call-template name="tContour"/>
				</xsl:copy>
			</xsl:for-each>
			<xsl:for-each select="DeleteAllBorder">
				<xsl:copy>
					<xsl:copy-of select="@Number_Record"/>
					<xsl:for-each select="OldOrdinate">
						<xsl:copy>
							<xsl:call-template name="tOrdinate"/>
						</xsl:copy>
					</xsl:for-each>
				</xsl:copy>
			</xsl:for-each>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Coord_Systems">
		<xsl:copy>
			<xsl:apply-templates select="Coord_System"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Coord_System">
		<xsl:copy>
			<xsl:choose>
				<xsl:when test="@Name">
					<xsl:copy-of select="@Name"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="Name">СК кадастрового округа</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:attribute name="Cs_Id">SystemCoord</xsl:attribute>
		</xsl:copy>
	</xsl:template>
	<!--****************************************/Elements****************************************-->
	<!--****************************************ComplexType****************************************-->
	<!--Почтовый адрес-->
	<xsl:template name="tAddress">
		<xsl:copy-of select="Code_OKATO"/>
		<xsl:copy-of select="Code_KLADR"/>
		<xsl:if test="Postal_Code != ''">
			<xsl:copy-of select="Postal_Code"/>
		</xsl:if>
		<xsl:copy-of select="Region"/>
		<xsl:if test="(District/@Name!='') and (District/@Type!='')">
			<xsl:element name="District">
				<xsl:attribute name="Name"><xsl:value-of select="District/@Name"/></xsl:attribute>
				<xsl:attribute name="Type"><xsl:value-of select="District/@Type"/></xsl:attribute>
			</xsl:element>
		</xsl:if>
		<xsl:if test="(City/@Name!='') and (City/@Type!='')">
			<xsl:element name="City">
				<xsl:attribute name="Name"><xsl:value-of select="City/@Name"/></xsl:attribute>
				<xsl:attribute name="Type"><xsl:value-of select="City/@Type"/></xsl:attribute>
			</xsl:element>
		</xsl:if>
		<xsl:if test="Urban_District/@Name!=''">
			<xsl:element name="Urban_District">
				<xsl:attribute name="Name"><xsl:value-of select="Urban_District/@Name"/></xsl:attribute>
				<xsl:attribute name="Type"><xsl:value-of select="'р-н'"/></xsl:attribute>
			</xsl:element>
		</xsl:if>
		<xsl:if test="Soviet_Village/@Name!=''">
			<xsl:element name="Soviet_Village">
				<xsl:attribute name="Name"><xsl:value-of select="Soviet_Village/@Name"/></xsl:attribute>
				<xsl:attribute name="Type"><xsl:value-of select="'с/с'"/></xsl:attribute>
			</xsl:element>
		</xsl:if>
		<xsl:if test="(Locality/@Name!='') and (Locality/@Type!='')">
			<xsl:element name="Locality">
				<xsl:attribute name="Name"><xsl:value-of select="Locality/@Name"/></xsl:attribute>
				<xsl:attribute name="Type"><xsl:value-of select="Locality/@Type"/></xsl:attribute>
			</xsl:element>
		</xsl:if>
		<xsl:if test="(Street/@Name!='') and (Street/@Type!='')">
			<xsl:element name="Street">
				<xsl:attribute name="Name"><xsl:value-of select="Street/@Name"/></xsl:attribute>
				<xsl:attribute name="Type"><xsl:value-of select="Street/@Type"/></xsl:attribute>
			</xsl:element>
		</xsl:if>
		<xsl:if test="(Level1/@Value!='') and (Level1/@Type!='')">
			<xsl:element name="Level1">
				<xsl:attribute name="Value"><xsl:value-of select="Level1/@Value"/></xsl:attribute>
				<xsl:attribute name="Type"><xsl:value-of select="Level1/@Type"/></xsl:attribute>
			</xsl:element>
		</xsl:if>
		<xsl:if test="(Level2/@Value!='') and (Level2/@Type!='')">
			<xsl:element name="Level2">
				<xsl:attribute name="Value"><xsl:value-of select="Level2/@Value"/></xsl:attribute>
				<xsl:attribute name="Type"><xsl:value-of select="Level2/@Type"/></xsl:attribute>
			</xsl:element>
		</xsl:if>
		<xsl:if test="(Level3/@Value!='') and (Level3/@Type!='')">
			<xsl:element name="Level3">
				<xsl:attribute name="Value"><xsl:value-of select="Level3/@Value"/></xsl:attribute>
				<xsl:attribute name="Type"><xsl:value-of select="Level3/@Type"/></xsl:attribute>
			</xsl:element>
		</xsl:if>
		<xsl:copy-of select="Apartment"/>
		<xsl:copy-of select="Other"/>
		<xsl:copy-of select="Note"/>
	</xsl:template>
	<!--Приложенные файлы с образами-->
	<xsl:template match="AppliedFiles" mode="AppliedFile">
		<xsl:if test="AppliedFile">
			<xsl:copy>
				<xsl:apply-templates select="AppliedFile" mode="tAppliedFile"/>
			</xsl:copy>
		</xsl:if>
	</xsl:template>
	<xsl:template match="AppliedFile" mode="tAppliedFile">
		<xsl:copy>
			<xsl:call-template name="tAppliedFile"/>
			<xsl:call-template name="tAppliedFile"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template name="tAppliedFile">
		<xsl:copy-of select="@Kind"/>
		<xsl:copy-of select="@Name"/>
	</xsl:template>
	<!--Площадь-->
	<xsl:template match="Area" mode="tArea">
		<xsl:copy>
			<xsl:call-template name="tArea"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template name="tArea">
		<xsl:copy-of select="Area"/>
		<!--xsl:copy-of select="Unit"/-->
		<xsl:element name="Unit">055</xsl:element>
		<xsl:copy-of select="Innccuracy"/>
	</xsl:template>
	<!--Площадь контура-->
	<xsl:template match="Area" mode="tArea_Contour">
		<xsl:copy>
			<xsl:call-template name="tArea_Contour"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template name="tArea_Contour">
		<xsl:copy-of select="Area"/>
		<!--xsl:copy-of select="Unit"/-->
		<xsl:element name="Unit">055</xsl:element>
		<xsl:copy-of select="Innccuracy"/>
	</xsl:template>
	<!--Сведения о предельных минимальных и максимальных размерах ЗУ-->
	<xsl:template match="Area" mode="tArea_without_Innccuracy">
		<xsl:copy>
			<xsl:call-template name="tArea_without_Innccuracy"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template name="tArea_without_Innccuracy">
		<xsl:copy-of select="Area"/>
		<xsl:element name="Unit">055</xsl:element>
	</xsl:template>
	<!--Учточненная площадь-->
	<xsl:template match="Area" mode="tAreaNew">
		<xsl:copy>
			<xsl:call-template name="tAreaNew"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template name="tAreaNew">
		<xsl:copy-of select="Area"/>
		<!--xsl:copy-of select="Unit"/-->
		<xsl:element name="Unit">055</xsl:element>
		<xsl:copy-of select="Innccuracy"/>
	</xsl:template>
	<!--Описание границы-->
	<xsl:template name="tBorder">
		<xsl:copy-of select="@Spatial"/>
		<xsl:copy-of select="@Point1"/>
		<xsl:copy-of select="@Point2"/>
		<xsl:copy-of select="@ByDef"/>
		<xsl:if test="Edge">
			<xsl:element name="Edge">
				<xsl:copy-of select="Edge/Length"/>
				<xsl:copy-of select="Edge/Definition"/>
			</xsl:element>
		</xsl:if>
	</xsl:template>
	<!--Наименование категории земель-->
	<xsl:template match="Category" mode="tCategory">
		<xsl:copy>
			<xsl:call-template name="tCategory"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template name="tCategory">
		<xsl:copy-of select="@Category"/>
		<xsl:apply-templates select="DocCategory" mode="tCategory"/>
	</xsl:template>
	<xsl:template match="DocCategory" mode="tCategory">
		<xsl:copy>
			<xsl:call-template name="tDocument"/>
		</xsl:copy>
	</xsl:template>
	<!--Описание изменненого участка-->
	<xsl:template name="tChangeParcel">
		<xsl:if test="@CadastralNumber !=''">
			<xsl:attribute name="CadastralNumber"><xsl:value-of select="normalize-space(@CadastralNumber)"/></xsl:attribute>
		</xsl:if>
		<xsl:copy-of select="CadastralBlock"/>
		<xsl:apply-templates select="Providing_Pass_CadastralNumbers" mode="tProviding_Pass_CadastralNumbers"/>
		<xsl:apply-templates select="Inner_CadastralNumbers"/>
		<xsl:apply-templates select="SubParcels" mode="tChangeParcel"/>
		<xsl:if test="count(DeleteEntryParcels/child::*) != 0">
			<xsl:copy-of select="DeleteEntryParcels"/>
		</xsl:if>
		<xsl:copy-of select="Node"/>
	</xsl:template>
	<xsl:template match="SubParcels" mode="tChangeParcel">
		<xsl:copy>
			<xsl:apply-templates select="FormSubParcel" mode="tChangeParcel"/>
			<xsl:apply-templates select="ExistSubParcel" mode="tChangeParcel"/>
			<xsl:apply-templates select="InvariableSubParcel" mode="tChangeParcel"/>
		</xsl:copy>
	</xsl:template>
	<!--Образуемая часть участка-->
	<xsl:template match="FormSubParcel" mode="tChangeParcel">
		<xsl:copy>
			<xsl:copy-of select="@Definition"/>
			<xsl:call-template name="tSubParcel"/>
		</xsl:copy>
	</xsl:template>
	<!--Существующая (уточняемая, изменяемая ЧЗУ)-->
	<xsl:template match="ExistSubParcel" mode="tChangeParcel">
		<xsl:copy>
			<xsl:copy-of select="@Number_Record"/>
			<xsl:copy-of select="@CadastralNumber_EntryParcel"/>
			<xsl:call-template name="tSubParcel"/>
		</xsl:copy>
	</xsl:template>
	<!--Существующая (неизменяемая) часть участка-->
	<xsl:template match="InvariableSubParcel" mode="tChangeParcel">
		<xsl:copy>
			<xsl:copy-of select="@Number_Record"/>
			<xsl:copy-of select="@SubParcel_Realty"/>
			<xsl:apply-templates select="Area" mode="tArea_without_Innccuracy"/>
			<xsl:apply-templates select="Encumbrance"/>
			<xsl:if test="count(Contours/child::*) != 0">
				<xsl:element name="Contours">
					<xsl:for-each select="Contours/Contour">
						<xsl:copy>
							<xsl:copy-of select="@Number"/>
							<xsl:apply-templates select="Area" mode="tArea_Contour"/>
						</xsl:copy>
					</xsl:for-each>
				</xsl:element>
			</xsl:if>
		</xsl:copy>
	</xsl:template>
	<!--Контур МЗУ-->
	<xsl:template match="Contour" mode="tContour">
		<xsl:copy>
			<xsl:call-template name="tContour"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template name="tContour">
		<xsl:apply-templates select="Area" mode="tArea_Contour"/>
		<xsl:apply-templates select="Entity_Spatial"/>
	</xsl:template>
	<!--Документ-описание-->
	<xsl:template match="Documents" mode="Document">
		<xsl:if test="Document">
			<xsl:copy>
				<xsl:apply-templates select="Document" mode="tDocument"/>
			</xsl:copy>
		</xsl:if>
	</xsl:template>
	<xsl:template match="Document" mode="tDocument">
		<xsl:copy>
			<xsl:call-template name="tDocument"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template name="tDocument">
		<xsl:copy-of select="Code_Document"/>
		<xsl:if test="Name !=''">
			<xsl:copy-of select="Name"/>
		</xsl:if>
		<xsl:if test="Series !=''">
			<xsl:copy-of select="Series"/>
		</xsl:if>
		<xsl:copy-of select="Number"/>
		<xsl:if test="Date !=''">
			<xsl:copy-of select="Date"/>
		</xsl:if>
		<xsl:if test="IssueOrgan !=''">
			<xsl:copy-of select="IssueOrgan"/>
		</xsl:if>
		<xsl:if test="NumberReg!=''">
			<xsl:copy-of select="NumberReg"/>
		</xsl:if>
		<xsl:if test="DateReg !=''">
			<xsl:copy-of select="DateReg"/>
		</xsl:if>
		<xsl:if test="Duration">
			<xsl:variable name="isStarted" select="Duration/Started != ''"/>
			<xsl:variable name="isStopped" select="Duration/Stopped != ''"/>
			<xsl:if test="$isStarted or $isStopped">
				<xsl:element name="Duration">
					<xsl:if test="$isStarted">
						<xsl:copy-of select="Duration/Started"/>
					</xsl:if>
					<xsl:if test="$isStopped">
						<xsl:copy-of select="Duration/Stopped"/>
					</xsl:if>
				</xsl:element>
			</xsl:if>
		</xsl:if>
		<xsl:if test="Register !=''">
			<xsl:copy-of select="Register"/>
		</xsl:if>
		<xsl:if test="Desc !=''">
			<xsl:copy-of select="Desc"/>
		</xsl:if>
		<xsl:if test="IssueOrgan_Code !=''">
			<xsl:copy-of select="IssueOrgan_Code"/>
		</xsl:if>
		<xsl:apply-templates select="AppliedFiles" mode="AppliedFile"/>
	</xsl:template>
	<!--Обременения-->
	<xsl:template match="Encumbrance">
		<xsl:copy>
			<xsl:call-template name="tEncumbrance"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template name="tEncumbrance">
		<xsl:copy-of select="Name"/>
		<xsl:copy-of select="Type"/>
		<xsl:copy-of select="CadastralNumber_Restriction"/>
		<xsl:apply-templates select="Documents" mode="Document"/>
	</xsl:template>
	<!--Описание уточняемого входящего в единое землепользование участка-->
	<xsl:template name="tExistEZEntryParcel">
		<xsl:if test="@CadastralNumber!=''">
			<xsl:attribute name="CadastralNumber"><xsl:value-of select="normalize-space(@CadastralNumber)"/></xsl:attribute>
		</xsl:if>
		<xsl:apply-templates select="Area" mode="tAreaNew"/>
		<xsl:copy-of select="Area_In_GKN"/>
		<xsl:apply-templates select="Encumbrance"/>
		<xsl:apply-templates select="Entity_Spatial"/>
		<xsl:copy-of select="Note"/>
	</xsl:template>
	<xsl:template name="tExistEZParcel">
		<xsl:copy-of select="@CadastralNumber"/>
		<xsl:copy-of select="CadastralBlock"/>
		<xsl:apply-templates select="Inner_CadastralNumbers"/>
		<xsl:apply-templates select="Area" mode="tAreaNew"/>
		<xsl:apply-templates select="SubParcels" mode="tExistEZParcel"/>
		<xsl:apply-templates select="Composition_EZ" mode="tExistEZParcel"/>
		<xsl:copy-of select="Area_In_GKN"/>
		<xsl:copy-of select="Delta_Area"/>
		<xsl:apply-templates select="Min_Area" mode="tExistEZParcel"/>
		<xsl:apply-templates select="Max_Area" mode="tExistEZParcel"/>
		<xsl:copy-of select="Note"/>
		<xsl:apply-templates select="RelatedParcels"/>
	</xsl:template>
	<xsl:template match="SubParcels" mode="tExistEZParcel">
		<xsl:copy>
			<xsl:apply-templates select="FormSubParcel" mode="tExistEZParcel"/>
			<xsl:apply-templates select="ExistSubParcel" mode="tExistEZParcel"/>
			<xsl:apply-templates select="InvariableSubParcel" mode="tExistEZParcel"/>
		</xsl:copy>
	</xsl:template>
	<!--Образуемая часть участка-->
	<xsl:template match="FormSubParcel" mode="tExistEZParcel">
		<xsl:copy>
			<xsl:copy-of select="@Definition"/>
			<xsl:call-template name="tSubParcel"/>
		</xsl:copy>
	</xsl:template>
	<!--Существующая (уточняемая, изменяемая ЧЗУ)-->
	<xsl:template match="ExistSubParcel" mode="tExistEZParcel">
		<xsl:copy>
			<xsl:copy-of select="@Number_Record"/>
			<xsl:call-template name="tSubParcel"/>
		</xsl:copy>
	</xsl:template>
	<!--Существующая (неизменяемая) часть участка-->
	<xsl:template match="InvariableSubParcel" mode="tExistEZParcel">
		<xsl:copy>
			<xsl:copy-of select="@Number_Record"/>
			<xsl:copy-of select="@SubParcel_Realty"/>
			<xsl:apply-templates select="Area" mode="tArea_without_Innccuracy"/>
			<xsl:apply-templates select="Encumbrance"/>
			<xsl:if test="count(Contours/child::*) != 0">
				<xsl:element name="Contours">
					<xsl:for-each select="Contours/Contour">
						<xsl:copy>
							<xsl:copy-of select="@Number"/>
							<xsl:apply-templates select="Area" mode="tArea_Contour"/>
						</xsl:copy>
					</xsl:for-each>
				</xsl:element>
			</xsl:if>
		</xsl:copy>
	</xsl:template>
	<!--СоставЕЗ-->
	<xsl:template match="Composition_EZ" mode="tExistEZParcel">
		<xsl:copy>
			<xsl:if test="count(InsertEntryParcels/child::*)>0">
				<xsl:apply-templates select="InsertEntryParcels" mode="tExistEZParcel"/>
			</xsl:if>
			<xsl:if test="count(DeleteEntryParcels/child::*)>0">
				<xsl:apply-templates select="DeleteEntryParcels" mode="tExistEZParcel"/>
			</xsl:if>
		</xsl:copy>
	</xsl:template>
	<!--Включаемые в ЕЗ зу-->
	<xsl:template match="InsertEntryParcels" mode="tExistEZParcel">
		<xsl:copy>
			<xsl:apply-templates select="InsertEntryParcel" mode="tExistEZParcel"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="InsertEntryParcel" mode="tExistEZParcel">
		<xsl:copy>
			<xsl:apply-templates select="ExistEntryParcel" mode="tExistEZParcel"/>
			<xsl:apply-templates select="NewEntryParcel" mode="tExistEZParcel"/>
		</xsl:copy>
	</xsl:template>

    <xsl:template name="uppercase">            
           <xsl:param name="input"/>            
           <xsl:value-of select="translate($input,
           'йцукенгшщзхъфывапролджэячсмитьбю',   
           'ЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮ')"/>            
       </xsl:template>     
       
	<!--Сведения о существующих обособленных и условных участках, включаемых в состав ЕЗ-->
	<xsl:template match="ExistEntryParcel" mode="tExistEZParcel">
		<xsl:copy>
			<xsl:if test="@CadastralNumber != ''">
				<xsl:attribute name="CadastralNumber"><xsl:value-of select="normalize-space(@CadastralNumber)"/></xsl:attribute>
			</xsl:if>
		</xsl:copy>
	</xsl:template>
	<!--Сведения о новых обособленных и условных участках, включаемых в ЕЗ-->
	<xsl:template match="NewEntryParcel" mode="tExistEZParcel">
		<xsl:copy>
			<xsl:call-template name="tNewEZEntryParcel"/>
		</xsl:copy>
	</xsl:template>
	<!--Исключаемые входящий участок-->
	<xsl:template match="DeleteEntryParcels" mode="tExistEZParcel">
		<xsl:copy>
			<xsl:apply-templates select="DeleteEntryParcel" mode="tExistEZParcel"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="DeleteEntryParcel" mode="tExistEZParcel">
		<xsl:copy>
			<xsl:if test="@CadastralNumber!=''">
				<xsl:attribute name="CadastralNumber"><xsl:value-of select="normalize-space(@CadastralNumber)"/></xsl:attribute>
			</xsl:if>
		</xsl:copy>
	</xsl:template>
	<!--ПредМИНразмер участка-->
	<xsl:template match="Min_Area" mode="tExistEZParcel">
		<xsl:if test="Area!=''">
			<xsl:copy>
				<xsl:call-template name="tArea_without_Innccuracy"/>
			</xsl:copy>
		</xsl:if>
	</xsl:template>
	<!--ПредМАХразмер участка-->
	<xsl:template match="Max_Area" mode="tExistEZParcel">
		<xsl:if test="Area!=''">
			<xsl:copy>
				<xsl:call-template name="tArea_without_Innccuracy"/>
			</xsl:copy>
		</xsl:if>
	</xsl:template>
	<xsl:template name="tExistParcel">
		<xsl:copy-of select="@CadastralNumber"/>
		<xsl:copy-of select="CadastralBlock"/>
		<xsl:apply-templates select="Inner_CadastralNumbers"/>
		<xsl:apply-templates select="Area" mode="tAreaNew"/>
		<xsl:apply-templates select="SubParcels" mode="tExistParcel"/>
		<xsl:apply-templates select="Entity_Spatial"/>
		<xsl:apply-templates select="Contours"/>
		<xsl:copy-of select="Area_In_GKN"/>
		<xsl:copy-of select="Delta_Area"/>
		<xsl:apply-templates select="Min_Area" mode="tExistParcel"/>
		<xsl:apply-templates select="Max_Area" mode="tExistParcel"/>
		<xsl:copy-of select="Note"/>
		<xsl:apply-templates select="RelatedParcels"/>
	</xsl:template>
	<xsl:template match="SubParcels" mode="tExistParcel">
		<xsl:copy>
			<xsl:apply-templates select="FormSubParcel" mode="tExistParcel"/>
			<xsl:apply-templates select="ExistSubParcel" mode="tExistParcel"/>
			<xsl:apply-templates select="InvariableSubParcel" mode="tExistParcel"/>
		</xsl:copy>
	</xsl:template>
	<!--Образуемая часть участка-->
	<xsl:template match="FormSubParcel" mode="tExistParcel">
		<xsl:copy>
			<xsl:copy-of select="@Definition"/>
			<xsl:call-template name="tSubParcel"/>
		</xsl:copy>
	</xsl:template>
	<!--Существующая (уточняемая, изменяемая ЧЗУ)-->
	<xsl:template match="ExistSubParcel" mode="tExistParcel">
		<xsl:copy>
			<xsl:copy-of select="@Number_Record"/>
			<xsl:call-template name="tSubParcel"/>
		</xsl:copy>
	</xsl:template>
	<!--Существующая (неизменяемая) часть участка-->
	<xsl:template match="InvariableSubParcel" mode="tExistParcel">
		<xsl:copy>
			<xsl:copy-of select="@Number_Record"/>
			<xsl:copy-of select="@SubParcel_Realty"/>
			<xsl:apply-templates select="Area" mode="tArea_without_Innccuracy"/>
			<xsl:apply-templates select="Encumbrance"/>
			<xsl:if test="count(Contours/child::*) != 0">
				<xsl:element name="Contours">
					<xsl:for-each select="Contours/Contour">
						<xsl:copy>
							<xsl:copy-of select="@Number"/>
							<xsl:apply-templates select="Area" mode="tArea_Contour"/>
						</xsl:copy>
					</xsl:for-each>
				</xsl:element>
			</xsl:if>
		</xsl:copy>
	</xsl:template>
	<!--ПредМИНразмер участка-->
	<xsl:template match="Min_Area" mode="tExistParcel">
		<xsl:if test="Area">
			<xsl:copy>
				<xsl:call-template name="tArea_without_Innccuracy"/>
			</xsl:copy>
		</xsl:if>
	</xsl:template>
	<!--ПредМАХразмер участка-->
	<xsl:template match="Max_Area" mode="tExistParcel">
		<xsl:if test="Area">
			<xsl:copy>
				<xsl:call-template name="tArea_without_Innccuracy"/>
			</xsl:copy>
		</xsl:if>
	</xsl:template>
	<!--FIO-->
	<xsl:template match="FIO" mode="tFIO">
		<xsl:copy>
			<xsl:call-template name="tFIO"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template name="tFIO">
		<xsl:if test="Surname!= ''">
			<xsl:element name="Surname">
				<xsl:value-of select="normalize-space(Surname)"/>
			</xsl:element>
		</xsl:if>
		<xsl:if test="First!= ''">
			<xsl:element name="First">
				<xsl:value-of select="normalize-space(First)"/>
			</xsl:element>
		</xsl:if>
		<xsl:if test="Patronymic!= ''">
			<xsl:element name="Patronymic">
				<xsl:value-of select="normalize-space(Patronymic)"/>
			</xsl:element>
		</xsl:if>
	</xsl:template>
	<!--Опсание лесного участка-->
	<xsl:template match="Natural_Object" mode="tNatural_Object">
		<xsl:copy>
			<xsl:call-template name="tNatural_Object"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template name="tNatural_Object">
		<xsl:copy-of select="Name"/>
		<xsl:copy-of select="ForestUse"/>
		<xsl:copy-of select="Type_ProtectiveForest"/>
	</xsl:template>
	<!--Сведения о новых обособленных и условных участках, включаемых в состав ЕЗ-->
	<xsl:template name="tNewEZEntryParcel">
		<xsl:if test="@Name != ''">
			<xsl:attribute name="Name"><xsl:value-of select="normalize-space(@Name)"/></xsl:attribute>
		</xsl:if>
		<xsl:if test="@Definition!=''">
		    <xsl:variable name="DEF" select="@Definition"/>
			<xsl:attribute name="Definition"><xsl:value-of select="normalize-space(translate($DEF,'абвгдеёжзийклмнопрстуфхцчшщъыьэюя',
            'АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ'))"/></xsl:attribute>
		</xsl:if>
		<xsl:apply-templates select="Area" mode="tAreaNew"/>
		<xsl:apply-templates select="Entity_Spatial"/>
		<xsl:apply-templates select="Encumbrance"/>
	</xsl:template>
	<!--Описание образуемого ЗУ(ЧЗУ)-->
	<xsl:template name="tNewParcel">
		<xsl:variable name="isDefinition" select="@Definition"/>
		<xsl:choose>
			<xsl:when test="starts-with($isDefinition,'ЗУ')">
				<xsl:attribute name="Definition"><xsl:value-of select="normalize-space(concat(':',$isDefinition))"/></xsl:attribute>
			</xsl:when>
			<xsl:otherwise>
				<xsl:attribute name="Definition"><xsl:value-of select="normalize-space($isDefinition)"/></xsl:attribute>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:copy-of select="CadastralBlock"/>
		<xsl:copy-of select="Prev_CadastralNumbers"/>
		<xsl:apply-templates select="Providing_Pass_CadastralNumbers" mode="tProviding_Pass_CadastralNumbers"/>
		<xsl:apply-templates select="Inner_CadastralNumbers"/>
		<xsl:apply-templates select="Area" mode="tAreaNew"/>
		<xsl:apply-templates select="Location" mode="tNewParcel"/>
		<xsl:apply-templates select="Category" mode="tCategory"/>
		<xsl:apply-templates select="NaturalObject" mode="tNaturalObject"/>
		<xsl:apply-templates select="Utilization" mode="tUtilization"/>
		<xsl:apply-templates select="SubParcels" mode="tNewParcel"/>
		<xsl:apply-templates select="Contours" mode="tNewParcel"/>
		<xsl:apply-templates select="Entity_Spatial"/>
		<xsl:apply-templates select="Min_Area" mode="tNewParcel"/>
		<xsl:apply-templates select="Max_Area" mode="tNewParcel"/>
		<xsl:copy-of select="Note"/>
	</xsl:template>
	<!--Местоположение ЗУ-->
	<xsl:template match="Location" mode="tNewParcel">
		<xsl:copy>
			<xsl:call-template name="tAddress"/>
			<xsl:apply-templates select="Document" mode="tDocument"/>
		</xsl:copy>
	</xsl:template>
	<!--Части участка-->
	<xsl:template match="SubParcels" mode="tNewParcel">
		<xsl:copy>
			<xsl:apply-templates select="FormSubParcel" mode="tNewParcel"/>
		</xsl:copy>
	</xsl:template>
	<!--Образуемая часть участка-->
	<xsl:template match="FormSubParcel" mode="tNewParcel">
		<xsl:copy>
			<xsl:copy-of select="@Definition"/>
			<xsl:call-template name="tSubParcel"/>
		</xsl:copy>
	</xsl:template>
	<!--контура МЗУ-->
	<xsl:template match="Contours" mode="tNewParcel">
		<xsl:copy>
			<xsl:apply-templates select="NewContour" mode="tNewParcel"/>
		</xsl:copy>
	</xsl:template>
	<!--НовыйКонтур-->
	<xsl:template match="NewContour" mode="tNewParcel">
		<xsl:copy>
			<xsl:copy-of select="@Definition"/>
			<xsl:call-template name="tContour"/>
			<xsl:apply-templates select="Providing_Pass_CadastralNumbers" mode="tProviding_Pass_CadastralNumbers"/>
		</xsl:copy>
	</xsl:template>
	<!--ПредМИНразмер участка-->
	<xsl:template match="Min_Area" mode="tNewParcel">
		<xsl:if test="Area!=''">
			<xsl:copy>
				<xsl:call-template name="tArea_without_Innccuracy"/>
			</xsl:copy>
		</xsl:if>
	</xsl:template>
	<!--ПредМАХразмер участка-->
	<xsl:template match="Max_Area" mode="tNewParcel">
		<xsl:if test="Area!=''">
			<xsl:copy>
				<xsl:call-template name="tArea_without_Innccuracy"/>
			</xsl:copy>
		</xsl:if>
	</xsl:template>
	<!--Описание образуемой ЧЗУ-->
	<xsl:template name="tNewSubParcel">
		<xsl:copy-of select="@Definition"/>
		<xsl:copy-of select="@SubParcel_Realty"/>
		<xsl:copy-of select="CadastralNumber_Parcel"/>
		<xsl:apply-templates select="Area" mode="tArea"/>
		<xsl:apply-templates select="Encumbrance"/>
		<xsl:apply-templates select="Entity_Spatial" mode="tNewSubParcel"/>
		<xsl:apply-templates select="Contours" mode="tNewSubParcel"/>
	</xsl:template>
	<!--Местоположение границ-->
	<xsl:template match="Entity_Spatial" mode="tNewSubParcel">
		<xsl:copy>
			<xsl:copy-of select="@Ent_Sys"/>
			<xsl:for-each select="Spatial_Element">
				<xsl:copy>
					<xsl:call-template name="tSPATIAL_ELEMENT_OLD_NEW"/>
				</xsl:copy>
			</xsl:for-each>
		</xsl:copy>
	</xsl:template>
	<!--Контура многоконтурной части-->
	<xsl:template match="Contours" mode="tNewSubParcel">
		<xsl:copy>
			<xsl:for-each select="Contour">
				<xsl:copy>
					<xsl:copy-of select="@Number"/>
					<xsl:apply-templates select="Area" mode="tArea_Contour"/>
					<xsl:apply-templates select="Entity_Spatial" mode="tNewSubParcel"/>
				</xsl:copy>
			</xsl:for-each>
		</xsl:copy>
	</xsl:template>
	<!--Координата-->
	<xsl:template match="NewOrdinate" mode="tOrdinate">
		<xsl:if test="(@X!='')and(@Y!='')">
			<xsl:copy>
				<xsl:call-template name="tOrdinate"/>
			</xsl:copy>
		</xsl:if>
	</xsl:template>
	<xsl:template match="OldOrdinate" mode="tOrdinate">
		<xsl:if test="(@X!='')and(@Y!='')">
			<xsl:copy>
				<xsl:call-template name="tOrdinate"/>
			</xsl:copy>
		</xsl:if>
	</xsl:template>
	<xsl:template name="tOrdinate">
		<xsl:copy-of select="@X"/>
		<xsl:copy-of select="@Y"/>
		<xsl:copy-of select="@Num_Geopoint"/>
		<xsl:copy-of select="@Geopoint_Zacrep"/>
		<xsl:copy-of select="@Delta_Geopoint"/>
		<xsl:copy-of select="@Point_Pref"/>
	</xsl:template>
	<!--Кадастровые номера ЗУ, посредством которых обеспечивается доступ-->
	<xsl:template match="Providing_Pass_CadastralNumbers" mode="tProviding_Pass_CadastralNumbers">
		<xsl:copy>
			<xsl:call-template name="tProviding_Pass_CadastralNumbers"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template name="tProviding_Pass_CadastralNumbers">
		<xsl:copy-of select="CadastralNumber"/>
		<xsl:copy-of select="Definition"/>
		<xsl:if test="Other!=''">
			<xsl:element name="Other">
				<xsl:value-of select="normalize-space(Other)"/>
			</xsl:element>
		</xsl:if>
		<xsl:if test="count(Documents/child::*) != 0">
			<xsl:apply-templates select="Documents" mode="Document"/>
		</xsl:if>
	</xsl:template>
	<!--Элемент контура-->
	<xsl:template name="tSPATIAL_ELEMENT_OLD_NEW">
		<xsl:for-each select="Spelement_Unit">
			<xsl:copy>
				<xsl:call-template name="tSPELEMENT_UNIT_OLD_NEW"/>
			</xsl:copy>
		</xsl:for-each>
	</xsl:template>
	<!--уточненние границ смежного ЗУ-->
	<xsl:template match="SpecifyRelatedParcel" mode="tSpecifyRelatedParcel">
		<xsl:copy>
			<xsl:call-template name="tSpecifyRelatedParcel"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template name="tSpecifyRelatedParcel">
		<xsl:if test="@CadastralNumber!=''">
			<xsl:attribute name="CadastralNumber"><xsl:value-of select="normalize-space(@CadastralNumber)"/></xsl:attribute>
		</xsl:if>
		<xsl:if test="@Number_Record!=''">
			<xsl:attribute name="Number_Record"><xsl:value-of select="normalize-space(@Number_Record)"/></xsl:attribute>
		</xsl:if>
		<xsl:apply-templates select="AllBorder" mode="tSpecifyRelatedParcel"/>
		<xsl:apply-templates select="ChangeBorder" mode="tSpecifyRelatedParcel"/>
		<xsl:apply-templates select="Contours" mode="tSpecifyRelatedParcel"/>
		<xsl:apply-templates select="DeleteAllBorder" mode="tSpecifyRelatedParcel"/>
		<xsl:apply-templates select="ExistSubParcels" mode="tSpecifyRelatedParcel"/>
	</xsl:template>
	<xsl:template match="AllBorder" mode="tSpecifyRelatedParcel">
		<xsl:copy>
			<xsl:apply-templates select="Entity_Spatial"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="ChangeBorder" mode="tSpecifyRelatedParcel">
		<xsl:copy>
			<xsl:apply-templates select="OldOrdinate" mode="tOrdinate"/>
			<xsl:apply-templates select="NewOrdinate" mode="tOrdinate"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Contours" mode="tSpecifyRelatedParcel">
		<xsl:copy>
			<xsl:for-each select="NewContour">
				<xsl:copy>
					<xsl:if test="@Definition!=''">
						<xsl:attribute name="Definition"><xsl:value-of select="normalize-space(@Definition)"/></xsl:attribute>
					</xsl:if>
					<xsl:call-template name="tContour"/>
					<xsl:apply-templates select="Providing_Pass_CadastralNumbers" mode="tProviding_Pass_CadastralNumbers"/>
				</xsl:copy>
			</xsl:for-each>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="DeleteAllBorder" mode="tSpecifyRelatedParcel">
		<xsl:copy>
			<xsl:apply-templates select="OldOrdinate" mode="tOrdinate"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="ExistSubParcels" mode="tSpecifyRelatedParcel">
		<xsl:if test="ExistSubParcel">
			<xsl:copy>
				<xsl:apply-templates select="ExistSubParcel" mode="tSpecifyRelatedParcel"/>
			</xsl:copy>
		</xsl:if>
	</xsl:template>
	<xsl:template match="ExistSubParcel" mode="tSpecifyRelatedParcel">
		<xsl:copy>
			<xsl:if test="@Number_Record!=''">
				<xsl:attribute name="Number_Record"><xsl:value-of select="normalize-space(@Number_Record)"/></xsl:attribute>
			</xsl:if>
			<xsl:apply-templates select="Entity_Spatial" mode="tSpecifyRelatedParcel"/>
			<xsl:apply-templates select="Contours" mode="tSpecifyRelatedParcel"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Entity_Spatial" mode="tSpecifyRelatedParcel">
		<xsl:copy>
			<xsl:copy-of select="@Ent_Sys"/>
			<xsl:for-each select="Spatial_Element">
				<xsl:copy>
					<xsl:call-template name="tSPATIAL_ELEMENT_OLD_NEW"/>
				</xsl:copy>
			</xsl:for-each>
		</xsl:copy>
	</xsl:template>
	<!--xsl:template match="Contours" mode="tSpecifyRelatedParcel">
		<xsl:copy>
			<xsl:for-each select="Contour">
				<xsl:copy>
					<xsl:copy-of select="@Number"/>
					<xsl:apply-templates select="Entity_Spatial" mode="tSpecifyRelatedParcel"/>
				</xsl:copy>
			</xsl:for-each>
		</xsl:copy>
	</xsl:template-->
	<!--Части элементов новых и существующих контуров (точка)-->
	<xsl:template name="tSPELEMENT_UNIT_OLD_NEW">
		<!--xsl:copy-of select="@Type_Unit"/-->
		<xsl:attribute name="Type_Unit"><xsl:text>Точка</xsl:text></xsl:attribute>
		<xsl:apply-templates select="NewOrdinate" mode="tOrdinate"/>
		<xsl:apply-templates select="OldOrdinate" mode="tOrdinate"/>
	</xsl:template>
	<!--Описание части земельного участка при образовании ЗУ или уточнении границ ЗУ-->
	<xsl:template name="tSubParcel">
		<xsl:copy-of select="@SubParcel_Realty"/>
		<xsl:apply-templates select="Area" mode="tArea"/>
		<xsl:apply-templates select="Encumbrance"/>
		<xsl:choose>
			<xsl:when test="Entity_Spatial">
				<xsl:apply-templates select="Entity_Spatial"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:apply-templates select="Contours" mode="tSubParcel"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="Contours" mode="tSubParcel">
		<xsl:copy>
			<xsl:for-each select="Contour">
				<xsl:copy>
					<xsl:copy-of select="@Number"/>
					<xsl:apply-templates select="Area" mode="tArea_Contour"/>
					<xsl:apply-templates select="Entity_Spatial" mode="tSubParcel"/>
				</xsl:copy>
			</xsl:for-each>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Entity_Spatial" mode="tSubParcel">
		<xsl:copy>
			<xsl:copy-of select="@Ent_Sys"/>
			<xsl:for-each select="Spatial_Element">
				<xsl:copy>
					<xsl:call-template name="tSPATIAL_ELEMENT_OLD_NEW"/>
				</xsl:copy>
			</xsl:for-each>
		</xsl:copy>
	</xsl:template>
	<!--Вид использования ЗУ-->
	<xsl:template match="Utilization" mode="tUtilization">
		<xsl:copy>
			<xsl:call-template name="tUtilization"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template name="tUtilization">
		<xsl:if test="@Utilization!=''">
			<xsl:attribute name="Utilization"><xsl:value-of select="normalize-space(@Utilization)"/></xsl:attribute>
		</xsl:if>
		<xsl:if test="@ByDoc!=''">
			<xsl:attribute name="ByDoc"><xsl:value-of select="normalize-space(@ByDoc)"/></xsl:attribute>
		</xsl:if>
		<xsl:if test="@AdditionalName!=''">
			<xsl:attribute name="AdditionalName"><xsl:value-of select="normalize-space(@AdditionalName)"/></xsl:attribute>
		</xsl:if>
		<xsl:apply-templates select="DocUtilization" mode="tUtilization"/>
	</xsl:template>
	<xsl:template match="DocUtilization" mode="tUtilization">
		<xsl:copy>
			<xsl:call-template name="tDocument"/>
		</xsl:copy>
	</xsl:template>
	<!--****************************************/ComplexType****************************************-->
	<!--Кадастровые или иные номера зданий, сооружений, объектов незавершенного строительства, расположенных на земельном участке-->
	<xsl:template match="Inner_CadastralNumbers">
		<xsl:call-template name="tInnerCadNum"/>
	</xsl:template>
	<xsl:template name="tInnerCadNum">
		<xsl:copy>
			<xsl:if test="CadastralNumber!=''">
				<xsl:copy-of select="CadastralNumber"/>
			</xsl:if>
			<xsl:if test="Number != ''">
				<xsl:copy-of select="Number"/>
			</xsl:if>
		</xsl:copy>
	</xsl:template>
	<!--Система координат-->
	<xsl:template name="Coord_Systems_def">
		<Coord_Systems>
			<Coord_System Cs_Id="SystemCoord" Name="СК кадастрового округа"/>
		</Coord_Systems>
	</xsl:template>
	<!--Титульный лист-->
	<xsl:template match="Title">
		<xsl:copy>
			<xsl:apply-templates select="Contractor" mode="Title"/>
			<xsl:if test="Purpose != ''">
				<xsl:element name="Purpose">
					<xsl:value-of select="normalize-space(Purpose)"/>
				</xsl:element>
			</xsl:if>
			<xsl:if test="Reason != ''">
				<xsl:element name="Reason">
					<xsl:value-of select="normalize-space(Reason)"/>
				</xsl:element>
				<xsl:apply-templates select="Client" mode="Title"/>
			</xsl:if>
		</xsl:copy>
	</xsl:template>
	<!--Кадастровый инженер-->
	<xsl:template match="Contractor" mode="Title">
		<xsl:copy>
			<xsl:copy-of select="@Date"/>
			<xsl:apply-templates select="FIO" mode="tFIO"/>
			<xsl:if test="N_Certificate!= ''">
				<xsl:element name="N_Certificate">
					<xsl:value-of select="normalize-space(N_Certificate)"/>
				</xsl:element>
			</xsl:if>
			<xsl:if test="Telephone!=''">
				<xsl:element name="Telephone">
					<xsl:value-of select="normalize-space(Telephone)"/>
				</xsl:element>
			</xsl:if>
			<xsl:if test="Address!= ''">
				<xsl:element name="Address">
					<xsl:value-of select="normalize-space(Address)"/>
				</xsl:element>
			</xsl:if>
			<xsl:if test="E_mail!= ''">
				<xsl:element name="E_mail">
					<xsl:value-of select="normalize-space(E_mail)"/>
				</xsl:element>
			</xsl:if>
			<xsl:if test="Organization != ''">
				<xsl:element name="Organization">
					<xsl:value-of select="normalize-space(Organization)"/>
				</xsl:element>
			</xsl:if>
		</xsl:copy>
	</xsl:template>
	<!--Сведения о заказчике-->
	<xsl:template match="Client" mode="Title">
		<xsl:copy>
			<xsl:copy-of select="@Date"/>
			<xsl:apply-templates select="Person" mode="Title"/>
			<xsl:apply-templates select="Organization" mode="Title"/>
			<xsl:apply-templates select="Foreign_Organization" mode="Title"/>
			<xsl:apply-templates select="Governance" mode="Title"/>
		</xsl:copy>
	</xsl:template>
	<!--Физическое лицо-->
	<xsl:template match="Person" mode="Title">
		<xsl:copy>
			<xsl:apply-templates select="FIO" mode="tFIO"/>
		</xsl:copy>
	</xsl:template>
	<!--Organization Российское юридическое лицо-->
	<xsl:template match="Organization" mode="Title">
		<xsl:copy>
			<xsl:if test="Name!=''">
				<xsl:element name="Name">
					<xsl:value-of select="normalize-space(Name)"/>
				</xsl:element>
			</xsl:if>
			<xsl:apply-templates select="Agent" mode="Title"/>
		</xsl:copy>
	</xsl:template>
	<!--Foreign_Organization Иностранное юридическое лицо-->
	<xsl:template match="Foreign_Organization" mode="Title">
		<xsl:copy>
			<xsl:if test="Name!=''">
				<xsl:element name="Name">
					<xsl:value-of select="normalize-space(Name)"/>
				</xsl:element>
			</xsl:if>
			<xsl:if test="Country!=''">
				<xsl:element name="Country">
					<xsl:value-of select="normalize-space(Country)"/>
				</xsl:element>
			</xsl:if>
			<xsl:apply-templates select="Agent" mode="Title"/>
		</xsl:copy>
	</xsl:template>
	<!--Орган гос. власти-->
	<xsl:template match="Governance" mode="Title">
		<xsl:copy>
			<xsl:if test="Name!=''">
				<xsl:element name="Name">
					<xsl:value-of select="normalize-space(Name)"/>
				</xsl:element>
			</xsl:if>
			<xsl:apply-templates select="Agent" mode="Title"/>
		</xsl:copy>
	</xsl:template>
	<!--Agent Представитель-->
	<xsl:template match="Agent" mode="Title">
		<xsl:copy>
			<xsl:call-template name="tFIO"/>
			<xsl:if test="Appointment != ''">
				<xsl:element name="Appointment">
					<xsl:value-of select="normalize-space(Appointment)"/>
				</xsl:element>
			</xsl:if>
		</xsl:copy>
	</xsl:template>
	<!--Пакет информации-->
	<xsl:template match="Package">
		<xsl:copy>
			<xsl:apply-templates select="SpecifyParcel" mode="Package"/>
			<xsl:apply-templates select="FormParcels" mode="Package"/>
			<xsl:apply-templates select="NewSubParcel" mode="Package"/>
		</xsl:copy>
	</xsl:template>
	<!-- Образование частей -->
	<xsl:template match="NewSubParcel" mode="Package">
		<xsl:copy>
			<xsl:call-template name="tNewSubParcel"/>
		</xsl:copy>
	</xsl:template>
	<!--SpecifyParcels уточнение границ-->
	<xsl:template match="SpecifyParcel" mode="Package">
		<xsl:copy>
			<xsl:apply-templates select="ExistParcel" mode="SpecifyParcels"/>
			<xsl:apply-templates select="ExistEZ" mode="SpecifyParcels"/>
			<xsl:apply-templates select="SpecifyRelatedParcel" mode="tSpecifyRelatedParcel"/>
		</xsl:copy>
	</xsl:template>
	<!--Сведения об уточняемых ЗУ (не ЕЗ)-->
	<xsl:template match="ExistParcel" mode="SpecifyParcels">
		<xsl:copy>
			<xsl:call-template name="tExistParcel"/>
		</xsl:copy>
	</xsl:template>
	<!--Сведения об уточняемых ЗУ (ЕЗ)-->
	<xsl:template match="ExistEZ" mode="SpecifyParcels">
		<xsl:copy>
			<xsl:apply-templates select="ExistEZParcels" mode="SpecifyParcels"/>
			<xsl:apply-templates select="ExistEZEntryParcels" mode="SpecifyParcels"/>
		</xsl:copy>
	</xsl:template>
	<!--Уточняемое ЕЗ-->
	<xsl:template match="ExistEZParcels" mode="SpecifyParcels">
		<xsl:copy>
			<xsl:call-template name="tExistEZParcel"/>
		</xsl:copy>
	</xsl:template>
	<!--Уточняемые входящие-->
	<xsl:template match="ExistEZEntryParcels" mode="SpecifyParcels">
		<xsl:if test="ExistEZEntryParcel">
			<xsl:copy>
				<xsl:apply-templates select="ExistEZEntryParcel" mode="SpecifyParcels"/>
			</xsl:copy>
		</xsl:if>
	</xsl:template>
	<xsl:template match="ExistEZEntryParcel" mode="SpecifyParcels">
		<xsl:copy>
			<xsl:call-template name="tExistEZEntryParcel"/>
		</xsl:copy>
	</xsl:template>
	<!--Образование участков-->
	<xsl:template match="FormParcels" mode="Package">
		<xsl:copy>
			<xsl:copy-of select="@Method"/>
			<xsl:apply-templates select="NewParcel" mode="FormParcels"/>
			<xsl:apply-templates select="ChangeParcel" mode="FormParcels"/>
			<xsl:apply-templates select="SpecifyRelatedParcel" mode="tSpecifyRelatedParcel"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="NewParcel" mode="FormParcels">
		<xsl:copy>
			<xsl:call-template name="tNewParcel"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="ChangeParcel" mode="FormParcels">
		<xsl:copy>
			<xsl:call-template name="tChangeParcel"/>
		</xsl:copy>
	</xsl:template>
	<!--Исходные данные-->
	<xsl:template match="Input_Data">
		<xsl:copy>
			<xsl:if test="Documents/Document">
				<xsl:element name="Documents">
					<xsl:for-each select="Documents/Document">
						<xsl:copy>
							<xsl:call-template name="tDocument"/>
							<xsl:if test="Scale != ''">
								<xsl:element name="Scale">
									<xsl:value-of select="normalize-space(Scale)"/>
								</xsl:element>
							</xsl:if>
							<xsl:if test="Date_Create!= ''">
								<xsl:element name="Date_Create">
									<xsl:value-of select="normalize-space(Date_Create)"/>
								</xsl:element>
							</xsl:if>
							<xsl:if test="Date_Update!= ''">
								<xsl:element name="Date_Update">
									<xsl:value-of select="normalize-space(Date_Update)"/>
								</xsl:element>
							</xsl:if>
						</xsl:copy>
					</xsl:for-each>
				</xsl:element>
			</xsl:if>
			<xsl:if test="Geodesic_Bases/Geodesic_Base">
				<xsl:element name="Geodesic_Bases">
					<xsl:for-each select="Geodesic_Bases/Geodesic_Base">
						<xsl:copy>
							<xsl:if test="PName!=''">
								<xsl:element name="PName">
									<xsl:value-of select="normalize-space(PName)"/>
								</xsl:element>
							</xsl:if>
							<xsl:if test="PKind!=''">
								<xsl:element name="PKind">
									<xsl:value-of select="normalize-space(PKind)"/>
								</xsl:element>
							</xsl:if>
							<xsl:if test="PKlass!=''">
								<xsl:element name="PKlass">
									<xsl:value-of select="normalize-space(PKlass)"/>
								</xsl:element>
							</xsl:if>
							<xsl:if test="OrdX!=''">
								<xsl:element name="OrdX">
									<xsl:value-of select="normalize-space(OrdX)"/>
								</xsl:element>
							</xsl:if>
							<xsl:if test="OrdY!=''">
								<xsl:element name="OrdY">
									<xsl:value-of select="normalize-space(OrdY)"/>
								</xsl:element>
							</xsl:if>
						</xsl:copy>
					</xsl:for-each>
				</xsl:element>
			</xsl:if>
			<xsl:if test="Means_Survey/Means_Survey">
				<xsl:element name="Means_Survey">
					<xsl:for-each select="Means_Survey/Means_Survey">
						<xsl:copy>
							<xsl:if test="Name!=''">
								<xsl:element name="Name">
									<xsl:value-of select="normalize-space(Name)"/>
								</xsl:element>
							</xsl:if>
							<xsl:if test="Certificate != ''">
								<xsl:element name="Certificate">
									<xsl:value-of select="normalize-space(Certificate)"/>
								</xsl:element>
							</xsl:if>
							<xsl:if test="Certificate_Verification!=''">
								<xsl:element name="Certificate_Verification">
									<xsl:value-of select="normalize-space(Certificate_Verification)"/>
								</xsl:element>
							</xsl:if>
						</xsl:copy>
					</xsl:for-each>
				</xsl:element>
			</xsl:if>
			<xsl:if test="Realty/OKS">
				<xsl:element name="Realty">
					<xsl:for-each select="Realty/OKS">
						<xsl:copy>
							<xsl:if test="CadastralNumber!=''">
								<xsl:element name="CadastralNumber">
									<xsl:value-of select="normalize-space(CadastralNumber)"/>
								</xsl:element>
							</xsl:if>
							<xsl:if test="CadastralNumber_OtherNumber!=''">
								<xsl:element name="CadastralNumber_OtherNumber">
									<xsl:value-of select="normalize-space(CadastralNumber_OtherNumber)"/>
								</xsl:element>
							</xsl:if>
							<xsl:if test="Name_OKS != ''">
								<xsl:element name="Name_OKS">
									<xsl:value-of select="normalize-space(Name_OKS)"/>
								</xsl:element>
							</xsl:if>
						</xsl:copy>
					</xsl:for-each>
				</xsl:element>
			</xsl:if>
			<xsl:if test="SubParcels/SubParcel">
				<xsl:element name="SubParcels">
					<xsl:for-each select="SubParcels/SubParcel">
						<xsl:copy>
							<xsl:copy-of select="CadastralNumber"/>
							<xsl:copy-of select="Number_Record"/>
						</xsl:copy>
					</xsl:for-each>
				</xsl:element>
			</xsl:if>
		</xsl:copy>
	</xsl:template>
	<!--Выполненные измерения-->
	<xsl:template match="Survey">
		<xsl:if test="AppliedFile">
			<xsl:copy>
				<xsl:apply-templates select="AppliedFile" mode="tAppliedFile"/>
			</xsl:copy>
		</xsl:if>
	</xsl:template>
	<!--заключение-->
	<xsl:template match="Conclusion">
		<xsl:copy>
			<xsl:value-of select="normalize-space(self::node())"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Scheme_Geodesic_Plotting">
		<xsl:copy>
			<xsl:apply-templates select="AppliedFile" mode="tAppliedFile"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Scheme_Disposition_Parcels">
		<xsl:copy>
			<xsl:apply-templates select="AppliedFile" mode="tAppliedFile"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Diagram_Parcels_SubParcels">
		<xsl:copy>
			<xsl:apply-templates select="AppliedFile" mode="tAppliedFile"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Agreement_Document">
		<xsl:if test="AppliedFile">
			<xsl:copy>
				<xsl:apply-templates select="AppliedFile" mode="tAppliedFile"/>
			</xsl:copy>
		</xsl:if>
	</xsl:template>
	<xsl:template match="NodalPointSchemes">
		<xsl:if test="NodalPointScheme">
			<xsl:copy>
				<xsl:for-each select="NodalPointScheme">
					<xsl:copy>
						<xsl:copy-of select="@Definition"/>
						<xsl:apply-templates select="AppliedFile" mode="tAppliedFile"/>
					</xsl:copy>
				</xsl:for-each>
			</xsl:copy>
		</xsl:if>
	</xsl:template>
	<xsl:template match="Appendix">
		<xsl:if test="Document">
			<xsl:copy>
				<xsl:apply-templates select="Document" mode="tDocument"/>
			</xsl:copy>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
