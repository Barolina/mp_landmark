unit YouTrack.Intf;

interface

uses
  WinAPI.Windows,
  Issues.API.Intf;

type


IBagTracker = interface
  procedure SetHost(const Value: string);
  procedure SetPort(const Value: integer);
  procedure SetProxyHost(const Value: string);
  procedure SetProxyPort(const Value: integer);
  procedure SetProgectName(const Value: string);
  procedure SetLogin(const Value:string);
  procedure SetPassword(const Value:string);

  function GetHost: string;
  function GetPort: integer;
  function GetProxyHost: string;
  function GetProxyPort: integer;
  function GetProjectName: string;
  function GetLogin:string;
  function GetPassword:string;

  procedure Connect(aLogin, aPassword:string);
  procedure ExecuteDialog_Login(aParent:HWND);
  procedure ExecuteDialog_NewIssue(aParent:HWND);
  function IsConnected:boolean;

  property Host:string read GetHost write SetHost;
  property Port:integer read GetPort write SetPort;
  property ProxyHost:string read GetProxyHost write SetProxyHost;
  property ProxyPort:integer read GetProxyPort write SetProxyPort;
  property ProjectName:string read GetProjectName write SetProgectName;
  property Login:string read GetLogin write SetLogin;
  property Password: string read GetPassword write SetPassword;
end;


implementation

end.
