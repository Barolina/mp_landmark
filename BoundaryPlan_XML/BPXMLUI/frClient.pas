unit frClient;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,

  STD_MP, Vcl.ExtCtrls, Vcl.StdCtrls,
  frPerson, frOrganisation, frForeign_Organisation,XMLIntf, Vcl.Mask,
  Vcl.ComCtrls, Vcl.ToolWin,frGovernance,unRussLang, JvExMask, JvToolEdit,
  JvMaskEdit, JvCheckedMaskEdit, JvDatePickerEdit;

type
  TFrameClient = class(TFrame)
    ToolBarClient: TToolBar;
    ToolButPerson: TToolButton;
    ToolButRusshOrg: TToolButton;
    ToolButInostrOrg: TToolButton;
    pnlData: TPanel;
    Label1: TLabel;
    ToolButGovernance: TToolButton;
    jvdtpckrdtDate: TJvDatePickerEdit;
    procedure mskEdDateExit(Sender: TObject);
    procedure ToolBarClientClick(Sender: TObject);
    procedure jvdtpckrdtDateExit(Sender: TObject);
  private
    { Private declarations }
    FXMLClient           : IXMLSTD_MP_Title_Client;

    FPerson              : TFramePerson;
    FOrganisation        : TFrameOrganisation;
    FForeign_Organisation: TFrameForeign_Organisation;
    FGovernance          : TFrameGovernance;
    frmt                 : TFormatSettings;

  protected
    procedure InitializationUI;
    procedure SetClient(aClient: IXMLSTD_MP_Title_Client);

    procedure UI_Date_WriteTo(aClient: IXMLSTD_MP_Title_Client);
    procedure UI_Date_ReadTo(aClient: IXMLSTD_MP_Title_Client);
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    function  IsAssignedClient: boolean;

      { ��� ��� ������ ����������� }
    procedure SetDisplayPerson(fl : Boolean);
    procedure SetDisplayOrganisation(fl : Boolean);
    procedure SetDisplayForeign_Organisation(fl : Boolean);
    procedure SetDisplayGovernance(fl : Boolean);

    procedure SetTheFormatData(formatData  : string; const aSeparator : Char = '-');
    procedure SetCurrentPerson();
    procedure SetCurrentOrganisation();
    procedure SetCurrentForeign_Organisation();
    procedure SetCurrentGovernance();

    property XMlClient: IXMLSTD_MP_Title_Client read FXMLClient write SetClient;
  end;

implementation

uses MsXmlApi;

{$R *.dfm}

{ TFrameClient }

constructor TFrameClient.Create(AOwner: TComponent);
begin
  inherited;
  FPerson := TFramePerson.Create(self);
  FPerson.Parent := self;
  FPerson.Align := alClient;
  FPerson.Visible := true;
  ToolBarClient.Tag := 1; {��� ������������ OnclickToolobar}

  FOrganisation := TFrameOrganisation.Create(self);
  FOrganisation.Parent := self;
  FOrganisation.Align := alClient;
  FOrganisation.Visible := false;

  FForeign_Organisation := TFrameForeign_Organisation.Create(self);
  FForeign_Organisation.Parent := self;
  FForeign_Organisation.Align := alClient;
  FForeign_Organisation.Visible := false;

  FGovernance := TFrameGovernance.Create(self);
  FGovernance.Parent := self;
  FGovernance.Align := alClient;
  FGovernance.Visible := false;

end;

procedure TFrameClient.InitializationUI;
var
 _node: IXMLNode;
begin
 if IsAssignedClient then
 begin
  UI_Date_ReadTo(FXMLClient);
  ///
  ///Person
  ///
  _node:= self.FXMLClient.ChildNodes.FindNode(cnstXmlPerson);
  ToolButPerson.Down := (_node<> nil);
  SetDisplayPerson(_node <> nil);
  ///
  ///Organisaion
  ///
  _node:= self.FXMLClient.ChildNodes.FindNode(cnstXmlOrganization);
  ToolButRusshOrg.Down  := (_node <> nil);
  SetDisplayOrganisation(_node <> nil);
  ///
  ///Foreign Organisaion
  ///
  _node:= self.FXMLClient.ChildNodes.FindNode(cnstXmlForeign_Organization);
  ToolButInostrOrg.Down := (_node<> nil);
  SetDisplayForeign_Organisation(_node<> nil);
  ///
  ///Governance
  ///
  _node:= self.FXMLClient.ChildNodes.FindNode(cnstXmlGovernance);
  ToolButGovernance.Down := (_node <> nil);
  SetDisplayGovernance(_node<> nil);

  _node := nil;
 end
 else
 begin
   FPerson.XMLPerson := nil;
   FOrganisation.XMLTitleOrganisation := nil;
   FForeign_Organisation.XMLTitleOrganisation := nil;
   FGovernance.XMLTitleOrganisation  := nil;
   jvdtpckrdtDate.Text := '';
 end;
end;

function TFrameClient.IsAssignedClient: boolean;
begin
  Result := FXMLClient <> nil;
end;

procedure TFrameClient.jvdtpckrdtDateExit(Sender: TObject);
begin
 if not self.IsAssignedClient then raise Exception.Create(ExMSG_NotAssignedROOT);
 self.UI_Date_WriteTo(self.FXMLClient);
end;

procedure TFrameClient.mskEdDateExit(Sender: TObject);
begin
 if not self.IsAssignedClient then raise Exception.Create(ExMSG_NotAssignedROOT);
 self.UI_Date_WriteTo(self.FXMLClient);
end;

procedure TFrameClient.SetClient(aClient: IXMLSTD_MP_Title_Client);
begin
  if FXMLClient = aClient then Exit;
  self.FXMLClient  := aClient;
  InitializationUI;
end;

procedure TFrameClient.SetCurrentForeign_Organisation;
begin
   if not IsAssignedClient then  Exit;
   self.ToolButInostrOrg.Down:= true;
   SetDisplayForeign_Organisation(true);
   MsXml_RemoveChildNode(FXMLClient,cnstXmlPerson);
   SetDisplayPerson(false);
   MsXml_RemoveChildNode(FXMLClient,cnstXmlOrganization);
   SetDisplayOrganisation(false);
   MsXml_RemoveChildNode(FXMLClient,cnstXmlGovernance);
   SetDisplayGovernance(false);

end;

procedure TFrameClient.SetCurrentGovernance;
begin
   if not IsAssignedClient  then Exit;
   self.ToolButGovernance.Down:= true;
   SetDisplayGovernance(true);
   MsXml_RemoveChildNode(FXMLClient,cnstXmlForeign_Organization);
   SetDisplayForeign_Organisation(false);
   MsXml_RemoveAttribute(FXMLClient,cnstXmlPerson);
   SetDisplayPerson(false);
   MsXml_RemoveChildNode(FXMLClient,cnstXmlOrganization);
   SetDisplayOrganisation(false);
end;

procedure TFrameClient.SetCurrentOrganisation;
begin
   if not IsAssignedClient  then Exit;
   self.ToolButRusshOrg.Down:= true;
   SetDisplayOrganisation(true);
   MsXml_RemoveChildNode(FXMLClient,cnstXmlForeign_Organization);
   SetDisplayForeign_Organisation(false);
   MsXml_RemoveAttribute(FXMLClient,cnstXmlPerson);
   SetDisplayPerson(false);
   MsXml_RemoveChildNode(FXMLClient,cnstXmlGovernance);
   SetDisplayGovernance(false);
end;

procedure TFrameClient.SetCurrentPerson;
begin
   if not IsAssignedClient then Exit;
   self.ToolButPerson.Down:= true;
   SetDisplayPerson(true);
   MsXml_RemoveChildNode(FXMLClient,cnstXmlOrganization);
   SetDisplayOrganisation(false);
   MsXml_RemoveChildNode(FXMLClient,cnstXmlForeign_Organization);
   SetDisplayForeign_Organisation(false);
   MsXml_RemoveChildNode(FXMLClient,cnstXmlGovernance);
   SetDisplayGovernance(false);
end;

procedure TFrameClient.SetDisplayForeign_Organisation;
begin
  Self.FForeign_Organisation.Visible := fl;
  if fl then FForeign_Organisation.XMLTitleOrganisation := FXMLClient.Foreign_Organization
  else self.FForeign_Organisation.XMLTitleOrganisation := nil;
end;

procedure TFrameClient.SetDisplayGovernance;
begin
  self.FGovernance.Visible := fl;
  if fl then FGovernance.XMLTitleOrganisation := FXMLClient.Governance
  else Self.FGovernance.XMLTitleOrganisation := nil;
end;

procedure TFrameClient.SetDisplayOrganisation;
begin
 FOrganisation.Visible :=  fl;
 if fl then FOrganisation.XMLTitleOrganisation := FXMLClient.Organization
 else self.FOrganisation.XMLTitleOrganisation := nil;
end;

procedure TFrameClient.SetDisplayPerson;
begin
 FPerson.Visible := fl;
 if fl  then   Self.FPerson.XMLPerson := FXMLClient.Person
 else  Self.FPerson.XMLPerson := nil;
end;

procedure TFrameClient.SetTheFormatData(formatData  : string; const aSeparator : Char = '-');
begin
  frmt.DateSeparator := aSeparator;
  frmt.ShortDateFormat := formatData;
end;

procedure TFrameClient.ToolBarClientClick(Sender: TObject);
     procedure  SetClient(index : Integer);
     begin
        case index of
          0 :  SetCurrentPerson;
          1 :  SetCurrentOrganisation;
          2 :  SetCurrentForeign_Organisation;
          3 :  SetCurrentGovernance;
        end;
     end;
begin
 if (not (Sender is TToolButton)) then EXIT;
 if MessageDlg(rsChangeTypeClient, mtConfirmation, mbYesNo,-1) = mrYes  then
    SetClient((Sender as TToolButton).Index) else
  InitializationUI;
end;

procedure TFrameClient.UI_Date_ReadTo(aClient: IXMLSTD_MP_Title_Client);
var
  _node: IXMLNode;
  str  : string;
begin
 jvdtpckrdtDate.Clear;
 str :=MsXml_ReadAttribute(aClient,cnstXmlDate);
 if  str <> '' then jvdtpckrdtDate.Text := str;
end;

procedure TFrameClient.UI_Date_WriteTo(aClient: IXMLSTD_MP_Title_Client);
begin
  aClient.Date := jvdtpckrdtDate.Text;
end;

end.
