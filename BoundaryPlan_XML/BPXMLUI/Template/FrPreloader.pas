unit FrPreloader;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, ButtonGroup, StdCtrls, ActnList, JvBaseDlg, JvProgressDialog, JvWaitingGradient,
  JvExControls, JvAnimatedImage, JvGIFCtrl;

type
  TFrLoader = class(TForm)
    dlg1: TJvProgressDialog;
    jvgfnmtr: TJvGIFAnimator;
    jvwtngrdnt1: TJvWaitingGradient;
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    { Private declarations }
     class  var _Inctanse : TFrloader;   
     procedure SetRun(fl  : Boolean);
  public     
     class function Inctance : TFrLoader; static;
     property RunAnimation : Boolean write SetRun;
    { Public declarations }
  end;

implementation

{$R *.dfm}

{ TFrLoader }


procedure TFrLoader.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
 if CanClose then Exit;
 
end;

class function TFrLoader.Inctance: TFrLoader;
begin
 if not Assigned(_Inctanse) then
   Application.CreateForm(TFrLoader,_Inctanse);
 Result := _Inctanse;
end;

procedure TFrLoader.SetRun(fl: Boolean);
begin
  if Fl  then Self.Show else self.Hide;
  self.jvgfnmtr.Animate := fl;
end;

end.
