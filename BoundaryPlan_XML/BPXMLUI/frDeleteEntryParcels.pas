unit frDeleteEntryParcels;
{
  ����������� ��� ���� ������ ������, ����������,
  �������� �������������� �������������, ������������� ��
  ��������� �������
  ����������� ����� ��� ���� �����

  InnerCadastralNumber
}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ActnList, StdCtrls, Mask, Buttons, ButtonGroup,

  XMLIntf, std_mp, ImgList, System.Actions, unRussLang,

  VCL.clipbrd,
  System.Generics.collections,
  System.Generics.defaults,
  System.RegularExpressions,fmContainer, JvExStdCtrls, JvListBox;

type
  TFrameDeleteEntryPArcels = class(TFrame)
    ActionList: TActionList;
    CadNum_ButtonGroup_Actions: TButtonGroup;
    CadNum_ListCN_ListBox_: TListBox;
    CadNum_NewCN_MaskEdit: TMaskEdit;
    Action_DoAdd: TAction;
    PanelCentre: TPanel;

    Action_CopySel: TAction;
    Action_DelSel: TAction;
    Action_Paste: TAction;
    Panel1: TPanel;
    CadNum_ListCN_ListBox: TJvListBox;
    procedure Action_DoAddExecute(Sender: TObject);
    procedure Action_CopySelExecute(Sender: TObject);
    procedure Action_DelSelExecute(Sender: TObject);
    procedure Action_PasteExecute(Sender: TObject);

  private type
     TNumberKind = (nkCadastral,nkOther); //��� ������������ ������
  private
    { Private declarations }
   // FXmlNode                    : IXMLNode;
    FXmlCadastralNumbers        : IXMLTChangeParcel_DeleteEntryParcels;

    FCadastralNumbers: TDictionary<string,TNumberKind>;

    procedure doAppendNumber(Value: string; Kind:TNumberKind);
    procedure doRemoveNumber(Value: string);
    procedure doClearAll;
    class function getKind(Value: string): TNumberKind;

    procedure Data_DoAdd;
    procedure Data_DoDeleteSelected;
    procedure Data_DoRead;

  protected
    procedure InitializationUI;
    procedure SetCadNums(const aCadastralNumbers: IXMLTChangeParcel_DeleteEntryParcels);
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    function IsAssignedCadastralNumbers: Boolean;
    property XmlCadastralNumbers: IXMLTChangeParcel_DeleteEntryParcels read FXmlCadastralNumbers write SetCadNums;
  end;


implementation

uses MsXMLAPI;

resourcestring
 rsXMLCadastralNumber = 'DeleteEntryParcel';
 rsXMLNumber = 'Number';
 rsPatternCadastralNumber = '\d+:\d+:\d+:\d+';
 rs275A055E = '��������� ������ �� ������������� ������� ������������ ������'
               +sLineBreak+
              '������������� ��������� �������� ��� ���� ����� �������?';
 rsD30E1CC6 = '������� ��������� � ������ ������?';
{$R *.dfm}


{ TFrameCadastralNumbersCollections }

procedure TFrameDeleteEntryPArcels.Action_CopySelExecute(Sender: TObject);
var
 _stb: TStringBuilder;
 _i: Integer;
begin
 if not self.IsAssignedCadastralNumbers then raise Exception.Create(ExMSG_NotAssignedROOT);
 _stb:= TStringBuilder.Create;
 for  _i:= 0 to self.CadNum_ListCN_ListBox.Count - 1 do
  if self.CadNum_ListCN_ListBox.Selected[_i] then begin
   _stb.Append(self.CadNum_ListCN_ListBox.Items[_i]);
   if  _i <> self.CadNum_ListCN_ListBox.Count - 1 then
    _stb.AppendLine;
  end;

  TRY
   Clipboard.Open;
   Clipboard.AsText:= _stb.ToString;
  FINALLY
   Clipboard.Close;
  END;
 _stb.Free;
end;

procedure TFrameDeleteEntryPArcels.Action_DelSelExecute(
  Sender: TObject);
begin
 if not self.IsAssignedCadastralNumbers then raise Exception.Create(ExMSG_NotAssignedROOT);

 if MessageDlg(rsD30E1CC6,mtConfirmation, [mbYes, mbNo], -1) = mrYes then
 self.Data_DoDeleteSelected;
end;

procedure TFrameDeleteEntryPArcels.Action_DoAddExecute(Sender: TObject);
begin
 if not self.IsAssignedCadastralNumbers then
    raise Exception.Create(ExMSG_NotAssignedROOT);
 self.Data_DoAdd;
end;

procedure TFrameDeleteEntryPArcels.Action_PasteExecute(Sender: TObject);
var
 _i: Integer;
 _stl: TStringList;
 _cnum: string;
begin
 if not self.IsAssignedCadastralNumbers then raise Exception.Create(ExMSG_NotAssignedROOT);

 if self.CadNum_ListCN_ListBox.Count<>0 then
   if MessageDlg(rsD30E1CC6,mtConfirmation,mbYesNo,-1)=mrYes then begin
     self.CadNum_ListCN_ListBox.SelectAll;
     Data_DoDeleteSelected;
   end;
 _stl:= TStringList.Create;
 TRY
  Clipboard.Open;
  _stl.Text:= Clipboard.AsText;
 FINALLY
  Clipboard.Close;
 END;
 for _i := 0 to _stl.Count - 1 do
  self.doAppendNumber(_stl[_i],getKind(_stl[_i]));
 _stl.Free;
end;

constructor TFrameDeleteEntryPArcels.Create(AOwner: TComponent);
begin
 inherited;
 self.FCadastralNumbers:= TDictionary<string,TNumberKind>.Create(TIStringComparer.Ordinal);
 self.InitializationUI;
end;

procedure TFrameDeleteEntryPArcels.InitializationUI;
begin
 self.CadNum_NewCN_MaskEdit.Text:= '';
 if self.IsAssignedCadastralNumbers then
  self.Data_DoRead
 else
 begin
  self.CadNum_ListCN_ListBox.Clear;
  self.FCadastralNumbers.Clear;
 end; 
end;

function TFrameDeleteEntryPArcels.IsAssignedCadastralNumbers: Boolean;
begin
 Result:= self.FXmlCadastralNumbers<>nil;
end;

destructor TFrameDeleteEntryPArcels.Destroy;
begin
  self.FCadastralNumbers.Free;
  inherited;
end;


procedure TFrameDeleteEntryPArcels.doAppendNumber(Value: string; Kind: TNumberKind);
begin
 if self.FCadastralNumbers.ContainsKey(value) then EXIT;//����������� ���������� ��������

 self.FCadastralNumbers.Add(Value,Kind);
 case kind of
   nkCadastral: Self.FXmlCadastralNumbers.Add.CadastralNumber := Value;
//   nkOther: Self.FXmlNode.AddChild(rsXMLNumber).Text:= Value;
 end;
 self.CadNum_ListCN_ListBox.Items.Add(Value);
end;

procedure TFrameDeleteEntryPArcels.doClearAll;
begin
 MsXml_RemoveAttribute(self.FXmlCadastralNumbers,rsXMLCadastralNumber);
// MsXml_RemoveChildNode(self.FXmlNode,rsXMLNumber);
 self.FCadastralNumbers.Clear;
 self.CadNum_ListCN_ListBox.Clear;
end;

procedure TFrameDeleteEntryPArcels.doRemoveNumber(Value: string);
var  _i: integer;
     _compare: IEqualityComparer<string>;
      xmlNode : IXMLNode;

     procedure doXmlRemove(aValue : string);
     var  _i: integer;
     begin
        try
          for _i:= FXmlCadastralNumbers.Count - 1 downto 0 do
           if _compare.Equals(FXmlCadastralNumbers.DeleteEntryParcel[_i].CadastralNumber ,Value) then
             self.FXmlCadastralNumbers.ChildNodes.Delete(_i);
        except on E : Exception do
           raise Exception.Create('������ ��� ��������! �������� ������� ������ ��� � xml, ������� � ������' + inttostr(_i));
        end;
     end;

begin
 //������� ��������
 if self.FCadastralNumbers.ContainsKey(value) then begin
   _compare:= TIStringComparer.Ordinal;
  doXmlRemove(Value);

  self.FCadastralNumbers.Remove(value);
  for _i := self.CadNum_ListCN_ListBox.Count-1 downto 0 do
   if _compare.Equals(self.CadNum_ListCN_ListBox.Items[_i],Value) then
     self.CadNum_ListCN_ListBox.Items.Delete(_i);
 end;
 xmlNode := nil;
end;

class function TFrameDeleteEntryPArcels.getKind(Value: string): TNumberKind;
begin
// if TRegEx.IsMatch(value,rsPatternCadastralNumber) then
  {������ ��� �������� ��� �� ���� �������}
 if ((TRegEx.Matches(Value,rsPatternCadastralNumber).Count > 0) and
    (TRegEx.Matches(Value,rsPatternCadastralNumber).Item[0].Length = Length(Value))) then
    Result:= nkCadastral
 else Result:= nkOther;
end;

procedure TFrameDeleteEntryPArcels.SetCadNums(const aCadastralNumbers: IXMLTChangeParcel_DeleteEntryParcels);
begin
 if self.FXmlCadastralNumbers=aCadastralNumbers then EXIT;
 self.FXmlCadastralNumbers:= aCadastralNumbers;
 self.InitializationUI;
end;


procedure TFrameDeleteEntryPArcels.Data_DoAdd;
var
 _value:string;
 _kind: TNumberKind;
begin
 _value:= self.CadNum_NewCN_MaskEdit.Text;
 if _value='' then EXIT;

 _kind:= getKind(_value);
 if _kind=nkOther then
   if MessageDlg(rs275A055E,mtConfirmation,mbYesNo,-1)=mrNo then EXIT;

 self.doAppendNumber(_value,_kind);

end;

procedure TFrameDeleteEntryPArcels.Data_DoDeleteSelected;
var
 _i:integer;
begin
 for _i := self.CadNum_ListCN_ListBox.Count-1 downto 0 do
  if _i<self.CadNum_ListCN_ListBox.Count then //� ������ �������������� �������� ����������
   if self.CadNum_ListCN_ListBox.Selected[_i] then
    self.doRemoveNumber(self.CadNum_ListCN_ListBox.Items[_i]);
end;

procedure TFrameDeleteEntryPArcels.Data_DoRead;
var
 _i: integer;
begin
 self.CadNum_ListCN_ListBox.Clear;
 if Xml_IsExistChildNode(self.FXmlCadastralNumbers,rsXMLCadastralNumber) then
 begin
//    xmlNode := self.FXmlCadastralNumbers;
    for _i := 0 to FXmlCadastralNumbers.Count-1 do
      if not self.FCadastralNumbers.ContainsKey(FXmlCadastralNumbers.DeleteEntryParcel[_i].CadastralNumber) then
      begin
       self.CadNum_ListCN_ListBox.Items.Add(FXmlCadastralNumbers.DeleteEntryParcel[_i].CadastralNumber);
       self.FCadastralNumbers.Add(FXmlCadastralNumbers.DeleteEntryParcel[_i].CadastralNumber,nkCadastral);
      end;
 end;
// if Xml_IsExistChildNode(self.FXmlNode,rsXMLNumber) then
// begin
//    xmlNode := self.FXmlNode.ChildNodes.FindNode(rsXMLNumber);
//    for _i := 0 to xmlNode.Collection.Count-1 do
//      {� �� ��� ������ ������ - ����������� }
//      if (not self.FCadastralNumbers.ContainsKey(xmlNode.Collection.Nodes[_i].Text)) or (FCadastralNumbers.Count = 0) then
//      begin
//       self.CadNum_ListCN_ListBox.Items.Add(xmlNode.Collection.Nodes[_i].Text);
//       self.FCadastralNumbers.Add(xmlNode.Collection.Nodes[_i].Text,nkOther);
//      end;
// end;
// xmlNode := nil;
end;

end.
