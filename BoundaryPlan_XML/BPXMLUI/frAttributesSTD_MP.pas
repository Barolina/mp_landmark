unit frAttributesSTD_MP;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, ButtonGroup, StdCtrls, ActnList, Vcl.Menus, Vcl.ImgList, Vcl.ComCtrls,
  Vcl.ToolWin,System.IOUtils,
  
  STD_MP, BpXML_different,XMLIntf, unRussLang, FrPreloader;

type
  TFrameAttributedSTD_MP = class(TFrame)
    ListAppleField: TListBox;
    FileOpenDialog: TOpenDialog;
    ToolBarAction: TToolBar;
    ToolButLoadDoc: TToolButton;
    PopupMenuActionDoc: TPopupMenu;
    nDeleteAppliedFile: TMenuItem;
    procedure SubParcels_Actions_ButtonGroupItems0Click(Sender: TObject);
    procedure nDeleteAppliedFileClick(Sender: TObject);
  private
    { Private declarations }

    FXmlAppliedFile : IXMLTAppliedFile;

    fXMLSurvey                     : IXMLSTD_MP_Survey;
    FXMLAgreement_Document         : IXMLSTD_MP_Agreement_Document;
    FXMLDiagram_Parcels_SubParcels : IXMLSTD_MP_Diagram_Parcels_SubParcels;
    FXMLScheme_Disposition_Parcels : IXMLSTD_MP_Scheme_Disposition_Parcels;
    FXMLScheme_Geodesic_Plotting   : IXMLSTD_MP_Scheme_Geodesic_Plotting;
    FXMLNodalPointSchemes          : IXMLSTD_MP_NodalPointSchemes_NodalPointScheme;
    FXMLAppliedFiles               : IXMLAppliedFiles;

    FXMLNodeCollection        : IXMLNodeCollection;
    FisGetType                : string; //��������� ���  {����� ��� ���������� ������������}������������
    FOnChangeListCount        : TNotifyEvent;

    procedure IXMLSTD_MP_Scheme_Geodesic_Plotting(
      const Value: IXMLSTD_MP_Scheme_Geodesic_Plotting);

    procedure CopyFileApplied(currentDir : string; const aAppliedFile : IXMLTAppliedFile);

  protected
    procedure InitializationUI;
    procedure SetSurvey(const aSurvey : IXMLSTD_MP_Survey);
    procedure SetAgreement_Document (const aAgreement_Document : IXMLSTD_MP_Agreement_Document);
    procedure SetDiagram_Parcels_SubParcels (const aDiagram_Parcels_SubParcels : IXMLSTD_MP_Diagram_Parcels_SubParcels);
    procedure SetScheme_Disposition_Parcels(const sScheme_Disposition_Parcels : IXMLSTD_MP_Scheme_Disposition_Parcels);
    procedure SetScheme_Geodesic_Plotting (const aScheme_Geodesic_Plotting :IXMLSTD_MP_Scheme_Geodesic_Plotting);
    procedure SetNodalPointSchemes (const aNodalPointSchemes : IXMLSTD_MP_NodalPointSchemes_NodalPointScheme);
    procedure SetAppliedFiles (const aAppliedFiles : IXMLAppliedFiles);

    { ��������������  ������  ��� �������� (���  ���������� ������ - 
      ������ ��� (�������� ����������  ������ �������� ���������� ����� ))
      ���� ������� ��� �� ������������� �������  :)
    }
    procedure CtrlDeleteFile(key : string; const aAppledFile  : IXMLTAppliedFile);
    procedure CtrlCopyFile(const aAppliedFile : IXMLTAppliedFile;const copyDir : string='');
  public
    constructor Create(Owner: TComponent); override;
    destructor Destroy; override;
    function  IsAssignedNodeCollection: boolean;
    procedure DeleteSelAppliedFile();
    procedure ImportDocument(FileName: string);
    { Public declarations }
    property XMLSurvey : IXMLSTD_MP_Survey
                                             read fXMLSurvey
                                             write SetSurvey;
    property XMLAgreement_Document : IXMLSTD_MP_Agreement_Document
                                             read FXMLAgreement_Document
                                             write SetAgreement_Document;
    property XMLDiagram_Parcels_SubParcels : IXMLSTD_MP_Diagram_Parcels_SubParcels
                                             read FXMLDiagram_Parcels_SubParcels
                                             write SetDiagram_Parcels_SubParcels;
    property XMLScheme_Disposition_Parcels : IXMLSTD_MP_Scheme_Disposition_Parcels
                                             read FXMLScheme_Disposition_Parcels
                                             write SetScheme_Disposition_Parcels;
    property XMLScheme_Geodesic_Plotting   :  IXMLSTD_MP_Scheme_Geodesic_Plotting
                                             read FXMLScheme_Geodesic_Plotting
                                             write SetScheme_Geodesic_Plotting;
    property XMlNodalPointSchemes          : IXMLSTD_MP_NodalPointSchemes_NodalPointScheme
                                             read FXMLNodalPointSchemes
                                             write SetNodalPointSchemes;
    property XMLApplidFiles                : IXMLAppliedFiles
                                             read FXMLAppliedFiles
                                             write SetAppliedFiles;

    property OnChangeListCount: TNotifyEvent read FOnChangeListCount write FOnChangeListCount;
  end;

implementation
{$R *.dfm}

{ TFrameAppliedFile }

 /// <summary> ��������  ���� � ���������  </summary>
 /// <param name="aAppliedFile"> ��� ������ ���� �������������� ����� </param>
 //  <param name="CurrentDir">  </param>
procedure TFrameAttributedSTD_MP.CopyFileApplied(currentDir : string;const aAppliedFile: IXMLTAppliedFile);
var
    fileName         : string;    
    pathAppliedFile  : string;
    parentNodeName   : string;
    positionXML      : string;
    tempPath         : string;
    new_FileName     : string;
    fullPath         : string;
begin
  parentNodeName := aAppliedFile.ParentNode.NodeName;
 { ��������� ��������  }
  fullPath   := aAppliedFile.Name;
  fileName   := ExtractFileName(aAppliedFile.Name);
  fileName   := StringReplace(StringReplace(StringReplace(fileName,'"','',[rfReplaceAll]),'�','',[rfReplaceAll]),'�','',[rfReplaceAll]); /// todo : ���� ��� ����� �������� ������� - �������
  aAppliedFile.Name := ExtractFilePath(fullPath)+ fileName; ///todo :  � �������� � ����� xml
  pathAppliedFile := ExtractFilePath(aAppliedFile.Name);
  tempPath := TSettingPathFile.MakeTempPath;
  try
      if not FileExists(tempPath+'\'+aAppliedFile.Name) then
      begin
        if CopyFileName(currentDir,fileName, tempPath+'\'+pathAppliedFile) then
          TAppliedFileDirect.AppliedFileDirect.WriteSectionOpenIni(rsLoadFile,parentNodeName+'\'+fileName,aAppliedFile.Name)
        else
          TAppliedFileDirect.AppliedFileDirect.WriteSectionOpenIni(rsNotLoadFile,parentNodeName+'\'+fileName,aAppliedFile.Name)
      end
      else  {�� ���� ���� ���������� - �� �� ������������ - ����� ������ ���� ��������� ������ (�� ���� ���� ������������)}
      TAppliedFileDirect.AppliedFileDirect.WriteSectionOpenIni(rsLoadFile,parentNodeName+'\'+fileName,aAppliedFile.Name);
      
  except  raise Exception.Create(rsMsgCopyFile+' '+pathAppliedFile +' '+ fileName);
  end;
end;

constructor TFrameAttributedSTD_MP.Create(Owner: TComponent);
begin
  inherited;
  ListAppleField.Clear;
  FXMLNodeCollection := nil;
  FXmlAppliedFile := nil;
  FisGetType :='';
end;

/// ���� ����� - ���������������  �������� ���������(��� ��� ���  �������� �������� :))) }
procedure TFrameAttributedSTD_MP.CtrlCopyFile( const aAppliedFile: IXMLTAppliedFile;const copyDir : string='');
var  dirFile : string;
     _newFileName : string;
begin
  _newFileName := StringReplace(StringReplace(StringReplace(aAppliedFile.Name,'"','',[rfReplaceAll]),'�','',[rfReplaceAll]),'�','',[rfReplaceAll]);
  ListAppleField.AddItem(_newFileName, pointer(aAppliedFile));
  if copyDir <> '' then dirFile := copyDir
  else dirFile := ExtractFilePath(TAppliedFileDirect.AppliedFileDirect.ReadSectionOpenIni(rsNameOpenFile,rsNameOpenFile)) + '\'+aAppliedFile.Name;
  CopyFileApplied(dirFile,aAppliedFile);
end;

procedure TFrameAttributedSTD_MP.ImportDocument(FileName: string);
var _newFileName : string;
begin
   //TODO : ��������� ������ ��������
   if not self.IsAssignedNodeCollection then raise Exception.Create(ExMSG_NotAssignedROOT);
   if FXMLNodeCollection.LocalName = cnstXmlAppliedFiles then
       FXmlAppliedFile  :=(FXMLNodeCollection.AddChild(cnstXmlAppliedFile)) as IXMLTAppliedFile
   else
    FXmlAppliedFile  :=(FXMLNodeCollection.AddChild(cnstXmlAppliedFile)) as IXMLSTD_MP_Survey_AppliedFile;

   FXmlAppliedFile.Kind := '02';
   FXmlAppliedFile.Name := FisGetType+'\'+ExtractFileName(FileName);
   CtrlCopyFile(FXmlAppliedFile,FileName);
end;

procedure TFrameAttributedSTD_MP.InitializationUI;
var  i : integer;
     filePath  : string;
     pathOpenFile : string;
     fileName : string;
begin
 ListAppleField.Clear;
 if self.IsAssignedNodeCollection then
 begin
    //read Xml � ��������  ���� ����
    for I := 0 to FXMLNodeCollection.Count -1  do
    begin
      if FXMLNodeCollection.LocalName = cnstXmlAppliedFiles then {���������� ������}
        FXmlAppliedFile  :=(FXMLNodeCollection.Nodes[i]) as IXMLTAppliedFile
      else
        FXmlAppliedFile  :=(FXMLNodeCollection.Nodes[i]) as IXMLSTD_MP_Survey_AppliedFile;
      CtrlCopyFile(FXmlAppliedFile);
      Application.ProcessMessages;
    end;
 end
 else    FXMLNodeCollection := nil;
 FXmlAppliedFile := nil;
end;

function TFrameAttributedSTD_MP.IsAssignedNodeCollection: boolean;
begin
 Result := FXMLNodeCollection <> nil;
end;

procedure TFrameAttributedSTD_MP.IXMLSTD_MP_Scheme_Geodesic_Plotting(
  const Value: IXMLSTD_MP_Scheme_Geodesic_Plotting);
begin
  FXMLScheme_Geodesic_Plotting := Value;
end;

///
/// <param> Delete current ApledFiles </param>
///
procedure TFrameAttributedSTD_MP.nDeleteAppliedFileClick(Sender: TObject);
begin
  DeleteSelAppliedFile;
end;

procedure TFrameAttributedSTD_MP.SetAgreement_Document(
  const aAgreement_Document: IXMLSTD_MP_Agreement_Document);
begin
 if self.FXMLNodeCollection=aAgreement_Document then EXIT;
 self.FXMLNodeCollection:= aAgreement_Document;
 FisGetType := rsAltAgreement_Document;
 self.InitializationUI;
end;

procedure TFrameAttributedSTD_MP.SetAppliedFiles(
  const aAppliedFiles: IXMLAppliedFiles);
begin
 if self.FXMLNodeCollection=aAppliedFiles then EXIT;
 self.FXMLNodeCollection:= aAppliedFiles;
 FisGetType := rsAltAppendix;
 self.InitializationUI;
end;

procedure TFrameAttributedSTD_MP.SetDiagram_Parcels_SubParcels(
  const aDiagram_Parcels_SubParcels: IXMLSTD_MP_Diagram_Parcels_SubParcels);
begin
 if self.FXMLNodeCollection=aDiagram_Parcels_SubParcels then EXIT;
 self.FXMLNodeCollection:= aDiagram_Parcels_SubParcels;
 FisGetType := rsAltDPS;
 self.InitializationUI;
end;

procedure TFrameAttributedSTD_MP.SetNodalPointSchemes(
  const aNodalPointSchemes: IXMLSTD_MP_NodalPointSchemes_NodalPointScheme);
begin
 if self.FXMLNodeCollection=aNodalPointSchemes then EXIT;
 self.FXMLNodeCollection:= aNodalPointSchemes;
 FisGetType := rsAltNodalPointSchemes;
 self.InitializationUI;
end;

procedure TFrameAttributedSTD_MP.SetScheme_Disposition_Parcels(
  const sScheme_Disposition_Parcels: IXMLSTD_MP_Scheme_Disposition_Parcels);
begin
 if self.FXMLNodeCollection=sScheme_Disposition_Parcels  then EXIT;
 self.FXMLNodeCollection:= sScheme_Disposition_Parcels;
 FisGetType := rsAltSDP;
 self.InitializationUI;
end;

procedure TFrameAttributedSTD_MP.SetScheme_Geodesic_Plotting(
  const aScheme_Geodesic_Plotting: IXMLSTD_MP_Scheme_Geodesic_Plotting);
begin
  if self.FXMLNodeCollection=aScheme_Geodesic_Plotting then EXIT;
  self.FXMLNodeCollection:= aScheme_Geodesic_Plotting;
  FisGetType := rsAltSGP;
  self.InitializationUI;
end;

procedure TFrameAttributedSTD_MP.SetSurvey;
begin
 if self.FXMLNodeCollection=aSurvey then EXIT;
 self.FXMLNodeCollection:= aSurvey;
 FisGetType := rsAltSyrvey;
 self.InitializationUI;
end;

procedure TFrameAttributedSTD_MP.CtrlDeleteFile(key : string; const aAppledFile  : IXMLTAppliedFile);
var tempDir : string;
    deleteDir : string;
    elSection : string;
begin
 try
   tempDir := TSettingPathFile.MakeTempPath +'\'+ aAppledFile.Name;
   if FileExists(tempDir) then   DeleteFile(tempDir);
   {�������  ����� - ���� �����}
   deleteDir := TSettingPathFile.MakeTempPath+'\'+ ExtractFileDir(aAppledFile.Name);
   if TDirectory.Exists(deleteDir) then  RemoveDir(deleteDir);
   TAppliedFileDirect.AppliedFileDirect.DeleteKeyIni(rsLoadFile,key);
   TAppliedFileDirect.AppliedFileDirect.DeleteKeyIni(rsNotLoadFile,key);
 except   raise Exception.Create(self.ClassName+'; '+ rsMsgDeleteFile+'; '+deleteDir);
 end;
end;

procedure TFrameAttributedSTD_MP.DeleteSelAppliedFile;
var i : integer;
    xmlAppleField : IXMLTAppliedFile;
begin
  if not self.IsAssignedNodeCollection then raise Exception.Create(ExMSG_NotAssignedROOT);
  
  for i := ListAppleField.Count-1 downto 0 do
  begin
   if ListAppleField.SelCount <= 0  then  Exit;
   if   ListAppleField.Selected[i] then
   begin
     xmlAppleField := IXMLTAppliedFile(pointer(self.ListAppleField.Items.Objects[i]));
     {�������� ���������}
     CtrlDeleteFile(xmlAppleField.ParentNode.NodeName+'\'+ExtractFileName(xmlAppleField.Name),xmlAppleField);
     FXMLNodeCollection.Remove(xmlAppleField);{�� xml}
     ListAppleField.DeleteSelected;{�� ��������}
   end;
  end;
  if  Assigned(FOnChangeListCount) then FOnChangeListCount(self);
  xmlAppleField := nil;
end;

destructor TFrameAttributedSTD_MP.Destroy;
begin
  inherited;
end;

procedure TFrameAttributedSTD_MP.SubParcels_Actions_ButtonGroupItems0Click(
  Sender: TObject);
var i  : integer;
    xmlNode : IXMLNode;
begin
try
 if not self.IsAssignedNodeCollection then raise Exception.Create(ExMSG_NotAssignedROOT);
 TFrLoader.Inctance.RunAnimation := True;
 if FileOpenDialog.Execute then
   for i := 0 to FileOpenDialog.Files.Count -1 do begin
      ImportDocument(FileOpenDialog.Files[i]);
      Application.ProcessMessages;
   end;
finally
  TFrLoader.Inctance.RunAnimation := false;
end;
end;


end.
