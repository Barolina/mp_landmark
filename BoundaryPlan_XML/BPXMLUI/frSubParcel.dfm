object FrameSubParcel: TFrameSubParcel
  Left = 0
  Top = 0
  Width = 804
  Height = 453
  Color = clBtnFace
  ParentBackground = False
  ParentColor = False
  TabOrder = 0
  object NewParcelsPageControl: TJvgPageControl
    Left = 0
    Top = 24
    Width = 804
    Height = 429
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    ActivePage = tsEncumbrance
    Align = alClient
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clBlack
    Font.Height = -14
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    MultiLine = True
    ParentFont = False
    Style = tsFlatButtons
    TabHeight = 30
    TabOrder = 0
    TabStop = False
    TabWidth = 180
    OnChange = NewParcelsPageControlChange
    SingleGlyph = True
    TabStyle.Borders = [fsdLeft, fsdTop, fsdRight, fsdBottom]
    TabStyle.BevelInner = bvNone
    TabStyle.BevelOuter = bvNone
    TabStyle.Bold = False
    TabStyle.BackgrColor = clInactiveBorder
    TabStyle.Font.Charset = RUSSIAN_CHARSET
    TabStyle.Font.Color = clBlack
    TabStyle.Font.Height = -12
    TabStyle.Font.Name = 'Arial'
    TabStyle.Font.Style = [fsBold]
    TabStyle.TextStyle = fstVolumetric
    TabStyle.CaptionHAlign = fhaCenter
    TabStyle.GlyphHAlign = fhaCenter
    TabStyle.Gradient.ToColor = clSilver
    TabStyle.Gradient.Active = True
    TabStyle.Gradient.Orientation = fgdRightBias
    TabSelectedStyle.Borders = [fsdLeft, fsdTop, fsdRight, fsdBottom]
    TabSelectedStyle.BevelInner = bvNone
    TabSelectedStyle.BevelOuter = bvNone
    TabSelectedStyle.Bold = False
    TabSelectedStyle.BackgrColor = 15921123
    TabSelectedStyle.Font.Charset = ANSI_CHARSET
    TabSelectedStyle.Font.Color = clBlack
    TabSelectedStyle.Font.Height = -12
    TabSelectedStyle.Font.Name = 'Arial'
    TabSelectedStyle.Font.Style = [fsBold]
    TabSelectedStyle.TextStyle = fstVolumetric
    TabSelectedStyle.CaptionHAlign = fhaCenter
    TabSelectedStyle.GlyphHAlign = fhaCenter
    TabSelectedStyle.Gradient.FromColor = 15921123
    TabSelectedStyle.Gradient.ToColor = 691384
    TabSelectedStyle.Gradient.Active = True
    TabSelectedStyle.Gradient.Orientation = fgdHorizontal
    TabsPosition = fsdBottom
    Options = [ftoAutoFontDirection, ftoExcludeGlyphs, ftoTabColorAsGradientFrom, ftoTabColorAsGradientTo]
    object tsEnSp_Attribut: TTabSheet
      Hint = #1050#1086#1086#1088#1076#1080#1085#1072#1090#1085#1086#1077' '#1086#1087#1080#1089#1072#1085#1080#1077
      Caption = #1050#1086#1086#1088#1076#1080#1085#1072#1090#1085#1086#1077' '#1086#1087#1080#1089#1072#1085#1080#1077
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ImageIndex = 5
      ParentFont = False
      TabVisible = False
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object jvgrdnt3: TJvGradient
        Left = 0
        Top = 0
        Width = 796
        Height = 419
        StartColor = 16512
        EndColor = clSilver
        ExplicitLeft = 360
        ExplicitTop = 240
        ExplicitWidth = 100
        ExplicitHeight = 41
      end
    end
    object tsEncumbrance: TTabSheet
      Hint = #1061#1072#1088#1072#1082#1090#1077#1088#1080#1089#1090#1080#1082#1072' '#1095#1072#1089#1090#1080
      Caption = #1061#1072#1088#1072#1082#1090#1077#1088#1080#1089#1090#1080#1082#1072' '#1095#1072#1089#1090#1080
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ImageIndex = 2
      ParentFont = False
      TabVisible = False
      object jvgrdnt2: TJvGradient
        Left = 0
        Top = 0
        Width = 796
        Height = 419
        StartColor = 16512
        EndColor = clSilver
        ExplicitLeft = 360
        ExplicitTop = 240
        ExplicitWidth = 100
        ExplicitHeight = 41
      end
    end
    object tsArea: TTabSheet
      Hint = #1055#1083#1086#1097#1072#1076#1100'/'#1054#1073#1086#1079#1085#1072#1095#1077#1085#1080#1077
      Caption = #1055#1083#1086#1097#1072#1076#1100
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clBlack
      Font.Height = -14
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ImageIndex = 2
      ParentFont = False
      TabVisible = False
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object pnl2: TPanel
        Left = 0
        Top = 0
        Width = 796
        Height = 419
        Align = alClient
        BevelOuter = bvNone
        ParentBackground = False
        ParentColor = True
        TabOrder = 0
        object Area_Panel: TPanel
          Left = 0
          Top = 46
          Width = 796
          Height = 80
          Align = alTop
          BevelOuter = bvNone
          ParentColor = True
          TabOrder = 0
        end
        object pnl1: TPanel
          Left = 0
          Top = 0
          Width = 796
          Height = 46
          Align = alTop
          BevelOuter = bvNone
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 1
          object lblCaption: TLabel
            Left = 5
            Top = 4
            Width = 256
            Height = 13
            Caption = #1059#1095#1077#1090#1085#1099#1081' '#1085#1086#1084#1077#1088' '#1080#1083#1080' '#1086#1073#1086#1079#1085#1072#1095#1077#1085#1080#1077' '#1095#1072#1089#1090#1080' '#1047#1059
            Color = clBtnFace
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
          end
          object edtAttribute_DefOrNumPP_Edit: TEdit
            Left = 7
            Top = 23
            Width = 356
            Height = 20
            Hint = #1048#1079#1084#1077#1085#1080#1090#1100'  '#1091#1095#1077#1090#1085#1099#1081' '#1085#1086#1084#1077#1088' ('#1086#1073#1086#1079#1085#1072#1095#1077#1085#1080#1077') '#1095#1072#1089#1090#1080' '#1047#1059
            AutoSize = False
            BevelKind = bkFlat
            BorderStyle = bsNone
            Color = 15921123
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            StyleElements = []
            OnExit = ActionChange_Attribute_DefOrNumPPExecute
          end
          object pnlSubParcel_Realty: TPanel
            Left = 368
            Top = 17
            Width = 274
            Height = 35
            BevelOuter = bvNone
            ParentColor = True
            TabOrder = 1
            object chkAttribute_SubParcelRealty_CheckBox: TCheckBox
              Left = 0
              Top = 0
              Width = 274
              Height = 35
              Align = alClient
              Caption = #1063#1072#1089#1090#1100' '#1087#1086#1076' '#1086#1073#1098#1077#1082#1090#1086#1084' '#1085#1077#1076#1074#1080#1078#1080#1084#1086#1089#1090#1080
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 0
              OnClick = chkAttribute_SubParcelRealty_CheckBoxClick
            end
          end
        end
      end
    end
  end
  object pnlCaptioanAction: TPanel
    Left = 0
    Top = 0
    Width = 804
    Height = 24
    Align = alTop
    BevelOuter = bvNone
    Color = 12615680
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clYellow
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
  end
  object ActionList: TActionList
    Left = 600
    Top = 256
    object ActionChange_Attribute_DefOrNumPP: TAction
      Caption = 'ActionChange_Attribute_DefOrNumPP'
      OnExecute = ActionChange_Attribute_DefOrNumPPExecute
    end
    object ActionChange_ExistingOrNew: TAction
      Caption = 'ActionChange_ExistingOrNew'
    end
  end
  object jvnvpnstylmngr1: TJvNavPaneStyleManager
    Theme = nptXPBlue
    Left = 304
    Top = 202
  end
end
