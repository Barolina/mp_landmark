unit fmBPXML_GeneralInformation;

{
  Version 4
}
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, ComCtrls, xmldoc, XMLIntf,

  STD_MP,  frContractor,
  frAttributesSTD_MP, frMeans_Surveys, frClients, frGeodesisBases, frRealty,
  frInputDateSubParcels,
  frNodalPointSchemes, Vcl.XPMan, frDataDocuments,
  unRussLang,UnShowText;

type
  TFormBPXML_GeneralInformation = class(TForm)
    PanelRecipient: TPanel;
    Panel4: TPanel;
    PanelAttribute: TPanel;
    AttribyteCodeType_Edit: TEdit;
    AttribyteVersion_Edit: TEdit;
    AttributeGUID_Edit: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    GIPageControl: TPageControl;
    TabSheet2: TTabSheet;
    TabSheetContractor: TTabSheet;
    Panel2: TPanel;
    OpenDialog: TOpenDialog;
    PanelAttributes: TPanel;
    lbSurvey: TLabel;
    lbScheme_Geodesic_Plotting: TLabel;
    lbScheme_Disposition_Parcels: TLabel;
    lbDiagram_Parcels_SubParcels: TLabel;
    PanelTitle: TPanel;
    TabSheetClient: TTabSheet;
    Panel8: TPanel;
    lbAgreement_Document: TLabel;
    lblPurpose: TLabel;
    Label9: TLabel;
    MemoConclusion: TMemo;
    ConclusionTabSheet: TTabSheet;
    Panel5: TPanel;
    TabSheetInputDate: TTabSheet;
    pnlAttributeInputDate: TPanel;
    lbGeodesic_Bases: TLabel;
    lbMeans_Survey: TLabel;
    lbRealty: TLabel;
    lbSubParcels: TLabel;
    lbNodalPointSchemes: TLabel;
    XPManifest1: TXPManifest;
    lbAppendix: TLabel;
    lbDocuments: TLabel;
    Panel6: TPanel;
    Splitter3: TSplitter;
    Splitter4: TSplitter;
    grbxContractor: TGroupBox;
    mmoReason: TMemo;
    mmoPurpose: TMemo;
    btnReason: TBitBtn;
    btnPurpose: TBitBtn;
    pnl1: TPanel;
    pnl2: TPanel;
    lbl1: TLabel;
    edtSystemCoorName: TEdit;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SpeedButton_OpenClick(Sender: TObject);
    procedure lbSurveyClick(Sender: TObject);
    procedure lbScheme_Geodesic_PlottingClick(Sender: TObject);
    procedure lbScheme_Disposition_ParcelsClick(Sender: TObject);
    procedure lbDiagram_Parcels_SubParcelsClick(Sender: TObject);
    procedure lbAgreement_DocumentClick(Sender: TObject);
    procedure Label9Click(Sender: TObject);
    procedure Purpose_EditExit(Sender: TObject);
    procedure Reason_EditExit(Sender: TObject);
    procedure lbNodalPointSchemesClick(Sender: TObject);
    procedure lbGeodesic_BasesClick(Sender: TObject);
    procedure lbMeans_SurveyClick(Sender: TObject);
    procedure lbRealtyClick(Sender: TObject);
    procedure lbSubParcelsClick(Sender: TObject);
    procedure MemoConclusionEnter(Sender: TObject);
    procedure PanelRecipientMouseMove(Sender: TObject; Shift: TShiftState;
      X, Y: Integer);
    procedure PanelRecipientMouseLeave(Sender: TObject);
    procedure lbAppendixClick(Sender: TObject);
    procedure lbDocumentsClick(Sender: TObject);
    procedure lbSubParcelsMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X,
      Y: Integer);
    procedure btnReasonClick(Sender: TObject);
    procedure btnPurposeClick(Sender: TObject);
    procedure edtSystemCoorNameExit(Sender: TObject);
    procedure edtSystemCoorIDExit(Sender: TObject);
  private
    { Private declarations }
    FXmlSTDMP    : IXMLSTD_MP;
    FXMlTitle    : IXMLSTD_MP_Title;
    FXMlInputData: IXMLSTD_MP_Input_Data;
    FXMLAppendix : IXMLSTD_MP_Appendix;

    FAttributeSTD_MP  : TFrameAttributedSTD_MP;
    FMeans_Surveys    : TFrameMeans_Surveys;
    FContractor       : TFrameContractor;
    FClients          : TFrameClients;
    FGeodesicBase     : TFrameGeodesisBases;
    FRealty           : TFrameRealty;
    FSubParcels       : TFrameInputDateSubParcels;
    FNodalPointSchemes: TFrameNodalPointSchemes;
    FAppendix         : TFrameDataDocuments;
    FDocuments        : TFrameDataDocuments;

    procedure UI_Purpose_ReadOf(const aTitle: IXMLSTD_MP_Title);
    procedure UI_Reason_ReadOf(const aTitle: IXMLSTD_MP_Title);
    procedure UI_ReadOf(const aTitle: IXMLSTD_MP_Title);

    procedure UI_Conclusion_ReadOF(const aSTD_MP: IXMLSTD_MP);
    procedure UI_Conclusion_WriteOf(const aSTD_MP: IXMLSTD_MP);

    procedure UI_SystemCoor_ReadOf(const aSTD_MP: IXMLSTD_MP);

    procedure UI_Purpose_WriteTo(const aTitle: IXMLSTD_MP_Title);
    procedure UI_Reason_WriteTo(const aTitle: IXMLSTD_MP_Title);
    procedure UI_WriteTo(const aTitle: IXMLSTD_MP_Title);

    procedure UI_Read_InputDate(const aSTD_MP: IXMLSTD_MP);
    procedure UI_Read_AttributeSTD_MP(const aSTD_MP: IXMLSTD_MP);

    procedure VisibleFrameGenInf(Sender: TObject);
    procedure OnChangeListCount(Sender : TObject);
  protected
    procedure InitializationUI;
    procedure SetSTDMP(const aSTDMP: IXMLSTD_MP);
  public
    { Public declarations }
    procedure GetZoomInText(Sender : TObject);
    function IsAssignedXmlNode: Boolean;
    constructor Create(Owner: TComponent); override;
    procedure SetSettingsActivCtrl(Sender : TObject);

    property XmlSTDMP: IXMLSTD_MP read FXmlSTDMP write SetSTDMP;
  end;

var
  FormBPXML_GeneralInformation: TFormBPXML_GeneralInformation;

implementation

uses MsXmlApi;

resourcestring
  rsBPForm = '���������� �� ����';
  rsBPSpecify = '���� ���������';
  rsSTDMP = 'STD_MP';
  rsSender = 'Sender';
  rsRecipient = 'Recipient';
  rsContractor = 'Contractor';

{$R *.dfm}
  { TFormBPXML_GeneralInformation }

procedure TFormBPXML_GeneralInformation.BitBtn1Click(Sender: TObject);
begin
  self.Close;
end;

///
/// ����������� ������
///
procedure TFormBPXML_GeneralInformation.btnPurposeClick(Sender: TObject);
begin
 self.GetZoomInText(self.mmoPurpose);
end;

procedure TFormBPXML_GeneralInformation.btnReasonClick(Sender: TObject);
begin
 self.GetZoomInText(self.mmoReason);
end;

constructor TFormBPXML_GeneralInformation.Create(Owner: TComponent);
var i : integer;
begin
  inherited;
  self.FContractor := TFrameContractor.Create(self.grbxContractor);
  self.FContractor.Parent := self.grbxContractor;
  self.FContractor.Align := alClient;

  self.FClients := TFrameClients.Create(self.TabSheetClient);
  self.FClients.Parent := self.TabSheetClient;
  self.FClients.Align := alClient;

  self.FAttributeSTD_MP := TFrameAttributedSTD_MP.Create(self.PanelRecipient);
  self.FAttributeSTD_MP.OnChangeListCount := OnChangeListCount;
  self.FAttributeSTD_MP.Parent := self.PanelRecipient;
  self.FAttributeSTD_MP.Align := alClient;
  self.FAttributeSTD_MP.Visible := false;

  self.FGeodesicBase := TFrameGeodesisBases.Create(self.TabSheetInputDate);
  self.FGeodesicBase.Parent := self.TabSheetInputDate;
  self.FGeodesicBase.Align := alClient;
  self.FGeodesicBase.Visible := false;

  self.FMeans_Surveys := TFrameMeans_Surveys.Create(self.TabSheetInputDate);
  self.FMeans_Surveys.Parent := self.TabSheetInputDate;
  self.FMeans_Surveys.Align := alClient;
  self.FMeans_Surveys.Visible := false;

  self.FRealty := TFrameRealty.Create(self.TabSheetInputDate);
  self.FRealty.Parent := self.TabSheetInputDate;
  self.FRealty.Align := alClient;
  self.FRealty.Visible := false;

  self.FSubParcels := TFrameInputDateSubParcels.Create(self.TabSheetInputDate);
  self.FSubParcels.Parent := self.TabSheetInputDate;
  self.FSubParcels.Align := alClient;
  self.FSubParcels.Visible := false;

  self.FDocuments := TFrameDataDocuments.Create(self.TabSheetInputDate);
  self.FDocuments.Parent := self.TabSheetInputDate;
  self.FDocuments.Align := alClient;
  self.FDocuments.Visible := false;

  self.FNodalPointSchemes := TFrameNodalPointSchemes.Create
    (self.PanelRecipient);
  self.FNodalPointSchemes.Parent := self.PanelRecipient;
  self.FNodalPointSchemes.Align := alClient;
  self.FNodalPointSchemes.Visible := false;

  self.FAppendix := TFrameDataDocuments.Create(self.PanelRecipient);
  self.FAppendix.Parent := self.PanelRecipient;
  self.FAppendix.Align := alClient;
  self.FAppendix.Visible := false;
 
  lbSurvey.Caption := rsSyrvey;
  lbScheme_Geodesic_Plotting.Caption := rsScheme_Geodesic_Plotting;
  lbScheme_Disposition_Parcels.Caption := rsScheme_Diposition_Parcels;
  lbDiagram_Parcels_SubParcels.Caption := rsDiagram_Parcels_SubParcels;
  lbAgreement_Document.Caption := rsAgreement_Document;
  lbNodalPointSchemes.Caption := rsNodalPointSchemes;
  lbAppendix.Caption := rsAppendix;

  {���� ���������}
  for I := 0 to self.PanelAttributes.ControlCount-1 do
      if PanelAttributes.Controls[i] is TLabel then 
        TLabel(PanelAttributes.Controls[i]).Transparent := false;
  for I := 0 to self.pnlAttributeInputDate.ControlCount-1 do
      if pnlAttributeInputDate.Controls[i] is TLabel then 
        TLabel(pnlAttributeInputDate.Controls[i]).Transparent := false;
   
end;

procedure TFormBPXML_GeneralInformation.edtSystemCoorIDExit(Sender: TObject);
  var _st : string;
begin
  if FXmlSTDMP = nil then Exit;
  FXmlSTDMP.Coord_Systems.Coord_System.Cs_Id := 'SystemCoord'; //������ �� ���������
end;

procedure TFormBPXML_GeneralInformation.edtSystemCoorNameExit(Sender: TObject);
var _st : string;
begin
  if FXmlSTDMP = nil then Exit;
  _st := self.edtSystemCoorName.Text;
  if _st <> '' then  FXmlSTDMP.Coord_Systems.Coord_System.Name := _st
  else
    FXmlSTDMP.Coord_Systems.Coord_System.Name := '�� ������������ ������'; //������ �� ���������
end;

procedure TFormBPXML_GeneralInformation.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
//TODO:26  self.SetFocus;
end;

procedure TFormBPXML_GeneralInformation.GetZoomInText(Sender: TObject);
begin
  if not IsAssignedXmlNode then Exit;
  if not (Sender is TMemo) then Exit;
  (Sender as TMemo).Text := TFrShowText.InstanShowText.UpdateText((Sender as TMemo).Text);
  (Sender as TMemo).OnExit(Sender);
end;

procedure TFormBPXML_GeneralInformation.InitializationUI;
var
  xmlNode: IXMLNode;
  index: Integer;
  item: Integer;
  Sender: TObject;
begin
  { = "����������� Label� � ����������" = }
  item := 0;
  for index := 0 to PanelAttributes.ControlCount - 1 do
    if PanelAttributes.Controls[Index] is TLabel then
    begin
      TLabel(PanelAttributes.Controls[index]).OnMouseMove :=
        PanelRecipientMouseMove;
      TLabel(PanelAttributes.Controls[index]).OnMouseLeave :=
        PanelRecipientMouseLeave;
    end;

  for index := 0 to pnlAttributeInputDate.ControlCount - 1 do
    if pnlAttributeInputDate.Controls[index] is TLabel then
    begin
      TLabel(pnlAttributeInputDate.Controls[index]).OnMouseMove :=
        PanelRecipientMouseMove;
      TLabel(pnlAttributeInputDate.Controls[index]).OnMouseLeave :=
        PanelRecipientMouseLeave;
    end;
  { ------------------------ }
  if self.IsAssignedXmlNode then
  begin
    FXMlTitle := FXmlSTDMP.Title;
    UI_ReadOf(FXMlTitle);
    self.FContractor.XMLTitleContractor := FXMlTitle.Contractor;
    self.FClients.XmlClients := FXMlTitle.Client;

    UI_Read_InputDate(FXmlSTDMP);
    UI_Read_AttributeSTD_MP(FXmlSTDMP);
    UI_Conclusion_ReadOF(FXmlSTDMP);
    UI_SystemCoor_ReadOf(FXmlSTDMP);

    self.AttribyteVersion_Edit.Text := self.FXmlSTDMP.EDocument.Version;
    self.AttributeGUID_Edit.Text := self.FXmlSTDMP.EDocument.GUID;

    if self.FXmlSTDMP.EDocument.CodeType = cnstCodeTypeFormParcels then
      self.AttribyteCodeType_Edit.Text := rsBPForm;
    if self.FXmlSTDMP.EDocument.CodeType = cnstCodeTyteSpecifyRelated then
      self.AttribyteCodeType_Edit.Text := rsBPSpecify;

  end
  else
  begin
    self.FContractor.XMLTitleContractor := nil;
    self.FClients.XmlClients := nil;
    self.FGeodesicBase.XmlGeodesis_Bases := nil;
    self.FMeans_Surveys.XmlMeans_Surveys := nil;
    self.FRealty.XmlRealtys := nil;
    self.FSubParcels.XmlSubParcels := nil;
    self.AttribyteCodeType_Edit.Text := '';
    self.AttribyteVersion_Edit.Text := '';
    self.AttributeGUID_Edit.Text := '';
    self.FAttributeSTD_MP.XMLSurvey := nil;
    self.FAttributeSTD_MP.XMLScheme_Geodesic_Plotting := nil;
    self.FAttributeSTD_MP.XMLScheme_Disposition_Parcels := nil;
    self.FAttributeSTD_MP.XMLDiagram_Parcels_SubParcels := nil;
    self.FAttributeSTD_MP.XMLAgreement_Document := nil;
    self.mmoReason.Clear;
    self.mmoPurpose.Clear;
    self.MemoConclusion.Clear;
    self.edtSystemCoorName.Text := '�� ������������ ������';
  end;
end;

function TFormBPXML_GeneralInformation.IsAssignedXmlNode: Boolean;
begin
  Result := self.FXmlSTDMP <> nil;
end;

procedure TFormBPXML_GeneralInformation.lbNodalPointSchemesClick
  (Sender: TObject);
begin
  self.FNodalPointSchemes.XmlNodalPointSchemes := self.FXmlSTDMP.NodalPointSchemes;
  VisibleFrameGenInf(Sender);
  SetSettingsActivCtrl(Sender);
end;

procedure TFormBPXML_GeneralInformation.lbGeodesic_BasesClick(Sender: TObject);
begin
  self.FGeodesicBase.XmlGeodesis_Bases := FXmlSTDMP.Input_Data.Geodesic_Bases;
  VisibleFrameGenInf(Sender);
  SetSettingsActivCtrl(Sender);
end;

procedure TFormBPXML_GeneralInformation.lbMeans_SurveyClick(Sender: TObject);
begin
  self.FMeans_Surveys.XmlMeans_Surveys := FXMlInputData.Means_Survey;
  VisibleFrameGenInf(Sender);
  SetSettingsActivCtrl(Sender)
end;

procedure TFormBPXML_GeneralInformation.lbRealtyClick(Sender: TObject);
begin
  self.FRealty.XmlRealtys := FXMlInputData.Realty;
  VisibleFrameGenInf(Sender);
  SetSettingsActivCtrl(Sender)
end;

procedure TFormBPXML_GeneralInformation.lbSubParcelsClick(Sender: TObject);
begin
  self.FSubParcels.XmlSubParcels := FXMlInputData.SubParcels;
  VisibleFrameGenInf(Sender);
  SetSettingsActivCtrl(Sender)
end;

procedure TFormBPXML_GeneralInformation.lbSubParcelsMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
end;

{ tDocuments }
procedure TFormBPXML_GeneralInformation.lbAppendixClick(Sender: TObject);
begin
  self.FAppendix.XmlAppendix := self.FXmlSTDMP.Appendix;
  VisibleFrameGenInf(Sender);
  SetSettingsActivCtrl(Sender);
end;

{ tSurvey }
procedure TFormBPXML_GeneralInformation.lbSurveyClick(Sender: TObject);
begin
  self.FAttributeSTD_MP.XMLSurvey := self.FXmlSTDMP.Survey;
  VisibleFrameGenInf(Sender);
  SetSettingsActivCtrl(Sender);
end;

{ }
procedure TFormBPXML_GeneralInformation.lbScheme_Geodesic_PlottingClick
  (Sender: TObject);
begin
  self.FAttributeSTD_MP.XMLScheme_Geodesic_Plotting := self.FXmlSTDMP.Scheme_Geodesic_Plotting;
  VisibleFrameGenInf(Sender);
  SetSettingsActivCtrl(Sender);
end;

procedure TFormBPXML_GeneralInformation.lbScheme_Disposition_ParcelsClick
  (Sender: TObject);
begin
  self.FAttributeSTD_MP.XMLScheme_Disposition_Parcels := self.FXmlSTDMP.Scheme_Disposition_Parcels;
  VisibleFrameGenInf(Sender);
  SetSettingsActivCtrl(Sender); 
end;

procedure TFormBPXML_GeneralInformation.lbDiagram_Parcels_SubParcelsClick
  (Sender: TObject);
begin
  self.FAttributeSTD_MP.XMLDiagram_Parcels_SubParcels :=  self.FXmlSTDMP.Diagram_Parcels_SubParcels;
  VisibleFrameGenInf(Sender);
  SetSettingsActivCtrl(Sender);
end;

procedure TFormBPXML_GeneralInformation.lbDocumentsClick(Sender: TObject);
begin
  VisibleFrameGenInf(Sender);
  self.FDocuments.XmlInputDataDocuments := FXMlInputData.Documents;
  SetSettingsActivCtrl(Sender)
end;

procedure TFormBPXML_GeneralInformation.lbAgreement_DocumentClick
  (Sender: TObject);
begin
  self.FAttributeSTD_MP.XMLAgreement_Document := self.FXmlSTDMP.Agreement_Document;
  VisibleFrameGenInf(Sender);
  SetSettingsActivCtrl(Sender);
end;

procedure TFormBPXML_GeneralInformation.Label9Click(Sender: TObject);
begin
  self.FAttributeSTD_MP.XMLAgreement_Document :=self.FXmlSTDMP.Agreement_Document;
  FAttributeSTD_MP.Visible := true;
  SetSettingsActivCtrl(Sender);
end;

procedure TFormBPXML_GeneralInformation.MemoConclusionEnter(Sender: TObject);
begin
  if FXmlSTDMP = nil then Exit;
  UI_Conclusion_WriteOf(FXmlSTDMP);
end;

{Refresh}
procedure TFormBPXML_GeneralInformation.OnChangeListCount(Sender: TObject);
var i : integer;
begin
end;

{��� �������}
procedure TFormBPXML_GeneralInformation.PanelRecipientMouseLeave
  (Sender: TObject);
var pnlSender : TPanel;  

  procedure HideControlColor(pnl : TPanel);
  var i : Integer;
  begin
      for I := 0  to pnl.ControlCount -1 do
         if ((pnl.Controls[i] is TLabel)  and
            ((pnl.Controls[i] as TLabel).Font.Color = clPurple))  then
         begin
          (pnl.Controls[i] as TLabel).Font.Color := clBlack;
          (pnl.Controls[i] as TLabel).Tag := 0;
         end;
  end;
begin
  {������ ��� ���������  ��������� - ��� ���������  ������ : � ��� ������ "������������ " and "�������� �������"}
  if (Sender = nil) or (TLabel(Sender).Parent = nil) then Exit;  
  if (Sender is TLabel) and (TLabel(Sender).Parent is TPanel) then
  begin
    pnlSender := (TLabel(Sender).Parent as TPanel);
    HideControlColor(pnlSender);
    if TLabel(Sender).Tag = 0 then  TLabel(Sender).Font.Color := clBlack
    else    TLabel(Sender).Font.Color := clPurple;
  end;   
end;

procedure TFormBPXML_GeneralInformation.PanelRecipientMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
  TLabel(Sender).Font.Color := clBlue;
end;

procedure TFormBPXML_GeneralInformation.Purpose_EditExit(Sender: TObject);
begin
  if FXMlTitle <> nil then
    self.UI_Purpose_WriteTo(self.FXMlTitle);
end;

procedure TFormBPXML_GeneralInformation.Reason_EditExit(Sender: TObject);
begin
  if FXMlTitle <> nil then
    self.UI_Reason_WriteTo(self.FXMlTitle);
end;


{����������  ��������� �������}
procedure TFormBPXML_GeneralInformation.SetSettingsActivCtrl(Sender: TObject);
const currentColor = $0000B7B7;
      procedure InitSettingCtrl();
      var i : Integer;  prntParent : TPanel;
      begin
          prntParent := ((Sender as TLabel).Parent as TPanel);
          for I := 0  to   prntParent.ControlCount -1 do
             if ((prntParent.Controls[i] is TLabel)  and
                ((prntParent.Controls[i] as TLabel).Color = currentColor))  then
                  (prntParent.Controls[i] as TLabel).Color := clBtnFace;
      end;
begin
  TLabel(Sender).Tag := 1;
  InitSettingCtrl;
  TLabel(Sender).Color := currentColor;
end;

procedure TFormBPXML_GeneralInformation.SetSTDMP(const aSTDMP: IXMLSTD_MP);
begin
  if self.FXmlSTDMP = aSTDMP then
    Exit;
  self.FXmlSTDMP := aSTDMP;
  self.InitializationUI;
end;

{ - }
procedure TFormBPXML_GeneralInformation.VisibleFrameGenInf(Sender: TObject);
var
  flNodalPointSchemes : boolean; 
begin
  if not(Sender is TLabel) then EXIT;

  FAttributeSTD_MP.Visible := ((Sender as TLabel) = lbSurvey) or
    ((Sender as TLabel) = lbDiagram_Parcels_SubParcels) or
    ((Sender as TLabel) = lbScheme_Disposition_Parcels) or
    ((Sender as TLabel) = lbAgreement_Document) or
    ((Sender as TLabel) = lbScheme_Geodesic_Plotting);

  FNodalPointSchemes.Visible := (Sender as TLabel) = lbNodalPointSchemes;
  FGeodesicBase.Visible := (Sender as TLabel) = lbGeodesic_Bases;
  FMeans_Surveys.Visible := (Sender as TLabel) = lbMeans_Survey;
  FRealty.Visible := (Sender as TLabel) = lbRealty;
  FSubParcels.Visible := (Sender as TLabel) = lbSubParcels;
  FAppendix.Visible := (Sender as TLabel) = lbAppendix;
  FDocuments.Visible := (Sender as TLabel) = lbDocuments;
end;

procedure TFormBPXML_GeneralInformation.SpeedButton_OpenClick(Sender: TObject);
var
  _XMLDocument: IXMLDocument;
  _XMLSTD_MP: IXMLSTD_MP;

begin
  if not self.IsAssignedXmlNode then
    Exit;
  if self.OpenDialog.Execute then
  begin
    _XMLDocument := LoadXMLDocument(self.OpenDialog.FileName);
    _XMLSTD_MP := _XMLDocument.GetDocBinding(rsSTDMP, TXMLSTD_MP,
      TargetNamespace) as IXMLSTD_MP;
    TRY
      MsXml_CopyChildItem(_XMLSTD_MP.EDocument, self.FXmlSTDMP.EDocument,
        rsSender);
      MsXml_CopyChildItem(_XMLSTD_MP.EDocument, self.FXmlSTDMP.EDocument,
        rsRecipient);
      MsXml_CopyChildItem(_XMLSTD_MP, self.FXmlSTDMP, rsContractor);
    FINALLY
      self.InitializationUI;
      _XMLSTD_MP := nil;
      _XMLDocument := nil;
    END;
  end;
end;

procedure TFormBPXML_GeneralInformation.UI_Conclusion_ReadOF
  (const aSTD_MP: IXMLSTD_MP);
begin
  MemoConclusion.Text := MsXml_ReadChildNodeValue(aSTD_MP,cnstXmlConclusion);
end;

procedure TFormBPXML_GeneralInformation.UI_Conclusion_WriteOf
  (const aSTD_MP: IXMLSTD_MP);
var
  _st: string;
begin
  _st := self.MemoConclusion.Text;
  if _st <> '' then
    aSTD_MP.Conclusion := _st
  else
    MsXml_RemoveChildNode(aSTD_MP,cnstXmlConclusion);
end;

procedure TFormBPXML_GeneralInformation.UI_Purpose_ReadOf
  (const aTitle: IXMLSTD_MP_Title);
begin
  mmoPurpose.Lines.Add(MsXml_ReadChildNodeValue(aTitle, cnstXmlPurpose));
end;

procedure TFormBPXML_GeneralInformation.UI_Purpose_WriteTo
  (const aTitle: IXMLSTD_MP_Title);
var
  _st: string;
begin
  _st := self.mmoPurpose.Text;
  if _st <> '' then  aTitle.Purpose := _st
  else  MsXml_RemoveChildNode(aTitle, cnstXmlPurpose);
end;

procedure TFormBPXML_GeneralInformation.UI_ReadOf(const aTitle
  : IXMLSTD_MP_Title);
begin
  UI_Purpose_ReadOf(aTitle);
  UI_Reason_ReadOf(aTitle);
end;

procedure TFormBPXML_GeneralInformation.UI_Read_AttributeSTD_MP(const aSTD_MP: IXMLSTD_MP);
var
  _node: IXMLNode;
begin
  _node := FXmlSTDMP.ChildNodes.FindNode(cnstXmlSurvey);
  if _node <> nil then
  begin
    self.FAttributeSTD_MP.XMLSurvey := self.FXmlSTDMP.Survey;
    if (_node.ChildNodes.Count  > 0 )then   VisibleFrameGenInf(lbSurvey);
  end
  else   self.FAttributeSTD_MP.XMLSurvey := nil;

  _node := FXmlSTDMP.ChildNodes.FindNode(cnstXMlScheme_Geodesic_Plotting);
  if _node <> nil then
  begin
    self.FAttributeSTD_MP.XMLScheme_Geodesic_Plotting := self.FXmlSTDMP.Scheme_Geodesic_Plotting;
    if (_node.ChildNodes.Count > 0  )  then VisibleFrameGenInf(lbScheme_Geodesic_Plotting);
  end 
  else   self.FAttributeSTD_MP.XMLScheme_Geodesic_Plotting := nil;

  _node := FXmlSTDMP.ChildNodes.FindNode(cnstXmlScheme_Disposition_Parcels);
  if _node <> nil then
  begin
    self.FAttributeSTD_MP.XMLScheme_Disposition_Parcels :=  self.FXmlSTDMP.Scheme_Disposition_Parcels;
    if (_node.ChildNodes.Count > 0) then   VisibleFrameGenInf(lbScheme_Disposition_Parcels);
  end
  else   self.FAttributeSTD_MP.XMLScheme_Disposition_Parcels := nil;

  _node := FXmlSTDMP.ChildNodes.FindNode(cnstXmlDiagram_Parcels_SubParcels);
  if _node <> nil then
  begin
    self.FAttributeSTD_MP.XMLDiagram_Parcels_SubParcels := self.FXmlSTDMP.Diagram_Parcels_SubParcels;
    if (_node.ChildNodes.Count > 0 ) then  VisibleFrameGenInf(lbDiagram_Parcels_SubParcels);
  end
  else    self.FAttributeSTD_MP.XMLDiagram_Parcels_SubParcels := nil;

  _node := FXmlSTDMP.ChildNodes.FindNode(cnstXmlAgreement_Document);
  if _node <> nil then
  begin
    self.FAttributeSTD_MP.XMLAgreement_Document :=   self.FXmlSTDMP.Agreement_Document;
    if (_node.ChildNodes.Count > 0 ) then  VisibleFrameGenInf(lbAgreement_Document);
  end
  else   self.FAttributeSTD_MP.XMLAgreement_Document := nil;
  
  _node := FXmlSTDMP.ChildNodes.FindNode(cnstXmlNodalPointSchemes);
  if _node <> nil then
  begin
    self.FNodalPointSchemes.XmlNodalPointSchemes := FXmlSTDMP.NodalPointSchemes;
    if (_node.ChildNodes.Count > 0 ) then    VisibleFrameGenInf(lbNodalPointSchemes);
  end
  else  FNodalPointSchemes.XmlNodalPointSchemes := nil;
  
  _node := FXmlSTDMP.ChildNodes.FindNode(cnstXmlAppendix);
  if _node <> nil then
  begin
    self.FAppendix.XmlAppendix := FXmlSTDMP.Appendix;
    if (_node.ChildNodes.Count > 0 ) then    VisibleFrameGenInf(lbAppendix);
  end
  else  FAppendix.XmlAppendix := nil;
  _node := nil;
end;

procedure TFormBPXML_GeneralInformation.UI_Read_InputDate
  (const aSTD_MP: IXMLSTD_MP);
var
  _node: IXMLNode;
begin
  FXMlInputData := FXmlSTDMP.Input_Data;
  if FXMlInputData <> nil then
  begin
    _node := FXMlInputData.ChildNodes.FindNode(cnstXmlDocuments);
    if _node <> nil then
    begin
      self.FDocuments.XmlInputDataDocuments := self.FXMlInputData.Documents;
      VisibleFrameGenInf(lbDocuments);
    end
    else
      self.FDocuments.XmlInputDataDocuments := nil;

    _node := FXMlInputData.ChildNodes.FindNode(cnstXmlGeodesic_Bases);
    if _node <> nil then
    begin
      self.FGeodesicBase.XmlGeodesis_Bases := self.FXMlInputData.Geodesic_Bases;
      VisibleFrameGenInf(lbGeodesic_Bases);
    end
    else
      self.FGeodesicBase.XmlGeodesis_Bases := nil;

    _node := FXMlInputData.ChildNodes.FindNode(cnstXmlMeans_Survey);
    if _node <> nil then
    begin
      self.FMeans_Surveys.XmlMeans_Surveys := self.FXMlInputData.Means_Survey;
      VisibleFrameGenInf(lbMeans_Survey);
    end
    else
      self.FMeans_Surveys.XmlMeans_Surveys := nil;
    _node := FXMlInputData.ChildNodes.FindNode(cnstXmlRealty);
    if _node <> nil then
    begin
      self.FRealty.XmlRealtys := self.FXMlInputData.Realty;
      VisibleFrameGenInf(lbRealty);
    end
    else
      self.FRealty.XmlRealtys := nil;
    _node := FXMlInputData.ChildNodes.FindNode(cnstXmlInputDateSubParcels);
    if _node <> nil then
    begin
      self.FSubParcels.XmlSubParcels := self.FXMlInputData.SubParcels;
      VisibleFrameGenInf(lbSubParcels);
    end
    else
      self.FSubParcels.XmlSubParcels := nil;

  end
  else
  begin
    FDocuments.XmlInputDataDocuments := nil;
    FGeodesicBase.XmlGeodesis_Bases := nil;
    FMeans_Surveys.XmlMeans_Surveys := nil;
    FRealty.XmlRealtys := nil;
    FSubParcels.XmlSubParcels := nil;
  end;
end;

procedure TFormBPXML_GeneralInformation.UI_Reason_ReadOf
  (const aTitle: IXMLSTD_MP_Title);
begin
  mmoReason.Lines.Add(MsXml_ReadChildNodeValue(aTitle, cnstXmlReason));
end;

procedure TFormBPXML_GeneralInformation.UI_Reason_WriteTo
  (const aTitle: IXMLSTD_MP_Title);
var
  _st: string;
begin
  _st := self.mmoReason.Text;
  if _st <> '' then
    aTitle.Reason := _st
  else
    MsXml_RemoveChildNode(aTitle, cnstXmlReason);
end;

procedure TFormBPXML_GeneralInformation.UI_SystemCoor_ReadOf(
  const aSTD_MP: IXMLSTD_MP);
begin
  if MsXml_ReadAttribute(aSTD_MP.Coord_Systems.Coord_System,'Name') = '' then
     self.edtSystemCoorName.Text := '�� ������������ ������'
  else  self.edtSystemCoorName.Text := MsXml_ReadAttribute(aSTD_MP.Coord_Systems.Coord_System,'Name');
end;


procedure TFormBPXML_GeneralInformation.UI_WriteTo(const aTitle
  : IXMLSTD_MP_Title);
begin
  UI_Purpose_WriteTo(aTitle);
  UI_Reason_WriteTo(aTitle);
end;

end.

