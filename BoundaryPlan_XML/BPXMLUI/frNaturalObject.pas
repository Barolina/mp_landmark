unit frNaturalObject;
{
 TODO : ��� Newparcels
}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, StdCtrls,

  STD_MP, BpXML_different, ActnList, System.Actions,  unRussLang;

type
  TFrameNaturalObject = class(TFrame)
    ForestUse_ComboBox: TComboBox;
    TypeProtectiveForest_Edit: TEdit;
    ForestUse_Label_Caption: TLabel;
    Label2: TLabel;
    ActionList: TActionList;
    Action_ForestUse_write: TAction;
    Action_TypeProtectiveForest_write: TAction;
    procedure Action_ForestUse_writeExecute(Sender: TObject);
    procedure Action_TypeProtectiveForest_writeExecute(Sender: TObject);
    procedure TypeProtectiveForest_EditKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
    FXmlNaturalObject: IXMLTNatural_Object;
  protected
    procedure InitializationUI;
    procedure SetNaturalObject(const NatObj: IXMLTNatural_Object);
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;

    procedure UI_Name_233001000000_WriteTo(const aNaturalObject: IXMLTNatural_Object);
    procedure UI_ForestUse_ReadOf(const aNaturalObject: IXMLTNatural_Object);
    procedure UI_ForestUse_WriteTo(const aNaturalObject: IXMLTNatural_Object);
    function UI_ForestUse_IsValid: boolean;
    procedure UI_TypeProtectiveForest_ReadOf(const aNaturalObject: IXMLTNatural_Object);
    procedure UI_TypeProtectiveForest_WriteTo(const aNaturalObject: IXMLTNatural_Object);
    function UI_TypeProtectiveForest_IsEmpty: boolean;
    procedure UI_ReadOf(const aNaturalObject: IXMLTNatural_Object);
    procedure UI_WriteTo(const aNaturalObject: IXMLTNatural_Object);

    function IsAssignedNaturalObject: boolean;
    property XmlNaturalObject: IXMLTNatural_Object read FXmlNaturalObject write SetNaturalObject;
  end;

implementation

uses MsXMLAPI;

{$R *.dfm}

{ TFrameNaturalObject }

procedure TFrameNaturalObject.Action_ForestUse_writeExecute(Sender: TObject);
begin
 if not self.IsAssignedNaturalObject then raise Exception.Create(ExMSG_NotAssignedROOT);
 self.UI_ForestUse_WriteTo(self.FXmlNaturalObject);
end;

procedure TFrameNaturalObject.Action_TypeProtectiveForest_writeExecute(Sender: TObject);
begin
 if not self.IsAssignedNaturalObject then raise Exception.Create(ExMSG_NotAssignedROOT);
 self.UI_TypeProtectiveForest_WriteTo(self.FXmlNaturalObject);
end;

constructor TFrameNaturalObject.Create(AOwner: TComponent);
begin
 inherited;
 TSTD_MP_ListofIndexGenerate.BpXml_LoadDictionary(STD_MP_WorkDir+'dForest_Use.xsd',self.ForestUse_ComboBox.Items);
end;

procedure TFrameNaturalObject.InitializationUI;
begin
 if self.IsAssignedNaturalObject then
  self.UI_ReadOf(self.FXmlNaturalObject)
 else begin
  self.ForestUse_ComboBox.ItemIndex:= -1;
  self.TypeProtectiveForest_Edit.Text:= '';
 end;
end;

function TFrameNaturalObject.IsAssignedNaturalObject: boolean;
begin
 Result:= self.FXmlNaturalObject<>nil;
end;

procedure TFrameNaturalObject.SetNaturalObject(const NatObj: IXMLTNatural_Object);
begin
 if self.FXmlNaturalObject=NatObj then EXIT;
 self.FXmlNaturalObject:= NatObj;
 self.InitializationUI;
end;

procedure TFrameNaturalObject.TypeProtectiveForest_EditKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
 if key = VK_RETURN then
  if (Sender is TCustomEdit) then
   TCustomEdit(Sender).Parent.SetFocus;
end;

function TFrameNaturalObject.UI_ForestUse_IsValid: boolean;
begin
 Result:= self.ForestUse_ComboBox.ItemIndex<>-1;
end;

procedure TFrameNaturalObject.UI_ForestUse_ReadOf(const aNaturalObject: IXMLTNatural_Object);
var
 _code: string;
begin
 _code:= MsXml_ReadChildNodeValue(aNaturalObject,'ForestUse');
 self.ForestUse_ComboBox.ItemIndex:=
 TSTD_MP_ListofIndexGenerate.BpXml_FindItemByCode(self.ForestUse_ComboBox.Items,_code);
end;

procedure TFrameNaturalObject.UI_ForestUse_WriteTo(const aNaturalObject: IXMLTNatural_Object);
var
 _item: TBpXml_DictionaryItem;
 _index: Integer;
begin
 self.UI_Name_233001000000_WriteTo(self.FXmlNaturalObject);
 if not self.UI_ForestUse_IsValid then raise Exception.Create(format(ExMsg_Data_Invalid,
                                                  [self.ForestUse_Label_Caption.Caption]));

 _index:= self.ForestUse_ComboBox.ItemIndex;
 _item:= TBpXml_DictionaryItem(self.ForestUse_ComboBox.Items.Objects[_index]);
 aNaturalObject.ForestUse:= _item.Code;
end;

procedure TFrameNaturalObject.UI_Name_233001000000_WriteTo(const aNaturalObject: IXMLTNatural_Object);
begin
  aNaturalObject.Name:= '233001000000';
end;

procedure TFrameNaturalObject.UI_ReadOf(const aNaturalObject: IXMLTNatural_Object);
begin
 self.UI_ForestUse_ReadOf(aNaturalObject);
 self.UI_TypeProtectiveForest_ReadOf(aNaturalObject);
end;

function TFrameNaturalObject.UI_TypeProtectiveForest_IsEmpty: boolean;
begin
 Result:= self.TypeProtectiveForest_Edit.Text='';
end;

procedure TFrameNaturalObject.UI_TypeProtectiveForest_ReadOf(const aNaturalObject: IXMLTNatural_Object);
begin
  self.TypeProtectiveForest_Edit.Text:= MsXml_ReadChildNodeValue(aNaturalObject,'Type_ProtectiveForest');
end;

procedure TFrameNaturalObject.UI_TypeProtectiveForest_WriteTo(const aNaturalObject: IXMLTNatural_Object);
begin
 if not self.UI_TypeProtectiveForest_IsEmpty then
   aNaturalObject.Type_ProtectiveForest:= self.TypeProtectiveForest_Edit.Text
 else
  MsXml_RemoveChildNode(aNaturalObject,'Type_ProtectiveForest');
end;

procedure TFrameNaturalObject.UI_WriteTo( const aNaturalObject: IXMLTNatural_Object);
begin
 self.UI_Name_233001000000_WriteTo(aNaturalObject);
 self.UI_ForestUse_WriteTo(aNaturalObject);
 self.UI_TypeProtectiveForest_WriteTo(aNaturalObject);
end;


end.
