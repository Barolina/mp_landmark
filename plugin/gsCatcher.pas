unit gsCatcher;

//**! ----------------------------------------------------------
//**! This unit is a part of GSPackage project (Gregory Sitnin's
//**! Delphi Components Package).
//**! ----------------------------------------------------------
//**! You may use or redistribute this unit for your purposes
//**! while unit's code and this copyright notice is unchanged
//**! and exists.
//**! ----------------------------------------------------------
//**! (c) Gregory Sitnin, 2001-2002. All rights reserved.
//**! ----------------------------------------------------------

{***} interface {***}

uses classes, sysutils, jpeg;

type
tgscatcher = class(tcomponent)
private
fenabled: boolean;
fgeneratescreenshot: boolean;
fjpegscreenshot: boolean;
fjpegquality: tjpegqualityrange;
fcollectinfo: boolean;
fn: tfilename;
procedure setenabled(const value: boolean);
{ private declarations }
protected
{ protected declarations }
procedure enablecatcher;
procedure disablecatcher;
public
{ public declarations }
constructor create(aowner: tcomponent); override;
destructor destroy; override;
procedure catcher(sender: tobject; e: exception);
procedure dogeneratescreenshot;
function collectusername: string;
function collectcomputername: string;
procedure docollectinfo(e: exception);
published
{ published declarations }
property enabled: boolean read fenabled write setenabled
default false;
end;

procedure register;

{***} implementation {***}

uses windows, forms, dialogs, graphics,jpeg;

procedure register;
begin
registercomponents('gregory sitnin', [tgscatcher]);
end;

{ tgscatcher }
function tgscatcher.collectcomputername: string;
var
cname: pchar;
cnsiz: cardinal;
begin
cname := stralloc(max_computername_length + 1);
cnsiz := max_computername_length + 1;
getcomputername(cname,cnsiz);
if (cnsiz > 0) then
result := string(cname) else
result := 'n/a';
strdispose(cname);
end;

function tgscatcher.collectusername: string;
var
uname: pchar;
unsiz: cardinal;
begin
uname := stralloc(255);
unsiz := 254;
getusername(uname,unsiz);
if (unsiz > 0) then
result := string(uname) else
result := 'n/a';
strdispose(uname);

end;

constructor tgscatcher.create(aowner: tcomponent);
begin
inherited;
end;

destructor tgscatcher.destroy;
begin
disablecatcher;
inherited;
end;

procedure tgscatcher.setenabled(const value: boolean);
begin
fenabled := value;
if enabled then enablecatcher else disablecatcher;
end;

procedure tgscatcher.disablecatcher;
begin
application.onexception := nil;
end;

procedure tgscatcher.docollectinfo(e: exception);
var sl: tstringlist;
begin
sl := tstringlist.create;
sl.add('--- this report is created by automated '+
'reporting system.');
sl.add('computer name is: ['+collectcomputername+']');
sl.add('user name is: ['+collectusername+']');
sl.add('--- end of report ----------------------'+
'-----------------');
sl.savetofile(fn+'.txt');
end;

procedure tgscatcher.dogeneratescreenshot;
var bmp: tbitmap;
jpg: tjpegimage;
begin
bmp := screen.activeform.getformimage;
begin
jpg := tjpegimage.create;
jpg.compressionquality := 100;
jpg.assign(bmp);
jpg.savetofile(fn+'.jpg');
freeandnil(jpg);
end;
freeandnil(bmp);
end;

procedure tgscatcher.enablecatcher;
begin
application.onexception := catcher;
end;

procedure tgscatcher.catcher(sender: tobject; e: exception);
begin
{todo: write some exception handling code}
end;

end.



