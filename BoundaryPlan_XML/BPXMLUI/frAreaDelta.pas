unit frAreaDelta;

{
     � �� ����������� �  �� ���������  + and ExistEZ
}
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, StdCtrls, ExtCtrls, Mask,

  STD_MP, ActnList, System.Actions,XMLIntf,unRussLang;

type
  TFrameAreaDelta = class(TFrame)
    Area_In_GKN_MaskEdit: TMaskEdit;
    Delta_Area_MaskEdit: TMaskEdit;
    Area_In_GKN_Label_Caption: TLabel;
    Delta_Area_Label_Caption: TLabel;
    ActionList: TActionList;
    Min_Area_Lab_Cap: TLabel;
    Min_Area_MaskEdit: TMaskEdit;
    Max_Area_Lab_Cap: TLabel;
    Max_Area_MaskEdit: TMaskEdit;
    Label1: TLabel;
    Note_Ed: TEdit;
    Action_Area_In_GKN: TAction;
    Action_Delta_Write: TAction;
    Action_Min_Area_Write: TAction;
    Action_Max_Area_Write: TAction;
    Action_Note: TAction;
    GroupBox1: TGroupBox;
    procedure Area_In_GKN_MaskEditKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Delta_Area_MaskEditKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Action_Area_In_GKNExecute(Sender: TObject);
    procedure Action_Delta_WriteExecute(Sender: TObject);
    procedure Action_Min_Area_WriteExecute(Sender: TObject);
    procedure Min_Area_MaskEditKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Action_Max_Area_WriteExecute(Sender: TObject);
    procedure Max_Area_MaskEditKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Action_NoteExecute(Sender: TObject);
    procedure Note_EdKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
    FXmlArea_In_GKN   : LongWord;
    FXmlArea_Delta    : LongWord;
    FXmlArea_Min_Area : IXMLTArea_without_Innccuracy;
    FXmlArea_Max_Area : IXMLTArea_without_Innccuracy;
    FXmlNote          : UnicodeString;

    FXmlExistParcel   : IXMLTExistParcel;
    FXmlNewParcel     : IXMLTNewParcel;
    FXmlExistEZparcel : IXMLTExistEZParcel;
    FXmlExistEZEntryPArcel : IXMLTExistEZEntryParcel;

    FXmlParcel        : IXMLNode;
  protected
   procedure InitializationUI;
   procedure SetExistParcel(aExistParcel: IXMLTExistParcel);
   procedure SetNewParcel(aNewParcel : IXMLTNewParcel);
   procedure SetExistEZParcel(aExistEZParcel : IXMLTExistEZParcel);
   procedure SetExistEXExistParcel(aExistEZarcel : IXMLTExistEZEntryParcel);
  public
   constructor Create(AOwner: TComponent); override;
    { Public declarations }
   procedure UI_Area_IN_GKN_WriteTo;
   procedure UI_Area_IN_GKN_ReadOf;
   function  UI_Area_IN_GKN_IsValid: boolean;

   procedure UI_Delta_WriteTo;
   procedure UI_Delta_ReadOf;
   function  UI_Delta_IsValid: boolean;

   procedure UI_Area_Min_WriteTo;
   procedure UI_Unit_Min_Write055To;
   procedure UI_Area_Min_ReadOf;
   function  UI_Area_Min_IsValid: boolean;

   procedure UI_Area_Max_WriteTo;
   procedure UI_Unit_Max_Write055To;
   procedure UI_Area_Max_ReadOf;
   function  UI_Area_Max_IsValid: boolean;

   procedure UI_Note_WriteTo;
   procedure UI_Note_ReadOf;

   procedure UI_WriteTo;
   procedure UI_ReadOf;

   function IsAssignedExistParcel: boolean;
   property XmlExistParcel: IXMLTExistParcel read FXmlExistParcel write SetExistParcel;
   property XmlNewParcel  : IXMLTNewParcel   read FXmlNewParcel   write SetNewParcel;
   property XmlExistEZParcel : IXMLTExistEZParcel read FXmlExistEZparcel write SetExistEZParcel;
   property XmlExistEZExistParcel : IXMLTExistEZEntryParcel read FXmlExistEZEntryPArcel write SetExistEXExistParcel;

  end;

implementation

uses MsXMLAPI;


{$R *.dfm}


{ TFrameArea }


procedure TFrameAreaDelta.Action_Area_In_GKNExecute(Sender: TObject);
begin
 if not self.IsAssignedExistParcel then raise Exception.Create(ExMSG_NotAssignedROOT);
 if self.Area_In_GKN_MaskEdit.Text = '' then
 begin 
    MsXml_RemoveChildNode(FXmlParcel,cnstXmlArea_In_GKN);
    Exit;
 end;
 
 self.UI_Area_IN_GKN_WriteTo;
end;

procedure TFrameAreaDelta.Action_Delta_WriteExecute(Sender: TObject);
begin
 if not self.IsAssignedExistParcel then raise Exception.Create(ExMSG_NotAssignedROOT);
 if self.Delta_Area_MaskEdit.Text = '' then
 begin
    MsXml_RemoveChildNode(FXmlParcel,cnstXmlDelta);
    Exit;
 end;
 self.UI_Delta_WriteTo;
end;

procedure TFrameAreaDelta.Action_Max_Area_WriteExecute(Sender: TObject);
begin
 if not self.IsAssignedExistParcel then raise Exception.Create(ExMSG_NotAssignedROOT);
 {����  ��������  -  �������}
 if self.Max_Area_MaskEdit.Text = ''  then
 begin
   MsXml_RemoveChildNode(FXmlParcel,cnstXmlMax_Area);
   Exit;
 end;  
 
 self.UI_Area_Max_WriteTo;
 self.UI_Unit_Max_Write055To;
end;

procedure TFrameAreaDelta.Action_Min_Area_WriteExecute(Sender: TObject);
begin
 if not self.IsAssignedExistParcel then raise Exception.Create(ExMSG_NotAssignedROOT);
 {����  ��������  -  �������  }
 if self.Min_Area_MaskEdit.Text = '' then 
 begin
    MsXml_RemoveChildNode(FXmlParcel, cnstXmlMin_Area);
    Exit;
 end;   
 
 self.UI_Area_Min_WriteTo;
 self.UI_Unit_Min_Write055To;
end;

procedure TFrameAreaDelta.Action_NoteExecute(Sender: TObject);
begin
 if not self.IsAssignedExistParcel then raise Exception.Create(ExMSG_NotAssignedROOT);
 if self.Note_Ed.Text  = '' then
 begin
   MsXml_RemoveChildNode(FXmlParcel,cnstXmlNote);
   Exit;
 end;
 self.UI_Note_WriteTo;
end;

procedure TFrameAreaDelta.Area_In_GKN_MaskEditKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if key = VK_RETURN then
  if (Sender is TCustomEdit) then
   TCustomEdit(Sender).Parent.SetFocus;
end;

constructor TFrameAreaDelta.Create(AOwner: TComponent);
begin
  inherited;
end;

procedure TFrameAreaDelta.InitializationUI;

  procedure  SetVisibleParamExPl();
  var nameNode : string;
      isAreaInGKN,isDelta,isMinArea,isMaxArea : Boolean;
  begin
    nameNode := FXmlParcel.NodeName;
    isAreaInGKN :=  (nameNode = cnstXmlExistParcel) or
                    (nameNode = cnstXmlExistEZEntryParcel )or
                    (nameNode = cnstXmlExistEZParcels);
    Area_In_GKN_Label_Caption.Visible := isAreaInGKN;
    Area_In_GKN_MaskEdit.Visible := isAreaInGKN;
    isDelta :=      (nameNode = cnstXmlExistEZParcels )or
                    (nameNode = cnstXmlExistParcel );
    Delta_Area_Label_Caption.Visible :=isDelta;
    Delta_Area_MaskEdit.Visible := isDelta;
    isMaxArea :=    (nameNode =  cnstXmlNewParcel) or
                    (nameNode =  cnstXmlExistParcel) or
                    (nameNode =  cnstXmlExistEZParcels );
    Max_Area_Lab_Cap.Visible := isMaxArea;
    Max_Area_MaskEdit.Visible := isMaxArea;
{ ���� ���������  max and min � ����� ��������� }
    Min_Area_Lab_Cap.Visible :=  isMaxArea;
    Min_Area_MaskEdit.Visible := isMaxArea;
  end;
var  i  : integer;
begin
 if self.IsAssignedExistParcel then
 begin
    SetVisibleParamExPl;
    self.UI_ReadOf;
 end
 else
 begin
  for I := 0 to GroupBox1.ControlCount-1 do
   if (GroupBox1.Controls[i]  is TMaskEdit)  then (GroupBox1.Controls[i] as TMaskEdit).Text := '';
  Note_Ed.Text := '';
 end;
end;

procedure TFrameAreaDelta.Delta_Area_MaskEditKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if key = VK_RETURN then
  if (Sender is TCustomEdit) then
   TCustomEdit(Sender).Parent.SetFocus;
end;

function TFrameAreaDelta.IsAssignedExistParcel: boolean;
begin
 Result:= self.FXmlParcel<>nil;
end;

procedure TFrameAreaDelta.Max_Area_MaskEditKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key = VK_RETURN then
  if (Sender is TCustomEdit) then
   TCustomEdit(Sender).Parent.SetFocus;
end;

procedure TFrameAreaDelta.Min_Area_MaskEditKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
   if key = VK_RETURN then
  if (Sender is TCustomEdit) then
   TCustomEdit(Sender).Parent.SetFocus;
end;

procedure TFrameAreaDelta.Note_EdKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key = VK_RETURN then
  if (Sender is TCustomEdit) then
   TCustomEdit(Sender).Parent.SetFocus;
end;

procedure TFrameAreaDelta.SetExistEXExistParcel(
  aExistEZarcel: IXMLTExistEZEntryParcel);
begin
 if self.FXmlParcel =aExistEZarcel then EXIT;
 self.FXmlParcel:= aExistEZarcel;
 self.InitializationUI;
end;

procedure TFrameAreaDelta.SetExistEZParcel(aExistEZParcel: IXMLTExistEZParcel);
begin
 if self.FXmlParcel =aExistEZParcel then EXIT;
 self.FXmlParcel:= aExistEZParcel;
 self.InitializationUI;
end;

procedure TFrameAreaDelta.SetExistParcel;
begin
 if self.FXmlParcel =aExistParcel then EXIT;
 self.FXmlParcel:= aExistParcel;
 self.InitializationUI;
end;



procedure TFrameAreaDelta.SetNewParcel(aNewParcel: IXMLTNewParcel);
begin
 if self.FXmlParcel =aNewParcel then EXIT;
 self.FXmlParcel:= aNewParcel;
 self.InitializationUI;
end;

function TFrameAreaDelta.UI_Area_IN_GKN_IsValid: boolean;
var
 _t: Double;
begin
 Result:= TryStrToFloat(Self.Area_In_GKN_MaskEdit.Text,_t);
end;

procedure TFrameAreaDelta.UI_Area_IN_GKN_ReadOf;
begin
  self.Area_In_GKN_MaskEdit.Text:= MsXml_ReadChildNodeValue(FXmlParcel,cnstXmlArea_In_GKN);
end;

procedure TFrameAreaDelta.UI_Area_IN_GKN_WriteTo;
begin
 if self.Area_In_GKN_MaskEdit.Text='' then EXIT;
 if not self.UI_Area_IN_GKN_IsValid then
 begin
  self.UI_Area_IN_GKN_ReadOf;
  raise Exception.Create(format(ExMsg_Data_Invalid,[self.Area_In_GKN_Label_Caption.Caption]));
 end;
 FXmlParcel.ChildValues[cnstxmlArea_In_GKN] := Self.Area_In_GKN_MaskEdit.Text;
end;


function TFrameAreaDelta.UI_Area_Max_IsValid: boolean;
var
 _t: Double;
begin
 Result:= TryStrToFloat(Self.Max_Area_MaskEdit.Text,_t);
end;

procedure TFrameAreaDelta.UI_Area_Max_ReadOf;
var
 xmlMaxArea : IXMLNode;
begin
 xmlMaxArea :=  FXmlParcel.ChildNodes.FindNode(cnstXmlMax_Area);
 if xmlMaxArea <> nil  then
  self.Max_Area_MaskEdit.Text:= MsXml_ReadChildNodeValue(xmlMaxArea,cnstXmlArea)
 else
  self.Max_Area_MaskEdit.Text:= '';
end;

procedure TFrameAreaDelta.UI_Area_Max_WriteTo;
begin
 if self.Max_Area_MaskEdit.Text='' then EXIT;
 if not self.UI_Area_Max_IsValid then
 begin
  self.UI_Area_Max_ReadOf;
  raise Exception.Create(format(ExMsg_Data_Invalid,[self.Max_Area_Lab_Cap.Caption]));
 end;

 FXmlParcel.ChildNodes[cnstXmlMax_Area].ChildValues[cnstXmlArea] := Self.Max_Area_MaskEdit.Text;
end;

function TFrameAreaDelta.UI_Area_Min_IsValid: boolean;
var
 _t: Double;
begin
 Result:= TryStrToFloat(Self.Min_Area_MaskEdit.Text,_t);
end;

procedure TFrameAreaDelta.UI_Area_Min_ReadOf;
var
 xmlMinArea : IXMLNode;
begin
 xmlMinArea := FXmlParcel.ChildNodes.FindNode(cnstXmlMin_Area);
 if xmlMinArea <> nil  then
  self.Min_Area_MaskEdit.Text:= MsXml_ReadChildNodeValue(xmlMinArea,cnstXmlArea)
 else
  self.Min_Area_MaskEdit.Text:= '';
end;

procedure TFrameAreaDelta.UI_Area_Min_WriteTo;
begin
 if self.Min_Area_MaskEdit.Text='' then Exit;
 if not self.UI_Area_Min_IsValid then begin
  self.UI_Area_Min_ReadOf;
  raise Exception.Create(format(ExMsg_Data_Invalid,[self.Min_Area_Lab_Cap.Caption]));
 end;
 FXmlParcel.ChildNodes[cnstXmlMin_Area].ChildValues[cnstXmlArea] := Self.Min_Area_MaskEdit.Text;
end;

function TFrameAreaDelta.UI_Delta_IsValid: boolean;
var
 _t: Double;
begin
 Result:= TryStrToFloat(Self.Delta_Area_MaskEdit.Text,_t);
end;

procedure TFrameAreaDelta.UI_Delta_ReadOf;
begin
  self.Delta_Area_MaskEdit.Text:= MsXml_ReadChildNodeValue(FXmlParcel,cnstXmlDelta);
end;

procedure TFrameAreaDelta.UI_Delta_WriteTo;
begin
  if self.Delta_Area_MaskEdit.Text='' then EXIT;
 if not self.UI_Delta_IsValid then begin
  self.UI_Delta_ReadOf;
  raise Exception.Create(format(ExMsg_Data_Invalid,[self.Delta_Area_Label_Caption.Caption]));
 end;
 FXmlParcel.ChildValues[cnstXmlDelta] := Self.Delta_Area_MaskEdit.Text;
end;

procedure TFrameAreaDelta.UI_Note_ReadOf;
begin
  self.Note_Ed.Text:= MsXml_ReadChildNodeValue(FXmlParcel,cnstXmlNote);
end;

procedure TFrameAreaDelta.UI_Note_WriteTo;
begin
 if self.Note_Ed.Text='' then EXIT;
 FXmlParcel.ChildValues[cnstXmlNote] := Note_Ed.Text;
end;

procedure TFrameAreaDelta.UI_ReadOf;
begin
 UI_Note_ReadOf;
 if Area_In_GKN_MaskEdit.Visible then UI_Area_IN_GKN_ReadOf;
 if Delta_Area_MaskEdit.Visible then self.UI_Delta_ReadOf;
 if Min_Area_MaskEdit.Visible  then self.UI_Area_Min_ReadOf;
 if Max_Area_MaskEdit.Visible then self.UI_Area_Max_ReadOf;
end;

procedure TFrameAreaDelta.UI_Unit_Max_Write055To;
begin
  FXmlParcel.ChildNodes.Nodes[cnstXmlMax_Area].ChildValues[cnstXmlUnit] := StdMp_Area_Unit;
end;

procedure TFrameAreaDelta.UI_Unit_Min_Write055To;
begin
  FXmlParcel.ChildNodes.Nodes[cnstXmlMin_Area].ChildValues[cnstXmlUnit] := StdMp_Area_Unit;
end;

procedure TFrameAreaDelta.UI_WriteTo;
begin
 if Area_In_GKN_MaskEdit.Visible then UI_Area_IN_GKN_WriteTo;
 if Delta_Area_MaskEdit.Visible then self.UI_Delta_WriteTo;
 if Min_Area_MaskEdit.Visible then
 begin
     self.UI_Area_Min_WriteTo;
     self.UI_Unit_Min_Write055To;
 end;
 if Max_Area_MaskEdit.Visible then
 begin
     self.UI_Area_Max_WriteTo;
     self.UI_Unit_Max_Write055To;
 end;
 self.UI_Note_WriteTo;
end;

end.
