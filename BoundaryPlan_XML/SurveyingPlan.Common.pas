unit SurveyingPlan.Common;

interface
 type
  TCadNumItemsKind = (cnDistrict,cnMunicipality,cnBlock,cnCadNum,cnContour,
                      cnNewNum,cnNewContour,cnSubParcelNum,cnSubParcelContour, cnFullCadNum);
                      
 const
  cnCadNumItems: array[cnDistrict..cnFullCadNum] of string =
                ('district','municipality','block','cadnum','contour',
                 'newnum','newcontour','subparcelnum','subparcelcontour','cadnum');
  cnRegExCadastralNumber: string =
       '((?<district>\d+):(?<municipality>\d+):(?<block>\d+))?'+
       '(:)((?<cadnum>\d+)?(\((?<contour>[:\d]*)\))?)?'+
       ':?([����]{1,}(?<newnum>\d+))?(\((?<newcontour>\d*)\))?'+
       '(\/[������]{1,}(?<subparcelnum>\d+){1}(\((?<subparcelcontour>\d*)\))?)?';

  cnRegCadastralNumber = '\d+:\d+:\d+:\d+';
  cnRegCadastralBlock = '\d+:\d+:\d+';
  cnRegCadastralNumberAndContourIndex = '(?<cadnum>\d+:\d+:\d+:\d+)(\((?<contour>\d+)\))?';

//  cnRegFormParcelCadastralNumber = '(\d+:\d+:\d+:\d+)?:��\d+';
//  cnRegNewContourDefition ='d+:\d+:\d+:\d+)?:��\d+[(]\d+[)';

implementation
{$region 'for test cadnum'}
 {36:25:000000:��1(1)
11:22:3333333:4(111:222:333:44):��6(7)/���8(9)
11:22:3333333:4(5):��6(7)/���8(9)
:1(1):��1(1)/���1(1)
36:25:000000:1
36:25:000000:1(1)
:1
:1(1)
:(1)
:(1)/���1(1)
36:25:000000:1/���1
36:25:000000:1/���1(1)
36:25:000000:��1
36:25:000000:1:��1
36:25:000000:10:��10(10)
36:25:000000:��1/���1(1)
36:25:000000:��1(1)/���1(1)
}
{$endregion}
end.
