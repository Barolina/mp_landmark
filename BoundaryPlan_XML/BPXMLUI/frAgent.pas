unit frAgent;
   {
   Представитель
   IXMLSTD_MP_Title_Client_Organization_Agent
   }

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ButtonGroup, StdCtrls, ActnList,

  STD_MP, frFIO, XMLIntf, unRussLang;

type
  TFrameAgent = class(TFrame)
    Label_Appointment: TLabel;
    Label1: TLabel;
    FIOSurname_Edit: TEdit;
    Label2: TLabel;
    FIOFirst_Edit: TEdit;
    Label3: TLabel;
    FIOPatronymic_Edit: TEdit;
    mmoAppointMent: TMemo;
    procedure Appointment_EditEnter(Sender: TObject);
    procedure Appointment_EditKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FIOSurname_EditExit(Sender: TObject);
    procedure FIOFirst_EditExit(Sender: TObject);
    procedure FIOPatronymic_EditExit(Sender: TObject);
  private
    { Private declarations }
    FXmlAgent : IXMLSTD_MP_Title_Client_Organization_Agent;

    function IsAssignedAgent: boolean;
 protected
   procedure InitializationUI;

   procedure UI_Appointment_ReadOf(const aAgent: IXMLSTD_MP_Title_Client_Organization_Agent);
   procedure UI_FIOSurname_ReadOf(const aAgent: IXMLSTD_MP_Title_Client_Organization_Agent);
   procedure UI_FIOFirst_ReadOf(const aAgent: IXMLSTD_MP_Title_Client_Organization_Agent);
   procedure UI_FIOPatronymic_ReadOf(const aAgent: IXMLSTD_MP_Title_Client_Organization_Agent);
   procedure UI_ReadOf(const aAgent: IXMLSTD_MP_Title_Client_Organization_Agent);

   procedure UI_Appointment_WriteTo(const aAgent: IXMLSTD_MP_Title_Client_Organization_Agent);
   procedure UI_FIOSurname_WriteTo(const aAgent: IXMLSTD_MP_Title_Client_Organization_Agent);
   procedure UI_FIOFirst_WriteTo(const aAgent: IXMLSTD_MP_Title_Client_Organization_Agent);
   procedure UI_FIOPatronymic_WriteTo(const aAgent: IXMLSTD_MP_Title_Client_Organization_Agent);
   procedure UI_WriteTo(const aAgent: IXMLSTD_MP_Title_Client_Organization_Agent);

   procedure SetAgent(const aAgent: IXMLSTD_MP_Title_Client_Organization_Agent);
  public
   { Public declarations }
   constructor Create(AOwner: TComponent); override;
   property XMLAgent:IXMLSTD_MP_Title_Client_Organization_Agent read FXmlAgent write SetAgent;
  end;

implementation

uses MsXmlApi;

{$R *.dfm}
procedure TFrameAgent.Appointment_EditEnter(Sender: TObject);
begin
 if not self.IsAssignedAgent then raise Exception.Create(ExMSG_NotAssignedROOT);
 self.UI_Appointment_WriteTo(self.FXmlAgent);
end;

procedure TFrameAgent.Appointment_EditKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
{TODO:26 if key = VK_RETURN then
  if (Sender is TCustomEdit) then
   TCustomEdit(Sender).Parent.SetFocus;}
end;

constructor TFrameAgent.Create(AOwner: TComponent);
begin
  inherited;
end;


procedure TFrameAgent.FIOFirst_EditExit(Sender: TObject);
begin
 if not self.IsAssignedAgent then raise Exception.Create(ExMSG_NotAssignedROOT);
 self.UI_FIOFirst_WriteTo(self.FXmlAgent);
end;

procedure TFrameAgent.FIOPatronymic_EditExit(Sender: TObject);
begin
 if not self.IsAssignedAgent then raise Exception.Create(ExMSG_NotAssignedROOT);
 self.UI_FIOPatronymic_WriteTo(self.FXmlAgent);
end;

procedure TFrameAgent.FIOSurname_EditExit(Sender: TObject);
begin
 if not self.IsAssignedAgent then raise Exception.Create(ExMSG_NotAssignedROOT);
 self.UI_FIOSurname_WriteTo(self.FXmlAgent);
end;

procedure TFrameAgent.InitializationUI;
begin
 if self.IsAssignedAgent then
  UI_ReadOf(FXmlAgent)
 else
 BEGIN
  mmoAppointMent.Clear;
  FIOSurname_Edit.Text := '';
  FIOFirst_Edit.Text := '';
  FIOPatronymic_Edit.Text  := '';
 END;
end;

function TFrameAgent.IsAssignedAgent: boolean;
begin
 Result:= self.FXmlAgent<>nil;
end;

procedure TFrameAgent.SetAgent;
begin
 if self.FXmlAgent=aAgent then EXIT;
 self.FXmlAgent:= aAgent;
 self.InitializationUI;
end;

procedure TFrameAgent.UI_Appointment_ReadOf(
  const aAgent: IXMLSTD_MP_Title_Client_Organization_Agent);
begin
 self.mmoAppointMent.Lines.Add(MsXml_ReadChildNodeValue(aAgent,cnstXmlAppointment));
end;

procedure TFrameAgent.UI_Appointment_WriteTo(
  const aAgent: IXMLSTD_MP_Title_Client_Organization_Agent);
var
 _st: string;
begin
 _st:= self.mmoAppointMent.Text;
 if _st<>'' then
    aAgent.Appointment:= _st
 else
  MsXml_RemoveChildNode(aAgent,cnstXmlAppointment);
end;

procedure TFrameAgent.UI_FIOFirst_ReadOf(
  const aAgent: IXMLSTD_MP_Title_Client_Organization_Agent);
begin
  self.FIOFirst_Edit.Text:= MsXml_ReadChildNodeValue(aAgent,rsXML_FIO_First);
end;

procedure TFrameAgent.UI_FIOFirst_WriteTo(
  const aAgent: IXMLSTD_MP_Title_Client_Organization_Agent);
var
 _st: string;
begin
 _st:= self.FIOFirst_Edit.Text;
 if _st<>'' then
    aAgent.First:= _st
 else
  MsXml_RemoveChildNode(aAgent,rsXML_FIO_First);
end;

procedure TFrameAgent.UI_FIOPatronymic_ReadOf(
  const aAgent: IXMLSTD_MP_Title_Client_Organization_Agent);
begin
 self.FIOPatronymic_Edit.Text:= MsXml_ReadChildNodeValue(aAgent,rsXML_FIO_Patronymic);
end;

procedure TFrameAgent.UI_FIOPatronymic_WriteTo(
  const aAgent: IXMLSTD_MP_Title_Client_Organization_Agent);
var
 _st: string;
begin
 _st:= self.FIOPatronymic_Edit.Text;
 if _st<>'' then
    aAgent.Patronymic:= _st
 else
  MsXml_RemoveChildNode(aAgent,rsXML_FIO_Patronymic);
end;

procedure TFrameAgent.UI_FIOSurname_ReadOf(
  const aAgent: IXMLSTD_MP_Title_Client_Organization_Agent);
begin
   self.FIOSurname_Edit.Text:= MsXml_ReadChildNodeValue(aAgent,rsXML_FIO_Surname);
end;

procedure TFrameAgent.UI_FIOSurname_WriteTo(
  const aAgent: IXMLSTD_MP_Title_Client_Organization_Agent);
var
 _st: string;
begin
 _st:= self.FIOSurname_Edit.Text;
 if _st<>'' then
    aAgent.Surname:= _st
 else
  MsXml_RemoveChildNode(aAgent,rsXML_FIO_Surname);
end;


procedure TFrameAgent.UI_ReadOf(
  const aAgent: IXMLSTD_MP_Title_Client_Organization_Agent);
begin
 UI_Appointment_ReadOf(aAgent);
 UI_FIOSurname_ReadOf(aAgent);
 UI_FIOFirst_ReadOf(aAgent);
 UI_FIOPatronymic_ReadOf(aAgent);
end;

procedure TFrameAgent.UI_WriteTo(
  const aAgent: IXMLSTD_MP_Title_Client_Organization_Agent);
begin
  UI_Appointment_WriteTo(aAgent);
  UI_FIOSurname_WriteTo(aAgent);
  UI_FIOFirst_WriteTo(aAgent);
  UI_FIOPatronymic_WriteTo(aAgent);
end;

end.
