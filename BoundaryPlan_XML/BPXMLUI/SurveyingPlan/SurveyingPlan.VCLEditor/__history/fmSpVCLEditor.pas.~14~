unit fmSpVCLEditor;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.Bind.EngExt, Vcl.Bind.DBEngExt,
  Data.Bind.GenData, Vcl.Bind.Grid, System.Rtti, System.Bindings.Outputs,
  Vcl.Bind.Editors, Data.Bind.Components, Data.Bind.Grid, Data.Bind.ObjectScope,
  Vcl.Grids, system.IOUtils, Vcl.ExtCtrls, Vcl.Buttons, Vcl.ButtonGroup,
  Vcl.CategoryButtons, Vcl.StdCtrls, Vcl.ComCtrls,   Vcl.Bind.Navigator, Data.Bind.Controls,
  Vcl.Menus, Vcl.ClipBrd,

  fmcontainer,
  SurveyingPlan.Xml.Types,
  SurveyingPlan.Xml.Utils,
  SurveyingPlan.Editor.Prototypes,
  SurveyingPlan.Editor.Utils, Vcl.ImgList, System.Actions, Vcl.ActnList;

type
  TFormEntitySpatial = class(TForm)
    StGrPoints: TStringGrid;
    BindingsList: TBindingsList;
    pbsBorder: TPrototypeBindSource;
    LinkGridPointsToBorder: TLinkGridToDataSource;
    BindNavigatorBorder: TBindNavigator;
    PaneBorderNavigator: TPanel;
    PanelBorder: TPanel;
    PanelMBorder: TPanel;
    pbsContour: TPrototypeBindSource;
    BindNavigatorMBorder: TBindNavigator;
    Label2: TLabel;
    ComboBox1: TComboBox;
    SpeedButtonExport: TSpeedButton;
    SpeedButtonImport: TSpeedButton;
    SpeedButtonValid: TSpeedButton;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    actlst: TActionList;
    actApplyChange: TAction;
    procedure pbPointCreateAdapter(Sender: TObject;  var ABindSourceAdapter: TBindSourceAdapter);
    procedure pbsContourCreateAdapter(Sender: TObject; var ABindSourceAdapter: TBindSourceAdapter);
    procedure lbBordersClick(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
    procedure lbMBorderClick(Sender: TObject);
    procedure BindNavigatorMBorderClick(Sender: TObject; Button: TNavigateButton);
    procedure SpeedButtonExportClick(Sender: TObject);
    procedure StGrPointsDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure SpeedButtonSaveChangeClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure SpeedButtonValidClick(Sender: TObject);
    procedure btn1Click(Sender: TObject);
    procedure sClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButtonImportClick(Sender: TObject);
    procedure actApplyChangeExecute(Sender: TObject);
  private
    { Private declarations }
   FData: TSpPrototypeContour;
   FOnNeedSave: TNotifyEvent;
   FContourAdapter: TListBindSourceAdapter<TSpPrototypeBorder>;
   FBorderAdapter: TListBindSourceAdapter<TSpPrototypePoint>;
     procedure fillListBorders;
   procedure notifyContourItemIndex;

   procedure BeginUpdateData;
   procedure EndUpdateData;

   procedure _Test;
   procedure Refresh;

  public
    { Public declarations }
    function isValidate(LogOut: TStringBuilder = nil): boolean;

    procedure getAsText(var OutputText: string);
    procedure setAsText(InputText: string);

    procedure ReadData(aSource: TSpContour);
    procedure WriteData(aDest: TSpContour);

    procedure ClearALL;
    property OnNeedSave: TNotifyEvent read FOnNeedSave write FOnNeedSave;
  end;

var
  FormEntitySpatial: TFormEntitySpatial;

implementation

{$R *.dfm}

uses SurveyingPlan.DataFormat;

procedure TFormEntitySpatial.actApplyChangeExecute(Sender: TObject);
begin
 if Assigned(self.OnNeedSave) then
  self.OnNeedSave(sender);
end;

procedure TFormEntitySpatial.BeginUpdateData;
begin
 self.FBorderAdapter.Active:= FALSE;
 self.FContourAdapter.Active:= FALSE;
 self.FBorderAdapter.SetList(NIL, FALSE);
 self.FContourAdapter.SetList(NIL, FALSE);
end;

procedure TFormEntitySpatial.BindNavigatorMBorderClick(Sender: TObject;
  Button: TNavigateButton);
begin
 case Button of
   nbFirst,nbPrior,nbNext,nbLast: self.notifyContourItemIndex;
   nbInsert,nbDelete: begin
            TRY
               self.fillListBorders;
               self.notifyContourItemIndex;
             EXCEPT  on e : Exception do 
               raise Exception.Create(e.Message);
//               ShowMessage(E.Message);
             END;
   end;
   nbEdit: ;
   nbPost: ;
   nbCancel: ;
   nbRefresh: ;
   nbApplyUpdates: ;
   nbCancelUpdates: ;
 end;
end;

procedure TFormEntitySpatial.sClick(Sender: TObject);
begin
 //self.SaveToXml(self.FXmlnode);
end;

procedure TFormEntitySpatial.setAsText(InputText: string);
var
 _contour: TSpContour;
 _log: TStringBuilder;

begin
 self.BeginUpdateData;
_log:= TStringBuilder.Create(''); //TODO:Larisa | ������� ����, ��� ��� ��������� ������ ��� ������ _log.Free (���� �� ���������� ����� �������)
 TRY
  _contour:= DataFormat.Contour.Text.Parse(InputText);
  _contour._checkDirection;
  if not TSpValidator.Validate(_contour,_log) then begin
   if MessageDlg('������������ ����������� ������:'+sLineBreak+_log.ToString,mtError,mbYesNo,-1)<>mrYes then  EXIT;
  end;
  self.FData.Clear;
  self.FData.ReadData(_contour);
  _contour.Free;
 FINALLY
   _log.Free; 
   self.EndUpdateData;  
 END;
end;

procedure TFormEntitySpatial.btn1Click(Sender: TObject);
begin
 self._Test;
 self.Refresh;
end;

procedure TFormEntitySpatial.ClearALL;
begin
 self.BeginUpdateData;
 self.FData.Clear;
 self.EndUpdateData;
end;

procedure TFormEntitySpatial.ComboBox1Change(Sender: TObject);
begin
 self.pbsContour.ItemIndex:= self.ComboBox1.ItemIndex;
 self.notifyContourItemIndex;
end;

procedure TFormEntitySpatial.EndUpdateData;
begin
 self.FContourAdapter.SetList(self.FData, FALSE);
 if self.FData.Count<>0 then begin
   self.FContourAdapter.ItemIndex:= 0;
   self.FContourAdapter.Active:= TRUE;
 end;
 self.fillListBorders;
 self.notifyContourItemIndex;
end;

procedure TFormEntitySpatial.FillListBorders;
var
 _stb: TStringBuilder;
 _i,_cn: integer;
begin
  _stb:= TStringBuilder.Create;
 if self.FContourAdapter.ItemCount<>0 then
  _stb.Append('������� �������');
 _cn:= self.FContourAdapter.ItemCount;
 for _i := 1 to _cn-1 do begin
  _stb.AppendLine;
  _stb.Append(format('%s - %d/%d',['��������� �������',_I,_cn-1]));
 end;
  self.ComboBox1.Items.Text:= _stb.ToString;
  self.ComboBox1.ItemIndex:= self.FContourAdapter.ItemIndex;
 _stb.Free;
end;



procedure TFormEntitySpatial.FormCreate(Sender: TObject);
begin
 self.FData:= TSpPrototypeContour.Create;
end;

procedure TFormEntitySpatial.FormDestroy(Sender: TObject);
begin
 self.FData.Free;
 self.FContourAdapter.Free;
 self.FBorderAdapter.Free;
end;

procedure TFormEntitySpatial.getAsText(var OutputText: string);
var
 _contour: TSpContour;
begin
 _contour:= TSpContour.Create;
 TRY
  self.FData.WriteData(_contour);
  OutputText:= DataFormat.Contour.Text.Build(_contour);
 FINALLY
  _contour.Free;
 END;
end;

procedure TFormEntitySpatial.lbBordersClick(Sender: TObject);
begin
 //self.BindingsList.Notify(sender,'ItemIndex');
end;

procedure TFormEntitySpatial.lbMBorderClick(Sender: TObject);
begin
{
 if self.lbMBorder.Parent=self.PanelMBorder then begin
  self.lbMBorder.Height:= self.lbMBorder.Height*5;
  self.lbMBorder.Parent:= self;
 end else begin
  self.lbMBorder.Parent:= self.PanelMBorder;
  self.lbMBorder.Height:=self.lbMBorder.Height div 5;
  self.SetBorder;
 end;
 }
end;

procedure TFormEntitySpatial.NotifyContourItemIndex;
begin
try
   if (self.FContourAdapter.ItemIndex>=0) and (self.FContourAdapter.ItemIndex<self.FContourAdapter.List.Count) then begin
    self.FBorderAdapter.Active:= false;
    self.FBorderAdapter.SetList(self.FContourAdapter.List[self.FContourAdapter.ItemIndex],FALSE);
    self.FBorderAdapter.Active:= true;
    self.ComboBox1.ItemIndex:= self.pbsContour.ItemIndex;
   end else begin
    self.FBorderAdapter.Active:= FALSE;
    self.FBorderAdapter.SetList(nil,FALSE);
    self.ComboBox1.ItemIndex:=-1;
   end;
except on E : Exception do
//TODO:ERROR   raise Exception.Create(e.Message) at ReturnAddress;
  //howMessage(E.Message);
end;
end;

procedure TFormEntitySpatial.pbPointCreateAdapter(Sender: TObject; var ABindSourceAdapter: TBindSourceAdapter);
begin
 self.FBorderAdapter:= TListBindSourceAdapter<TSpPrototypePoint>.Create(self,nil,FALSE);
 ABindSourceAdapter:= self.FBorderAdapter;
end;

procedure TFormEntitySpatial.pbsContourCreateAdapter(Sender: TObject;
  var ABindSourceAdapter: TBindSourceAdapter);
begin
 self.FContourAdapter:= TListBindSourceAdapter<TSpPrototypeBorder>.Create(self,nil,FALSE);
 ABindSourceAdapter:= self.FContourAdapter;
end;

procedure TFormEntitySpatial.ReadData(aSource: TSpContour);
begin
 self.BeginUpdateData;
  self.FData.Clear;
  self.FData.ReadData(aSource);
 self.EndUpdateData;
end;

procedure TFormEntitySpatial.Refresh;
begin
 self.BeginUpdateData;
 self.EndUpdateData;
end;

procedure TFormEntitySpatial.SpeedButton1Click(Sender: TObject);
begin
 if self.FBorderAdapter.Active then begin
   self.FBorderAdapter.List.Reverse;
   self.FBorderAdapter.ResetNeeded;
 end;

end;

procedure TFormEntitySpatial.SpeedButtonExportClick(Sender: TObject);
var
 _st: string;
begin
 self.getAsText(_st);
 Clipboard.Open;
 TRY
  Clipboard.AsText:= _st;
 FINALLY
  Clipboard.Close;
 END;
end;

procedure TFormEntitySpatial.SpeedButtonImportClick(Sender: TObject);
var
 _st: string;
begin
 Clipboard.Open;
 TRY
  _st:= Clipboard.AsText;
 FINALLY
  Clipboard.Close;
 END;
 
 self.setAsText(_st);
end;


procedure TFormEntitySpatial.SpeedButtonSaveChangeClick(Sender: TObject);
 var
  _log: TStringBuilder;
begin
 _log:= TStringBuilder.Create;
 if self.isValidate(_log) then
  ShowMessage('��')
 else
  ShowMessage(_log.ToString);
 _log.Free;
end;

procedure TFormEntitySpatial.SpeedButtonValidClick(Sender: TObject);
var
 _stb: TStringBuilder;
begin
 _stb:= TStringBuilder.Create;

 if self.isValidate(_stb) then
  ShowMessage('��')
 else 
  ShowMessage(_stb.ToString); 
 

 _stb.Free;
end;

procedure TFormEntitySpatial.StGrPointsDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
var
 _invalid:  TSpPrototypePoint.TFields;
 _curf: TSpPrototypePoint.TField;
 _InvalidDataCell: Boolean;
 _st:string;
begin

  StGrPoints.Canvas.Brush.Style:= bsSolid;
  StGrPoints.Canvas.FillRect(Rect);
 _st:= StGrPoints.Cells[ACol,ARow];
 StGrPoints.Canvas.TextOut(Rect.Left+(((Rect.Right-Rect.Left) - StGrPoints.Canvas.TextWidth(_st)) div 2),
                           Rect.Top+(((Rect.Bottom-Rect.Top) - StGrPoints.Canvas.TextHeight(_st)) div 2),
                           _st);

 if not self.FBorderAdapter.Active then EXIT;

 if ((ARow-StGrPoints.FixedRows)<0) or (self.FBorderAdapter.List.Count=0) then EXIT;


 _invalid:= self.FBorderAdapter.List.Items[ARow-StGrPoints.FixedRows].getInvalid;
 if _invalid=[] then EXIT;

 _InvalidDataCell:= false;
 case ACol of
   0: if TSpPrototypePoint.TField.fenPrefix in _invalid then _InvalidDataCell:= true;
   1: if TSpPrototypePoint.TField.fenNumber in _invalid then _InvalidDataCell:= true;
   2: if TSpPrototypePoint.TField.fenOldX in _invalid then _InvalidDataCell:= true;
   3: if TSpPrototypePoint.TField.fenOldY in _invalid then _InvalidDataCell:= true;
   4: if TSpPrototypePoint.TField.fenNewX in _invalid then _InvalidDataCell:= true;
   5: if TSpPrototypePoint.TField.fenNewY in _invalid then _InvalidDataCell:= true;
   6: if TSpPrototypePoint.TField.fenDelta in _invalid then _InvalidDataCell:= true;
   7: if TSpPrototypePoint.TField.fenZacrep in _invalid then _InvalidDataCell:= true;
 end;

 StGrPoints.Canvas.Brush.Color:= RGB(250,150,150);
 StGrPoints.Canvas.Brush.Style:= bsSolid;
 StGrPoints.Canvas.FillRect(Rect);

 if _InvalidDataCell then begin
  StGrPoints.Canvas.Pen.Color:= RGB(255,0,0);
  StGrPoints.Canvas.Pen.Width:= 3;
  StGrPoints.Canvas.Rectangle(Rect);
 end;
 _st:= StGrPoints.Cells[ACol,ARow];
 StGrPoints.Canvas.TextOut(Rect.Left+(((Rect.Right-Rect.Left) - StGrPoints.Canvas.TextWidth(_st)) div 2),
                           Rect.Top+(((Rect.Bottom-Rect.Top) - StGrPoints.Canvas.TextHeight(_st)) div 2),
                           _st);
 StGrPoints.DefaultColWidth := 84
 StGrPoints.ColWidths[3] := 84;
end;

procedure TFormEntitySpatial.WriteData(aDest: TSpContour);
begin
 self.FData.WriteData(aDest);

end;

function TFormEntitySpatial.isValidate(LogOut: TStringBuilder): boolean;
var
 _border: TSpPrototypeBorder;
 _point: TSpPrototypePoint;
 _test: TSpContour;
begin
 Result:= true;
 for _border in self.FContourAdapter.List do
  for _point in _border do
   if not _point.isValid then begin
    if LogOut<>nil then begin
      if LogOut.Length<>0 then LogOut.AppendLine;
      LogOut.Append(format('������ � ����� �� ��������: ������ %d, ����� "%s" (%d)',
                           [self.FContourAdapter.List.IndexOf(_border),
                            _point.Prefix+_point.Number,
                            _border.IndexOf(_point)+1]));
    end;
    Result:= false;
   end;

  if Result then begin
   _test:= TSpContour.Create(true);
   self.FData.WriteData(_test);
   Result:= TSpValidator.Validate(_test,LogOut);
   _test.Free;
  end;
end;

procedure TFormEntitySpatial._Test;
const
 cnPoint = 100;
 cnBorders = 10;
var
 _i,_j,_k: integer;
 _Mborder: TSpPrototypeContour;
 _border: TSpPrototypeBorder;
 _point: TSpPrototypePoint;
begin
 _Mborder:= TSpPrototypeContour.Create;
 for _i := 1 to random(cnBorders)+1 do begin
  _border:= TSpPrototypeBorder.Create;
  _Mborder.Add(_border);
  for _j := 1 to random(cnPoint)+1 do begin
   _point:= TSpPrototypePoint.Create;
   _point.Number:= IntToStr(_i);
   _point.OldX:= IntToStr(Random(100));
   _point.OldY:= IntToStr(Random(100));
   _point.NewX:= IntToStr(Random(100));
   _point.NewY:= IntToStr(Random(100));
   _point.Delta:= '0.5';
   _point.Zacrep:= '�� �������������';
   _border.Add(_point);
  end;
 end;
 self.FData:= _Mborder;
end;

end.
