unit fmComposition_EZ;

//-- ������ ������� ���������������� (��) -- Composition_EZ

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, STD_MP, ComCtrls, Grids, DBGrids,
  System.Actions,


  MSMCommon_,
  frArea, frEntitySpatial, Buttons, Menus,Clipbrd, ButtonGroup, ActnList,BpXML_different,
  Mask, ImgList, XMLIntf,
  RegularExpressions,unRussLang, fmContainer;


type

  TFrameCompostion_EZ = class(TFrame)
    pgcComposition_EZ: TPageControl;
    NewEntryParcel_TabSheet: TTabSheet;
    pnlNewEntryparcel: TPanel;
    lblName: TLabel;
    cbbName: TComboBox;
    NewEntryParcelList_ComboBox: TComboBox;
    tsDeleteParcels: TTabSheet;
    IsFill_DeleteEntryParcels_CheckBox: TCheckBox;
    DeleteEntryParcels_Panel: TPanel;
    btngpCadNumActions: TButtonGroup;
    btngpActionAddDel: TButtonGroup;
    ActionList: TActionList;
    Action_DoAdd: TAction;
    Action_DelSel: TAction;
    ExistEntryParcelList_ListBox: TListBox;
    DeleteEntryParcelsList_ListBox: TListBox;
    pgcEntitySpatial: TPageControl;
    tsArea: TTabSheet;
    tsEntitySpatial: TTabSheet;
    meDeleteparcel: TMaskEdit;
    meExistEntryParcel: TMaskEdit;
    ButtonGroup2: TButtonGroup;
    ExistEntryParcels_TabSheet: TTabSheet;
    IsFill_ExistEntryParcel_CheckBox: TCheckBox;
    ExistEntryParcel_Panel: TPanel;
    Action_DeleteEntryParcel_DoAdd: TAction;
    Action_DeleteEntryParcel_DoDelSel: TAction;
    Panel1: TPanel;
    actNewEntryparcel_Paste: TAction;
    actNewEntryparcel_Valid: TAction;
    Action1: TAction;
    lblDefinition: TLabel;

    // -- �������� ������� � ��������
    procedure BitBtn_AddDefinitionClick(Sender: TObject);
    procedure BitBtn_DelDefinitionClick(Sender: TObject);
     procedure Action_DoAddExecute(Sender: TObject);
    procedure Action_DelSelExecute(Sender: TObject);
    procedure cbbNameChange(Sender: TObject);


    procedure IsFill_ExistEntryParcel_CheckBoxClick(Sender: TObject);
    procedure IsFill_DeleteEntryParcels_CheckBoxClick(Sender: TObject);
    procedure Action_DeleteEntryParcel_DoAddExecute(Sender: TObject);
    procedure Action_DeleteEntryParcel_DoDelSelExecute(Sender: TObject);
    procedure NewEntryParcelList_ComboBoxChange(Sender: TObject);
    procedure actNewEntryparcel_PasteExecute(Sender: TObject);
    procedure actNewEntryparcel_ValidExecute(Sender: TObject);
    procedure Action1Execute(Sender: TObject);
    procedure ButtonGroup2Items2Click(Sender: TObject);

  private
    FComposition_EZ: IXMLTExistEZParcel_Composition_EZ; // -- ������ ������� ����������������
    FArea: TFrameArea; // -- ���������� ������� ��� �������� �  ����� ���c�����
    FEntitySpatial: TFrameEntity_Spatial; // �������� ���������� ������������

    //c����� ���������� �� �����
    function UpDateList_NewEntryParcel: integer;
    //c����� ���������� �� ������������
    function UpDateList_ExistEntryParcel: integer;
    //������ �����������
    function UpDateList_DeleteEntryParcel: integer;

    procedure SetSpecifyParcels(const SpPar: IXMLTExistEZParcel_Composition_EZ);
    procedure InitializationUI;

    { Private declarations }
  public
    //������� ������� ����� �������� �������
    procedure SetCurrentNewEntryParcel(ItemIndex : integer);

    //�������� ������ ������������ ��
    procedure ExistEntryParcel_Clear;
    //�������� ������ ����� ��
    procedure NewEntryParcel_Clear;
    //�������� ������ �����������
    procedure DeleteEntryParcel_Clear;


    constructor Create(Owner: TComponent); override;
    property  XMLCompositionEZ : IXMLTExistEZParcel_Composition_EZ  read FComposition_EZ write SetSpecifyParcels;

    { Public declarations }
  end;

implementation

uses MsXMLAPI, SurveyingPlan.Xml.Utils, SurveyingPlan.Xml.Types,
  SurveyingPlan.DataFormat, fmLog;
Const
 _NewEntryParceItem = 0;
 _ExistEntryParceItem = 1;

 cnstDelimiter: string = chr(9) + ' ';
 cnstNewEntryParcelName_Separate  = '03';
 cnstNewEntryParcelName_Conditional = '04';
 cnstNewEntryParcelNameItem_Separate  = 0;
 cnstNewEntryParcelNameItem_Conditional = 1;

 procedure _HiddenChangeCheckBoxSate(ChBox: TCheckBox; NewSate: TCheckBoxState);
 var
  _OnClick: TNotifyEvent;
 begin
  if ChBox.State=NewSate then EXIT;
  _OnClick:= ChBox.OnClick;
  ChBox.OnClick:= nil;
  ChBox.State:= NewSate;
  ChBox.OnClick:= _OnClick;
 end;

//-->> -------------------------------------------------------------------------
procedure TFrameCompostion_EZ.SetCurrentNewEntryParcel(ItemIndex: integer);
var
 _XMLNewEntryParcel: IXMLTNewEZEntryParcel;
begin
  if (ItemIndex>-1) and (ItemIndex<self.NewEntryParcelList_ComboBox.Items.Count) then
  begin

   _XMLNewEntryParcel:= IXMLTNewEZEntryParcel(pointer(self.NewEntryParcelList_ComboBox.Items.Objects[ItemIndex]));
   if _XMLNewEntryParcel.Name = cnstNewEntryParcelName_Separate then
    cbbName.ItemIndex := cnstNewEntryParcelNameItem_Separate
   else
    cbbName.ItemIndex :=cnstNewEntryParcelNameItem_Conditional;

   self.FArea.XmlAreaExistEZ := _XMLNewEntryParcel.Area;
   self.FEntitySpatial.EntitySpatial := _XMLNewEntryParcel.Entity_Spatial;
   self.NewEntryParcelList_ComboBox.ItemIndex:= ItemIndex;
   self.pgcEntitySpatial.Visible:= true;
   _XMLNewEntryParcel := nil;

  end else
  begin
   self.FArea.XmlArea := nil;
   self.FEntitySpatial.EntitySpatial := nil;
   self.NewEntryParcelList_ComboBox.ItemIndex:= -1;
   self.pgcEntitySpatial.Visible:= false;
  end;
end;

//-->> -------------------------------------------------------------------------
procedure TFrameCompostion_EZ.SetSpecifyParcels (const SpPar: IXMLTExistEZParcel_Composition_EZ);
begin
 if self.FComposition_EZ = SpPar then Exit;
 self.FComposition_EZ := SpPar;
 self.InitializationUI;
end;

constructor TFrameCompostion_EZ.Create(Owner: TComponent);
begin
 inherited;
 self.FComposition_EZ:= nil;
 self.FEntitySpatial := TFrameEntity_Spatial.Create(self.tsEntitySpatial);
 self.FEntitySpatial.Parent := self.tsEntitySpatial;
 self.FEntitySpatial.Align := alClient;

 self.FArea := TFrameArea.Create(self.tsArea);
 self.FArea.Parent := self.tsArea;
 self.FArea.Align := alClient;
end;

function TFrameCompostion_EZ.UpDateList_DeleteEntryParcel: integer;
var
 _i: integer;
 _node: IXMLNode;
 _XMLDeleteEntryParcel: IXMLTExistEZParcel_Composition_EZ_DeleteEntryParcels_DeleteEntryParcel;
begin
 self.DeleteEntryParcelsList_ListBox.Items.Clear;
 _node:= self.FComposition_EZ.ChildNodes.FindNode('DeleteEntryParcels');
 if Assigned(_node) then
  for _I := 0 to self.FComposition_EZ.DeleteEntryParcels.Count -1 do begin
   _XMLDeleteEntryParcel:= self.FComposition_EZ.DeleteEntryParcels[_i];
   self.DeleteEntryParcelsList_ListBox.Items.AddObject(_XMLDeleteEntryParcel.CadastralNumber,pointer(_XMLDeleteEntryParcel));
  end;
 Result:= self.DeleteEntryParcelsList_ListBox.Items.Count;
end;

function TFrameCompostion_EZ.UpDateList_ExistEntryParcel: integer;
var
 _i: integer;
 _node: IXMLNode;
 _XMLExistEntryParcel: IXMLTExistEZParcel_Composition_EZ_InsertEntryParcels_InsertEntryParcel_ExistEntryParcel;
begin
 self.ExistEntryParcelList_ListBox.Items.Clear;
 _node:= self.FComposition_EZ.ChildNodes.FindNode('InsertEntryParcels');
 if Assigned(_node) then
  for _I := 0 to self.FComposition_EZ.InsertEntryParcels.Count -1 do begin
   _node:= self.FComposition_EZ.InsertEntryParcels[_i].ChildNodes.FindNode('ExistEntryParcel');
   if Assigned(_node) then begin
     _XMLExistEntryParcel:= self.FComposition_EZ.InsertEntryParcels[_i].ExistEntryParcel;
     self.ExistEntryParcelList_ListBox.Items.AddObject(_XMLExistEntryParcel.CadastralNumber,pointer(_XMLExistEntryParcel));
   end;
  end;
 Result:= self.ExistEntryParcelList_ListBox.Items.Count;
end;

function TFrameCompostion_EZ.UpDateList_NewEntryParcel: integer;
var
 _i,_index: integer;
 _node: IXMLNode;
 _XMLNewEntryParcel: IXMLTNewEZEntryParcel;
begin
 _index:= self.NewEntryParcelList_ComboBox.ItemIndex;
 self.NewEntryParcelList_ComboBox.Items.Clear;
 _node:= self.FComposition_EZ.ChildNodes.FindNode('InsertEntryParcels');
 if Assigned(_node) then
  for _I := 0 to self.FComposition_EZ.InsertEntryParcels.Count -1 do begin
   _node:= self.FComposition_EZ.InsertEntryParcels[_i].ChildNodes.FindNode('NewEntryParcel');
   if Assigned(_node) then begin
     _XMLNewEntryParcel:= self.FComposition_EZ.InsertEntryParcels[_i].NewEntryParcel;
     self.NewEntryParcelList_ComboBox.Items.AddObject(_XMLNewEntryParcel.Definition,pointer(_XMLNewEntryParcel));
   end;
  end;
 Result:= self.NewEntryParcelList_ComboBox.Items.Count;
 if _index<0 then _index:= 0;
 self.SetCurrentNewEntryParcel(_index);
end;

// -->>-- ���������� ��������� �������� ���������� �� �� �������� �������-------
procedure TFrameCompostion_EZ.InitializationUI;
var
 i : integer;
 _node: ixmlnode;
 _tempBool: Boolean;
begin
 //������� ������� ������
 self.DeleteEntryParcelsList_ListBox.Clear;
 self.ExistEntryParcelList_ListBox.Clear;
 self.NewEntryParcelList_ComboBox.Items.Clear;

 //��������� ������ (������ ������ ��������)
 if self.FComposition_EZ <> nil then begin

  //���������� �����
  self.UpDateList_NewEntryParcel;

  //���������� ������������
  if self.UpDateList_ExistEntryParcel>0 then
   _HiddenChangeCheckBoxSate(self.IsFill_ExistEntryParcel_CheckBox,cbChecked)
  else
   _HiddenChangeCheckBoxSate(self.IsFill_ExistEntryParcel_CheckBox,cbUnchecked);

  //����������� ������������
  if self.UpDateList_DeleteEntryParcel>0 then
   _HiddenChangeCheckBoxSate(self.IsFill_DeleteEntryParcels_CheckBox,cbChecked)
  else
   _HiddenChangeCheckBoxSate(self.IsFill_DeleteEntryParcels_CheckBox,cbUnchecked)

 end else
 begin
  self.NewEntryParcel_TabSheet.Visible:= false;
  self.ExistEntryParcels_TabSheet.Visible:= false;
  self.tsDeleteParcels.Visible:= false;

  self.FEntitySpatial.EntitySpatial:= nil;
  self.FArea.XmlArea:= nil;
 end;

 self.pgcEntitySpatial.Visible:= self.NewEntryParcelList_ComboBox.Items.Count>0;
 self.ExistEntryParcel_Panel.Visible:= self.IsFill_ExistEntryParcel_CheckBox.Checked;
 self.DeleteEntryParcels_Panel.Visible:= self.IsFill_DeleteEntryParcels_CheckBox.Checked;

end;

procedure TFrameCompostion_EZ.ExistEntryParcel_Clear;
var
 _i: integer;
 _node: IXMLNode;
 _XMLExistEntryParcel: IXMLTExistEZParcel_Composition_EZ_InsertEntryParcels_InsertEntryParcel_ExistEntryParcel;
begin
 self.ExistEntryParcelList_ListBox.Items.Clear;
 _node:= self.FComposition_EZ.ChildNodes.FindNode('InsertEntryParcels');
 if Assigned(_node) then
  for _I := self.FComposition_EZ.InsertEntryParcels.Count -1 downto 0 do begin
   _node:= self.FComposition_EZ.InsertEntryParcels[_i].ChildNodes.FindNode('ExistEntryParcel');
   if Assigned(_node) then begin
    _node:= self.FComposition_EZ.InsertEntryParcels[_i];
    self.FComposition_EZ.InsertEntryParcels.Remove(_node);
    _node:= nil;
   end;
  end;
end;

procedure TFrameCompostion_EZ.DeleteEntryParcel_Clear;
var
 _i: integer;
 _node: IXMLNode;
 _XMLDeleteEntryParcel: IXMLTExistEZParcel_Composition_EZ_DeleteEntryParcels_DeleteEntryParcel;
begin
 self.DeleteEntryParcelsList_ListBox.Items.Clear;
 MsXml_RemoveChildNode(self.FComposition_EZ.DeleteEntryParcels,'DeleteEntryParcel',true);
end;


procedure TFrameCompostion_EZ.actNewEntryparcel_PasteExecute(Sender: TObject);
var
 _st: string;
 _mcontour: TSpMContour;
 _log: TStringBuilder;
 _dlog: TformLog;
begin
 Clipboard.Open;
 TRY
  _st:= Clipboard.AsText;
 FINALLY
  Clipboard.Close;
 END;
 _mcontour:= DataFormat.MContour.Text.Parse(_st);
 _mcontour._checkDirection;
 _log:= TStringBuilder.Create;
 if not TSpValidator.Validate(_mcontour,_log) then begin
  _dlog:= TformLog.Create(nil);
  _dlog.TextLog:= _log.ToString;
  FreeAndNil(_log);
  _dlog.ShowModal;
  if MessageDlg('������������ ��������� ������?',mtConfirmation,mbYesNo,-1) = mrNo then begin
   _dlog.Free;
   EXIT;
  end;
  _dlog.Free;
 end;
 _log.Free;

 { TODO : ������� �� ������ ��������� �.�. ����������� ���� ���� ����
 ����������� ������� }
 if self.FComposition_EZ.InsertEntryParcels.Count<>0 then
  case MessageDlg('�������� ��������� ������ ����� ��������?',mtConfirmation,[mbYes,mbNo,mbCancel],-1) of
   mrYes: self.FComposition_EZ.InsertEntryParcels.Clear;
   mrNo: ;
   mrCancel: EXIT;
  end;

 TSpConvert.MContour.Convert(_mcontour,self.FComposition_EZ.InsertEntryParcels);
 self.UpDateList_NewEntryParcel;
end;

procedure TFrameCompostion_EZ.actNewEntryparcel_ValidExecute(Sender: TObject);
var
 _st: string;
 _mcontour: TSpMContour;
 _log: TStringBuilder;
 _dlog: TformLog;
begin
 _mcontour:= TSpMContour.Create;
 TSpConvert.MContour.Convert(self.FComposition_EZ.InsertEntryParcels,_mcontour);
 _log:= TStringBuilder.Create;
 _dlog:= TformLog.Create(nil);
 if not TSpValidator.Validate(_mcontour,_log) then
  _dlog.TextLog:= _log.ToString
 else
  _dlog.TextLog:= '������ �� ����������';
 _dlog.ShowModal;
 _log.Free;
 _dlog.Free;

 self.UpDateList_NewEntryParcel;
end;

procedure TFrameCompostion_EZ.Action1Execute(Sender: TObject);
var
 _mcontor: TSpMContour;
begin
 _mcontor:= TSpMContour.Create;
 TSpConvert.MContour.Convert(self.FComposition_EZ.InsertEntryParcels,_mcontor);
 _mcontor._checkDirection;
 TSpConvert.MContour.Convert(_mcontor,self.FComposition_EZ.InsertEntryParcels);
 _mcontor.Free;
 self.UpDateList_NewEntryParcel;
 ShowMessage('�������� ��������� �������.');
end;

procedure TFrameCompostion_EZ.Action_DeleteEntryParcel_DoAddExecute(Sender: TObject);
var
 _CadNum: string;
 _XMLDeleteParcel: IXMLTExistEZParcel_Composition_EZ_DeleteEntryParcels_DeleteEntryParcel;
 listCadNum : TStringList;
 i : Integer;
begin
 listCadNum := TStringList.Create;
 listCadNum := GetClipBoardList(regCadastralNumber);
 for I := 0  to listCadNum.Count-1 do
 begin
     _CadNum := listCadNum.Strings[I];
     _XMLDeleteParcel:= self.FComposition_EZ.DeleteEntryParcels.Add;
     _XMLDeleteParcel.CadastralNumber:=  _CadNum;
     self.DeleteEntryParcelsList_ListBox.AddItem(_CadNum,pointer(_XMLDeleteParcel));
 end;
 listCadNum.Destroy;
end;

procedure TFrameCompostion_EZ.Action_DeleteEntryParcel_DoDelSelExecute(Sender: TObject);
var
 _i: integer;
 _XMLDeleteEntryParcel: IXMLTExistEZParcel_Composition_EZ_DeleteEntryParcels_DeleteEntryParcel;
begin
 if MessageDlg(rsDeleteCurrentCadNum, mtConfirmation, [mbYes, mbNo], -1) = mrNo then Exit;
 for _i := self.DeleteEntryParcelsList_ListBox.Items.Count-1 downto 0 do
  if Self.DeleteEntryParcelsList_ListBox.Selected[_i] then begin
   _XMLDeleteEntryParcel:= IXMLTExistEZParcel_Composition_EZ_DeleteEntryParcels_DeleteEntryParcel(pointer(Self.DeleteEntryParcelsList_ListBox.Items.Objects[_i]));
   self.FComposition_EZ.DeleteEntryParcels.Remove(_XMLDeleteEntryParcel);
   Self.DeleteEntryParcelsList_ListBox.Items.Delete(_i);
  end;
end;

procedure TFrameCompostion_EZ.Action_DelSelExecute(Sender: TObject);
var
 _i,_j: integer;
 _XMLExistEntryParcel: IXMLTExistEZParcel_Composition_EZ_InsertEntryParcels_InsertEntryParcel_ExistEntryParcel;
begin
 if MessageDlg(rsDeleteCurrentCadNum, mtConfirmation, [mbYes, mbNo], -1) = mrNo then Exit;
 
 for _i := self.ExistEntryParcelList_ListBox.Items.Count-1 downto 0 do
  if Self.ExistEntryParcelList_ListBox.Selected[_i] then
  begin
   _XMLExistEntryParcel:= IXMLTExistEZParcel_Composition_EZ_InsertEntryParcels_InsertEntryParcel_ExistEntryParcel(pointer(Self.ExistEntryParcelList_ListBox.Items.Objects[_i]));
   for _j := 0 to self.FComposition_EZ.InsertEntryParcels.Count-1 do
    if self.FComposition_EZ.InsertEntryParcels[_j].ChildNodes.IndexOf(_XMLExistEntryParcel) >-1 then 
    begin
       self.FComposition_EZ.InsertEntryParcels.Delete(_j);
       self.ExistEntryParcelList_ListBox.Items.Delete(_i);
       BREAK;
    end;
  end;
end;

procedure TFrameCompostion_EZ.Action_DoAddExecute(Sender: TObject);
var
 _CadNum: string;
 _XMLExistEntryParcel: IXMLTExistEZParcel_Composition_EZ_InsertEntryParcels_InsertEntryParcel_ExistEntryParcel;
 listCadNum : TStringList;
 i : Integer;
begin
 listCadNum := TStringList.Create;
 listCadNum  :=  GetClipBoardList(regCadastralNumber);
 for i := 0 to listCadNum.Count-1 do
 begin
     _CadNum := listCadNum.Strings[i];
     _XMLExistEntryParcel:= self.FComposition_EZ.InsertEntryParcels.Add.ExistEntryParcel;
     _XMLExistEntryParcel.CadastralNumber :=  _CadNum;
     self.ExistEntryParcelList_ListBox.AddItem(_CadNum,pointer(_XMLExistEntryParcel));
 end;
 listCadNum.Destroy;
end;


// -->> ------------------------------------------------------------------------
procedure TFrameCompostion_EZ.BitBtn_AddDefinitionClick(Sender: TObject);
var
 strDefinition : string;
 _XMLNewEntryParcel: IXMLTNewEZEntryParcel;
begin
  if FComposition_EZ <> nil then begin
   strDefinition := Trim(InputBox('������� ��������','����������� ���������� �������',''));
   if strDefinition <> '' then begin
    _XMLNewEntryParcel := self.FComposition_EZ.InsertEntryParcels.Add.NewEntryParcel;
    _XMLNewEntryParcel.Definition := strDefinition;
    _XMLNewEntryParcel.Name := cnstNewEntryParcelName_Separate;
    self.FArea.XmlAreaExistEZ := _XMLNewEntryParcel.Area;
    self.FEntitySpatial.EntitySpatial := _XMLNewEntryParcel.Entity_Spatial;

    self.NewEntryParcelList_ComboBox.Items.AddObject(strDefinition,pointer(_XMLNewEntryParcel));
    SetCurrentNewEntryParcel(NewEntryParcelList_ComboBox.Items.Count-1);
   end;
  end;
end;

// -->> ------------------------------------------------------------------------
procedure TFrameCompostion_EZ.BitBtn_DelDefinitionClick(Sender: TObject);
var
 _i,_index: integer;
 _NodeDel: IXMLNode;
begin
 _index:= self.NewEntryParcelList_ComboBox.ItemIndex;
 if _index =-1 then EXIT;
 if MessageDlg('�������  �������� � �����  ��?', mtInformation,mbYesNo,-1) = mrYes then begin
  _NodeDel:= IXMLTNewEZEntryParcel(pointer(self.NewEntryParcelList_ComboBox.Items.Objects[_index]));
 for _i := 0 to self.FComposition_EZ.InsertEntryParcels.Count-1 do
  if self.FComposition_EZ.InsertEntryParcels[_i].ChildNodes.IndexOf(_NodeDel) >-1 then
  begin
    self.FComposition_EZ.InsertEntryParcels.Delete(_i);
    self.NewEntryParcelList_ComboBox.Items.Delete(_index);
    Break;
  end;
  self.SetCurrentNewEntryParcel(_index-1);
 end;
end;

procedure TFrameCompostion_EZ.ButtonGroup2Items2Click(Sender: TObject);
var
 _i,_index: integer;
 _NodeDel: IXMLNode;
 k : string;
begin
 _index:= self.NewEntryParcelList_ComboBox.ItemIndex;
 if _index =-1 then EXIT;
 if MessageDlg('�������� ����������� �������� � �����  ��?', mtInformation,mbYesNo,-1) = mrYes then
 begin
    _NodeDel:= IXMLTNewEZEntryParcel(pointer(self.NewEntryParcelList_ComboBox.Items.Objects[_index]));
     for _i := 0 to self.FComposition_EZ.InsertEntryParcels.Count-1 do
      if self.FComposition_EZ.InsertEntryParcels[_i].ChildNodes.IndexOf(_NodeDel) >-1 then
      begin
        k :=InputBox('����������� ��', '�������  ����������� ��', '');
        self.FComposition_EZ.InsertEntryParcels[_i].NewEntryParcel.Definition:= k;
        self.NewEntryParcelList_ComboBox.Items[_index] := k;
        Break;
      end;
      self.SetCurrentNewEntryParcel(_index);
 end;
end;

// -->> ------------------------------------------------------------------------
procedure TFrameCompostion_EZ.cbbNameChange(Sender: TObject);
var
 _XMLNewEntryParcel: IXMLTNewEZEntryParcel;
begin
 TRY
  _XMLNewEntryParcel:= IXMLTNewEZEntryParcel(
                pointer(self.NewEntryParcelList_ComboBox.Items.Objects[
                                 self.NewEntryParcelList_ComboBox.ItemIndex]));
 EXCEPT
  raise Exception.Create(ExMSG_NotAssignedROOT);
 END;

 case cbbName.ItemIndex of
  cnstNewEntryParcelNameItem_Separate: _XMLNewEntryParcel.Name := cnstNewEntryParcelName_Separate;
  cnstNewEntryParcelNameItem_Conditional: _XMLNewEntryParcel.Name := cnstNewEntryParcelName_Conditional;
 end;

end;

// -->> ------------------------------------------------------------------------
procedure TFrameCompostion_EZ.IsFill_DeleteEntryParcels_CheckBoxClick(
  Sender: TObject);
begin
 if not self.IsFill_DeleteEntryParcels_CheckBox.Checked then
 begin
   if MessageDlg('������� ������ ?',mtInformation,mbYesNo,-1) = mrYes then
   begin
      self.DeleteEntryParcels_Panel.Visible:= FALSE;
      MsXml_RemoveChildNode(self.FComposition_EZ,'DeleteEntryParcels');
   end else Self.IsFill_DeleteEntryParcels_CheckBox.Checked := True;
 end else
     begin
        self.DeleteEntryParcels_Panel.Visible:= TRUE;
        self.UpDateList_DeleteEntryParcel;
     end;
end;

procedure TFrameCompostion_EZ.IsFill_ExistEntryParcel_CheckBoxClick(Sender: TObject);
begin
 if not self.IsFill_ExistEntryParcel_CheckBox.Checked then
 begin
    if MessageDlg('������� ������: ?',mtInformation,mbYesNo,-1) = mrYes then
    begin
        self.ExistEntryParcel_Panel.Visible:= FALSE;
        self.ExistEntryParcel_Clear;
    end else self.IsFill_ExistEntryParcel_CheckBox.Checked := true;
 end else
     begin
        self.ExistEntryParcel_Panel.Visible:= TRUE;
        self.UpDateList_ExistEntryParcel;
     end;
end;



procedure TFrameCompostion_EZ.NewEntryParcelList_ComboBoxChange(
  Sender: TObject);
begin
self.SetCurrentNewEntryParcel(self.NewEntryParcelList_ComboBox.ItemIndex);
end;

procedure TFrameCompostion_EZ.NewEntryParcel_Clear;
var
 _i: integer;
 _node: IXMLNode;
 _XMLExistEntryParcel: IXMLTExistEZParcel_Composition_EZ_InsertEntryParcels_InsertEntryParcel_ExistEntryParcel;
begin
 self.NewEntryParcelList_ComboBox.Items.Clear;
 _node:= self.FComposition_EZ.ChildNodes.FindNode('InsertEntryParcels');
 if Assigned(_node) then
  for _I := self.FComposition_EZ.InsertEntryParcels.Count -1 downto 0 do begin
   _node:= self.FComposition_EZ.InsertEntryParcels[_i].ChildNodes.FindNode('NewEntryParcel');
   if Assigned(_node) then begin
    _node:= self.FComposition_EZ.InsertEntryParcels[_i];
    self.FComposition_EZ.InsertEntryParcels.Remove(_node);
    _node:= nil;
   end;
  end;
end;



{$R *.dfm}

end.
