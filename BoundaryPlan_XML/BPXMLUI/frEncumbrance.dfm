object FrameEncumbrance: TFrameEncumbrance
  Left = 0
  Top = 0
  Width = 834
  Height = 464
  Align = alClient
  Color = clBtnFace
  ParentBackground = False
  ParentColor = False
  TabOrder = 0
  ExplicitWidth = 451
  ExplicitHeight = 304
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 834
    Height = 49
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 451
    object pnl1: TPanel
      Left = 0
      Top = 0
      Width = 834
      Height = 41
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 451
      object Label1: TLabel
        Left = 2
        Top = 3
        Width = 102
        Height = 13
        Caption = #1042#1080#1076' '#1086#1073#1088#1077#1084#1077#1085#1077#1085#1080#1103
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label2: TLabel
        Left = 441
        Top = 3
        Width = 133
        Height = 13
        Hint = 
          #1057#1086#1076#1077#1088#1078#1072#1085#1080#1077' '#1086#1073#1088#1077#1084#1077#1085#1077#1085#1080#1103'  '#1057#1086#1076#1077#1088#1078#1072#1085#1080#1077' '#1086#1073#1088#1077#1084#1077#1085#1077#1085#1080#1103'/'#1057#1074#1077#1076#1077#1085#1080#1103' '#1086#1073' '#1054#1053', '#1088 +
          #1072#1089#1087#1086#1083#1086#1078#1077#1085#1085#1086#1084' '#1085#1072' '#1047#1059
        Caption = #1057#1086#1076#1077#1088#1078#1072#1085#1080#1077' '#1086#1073#1088#1077#1084#1077#1085#1077#1085#1080#1103
        ParentShowHint = False
        ShowHint = True
      end
      object Name_Edit: TEdit
        Left = 439
        Top = 18
        Width = 442
        Height = 23
        BevelKind = bkFlat
        BorderStyle = bsNone
        Color = clWhite
        TabOrder = 0
        OnExit = Action_Name_WriteExecute
        OnKeyUp = Name_EditKeyUp
      end
      object Type_ComboBox: TComboBox
        Left = 2
        Top = 18
        Width = 425
        Height = 22
        BevelInner = bvLowered
        BevelKind = bkFlat
        BevelOuter = bvRaised
        Style = csOwnerDrawFixed
        Color = 15921123
        DropDownCount = 15
        TabOrder = 1
        TextHint = #1042#1099#1073#1080#1088#1080#1090#1077' '#1086#1076#1080#1085' '#1080#1079' '#1074#1072#1088#1080#1072#1085#1090#1086#1074
        StyleElements = []
        OnChange = Action_Type_WriteExecute
      end
    end
  end
  object pnlDocuments: TJvGroupBox
    Left = 0
    Top = 49
    Width = 834
    Height = 415
    Align = alClient
    Caption = #1044#1086#1082#1091#1084#1077#1085#1090#1099', '#1085#1072' '#1086#1089#1085#1086#1074#1072#1085#1080#1080' '#1082#1086#1090#1086#1088#1099#1093' '#1074#1086#1079#1085#1080#1082#1072#1077#1090' '#1086#1073#1088#1077#1084#1077#1085#1077#1085#1080#1077
    Color = clBtnFace
    ParentBackground = False
    ParentColor = False
    TabOrder = 1
    Checkable = True
    PropagateEnable = True
    ExplicitWidth = 451
    ExplicitHeight = 255
  end
  object ActionList: TActionList
    Left = 280
    Top = 56
    object Action_Type_Write: TAction
      Caption = 'Action_Type_Write'
      OnExecute = Action_Type_WriteExecute
    end
    object Action_Name_Write: TAction
      Caption = 'Action_Name_Write'
      OnExecute = Action_Name_WriteExecute
    end
  end
end
