unit UnShowText;

{����������� ����������� �������������� ������}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, StdCtrls, ActnList, ButtonGroup, ExtCtrls;

type
  TFrShowText = class(TForm)
    mmoText: TMemo;
    pnl_ShowText: TPanel;
    btnOKBtn: TButton;
    btn1: TButton;
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormShow(Sender: TObject);
  strict private
    { Private declarations }
    class var instShowText : TFrShowText;
  protected    
    function  GetText() : string;
    procedure SetText(aText : string);
  public
    { Public declarations }
    function UpdateText(aText : string) : string;
    class function InstanShowText : TFrShowText; static;
  end;


implementation

{$R *.dfm}

{ TFrShowText }

procedure TFrShowText.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
 if CanClose then Exit; 
end;

procedure TFrShowText.FormShow(Sender: TObject);
begin
  instShowText.mmoText.SetFocus;
end;

function TFrShowText.GetText: string;
begin
  Result := instShowText.mmoText.Text;
end;

class function TFrShowText.InstanShowText: TFrShowText;
begin
  if not Assigned(instShowText) then
     Application.CreateForm(TFrShowText,instShowText);
  Result := instShowText    
end;

procedure TFrShowText.SetText(aText: string);
begin
  instShowText.mmoText.Clear;
  instShowText.mmoText.Text := aText;
end;

function TFrShowText.UpdateText(aText: string): string;
begin
  if not Assigned(instShowText) then Exit;
  SetText(aText);
  instShowText.ShowModal;
  if instShowText.ModalResult = mrOk then Result := GetText  else Result := aText;   
end;

end.
