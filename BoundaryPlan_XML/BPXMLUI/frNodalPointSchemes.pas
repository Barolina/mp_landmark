unit frNodalPointSchemes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ActnList, ButtonGroup, ExtCtrls,

  STD_MP, ImgList, frAttributesSTD_MP,
  System.Actions,fmContainer;

type
  TFrameNodalPointSchemes = class(TFrame)
    PanelNodalPointSchemes_List: TPanel;
    NodalPointSchemes_List_ButtonGroup: TButtonGroup;
    ActionList: TActionList;
    NodalPointSchemes_List_ListBox: TListBox;
    Action_Contrours_AddNew: TAction;
    Action_NodalPointSchemes_DeleteCurrent: TAction;
    Action_NodalPointSchemes_SetCurrent: TAction;

    procedure Action_Contrours_AddNewExecute(Sender: TObject);
    procedure Action_NodalPointSchemes_DeleteCurrentExecute(Sender: TObject);
    procedure Action_NodalPointSchemes_SetCurrentExecute(Sender: TObject);
  private
   { Private declarations }
   FNodalPointScheme_frame : TFrameAttributedSTD_MP;
   FXmlNodalPointSchemes   : IXMLSTD_MP_NodalPointSchemes;

   procedure NodalPointSchemes_List_DoSetCurrentItem(IndexItem: integer); //������� ������� ������� ������
   procedure NodalPointSchemes_List_DoRefresh(A: Integer = -1; B: Integer = -1);
   procedure NodalPointSchemes_List_DoUpDate;
   procedure NodalPointSchemes_List_DoAddNewItem;
   procedure NodalPointSchemes_List_DoDeleteItem(IndexItem: Integer);

   procedure OnChange_Attribute_DefinitionOrNumberPP(sender: Tobject);
  protected
   procedure InitializationUI;
   procedure SetNodalPointSchemes(const aNodalPointSchemes: IXMLSTD_MP_NodalPointSchemes);
   procedure NodalPointSchemes_List_SetCurrent(index: integer);
  public
   { Public declarations }
   constructor Create(Owner: TComponent); override;

   function IsAssignedNodalPointSchemes: Boolean;
   property NodalPointScheme: TFrameAttributedSTD_MP read FNodalPointScheme_frame;
   property XmlNodalPointSchemes: IXMLSTD_MP_NodalPointSchemes read FXmlNodalPointSchemes write SetNodalPointSchemes;
  end;

implementation

uses MsXMLAPI;

{$R *.dfm}

{ TFrameNodalPointSchemes }

procedure TFrameNodalPointSchemes.Action_NodalPointSchemes_DeleteCurrentExecute(Sender: TObject);
begin
 if not IsAssignedNodalPointSchemes then EXIT;
 if self.NodalPointSchemes_List_ListBox.ItemIndex=-1 then EXIT;
 self.NodalPointSchemes_List_DoDeleteItem(self.NodalPointSchemes_List_ListBox.ItemIndex);
 self.NodalPointSchemes_List_SetCurrent(self.NodalPointSchemes_List_ListBox.ItemIndex);
 if not self.FNodalPointScheme_frame.IsAssignedNodeCollection then
  self.NodalPointSchemes_List_SetCurrent(0);
end;

procedure TFrameNodalPointSchemes.Action_NodalPointSchemes_SetCurrentExecute(Sender: TObject);
begin
 self.NodalPointSchemes_List_SetCurrent(self.NodalPointSchemes_List_ListBox.ItemIndex);
end;

procedure TFrameNodalPointSchemes.Action_Contrours_AddNewExecute(Sender: TObject);
begin
 if not self.IsAssignedNodalPointSchemes then EXIT;
 self.NodalPointSchemes_List_DoAddNewItem;
 self.NodalPointSchemes_List_SetCurrent(self.NodalPointSchemes_List_ListBox.Count-1);
end;

procedure TFrameNodalPointSchemes.NodalPointSchemes_List_DoRefresh(A, B: Integer);
var
 _Contour: IXMLSTD_MP_NodalPointSchemes_NodalPointScheme;
 _i,_j: Integer;
 _ItemName: string;
begin
 if self.NodalPointSchemes_List_ListBox.Items.Count = 0 then EXIT;

 if (A<0) or (a>=self.NodalPointSchemes_List_ListBox.Items.Count)  then
  a:= 0;

 if (B<A) or (B>=self.NodalPointSchemes_List_ListBox.Items.Count)  then
  B:= self.NodalPointSchemes_List_ListBox.Items.Count-1;

 _j:= self.NodalPointSchemes_List_ListBox.ItemIndex;
 for _i := A to B do begin
  _Contour:= IXMLSTD_MP_NodalPointSchemes_NodalPointScheme(pointer(self.NodalPointSchemes_List_ListBox.Items.Objects[_i]));
  _ItemName:= MsXml_ReadAttribute(_Contour,'Definition');
  self.NodalPointSchemes_List_ListBox.Items.Strings[_i]:= _ItemName;
 end;
 self.NodalPointSchemes_List_ListBox.ItemIndex:= _j;
end;

procedure TFrameNodalPointSchemes.NodalPointSchemes_List_DoSetCurrentItem(IndexItem: integer);
var
 _contour: IXMLSTD_MP_NodalPointSchemes_NodalPointScheme;
begin
 _contour:= IXMLSTD_MP_NodalPointSchemes_NodalPointScheme(pointer(self.NodalPointSchemes_List_ListBox.Items.Objects[IndexItem]));
 self.FNodalPointScheme_frame.XMlNodalPointSchemes:= _contour;
end;

procedure TFrameNodalPointSchemes.NodalPointSchemes_List_SetCurrent(index: integer);
begin
 if (index>-1) and (index<self.NodalPointSchemes_List_ListBox.Count) then begin
  self.NodalPointSchemes_List_DoSetCurrentItem(index);
  self.NodalPointSchemes_List_ListBox.ItemIndex:= index;
  self.FNodalPointScheme_frame.Visible:= True;
 end else begin
  self.FNodalPointScheme_frame.Visible:= false;
  self.NodalPointSchemes_List_ListBox.ItemIndex:= -1;
  self.FNodalPointScheme_frame.XMlNodalPointSchemes:= nil;
 end;
end;

constructor TFrameNodalPointSchemes.Create(Owner: TComponent);
begin
 inherited;
 self.FNodalPointScheme_frame:= TFrameAttributedSTD_MP.Create(self);
 self.FNodalPointScheme_frame.Parent:= self;
 self.FNodalPointScheme_frame.Align:= alClient;
 self.FNodalPointScheme_frame.Visible:= False;
end;

procedure TFrameNodalPointSchemes.InitializationUI;
begin
 self.NodalPointSchemes_List_ListBox.Clear;
 if self.IsAssignedNodalPointSchemes then begin
  self.NodalPointSchemes_List_DoUpDate;
  self.NodalPointSchemes_List_SetCurrent(0);
 end
 else  FNodalPointScheme_frame.XMlNodalPointSchemes := nil;
end;

function TFrameNodalPointSchemes.IsAssignedNodalPointSchemes: Boolean;
begin
 Result:= self.FXmlNodalPointSchemes<>nil;
end;

procedure TFrameNodalPointSchemes.OnChange_Attribute_DefinitionOrNumberPP(sender: tobject);
begin
 self.NodalPointSchemes_List_DoRefresh(self.NodalPointSchemes_List_ListBox.ItemIndex,self.NodalPointSchemes_List_ListBox.ItemIndex);
end;

procedure TFrameNodalPointSchemes.SetNodalPointSchemes(const aNodalPointSchemes: IXMLSTD_MP_NodalPointSchemes);
begin
 if self.FXmlNodalPointSchemes=aNodalPointSchemes then EXIT;
 self.NodalPointSchemes_List_SetCurrent(-1);
 self.FXmlNodalPointSchemes:= aNodalPointSchemes;
 self.InitializationUI;
end;

procedure TFrameNodalPointSchemes.NodalPointSchemes_List_DoAddNewItem;
var
 _contour: IXMLSTD_MP_NodalPointSchemes_NodalPointScheme;
 _ItemName: string;
begin
 _contour:= self.FXmlNodalPointSchemes.Add;
 _ItemName:= IntToStr(self.FXmlNodalPointSchemes.Count);
 _contour.Definition:= _ItemName;
 self.NodalPointSchemes_List_ListBox.AddItem(_ItemName,pointer(_contour));
end;


procedure TFrameNodalPointSchemes.NodalPointSchemes_List_DoDeleteItem(IndexItem: Integer);
var
 _contour: IXMLSTD_MP_NodalPointSchemes_NodalPointScheme;
 _index: Integer;
begin
 _contour:= IXMLSTD_MP_NodalPointSchemes_NodalPointScheme(pointer(self.NodalPointSchemes_List_ListBox.Items.Objects[IndexItem]));
 if self.FNodalPointScheme_frame.XMlNodalPointSchemes=_contour then
  self.NodalPointSchemes_List_SetCurrent(IndexItem-1);
 self.NodalPointSchemes_List_ListBox.Items.Delete(IndexItem);
 self.FXmlNodalPointSchemes.Remove(_contour);
 _contour:= nil;
end;

procedure TFrameNodalPointSchemes.NodalPointSchemes_List_DoUpDate;
var
 _i, _index: Integer;
 _Contour: IXMLSTD_MP_NodalPointSchemes_NodalPointScheme;
 _ItemName: string;
begin
 _index:= self.NodalPointSchemes_List_ListBox.ItemIndex;
 self.NodalPointSchemes_List_ListBox.Clear;
 for _i := 0 to self.FXmlNodalPointSchemes.Count-1 do begin
  _Contour:= self.FXmlNodalPointSchemes.NodalPointScheme[_i];
  _ItemName:= MsXml_ReadAttribute(_Contour,'Definition');
  self.NodalPointSchemes_List_ListBox.AddItem(_ItemName,pointer(_Contour));
 end;
 self.NodalPointSchemes_List_SetCurrent(_index);
 if not self.FNodalPointScheme_frame.IsAssignedNodeCollection then
  self.NodalPointSchemes_List_SetCurrent(0);
end;

end.
