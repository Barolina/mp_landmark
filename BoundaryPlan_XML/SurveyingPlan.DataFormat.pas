unit SurveyingPlan.DataFormat;

interface
uses
 System.classes,
 System.Sysutils,
 System.RegularExpressions,
 SurveyingPlan.Xml.Types,
 STD_MP,
 Dialogs;

 type

 DataFormat = record
  type Point = record
   type Text = record
    type EError = class(Exception);
    class function Parse(aText: string):TSpBorderPoint; static;
    class function Build(aSource: TSpBorderPoint):string; static;
   end;
  end;

  Border = record
   type Text = record
    class function Parse(aText: string):TSpBorder; static;
    class function Build(aSource: TSpBorder):string; static;
   end;
  end;

  Contour = record
   type Text = record
    class function Parse(aText: string):TSpContour; static;
    class function Build(aSource: TSpContour):string; static;
   end;
  end;

  MContour = record
   type Text = record
    class function Parse(aText: string): TSpMContour; static;
    class function Build(aSource: TSpMContour):string; static;
   end;
  end;
  SubParcel = record
   type Text = record
    class function Parse(aText: string):TSpSubParcel; static;
    class function Build(aSource: TSpSubParcel):string; static;
   end;
  end;

  MSubParcel = record
   type Text = record
    class function Parse(aText: string): TSpMSubParcel; static;
    class function Build(aSource: TSpMSubParcel):string; static;
   end;
  end;

  MCSubParcel = record
   type Text  = record
     class function Parse(aText : string): TSpMCSubParcel; static;
     class function Build(aSourse : TSpMCSubParcel): string; static;
    end;
   end;
   
 end;

implementation

uses SurveyingPlan.Common;
 resourcestring
  rsDefDelimiter = ';';
  rsEmptyValue = '-';

  {$region '������ - ����� ������� �������'}
  rsPref = 'pref';
  rsNump = 'nump';
  rsOldx = 'oldx';
  rsOldy = 'oldy';
  rsNewx = 'newx';
  rsNewy = 'newy';
  rsDx = 'dx';
  rsZakrep = 'zakrep';
//  rsRegExpPointFormat = '(?<pref>�)?[ ,;\t]?(?<nump>(-+|[0-9]*))[ ,;\t]'+
//                  '(?<oldx>(-+|[0-9.]*))[ ,;\t](?<oldy>(-+|[0-9.]*))[ ,;\t]'+
//                  '(?<newx>(-+|[0-9.]*))[ ,;\t](?<newy>(-+|[0-9.]*))[ ,;\t]'+
//                  '(?<dx>(-+|[0-9.]*))[ ,;\t](?<zakrep>(-+|\w*))';
   rsRegExpPointFormat =  '(?<pref>�)?[ ,;\t]?(?<nump>(-+|[0-9]*))[ ,;\t]((?<oldx>(-+|(-{0,1})[0-9.]*))[ ,;\t](?<oldy>(-+|(-{0,1})[0-9.]*))[ ,;\t](?<newx>(-+|(-{0,1})[0-9.]*))[ ,;\t](?<newy>(-+|(-{0,1})[0-9.]*)))[ ,;\t](?<dx>(-+|[0-9.]*))[ ,;\t](?<zakrep>(-+|\w*))';
  {$endregion}

  rsRegExpWithOutDelimiters = '[^ ,;\t]';

  rsFE3EE45A = '�� ���������� ����� ������. ������ ������ : '+#10#13+
               '<������| ><�����><X(���)| ><Y(���)| ><X(����)| ><Y(����)| ><������.><�����������| >';

{ DataFormat.Point.Text }

class function DataFormat.Point.Text.Build(aSource: TSpBorderPoint): string;
var
 _stb: TStringBuilder;
begin
 _stb:= TStringBuilder.Create;
 case aSource.Kind of
  psbExisted: begin
   _stb.Append(aSource.NewPoint.Prefix).Append(rsDefDelimiter);
   if not aSource.NewPoint.Number.Empty then
     _stb.Append(aSource.NewPoint.Number.Value);
   _stb.Append(rsDefDelimiter);
   _stb.Append(aSource.OldPoint.X).Append(rsDefDelimiter);
   _stb.Append(aSource.OldPoint.Y).Append(rsDefDelimiter);
   _stb.Append(aSource.NewPoint.X).Append(rsDefDelimiter);
   _stb.Append(aSource.NewPoint.Y).Append(rsDefDelimiter);
   if not aSource.NewPoint.Delta.Empty then
     _stb.Append(aSource.NewPoint.Delta.Value);
   _stb.Append(rsDefDelimiter);
   _stb.Append(aSource.NewPoint.Zacrep).Append(rsDefDelimiter);
  end;
  psbCreated:begin
   _stb.Append(aSource.NewPoint.Prefix).Append(rsDefDelimiter);
   if not aSource.NewPoint.Number.Empty then
     _stb.Append(aSource.NewPoint.Number.Value);
   _stb.Append(rsDefDelimiter);
   _stb.Append(rsDefDelimiter);//oldx
   _stb.Append(rsDefDelimiter);//oldy
   _stb.Append(aSource.NewPoint.X).Append(rsDefDelimiter);
   _stb.Append(aSource.NewPoint.Y).Append(rsDefDelimiter);
   if not aSource.NewPoint.Delta.Empty then
     _stb.Append(aSource.NewPoint.Delta.Value);
   _stb.Append(rsDefDelimiter);
   _stb.Append(aSource.NewPoint.Zacrep).Append(rsDefDelimiter);
  end;
  psbRemoved:begin
   _stb.Append(rsDefDelimiter);//prefix
   if not aSource.OldPoint.Number.Empty then
    _stb.Append(aSource.OldPoint.Number.Value);
   _stb.Append(rsDefDelimiter);
   _stb.Append(aSource.OldPoint.X).Append(rsDefDelimiter);
   _stb.Append(aSource.OldPoint.Y).Append(rsDefDelimiter);
   _stb.Append(rsDefDelimiter);//newx
   _stb.Append(rsDefDelimiter);//newy
   _stb.Append(rsDefDelimiter);//delta
   _stb.Append(rsDefDelimiter);//zacrep
  end;
 end;
 Result:= _stb.ToString;
 _stb.Free;
end;

class function DataFormat.Point.Text.Parse(aText: string): TSpBorderPoint;
type
  TField = (fenPrefix,fenNumber,fenOldX,fenOldY,fenNewX,fenNewY,fenDelta,fenZacrep);
  TFields = set of TField;
  function _CheckEmpty(aData: string): string;
  begin
   if aData<>rsEmptyValue then Result:= aData else Result:='';
  end;
var
 _Matchs: TMatchCollection;
 _Match: TMatch;
 _emptyOld,_emptyNew: boolean;
 _float: Double;
 _kind: TSpBorderPoint.TKind;
 _Data: array[fenPrefix..fenZacrep] of string;

begin
 _Matchs:= TRegEx.Matches(aText,rsRegExpPointFormat);
 if (_Matchs.Count=0) then
  raise EError.CreateRes(@rsFE3EE45A);

 TRY
  _Match:= _Matchs.Item[0];
  _Data[fenPrefix]:= _CheckEmpty(_Match.Groups.Item['pref'].Value);
  _Data[fenNumber]:= _CheckEmpty(_Match.Groups.Item['nump'].Value);
  _Data[fenOldX]:= _CheckEmpty(_Match.Groups.Item['oldx'].Value);
  _Data[fenOldY]:= _CheckEmpty(_Match.Groups.Item['oldy'].Value);
  _Data[fenNewX]:= _CheckEmpty(_Match.Groups.Item['newx'].Value);
  _Data[fenNewY]:= _CheckEmpty(_Match.Groups.Item['newy'].Value);
  _Data[fenDelta]:= _CheckEmpty(_Match.Groups.Item['dx'].Value);
  _Data[fenZacrep]:= _CheckEmpty(_Match.Groups.Item['zakrep'].Value);
 EXCEPT
   raise EError.CreateRes(@rsFE3EE45A);
 END;

 _emptyOld:= (_Data[fenOldX]='') and (_Data[fenOldY]='');
 _emptyNew:= (_Data[fenNewX]='') and (_Data[fenNewY]='');

 if (not _emptyOld) and _emptyNew then //��������� �����
   _kind:= psbRemoved
 else
  if (not _emptyNew) and _emptyOld then //��������� �����
    _kind:= psbCreated
  else
   if not( _emptyNew and _emptyOld) then  //������������
    _kind:= psbExisted
    else
     raise EError.CreateRes(@rsFE3EE45A);


 //��������� �����
 if _kind = psbRemoved then begin
  if _Data[fenPrefix]<>'' then raise EError.CreateRes(@rsFE3EE45A);
  if not TSpPoint.TNumber.isAcceptable(_Data[fenNumber]) then raise EError.CreateRes(@rsFE3EE45A);
  if not TryStrToFloat(_Data[fenOldX],_float) then raise EError.CreateRes(@rsFE3EE45A);
  if not TryStrToFloat(_Data[fenOldY],_float) then raise EError.CreateRes(@rsFE3EE45A);

  if _Data[fenDelta]<>'' then raise EError.CreateRes(@rsFE3EE45A);
  if _Data[fenZacrep]<>'' then raise EError.CreateRes(@rsFE3EE45A);
 end;

 //����� �����
 if _kind = psbCreated then begin
  if {(_Data[fenPrefix]='') or }(not TSpPoint.TPrefix.isAcceptable(_Data[fenPrefix]))
      then raise EError.CreateRes(@rsFE3EE45A);
  if not TSpPoint.TNumber.isAcceptable(_Data[fenNumber]) then raise EError.CreateRes(@rsFE3EE45A);
  if not TryStrToFloat(_Data[fenNewX],_float) then raise EError.CreateRes(@rsFE3EE45A);
  if not TryStrToFloat(_Data[fenNewY],_float) then raise EError.CreateRes(@rsFE3EE45A);
  if not TSpPoint.TDelta.isAcceptable(_Data[fenDelta]) then raise EError.CreateRes(@rsFE3EE45A);
 end;

 //����������(������������) �����
 if  _kind = psbExisted then begin
 // if _Data[fenPrefix]<>'' then raise EError.CreateRes(@rsFE3EE45A); //TODO:Larisa (AccessVeoletion)
  if not TSpPoint.TNumber.isAcceptable(_Data[fenNumber]) then raise EError.CreateRes(@rsFE3EE45A);        
  if not TryStrToFloat(_Data[fenOldX],_float) then raise EError.CreateRes(@rsFE3EE45A);
  if not TryStrToFloat(_Data[fenOldY],_float) then raise EError.CreateRes(@rsFE3EE45A);
  if not TryStrToFloat(_Data[fenNewX],_float) then raise EError.CreateRes(@rsFE3EE45A);
  if not TryStrToFloat(_Data[fenNewY],_float) then raise EError.CreateRes(@rsFE3EE45A);
  if not TSpPoint.TDelta.isAcceptable(_Data[fenDelta]) then raise EError.CreateRes(@rsFE3EE45A);
 end;

 Result:= TSpBorderPoint.Create;
 Result.Kind:= _kind;
 case _Kind of
  psbExisted: begin
   Result.NewPoint.Prefix:= _Data[fenPrefix];
   Result.NewPoint.Number:= _Data[fenNumber];
   Result.OldPoint.Number:= _Data[fenNumber];
   Result.OldPoint.X:=  StrToFloat(_Data[fenOldX]);
   Result.OldPoint.Y:=  StrToFloat(_Data[fenOldY]);
   Result.NewPoint.X:=  StrToFloat(_Data[fenNewX]);
   Result.NewPoint.Y:=  StrToFloat(_data[fenNewY]);
   Result.NewPoint.Delta:= _Data[fenDelta];
   Result.NewPoint.Zacrep:= _Data[fenZacrep];
  end;
  psbCreated:begin
   Result.NewPoint.Prefix:= _Data[fenPrefix];
   Result.NewPoint.Number:= _Data[fenNumber];
   Result.NewPoint.X:=  StrToFloat(_Data[fenNewX]);
   Result.NewPoint.Y:=  StrToFloat(_data[fenNewY]);
   Result.NewPoint.Delta:= _Data[fenDelta];
   Result.NewPoint.Zacrep:= _Data[fenZacrep];
  end;
  psbRemoved:begin
   Result.OldPoint.Number:= _Data[fenNumber];
   Result.OldPoint.X:=  StrToFloat(_Data[fenOldX]);
   Result.OldPoint.Y:=  StrToFloat(_Data[fenOldY]);
  end;
 end;


end;

{ DataFormat.Border.Text }

class function DataFormat.Border.Text.Build(aSource: TSpBorder): string;
var
 _point: TSpBorderPoint;
 _stb: TStringBuilder;
begin
 _stb:= TStringBuilder.Create;
 for _point in aSource do begin
  _stb.Append(DataFormat.Point.Text.Build(_point));
  if _point <> aSource.Last then
   _stb.AppendLine;

 end;
 Result:= _stb.ToString;
 _stb.Free;
end;

class function DataFormat.Border.Text.Parse(aText: string): TSpBorder;
var
 _StR: TStringReader;
begin
 _StR:= TStringReader.Create(aText);
 Result:= TSpBorder.Create;
while _StR.Peek<>0 do
 Result.Add(DataFormat.Point.Text.Parse(_StR.ReadLine));
 _StR.Free;
end;

{ DataFormat.Contour.Text }

class function DataFormat.Contour.Text.Build(aSource: TSpContour): string;
var
 _border: TSpBorder;
 _stb: TStringBuilder;
begin
  _stb:= TStringBuilder.Create;
 for _border in aSource do begin
   _stb.Append(DataFormat.Border.Text.Build(_border));
  if _border<>aSource.Last then
   _stb.AppendLine.AppendLine;
 end;
 Result:= _stb.ToString;
 _stb.Free;
end;

class function DataFormat.Contour.Text.Parse(aText: string): TSpContour;
var
 _Border: TSpBorder;
 _StR: TStringReader;
 _line: string;
begin
 Result:= TSpContour.Create;
 _StR:= TStringReader.Create(aText);
 _border:= nil;
 while _StR.Peek<>-1 do begin
  _line:= _StR.ReadLine;
  if TRegEx.IsMatch(_line,rsRegExpWithOutDelimiters) then begin
   if not Assigned(_Border) then begin
    _border:= TSpBorder.Create;
    Result.Add(_border);
   end;
   _border.Add(DataFormat.Point.Text.Parse(_line));
  end else
   _Border:= nil;
 end;
 _StR.Free;
end;

{ DataFormat.MContour.Text }

class function DataFormat.MContour.Text.Build(aSource: TSpMContour): string;
var
 _stb: TStringBuilder;
 _contour: TSpContour;
begin
 _stb:= TStringBuilder.Create;
 for _contour in aSource do begin
  //_stb.Append('(').Append(_contour.Caption).Append(')').AppendLine;
  _stb.Append(_contour.Caption).AppendLine;
  _stb.Append(DataFormat.Contour.Text.Build(_contour));
  if _contour<>aSource.Last then
   _stb.AppendLine;
 end;
 Result:= _stb.ToString;
 _stb.Free;
end;

class function DataFormat.MContour.Text.Parse(aText: string): TSpMContour;
var
 _str: TStringReader;
 _stb: TStringBuilder;
 _line,_definition: string;
 _Match: TMatch;
 _def: string;
begin
 Result:= TSpMContour.Create;
 _stb:= TStringBuilder.Create;
 _str:= TStringReader.Create(aText);
 while _str.Peek<>-1 do begin
  _line:= _str.ReadLine;
  _Match:= TRegEx.Match(_line,cnRegExCadastralNumber);
  if _Match.Success then begin
   if _stb.Length<>0 then begin
    Result.Add(DataFormat.Contour.Text.Parse(_stb.ToString));
    Result.Last.Caption:= _def;
    _stb.Clear;
   end;
   _def:= _line;{ TODO -��������� : ������������ ������������ ������������ ������������ ������ }
  end else
   _stb.Append(_line).AppendLine;
 end;
  if _stb.Length<>0 then begin
   Result.Add(DataFormat.Contour.Text.Parse(_stb.ToString));
   Result.Last.Caption:= _def;
   _stb.Clear;
  end;

 _str.Free;
 _stb.Free;
end;

{ DataFormat.SubParcel.Text }

class function DataFormat.SubParcel.Text.Build(aSource: TSpSubParcel): string;
var
 _border: TSpBorder;
 _stb: TStringBuilder;
begin
  _stb:= TStringBuilder.Create;
 for _border in aSource do begin
   _stb.Append(DataFormat.Border.Text.Build(_border));
  if _border<>aSource.Last then
   _stb.AppendLine.AppendLine;
 end;
 Result:= _stb.ToString;
 _stb.Free;
end;

class function DataFormat.SubParcel.Text.Parse(aText: string): TSpSubParcel;
var
 _Border: TSpBorder;
 _StR: TStringReader;
 _line: string;
begin
 Result:= TSpSubParcel.Create;
 _StR:= TStringReader.Create(aText);
 _border:= nil;
 while _StR.Peek<>-1 do begin
  _line:= _StR.ReadLine;
  if TRegEx.IsMatch(_line,rsRegExpWithOutDelimiters) then begin
   if not Assigned(_Border) then begin
    _border:= TSpBorder.Create;
    Result.Add(_border);
   end;
   _border.Add(DataFormat.Point.Text.Parse(_line));
  end else
   _Border:= nil;
 end;
 _StR.Free;

end;

{ DataFormat.MSubParcel.Text }

class function DataFormat.MSubParcel.Text.Build(aSource: TSpMSubParcel): string;
var
 _stb: TStringBuilder;
 _contour: TSpSubParcel;
begin
 _stb:= TStringBuilder.Create;
 for _contour in aSource do begin
  //_stb.Append('(').Append(_contour.Caption).Append(')').AppendLine;
  _stb.Append(_contour.Caption).AppendLine;
  _stb.Append(DataFormat.SubParcel.Text.Build(_contour));
  if _contour<>aSource.Last then
   _stb.AppendLine;
 end;
 Result:= _stb.ToString;
 _stb.Free;

end;


class function DataFormat.MSubParcel.Text.Parse(aText: string): TSpMSubParcel;
var
 _str: TStringReader;
 _stb: TStringBuilder;
 _line,_definition: string;
 _Match: TMatch;
 _def: string;
begin
 Result:= TSpMSubParcel.Create;
 _stb:= TStringBuilder.Create;
 _str:= TStringReader.Create(aText);
 while _str.Peek<>-1 do
 begin
  _line:= _str.ReadLine;
  _Match:= TRegEx.Match(_line,cnRegExCadastralNumber);
  if _Match.Success then
  begin
   if _stb.Length<>0 then
   begin
    Result.Add(DataFormat.SubParcel.Text.Parse(_stb.ToString));
    Result.Last.Caption:= _def;
    _stb.Clear;
   end;
   _def:= _line;{ TODO -��������� : ������������ ������������ ������������ ������������ ������ }
  end else
   _stb.Append(_line).AppendLine;
 end;
 if _stb.Length<>0 then
 begin
   Result.Add(DataFormat.SubParcel.Text.Parse(_stb.ToString));
   Result.Last.Caption:= _def;
   _stb.Clear;
 end;

 _str.Free;
 _stb.Free;
end;

{ DataFormat.MCSubParcel.Text }

class function DataFormat.MCSubParcel.Text.Build(aSourse: TSpMCSubParcel): string;
begin
 {TODO: ?????}
end;

class function DataFormat.MCSubParcel.Text.Parse(aText: string): TSpMCSubParcel;
var
 _str: TStringReader;
 _stb: TStringBuilder;
 _line,_definition: string;
 _Match: TMatch;
 _def: string;
begin
 Result:= TSpMCSubParcel.Create;
 _stb:= TStringBuilder.Create;
 _str:= TStringReader.Create(aText);
 while _str.Peek<>-1 do 
 begin
  _line:= _str.ReadLine;
  _Match:= TRegEx.Match(_line,cnRegExCadastralNumber);
  if _Match.Success then 
  begin
   if _stb.Length<>0 then 
   begin
    Result.Add(DataFormat.MSubParcel.Text.Parse(_stb.ToString));
//    Result.Last.Caption:= _def;
    _stb.Clear;
   end;
   _def:= _line;{ TODO -��������� : ������������ ������������ ������������ ������������ ������ }
  end else
   _stb.Append(_line).AppendLine;
 end;
 if _stb.Length<>0 then 
 begin
   Result.Add(DataFormat.MSubParcel.Text.Parse(_stb.ToString));
  // Result.Last.Caption:= _def;
   _stb.Clear;
 end;

 _str.Free;
 _stb.Free;

end;

end.
