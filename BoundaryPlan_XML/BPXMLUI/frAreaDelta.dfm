object FrameAreaDelta: TFrameAreaDelta
  Left = 0
  Top = 0
  Width = 700
  Height = 131
  Color = clWhite
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  ParentBackground = False
  ParentColor = False
  ParentFont = False
  TabOrder = 0
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 700
    Height = 131
    Align = alClient
    Color = clBtnFace
    ParentBackground = False
    ParentColor = False
    TabOrder = 0
    object Label1: TLabel
      Left = 5
      Top = 3
      Width = 79
      Height = 13
      Hint = 
        #1059#1082#1072#1079#1099#1074#1072#1102#1090#1089#1103' '#1089#1074#1077#1076#1077#1085#1080#1103' '#1086' '#1089#1086#1076#1077#1088#1078#1072#1085#1080#1080' '#1086#1075#1088#1072#1085#1080#1095#1077#1085#1080#1103' ('#1086#1073#1088#1077#1084#1077#1085#1077#1085#1080#1103') '#1087#1088#1072#1074 +
        #1072', '#1077#1089#1083#1080' '#1090#1072#1082#1086#1077' '#1086#1075#1088#1072#1085#1080#1095#1077#1085#1080#1077' ('#1086#1073#1088#1077#1084#1077#1085#1077#1085#1080#1077') '#1087#1088#1072#1074#1072' '#1091#1089#1090#1072#1085#1086#1074#1083#1077#1085#1086' '#1080#1083#1080' '#1091#1089 +
        #1090#1072#1085#1072#1074#1083#1080#1074#1072#1077#1090#1089#1103' '#1074' '#1086#1090#1085#1086#1096#1077#1085#1080#1080' '#1074#1089#1077#1075#1086' '#1079#1077#1084#1077#1083#1100#1085#1086#1075#1086' '#1091#1095#1072#1089#1090#1082#1072' ('#1074' '#1090#1086#1084' '#1095#1080#1089#1083#1077' ' +
        #1074' '#1089#1074#1103#1079#1080' '#1089' '#1086#1073#1077#1089#1087#1077#1095#1077#1085#1080#1077#1084' '#1076#1086#1089#1090#1091#1087#1072' '#1082' '#1079#1077#1084#1077#1083#1100#1085#1099#1084' '#1091#1095#1072#1089#1090#1082#1072#1084' '#1080#1083#1080' '#1079#1077#1084#1083#1103#1084' '#1086 +
        #1073#1097#1077#1075#1086' '#1087#1086#1083#1100#1079#1086#1074#1072#1085#1080#1103') '
      Caption = #1048#1085#1099#1077' '#1089#1074#1077#1076#1077#1085#1080#1103
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
    end
    object Max_Area_Lab_Cap: TLabel
      Left = 168
      Top = 47
      Width = 204
      Height = 13
      Hint = 
        #1059#1082#1072#1079#1099#1074#1072#1077#1090#1089#1103' '#1074' '#1082#1074#1072#1076#1088#1072#1090#1085#1099#1093' '#1084#1077#1090#1088#1072#1093' '#1089' '#1086#1082#1088#1091#1075#1083#1077#1085#1080#1077#1084' '#1076#1086' 1 '#1082#1074#1072#1076#1088#1072#1090#1085#1086#1075#1086' '#1084 +
        #1077#1090#1088#1072' '
      Caption = #1055#1088#1077#1076#1077#1083#1100#1085#1099#1081' '#1084#1072#1082#1089#1080#1084#1072#1083#1100#1085#1099#1081' '#1088#1072#1079#1084#1077#1088' '#1047#1059
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
    end
    object Min_Area_Lab_Cap: TLabel
      Left = 3
      Top = 47
      Width = 149
      Height = 13
      Hint = 
        #1059#1082#1072#1079#1099#1074#1072#1077#1090#1089#1103' '#1074' '#1082#1074#1072#1076#1088#1072#1090#1085#1099#1093' '#1084#1077#1090#1088#1072#1093' '#1089' '#1086#1082#1088#1091#1075#1083#1077#1085#1080#1077#1084' '#1076#1086' 1 '#1082#1074#1072#1076#1088#1072#1090#1085#1086#1075#1086' '#1084 +
        #1077#1090#1088#1072' '
      Caption = #1055#1088#1077#1076#1077#1083#1100#1085#1099#1081' '#1084#1080#1085'. '#1088#1072#1079#1084#1077#1088' '#1047#1059
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
    end
    object Area_In_GKN_Label_Caption: TLabel
      Left = 230
      Top = 3
      Width = 159
      Height = 13
      Hint = 
        #1055#1083#1086#1097#1072#1076#1100' '#1079#1077#1084#1077#1083#1100#1085#1086#1075#1086' '#1091#1095#1072#1089#1090#1082#1072' '#1087#1086' '#1089#1074#1077#1076#1077#1085#1080#1103#1084' '#1075#1086#1089#1091#1076#1072#1088#1089#1090#1074#1077#1085#1085#1086#1075#1086' '#1082#1072#1076#1072#1089#1090#1088 +
        #1072' '#1085#1077#1076#1074#1080#1078#1080#1084#1086#1089#1090#1080' '
      Caption = #1055#1083#1086#1097#1072#1076#1100' '#1079#1091' '#1087#1086' '#1089#1074#1077#1076#1077#1085#1080#1103#1084' '#1043#1050#1053
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
    end
    object Delta_Area_Label_Caption: TLabel
      Left = 3
      Top = 90
      Width = 162
      Height = 13
      Hint = 
        #1059#1082#1072#1079#1099#1074#1072#1077#1090#1089#1103' '#1074' '#1082#1074#1072#1076#1088#1072#1090#1085#1099#1093' '#1084#1077#1090#1088#1072#1093' '#1089' '#1086#1082#1088#1091#1075#1083#1077#1085#1080#1077#1084' '#1076#1086' 1 '#1082#1074#1072#1076#1088#1072#1090#1085#1086#1075#1086' '#1084 +
        #1077#1090#1088#1072' '
      Caption = #1054#1094#1077#1085#1082#1072' '#1088#1072#1089#1093#1086#1078#1076#1077#1085#1080#1103' '#1087#1083#1086#1097#1072#1076#1077#1081
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
    end
    object Max_Area_MaskEdit: TMaskEdit
      Left = 168
      Top = 63
      Width = 215
      Height = 21
      Hint = 
        #1059#1082#1072#1079#1099#1074#1072#1077#1090#1089#1103' '#1074' '#1082#1074#1072#1076#1088#1072#1090#1085#1099#1093' '#1084#1077#1090#1088#1072#1093' '#1089' '#1086#1082#1088#1091#1075#1083#1077#1085#1080#1077#1084' '#1076#1086' 1 '#1082#1074#1072#1076#1088#1072#1090#1085#1086#1075#1086' '#1084 +
        #1077#1090#1088#1072' '
      BevelKind = bkFlat
      BorderStyle = bsNone
      Color = clWhite
      ParentShowHint = False
      ShowHint = False
      TabOrder = 1
      Text = ''
      StyleElements = []
      OnExit = Action_Max_Area_WriteExecute
      OnKeyUp = Max_Area_MaskEditKeyUp
    end
    object Min_Area_MaskEdit: TMaskEdit
      Left = 2
      Top = 63
      Width = 160
      Height = 21
      BevelKind = bkFlat
      BorderStyle = bsNone
      Color = clWhite
      ParentShowHint = False
      ShowHint = False
      TabOrder = 0
      Text = ''
      StyleElements = []
      OnExit = Action_Min_Area_WriteExecute
      OnKeyUp = Min_Area_MaskEditKeyUp
    end
    object Note_Ed: TEdit
      Left = 3
      Top = 20
      Width = 215
      Height = 21
      Hint = 
        #1059#1082#1072#1079#1099#1074#1072#1077#1090#1089#1103' '#1074' '#1082#1074#1072#1076#1088#1072#1090#1085#1099#1093' '#1084#1077#1090#1088#1072#1093' '#1089' '#1086#1082#1088#1091#1075#1083#1077#1085#1080#1077#1084' '#1076#1086' 1 '#1082#1074#1072#1076#1088#1072#1090#1085#1086#1075#1086' '#1084 +
        #1077#1090#1088#1072' '
      BevelKind = bkFlat
      BorderStyle = bsNone
      Color = clWhite
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      StyleElements = []
      OnExit = Action_NoteExecute
      OnKeyUp = Note_EdKeyUp
    end
    object Area_In_GKN_MaskEdit: TMaskEdit
      Left = 229
      Top = 20
      Width = 154
      Height = 21
      Hint = 
        #1059#1082#1072#1079#1099#1074#1072#1077#1090#1089#1103' '#1074' '#1082#1074#1072#1076#1088#1072#1090#1085#1099#1093' '#1084#1077#1090#1088#1072#1093' '#1089' '#1086#1082#1088#1091#1075#1083#1077#1085#1080#1077#1084' '#1076#1086' 1 '#1082#1074#1072#1076#1088#1072#1090#1085#1086#1075#1086' '#1084 +
        #1077#1090#1088#1072' '
      BevelKind = bkFlat
      BorderStyle = bsNone
      Color = clWhite
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      Text = ''
      StyleElements = []
      OnExit = Action_Area_In_GKNExecute
      OnKeyUp = Delta_Area_MaskEditKeyUp
    end
    object Delta_Area_MaskEdit: TMaskEdit
      Left = 3
      Top = 107
      Width = 380
      Height = 21
      Hint = 
        #1059#1082#1072#1079#1099#1074#1072#1077#1090#1089#1103' '#1074' '#1082#1074#1072#1076#1088#1072#1090#1085#1099#1093' '#1084#1077#1090#1088#1072#1093' '#1089' '#1086#1082#1088#1091#1075#1083#1077#1085#1080#1077#1084' '#1076#1086' 1 '#1082#1074#1072#1076#1088#1072#1090#1085#1086#1075#1086' '#1084 +
        #1077#1090#1088#1072' '
      BevelKind = bkFlat
      BorderStyle = bsNone
      Color = clWhite
      ParentShowHint = False
      ShowHint = False
      TabOrder = 4
      Text = ''
      StyleElements = []
      OnExit = Action_Delta_WriteExecute
      OnKeyUp = Delta_Area_MaskEditKeyUp
    end
  end
  object ActionList: TActionList
    Left = 656
    Top = 48
    object Action_Area_In_GKN: TAction
      Caption = 'Action_Area_In_GKN'
      OnExecute = Action_Area_In_GKNExecute
    end
    object Action_Delta_Write: TAction
      Caption = 'Action_Delta_Write'
      OnExecute = Action_Delta_WriteExecute
    end
    object Action_Min_Area_Write: TAction
      Caption = 'Action_Min_Area_Write'
      OnExecute = Action_Min_Area_WriteExecute
    end
    object Action_Max_Area_Write: TAction
      Caption = 'Action_Max_Area_Write'
      OnExecute = Action_Max_Area_WriteExecute
    end
    object Action_Note: TAction
      Caption = 'Action_Note'
      OnExecute = Action_NoteExecute
    end
  end
end
