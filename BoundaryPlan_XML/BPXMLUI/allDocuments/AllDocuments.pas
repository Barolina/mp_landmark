unit AllDocuments;

interface
 uses
  XML.Xmldoc,
  XML.XmlIntf,

  System.SysUtils,
  System.Classes,
  System.RegularExpressions,
  System.Generics.Collections,
  System.Generics.Defaults;

Type

 TAllDocuments = class
  public type
   //���� ������ ��� ���������� ������ ��������������
   TNode = class
    strict private
     FId: integer;
     FParent: TNode;
     FChildList: TObjectList<TNode>;
     procedure NotifyChildList(Sender: TObject; const Item: TNode; Action: TCollectionNotification);
    public
     constructor Create(aId: integer);
     destructor Destroy; override;

     property Parent: TNode read FParent;
     property ChildList: TObjectList<TNode> read FChildList;
     property Id: integer read FId write FId;
   end;
  private
   FKeys: TDictionary<Integer,string>;// ������� �����
   FValues: TDictionary<Integer,string>; //������� �������� �����
   FRootNode: TNode; //�������� ���� �������� ��������������

  protected type
   TGetParentKeyFunc = reference to function(aKey:string):string;
  protected
   //��������� ���� ������������� ���� (���� ��� �� return '')
   function getParentKey_00800(aKey:string): string;
   function getParentKey_558(aKey:string): string;
   function getParentKey_empty(aKey:string):string;

   function FindNodeByIdKey(aRoot:TNode; aID: Integer): TNode;
   procedure doAppent(aKey,aValue:string; funcGetParent: TGetParentKeyFunc);
  public
   constructor Create;
   destructor Destroy; override;

   function getKey(aID: integer): string;
   function getValue(aID: integer): string;
   function getIdByKey(aKey: string): integer;
   function getIdByValue(aValue: string): integer;
   function Key2Value(aKey: string): string;
   function Value2Key(aValue: string): string;


   ///	<summary>
   ///	  <para>
   ///	    ����� ��������� ���� "����-��������" � ��������� ������.
   ///	  </para>
   ///	  <para>
   ///	    �� ������ �������� "�����" ����������� ID (��� ���) ����������
   ///	    ���������� ��������������� � ������� ������ � �������� � �����
   ///	    �������������������� ���� �������� ��������������
   ///	  </para>
   ///	</summary>
   ///	<param name="aKey">
   ///	  ���� (���������� ��������)
   ///	</param>
   ///	<param name="aValue">
   ///	  �������� �����
   ///	</param>
   ///	<remarks>
   ///	  ����������� ���������� ������������ �� ����, ������� ������������
   ///	  �������� ������ ���������� ������ ��������
   ///	</remarks>
   procedure Appent(aKey,aValue: string); virtual;
   procedure LoadFromFile(PathFile:string);

   property RootNode: TNode read FRootNode;
 end;



implementation

resourcestring
  rsMask558 = '^558[^0]\d*$';
  rsMask00800 = '^00800[^0]\d*$';
  rsMask55500 = '^55500[^0]\d*$';

{ TAllDocuments.TNode }

constructor TAllDocuments.TNode.Create(aID: integer);
begin
 self.FId:= aID;
 self.FChildList:= TObjectList<TNode>.Create;
 self.FChildList.OnNotify:= self.NotifyChildList;
end;

destructor TAllDocuments.TNode.Destroy;
begin
 self.FChildList.OnNotify:= nil;
 self.FChildList.Free;
end;

procedure TAllDocuments.TNode.NotifyChildList(Sender: TObject; const Item: TNode; Action: TCollectionNotification);
var
 _parent: TNode;
begin
 case Action of
   cnAdded:
    if Assigned(Item.Parent) then begin
     Item.Parent.ChildList.Extract(Item);
     Item.FParent:= self;
    end;
   cnRemoved,cnExtracted: Item.FParent:= nil;
 end;
end;

{ TAllDocuments }

procedure TAllDocuments.Appent(aKey, aValue: string);
begin
 if TRegEx.IsMatch(aKey,rsMask558) then begin
  self.doAppent(aKey,aValue,self.getParentKey_558);
  EXIT;
 end;

 if TRegEx.IsMatch(aKey,rsMask00800) then begin
  self.doAppent(aKey,aValue,self.getParentKey_00800);
  EXIT;
 end;

// if TRegEx.IsMatch(aKey,rsMask55500) then
  self.doAppent(aKey,aValue,self.getParentKey_empty);


end;

procedure TAllDocuments.doAppent(aKey, aValue: string; funcGetParent: TGetParentKeyFunc);
var
 _idKey: integer;
 _key: string;
 _node: TNode;
begin
 _key := AnsiLowerCase(funcGetParent(aKey));
 _idKey := BobJenkinsHash(PChar(_key)^, SizeOf(Char) * Length(_key), 0);
 _node:= self.FindNodeByIdKey(self.FRootNode,_idKey);
 if not Assigned(_node) then
  _node:= self.FRootNode;

 _key := AnsiLowerCase(akey);
 _idKey := BobJenkinsHash(PChar(_key)^, SizeOf(Char) * Length(_key), 0);

 self.FKeys.Add(_idKey,aKey);
 self.FValues.Add(_idKey,aValue);

 _node.ChildList.Add(TNode.Create(_idKey));
end;


constructor TAllDocuments.Create;
begin
 self.FKeys:= TDictionary<Integer,string>.Create;
 self.FValues:= TDictionary<Integer,string>.Create;
 self.FRootNode:= TNode.Create(-1);
end;

destructor TAllDocuments.Destroy;
begin

  inherited;
end;

function TAllDocuments.FindNodeByIdKey(aRoot:TNode; aID: Integer): TNode;
var
 _node: TNode;
begin
 Result:= nil;
 if aRoot.Id=aID then Exit(aRoot);
 for _node in aRoot.ChildList do begin
  Result:= self.FindNodeByIdKey(_node,aID);
  if Assigned(Result) then BREAK;
 end;
end;

function TAllDocuments.getIdByKey(aKey: string): integer;
begin
 akey := AnsiLowerCase(aKey);
 Result:= BobJenkinsHash(PChar(aKey)^, SizeOf(Char) * Length(aKey), 0);
 if not self.FKeys.ContainsKey(Result) then
  Result:= -1;
end;

function TAllDocuments.getIdByValue(aValue: string): integer;
var
 _Pair: TPair<integer,string>;
begin
 Result:= -1;
 for _Pair in self.FValues do
  if CompareText(_Pair.Value,aValue) = 0 then Result:= _Pair.Key;
end;

function TAllDocuments.getKey(aID: integer): string;
begin
 if not self.FKeys.TryGetValue(aID,Result) then
   Result:= '';
end;

function TAllDocuments.getParentKey_00800(aKey: string): string;
begin
 if Length(aKey)<>12 then Exit('');
 if aKey.Contains('000000') then EXIT('');
 if aKey.Contains('000') then EXIT(copy(akey,low(akey),6)+'000000');
 Result:=copy(akey,low(akey),9)+'000'
end;

function TAllDocuments.getParentKey_558(aKey: string): string;
begin
 if Length(aKey)<>12 then Exit('');
 if aKey.Contains('00000000') then EXIT('');//NNNN00000000->''
 if aKey.Contains('000000') then EXIT(copy(akey,low(akey),4)+'00000000');//nnnn99000000->558100000000
 if aKey.Contains('0000') then EXIT(copy(akey,low(akey),6)+'000000');//nnnn99990000->558199000000
 if aKey.Contains('00') then EXIT(copy(akey,low(akey),8)+'0000'); //nnnn99999900->558199990000
 Result:=copy(akey,low(akey),10)+'00' //nnnn99999999->558199999900
end;

function TAllDocuments.getParentKey_empty(aKey: string): string;
begin
 Result:= '';
end;

function TAllDocuments.getValue(aID: integer): string;
begin
 if not self.FValues.TryGetValue(aID,Result) then
  Result:= '';
end;

function TAllDocuments.Key2Value(aKey: string): string;
var
 _id: integer;
begin
 _id:= self.getIdByKey(aKey);
 if _id<>-1 then
  Result:= self.FValues[_id]
 else
  Result:= '';
end;

procedure TAllDocuments.LoadFromFile(PathFile: string);
 var
  _i: Integer;
  _Doc: IXMLDocument;
  _Node, _Enum: IXMLNode;
  _key,_value: string;
begin
 self.FRootNode.ChildList.Clear;
 self.FKeys.Clear;
 self.FValues.Clear;
 _Doc := TXMLDocument.Create(nil);
 _Doc.fileName := PathFile;
 _Doc.Active := true;
 _Node := _Doc.DocumentElement.ChildNodes.Nodes['simpleType'];
 _Node := _Node.ChildNodes.Nodes['restriction'];
 for _i := 0 to _Node.ChildNodes.Count - 1 do begin

    _Enum := _Node.ChildNodes.Get(_i);
    _key := _Enum.Attributes['value'];
    _value := _Enum.ChildNodes.Nodes['annotation'].ChildNodes.Nodes
      ['documentation'].NodeValue;
    self.Appent(_key,_value);
 end;
  _Doc := nil;
end;

function TAllDocuments.Value2Key(aValue: string): string;
var
 _id: integer;
begin
 _id:= self.getIdByValue(aValue);
 if _id<>-1 then
  Result:= self.FKeys[_id]
 else
  Result:= '';
end;

end.
