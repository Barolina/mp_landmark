unit MSMFilter;
interface
uses MSType;
procedure msmfSeedDist(Points: TPointsLogic; val: Double);
implementation
procedure msfDelPoint(Points: TPointsLogic; index: integer);
var  
 i: integer;
begin
 for I := index to High(Points) - 1 do
  Points[i]:= Points[i+1];
 SetLength(Points,length(Points)-1);  
end;
procedure msmfSeedDist(Points: TPointsLogic; val: Double);
var
 i: integer;
 d: Double;
begin
 if Length(Points)<3 then Exit;
 i:= 0;
 while i<>High(Points) do begin
  d:= Points[i].GetDistance(Points[i+1].x,Points[i+1].y);
  if d<val then
   msfDelPoint(Points,i+1)
  else
   inc(i);   
 end;                     
end;
end.
