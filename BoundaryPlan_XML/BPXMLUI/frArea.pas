unit frArea;

{
  ���������� ������� ����������  �������
}
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, StdCtrls, ExtCtrls, Mask,

  STD_MP, ActnList, System.Actions,Xml.XMLIntf,unRussLang;

type
  TFrameArea = class(TFrame)
    AreaValue_MaskEdit: TMaskEdit;
    Innccuracy_MaskEdit: TMaskEdit;
    AreaValue_Label_Caption: TLabel;
    Innccuracy_Label_Caption: TLabel;
    ActionList: TActionList;
    Action_AreaValue_Write: TAction;
    Action_Innccuracy_Write: TAction;
    procedure Action_AreaValue_WriteExecute(Sender: TObject);
    procedure Action_Innccuracy_WriteExecute(Sender: TObject);
    procedure AreaValue_MaskEditKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Innccuracy_MaskEditKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }

    FXmlArea        : IXMLTArea;
    FXmlAreaExistEZ :IXMLTAreaNew;
    FXmlNode        : IXMLNode;
  protected
   procedure InitializationUI;
   procedure SetArea(aArea: IXMLTArea);
   procedure SetAreaExistEZ(aArea : IXMLTAreaNew);
  public
    { Public declarations }

   procedure UI_Unit_Write055To;
   procedure UI_Unit_Write055ToInvariable(const aArea: IXMLTExistParcel_SubParcels_InvariableSubParcel_Area);

   procedure UI_AreaValue_WriteTo;
   procedure UI_AreaValue_ReadOf;
   function  UI_Areavalue_IsValid: boolean;

   procedure UI_Innccuracy_WriteTo;
   procedure UI_Innccuracy_ReadOf;
   function  UI_Innccuracy_IsValid: boolean;

   procedure UI_WriteTo;
   procedure UI_ReadOf;

   function IsAssignedArea: boolean;
   property XmlArea: IXMLTArea read FXmlArea write SetArea;
   property XmlAreaExistEZ : IXMLTAreaNew read FXmlAreaExistEZ write SetAreaExistEZ;
  end;

  {
    TODO: isVisible_Area_Innccuracy -  ����������  �� �������   Innccuracy
  }
implementation

uses MsXMLAPI;


{$R *.dfm}

{ TFrameArea }

procedure TFrameArea.Action_AreaValue_WriteExecute(Sender: TObject);
begin
     if not self.IsAssignedArea then raise Exception.Create(ExMSG_NotAssignedROOT);
     self.UI_AreaValue_WriteTo;
     self.UI_Unit_Write055To;
     self.UI_Innccuracy_WriteTo;
end;

procedure TFrameArea.Action_Innccuracy_WriteExecute(Sender: TObject);
begin
 if not self.IsAssignedArea then raise Exception.Create(ExMSG_NotAssignedROOT);
   self.UI_Innccuracy_WriteTo;
   self.UI_Unit_Write055To;
end;

procedure TFrameArea.AreaValue_MaskEditKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if key = VK_RETURN then
  if (Sender is TMaskEdit) then
   TMaskEdit(Sender).Parent.SetFocus;
end;

procedure TFrameArea.InitializationUI;
begin
 if self.IsAssignedArea then
 begin
     self.UI_ReadOf;
 end
 else begin
  self.AreaValue_MaskEdit.Text:= '';
  self.Innccuracy_MaskEdit.Text:= '';
 end;
end;

procedure TFrameArea.Innccuracy_MaskEditKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if key = VK_RETURN then
  if (Sender is TCustomEdit) then
   TCustomEdit(Sender).Parent.SetFocus;
end;

function TFrameArea.IsAssignedArea: boolean;
begin
   Result:= self.FXmlNode<>nil
end;

procedure TFrameArea.SetArea(aArea: IXMLTArea);
begin
 if self.FXmlNode=aArea then EXIT;
 self.FXmlNode:= aArea;
 self.InitializationUI;
end;

procedure TFrameArea.SetAreaExistEZ(aArea: IXMLTAreaNew);
begin
 if self.FXmlNode=aArea then EXIT;
 self.FXmlNode:= aArea;
 self.InitializationUI;
end;

function TFrameArea.UI_Areavalue_IsValid: boolean;
var
 _t: Extended;
begin
// Result:= TryStrToFloat(Self.AreaValue_MaskEdit.Text,_t)
Result := True;
end;


procedure TFrameArea.UI_AreaValue_ReadOf;
begin
 self.AreaValue_MaskEdit.Text:= MsXml_ReadChildNodeValue(FXmlNode,cnstXmlArea);
end;

procedure TFrameArea.UI_AreaValue_WriteTo;
begin
 if self.AreaValue_MaskEdit.Text='' then EXIT;
 if not self.UI_Areavalue_IsValid then begin
  self.UI_AreaValue_ReadOf;
  raise Exception.Create(format(ExMsg_Data_Invalid,[self.AreaValue_Label_Caption.Caption]));
 end;
 FXmlNode.ChildValues[cnstXmlArea]:=StrToFloat(self.AreaValue_MaskEdit.Text);
end;


function TFrameArea.UI_Innccuracy_IsValid: boolean;
var
 _t: Double;
begin
 Result:= TryStrToFloat(Self.Innccuracy_MaskEdit.Text,_t);
end;

procedure TFrameArea.UI_Innccuracy_ReadOf;
begin
 self.Innccuracy_MaskEdit.Text:= MsXml_ReadChildNodeValue(FXmlNode,'Innccuracy');
end;

procedure TFrameArea.UI_Innccuracy_WriteTo;
begin
 if self.Innccuracy_MaskEdit.Text='' then EXIT;
 if not self.UI_Innccuracy_IsValid then begin
  self.UI_Innccuracy_ReadOf;
  raise Exception.Create(format(ExMsg_Data_Invalid,[self.Innccuracy_Label_Caption.Caption]));
 end;
 FXmlNode.ChildValues['Innccuracy']:=StrtoInt(self.Innccuracy_MaskEdit.Text);
end;

procedure TFrameArea.UI_ReadOf;
begin
 self.UI_AreaValue_ReadOf;
 self.UI_Innccuracy_ReadOf;
end;


procedure TFrameArea.UI_Unit_Write055To;
begin
 FXmlNode.ChildValues['Unit'] := StdMp_Area_Unit;
end;

procedure TFrameArea.UI_Unit_Write055ToInvariable(
  const aArea: IXMLTExistParcel_SubParcels_InvariableSubParcel_Area);
begin
 aArea.Unit_:= StdMp_Area_Unit;
end;

procedure TFrameArea.UI_WriteTo;
begin
 self.UI_AreaValue_WriteTo;
 self.UI_Unit_Write055To;
 self.UI_Innccuracy_WriteTo;
end;

end.
