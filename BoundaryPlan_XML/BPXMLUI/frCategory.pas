unit frCategory;
{
   ��� NewParcels
}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, ButtonGroup, StdCtrls, ActnList,

   STD_MP, BpXML_different, xmlintf, ImgList, System.Actions, Vcl.Mask,
   unRussLang, Vcl.ExtCtrls
  ,frDocument;

type
  TFrameCategory = class(TFrame)
    ActionList: TActionList;
    Action_AddByKind: TAction;
    Action_AddByDoc: TAction;
    Action_DelSel: TAction;
    ImageList: TImageList;
    pnlDoc: TPanel;
    pnlAttrib: TPanel;
    lblDoc: TLabel;
    chkDoc: TCheckBox;
    cbbCategory_ComboBox: TComboBox;
    lblCategory: TLabel;
    procedure ByDoc_EditExit(Sender: TObject);
    procedure chkDocClick(Sender: TObject);
    procedure cbbCategory_ComboBoxChange(Sender: TObject);
  private
    { Private declarations }
    FXmlCategory: IXMLTCategory;
    FFrame_Documnet    : TFrameDocument;

    procedure UI_Category_Read;
    procedure UI_Category_Write;

  protected
    procedure InitializationUI;
    procedure SetCategory(const Category: IXMLTCategory);
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;

    function IsAssignedcATEGORY: Boolean;
    property XmlCategory: IXMLTCategory read FXmlCategory write SetCategory;
  end;

implementation

uses MsXMLAPI;
{$R *.dfm}


procedure _HiddenChangeCheckBoxSate(ChBox: TCheckBox; NewSate: TCheckBoxState);
var
  _OnClick: TNotifyEvent;
begin
  if ChBox.State = NewSate then
    EXIT;
  _OnClick := ChBox.OnClick;
  ChBox.OnClick := nil;
  ChBox.State := NewSate;
  ChBox.OnClick := _OnClick;
end;
{ TFrameUtilizationList }

procedure TFrameCategory.ByDoc_EditExit(Sender: TObject);
begin
  if  self.IsAssignedcATEGORY  then
    self.UI_Category_Write;
end;

procedure TFrameCategory.cbbCategory_ComboBoxChange(Sender: TObject);
begin
  if not self.IsAssignedcATEGORY then
    raise Exception.Create(ExMSG_NotAssignedROOT);
  self.UI_Category_Write;
end;

procedure TFrameCategory.chkDocClick(Sender: TObject);
begin
  if not chkDoc.Checked then
  begin
     if FFrame_Documnet.XMlDOcument <> nil then
        if MessageDlg('������� ������: '+lblDoc.Caption+'?',mtInformation,mbYesNo,-1) = mrYes then
        begin
           MsXml_RemoveChildNode(self.FXmlCategory, 'DocCategory');
           self.FFrame_Documnet.XMlDOcument := nil;
           self.FFrame_Documnet.Visible := false;
        end else chkDoc.Checked := True;
  end
  else
  begin
    self.FFrame_Documnet.XMlDOcument := self.FXmlCategory.DocCategory;
    self.FFrame_Documnet.Visible := true;
  end
end;

constructor TFrameCategory.Create(AOwner: TComponent);
begin
 inherited;
 TSTD_MP_ListofIndexGenerate.BpXml_LoadDictionary(STD_MP_WorkDir + 'dCategories.xsd', self.cbbCategory_ComboBox.Items);
 self.FFrame_Documnet := TFrameDocument.Create(self.pnlDoc);
 Self.FFrame_Documnet.Parent := self.pnlDoc;
 self.FFrame_Documnet.Align := alClient;
 self.FFrame_Documnet.Visible := false;
end;

procedure TFrameCategory.InitializationUI;
var _node : IXMLNode;
begin
 self.cbbCategory_ComboBox.ItemIndex:= -1;
 if self.IsAssignedcATEGORY then
 begin
    Self.UI_Category_Read;
    _node := self.FXmlCategory.ChildNodes.FindNode('DocCategory');
    if _node <> nil then
    begin
      self.FFrame_Documnet.XMlDOcument := self.FXmlCategory.DocCategory;
      self.FFrame_Documnet.Visible := true;
      _HiddenChangeCheckBoxSate(self.chkDoc, cbChecked);
    end
    else
    begin
      self.FFrame_Documnet.XMlDOcument := nil;
      self.FFrame_Documnet.Visible := false;
      _HiddenChangeCheckBoxSate(self.chkDoc,cbUnchecked);
    end;
 end
 else
 begin
   self.FFrame_Documnet.XMlDOcument := nil;
 end;
end;

function TFrameCategory.IsAssignedcATEGORY: Boolean;
begin
 Result:= self.FXmlCategory<>nil;
end;

procedure TFrameCategory.SetCategory( const Category: IXMLTCategory);
begin
 if self.FXmlCategory=Category then EXIT;
 self.FXmlCategory:= Category;
 self.InitializationUI;
end;


procedure TFrameCategory.UI_Category_Read;
var
  _code: string;
begin
  _code := MsXml_ReadAttribute(self.FXmlCategory, 'Category');
  self.cbbCategory_ComboBox.ItemIndex :=
    TSTD_MP_ListofIndexGenerate.BpXml_FindItemByCode(self.cbbCategory_ComboBox.Items, _code);
end;

procedure TFrameCategory.UI_Category_Write;
var
  _item: TBpXml_DictionaryItem;
  _i: integer;
begin
  _i := self.cbbCategory_ComboBox.ItemIndex;
  if _i = -1 then
    EXIT;
  _item := TBpXml_DictionaryItem(self.cbbCategory_ComboBox.Items.Objects[_i]);
  self.FXmlCategory.Category := _item.Code;
end;

end.
