﻿object FrameAddress: TFrameAddress
  Left = 0
  Top = 0
  Width = 919
  Height = 541
  Color = clWhite
  ParentBackground = False
  ParentColor = False
  TabOrder = 0
  object Label2: TLabel
    Left = 448
    Top = 8
    Width = 36
    Height = 13
    Caption = #1050#1086#1088#1087#1091#1089
  end
  object pnlLocation: TPanel
    Left = 0
    Top = 0
    Width = 919
    Height = 45
    Align = alTop
    BevelEdges = [beLeft, beTop, beRight]
    BevelOuter = bvNone
    ParentBackground = False
    TabOrder = 0
    ExplicitWidth = 909
    object Region_Label_Caption: TLabel
      Left = 8
      Top = 2
      Width = 35
      Height = 13
      Caption = #1056#1077#1075#1080#1086#1085
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object lblCodе_KLADR: TLabel
      Left = 520
      Top = 1
      Width = 34
      Height = 16
      Caption = #1082#1083#1072#1076#1088
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object lblCode_OKATO: TLabel
      Left = 405
      Top = 1
      Width = 33
      Height = 16
      Caption = #1086#1082#1072#1090#1086
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object jvlnklblOKato: TJvLinkLabel
      Left = 746
      Top = 26
      Height = 13
      Caption = #1044#1072#1085#1085#1099#1077' '#1054#1050#1040#1058#1054' '#1080' '#1050#1051#1040#1044#1056#13#10
      Text.Strings = (
        #1044#1072#1085#1085#1099#1077' '#1054#1050#1040#1058#1054' '#1080' '#1050#1051#1040#1044#1056#13#10)
      Layout = tlCenter
      DragCursor = crHandPoint
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      Font.Quality = fqClearType
      ParentFont = False
      OnClick = jvlnklblOKatoClick
    end
    object edtCodе_KLADR: TEdit
      Left = 520
      Top = 18
      Width = 220
      Height = 21
      BevelKind = bkFlat
      BorderStyle = bsNone
      Color = 15921123
      MaxLength = 20
      TabOrder = 1
      OnExit = edtCodе_KLADRExit
      OnKeyUp = _ParentSetFocus
    end
    object edtCode_OKATO: TEdit
      Left = 399
      Top = 18
      Width = 105
      Height = 21
      BevelKind = bkFlat
      BorderStyle = bsNone
      Color = 15921123
      MaxLength = 11
      TabOrder = 0
      OnExit = edtCode_OKATOExit
      OnKeyUp = _ParentSetFocus
    end
  end
  object CategoryPanelGroup1: TCategoryPanelGroup
    Left = 0
    Top = 45
    Width = 919
    Height = 496
    VertScrollBar.Tracking = True
    Align = alClient
    BevelInner = bvNone
    BevelOuter = bvNone
    Color = clBtnFace
    GradientBaseColor = 15269864
    GradientColor = 15269864
    HeaderAlignment = taCenter
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -11
    HeaderFont.Name = 'Tahoma'
    HeaderFont.Style = []
    HeaderHeight = 15
    HeaderStyle = hsThemed
    TabOrder = 2
    object ctpnlNote: TCategoryPanel
      Top = 345
      Height = 58
      Caption = #1053#1077#1092#1086#1088#1084#1072#1083#1080#1079#1086#1074#1072#1085#1085#1086#1077' '#1086#1087#1080#1089#1072#1085#1080#1077' '#1084#1077#1089#1090#1086#1087#1086#1083#1086#1078#1077#1085#1080#1103
      TabOrder = 8
      object mmoNote: TMemo
        Left = 0
        Top = 0
        Width = 915
        Height = 41
        Align = alClient
        MaxLength = 4000
        TabOrder = 0
        OnExit = mmoNoteExit
        OnKeyUp = _ParentSetFocus
      end
    end
    object ctgrypnlOther: TCategoryPanel
      Top = 287
      Height = 58
      Caption = #1048#1085#1086#1077
      TabOrder = 7
      object mmoOther: TMemo
        Left = 0
        Top = 0
        Width = 915
        Height = 41
        Align = alClient
        MaxLength = 4000
        TabOrder = 0
        OnExit = mmoOtherExit
        OnKeyUp = _ParentSetFocus
        ExplicitTop = -5
      end
    end
    object ctgrypnlOtherGeneral: TCategoryPanel
      Top = 229
      Height = 58
      Caption = #1055#1086#1076#1088#1086#1073#1085#1086#1077' '#1086#1087#1080#1089#1072#1085#1080#1077' '#1072#1076#1088#1077#1089#1072
      TabOrder = 6
      object LabelStreet: TLabel
        Left = 168
        Top = 0
        Width = 20
        Height = 13
        Caption = #1044#1086#1084
      end
      object Label1: TLabel
        Left = 392
        Top = 1
        Width = 36
        Height = 13
        Caption = #1050#1086#1088#1087#1091#1089
      end
      object Label3: TLabel
        Left = 607
        Top = 2
        Width = 49
        Height = 13
        Caption = #1057#1090#1088#1086#1077#1085#1080#1077
      end
      object Label4: TLabel
        Left = 6
        Top = 0
        Width = 90
        Height = 13
        Caption = #1055#1086#1095#1090#1086#1074#1099#1081' '#1080#1085#1076#1077#1082#1089
      end
      object edtLevel1: TEdit
        Left = 164
        Top = 15
        Width = 154
        Height = 21
        MaxLength = 255
        TabOrder = 2
        OnExit = edtLevel1Exit
        OnKeyUp = _ParentSetFocus
      end
      object btndtLevel1: TButtonedEdit
        Left = 317
        Top = 15
        Width = 60
        Height = 22
        BevelKind = bkFlat
        BorderStyle = bsNone
        Color = 15921123
        Images = ImageList
        ReadOnly = True
        RightButton.ImageIndex = 2
        RightButton.Visible = True
        TabOrder = 3
        OnRightButtonClick = btndtLevel1RightButtonClick
      end
      object edtLevel2: TEdit
        Left = 387
        Top = 16
        Width = 154
        Height = 21
        MaxLength = 255
        TabOrder = 5
        OnExit = edtLevel2Exit
        OnKeyUp = _ParentSetFocus
      end
      object btndtLevel2: TButtonedEdit
        Left = 541
        Top = 16
        Width = 54
        Height = 22
        BevelKind = bkFlat
        BorderStyle = bsNone
        Color = 15921123
        Images = ImageList
        ReadOnly = True
        RightButton.ImageIndex = 2
        RightButton.Visible = True
        TabOrder = 6
        OnRightButtonClick = btndtLevel2RightButtonClick
      end
      object edtLevel3: TEdit
        Left = 604
        Top = 15
        Width = 154
        Height = 21
        MaxLength = 255
        TabOrder = 4
        OnExit = edtLevel3Exit
        OnKeyUp = _ParentSetFocus
      end
      object btndtLevel3: TButtonedEdit
        Left = 758
        Top = 14
        Width = 61
        Height = 22
        BevelKind = bkFlat
        BorderStyle = bsNone
        Color = 15921123
        Images = ImageList
        ReadOnly = True
        RightButton.ImageIndex = 2
        RightButton.Visible = True
        TabOrder = 0
        OnRightButtonClick = btndtLevel3RightButtonClick
      end
      object edtPostal_Code: TEdit
        Left = 4
        Top = 15
        Width = 154
        Height = 21
        MaxLength = 255
        TabOrder = 1
        OnExit = lbledtPostal_CodeExit
        OnKeyUp = _ParentSetFocus
      end
    end
    object ctgrypnlOtherStreet: TCategoryPanel
      Top = 191
      Height = 38
      Caption = #1059#1083#1080#1094#1072
      TabOrder = 5
      object btndtStreet: TButtonedEdit
        Left = 765
        Top = 0
        Width = 150
        Height = 21
        Align = alRight
        BevelKind = bkFlat
        BorderStyle = bsNone
        Color = 15921123
        Images = ImageList
        ReadOnly = True
        RightButton.ImageIndex = 2
        RightButton.Visible = True
        TabOrder = 1
        OnRightButtonClick = btndtStreetRightButtonClick
        ExplicitLeft = 771
        ExplicitTop = -5
      end
      object edtStreet: TEdit
        Left = 0
        Top = 0
        Width = 765
        Height = 21
        Align = alClient
        MaxLength = 255
        TabOrder = 0
        OnExit = edtStreetExit
        OnKeyUp = _ParentSetFocus
      end
    end
    object ctpnlLocality: TCategoryPanel
      Top = 153
      Height = 38
      Caption = #1053#1072#1089#1077#1083#1077#1085#1085#1099#1081' '#1087#1091#1085#1082#1090
      TabOrder = 4
      object edtLocality: TEdit
        Left = 0
        Top = 0
        Width = 765
        Height = 21
        Align = alClient
        MaxLength = 255
        TabOrder = 0
        OnExit = edtLocalityExit
        OnKeyUp = _ParentSetFocus
      end
      object btnEdLocality: TButtonedEdit
        Left = 765
        Top = 0
        Width = 150
        Height = 21
        Align = alRight
        BevelKind = bkFlat
        BorderStyle = bsNone
        Color = 15921123
        Images = ImageList
        ReadOnly = True
        RightButton.ImageIndex = 2
        RightButton.Visible = True
        TabOrder = 1
        OnRightButtonClick = btnEdLocalityRightButtonClick
      end
    end
    object ctpnlSoviet_Village: TCategoryPanel
      Top = 114
      Height = 39
      Caption = #1057#1077#1083#1100#1089#1086#1074#1077#1090
      TabOrder = 3
      object edtSoviet_Village: TEdit
        Left = 0
        Top = 0
        Width = 915
        Height = 22
        Align = alClient
        MaxLength = 255
        TabOrder = 0
        OnExit = edtSoviet_VillageExit
        OnKeyUp = _ParentSetFocus
        ExplicitHeight = 21
      end
    end
    object ctpnlUrban_District: TCategoryPanel
      Top = 76
      Height = 38
      Caption = #1043#1086#1088#1086#1076#1089#1082#1086#1081' '#1088#1072#1081#1086#1085
      TabOrder = 2
      object edtUrban_District: TEdit
        Left = 0
        Top = 0
        Width = 915
        Height = 21
        Align = alClient
        MaxLength = 255
        TabOrder = 0
        OnExit = edtUrban_DistrictExit
        OnKeyUp = _ParentSetFocus
      end
    end
    object ctpnlCity: TCategoryPanel
      Top = 38
      Height = 38
      Caption = #1052#1091#1085#1080#1094#1080#1087#1072#1083#1100#1085#1086#1077' '#1086#1073#1088#1072#1079#1086#1074#1072#1085#1080#1077
      TabOrder = 1
      object edtCity: TEdit
        Left = 0
        Top = 0
        Width = 765
        Height = 21
        Align = alClient
        MaxLength = 255
        TabOrder = 0
        OnExit = edtCityExit
        OnKeyUp = _ParentSetFocus
      end
      object btnEdCity: TButtonedEdit
        Left = 765
        Top = 0
        Width = 150
        Height = 21
        Align = alRight
        BevelKind = bkFlat
        BorderStyle = bsNone
        Color = 15921123
        Images = ImageList
        ReadOnly = True
        RightButton.ImageIndex = 2
        RightButton.Visible = True
        TabOrder = 1
        OnRightButtonClick = btnEdCityRightButtonClick
      end
    end
    object ctpnlDistrict: TCategoryPanel
      Top = 0
      Height = 38
      Caption = #1056#1072#1081#1086#1085
      TabOrder = 0
      object edtDistrict: TEdit
        Left = 0
        Top = 0
        Width = 765
        Height = 21
        Align = alClient
        MaxLength = 255
        TabOrder = 0
        OnExit = edtDistrictExit
        OnKeyUp = _ParentSetFocus
      end
      object btnEdDistrict: TButtonedEdit
        Left = 765
        Top = 0
        Width = 150
        Height = 21
        Align = alRight
        BevelKind = bkFlat
        BorderStyle = bsNone
        Color = 15921123
        Images = ImageList
        ReadOnly = True
        RightButton.ImageIndex = 2
        RightButton.Visible = True
        TabOrder = 1
        OnRightButtonClick = btnEdDistrictRightButtonClick
      end
    end
  end
  object btnRegion: TButtonedEdit
    Left = 8
    Top = 18
    Width = 385
    Height = 21
    BevelKind = bkFlat
    BorderStyle = bsNone
    Color = 15921123
    Images = ImageList
    ReadOnly = True
    RightButton.ImageIndex = 2
    RightButton.Visible = True
    TabOrder = 1
    OnRightButtonClick = btnRegionRightButtonClick
  end
  object ActionList: TActionList
    Left = 512
    Top = 64
    object Action_WriteRegion: TAction
      Caption = 'Action_WriteRegion'
    end
    object Action_WriteNote: TAction
      Caption = 'Action_WriteNote'
    end
  end
  object ImageList: TImageList
    Left = 576
    Top = 64
    Bitmap = {
      494C0101030004005C0010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6847300B5848400B584
      8400B5848400B5848400B5848400B5848400B5848400B5848400B5848400B584
      8400B5848400B5848400B5848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000010CB970012CA960012CA960011CD9900000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C6A59C00FFFFEF00FFFFEF00FFFF
      EF00FFFFEF00FFF7DE00FFF7DE00FFF7DE00FFF7DE00FFEFD600FFEFD600FFEF
      D600FFDEB500FFDEB500FFD6A500B58484000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000009C993000CC892000CC892000BC99400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000044FDA00000000000000000000000000000000000000000000000000044E
      DA0000000000000000000000000000000000C6A59C00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFEFD600FFEFD600FFEFD600FFEFD600FFEFD600FFEFD600FFEF
      D600FFEFD600FFEFD600FFDEB500B58484000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000ACA95000DCA94000CCA94000CCA9600000000000000
      0000000000000000000000000000000000000000000000000000000000000553
      DF000351D8000552DD00000000000000000000000000000000000452DC000351
      D7000452DD00000000000000000000000000C6ADA500FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00E77B2100E77B2100E77B2100E77B2100E77B2100E77B2100FFF7
      DE00FFEFD600FFEFD600FFEFD600B58484000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000BCD98000DCC96000DCC96000CCE9800000000000000
      00000000000000000000000000000000000000000000000000000654DB000655
      DA000755DA000655DB000652D80000000000000000000651D7000655DB000755
      DA000655DA000654DA000000000000000000C6ADA500FFFFFF00FFEFD600FFEF
      D600FFEFD600FFEFD600FFEFD600FFEFD600FFEFD600FFEFD600FFEFD600FFEF
      D600FFEFD600FFEFD600FFEFD600B58484000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000CD099000DCF99000DCF99000CCF9900000000000000
      0000000000000000000000000000000000000000000000000000000000000959
      DE000858DC000958DC000858DD000B57DB000B58DB000858DD000858DD000858
      DC000959DD000957DB000000000000000000D6B5AD00FFFFFF00E77B2100E77B
      2100E77B2100E77B2100E77B2100E77B2100E77B2100E77B2100E77B2100E77B
      2100E77B2100E77B2100FFEFD600B58484000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000CD49F000ED29E000ED2
      9E000ED29E000ED19C000DD19C000ED19C000DD19C000DD19C000ED19D000ED2
      9E000ED29E000ED29E000DD5A100000000000000000000000000000000000000
      00000B5CE0000B5BDF000B5BDF000A5BDF000B5CDF000B5BDF000B5BDF000B5C
      E0000B5BDD00000000000000000000000000D6B5AD00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFEFD600FFEFD600FFEFD600FFEFD600FFEFD600FFEFD600FFFF
      FF00FFFFEF00FFF7DE00FFEFD600B58484000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000CD6A0000ED49F000ED4
      9F000ED49F000ED49F000ED49F000ED49F000ED49F000ED49F000ED59F000ED4
      9F000ED59F000ED49F000DD7A200000000000000000000000000000000000000
      0000000000000D5FE2000D5EE1000D5EE2000D5FE2000D5FE1000D5FE2000D5C
      DF0000000000000000000000000000000000D6BDB500FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00E77B2100E77B2100E77B2100E77B2100E77B2100E77B2100FFFF
      FF00FFFFEF00FFFFEF00FFF7DE00B58484000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000CD8A3000ED7A2000ED7
      A2000ED7A2000ED7A2000ED7A2000ED7A2000ED7A2000ED7A2000ED7A2000ED7
      A2000ED7A2000ED7A2000EDAA500000000000000000000000000000000000000
      0000000000000F63E5000F62E4000F62E4000F62E4000F62E4000F63E5000000
      000000000000000000000000000000000000D6BDB500FFFFFF00FFEFD600FFEF
      D600FFEFD600FFEFD600FFEFD600FFEFD600FFEFD600FFEFD600FFEFD600FFEF
      D600FFEFD600FFEFD600FFF7DE00B58484000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000ADCA7000CDBA6000BDB
      A6000BDBA6000DDAA5000FDAA5000EDAA5000EDAA5000FDAA5000CDBA5000BDB
      A6000CDBA6000CDBA6000CDEA900000000000000000000000000000000000000
      00001267E8001165E6001166E7001065E6001065E6001266E7001165E6001267
      E80000000000000000000000000000000000D6BDB500FFFFFF00E77B2100E77B
      2100E77B2100E77B2100E77B2100E77B2100E77B2100E77B2100E77B2100E77B
      2100E77B2100E77B2100FFF7DE00B58484000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000CDDA8000FDDA8000FDDA8000DDDA800000000000000
      000000000000000000000000000000000000000000000000000000000000146B
      EB001369E9001369E9001268E9001B6FEB001B6FEB001267E9001469E9001469
      E900146BEB00000000000000000000000000E7C6B500FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFEFD600FFEFD600FFEFD600FFEFD600FFEFD600FFEFD600FFFF
      FF00FFFFFF00FFFFFF00FFFFEF00B58484000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000EE1AC000FDFAB000FE0AB000FE2AD00000000000000
      00000000000000000000000000000000000000000000000000001A73F000146B
      EB00156CEB00146BEB00186FEE000000000000000000176FED00146BEB00166C
      EB00146AEB001A72EF000000000000000000E7C6B500FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00E77B2100E77B2100E77B2100E77B2100E77B2100E77B2100FFFF
      FF00FFFFFF00FFFFFF00FFFFEF00B58484000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000FE3AF0010E2AE000FE2AD0010E4B000000000000000
      0000000000000000000000000000000000000000000000000000000000001C75
      F100146CEC001D74F000000000000000000000000000000000001C74F000146C
      ED001C74F000000000000000000000000000E7C6B500FFFFFF00FFEFD600FFEF
      D600FFEFD600FFEFD600FFEFD600FFEFD600FFEFD600FFEFD600FFEFD600FFEF
      D600FFEFD600FFEFD600FFFFEF00B58484000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000EE5B10010E4B0000FE4B00011E6B300000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000267DF400000000000000000000000000000000000000000000000000267D
      F40000000000000000000000000000000000E7C6B500FFFFFF00E77B2100E77B
      2100E77B2100E77B2100E77B2100E77B2100E77B2100E77B2100E77B2100E77B
      2100E77B2100E77B2100FFFFEF00B58484000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000CE9B4000EE7B3000EE7B3000FEAB600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E7C6B500FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFEF00C68473000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000E7C6B500E7C6B500E7C6
      B500E7C6B500E7C6B500E7C6B500D6BDB500D6BDB500D6B5AD00D6B5AD00C6AD
      A500C6ADA500C6A59C00C6A59C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00F81FFFFF80010000F81FF7EF00000000
      F81FE3C700000000F81FC18300000000F81F8001000000000000C00100000000
      0000E003000000000000F007000000000000F00F000000000000E00700000000
      0000C00300000000F81F800100000000F81FC18300000000F81FE3C700000000
      F81FF7EF00000000F81FFFFF8001000000000000000000000000000000000000
      000000000000}
  end
end
