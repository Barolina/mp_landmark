object frmTypeParcel: TfrmTypeParcel
  Left = 0
  Top = 0
  BorderIcons = []
  BorderStyle = bsToolWindow
  Caption = #1042#1099#1073#1086#1088'...'
  ClientHeight = 103
  ClientWidth = 425
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 81
    Width = 425
    Height = 22
    Align = alBottom
    BevelEdges = []
    BevelOuter = bvNone
    Color = clWhite
    ParentBackground = False
    TabOrder = 0
    object OKBtn: TButton
      Left = 350
      Top = 0
      Width = 75
      Height = 22
      Align = alRight
      Caption = 'OK'
      Default = True
      ModalResult = 1
      TabOrder = 0
    end
  end
  object RadioButton1: TRadioButton
    Left = 160
    Top = 32
    Width = 113
    Height = 17
    Caption = 'RadioButton1'
    TabOrder = 1
  end
  object PanelRadioGroupEntityContours: TPanel
    Left = 0
    Top = 0
    Width = 425
    Height = 81
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    object TypeLandRadioGr: TRadioGroup
      Left = 0
      Top = 0
      Width = 425
      Height = 81
      Align = alClient
      Color = clWhite
      ItemIndex = 0
      Items.Strings = (
        #1054#1087#1080#1089#1072#1085#1080#1077' '#1084#1077#1089#1090#1086#1087#1086#1083#1086#1078#1077#1085#1080#1103' '#1075#1088#1072#1085#1080#1094' ('#1095#1072#1089#1090#1100' '#1091#1095#1072#1089#1090#1082#1072' '#1080#1084#1077#1077#1090' '#1086#1076#1080#1085' '#1082#1086#1085#1090#1091#1088')'
        #1050#1086#1085#1090#1091#1088#1099' '#1084#1085#1086#1075#1086#1082#1086#1085#1090#1091#1088#1085#1086#1081' '#1095#1072#1089#1090#1080)
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      WordWrap = True
    end
  end
end
