unit frMeans_Surveys;
    {
    �������� � ��������� ���������
    IXMLSTD_MP_Input_Data_Means_Survey
    }
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ButtonGroup, StdCtrls, ActnList,
  Vcl.ImgList, Vcl.ExtCtrls,
  STD_MP, Vcl.Grids,
  Vcl.Menus;

const cnstWName =105;
      cnstWCertificate = 351;
      cnstWCertificate_Vertification = 265;  
      cnstWN = 35;
      cnstColCount = 4;
      cnstNN = 0;
      cnstNName= 3;
      cnstNCertification = 2;
      cnstNCertifi_Vertifi =1;
  
type
  THackGrid = class(TStringGrid);

type
  TFrameMeans_Surveys = class(TFrame)
    Panel: TPanel;
    stringGridDate: TStringGrid;
    poMenuTable: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    pnlLabelTabl: TPanel;
    lblCertificate_Verification: TLabel;
    lblName: TLabel;
    lblCertificate: TLabel;
    spl1: TSplitter;
    spl2: TSplitter;
    spl3: TSplitter;
    lblN: TLabel;
    spl4: TSplitter;
    procedure SubParcels_Actions_ButtonGroupItems0Click(Sender: TObject);
    procedure stringGridDateSetEditText(Sender: TObject; ACol, ARow: Integer;
      const Value: string);
    procedure stringGridDateKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SubParcels_Actions_ButtonGroupItems1Click(Sender: TObject);
    procedure spl3Moved(Sender: TObject);
    procedure spl2Moved(Sender: TObject);
    procedure spl1Moved(Sender: TObject);
    procedure stringGridDateDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
  private
    { Private declarations }
   FXmlMeans_Surveys: IXMLSTD_MP_Input_Data_Means_Survey;
   FXMLMeans_Survey : IXMLSTD_MP_Input_Data_Means_Survey_Means_Survey;

   procedure UI_ReadOf(const aMeans_Survey: IXMLSTD_MP_Input_Data_Means_Survey);
   procedure Data_Means_Survey_DoAddNew;
   procedure Data_Means_Survey_DoDelete;

  protected
    procedure SetMeans_Surveys(const aMeans_Surveys: IXMLSTD_MP_Input_Data_Means_Survey);
    procedure InitializationUI;

  public
    { Public declarations }
   procedure AddRowStrGrid();
   procedure DelRowStrGrid(aRow : integer);
    
   constructor Create(Owner: TComponent); override;
   function   IsAssigned_Means_Surveys: Boolean;

   property XmlMeans_Surveys: IXMLSTD_MP_Input_Data_Means_Survey read FXmlMeans_Surveys write SetMeans_Surveys;
  end;

implementation


{$R *.dfm}

{ TFrameMeans_Surveys }

procedure TFrameMeans_Surveys.AddRowStrGrid;
var rowCount  : integer;
begin
  rowCount := stringGridDate.RowCount;
  StringGridDate.RowCount := rowCount +1;
  stringGridDate.Cells[0,rowCount] := IntToStr(rowCount+1);
end;

constructor TFrameMeans_Surveys.Create(Owner: TComponent);
begin
  inherited;
  FXmlMeans_Surveys := nil;
  FXMLMeans_Survey :=  nil;
  {setting ^)}
  stringGridDate.ColCount := cnstColCount;
  stringGridDate.ColWidths[cnstNN] := cnstWN; 
  stringGridDate.ColWidths[cnstNName] :=cnstWName; 
  stringGridDate.ColWidths[cnstNCertification] :=cnstWCertificate; 
  stringGridDate.ColWidths[cnstNCertifi_Vertifi] :=cnstWCertificate_Vertification; 
  lblN.Width := cnstWN;
  lblName.Width := cnstWName;
  lblCertificate.Width := cnstWCertificate; 
  lblCertificate_Verification.Width := cnstWCertificate_Vertification;

  self.InitializationUI;
end;

procedure TFrameMeans_Surveys.Data_Means_Survey_DoAddNew;
begin
  if not self.IsAssigned_Means_Surveys then EXIT;
  AddRowStrGrid;
  FXMLMeans_Survey := FXmlMeans_Surveys.Add;
end;

procedure TFrameMeans_Surveys.Data_Means_Survey_DoDelete;
var FArow : integer;
begin
   FArow := StringGridDate.Row;
   if (MessageDlg('������� ������ � '+IntToStr(FArow+1)+'?', mtInformation, mbYesNo,-1) = mrYes )
      and (FArow >=0) and (FXmlMeans_Surveys.Count > FArow) then
   begin
     FXMLMeans_Survey := FXmlMeans_Surveys.Means_Survey[FArow];
     FXmlMeans_Surveys.Remove(FXMLMeans_Survey);
     FXMLMeans_Survey := nil;
     DelRowStrGrid(FArow);
   end;
end;

procedure TFrameMeans_Surveys.DelRowStrGrid(aRow: integer);
begin
  if aRow  < 0  then Exit;
  stringGridDate.Rows[aRow].Clear;
  if aRow > 0 then THackGrid(stringGridDate).DeleteRow(aRow);
end;

procedure TFrameMeans_Surveys.InitializationUI;
begin
  stringGridDate.RowCount := 1;
  THackGrid(stringGridDate).Rows[0].Clear;
  stringGridDate.Cells[cnstNN,0] := '1';
  if  IsAssigned_Means_Surveys then   UI_ReadOf(FXmlMeans_Surveys);
end;

function TFrameMeans_Surveys.IsAssigned_Means_Surveys: Boolean;
begin
Result := FXmlMeans_Surveys <> nil;
end;

procedure TFrameMeans_Surveys.SetMeans_Surveys(const aMeans_Surveys: IXMLSTD_MP_Input_Data_Means_Survey);
begin
 if FXmlMeans_Surveys = aMeans_Surveys then Exit;
 FXmlMeans_Surveys := aMeans_Surveys;
 InitializationUI;
end;


procedure TFrameMeans_Surveys.spl1Moved(Sender: TObject);
begin
  self.stringGridDate.ColWidths[cnstNCertifi_Vertifi] := self.lblCertificate_Verification.Width;
end;

procedure TFrameMeans_Surveys.spl2Moved(Sender: TObject);
begin
  self.stringGridDate.ColWidths[cnstNCertification] := self.lblCertificate.Width;
end;

procedure TFrameMeans_Surveys.spl3Moved(Sender: TObject);
begin
  self.stringGridDate.ColWidths[cnstNName] := lblName.Width;
end;

procedure TFrameMeans_Surveys.stringGridDateDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
var s:string;
 Flag: Cardinal;
 H: integer;
begin
 stringGridDate.Canvas.FillRect(Rect);
 s := stringGridDate.Cells[ACol,ARow];
 Case Acol mod 3 of
   0: Flag := DT_LEFT;
   1: Flag := DT_CENTER;
 else
   Flag := DT_RIGHT;
 end;
 Flag := Flag or DT_WORDBREAK;
 Inc(Rect.Left,3);
 Dec(Rect.Right,3);
 H := DrawText(stringGridDate.Canvas.Handle,PChar(s),length(s),Rect,Flag);
 if H > stringGridDate.RowHeights[ARow] then
    stringGridDate.RowHeights[ARow] := H;  //�����������
end;

procedure TFrameMeans_Surveys.stringGridDateKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var i : integer;
begin
  if (Key = VK_DELETE) and (ssCtrl in Shift) then Data_Means_Survey_DoDelete;
  if (Key = VK_INSERT) and (ssCtrl in Shift )then Data_Means_Survey_DoAddNew;
end;

procedure  TFrameMeans_Surveys.stringGridDateSetEditText(Sender: TObject; ACol,
  ARow: Integer; const Value: string);
begin
  if not IsAssigned_Means_Surveys then Exit;
  if Value = '' then Exit;
  
  if ARow  >= 0 then
  begin
    if FXmlMeans_Surveys.Count > ARow then FXMLMeans_Survey := FXmlMeans_Surveys.Means_Survey[ARow]
    else FXMLMeans_Survey := FXmlMeans_Surveys.Add;
    case ACol of
     cnstNName  :   FXMLMeans_Survey.Name := Value;
     cnstNCertification :   FXMLMeans_Survey.Certificate := Value;
     cnstNCertifi_Vertifi  :   FXMLMeans_Survey.Certificate_Verification := Value;
    end;
  end;
end;

procedure TFrameMeans_Surveys.SubParcels_Actions_ButtonGroupItems0Click(
  Sender: TObject);
begin
 Data_Means_Survey_DoAddNew;
end;

procedure TFrameMeans_Surveys.SubParcels_Actions_ButtonGroupItems1Click(
  Sender: TObject);
begin
 Data_Means_Survey_DoDelete
end;

procedure TFrameMeans_Surveys.UI_ReadOf(
  const aMeans_Survey: IXMLSTD_MP_Input_Data_Means_Survey);
var  i  : integer;
begin
  StringGridDate.RowCount := aMeans_Survey.Count;
  for I := 0 to aMeans_Survey.Count -1 do
  begin
      FXMLMeans_Survey := aMeans_Survey.Means_Survey[i];
      StringGridDate.Cells[cnstNName,i] := FXMLMeans_Survey.Name;
      StringGridDate.Cells[cnstNCertification,i] := FXMLMeans_Survey.Certificate;
      StringGridDate.Cells[cnstNCertifi_Vertifi,i] := FXMLMeans_Survey.Certificate_Verification;
      StringGridDate.Cells[cnstNN,i] := IntToStr(i+1);
  end;
  FXMLMeans_Survey := nil;
end;


end.
