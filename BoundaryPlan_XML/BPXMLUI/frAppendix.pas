unit frAppendix;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, StdCtrls, ActnList, ButtonGroup, ExtCtrls,

  frContour,STD_MP, BpXML_different, ImgList, BPCommon,frAttributesSTD_MP,
  System.Actions, frDocument;

type
  TFrameAppendix = class(TFrame)
    PanelNodalPointSchemes_List: TPanel;
    NodalPointSchemes_List_ButtonGroup: TButtonGroup;
    ActionList: TActionList;
    Documents_List_ListBox: TListBox;
    Action_Contrours_AddNew: TAction;
    Action_NodalPointSchemes_DeleteCurrent: TAction;
    Action_NodalPointSchemes_SetCurrent: TAction;
    ImageList: TImageList;
    Label1: TLabel;

    procedure Action_Contrours_AddNewExecute(Sender: TObject);
    procedure Action_NodalPointSchemes_DeleteCurrentExecute(Sender: TObject);
    procedure Action_NodalPointSchemes_SetCurrentExecute(Sender: TObject);
  private
   { Private declarations }
     FDocument_frame: TFrameDocument;

     FXmlAppendix: IXMLSTD_MP_Appendix;

     procedure AppendixListDoSetCurrentItem(indexItem : integer);
     procedure AppendixLIstUpdateDoUpdate;
     procedure AppendixListDoAddItem;
     procedure AppendixListDoDeleteItem(indexItem : integer);

  protected
     procedure InitializationUI;
     procedure SetAppendix(const aAppendix: IXMLSTD_MP_Appendix);
     procedure AppendixSetCurrent(index : integer);
  public
    { Public declarations }
     constructor Create(Owner: TComponent); override;
     function IsAssignedAppendix: Boolean;
     property Documents: TFrameDocument read FDocument_frame;
     property XmlAppendix: IXMLSTD_MP_Appendix read FXmlAppendix write SetAppendix;
  end;

implementation

uses MsXMLAPI;

{$R *.dfm}

{ TFrameNodalPointSchemes }

procedure TFrameAppendix.Action_NodalPointSchemes_DeleteCurrentExecute(Sender: TObject);
begin
 if not IsAssignedAppendix then EXIT;
 if self.Documents_List_ListBox.ItemIndex=-1 then EXIT;

 self.AppendixListDoDeleteItem(self.Documents_List_ListBox.ItemIndex);
 self.AppendixSetCurrent(self.Documents_List_ListBox.ItemIndex);
 if not self.FDocument_frame.IsAssignedDocument then
  self.AppendixSetCurrent(0);
end;

procedure TFrameAppendix.Action_NodalPointSchemes_SetCurrentExecute(Sender: TObject);
begin
 self.AppendixSetCurrent(self.Documents_List_ListBox.ItemIndex);
end;

procedure TFrameAppendix.AppendixListDoAddItem;
var document : IXMLTDocument;
    itemName : string;
begin
   document := FXmlAppendix.Add;
   itemName := inttostr(FXmlAppendix.Count);
   Documents_List_ListBox.AddItem(itemName, pointer(document));
end;

procedure TFrameAppendix.AppendixListDoDeleteItem(indexItem: integer);
var document : IXMLTDocument;
begin
  document := IXMLTDocument(pointer(self.Documents_List_ListBox.Items.Objects[indexItem]));
  if FDocument_frame.XMlDOcument = document then
      AppendixListDoSetCurrentItem(indexItem-1);
  Documents_List_ListBox.Items.Delete(indexItem);
  FXmlAppendix.Remove(document);
  document := nil;
end;

procedure TFrameAppendix.AppendixListDoSetCurrentItem(indexItem: integer);
var
 document : IXMLTDocument;
begin
 document := IXMLTDocument(Pointer(Documents_List_ListBox.Items.Objects[indexItem]));
 self.FDocument_frame.XMlDOcument := document;
end;

procedure TFrameAppendix.AppendixLIstUpdateDoUpdate;
var
  i,index : integer;
  document : IXMLTDocument;
begin
   index := Documents_List_ListBox.ItemIndex;
   Documents_List_ListBox.Clear;
   for i := 0 to FXmlAppendix.Count-1 do
   begin
      document := FXmlAppendix.Document[i];
      Documents_List_ListBox.AddItem(inttostr(i+1),pointer(document));
   end;
   self.AppendixSetCurrent(index);
   if not FDocument_frame.IsAssignedDocument then    self.AppendixSetCurrent(0);
end;

procedure TFrameAppendix.AppendixSetCurrent(index: integer);
begin
 if (index>-1) and (index<self.Documents_List_ListBox.Count) then begin
  self.AppendixListDoSetCurrentItem(index);
  self.Documents_List_ListBox.ItemIndex:= index;
  self.FDocument_frame.Visible:= True;
 end else begin
  self.FDocument_frame.Visible:= false;
  self.Documents_List_ListBox.ItemIndex:= -1;
  self.FDocument_frame.XMlDOcument:= nil;
 end;

end;

procedure TFrameAppendix.Action_Contrours_AddNewExecute(Sender: TObject);
begin
 if not self.IsAssignedAppendix then EXIT;
 self.AppendixListDoAddItem;
 self.AppendixSetCurrent(self.Documents_List_ListBox.Count-1);
end;


constructor TFrameAppendix.Create(Owner: TComponent);
begin
 inherited;
 self.FDocument_frame:= TFrameDocument.Create(self);
 self.FDocument_frame.Parent:= self;
 self.FDocument_frame.Align:= alClient;
 self.FDocument_frame.Visible:= False;
end;

procedure TFrameAppendix.InitializationUI;
begin
 self.Documents_List_ListBox.Clear;
 if self.IsAssignedAppendix then begin
  self.AppendixLIstUpdateDoUpdate;
  self.AppendixSetCurrent(0);
 end;
end;

function TFrameAppendix.IsAssignedAppendix: Boolean;
begin
 Result:= self.FXmlAppendix<>nil;
end;

procedure TFrameAppendix.SetAppendix(const aAppendix: IXMLSTD_MP_Appendix);
begin
 if self.FXmlAppendix=aAppendix then EXIT;
 self.AppendixSetCurrent(-1);
 self.FXmlAppendix:= aAppendix;
 self.InitializationUI;
end;

end.
