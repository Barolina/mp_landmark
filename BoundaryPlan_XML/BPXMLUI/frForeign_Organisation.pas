unit frForeign_Organisation;
{

  ����������� ����������� ����
  IXMLSTD_MP_Title_Client_Foreign_Organization
}
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls,

  STD_MP, frAgent, XMLIntf,unRussLang;

type
  TFrameForeign_Organisation = class(TFrame)
    Label_Name: TLabel;
    pnlAgent: TPanel;
    mmoName: TMemo;

    procedure Name_EditExit(Sender: TObject);
    procedure Name_EditKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
    FXmlForeign_Organisation : IXMLSTD_MP_Title_Client_Foreign_Organization;
    FAgent                   : TFrameAgent;
    function IsAssignedForeign_Organisation: boolean;

 protected
   procedure InitializationUI;
   procedure SeTitle_ForeignOrganisation(const aOrganisation: IXMLSTD_MP_Title_Client_Foreign_Organization);
  public
   { Public declarations }
   constructor Create(AOwner: TComponent); override;

   procedure UI_Name_ReadOf(const aForeign_Organisation: IXMLSTD_MP_Title_Client_Foreign_Organization);
   procedure UI_ReadOf(const aForeign_Organisation: IXMLSTD_MP_Title_Client_Foreign_Organization);

   procedure UI_Name_WriteTo(const aForeign_Organisation: IXMLSTD_MP_Title_Client_Foreign_Organization);
   procedure UI_WriteTo(const aForeign_Organisation: IXMLSTD_MP_Title_Client_Foreign_Organization);

   property XMLTitleOrganisation:IXMLSTD_MP_Title_Client_Foreign_Organization read FXmlForeign_Organisation write SeTitle_ForeignOrganisation;
  end;

implementation

uses MsXmlApi;
{$R *.dfm}


{ TFrameOrgabisation }


constructor TFrameForeign_Organisation.Create(AOwner: TComponent);
begin
  inherited;
  self.FAgent:= TFrameAgent.Create(self.pnlAgent);
  self.FAgent.Parent:= self.pnlAgent;
  self.FAgent.Align:= alClient;
end;

procedure TFrameForeign_Organisation.InitializationUI;
begin
 if self.IsAssignedForeign_Organisation then
 begin
  self.UI_ReadOf(self.FXmlForeign_Organisation);
  self.FAgent.XMLAgent := FXmlForeign_Organisation.Agent;
 end
 else begin
  self.mmoName.Clear;
  FAgent.XMLAgent := nil;
 end;
end;

procedure TFrameForeign_Organisation.Name_EditExit(Sender: TObject);
begin
 if not self.IsAssignedForeign_Organisation then raise Exception.Create(ExMSG_NotAssignedROOT);
 self.UI_Name_WriteTo(self.FXmlForeign_Organisation);
end;

procedure TFrameForeign_Organisation.Name_EditKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
{TODO:26 if key = VK_RETURN then
  if (Sender is TCustomEdit) then
   TCustomEdit(Sender).Parent.SetFocus;}
end;

function TFrameForeign_Organisation.IsAssignedForeign_Organisation: boolean;
begin
 Result:= self.FXmlForeign_Organisation<>nil;
end;

procedure TFrameForeign_Organisation.SeTitle_ForeignOrganisation(const aOrganisation: IXMLSTD_MP_Title_Client_Foreign_Organization);
begin
 if self.FXmlForeign_Organisation=aOrganisation then EXIT;
 self.FXmlForeign_Organisation:= aOrganisation;
 self.InitializationUI;
end;

procedure TFrameForeign_Organisation.UI_Name_ReadOf(
  const aForeign_Organisation: IXMLSTD_MP_Title_Client_Foreign_Organization);
begin
 mmoName.Lines.Add(MsXml_ReadChildNodeValue(aForeign_Organisation,'Name'));
end;

procedure TFrameForeign_Organisation.UI_Name_WriteTo;
var
 _st: string;
begin
 _st:= self.mmoName.Text;
 if _st<>'' then
  aForeign_Organisation.Name:= _st
 else
  MsXml_RemoveChildNode(aForeign_Organisation,'Name');
end;


procedure TFrameForeign_Organisation.UI_ReadOf;
begin
 self.UI_Name_ReadOf(aForeign_Organisation);
end;

procedure TFrameForeign_Organisation.UI_WriteTo;
begin
 self.UI_WriteTo(aForeign_Organisation);
end;

end.
