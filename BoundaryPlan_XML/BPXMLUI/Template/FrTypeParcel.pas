unit FrTypeParcel;

{
  Asked type
  Entity_Spatial  or Contours
}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, ButtonGroup, StdCtrls, ActnList, Vcl.ExtCtrls;


type
  TfrmTypeParcel = class(TForm)
    Panel1: TPanel;
    OKBtn: TButton;
    RadioButton1: TRadioButton;
    PanelRadioGroupEntityContours: TPanel;
    TypeLandRadioGr: TRadioGroup;
  private
    { Private declarations }
  public
    { Public declarations }
    function GetTypeObject  : integer;
  end;

var
  frmTypeParcel: TfrmTypeParcel;

implementation

{$R *.dfm}

{ TfrmTypeParcel }

function TfrmTypeParcel.GetTypeObject: integer;
begin
   Result := 0; {�� ���������  EntitySpatial}
   if not Assigned(FrTypeParcel.frmTypeParcel) then Application.CreateForm(TfrmTypeParcel,FrTypeParcel.frmTypeParcel);
   FrTypeParcel.frmTypeParcel.ShowModal;
   if FrTypeParcel.frmTypeParcel.ModalResult = mrOk then
      Result := FrTypeParcel.frmTypeParcel.TypeLandRadioGr.ItemIndex;
   FrTypeParcel.frmTypeParcel.Close;
end;

end.
