object FrameDeleteEntryParcels: TFrameDeleteEntryParcels
  Left = 0
  Top = 0
  Width = 451
  Height = 304
  Margins.Left = 0
  Margins.Top = 0
  Margins.Right = 0
  Margins.Bottom = 0
  Align = alClient
  ParentBackground = False
  TabOrder = 0
  object pnlDeleteEntryParcels: TPanel
    Left = 0
    Top = 16
    Width = 451
    Height = 288
    Align = alClient
    TabOrder = 0
    object btngpActionAddDel: TButtonGroup
      Left = 1
      Top = 22
      Width = 160
      Height = 265
      Align = alLeft
      ButtonOptions = [gboFullSize, gboShowCaptions]
      Images = formContainer.ilCommands
      Items = <
        item
          Action = Action_DoAdd
        end
        item
          Action = Action_DelSel
        end>
      TabOrder = 0
    end
    object lstDeleteEntryParcelsList_ListBox: TListBox
      Left = 161
      Top = 22
      Width = 289
      Height = 265
      Align = alClient
      Color = 15921123
      ItemHeight = 13
      MultiSelect = True
      TabOrder = 1
    end
    object medtDeleteparcel: TMaskEdit
      Left = 1
      Top = 1
      Width = 449
      Height = 21
      Hint = #1050#1072#1076#1072#1089#1090#1088#1086#1074#1099#1081' '#1085#1086#1084#1077#1088' '#1082#1074#1072#1088#1090#1072#1083#1072'  (12 - 13 '#1089#1080#1084#1074#1086#1083#1086#1074')'
      Align = alTop
      Color = 15921123
      TabOrder = 2
      Text = ''
      Visible = False
    end
  end
  object chkIsFill_DeleteEntryParcels_CheckBox: TCheckBox
    Left = 0
    Top = 0
    Width = 451
    Height = 16
    Align = alTop
    Caption = 
      #1057#1087#1080#1089#1086#1082' '#1079#1077#1084#1077#1083#1100#1085#1099#1093' '#1091#1095#1072#1089#1090#1086#1074' '#1080#1089#1082#1083#1102#1095#1072#1077#1084#1099#1093' '#1080#1079' '#1089#1086#1089#1090#1072#1074#1072' '#1077#1076#1080#1085#1086#1075#1086' '#1079#1077#1084#1083#1077#1087#1086#1083 +
      #1100#1079#1086#1074#1072#1085#1080#1103
    Color = 13171400
    DoubleBuffered = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentColor = False
    ParentDoubleBuffered = False
    ParentFont = False
    TabOrder = 1
    OnClick = chkIsFill_DeleteEntryParcels_CheckBoxClick
  end
  object ActionList: TActionList
    Images = formContainer.ilCommands
    Left = 392
    Top = 114
    object Action_DoAdd: TAction
      Caption = 'Action_ExistEntryParcel_DoAdd'
      ImageIndex = 0
      OnExecute = Action_DoAddExecute
    end
    object Action_DelSel: TAction
      Caption = 'Action_ExistEntryParcel_DelSel'
      ImageIndex = 1
      OnExecute = Action_DelSelExecute
    end
  end
end
