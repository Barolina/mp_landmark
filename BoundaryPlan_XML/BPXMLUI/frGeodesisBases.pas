unit frGeodesisBases;
    {
    �������� � ��������� ���������
    IXMLSTD_MP_Input_Data_Geodesis_Bases
    }
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ButtonGroup, StdCtrls, ActnList,
  Vcl.Menus, Vcl.ImgList,
  Vcl.ExtCtrls,
  STD_MP, Vcl.Grids;

const cnstWName =125;
      cnstWPKInd = 191;
      cnstWPKlass = 163;  
      cnstWPX = 115;
      cnstWPY = 115;
      cnstWN = 35;
      
      cnstColCount = 6;
      cnstNN = 0;
      cnstNName= 1;
      cnstNKind = 2;
      cnstNKlass =3;
      cnstNPX = 4;
      cnstNPY = 5;
  
type
  THackGrid = class(TStringGrid);

type
  TFrameGeodesisBases = class(TFrame)
    Panel: TPanel;
    stringGridDate: TStringGrid;
    poMenuTable: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    pnlLabelTabl: TPanel;
    lblPName: TLabel;
    lblPY: TLabel;
    lblPKind: TLabel;
    spl1: TSplitter;
    spl2: TSplitter;
    spl3: TSplitter;
    lblN: TLabel;
    spl4: TSplitter;
    lblPKlass: TLabel;
    lblPX: TLabel;
    spl5: TSplitter;
    procedure SubParcels_Actions_ButtonGroupItems0Click(Sender: TObject);
    procedure stringGridDateSetEditText(Sender: TObject; ACol, ARow: Integer;
      const Value: string);
    procedure stringGridDateKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SubParcels_Actions_ButtonGroupItems1Click(Sender: TObject);
    procedure stringGridDateKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
   FXmlGeodesis_Bases:  IXMLSTD_MP_Input_Data_Geodesic_Bases;
   FXMLGeodesis_Base :  IXMLSTD_MP_Input_Data_Geodesic_Bases_Geodesic_Base;

   procedure UI_ReadOf(const aGeodesis_Base: IXMLSTD_MP_Input_Data_Geodesic_Bases);
   procedure Data_Geodesis_Base_DoAddNew;
   procedure Data_Geodesis_Base_DoDelete;

  protected
    procedure SetGeodesis_Bases(const aGeodesis_Bases: IXMLSTD_MP_Input_Data_Geodesic_Bases);
    procedure InitializationUI;

  public
    { Public declarations }
   procedure AddRowStrGrid();
   procedure DelRowStrGrid(aRow : integer);
    
   constructor Create(Owner: TComponent); override;
   function   IsAssigned_Geodesis_Bases: Boolean;

   property XmlGeodesis_Bases: IXMLSTD_MP_Input_Data_Geodesic_Bases read FXmlGeodesis_Bases write SetGeodesis_Bases;
  end;

implementation


{$R *.dfm}

{ TFrameGeodesis_Bases }

procedure TFrameGeodesisBases.AddRowStrGrid;
var rowCount  : integer;
begin
  rowCount := stringGridDate.RowCount;
  StringGridDate.RowCount := rowCount +1;
  stringGridDate.Cells[0,rowCount] := IntToStr(rowCount+1);
end;

constructor TFrameGeodesisBases.Create(Owner: TComponent);
begin
  inherited;
  FXmlGeodesis_Bases := nil;
  FXMLGeodesis_Base :=  nil;
  {setting ^)}
  stringGridDate.ColCount := cnstColCount;
  stringGridDate.ColWidths[cnstNN] := cnstWN; 
  stringGridDate.ColWidths[cnstNName] :=cnstWName; 
  stringGridDate.ColWidths[cnstNKind] :=cnstWPKInd; 
  stringGridDate.ColWidths[cnstNKlass] :=cnstWPKlass; 
  stringGridDate.ColWidths[cnstNPX] :=cnstWPX; 
  stringGridDate.ColWidths[cnstNPY] :=cnstWPY;   
  lblN.Width := cnstWN;
  lblPName.Width := cnstWName;
  lblPKind.Width := cnstWPKInd; 
  lblPKlass.Width := cnstWPKlass;
  lblPX.Width := cnstWPX;
  lblPY.Width := cnstWPY;

  self.InitializationUI;
end;


procedure TFrameGeodesisBases.Data_Geodesis_Base_DoAddNew;
begin
  if not self.IsAssigned_Geodesis_Bases then EXIT;
  AddRowStrGrid;
  FXMLGeodesis_Base := FXmlGeodesis_Bases.Add;
end;

procedure TFrameGeodesisBases.Data_Geodesis_Base_DoDelete;
var FArow : integer;
begin
   FArow := StringGridDate.Row;
   if (MessageDlg('������� ������ � '+IntToStr(FArow+1)+'?', mtInformation, mbYesNo,-1) = mrYes )
      and (FArow >=0) and (FXmlGeodesis_Bases.Count > FArow) then
   begin
     FXMLGeodesis_Base := FXmlGeodesis_Bases.Geodesic_Base[FArow];
     FXmlGeodesis_Bases.Remove(FXMLGeodesis_Base);
     FXMLGeodesis_Base := nil;
     DelRowStrGrid(FArow);
   end;
end;

procedure TFrameGeodesisBases.DelRowStrGrid(aRow: integer);
begin
  if aRow <0  then Exit;
   THackGrid(stringGridDate).Rows[aRow].Clear;
  if aRow > 0 then THackGrid(stringGridDate).DeleteRow(aRow);
end;

procedure TFrameGeodesisBases.InitializationUI;
begin
  stringGridDate.RowCount := 1;
  THackGrid(stringGridDate).Rows[0].Clear;
  stringGridDate.Cells[cnstNN,0] := '1';
  if  IsAssigned_Geodesis_Bases then   UI_ReadOf(FXmlGeodesis_Bases);
end;

function TFrameGeodesisBases.IsAssigned_Geodesis_Bases: Boolean;
begin
Result := FXmlGeodesis_Bases <> nil;
end;

procedure TFrameGeodesisBases.SetGeodesis_Bases(
  const aGeodesis_Bases: IXMLSTD_MP_Input_Data_Geodesic_Bases);
begin
 if FXmlGeodesis_Bases = aGeodesis_Bases then Exit;
 FXmlGeodesis_Bases := aGeodesis_Bases;
 InitializationUI;
end;

procedure TFrameGeodesisBases.stringGridDateKeyPress(Sender: TObject; var Key: Char);
begin
  case stringGridDate.Col of
   cnstNPX,cnstNPY :  if not (key in ['0'..'9',',','.']) then key := #0;
  end;
end;

procedure TFrameGeodesisBases.stringGridDateKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var i : integer;
begin
  if (Key = VK_DELETE) and (ssCtrl in Shift) then Data_Geodesis_Base_DoDelete;
  if (Key = VK_INSERT) and (ssCtrl in Shift )then Data_Geodesis_Base_DoAddNew;
end;

procedure  TFrameGeodesisBases.stringGridDateSetEditText(Sender: TObject; ACol,
  ARow: Integer; const Value: string);
begin
  if not IsAssigned_Geodesis_Bases then Exit;
  if Value = '' then Exit;
  
  if ARow  >= 0 then
  begin
    if FXmlGeodesis_Bases.Count > ARow then FXMLGeodesis_Base := FXmlGeodesis_Bases.Geodesic_Base[ARow]
    else FXmlGeodesis_Base := FXmlGeodesis_Bases.Add;
    case ACol of
     cnstNName  : FXMLGeodesis_Base.PName := Value;
     cnstNKind  : FXMLGeodesis_Base.PKind := Value;
     cnstNKlass : FXMLGeodesis_Base.PKlass := Value;
     cnstNPX    : FXMLGeodesis_Base.OrdX := Value;
     cnstNPY    : FXMLGeodesis_Base.OrdY := Value;
    end;
  end;
end;

procedure TFrameGeodesisBases.SubParcels_Actions_ButtonGroupItems0Click(
  Sender: TObject);
begin
 Data_Geodesis_Base_DoAddNew;
end;

procedure TFrameGeodesisBases.SubParcels_Actions_ButtonGroupItems1Click(
  Sender: TObject);
begin
 Data_Geodesis_Base_DoDelete
end;

procedure TFrameGeodesisBases.UI_ReadOf(const aGeodesis_Base: IXMLSTD_MP_Input_Data_Geodesic_Bases);
var  i  : integer;
begin
  StringGridDate.RowCount := aGeodesis_Base.Count;
  for I := 0 to aGeodesis_Base.Count -1 do
  begin
      FXMLGeodesis_Base := aGeodesis_Base.Geodesic_Base[i];
      StringGridDate.Cells[cnstNName,i] := FXMLGeodesis_Base.PName;
      StringGridDate.Cells[cnstNKind,i] := FXMLGeodesis_Base.PKind;
      StringGridDate.Cells[cnstNKlass,i] := FXMLGeodesis_Base.PKlass;
      StringGridDate.Cells[cnstNPX,i] := FXMLGeodesis_Base.OrdX;
      StringGridDate.Cells[cnstNPY,i] := FXMLGeodesis_Base.OrdY;
      StringGridDate.Cells[cnstNN,i] := IntToStr(i+1);
  end;
  FXMLGeodesis_Base := nil;
end;


end.
