unit fmBPXML_FormParcels;

interface

uses
  Windows, Messages, SysUtils, Forms, Dialogs, StdCtrls, BPCommon,
  ExtCtrls, ComCtrls, Controls,
  Vcl.Mask, System.Classes, System.Actions, Vcl.ActnList,
  Vcl.PlatformDefaultStyleActnCtrls, Vcl.ActnMan, Vcl.ButtonGroup,
  frNewParcel, fmBpXml_WizardGroupValues,  frSpecifyRelatedsParcels,
  frChangeParcel,
  XMLDoc,XMLIntf,STD_MP,
  BpXML_different, unRussLang,
  JvgPage;

type
  TFormBPXML_FormParcels = class(TForm)
    Panel_ControlNewParcel: TPanel;
    NewParcels_ComboBox: TComboBox;
    ActionManager_FormParcels: TActionManager;
    ButtonGroup1: TButtonGroup;
    Label2: TLabel;

    Action_NewParcels_SetCurrent: TAction;
    ActionXML_NewParcel_AddNew: TAction;
    ActionXML_NewParcel_DelCurrent: TAction;
    ActionCP_AddNew: TAction;
    ActionCP_DelCurrent: TAction;
    ActionCP_SetCurrent: TAction;
    ActionXML_NewParcel_SetCurrentNext: TAction;
    ActionXML_NewParcel_SetCurrentBack: TAction;
    ActionXML_NewParcel_GroupEdit: TAction;
    actEditDeifinition: TAction;
    medtFormParcel_CadastralBlock: TMaskEdit;
    lblCadastralBlock: TLabel;
    cbbFormParcel_Type: TComboBox;
    LabelName: TLabel;
    jvPageControl_FP: TJvgPageControl;
    ts_ChangeParcels: TTabSheet;
    pnlControl_ChangeParcel: TPanel;
    lbl1: TLabel;
    cbbChangeParcelItems_ComboBox: TComboBox;
    btnChangeParcels_ButtonGroup: TButtonGroup;
    ts_NewParcels: TTabSheet;
    ts_SpecifyRelatedParcel: TTabSheet;
    lblFormParcels: TLabel;
    lbl2: TLabel;
    medtChangeParCadBlock: TMaskEdit;

    procedure Action_NewParcels_SetCurrentExecute(Sender: TObject);
    procedure ActionXML_NewParcel_AddNewExecute(Sender: TObject);
    procedure ActionXML_NewParcel_DelCurrentExecute(Sender: TObject);

    procedure ActionCP_AddNewExecute(Sender: TObject);
    procedure ActionCP_DelCurrentExecute(Sender: TObject);
    procedure ActionCP_SetCurrentExecute(Sender: TObject);

    procedure ActionXML_NewParcel_SetCurrentNextExecute(Sender: TObject);
    procedure ActionXML_NewParcel_SetCurrentBackExecute(Sender: TObject);

    procedure ActionXML_NewParcel_GroupEditExecute(Sender: TObject);
    procedure ButtonGroup1Items5Click(Sender: TObject);
    procedure cbbFormParcel_TypeChange(Sender: TObject);
    procedure medtFormParcel_CadastralBlockKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure medtFormParcel_CadastralBlockExit(Sender: TObject);
    procedure medtChangeParCadBlockExit(Sender: TObject);
  private
    FXmlFormParcels         : IXMLSTD_MP_Package_FormParcels; // �������� ����� "����������� ��������"

    FNewParcel_frame        : TFrameNewParcel;
    FChangeParcel_frame     : TFrameChangeParcel;
    FSpecifyRelatedsParcels : TFrameSpecifyRelatedsParcels;

    //����������� ��������� �������
    procedure FormParcels_DoCreateNewParcel;
    procedure FormParcels_DoDelete(index: Integer);
    procedure FormParcels_DoSetCurrentNewParcel(index: Integer);

    procedure FormParcel_doRead_CadastralBlock;
    procedure FormParcel_doWrite_CadastralBlock;
    procedure FormParcel_doRead_Type;
    procedure FormParcel_doWrite_Type;

    //���������� ��������� �������
    procedure ChangeParcel_DoCreateNewItem(CadNum: string);
    procedure ChangeParcel_DoDelete(index: Integer);
    procedure ChangeParcel_DoSetCurrentItem(index: Integer);

    procedure ChangeParcel_doRead_CadastralBlock;
    procedure ChangeParcel_doWrite_CadastralBlock;

    //������� ����� FormParcel
    procedure SetFormParcels(const aFormParcels: IXMLSTD_MP_Package_FormParcels);

    procedure WriteMethodFormed();
    procedure ReadMethodFormed();
    procedure UpdateMethodFormed;
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    function IsAssigned_FormParcels: boolean;

    //������ � ���������� FormParcels - NewParcel-------------------------------
    procedure FormParcels_CreateNewParcel;
    procedure FormParcels_UpDateList;
    procedure FormParcels_Delete(index: Integer);
    procedure FormParcels_SetCurrentNewParcel(index: Integer);
    procedure FormParcels_RefreshList(_A: Integer=-1;_B:Integer=-1);
    //--------------------------------------------------------------------------

    //changeparcel - ���������� ��������� �������-------------------------------
    procedure ChangeParcel_CreateNewItem(CadNum: string);
    procedure ChangeParcel_UpDateList;
    procedure ChangeParcel_Delete(index: Integer);
    procedure ChangeParcel_SetCurrentItem(index: Integer);
    //--------------------------------------------------------------------------


    property XmlFormParcels: IXMLSTD_MP_Package_FormParcels read FXmlFormParcels write SetFormParcels;
  end;

var
  FormBPXML_FormParcels: TFormBPXML_FormParcels;

implementation

uses MsXMLAPI;

{$R *.dfm}

{ TFormBoundaryPlan_XML }

procedure TFormBPXML_FormParcels.ActionCP_AddNewExecute(Sender: TObject);
var
 _CadNum: string;
begin
 _CadNum:= InputBox('���������� ��������� �������.','������� ����������� �����','');
 if _CadNum='' then EXIT;
 self.ChangeParcel_CreateNewItem(_CadNum);
 self.ChangeParcel_SetCurrentItem(self.cbbChangeParcelItems_ComboBox.Items.Count-1);
end;

procedure TFormBPXML_FormParcels.ActionCP_DelCurrentExecute(Sender: TObject);
begin
 self.ChangeParcel_Delete(self.cbbChangeParcelItems_ComboBox.ItemIndex);
end;

procedure TFormBPXML_FormParcels.ActionCP_SetCurrentExecute(Sender: TObject);
begin
 self.ChangeParcel_SetCurrentItem(self.cbbChangeParcelItems_ComboBox.ItemIndex);
end;

procedure TFormBPXML_FormParcels.ActionXML_NewParcel_AddNewExecute(Sender: TObject);
begin
 self.FormParcels_CreateNewParcel;
 self.FormParcels_SetCurrentNewParcel(self.NewParcels_ComboBox.Items.Count-1);
end;

procedure TFormBPXML_FormParcels.ActionXML_NewParcel_DelCurrentExecute(Sender: TObject);
begin
 self.FormParcels_Delete(self.NewParcels_ComboBox.ItemIndex);
end;


procedure TFormBPXML_FormParcels.ActionXML_NewParcel_GroupEditExecute(
  Sender: TObject);
var
 _i,_j: Integer;
 _NewParcel: IXMLTNewParcel;
begin
 if not Assigned(FormBpXml_WizardGroupValues) then
  FormBpXml_WizardGroupValues:= TFormBpXml_WizardGroupValues.Create(nil);

 FormBpXml_WizardGroupValues.ListSource_Clear;
 FormBpXml_WizardGroupValues.ListElements_Clear;
 FormBpXml_WizardGroupValues.SetActivTab(0,false);

 for _i := 0 to self.FXmlFormParcels.NewParcel.Count-1 do begin
  _NewParcel:= self.FXmlFormParcels.NewParcel.Items[_i];
  FormBpXml_WizardGroupValues.ListSource_AddItem(_NewParcel.Definition,_NewParcel);
 end;

 FormBpXml_WizardGroupValues.ListElements_AddItem('����� ������������ ��������','CadastralBlock');
 FormBpXml_WizardGroupValues.ListElements_AddItem('������ ��������� ��������, �� ������� ��������� ������ �������','Prev_CadastralNumbers');
 FormBpXml_WizardGroupValues.ListElements_AddItem('����������� ��� ���� ������ ��� ����������� ������� � �������','Providing_Pass_CadastralNumbers');
 FormBpXml_WizardGroupValues.ListElements_AddItem('����������� ��� ���� ������ �������� ������������ �������������','Inner_CadastralNumbers');
 FormBpXml_WizardGroupValues.ListElements_AddItem('������ �� �������������� �������','Location');
 FormBpXml_WizardGroupValues.ListElements_AddItem('��������� ������ (�� �����������)','Category');
 FormBpXml_WizardGroupValues.ListElements_AddItem('�������������� ������� �������','NaturalObject');
 FormBpXml_WizardGroupValues.ListElements_AddItem('������������� �������','Utilization');
 FormBpXml_WizardGroupValues.ListElements_AddItem('�������� � �� ������������','Encumbrances');

 FormBpXml_WizardGroupValues.ShowModal;
 self.FormParcels_SetCurrentNewParcel(0);
end;

procedure TFormBPXML_FormParcels.ActionXML_NewParcel_SetCurrentBackExecute(
  Sender: TObject);
begin
self.FormParcels_SetCurrentNewParcel(self.NewParcels_ComboBox.ItemIndex-1);
end;

procedure TFormBPXML_FormParcels.ActionXML_NewParcel_SetCurrentNextExecute(
  Sender: TObject);
begin
self.FormParcels_SetCurrentNewParcel(self.NewParcels_ComboBox.ItemIndex+1);
end;

procedure TFormBPXML_FormParcels.Action_NewParcels_SetCurrentExecute(Sender: TObject);
begin
 self.FormParcels_SetCurrentNewParcel(self.NewParcels_ComboBox.ItemIndex);
end;

procedure TFormBPXML_FormParcels.ButtonGroup1Items5Click(Sender: TObject);
var difinition : string;
    currentNewPr : integer;
begin
  if not IsAssigned_FormParcels then exit;
  if NewParcels_ComboBox.ItemIndex < 0  then   Exit;
  
  difinition := InputBox('�������� ������������ �� ', '����������� ��','');
  if difinition = '' then Exit;
  currentNewPr:= NewParcels_ComboBox.ItemIndex;
  self.FXmlFormParcels.NewParcel.Items[NewParcels_ComboBox.ItemIndex].Definition :=  difinition;
  NewParcels_ComboBox.Items[currentNewPr] := difinition;
  self.FormParcels_SetCurrentNewParcel(currentNewPr); 
end;

procedure TFormBPXML_FormParcels.cbbFormParcel_TypeChange(Sender: TObject);
var
  _node: IXMLNode;
  _curType: integer;
  _newType: integer;
begin
 if not self.FNewParcel_frame.IsAssigned_NewParcel then EXIT;
 _curType:= self.FNewParcel_frame.TypeNewParcel;
 _newType:= self.cbbFormParcel_Type.ItemIndex;
 if (_curType<>-1) and (_newType<>_curType) then
  if MessageDlg('��������� ���� ���������� ������� �������� � ������ ����� ��������� ������ '
                +'������������� �������� ��������������.' + sLineBreak +
                '���������� ���������� �������?', mtConfirmation, [mbYes, mbNo], -1) = mrNo then begin
    self.cbbFormParcel_Type.ItemIndex:= _curType;
    EXIT;
  end;
  self.FormParcel_doWrite_Type;
end;

procedure TFormBPXML_FormParcels.ChangeParcel_CreateNewItem(CadNUm: string);
begin
 if not self.IsAssigned_FormParcels then EXIT;
 self.ChangeParcel_DoCreateNewItem(CadNum);
 self.ChangeParcel_DoSetCurrentItem(self.cbbChangeParcelItems_ComboBox.Items.Count-1);
end;

procedure TFormBPXML_FormParcels.ChangeParcel_Delete(index: Integer);
begin
 if (index>-1) and (index<self.cbbChangeParcelItems_ComboBox.Items.Count) then
   if index=self.cbbChangeParcelItems_ComboBox.ItemIndex then begin
    self.ChangeParcel_DoDelete(index);
    self.ChangeParcel_SetCurrentItem(index);
   end else
    self.ChangeParcel_DoDelete(index);
end;

procedure TFormBPXML_FormParcels.ChangeParcel_DoCreateNewItem(CadNum:string);
var
 _ChangeParcel: IXMLTChangeParcel;
 _cNumCv,_cNum: string;
begin
 _cNumCv:= BPcCadNum_GetItemValue(CadNum,[cnDistrict,cnMunicipality,cnBlock]);
 _cNum:= BPcCadNum_GetItemValue(CadNum,[cnNum]);
 if (_cNumCv='') or (_cNum= '') then
  raise Exception.Create('����������� ����� ��������������� �������');

 _ChangeParcel:= self.FXmlFormParcels.ChangeParcel.Add;
 CadNum:= _cNumCv+_DefDelemitersCadNumItem+_cNum;
 _ChangeParcel.CadastralNumber:= CadNum;
 _ChangeParcel.CadastralBlock:= _cNumCv;

 self.cbbChangeParcelItems_ComboBox.AddItem(CadNum, pointer(_ChangeParcel));
end;

procedure TFormBPXML_FormParcels.ChangeParcel_DoDelete(index: Integer);
var
 _ChangeParcel: IXMLTChangeParcel;
begin
 _ChangeParcel:=  IXMLTChangeParcel(Pointer(self.cbbChangeParcelItems_ComboBox.Items.Objects[index]));
 self.FXmlFormParcels.ChangeParcel.Remove(_ChangeParcel);
 self.cbbChangeParcelItems_ComboBox.Items.Delete(index);
 _ChangeParcel:= nil;
end;

procedure TFormBPXML_FormParcels.ChangeParcel_doRead_CadastralBlock;
begin
 if not self.FChangeParcel_frame.IsAssigned_ChangeParcel then EXIT;
 self.medtChangeParCadBlock.Text :=
 MsXml_ReadChildNodeValue(self.FChangeParcel_frame.XmlChangeParcel, cnstXmlCadastralBlock);
end;


procedure TFormBPXML_FormParcels.ChangeParcel_DoSetCurrentItem(index: Integer);
begin
  self.FChangeParcel_frame.XmlChangeParcel:= IXMLTChangeParcel(pointer(self.cbbChangeParcelItems_ComboBox.Items.Objects[index]));
end;

procedure TFormBPXML_FormParcels.ChangeParcel_doWrite_CadastralBlock;
begin
 if not self.FChangeParcel_frame.IsAssigned_ChangeParcel then EXIT;
 self.FChangeParcel_frame.XmlChangeParcel.CadastralBlock:=
 StringReplace(self.medtChangeParCadBlock.Text,' ','',[rfReplaceAll,rfIgnoreCase]);
end;


procedure TFormBPXML_FormParcels.ChangeParcel_SetCurrentItem(index: Integer);
begin
 if (index>-1) and (index<self.cbbChangeParcelItems_ComboBox.Items.Count) then begin
  self.cbbChangeParcelItems_ComboBox.ItemIndex:= index;
  self.ChangeParcel_DoSetCurrentItem(index);
  self.FChangeParcel_frame.Visible:= TRUE;
  self.ChangeParcel_doRead_CadastralBlock;
 end else begin
   self.FChangeParcel_frame.XmlChangeParcel:= nil;
   self.cbbChangeParcelItems_ComboBox.ItemIndex:= -1;
   self.cbbChangeParcelItems_ComboBox.Text:= '';
   self.medtChangeParCadBlock.Text := '';
   self.FChangeParcel_frame.Visible:= FALSE;
 end;
end;

procedure TFormBPXML_FormParcels.ChangeParcel_UpDateList;
var
 _index,_i: Integer;
 _ChangeParcel: IXMLTChangeParcel;
begin
 _index:= self.cbbChangeParcelItems_ComboBox.ItemIndex;
 self.cbbChangeParcelItems_ComboBox.Clear;
 if self.IsAssigned_FormParcels then
   for _i := 0 to self.FXmlFormParcels.ChangeParcel.Count - 1 do begin
   _ChangeParcel:= self.FXmlFormParcels.ChangeParcel.Items[_i];
   self.cbbChangeParcelItems_ComboBox.AddItem(_ChangeParcel.CadastralNumber, pointer(_ChangeParcel));
  end;
 if _index<0 then _index:= 0;
 self.ChangeParcel_SetCurrentItem(_index);
end;

constructor TFormBPXML_FormParcels.Create(AOwner: TComponent);
begin
  inherited;

  self.jvPageControl_FP.ActivePage:= self.ts_NewParcels;

  //���������� ��
   self.FChangeParcel_frame:= TFrameChangeParcel.Create(self.ts_ChangeParcels);
   self.FChangeParcel_frame.Parent:= self.ts_ChangeParcels;
   self.FChangeParcel_frame.Align:= alClient;
   self.FChangeParcel_frame.Visible:= False;

   //��������� ������ ������� ��������
  self.FSpecifyRelatedsParcels := TFrameSpecifyRelatedsParcels.Create(self.ts_SpecifyRelatedParcel);
  self.FSpecifyRelatedsParcels.Parent := self.ts_SpecifyRelatedParcel;
  self.FSpecifyRelatedsParcels.Align := alClient;
  self.FSpecifyRelatedsParcels.Visible := true;

  //���������� ��
  self.FNewParcel_frame:= TFrameNewParcel.Create(self.ts_NewParcels);
  self.FNewParcel_frame.Parent:= self.ts_NewParcels;
  self.FNewParcel_frame.Align:= alClient;
  self.FNewParcel_frame.Visible:= False;
  self.FNewParcel_frame.SetActivTab(0,FALSE);

end;

function TFormBPXML_FormParcels.IsAssigned_FormParcels: boolean;
begin
 Result:= self.FXmlFormParcels<>nil;
end;

procedure TFormBPXML_FormParcels.medtChangeParCadBlockExit(Sender: TObject);
begin
   self.ChangeParcel_doWrite_CadastralBlock;
end;

procedure TFormBPXML_FormParcels.medtFormParcel_CadastralBlockExit(Sender: TObject);
begin
 self.FormParcel_doWrite_CadastralBlock;
end;

procedure TFormBPXML_FormParcels.medtFormParcel_CadastralBlockKeyUp(
                            Sender: TObject; var Key: Word; Shift: TShiftState);
begin
 if Key = VK_RETURN then
    if (Sender is TCustomEdit) then
      TCustomEdit(Sender).Parent.SetFocus;
end;

procedure TFormBPXML_FormParcels.ReadMethodFormed;
begin
  self.Caption := '������ ����������� ��������: '+ FXmlFormParcels.Method;
end;

procedure TFormBPXML_FormParcels.FormParcels_CreateNewParcel;
begin
 if self.IsAssigned_FormParcels then
 self.FormParcels_DoCreateNewParcel;
 self.FormParcels_DoSetCurrentNewParcel(self.NewParcels_ComboBox.Items.Count-1);
end;

procedure TFormBPXML_FormParcels.FormParcels_Delete(index: Integer);
begin
 if (index>-1) and (index<self.NewParcels_ComboBox.Items.Count) then
  if index=self.NewParcels_ComboBox.ItemIndex then begin
   self.FormParcels_DoDelete(index);
   self.FormParcels_SetCurrentNewParcel(index);
  end else
   self.FormParcels_DoDelete(index);
end;

procedure TFormBPXML_FormParcels.FormParcels_DoSetCurrentNewParcel(index: Integer);
begin
 self.FNewParcel_frame.XmlNewParcel:= IXMLTNewParcel(Pointer(self.NewParcels_ComboBox.Items.Objects[index]));
end;

procedure TFormBPXML_FormParcels.FormParcels_RefreshList(_A: Integer=-1;_B:Integer=-1);
var 
 _NewParcel: IXMLTNewParcel;
 _i,_j: Integer;
begin
 if self.NewParcels_ComboBox.Items.Count = 0 then EXIT;

 if (_A<0) or (_a>=self.NewParcels_ComboBox.Items.Count)  then
  _a:= 0;

 if (_B<_A) or (_B>=self.NewParcels_ComboBox.Items.Count)  then
  _b:= self.NewParcels_ComboBox.Items.Count-1;
 _j:= self.NewParcels_ComboBox.ItemIndex;
 for _i := _a to _b do begin
  _NewParcel:= IXMLTNewParcel(pointer(self.NewParcels_ComboBox.Items.Objects[_i]));
  self.NewParcels_ComboBox.Items.Strings[_i]:= MsXml_ReadAttribute(_NewParcel,cnstAtrDefinition);
 end;
 self.NewParcels_ComboBox.ItemIndex:= _j;
end;

procedure TFormBPXML_FormParcels.FormParcels_SetCurrentNewParcel(index: Integer);
begin
 if (index>-1) and (index<self.NewParcels_ComboBox.Items.Count) then begin
  self.NewParcels_ComboBox.ItemIndex:= index;
  self.FormParcels_DoSetCurrentNewParcel(index);
  self.FNewParcel_frame.Visible:= true;
  self.FormParcel_doRead_Type;
  self.FormParcel_doRead_CadastralBlock;
  self.medtFormParcel_CadastralBlock.Enabled:= true;
  self.cbbFormParcel_Type.Enabled:= true;
 end else begin
   self.cbbFormParcel_Type.ItemIndex:=-1;
   self.cbbFormParcel_Type.Enabled:= false;
   self.medtFormParcel_CadastralBlock.Enabled:= false;
   self.medtFormParcel_CadastralBlock.Text:= '';

   //self.FXmlFormParcel:= nil;
   self.NewParcels_ComboBox.ItemIndex:= -1;
   self.NewParcels_ComboBox.Text:= '';
   self.FNewParcel_frame.XmlNewParcel:= nil;
   self.FNewParcel_frame.Visible:= false;
 end;
end;

procedure TFormBPXML_FormParcels.FormParcels_UpDateList;
var
 _i,_j: Integer;
 _NewParcel: IXMLTNewParcel;
begin
 _j:= self.NewParcels_ComboBox.ItemIndex;
 self.NewParcels_ComboBox.Clear;
 if self.IsAssigned_FormParcels then
  for _i := 0 to self.FXmlFormParcels.NewParcel.Count-1 do begin
   _NewParcel:= self.FXmlFormParcels.NewParcel.Items[_i];
   self.NewParcels_ComboBox.Items.AddObject(_NewParcel.Definition,pointer(_NewParcel));
  end;

 if _j<0 then _j:= 0;
 self.FormParcels_SetCurrentNewParcel(_j);
end;

procedure TFormBPXML_FormParcels.FormParcel_doRead_CadastralBlock;
begin
 if not self.FNewParcel_frame.IsAssigned_NewParcel then EXIT;
 self.medtFormParcel_CadastralBlock.Text :=
  MsXml_ReadChildNodeValue(self.FNewParcel_frame.XmlNewParcel,cnstXmlCadastralBlock);
end;

procedure TFormBPXML_FormParcels.FormParcel_doRead_Type;
begin
 if not self.FNewParcel_frame.IsAssigned_NewParcel then EXIT;
 self.cbbFormParcel_Type.ItemIndex:= self.FNewParcel_frame.TypeNewParcel;
end;

procedure TFormBPXML_FormParcels.FormParcel_doWrite_CadastralBlock;
begin
 if not self.FNewParcel_frame.IsAssigned_NewParcel then EXIT;
 self.FNewParcel_frame.XmlNewParcel.CadastralBlock:=
  StringReplace(self.medtFormParcel_CadastralBlock.Text,' ','',[rfReplaceAll,rfIgnoreCase]);
end;

procedure TFormBPXML_FormParcels.FormParcel_doWrite_Type;
begin
 if not self.FNewParcel_frame.IsAssigned_NewParcel then EXIT;
 self.FNewParcel_frame.TypeNewParcel:= self.cbbFormParcel_Type.ItemIndex;
end;

procedure TFormBPXML_FormParcels.SetFormParcels(const aFormParcels: IXMLSTD_MP_Package_FormParcels);
begin
 if self.FXmlFormParcels=aFormParcels then EXIT;
 self.FXmlFormParcels:= aFormParcels;
 {��������� ����� ����������� ��������}
 self.UpdateMethodFormed;
 self.FormParcels_UpDateList;
 self.ChangeParcel_UpDateList;
 if IsAssigned_FormParcels then
  self.FSpecifyRelatedsParcels.XmlSpecifyRelatedParcelList:= self.FXmlFormParcels.SpecifyRelatedParcel
 else begin
  self.FSpecifyRelatedsParcels.XmlSpecifyRelatedParcelList:= nil;
  self.FNewParcel_frame.XmlNewParcel:= nil;
  self.FChangeParcel_frame.XmlChangeParcel:= nil;
  self.FSpecifyRelatedsParcels.XmlSpecifyRelatedParcelList:= nil;
 end;
end;


procedure TFormBPXML_FormParcels.UpdateMethodFormed;
begin
  if not IsAssigned_FormParcels then Exit;

  if not  FXmlFormParcels.HasAttribute(cnstAtrXmlMethod) then  WriteMethodFormed
  else  ReadMethodFormed;
  self.Caption := FXmlFormParcels.Method;
end;

procedure TFormBPXML_FormParcels.WriteMethodFormed;
begin
 FXmlFormParcels.Method := IntToStr(TSTD_MP_ListofIndexGenerate.IndexOfSelect_MethodFormParcels+1);{��������� � ������ � 0}
end;

procedure TFormBPXML_FormParcels.FormParcels_DoCreateNewParcel;
var
 _NewParcel: IXMLTNewParcel;
 _st: string;
begin
 _NewParcel:= self.FXmlFormParcels.NewParcel.Add;
 _st:= cnstNewParcelDefinition +IntToStr(self.FXmlFormParcels.NewParcel.Count);
 _NewParcel.Definition:= _st;
 self.NewParcels_ComboBox.Items.AddObject(_st,pointer(_NewParcel));
end;

procedure TFormBPXML_FormParcels.FormParcels_DoDelete(index: Integer);
var
 _NewParcel: IXMLTNewParcel;
begin
 _NewParcel:=  IXMLTNewParcel(Pointer(self.NewParcels_ComboBox.Items.Objects[index]));
 self.FXmlFormParcels.NewParcel.Remove(_NewParcel);
 self.NewParcels_ComboBox.Items.Delete(index);
 _NewParcel:= nil;
end;

end.

