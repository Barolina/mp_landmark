unit MSMTopology;
interface
 uses
  classes;
 {
 const
  cMSmTopo_DigitRound: integer = -2;
 }
 Type
  {���������}
  TMSmVirtualTopoNode = class;
  TMSmVirtualTopology=class;
  TMSmVTopo_Accept = reference to procedure(AcceptValue: boolean);
  TMSmVTopoNotifyEvent = procedure (Sender: TMSmVirtualTopology) of object;
  TMSmVTopoNE_IncludingNode = procedure (Sender: TMSmVirtualTopology; aNode: TMSmVirtualTopoNode; var Index: integer; Accept: TMSmVTopo_Accept) of object;
  TMSmVTopoNE_IncludeNode = procedure (Sender: TMSmVirtualTopology; aNode: TMSmVirtualTopoNode; Index: integer) of object;
  TMSmVTopoNE_ExcludingNode = procedure (Sender: TMSmVirtualTopology; aNode: TMSmVirtualTopoNode; Index: integer;  Accept: TMSmVTopo_Accept) of object;
  TMSmVTopoNE_ExcludeNode = procedure (Sender: TMSmVirtualTopology; aNode: TMSmVirtualTopoNode; OldIndex: integer) of object;
  TMSmVTopoNE_BindingNodes = procedure ( Sender: TMSmVirtualTopology;
                                         aNode,BindNode: TMSmVirtualTopoNode;
                                         var aInitData: pointer; var aSizeData: integer; var IndexBind: integer; 
                                         Accept: TMSmVTopo_Accept
                                        ) of object;
  TMSmVTopoNE_BindNodes = procedure (Sender: TMSmVirtualTopology; aNode,BindNode: TMSmVirtualTopoNode; IndexBind: integer) of object;
  TMSmVTopoNE_UnBindingNodes = procedure (Sender: TMSmVirtualTopology;
                                          aNode,UnBindNode: TMSmVirtualTopoNode;
                                          IndexBind: integer;
                                          Accept: TMSmVTopo_Accept) of object;
  TMSmVTopoNE_UnBindNodes = procedure (Sender: TMSmVirtualTopology; aNode,UnBindNode: TMSmVirtualTopoNode; OldIndexBind: integer) of object;
  TMSmVTopoNE_GetData = procedure (Sender: TMSmVirtualTopology; aNode: TMSmVirtualTopoNode) of object;
  TMSmVTopoNE_GetBindData = procedure (Sender: TMSmVirtualTopology; aNode: TMSmVirtualTopoNode; IndexBind: integer) of object;
  TMSmVirtualTopology=class(TObject)
   private
    FNodes: TList;
    FOwnerNodes: boolean;
    FIsDestroyable: boolean;
    FOnIncludingNode: TMSmVTopoNE_IncludingNode;
    FOnIncludeNode: TMSmVTopoNE_IncludeNode;
    FOnExcludingNode: TMSmVTopoNE_ExcludingNode;
    FOnExcludeNode: TMSmVTopoNE_ExcludeNode;
    FOnBindingNodes: TMSmVTopoNE_BindingNodes;
    FOnBindNodes: TMSmVTopoNE_BindNodes;
    FOnUnBindingNodes: TMSmVTopoNE_UnBindingNodes;
    FOnUnBindNodes: TMSmVTopoNE_UnBindNodes;
    FOnGetBindData: TMSmVTopoNE_GetBindData;
    FOnGetNodeData: TMSmVTopoNE_GetData;
    function GetNode(index: integer): TMSmVirtualTopoNode;
    procedure NENodeOnBindingNodes(aNode,BindNode: TMSmVirtualTopoNode; var InitData: pointer; var SizeData: integer; var IndexBind: integer; Accept: TMSmVTopo_Accept);
    procedure NENodeOnBindNodes(aNode,BindNode: TMSmVirtualTopoNode; IndexBind: integer);
    procedure NENodeOnUnBindingNodes(aNode,UnBindNode: TMSmVirtualTopoNode; IndexBind: integer; Accept: TMSmVTopo_Accept);
    procedure NENodeOnUnBindNodes(aNode,UnBindNode: TMSmVirtualTopoNode; IndexBind: integer);
    procedure NENodeOnGetBindData(aNode: TMSmVirtualTopoNode; IndexBind: integer);
    procedure NENodeOnGetNodeData(aNode: TMSmVirtualTopoNode);
   protected
    
     
     
     
    function DoIncludeNode(aNode: TMSmVirtualTopoNode; IndexPos: integer = -1): integer;
    
    function DoExcludeNode(Index: integer): TMSmVirtualTopoNode;
    Function GetNodesCount: integer;
   public
    constructor Create(OwnerNodes: boolean = true);
    destructor Destroy; override;
    function IncludeNode(aNode: TMSmVirtualTopoNode; IndexPos: integer = -1): integer; virtual;
    function ExcludeNode(aNode: TMSmVirtualTopoNode): TMSmVirtualTopoNode; virtual;
    procedure ExcludeAllNode;
    function CreateNode(aInitData: pointer=nil; aSizeData: integer = 0): TMSmVirtualTopoNode; virtual;
    procedure DeleteNode(index: integer); virtual;
    procedure DeleteAllNode;
    function IndexOfNode(aNode: TMSmVirtualTopoNode): integer;
    property NodesEnum[index: integer]: TMSmVirtualTopoNode read GetNode;
    property NodesCount: integer read GetNodesCount;
    property OwnerNodes: boolean read FOwnerNodes write FOwnerNodes;
    property OnIncludingNode: TMSmVTopoNE_IncludingNode read FOnIncludingNode write FOnIncludingNode;
    property OnIncludeNode: TMSmVTopoNE_IncludeNode read FOnIncludeNode write FOnIncludeNode;
    property OnExcludingNode: TMSmVTopoNE_ExcludingNode read FOnExcludingNode write FOnExcludingNode;
    property OnExcludeNode: TMSmVTopoNE_ExcludeNode read FOnExcludeNode write FOnExcludeNode;
    property OnBindingNodes: TMSmVTopoNE_BindingNodes read FOnBindingNodes write FOnBindingNodes;
    property OnBindNodes: TMSmVTopoNE_BindNodes read FOnBindNodes write FOnBindNodes;
    property OnUnBindingNodes: TMSmVTopoNE_UnBindingNodes read FOnUnBindingNodes write FOnUnBindingNodes;
    property OnUnBindNodes: TMSmVTopoNE_UnBindNodes read FOnUnBindNodes write FOnUnBindNodes;
    property OnGetBindData: TMSmVTopoNE_GetBindData read FOnGetBindData write FOnGetBindData;
    property OnGetNodeData: TMSmVTopoNE_GetData read FOnGetNodeData write FOnGetNodeData;
  end;
  TMSmTNode_State = (tnsTopology,tnsBind,tnsDestroyable);
  TMSmTNode_States = set of TMSmTNode_State;
  TMSmVirtualTopoNode = class
   private
    FStates: TMSmTNode_States;
    FTopology: TMSmVirtualTopology;
    FBinds: TList;
    FDataSize: integer;
    FData: pointer;
    type
    
    
     PBind = ^TBind;
     TBind = record
      Node: TMSmVirtualTopoNode; 
      SizeD: Integer; 
      DataP: pointer; 
     end;
    
    function CreateBind(Index: integer):PBind;
    function ExtractBind(aBind: PBind):PBind;
    procedure DeleteBind(Index: integer);
    function GetBind(index: Integer): PBind;
    function GetIndexBind(aBind: PBind): integer;
   protected
    function GetBindsCount: integer; 
   
    
    function GetIndexBindNode(aBindNode: TMSmVirtualTopoNode): integer;
    
    function GetBindNode(index: Integer): TMSmVirtualTopoNode;
   
    
    function GetBindDataSize(index: integer): integer;
    
    procedure SetBindDataSize(index: integer; Value: integer);
    
    function GetBindData(index: integer): pointer;
    
    
    procedure SetBindData(index: integer; Source: pointer);
   
    function GetDataSize: integer;
    procedure SetDataSize(Value: integer);
    function GetData: pointer;
    
    
    procedure SetData(Source: pointer);
   
    
    Procedure DoSetTopology(OwnT: TMSmVirtualTopology);
    
    function GetTopology: TMSmVirtualTopology;
   
    
    
    function DoBind(aNode: TMSmVirtualTopoNode; aInitData: pointer=nil; aSizeData: integer = 0; IndexPos: integer = -1): integer;
    
    function DoUnBind(aNode: TMSmVirtualTopoNode):TMSmVirtualTopoNode;
   public
    constructor Create(OwnT: TMSmVirtualTopology);
    destructor Destroy; override;
    function Bind(aNode: TMSmVirtualTopoNode; aInitData: pointer=nil; aSizeData: integer = 0; IndexPos: integer = -1):integer; virtual;
    function UnBind(aNode: TMSmVirtualTopoNode): TMSmVirtualTopoNode; virtual;
    procedure UnBindAll;
    function IndexOfBindNode(aBindNode: TMSmVirtualTopoNode): integer;
    property Data: Pointer read GetData write SetData;
    property DataSize: integer read GetDataSize write SetDataSize;
    property BindData[index: integer]: Pointer read GetBindData write SetBindData;
    property BindDataSize[index: integer]: integer read GetBindDataSize write SetBindDataSize;
    property BindNodes[index: integer]: TMSmVirtualTopoNode read GetBindNode;
    property BindsCount: integer read GetBindsCount;
    property Topology: TMSmVirtualTopology read GetTopology write DoSetTopology;
    property State: TMSmTNode_States read FStates;
   end;
implementation
{ TMSmTopoNode }
function TMSmVirtualTopoNode.Bind(aNode: TMSmVirtualTopoNode; aInitData: pointer; aSizeData: integer; IndexPos: integer): integer;
begin
 Result:= self.DoBind(aNode,aInitData,aSizeData,IndexPos)
end;
procedure TMSmVirtualTopoNode.UnBindAll;
var
 _i: integer;
begin
 for _i := self.GetBindsCount-1 downto 0 do
  self.UnBind(self.GetBindNode(_i));
end;
constructor TMSmVirtualTopoNode.Create(OwnT: TMSmVirtualTopology);
begin
 self.FStates:= [];
 self.FTopology:= nil;
 self.FBinds:= TList.Create;
 self.FDataSize:= 0;
 self.FData:= nil;
 self.DoSetTopology(OwnT);
end;
function TMSmVirtualTopoNode.CreateBind(Index: integer): PBind;
begin
 GetMem(Result,sizeof(TBind));
 TRY
  Result^.Node:= nil;
  Result^.SizeD:= 0;
  result^.DataP:= nil;
  self.FBinds.Insert(Index,Result);
 EXCEPT
  FreeMem(Result,sizeof(tbind));
  raise;
 END;
end;
procedure TMSmVirtualTopoNode.DeleteBind(Index: integer);
var
 _bind: PBind;
 _p: pointer;
 _s: integer;
begin
 _bind:= self.GetBind(Index);
 self.FBinds.Delete(index);
 _s:= _bind.SizeD;
 _p:= _bind.DataP;
 FreeMem(_bind,sizeof(tbind));
 if _s>0 then
  FreeMem(_p,_s);
end;
destructor TMSmVirtualTopoNode.Destroy;
begin
 include(self.FStates,tnsDestroyable);
 self.UnBindAll;
 if Assigned(self.FTopology) then
  self.FTopology.ExcludeNode(self);
 inherited;
end;
function TMSmVirtualTopoNode.DoBind(aNode: TMSmVirtualTopoNode; aInitData: pointer; aSizeData: integer; IndexPos: integer): integer;
var
 _i: integer;
 _Bind: PBind;
 _accept: boolean;
 _AcceptPr: TMSmVTopo_Accept;
begin
 Result:= -1;
 if (self=aNode) or (self.GetIndexBindNode(aNode)<>-1) then EXIT;
 _accept:= TRUE;
 
 if (aNode.GetIndexBindNode(self)<>-1) then
  _AcceptPr:= nil
 else
  
   
  _AcceptPR:= procedure(AcceptValue: boolean) begin _accept:= AcceptValue; end;
 if Assigned(self.FTopology) then
  self.FTopology.NENodeOnBindingNodes(self,aNode,aInitData,aSizeData, IndexPos, _AcceptPr);
 if not _accept then EXIT;
 if (IndexPos<0) or (IndexPos>=self.GetBindsCount) then
   IndexPos:= self.GetBindsCount;
 _Bind:= CreateBind(IndexPos);
 _Bind.Node:= aNode;
 self.SetBindDataSize(IndexPos,aSizeData);
 self.SetBindData(IndexPos,aInitData);
 Include(self.FStates,tnsBind);
 if Assigned(self.FTopology) then
   self.FTopology.NENodeOnBindNodes(self,aNode,result);
 aNode.Bind(self,aInitData,aSizeData,IndexPos);
end;
function TMSmVirtualTopoNode.DoUnBind(aNode: TMSmVirtualTopoNode): TMSmVirtualTopoNode;
var
 _accept: boolean;
 _AcceptPr: TMSmVTopo_Accept;
 _index: integer;
begin
 Result:= NIL;
 _index:= self.GetIndexBindNode(aNode);
 if (_index=-1) then EXIT;
 _accept:= TRUE;
 
 if (aNode.GetIndexBindNode(self)=-1) or
    (tnsDestroyable in self.FStates) or (tnsDestroyable in aNode.State)  then
  _AcceptPr:= nil
 else
   
  _AcceptPR:= procedure(AcceptValue: boolean) begin _accept:= AcceptValue; end;
 if Assigned(self.FTopology) then
  self.FTopology.NENodeOnUnBindingNodes(self,aNode,_index,_AcceptPr);
 if not _accept then EXIT;
 self.DeleteBind(_index);
 if self.GetBindsCount=0 then
  Exclude(self.FStates,tnsBind);
 if Assigned(self.FTopology) then
  self.FTopology.NENodeOnUnBindNodes(self,aNode,_index);
  Result:= aNode;
  aNode.UnBind(self);
end;
function TMSmVirtualTopoNode.ExtractBind(aBind: PBind): PBind;
begin
 Result:= PBind(self.FBinds.Extract(aBind));
end;
function TMSmVirtualTopoNode.GetBindData(index: integer): pointer;
begin
 if Assigned(self.FTopology) then
  self.FTopology.NENodeOnGetBindData(self,index);
 Result:= self.GetBind(index).DataP;
end;
function TMSmVirtualTopoNode.GetBindDataSize(index: integer): integer;
begin
 Result:=self.GetBind(index).SizeD;
end;
function TMSmVirtualTopoNode.GetBindNode(index: Integer): TMSmVirtualTopoNode;
begin
 Result:= self.GetBind(index)^.Node;
end;
function TMSmVirtualTopoNode.GetBind(index: Integer): PBind;
begin
 Result:= PBind(self.FBinds[index]);
end;
function TMSmVirtualTopoNode.GetBindsCount: integer;
begin
 Result:= self.FBinds.Count;
end;
function TMSmVirtualTopoNode.GetData: pointer;
begin
 if Assigned(self.FTopology) then
  self.FTopology.NENodeOnGetNodeData(self);
 Result:= Self.FData;
end;
function TMSmVirtualTopoNode.GetDataSize: integer;
begin
 Result:= self.FDataSize;
end;
function TMSmVirtualTopoNode.GetIndexBind(aBind: PBind): integer;
begin
 Result:= self.FBinds.IndexOf(aBind);
end;
function TMSmVirtualTopoNode.GetIndexBindNode(aBindNode: TMSmVirtualTopoNode): integer;
var
 _i: integer;
begin
 Result:= -1;
 for _i := 0 to self.GetBindsCount-1 do
  if self.GetBindNode(_i)=aBindNode then begin
    Result:= _i;
    BREAK;
  end;
end;
function TMSmVirtualTopoNode.GetTopology: TMSmVirtualTopology;
begin
 Result:= self.FTopology;
end;
function TMSmVirtualTopoNode.IndexOfBindNode(aBindNode: TMSmVirtualTopoNode): integer;
begin
 Result:= self.GetIndexBindNode(aBindNode);
end;
procedure TMSmVirtualTopoNode.SetBindData(index: integer; Source: pointer);
var
 _bind: PBind;
begin
 _bind:= self.GetBind(index);
 if _bind.SizeD>0 then
   Move(Source^, _bind.DataP^, _bind.SizeD)
 else
  _bind.DataP:= Source;
end;
procedure TMSmVirtualTopoNode.SetBindDataSize(index, Value: integer);
var
 _bind: PBind;
 _p: Pointer;
begin
 _bind:= self.GetBind(index);
 if _bind.SizeD=Value then EXIT;
 if _bind.SizeD>0 then begin
  _p:= _bind.DataP;
  FreeMem(_p,_bind.SizeD);
 end;
 _bind.SizeD:= Value;
 if _bind.SizeD>0 then
  GetMem(_bind.DataP,_bind.SizeD);
end;
procedure TMSmVirtualTopoNode.SetData(Source: pointer);
begin
 if self.FDataSize>0 then
   Move(Source^, self.FData^, self.FDataSize)
 else
  self.FData:= Source;
end;
procedure TMSmVirtualTopoNode.SetDataSize(Value: integer);
begin
 if self.FDataSize=Value then EXIT;
 if self.FDataSize>0 then
  FreeMem(self.FData,self.FDataSize);
 self.FDataSize:= Value;
 if self.FDataSize>0 then
  GetMem(self.FData,self.FDataSize);
end;
procedure TMSmVirtualTopoNode.DoSetTopology(OwnT: TMSmVirtualTopology);
begin
 if self.FTopology=OwnT then EXIT;
 if Assigned(self.FTopology) then
  if self.FTopology.IndexOfNode(self)<>-1 then
   if self.FTopology.ExcludeNode(self)<>self then EXIT;
 if Assigned(OwnT) and ((OwnT.IndexOfNode(self)<>-1) or (OwnT.IncludeNode(self) <> -1)) then begin
  self.FTopology:= OwnT;
  Include(self.FStates,tnsTopology);
 end else begin
  self.FTopology:= nil;
  Exclude(self.FStates,tnsTopology);
 end;
end;
function TMSmVirtualTopoNode.UnBind(aNode: TMSmVirtualTopoNode): TMSmVirtualTopoNode;
begin
 Result:= self.DoUnBind(aNode);
end;
{ TMSmTopology }
function TMSmVirtualTopology.CreateNode(aInitData: pointer; aSizeData: integer): TMSmVirtualTopoNode;
begin
 Result:= TMSmVirtualTopoNode.Create(nil);
 Result.DataSize:= aSizeData;
 Result.Data:= aInitData;
 if self.IncludeNode(Result) = -1 then begin
  Result.Free;
  Result:= nil;
 end;
end;
constructor TMSmVirtualTopology.Create(OwnerNodes: boolean);
begin
 self.FNodes:= TList.Create;
 self.FOwnerNodes:= OwnerNodes;
end;
procedure TMSmVirtualTopology.DeleteAllNode;
var
 _i: integer;
begin
 while self.NodesCount<>0 do
  self.DeleteNode(0);
end;
procedure TMSmVirtualTopology.DeleteNode(index: integer);
var
 _node: TMSmVirtualTopoNode;
begin
 _node:= self.GetNode(index);
 if self.ExcludeNode(_node)<>nil then
  _node.Free;
end;
destructor TMSmVirtualTopology.Destroy;
begin
 self.FIsDestroyable:= TRUE;
 if self.OwnerNodes then
  self.DeleteAllNode
 else
  self.ExcludeAllNode;
 self.FNodes.Free;
 inherited;
end;
function TMSmVirtualTopology.DoExcludeNode(Index: integer):TMSmVirtualTopoNode;
var
 _Accept: boolean;
 _aNode: TMSmVirtualTopoNode;
 _AcceptPR: TMSmVTopo_Accept;
begin
 Result:= nil;
 if (index<0) or (index>=self.FNodes.Count) then EXIT;
 _aNode:= self.GetNode(Index);
 _Accept:= TRUE;
 if self.FIsDestroyable or (tnsDestroyable in _aNode.State) then
  _AcceptPR:= nil
 else
 _AcceptPR:= procedure(AcceptValue: boolean) begin _accept:= AcceptValue; end;
 if Assigned(self.FOnExcludingNode) then
   self.FOnExcludingNode(self,_aNode,Index,_AcceptPR);
 if (not _Accept) then EXIT;
 self.FNodes.Delete(index);
 _aNode.DoSetTopology(nil);
 Result:= _aNode;
 if Assigned(self.FOnExcludeNode) then
   self.FOnExcludeNode(self,_aNode,Index);
end;
function TMSmVirtualTopology.DoIncludeNode(aNode: TMSmVirtualTopoNode; IndexPos: integer): integer;
var
 _accept: boolean;
 _AcceptPR: TMSmVTopo_Accept;
begin
  Result:= -1;
  if self.IndexOfNode(aNode)<>-1 then EXIT;
  _accept:= TRUE;
  _AcceptPR:= procedure(AcceptValue: boolean) begin _accept:= AcceptValue; end;
  if Assigned(self.FOnIncludingNode) then
     self.FOnIncludingNode(self,aNode,IndexPos,_AcceptPR);
  if not _accept then EXIT;
  aNode.DoSetTopology(nil);
  if aNode.GetTopology<>nil then EXIT; 
  
  if (IndexPos<0) or (IndexPos>self.FNodes.Count) then
   IndexPos:= self.FNodes.Count;
   self.FNodes.Insert(IndexPos,aNode);
   Result:= IndexPos;
  aNode.DoSetTopology(self);
  if Assigned(self.FOnIncludeNode) then
    self.FOnIncludeNode(self,aNode,Result);
end;
procedure TMSmVirtualTopology.ExcludeAllNode;
begin
 while self.NodesCount<>0 do
  self.ExcludeNode(self.NodesEnum[0]);
end;
function TMSmVirtualTopology.ExcludeNode(aNode: TMSmVirtualTopoNode): TMSmVirtualTopoNode;
begin
  Result:= self.DoExcludeNode(self.IndexOfNode(aNode));
end;
function TMSmVirtualTopology.GetNode(index: integer): TMSmVirtualTopoNode;
begin
 Result:= TMSmVirtualTopoNode(self.FNodes[index]);
end;
function TMSmVirtualTopology.GetNodesCount: integer;
begin
 Result:= self.FNodes.Count;
end;
function TMSmVirtualTopology.IncludeNode(aNode: TMSmVirtualTopoNode; IndexPos: integer): integer;
begin
 Result:= self.DoIncludeNode(aNode,IndexPos);
end;
function TMSmVirtualTopology.IndexOfNode(aNode: TMSmVirtualTopoNode): integer;
begin
 Result:= self.FNodes.IndexOf(aNode);
end;
procedure TMSmVirtualTopology.NENodeOnBindingNodes(aNode, BindNode: TMSmVirtualTopoNode;
                                                   var InitData: pointer; var SizeData: integer; var IndexBind: integer;
                                                   Accept: TMSmVTopo_Accept);
begin
 if Assigned(self.FOnBindingNodes) then
  self.FOnBindingNodes(self,aNode,BindNode,InitData,SizeData,IndexBind,Accept);
end;
procedure TMSmVirtualTopology.NENodeOnBindNodes(aNode, BindNode: TMSmVirtualTopoNode; IndexBind: integer);
begin
 if Assigned(self.FOnBindNodes) then
  self.FOnBindNodes(self,aNode,BindNode,IndexBind);
end;
procedure TMSmVirtualTopology.NENodeOnGetBindData(aNode: TMSmVirtualTopoNode; IndexBind: integer);
begin
 if Assigned(self.FOnGetBindData) then
  self.FOnGetBindData(self,aNode,IndexBind);
end;
procedure TMSmVirtualTopology.NENodeOnGetNodeData(aNode: TMSmVirtualTopoNode);
begin
 if Assigned(self.FOnGetNodeData) then
  self.FOnGetNodeData(self,aNode);
end;
procedure TMSmVirtualTopology.NENodeOnUnBindingNodes(aNode,
  UnBindNode: TMSmVirtualTopoNode; IndexBind: integer; Accept: TMSmVTopo_Accept);
begin
 if Assigned(self.FOnUnBindingNodes) then
  self.FOnUnBindingNodes(self,aNode,UnBindNode,IndexBind,Accept);
end;
procedure TMSmVirtualTopology.NENodeOnUnBindNodes(aNode,UnBindNode: TMSmVirtualTopoNode; IndexBind: integer);
begin
 if Assigned(self.FOnUnBindNodes) then
  self.FOnUnBindNodes(self,aNode,UnBindNode,IndexBind);
end;
end.
