object FrameMeans_Surveys: TFrameMeans_Surveys
  Left = 0
  Top = 0
  Width = 985
  Height = 413
  TabOrder = 0
  object Panel: TPanel
    Left = 0
    Top = 73
    Width = 985
    Height = 340
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object stringGridDate: TStringGrid
      AlignWithMargins = True
      Left = 3
      Top = 3
      Width = 979
      Height = 334
      HelpType = htKeyword
      Align = alClient
      BevelEdges = [beTop]
      BevelInner = bvNone
      BevelKind = bkSoft
      BevelOuter = bvNone
      BorderStyle = bsNone
      Color = 13750737
      ColCount = 3
      DefaultColWidth = 217
      DrawingStyle = gdsGradient
      FixedColor = clGradientActiveCaption
      FixedCols = 0
      RowCount = 1
      FixedRows = 0
      GradientEndColor = clBlack
      GradientStartColor = clSkyBlue
      Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goDrawFocusSelected, goRowSizing, goColSizing, goEditing, goTabs]
      PopupMenu = poMenuTable
      TabOrder = 0
      OnDrawCell = stringGridDateDrawCell
      OnKeyUp = stringGridDateKeyUp
      OnSetEditText = stringGridDateSetEditText
    end
  end
  object pnlLabelTabl: TPanel
    Left = 0
    Top = 0
    Width = 985
    Height = 73
    Align = alTop
    BevelEdges = [beTop]
    BevelKind = bkFlat
    BevelOuter = bvNone
    Color = 15790320
    ParentBackground = False
    TabOrder = 1
    object lblCertificate_Verification: TLabel
      Left = 36
      Top = 0
      Width = 165
      Height = 71
      Align = alLeft
      Alignment = taCenter
      AutoSize = False
      Caption = 
        #1056#1077#1082#1074#1080#1079#1080#1090#1099' '#1089#1074#1080#1076#1077#1090#1077#1083#1100#1089#1090#1074#1072' '#1086' '#1087#1086#1074#1077#1088#1082#1077' '#1087#1088#1080#1073#1086#1088#1072' ('#1080#1085#1089#1090#1088#1091#1084#1077#1085#1090#1072', '#1072#1087#1087#1072#1088#1072#1090#1091 +
        #1088#1099')'
      Color = 15790320
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Layout = tlCenter
      WordWrap = True
      ExplicitLeft = -4
      ExplicitTop = -3
      ExplicitHeight = 49
    end
    object lblName: TLabel
      Left = 674
      Top = 0
      Width = 163
      Height = 71
      Align = alLeft
      Alignment = taCenter
      AutoSize = False
      Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1087#1088#1080#1073#1086#1088#1072#13#10'('#1080#1085#1089#1090#1088#1091#1084#1077#1085#1090#1072', '#1072#1087#1087#1072#1088#1072#1090#1091#1088#1099')'
      Color = 15790320
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Layout = tlCenter
      WordWrap = True
      ExplicitLeft = 678
      ExplicitTop = -1
      ExplicitHeight = 45
    end
    object lblCertificate: TLabel
      Left = 203
      Top = 0
      Width = 469
      Height = 71
      Align = alLeft
      Alignment = taCenter
      AutoSize = False
      Caption = 
        #1056#1077#1082#1074#1080#1079#1080#1090#1099' '#1089#1077#1088#1090#1080#1092#1080#1082#1072#1090#1072' '#1087#1088#1080#1073#1086#1088#1072' ('#1080#1085#1089#1090#1088#1091#1084#1077#1085#1090#1072', '#1072#1087#1087#1072#1088#1072#1090#1091#1088#1099'), '#1087#1088#1080' '#1085#1072#1083 +
        #1080#1095#1080#1080' '#1090#1072#1082#1086#1075#1086' '#1089#1088#1077#1076#1089#1090#1074#1072#13#10
      Color = 15790320
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Layout = tlCenter
      WordWrap = True
      ExplicitLeft = 162
      ExplicitTop = -3
      ExplicitHeight = 49
    end
    object spl1: TSplitter
      Left = 201
      Top = 0
      Width = 2
      Height = 71
      Color = clBtnText
      ParentColor = False
      OnMoved = spl1Moved
      ExplicitHeight = 49
    end
    object spl2: TSplitter
      Left = 672
      Top = 0
      Width = 2
      Height = 71
      Color = clDefault
      ParentColor = False
      OnMoved = spl2Moved
      ExplicitLeft = 671
      ExplicitHeight = 49
    end
    object spl3: TSplitter
      Left = 837
      Top = 0
      Width = 2
      Height = 71
      Color = clBtnText
      ParentColor = False
      OnMoved = spl3Moved
      ExplicitLeft = 836
      ExplicitHeight = 49
    end
    object lblN: TLabel
      Left = 0
      Top = 0
      Width = 35
      Height = 71
      Align = alLeft
      Alignment = taCenter
      AutoSize = False
      Caption = #8470
      Color = 15790320
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Layout = tlCenter
      WordWrap = True
      ExplicitHeight = 49
    end
    object spl4: TSplitter
      Left = 35
      Top = 0
      Width = 1
      Height = 71
      Color = clBtnText
      ParentColor = False
      ExplicitHeight = 49
    end
  end
  object poMenuTable: TPopupMenu
    Left = 224
    Top = 120
    object N1: TMenuItem
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1089#1090#1088#1086#1082#1091
      ShortCut = 16429
      OnClick = SubParcels_Actions_ButtonGroupItems0Click
    end
    object N2: TMenuItem
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1089#1090#1088#1086#1082#1091' '
      ShortCut = 16430
      OnClick = SubParcels_Actions_ButtonGroupItems1Click
    end
  end
end
