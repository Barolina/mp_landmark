object FrameDataDocuments: TFrameDataDocuments
  Left = 0
  Top = 0
  Width = 642
  Height = 325
  Color = clBtnFace
  ParentBackground = False
  ParentColor = False
  TabOrder = 0
  object spl1: TSplitter
    Left = 113
    Top = 0
    Width = 5
    Height = 325
    Beveled = True
  end
  object PanelNodalPointSchemes_List: TPanel
    Left = 0
    Top = 0
    Width = 113
    Height = 325
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 0
    object Documents_List_ListBox: TListBox
      Left = 0
      Top = 25
      Width = 113
      Height = 300
      Hint = #1056#1077#1074#1080#1079#1080#1090#1099' '#1076#1086#1082#1091#1084#1077#1085#1090#1086#1074', '#1087#1086#1076#1090#1074#1077#1088#1078#1076#1072#1102#1097#1080#1093' '#1087#1088#1072#1074#1072' '#1085#1072' '#1079#1077#1084#1077#1083#1100#1085#1099#1081' '#1091#1095#1072#1089#1090#1086#1082
      Style = lbOwnerDrawVariable
      Align = alClient
      BevelEdges = [beLeft, beBottom]
      BevelInner = bvNone
      BevelKind = bkFlat
      BorderStyle = bsNone
      Color = 15921123
      ItemHeight = 13
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      StyleElements = []
      OnClick = Action_NodalPointSchemes_SetCurrentExecute
    end
    object jvtlbrElemeny: TJvToolBar
      Left = 0
      Top = 0
      Width = 113
      Height = 25
      AutoSize = True
      ButtonHeight = 25
      ButtonWidth = 41
      DoubleBuffered = False
      DockSite = True
      DrawingStyle = dsGradient
      EdgeBorders = [ebRight, ebBottom]
      EdgeInner = esNone
      EdgeOuter = esNone
      Images = formContainer.ilCommands
      List = True
      GradientDirection = gdHorizontal
      ParentDoubleBuffered = False
      ParentShowHint = False
      AllowTextButtons = True
      ShowHint = True
      TabOrder = 1
      object btnAdd: TToolButton
        AlignWithMargins = True
        Left = 0
        Top = 0
        Hint = #13#10#1044#1086#1073#1072#1074#1080#1090#1100' '#1076#1086#1082#1091#1084#1077#1085#1090#13#10
        Action = Action_Contrours_AddNew
        AllowAllUp = True
        AutoSize = True
        EnableDropdown = True
        Grouped = True
        Indeterminate = True
        ParentShowHint = False
        ShowHint = True
      end
      object btnDel: TToolButton
        Left = 24
        Top = 0
        Hint = #13#10#1059#1076#1072#1083#1080#1090#1100' '#1074#1099#1073#1088#1072#1085#1085#1099#1081' '#1076#1086#1082#1091#1084#1077#1085#1090#13#10
        Action = Action_NodalPointSchemes_DeleteCurrent
        EnableDropdown = True
        Grouped = True
        Indeterminate = True
        ParentShowHint = False
        ShowHint = True
      end
      object btnimport: TToolButton
        Left = 48
        Top = 0
        Hint = 
          #1047#1072#1075#1088#1091#1079#1082#1072' '#1085#1077#1089#1082#1086#1083#1100#1082#1080#1093' '#1087#1088#1080#1083#1086#1078#1077#1085#1085#1099#1093' '#1076#1086#1082#1091#1084#1077#1085#1090#1086#1074','#13#10'c '#1088#1072#1079#1073#1086#1088#1086#1084' '#1072#1090#1088#1080#1073#1091#1090#1086 +
          #1074' '#1076#1086#1082#1091#1084#1077#1085#1090#1072' : '#13#10'             '#1042#1080#1076' '#1076#1086#1082#1091#1084#1077#1085#1090#1072','#13#10'             '#1053#1086#1084#1077#1088' ' +
          #1076#1086#1082#1091#1084#1077#1085#1090#1072','#13#10'             '#1053#1072#1080#1084#1077#1085#1086#1074#1085#1072#1080#1077' '#1076#1086#1082#1091#1084#1077#1085#1090#1072','#13#10#1077#1089#1083#1080' '#1080#1084#1103' '#1087#1086#1076#1075#1088 +
          #1091#1078#1072#1077#1084#1086#1075#1086' '#1092#1072#1081#1083#1072' '#1080#1084#1077#1077#1090' '#1092#1086#1088#1084#1072#1090' :'#13#10#13#10'         "'#1085#1072#1080#1084#1085#1086#1074#1072#1085#1080#1077';'#1085#1086#1084#1077#1088';'#1076#1072#1090 +
          #1072'.pdf".'#13#10#1042#1080#1076' '#1076#1086#1082#1091#1084#1077#1085#1090#1072' '#1087#1088#1086#1089#1090#1072#1074#1083#1103#1077#1090#1089#1103' '#1087#1086' '#1091#1084#1086#1083#1095#1072#1085#1080#1102', '#1082#1072#1082' '#1080#1085#1086#1077'.'
        Action = actImportAll
        EnableDropdown = True
        Grouped = True
        Indeterminate = True
        ParentShowHint = False
        ShowHint = True
      end
    end
  end
  object ActionList: TActionList
    Images = formContainer.ilCommands
    Left = 440
    Top = 104
    object Action_Contrours_AddNew: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1082#1086#1085#1090#1091#1088
      Hint = #1044#1086#1073#1072#1074#1080#1090#1100' '#1082#1086#1085#1090#1091#1088
      ImageIndex = 0
      OnExecute = Action_Contrours_AddNewExecute
    end
    object Action_NodalPointSchemes_DeleteCurrent: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1074#1099#1073#1088#1072#1085#1085#1099#1081' '#1082#1086#1085#1090#1091#1088
      Hint = #1059#1076#1072#1083#1080#1090#1100' '#1074#1099#1073#1088#1072#1085#1085#1099#1081' '#1082#1086#1085#1090#1091#1088
      ImageIndex = 1
      OnExecute = Action_NodalPointSchemes_DeleteCurrentExecute
    end
    object Action_NodalPointSchemes_SetCurrent: TAction
      Caption = #1057#1076#1077#1083#1072#1090#1100' '#1090#1077#1082#1091#1097#1080#1084
      OnExecute = Action_NodalPointSchemes_SetCurrentExecute
    end
    object actImportAll: TAction
      Caption = #1048#1084#1087#1086#1088#1090' '#1085#1077#1089#1082#1086#1083#1100#1082#1080#1093' '#1076#1086#1082#1091#1084#1077#1085#1090#1086#1074
      ImageIndex = 2
      OnExecute = actImportAllExecute
    end
    object actHint: TAction
      Caption = 'actHint'
    end
  end
  object dlgFileOpenDialog: TOpenDialog
    Filter = 'PDF|*.pdf'
    Options = [ofHideReadOnly, ofAllowMultiSelect, ofFileMustExist, ofEnableSizing]
    Left = 296
    Top = 144
  end
  object blnhnt1: TBalloonHint
    Images = formContainer.il16
    Delay = 0
    HideAfter = 5000
    Left = 304
    Top = 64
  end
end
