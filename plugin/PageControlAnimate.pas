unit PageControlAnimate;

interface
 uses
  Windows, MMsystem,Graphics, Controls, ComCtrls, Classes;


 procedure pcaAnimatingChangeTabSheet(Pc:TPageControl; Tab1, Tab2: integer);
 procedure pcaSetActivTab(Pc:TPageControl; Tab: TTabSheet);
 procedure pcaGoNextTab(Pc:TPageControl);
 procedure pcaGoBackTab(Pc:TPageControl);

implementation
const
 _ACTH_dx = 10;
 _ACTH_POROG = 11;

var
 _ACTH_Direction: integer;
 _ACTH_BmTab1,_ACTH_BmTab2: TBitmap;
 _ACTH_Speed: cardinal = 50;
 _ACTH_uTimerID: Cardinal;
 _ACTH_shiftTab1,_ACTH_shiftTab2: integer;

 _Pc: TPageControl;

procedure _Initialization(Pc: TPageControl);
begin
 _Pc:= pc;
end;

 procedure _OnTime(uTimerID, uMessage: UINT; dwUser, dw1, dw2: DWORD); stdcall;
 begin
  timeKillEvent(uTimerID);
  _ACTH_shiftTab1:= _ACTH_shiftTab1-_ACTH_dx*_ACTH_Direction;
  _ACTH_shiftTab2:= _ACTH_shiftTab2-_ACTH_dx*_ACTH_Direction;
  if ABS(_ACTH_shiftTab2)<_ACTH_POROG then _ACTH_shiftTab2:= 0;
  _pc.canvas.Draw(_ACTH_shiftTab1,0,_ACTH_BmTab1);
  _Pc.canvas.Draw(_ACTH_shiftTab2,0,_ACTH_BmTab2);
  _ACTH_Speed:= round(_ACTH_BmTab2.Width/abs(_ACTH_shiftTab2));
  if _ACTH_Speed=0 then _ACTH_Speed:= 1;
  if _ACTH_shiftTab2<>0 then
    _ACTH_uTimerID:=timeSetEvent(_ACTH_Speed,500,@_onTime,0,TIME_ONESHOT);
 end;

 procedure pcaAnimatingChangeTabSheet(Pc:TPageControl;Tab1, Tab2: integer);
begin
 _Initialization(Pc);
 if not ((Tab1<>Tab2) and (Tab1>=0) and (Tab1<_pc.PageCount) and
    (Tab2>=0) and (Tab2<_pc.PageCount)) then EXIT;
 if Tab2> Tab1 then _ACTH_Direction:= 1 else _ACTH_Direction:= -1;

 _ACTH_BmTab1:= TBitmap.Create;
 _ACTH_BmTab1.Width:= _pc.Width;
 _ACTH_BmTab1.Height:= _pc.Height;
 _ACTH_BmTab1.Canvas.Brush:= _pc.Brush;
 _ACTH_BmTab1.Canvas.FillRect(rect(0,0,_pc.Width,_pc.Height));
 _pc.Pages[Tab1].PaintTo(_ACTH_BmTab1.Canvas,_pc.Pages[Tab1].Left,_pc.Pages[Tab1].Top);

 _ACTH_BmTab2:= TBitmap.Create;
 _ACTH_BmTab2.Width:= _pc.Width;
 _ACTH_BmTab2.Height:= _pc.Height;
 _ACTH_BmTab2.Canvas.Brush:= _pc.Brush;
 _ACTH_BmTab2.Canvas.FillRect(rect(0,0,_pc.Width,_pc.Height));
 _pc.Pages[Tab2].PaintTo(_ACTH_BmTab2.Canvas,_pc.Pages[Tab2].Left,_pc.Pages[Tab2].Top);

 _ACTH_uTimerID:= 0;
 //_ACTH_Speed:= 50;
 _ACTH_shiftTab1:= 0;
 _ACTH_shiftTab2:= _ACTH_BmTab1.Width*_ACTH_Direction;

 _pc.Canvas.Lock;
 _ACTH_uTimerID:=timeSetEvent(_ACTH_Speed,500,@_onTime,0,TIME_ONESHOT);
 while _ACTH_shiftTab2<>0 do ;
 _pc.Canvas.Unlock;

 _ACTH_BmTab1.Free;
 _ACTH_BmTab2.Free;
end;

procedure pcaSetActivTab(Pc:TPageControl; Tab: TTabSheet);
var
 _i1,_i2: integer;
begin
 _Initialization(Pc);
 _i1:= _pc.ActivePageIndex;
 _pc.ActivePage:= nil;
 _i2:= tab.PageIndex;
 pcaAnimatingChangeTabSheet(Pc,_i1,_i2);
 _pc.ActivePage:= Tab;
end;

procedure pcaGoNextTab(Pc:TPageControl);
 var
 _i: Integer;
 _CurPage: TTabSheet;
begin
 _Initialization(Pc);
 _i:= _pc.ActivePageIndex;
 if _i=_pc.PageCount-1 then EXIT;
 _i:= _i+1;
 _CurPage:= _pc.ActivePage;
 pcaSetActivTab(_pc, _pc.Pages[_i]);
end;

procedure pcaGoBackTab(Pc:TPageControl);
var
 _i: integer;
 _CurPage: TTabSheet;
begin
 _Initialization(Pc);
 _i:= _pc.ActivePageIndex;
 if (_i<1) then EXIT;
 _CurPage:= _pc.ActivePage;
 _i:= _i-1;
 pcaSetActivTab(_pc,_pc.Pages[_i]);
end;



end.
