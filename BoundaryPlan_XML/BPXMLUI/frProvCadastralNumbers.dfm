object FrameProvCadastralNumber: TFrameProvCadastralNumber
  Left = 0
  Top = 0
  Width = 700
  Height = 300
  TabOrder = 0
  object PageControl1: TPageControl
    Left = 0
    Top = 25
    Width = 700
    Height = 275
    ActivePage = tsOther
    Align = alClient
    Style = tsFlatButtons
    TabOrder = 0
    object tsCadNum: TTabSheet
      Caption = 'tsCadNum'
      TabVisible = False
      object pnlCadastral: TPanel
        Left = 0
        Top = 0
        Width = 692
        Height = 265
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object PanelCentre: TPanel
          Left = 0
          Top = 0
          Width = 483
          Height = 265
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          object CadNum_ListCN_ListBox: TListBox
            Left = 0
            Top = 21
            Width = 483
            Height = 244
            Align = alClient
            BevelEdges = [beLeft]
            BevelKind = bkFlat
            BorderStyle = bsNone
            Color = 15790320
            ItemHeight = 13
            MultiSelect = True
            TabOrder = 0
            StyleElements = []
          end
          object CadNum_NewCN_MaskEdit: TMaskEdit
            Left = 0
            Top = 0
            Width = 483
            Height = 21
            Hint = #1050#1072#1076#1072#1089#1090#1088#1086#1074#1099#1081' '#1085#1086#1084#1077#1088
            Align = alTop
            BevelKind = bkFlat
            BorderStyle = bsNone
            Color = 15921123
            TabOrder = 1
            Text = ''
            StyleElements = []
          end
        end
        object PanelLeft: TPanel
          Left = 483
          Top = 0
          Width = 209
          Height = 265
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 0
          ExplicitLeft = 0
          object CadNum_ButtonGroup_Actions: TButtonGroup
            Left = 0
            Top = 0
            Width = 209
            Height = 265
            Align = alClient
            BevelEdges = [beLeft, beRight]
            BevelKind = bkFlat
            BorderStyle = bsNone
            ButtonOptions = [gboFullSize, gboShowCaptions]
            Images = formContainer.ilCommands
            Items = <
              item
                Action = Action_DoAdd
                Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1074' '#1089#1087#1080#1089#1086#1082' >'
              end
              item
                Action = Action_CopySel
                Caption = #1050#1086#1087#1080#1088#1086#1074#1072#1090#1100' '#1074#1099#1076#1077#1083#1077#1085#1086#1077
                ImageIndex = 18
              end
              item
                Action = Action_DelSel
                Caption = #1059#1076#1072#1083#1080#1090#1100' '#1074#1099#1076#1077#1083#1077#1085#1085#1099#1077
                ImageIndex = 1
              end
              item
                Action = Action_Paste
                Caption = #1042#1089#1090#1072#1074#1080#1090#1100' '#1089#1087#1080#1089#1086#1082' '#1080#1079' '#1073#1091#1092#1077#1088' '#1086#1073#1084#1077#1085#1072
              end>
            TabOrder = 0
          end
        end
      end
    end
    object tsDocuments: TTabSheet
      Caption = 'tsDocuments'
      ImageIndex = 1
      TabVisible = False
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
    end
    object tsOther: TTabSheet
      Caption = 'tsOther'
      ImageIndex = 2
      TabVisible = False
      object mmOther: TMemo
        Left = 0
        Top = 0
        Width = 692
        Height = 265
        Align = alClient
        BevelInner = bvNone
        BevelOuter = bvNone
        BorderStyle = bsNone
        TabOrder = 0
        OnChange = mmOtherExit
      end
    end
  end
  object tlbProvCadastral: TToolBar
    AlignWithMargins = True
    Left = 3
    Top = 3
    Width = 694
    Height = 19
    AutoSize = True
    ButtonHeight = 19
    ButtonWidth = 127
    Color = clBtnFace
    EdgeInner = esNone
    EdgeOuter = esNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsUnderline]
    List = True
    ParentColor = False
    ParentFont = False
    ParentShowHint = False
    ShowCaptions = True
    ShowHint = True
    TabOrder = 1
    object btnCadastralNumber: TToolButton
      Left = 0
      Top = 0
      AllowAllUp = True
      AutoSize = True
      Caption = #1050#1072#1076#1072#1089#1090#1088#1086#1074#1099#1081' '#1085#1086#1084#1077#1088
      Grouped = True
      ImageIndex = 0
      Style = tbsCheck
      OnClick = RadGInnerCadastralNumberClick
    end
    object btnDefinition: TToolButton
      Left = 118
      Top = 0
      Hint = 
        '|'#1091#1082#1072#1079#1099#1074#1072#1077#1090#1089#1103' '#1086#1073#1086#1079#1085#1072#1095#1077#1085#1080#1077' '#1086#1073#1088#1072#1079#1091#1077#1084#1086#1075#1086' '#1079#1077#1084#1077#1083#1100#1085#1086#1075#1086' '#1091#1095#1072#1089#1090#1082#1072','#13#10' '#1087#1086#1089#1088#1077 +
        #1076#1089#1090#1074#1086#1084' '#1082#1086#1090#1086#1088#1086#1075#1086' '#1086#1073#1077#1089#1087#1077#1095#1080#1074#1072#1077#1090#1089#1103' '#1076#1086#1089#1090#1091#1087
      AllowAllUp = True
      AutoSize = True
      Caption = #1054#1073#1086#1079#1085#1072#1095#1077#1085#1080#1077' '#1047#1059
      Grouped = True
      ImageIndex = 1
      ParentShowHint = False
      ShowHint = True
      Style = tbsCheck
      OnClick = RadGInnerCadastralNumberClick
    end
    object btnOther: TToolButton
      Left = 217
      Top = 0
      Hint = 
        '|'#1091#1082#1072#1079#1099#1074#1072#1102#1090#1089#1103' '#1089#1083#1086#1074#1072' '#171#1079#1077#1084#1083#1080' ('#1079#1077#1084#1077#1083#1100#1085#1099#1077' '#1091#1095#1072#1089#1090#1082#1080') '#13#10#1086#1073#1097#1077#1075#1086' '#1087#1086#1083#1100#1079#1086#1074#1072#1085 +
        #1080#1103#187' '#1080#1083#1080' '#171#1076#1086#1089#1090#1091#1087' '#1086#1073#1077#1089#1087#1077#1095#1077#1085' '#1087#1086#1089#1088#1077#1076#1089#1090#1074#1086#1084' '#13#10#1079#1086#1085#1099' '#8230#187' '#1076#1083#1103' '#1089#1083#1091#1095#1072#1077#1074', '#1077#1089#1083 +
        #1080' '#1086#1073#1088#1072#1079#1091#1077#1084#1099#1081' ('#1080#1079#1084#1077#1085#1077#1085#1085#1099#1081') '#13#10#1079#1077#1084#1077#1083#1100#1085#1099#1081' '#1091#1095#1072#1089#1090#1086#1082' '#1080#1084#1077#1077#1090' '#1085#1077#1087#1086#1089#1088#1077#1076#1089#1090#1074#1077 +
        #1085#1085#1099#1081' '#1076#1086#1089#1090#1091#1087' '#1082' '#13#10#1079#1077#1084#1083#1103#1084' '#1080#1083#1080' '#1079#1077#1084#1077#1083#1100#1085#1099#1084' '#1091#1095#1072#1089#1090#1082#1072#1084' '#1086#1073#1097#1077#1075#1086' '#1087#1086#1083#1100#1079#1086#1074#1072#1085#1080#1103 +
        #13#10' '#1080#1083#1080' '#1076#1086#1089#1090#1091#1087' '#1082' '#1090#1072#1082#1080#1084' '#1079#1077#1084#1083#1103#1084' '#1086#1073#1077#1089#1087#1077#1095#1080#1074#1072#1077#1090#1089#1103' '#1087#1086#1089#1088#1077#1076#1089#1090#1074#1086#1084' '#1079#1086#1085#1099' '#1089' '#1086 +
        #1089#1086#1073#1099#1084#1080' '#1091#1089#1083#1086#1074#1080#1103#1084#1080' '#1080#1089#1087#1086#1083#1100#1079#1086#1074#1072#1085#1080#1103' '#1090#1077#1088#1088#1080#1090#1086#1088#1080#1080' ('#1087'.65, 106 '#1058#1088#1077#1073#1086#1074#1072#1085#1080#1081')' +
        '.'#13#10'|3'
      AllowAllUp = True
      AutoSize = True
      Caption = #1048#1085#1086#1077
      Grouped = True
      ImageIndex = 2
      Style = tbsCheck
      OnClick = RadGInnerCadastralNumberClick
    end
    object btnDocuments: TToolButton
      Left = 258
      Top = 0
      Hint = 
        '|'#1087#1088#1077#1076#1085#1072#1079#1085#1072#1095#1077#1085' '#1076#1083#1103' '#1088#1077#1082#1074#1080#1079#1080#1090#1086#1074' '#1076#1086#1082#1091#1084#1077#1085#1090#1086#1074' '#1086#1089#1085#1086#1074#1072#1085#1080#1081#13#10' '#1091#1089#1090#1072#1085#1086#1074#1083#1077#1085#1080#1103 +
        ' '#1079#1086#1085#1099' ('#1087'. 106 '#1058#1088#1077#1073#1086#1074#1072#1085#1080#1081').'#13#10
      AllowAllUp = True
      AutoSize = True
      Caption = #1044#1086#1082#1091#1084#1077#1085#1090#1099'-'#1086#1089#1085#1086#1074#1072#1085#1080#1103
      Grouped = True
      ImageIndex = 3
      Style = tbsCheck
      OnClick = RadGInnerCadastralNumberClick
    end
  end
  object ActionList: TActionList
    Left = 648
    Top = 248
    object Action_DoAdd: TAction
      Caption = 'Action_DoAdd'
      ImageIndex = 0
      OnExecute = Action_DoAddExecute
    end
    object Action_DelSel: TAction
      Caption = 'Action_DelSel'
      ImageIndex = 2
      OnExecute = Action_DelSelExecute
    end
    object Action_CopySel: TAction
      Caption = 'Action_CopySel'
      ImageIndex = 1
      OnExecute = Action_CopySelExecute
    end
    object Action_Paste: TAction
      Caption = 'Action_Paste'
      ImageIndex = 3
      OnExecute = Action_PasteExecute
    end
    object actHint: TAction
      Caption = 'actHint'
    end
  end
end
