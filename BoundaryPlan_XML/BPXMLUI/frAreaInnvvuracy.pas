unit frAreaInnvvuracy;

{
   ���������� ������� ����������  �������
   (���������� �����  Area  (��� ����������� ))

   IXMLTExistParcel_SubParcels_InvariableSubParcel_Area
}
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, StdCtrls, ExtCtrls, Mask,

  STD_MP, ActnList, System.Actions,unRussLang;

type
  TFrameAreaInnccuracy = class(TFrame)
    AreaValue_MaskEdit: TMaskEdit;
    AreaValue_Label_Caption: TLabel;
    ActionList: TActionList;
    Action_AreaValue_Write: TAction;
    Action_Innccuracy_Write: TAction;
    procedure Action_AreaValue_WriteExecute(Sender: TObject);
    procedure Innccuracy_MaskEditKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }

    FXmlInvariable       :IXMLTExistParcel_SubParcels_InvariableSubParcel_Area;
    FXmlChangeInvariable : IXMLTChangeParcel_SubParcels_InvariableSubParcel_Area;
  protected
   procedure InitializationUI;
   procedure SetAreaInvariable(aArea: IXMLTExistParcel_SubParcels_InvariableSubParcel_Area);
  public
    { Public declarations }

   procedure UI_Unit_Write055To(const aArea: IXMLTExistParcel_SubParcels_InvariableSubParcel_Area);
   procedure UI_Unit_Write055ToInvariable(const aArea: IXMLTExistParcel_SubParcels_InvariableSubParcel_Area);

   procedure UI_AreaValue_WriteTo(const aArea: IXMLTExistParcel_SubParcels_InvariableSubParcel_Area);
   procedure UI_AreaValue_ReadOf(const aArea: IXMLTExistParcel_SubParcels_InvariableSubParcel_Area);
   function  UI_Areavalue_IsValid: boolean;

   procedure UI_WriteTo(const aArea: IXMLTExistParcel_SubParcels_InvariableSubParcel_Area);
   procedure UI_ReadOf(const aArea: IXMLTExistParcel_SubParcels_InvariableSubParcel_Area);


   function IsAssignedAreaInvariable: boolean;
   property XmlAreaInvariable: IXMLTExistParcel_SubParcels_InvariableSubParcel_Area read FXmlInvariable write SetAreaInvariable;
  end;

implementation

uses MsXMLAPI;


{$R *.dfm}

{ TFrameAreaInnccuracy }

procedure TFrameAreaInnccuracy.Action_AreaValue_WriteExecute(Sender: TObject);
begin
 if not self.IsAssignedAreaInvariable then raise Exception.Create(ExMSG_NotAssignedROOT);
  self.UI_AreaValue_WriteTo(self.FXmlInvariable);
  self.UI_Unit_Write055To(self.FXmlInvariable);
end;


procedure TFrameAreaInnccuracy.InitializationUI;
begin
 if self.IsAssignedAreaInvariable then
 begin
    self.UI_ReadOf(self.FXmlInvariable)
 end
 else begin
  self.AreaValue_MaskEdit.Text:= '';
 end;
end;

procedure TFrameAreaInnccuracy.Innccuracy_MaskEditKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if key = VK_RETURN then
  if (Sender is TCustomEdit) then
   TCustomEdit(Sender).Parent.SetFocus;
end;

function TFrameAreaInnccuracy.IsAssignedAreaInvariable: boolean;
begin
  Result := Self.FXmlInvariable <> nil;
end;


procedure TFrameAreaInnccuracy.SetAreaInvariable(
  aArea: IXMLTExistParcel_SubParcels_InvariableSubParcel_Area);
begin
 if self.FXmlInvariable=aArea then EXIT;
 self.FXmlInvariable:= aArea;
 self.InitializationUI;
end;


function TFrameAreaInnccuracy.UI_Areavalue_IsValid: boolean;
var
 _t: Double;
begin
 Result:= TryStrToFloat(Self.AreaValue_MaskEdit.Text,_t)
end;


procedure TFrameAreaInnccuracy.UI_AreaValue_ReadOf;
begin
 self.AreaValue_MaskEdit.Text:= MsXml_ReadChildNodeValue(aArea,cnstAtrArea);
end;


procedure TFrameAreaInnccuracy.UI_AreaValue_WriteTo;
begin
 if self.AreaValue_MaskEdit.Text='' then EXIT;
 if not self.UI_Areavalue_IsValid then begin
  self.UI_AreaValue_ReadOf(aArea);
  raise Exception.Create(format(ExMsg_Data_Invalid,[self.AreaValue_Label_Caption.Caption]));
 end;
 aArea.Area:= Strtoint(self.AreaValue_MaskEdit.Text);
end;




procedure TFrameAreaInnccuracy.UI_ReadOf;
begin
 self.UI_AreaValue_ReadOf(aArea);
end;


procedure TFrameAreaInnccuracy.UI_Unit_Write055To;
begin
 aArea.Unit_:= StdMp_Area_Unit;
end;

procedure TFrameAreaInnccuracy.UI_Unit_Write055ToInvariable(
  const aArea: IXMLTExistParcel_SubParcels_InvariableSubParcel_Area);
begin
 aArea.Unit_:= StdMp_Area_Unit;
end;

procedure TFrameAreaInnccuracy.UI_WriteTo;
begin
 self.UI_AreaValue_WriteTo(aArea);
 self.UI_Unit_Write055To(aArea);
end;

end.
