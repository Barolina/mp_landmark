<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="text" version="1.0" encoding="UTF-8" indent="yes"/>
<xsl:strip-space elements="*"/>

 <!-- =========================================================================================== -->
  <xsl:template match="/*" priority="1">
      <xsl:element name="M.Gis">
	         <xsl:apply-templates select="Package"/>    		 
	      </xsl:element>	
  </xsl:template>

 <!-- =========================================================================================== -->
 <xsl:variable name="letterQuote" >  <xsl:text>"</xsl:text></xsl:variable>
 <xsl:variable name="letterBackSpark" >  <xsl:text>",</xsl:text> </xsl:variable>
 <xsl:variable name="letterEnter" >  <xsl:text>"&#10;</xsl:text> </xsl:variable>
 <xsl:variable name="letterApos"> <xsl:text>&apos;</xsl:text> </xsl:variable>
 <xsl:variable name="onlyEnter" >  <xsl:text>&#10;</xsl:text> </xsl:variable>
 <xsl:variable name="onlyComma" >  <xsl:text>,</xsl:text> </xsl:variable>
 
 <!-- категория  земель -->
 <xsl:template match="Category" name="Category">
        <xsl:value-of select="concat($letterQuote,@Category,$letterQuote)"/>   
 </xsl:template>
 
  <xsl:template match="Utilization">
        <xsl:value-of select="concat($letterQuote,@Kind,$letterQuote)"/>   
 </xsl:template>
 
 <xsl:template name="getCategotyParcel">
     <xsl:choose>
            <xsl:when test="count(Category) = 0 ">
              <xsl:text>""</xsl:text>                 
            </xsl:when>
            <xsl:otherwise>
                 <xsl:apply-templates select ="Category"/>    
            </xsl:otherwise>
    </xsl:choose>  
 </xsl:template>
 
  <xsl:template name="getUtilization">
     <xsl:choose>
            <xsl:when test="count(Utilization) = 0 ">
              <xsl:text>""</xsl:text>                 
            </xsl:when>
            <xsl:otherwise>
                 <xsl:apply-templates select ="Utilization[position()=1]"/>    
            </xsl:otherwise>
    </xsl:choose>  
 </xsl:template>

<!--осноная  площадь  и др  площади -->
	<xsl:template match="Areas" name="Areas">
     <xsl:param name="prAreaPos" select="1"/>                   	   
     <xsl:value-of select="concat($letterQuote,self::node()/Area[position()=$prAreaPos]/Area[text()],$letterBackSpark)"/>	                     	          
     <xsl:value-of select="concat($letterQuote,self::node()/Area[position()=$prAreaPos]/Unit[text()],$letterBackSpark)"/>	                     	          
     <xsl:value-of select="concat($letterQuote,self::node()/Area[position()=$prAreaPos]/Innccuracy[text()],$letterBackSpark)"/>      	    
	</xsl:template>

<!-- =========================================================================================== -->
	 <xsl:template match="Package" name="Package">
		<xsl:apply-templates  select="//Cadastral_Block"/>
	 </xsl:template>
 
	 <xsl:template match="Cadastral_Block" name="Cadastral_Block">	
         <xsl:apply-templates select="Parcels"/>
	  </xsl:template> 
 
	 <xsl:template match="Parcels" name="Parcels">
		<xsl:apply-templates select="Parcel"/>
     </xsl:template>
	
	<xsl:template match="Parcel" name="Parcel">
	   <xsl:if test="count(child::Entity_Spatial) >0 ">              
			 <xsl:apply-templates select="Entity_Spatial">
				 <xsl:with-param name="vKV" select="parent::node()/parent::node()/@CadastralNumber"/> 
				 <xsl:with-param name="vKU" select="@CadastralNumber"/>  
				 <xsl:with-param name="vKNEZ" select="Unified_Land_Unit/Preceding_Land_Unit"/> 		 
			 </xsl:apply-templates>			         	        	      
			 <xsl:apply-templates select="Areas" />
             <xsl:call-template name="getCategotyParcel"/> 
            <xsl:value-of select="$onlyComma"/> 
             <xsl:call-template name="getUtilization"/> 
             <xsl:value-of select="$onlyEnter"/> 
       </xsl:if> 	       	
		<xsl:if test="count(Contours) >0 ">
            <xsl:apply-templates select="Contours" />   
        </xsl:if>         		
	</xsl:template>
 
<!-- =========================================================================================== -->
<!-- Описание пространственной составляющей сущности данных  -->	
 <xsl:template match="Entity_Spatial"  name="EntitySpatial">
	<xsl:param name="vKV" select="NoInf"/> <!--кадастровый номер квартала-->   
	<xsl:param name="vKU" select="NoInf"/> <!--кадастровый номер участка (может быть полным т.е. включать номер квартала)-->
	<xsl:param name="vKNEZ" select="NoInf"/> <!--кадастровый номер ЕЗ в состав которого входит данный зу--> 	
    
    <xsl:choose>
    
		<xsl:when test="contains($vKU,$vKV)"> <!--если в кад.номер. зу полный-->
			<xsl:choose>
				<xsl:when test="$vKNEZ !=''">
					<xsl:value-of select="concat($letterQuote,$vKNEZ,'(',$vKU,')',$letterBackSpark)"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="concat($letterQuote,$vKU,$letterBackSpark)"/>
				</xsl:otherwise>
			</xsl:choose>			
		</xsl:when>
		
		<xsl:otherwise>	
			<xsl:choose>
				<xsl:when test="$vKNEZ !=''"> <!--если в кад.номер. зу НЕ полный-->
					<xsl:value-of select="concat($letterQuote,$vKNEZ,'(',$vKV,$vKU,')',$letterBackSpark)"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="concat($letterQuote,$vKV,$vKU,$letterBackSpark)"/>
				</xsl:otherwise>
			</xsl:choose>			
		</xsl:otherwise>
		
	</xsl:choose>	

	 <xsl:value-of select="concat($letterQuote,$vKU,$letterBackSpark)"/>  
     <xsl:value-of select="concat($letterQuote,$vKNEZ,$letterBackSpark)"/>    
 </xsl:template>
 <!-- =========================================================================================== -->
  
<!-- =========================================================================================== -->
<xsl:template match="Contours">
		<xsl:variable name="vCategory" select="concat($letterQuote,parent::node()/Category/@Category,$letterQuote)"/>
		<xsl:variable name="vUtilization" select="concat($letterQuote,parent::node()/Utilization[position()=1]/@Kind,$letterQuote)"/>
	   
	    <xsl:for-each select="Contour">   
			 <xsl:apply-templates select="Entity_Spatial">
				 <xsl:with-param name="vKV" select="parent::node()/parent::node()/parent::node()/parent::node()/@CadastralNumber"/> 
				 <xsl:with-param name="vKU" select="concat(parent::node()/parent::node()/@CadastralNumber,'(',@Number_PP,')')"/> 
				 <xsl:with-param name="vKNEZ" select="Unified_Land_Unit/Preceding_Land_Unit"/>  			 
			 </xsl:apply-templates>		
			 <xsl:call-template name="Areas">
				<xsl:with-param name="prAreaPos" select="position()"></xsl:with-param>
			</xsl:call-template>

            <xsl:value-of select="$vCategory"/>   
            <xsl:value-of select="$onlyComma"/>
            <xsl:value-of select="$vUtilization"/>    
            <xsl:value-of select="$onlyEnter"/>                          
                     
            </xsl:for-each>
	</xsl:template>
 <!-- =========================================================================================== -->

</xsl:stylesheet>