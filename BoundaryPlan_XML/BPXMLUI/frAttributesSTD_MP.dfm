object FrameAttributedSTD_MP: TFrameAttributedSTD_MP
  Left = 0
  Top = 0
  Width = 716
  Height = 316
  TabOrder = 0
  object ListAppleField: TListBox
    Left = 0
    Top = 0
    Width = 716
    Height = 286
    Align = alClient
    BevelKind = bkFlat
    BorderStyle = bsNone
    Color = 15790320
    ItemHeight = 13
    MultiSelect = True
    ParentShowHint = False
    PopupMenu = PopupMenuActionDoc
    ShowHint = True
    TabOrder = 1
    StyleElements = []
  end
  object ToolBarAction: TToolBar
    AlignWithMargins = True
    Left = 3
    Top = 289
    Width = 710
    Height = 24
    Align = alBottom
    ButtonWidth = 175
    Caption = 'ToolBarAction'
    Color = clBtnFace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    List = True
    GradientDirection = gdHorizontal
    ParentColor = False
    ParentFont = False
    ShowCaptions = True
    TabOrder = 0
    Transparent = False
    object ToolButLoadDoc: TToolButton
      Left = 0
      Top = 0
      Caption = #1047#1072#1075#1088#1091#1079#1080#1090#1100' '#1076#1086#1082#1091#1084#1077#1085#1090#1099'....'
      ImageIndex = 0
      ParentShowHint = False
      ShowHint = True
      OnClick = SubParcels_Actions_ButtonGroupItems0Click
    end
  end
  object FileOpenDialog: TOpenDialog
    Filter = 'PDF|*.pdf'
    Options = [ofHideReadOnly, ofAllowMultiSelect, ofFileMustExist, ofEnableSizing]
    Left = 296
    Top = 144
  end
  object PopupMenuActionDoc: TPopupMenu
    Left = 200
    Top = 144
    object nDeleteAppliedFile: TMenuItem
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1076#1086#1082#1091#1084#1077#1085#1090'('#1099')'
      ShortCut = 16430
      OnClick = nDeleteAppliedFileClick
    end
  end
end
