unit frContractor;
{

  ����������� �������
  IXMLSTD_MP_Title_Contractor
}
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls,

  STD_MP, frFIO, Vcl.Mask, XMLIntf,unRussLang, JvExMask, JvToolEdit, JvMaskEdit,
  JvCheckedMaskEdit, JvDatePickerEdit;

type
  TFrameContractor = class(TFrame)
    NCertificate_Edit: TEdit;
    Organization_Edit: TEdit;
    Telephone_Edit: TEdit;
    Email_Edit: TEdit;
    Address_Edit: TEdit;
    Label4: TLabel;
    Label_Organization: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    pnlFIO: TPanel;
    Label1: TLabel;
    pnlContractor: TPanel;
    jvdtpckrdtDate: TJvDatePickerEdit;

    procedure NCertificate_EditExit(Sender: TObject);
    procedure Organization_EditExit(Sender: TObject);
    procedure Telephone_EditExit(Sender: TObject);
    procedure Email_EditExit(Sender: TObject);
    procedure Address_EditExit(Sender: TObject);
    procedure FIOSurname_EditKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FIOFirst_EditKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FIOPatronymic_EditKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure NCertificate_EditKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Organization_EditKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Telephone_EditKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Email_EditKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Address_EditKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure mskEdDateExit(Sender: TObject);
    procedure jvdtpckrdtDateExit(Sender: TObject);
  private
    { Private declarations }
    FXmlTitleContractor : IXMLSTD_MP_Title_Contractor;
    FFIO                : TFrameFIO;
    frmt                : TFormatSettings;
    
    function IsAssignedCadastral_Engineer: boolean;

 protected
   procedure InitializationUI;
   procedure SeTitleContractor(const aTitleContractor: IXMLSTD_MP_Title_Contractor);
  public
   { Public declarations }
   constructor Create(AOwner: TComponent); override;

   procedure SetTheFormatData(formatData  : string; const aSeparator : Char = '-');

   procedure UI_DataAttr_ReadOF(const aTitleContractor: IXMLSTD_MP_Title_Contractor);
   procedure UI_DataAttr_WriteTo (const aTitleContractor: IXMLSTD_MP_Title_Contractor);

   procedure UI_NCertificate_ReadOf(const aTitleContractor: IXMLSTD_MP_Title_Contractor);
   procedure UI_INN_ReadOf(const aTitleContractor: IXMLSTD_MP_Title_Contractor);
   procedure UI_Telephone_ReadOf(const aTitleContractor: IXMLSTD_MP_Title_Contractor);
   procedure UI_Email_ReadOf(const aTitleContractor: IXMLSTD_MP_Title_Contractor);
   procedure UI_Address_ReadOf(const aTitleContractor: IXMLSTD_MP_Title_Contractor);
   procedure UI_ReadOf(const aTitleContractor: IXMLSTD_MP_Title_Contractor);

   procedure UI_NCertificate_WriteTo(const aTitleContractor: IXMLSTD_MP_Title_Contractor);
   procedure UI_INN_WriteTo(const aTitleContractor: IXMLSTD_MP_Title_Contractor);
   procedure UI_Telephone_WriteTo(const aTitleContractor: IXMLSTD_MP_Title_Contractor);
   procedure UI_Email_WriteTo(const aTitleContractor: IXMLSTD_MP_Title_Contractor);
   procedure UI_Address_WriteTo(const aTitleContractor: IXMLSTD_MP_Title_Contractor);
   procedure UI_WriteTo(const aTitleContractor: IXMLSTD_MP_Title_Contractor);

   property XMLTitleContractor:IXMLSTD_MP_Title_Contractor read FXmlTitleContractor write SeTitleContractor;
  end;

implementation

uses MsXmlApi;
{$R *.dfm}

Resourcestring
  rsXML_NCertificate = 'N_Certificate';
  rsXML_INN = 'INN';
  rsXML_Telephone = 'Telephone';
  rsXML_Email = 'E_mail';
  rsXML_Address = 'Address';



{ TFrameCadastral_Engineer }

procedure TFrameContractor.Address_EditExit(Sender: TObject);
begin
 if not self.IsAssignedCadastral_Engineer then raise Exception.Create(ExMSG_NotAssignedROOT);
 self.UI_Address_WriteTo(self.FXmlTitleContractor);
end;

procedure TFrameContractor.Address_EditKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if key = VK_RETURN then
  if (Sender is TCustomEdit) then
   TCustomEdit(Sender).Parent.SetFocus;
end;

constructor TFrameContractor.Create(AOwner: TComponent);
begin
  inherited;
  self.FFIO:= TFrameFIO.Create(self.pnlFIO);
  self.FFIO.Parent:= self.pnlFIO;
  self.FFIO.Align:= alClient;
end;

procedure TFrameContractor.Email_EditExit(Sender: TObject);
begin
 if not self.IsAssignedCadastral_Engineer then raise Exception.Create(ExMSG_NotAssignedROOT);
 self.UI_Email_WriteTo(self.FXmlTitleContractor);
end;

procedure TFrameContractor.Email_EditKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if key = VK_RETURN then
  if (Sender is TCustomEdit) then
   TCustomEdit(Sender).Parent.SetFocus;
end;

procedure TFrameContractor.FIOFirst_EditKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if key = VK_RETURN then
  if (Sender is TCustomEdit) then
   TCustomEdit(Sender).Parent.SetFocus;
end;


procedure TFrameContractor.FIOPatronymic_EditKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
 if key = VK_RETURN then
  if (Sender is TCustomEdit) then
   TCustomEdit(Sender).Parent.SetFocus;
end;


procedure TFrameContractor.FIOSurname_EditKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if key = VK_RETURN then
  if (Sender is TCustomEdit) then
   TCustomEdit(Sender).Parent.SetFocus;
end;

procedure TFrameContractor.InitializationUI;
begin
 if self.IsAssignedCadastral_Engineer then
 begin
  self.UI_ReadOf(self.FXmlTitleContractor);
  self.FFIO.XMLFIO := FXmlTitleContractor.FIO;
 end
 else begin
  self.NCertificate_Edit.Text:= '';
  self.Organization_Edit.Text:= '';
  self.Telephone_Edit.Text:= '';
  self.Email_Edit.Text:= '';
  self.Address_Edit.Text:= '';
  self.jvdtpckrdtDate.Clear;
  FFIO.XMLFIO := nil;
 end;
end;

procedure TFrameContractor.Organization_EditExit(Sender: TObject);
begin
 if not self.IsAssignedCadastral_Engineer then raise Exception.Create(ExMSG_NotAssignedROOT);
 self.UI_INN_WriteTo(self.FXmlTitleContractor);
end;

procedure TFrameContractor.Organization_EditKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if key = VK_RETURN then
  if (Sender is TCustomEdit) then
   TCustomEdit(Sender).Parent.SetFocus;
end;

function TFrameContractor.IsAssignedCadastral_Engineer: boolean;
begin
 Result:= self.FXmlTitleContractor<>nil;
end;

procedure TFrameContractor.jvdtpckrdtDateExit(Sender: TObject);
begin
 if not self.IsAssignedCadastral_Engineer then raise Exception.Create(ExMSG_NotAssignedROOT);
 UI_DataAttr_WriteTo(FXmlTitleContractor);
end;

procedure TFrameContractor.mskEdDateExit(Sender: TObject);
begin
 if not self.IsAssignedCadastral_Engineer then raise Exception.Create(ExMSG_NotAssignedROOT);
 UI_DataAttr_WriteTo(FXmlTitleContractor);
end;

procedure TFrameContractor.NCertificate_EditExit(Sender: TObject);
begin
 if not self.IsAssignedCadastral_Engineer then raise Exception.Create(ExMSG_NotAssignedROOT);
 self.UI_NCertificate_WriteTo(self.FXmlTitleContractor);
end;

procedure TFrameContractor.NCertificate_EditKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
 if key = VK_RETURN then
  if (Sender is TCustomEdit) then
   TCustomEdit(Sender).Parent.SetFocus;
end;

procedure TFrameContractor.SeTitleContractor(const aTitleContractor: IXMLSTD_MP_Title_Contractor);
begin
 if self.FXmlTitleContractor=aTitleContractor then EXIT;
 self.FXmlTitleContractor:= aTitleContractor;
 self.InitializationUI;
end;

procedure TFrameContractor.SetTheFormatData(formatData: string; const aSeparator: Char);
begin
  frmt.DateSeparator := aSeparator;
  frmt.ShortDateFormat := formatData;
end;

procedure TFrameContractor.Telephone_EditExit(Sender: TObject);
begin
 if not self.IsAssignedCadastral_Engineer then raise Exception.Create(ExMSG_NotAssignedROOT);
 self.UI_Telephone_WriteTo(self.FXmlTitleContractor);
end;

procedure TFrameContractor.Telephone_EditKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if key = VK_RETURN then
  if (Sender is TCustomEdit) then
   TCustomEdit(Sender).Parent.SetFocus;
end;

procedure TFrameContractor.UI_Address_ReadOf;
begin
 self.Address_Edit.Text:= MsXml_ReadChildNodeValue(aTitleContractor,rsXML_Address);
end;

procedure TFrameContractor.UI_Address_WriteTo;
var
 _st: string;
begin
 _st:= self.Address_Edit.Text;
 if _st<>'' then
  aTitleContractor.Address:= _st
 else
  MsXml_RemoveChildNode(aTitleContractor,rsXML_Address);
end;

procedure TFrameContractor.UI_DataAttr_ReadOF(const aTitleContractor: IXMLSTD_MP_Title_Contractor);
var
  _node: IXMLNode;
  str  : string;
begin
  str := MsXml_ReadAttribute(aTitleContractor, cnstXmlDate);
  if str <> '' then
     jvdtpckrdtDate.Text := str
  else jvdtpckrdtDate.Text := '';
end;

procedure TFrameContractor.UI_DataAttr_WriteTo(const aTitleContractor: IXMLSTD_MP_Title_Contractor);
 var dataTime  : TDateTime;
begin
   aTitleContractor.Date := jvdtpckrdtDate.Text;
end;

procedure TFrameContractor.UI_Email_ReadOf;
begin
 self.Email_Edit.Text:= MsXml_ReadChildNodeValue(aTitleContractor,rsXML_Email);
end;

procedure TFrameContractor.UI_Email_WriteTo;
var
 _st: string;
begin
 _st:= self.Email_Edit.Text;
 if _st<>'' then
  aTitleContractor.E_Mail:= _st
 else
  MsXml_RemoveChildNode(aTitleContractor,rsXML_Email)
end;

procedure TFrameContractor.UI_INN_ReadOf;
begin
 self.Organization_Edit.Text:= MsXml_ReadChildNodeValue(aTitleContractor,cnstXmlOrganization);
end;

procedure TFrameContractor.UI_INN_WriteTo;
var
 _st: string;
begin
 _st:= self.Organization_Edit.Text;
 if _st<>'' then
  aTitleContractor.Organization:= _st
 else
  MsXml_RemoveChildNode(aTitleContractor,rsXML_INN)
end;

procedure TFrameContractor.UI_NCertificate_ReadOf;
begin
 self.NCertificate_Edit.Text:= MsXml_ReadChildNodeValue(aTitleContractor,rsXML_NCertificate);
end;

procedure TFrameContractor.UI_NCertificate_WriteTo;
var
 _st: string;
begin
 _st:= self.NCertificate_Edit.Text;
 if _st<>'' then
  aTitleContractor.N_Certificate:= _st
 else
  MsXml_RemoveChildNode(aTitleContractor,rsXML_NCertificate)
end;

procedure TFrameContractor.UI_ReadOf;
begin
 self.UI_NCertificate_ReadOf(aTitleContractor);
 self.UI_INN_ReadOf(aTitleContractor);
 self.UI_Telephone_ReadOf(aTitleContractor);
 self.UI_Email_ReadOf(aTitleContractor);
 self.UI_Address_ReadOf(aTitleContractor);

 self.UI_DataAttr_ReadOF(aTitleContractor);
end;

procedure TFrameContractor.UI_Telephone_ReadOf;
begin
 self.Telephone_Edit.Text:= MsXml_ReadChildNodeValue(aTitleContractor,rsXML_Telephone);
end;

procedure TFrameContractor.UI_Telephone_WriteTo;
var
 _st: string;
begin
 _st:= self.Telephone_Edit.Text;
 if _st<>'' then
  aTitleContractor.Telephone:= _st
 else
  MsXml_RemoveChildNode(aTitleContractor,rsXML_Telephone)
end;

procedure TFrameContractor.UI_WriteTo;
begin
 self.UI_NCertificate_WriteTo(aTitleContractor);
 self.UI_INN_WriteTo(aTitleContractor);
 self.UI_Telephone_WriteTo(aTitleContractor);
 self.UI_Email_WriteTo(aTitleContractor);
 self.UI_Address_WriteTo(aTitleContractor);

 self.UI_DataAttr_WriteTo(aTitleContractor);
end;

end.
