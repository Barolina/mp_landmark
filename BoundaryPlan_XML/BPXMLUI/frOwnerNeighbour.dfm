object FrameOwnerNeighbour: TFrameOwnerNeighbour
  Left = 0
  Top = 0
  Width = 938
  Height = 379
  Color = clBtnFace
  ParentBackground = False
  ParentColor = False
  TabOrder = 0
  object Attribute_Panel: TPanel
    Left = 0
    Top = 0
    Width = 938
    Height = 44
    Align = alTop
    BevelEdges = [beLeft, beRight]
    BevelKind = bkFlat
    BevelOuter = bvNone
    ParentBackground = False
    TabOrder = 0
    ExplicitWidth = 939
    object Attribute_NameOwner_Label: TLabel
      Left = 3
      Top = 5
      Width = 108
      Height = 13
      Caption = #1055#1088#1072#1074#1086#1086#1073#1083#1072#1076#1072#1090#1077#1083#1100
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Attribute_ContactAddress_Lab: TLabel
      Left = 415
      Top = 5
      Width = 114
      Height = 13
      Caption = #1050#1086#1085#1090#1072#1082#1090#1085#1099#1081' '#1072#1076#1088#1077#1089' '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Attribute_NameOwner: TEdit
      Left = 1
      Top = 19
      Width = 411
      Height = 21
      BevelKind = bkFlat
      BorderStyle = bsNone
      Color = 15921123
      TabOrder = 0
      OnExit = ActionChange_Attribute_DefOrNumPPExecute
      OnKeyUp = Attribute_NameOwnerKeyUp
    end
    object Attribute_ContactAddress: TEdit
      Left = 415
      Top = 19
      Width = 516
      Height = 21
      BevelKind = bkFlat
      BorderStyle = bsNone
      Color = 15921123
      TabOrder = 1
      OnExit = Attribute_ContactAddressExit
      OnKeyUp = Attribute_NameOwnerKeyUp
    end
  end
  object grpDocuments: TGroupBox
    Left = 0
    Top = 44
    Width = 938
    Height = 335
    Align = alClient
    Caption = #1056#1077#1082#1074#1080#1079#1080#1090#1099' '#1076#1086#1082#1091#1084#1077#1085#1090#1086#1074' '#1087#1086#1076#1090#1074#1077#1088#1078#1076#1072#1102#1097#1080#1093' '#1087#1088#1072#1074#1086' '#1085#1072' '#1079#1077#1084#1077#1083#1100#1085#1099#1081' '#1091#1095#1072#1089#1090#1086#1082
    Color = clBtnFace
    DoubleBuffered = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentDoubleBuffered = False
    ParentFont = False
    TabOrder = 1
    ExplicitWidth = 939
    object Area_Panel: TPanel
      AlignWithMargins = True
      Left = 5
      Top = 18
      Width = 928
      Height = 312
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentBackground = False
      ParentFont = False
      TabOrder = 0
      VerticalAlignment = taAlignTop
      ExplicitWidth = 929
    end
  end
  object ActionList: TActionList
    Left = 600
    Top = 256
    object ActionChange_Attribute_DefOrNumPP: TAction
      Caption = 'ActionChange_Attribute_DefOrNumPP'
      OnExecute = ActionChange_Attribute_DefOrNumPPExecute
    end
    object ActionChange_ExistingOrNew: TAction
      Caption = 'ActionChange_ExistingOrNew'
    end
    object ActionChange_Attribute_ContactAdress: TAction
      Caption = 'ActionChange_Attribute_ContactAdress'
      OnExecute = ActionChange_Attribute_ContactAdressExecute
    end
  end
end
