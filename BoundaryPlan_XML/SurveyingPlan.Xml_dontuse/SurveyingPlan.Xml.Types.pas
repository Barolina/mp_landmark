unit SurveyingPlan.Xml.Types;
{$define TSpPointFormatInputData}
interface
 uses
 {$region 'System'}
  System.Rtti,
  System.Math,
  System.SysUtils,
  system.Classes,
  System.Generics.Collections,
  System.Generics.Defaults,
 {$endregion} 
 MSMType_, MSMath_
 ;

 type
  {$region 'TSpStrValue'}
  TSpStrValue = type string;
  TSpStrValueHelper = record helper for TSpStrValue
   function IsEmpty: boolean;
   function IsError: boolean;
   function IsValid: boolean;
  end;
  {$endregion}

  {$REGION 'TSpPoint = �����'}
  ///	<summary>
  ///	  ����� ������������ ���������� ���������tOrdinate �STD_MPV04
  ///	</summary>
  TSpPoint = class(TMSMPoint)
   private
    FNumGeopoint: TValue; //����� ������� ����� (��.)
    FGeopointZacrep: TValue; //����������� ��.
    FDeltaGeopoint: TValue; //����������� ����������� ��������� ��.
    FPointPref: TValue; //������� ��.

    procedure DoSetNumGeopoint(const Value: TSpStrValue);
    procedure DoPointPref(const Value: TSpStrValue);
   protected
    {$ifdef TSpPointFormatInputData}
    procedure SetX(Value: Double); override;
    procedure SetY(Value: Double); override;
    procedure SetCaption(Value: string); override;
    {$endif}

    procedure setNumGeopoint(const Value: TSpStrValue);
    function getNumGeopoint: TSpStrValue;
    procedure setGeopointZacrep(const value: TSpStrValue);
    function getGeopointZacrep:TSpStrValue;
    procedure setDeltaGeopoint(const value:TSpStrValue);
    function getDeltageopoint: TSpStrValue;
    procedure setPointPref(const Value: TSpStrValue);
    function getPointPref: TSpStrValue;

    //������ ������������� caption � ������ � PointPref � NumGeopoint
    procedure ParseCaption;
    //������ PointPref + NumGeopoint � caption
    procedure BuildCaption;
    //�������� �����
    function DoCreateCopy: TSpPoint;
    //�������� ������
    procedure DoAssingnTo(aDest: TSpPoint); virtual;
    {$region 'protected property'}
    //����� ������� �����. ����������� ������ ������������� ��������
    //��� ������ ������ ����� �������� ��������� � ���� caption ������������� ������
    property NumGeopoint: TSpStrValue read getNumGeopoint write setNumGeopoint;
    property GeopointZacrep: TSpStrValue read getGeopointZacrep write setGeopointZacrep;
    //����������� ����������� ��������� �����. ��������� ��������
    //������������� ����� (������������� ����������� �� ������ ��������� �������)
    property DeltaGeopount: TSpStrValue  read getDeltageopoint write setDeltaGeopoint;
    //������� ������� �����. ������������ �������� ����� ����� ��� ������
    //�� ������ �������� �������"�" ���� ������������ ������.
    //��� ������ ������ ����� �������� ��������� � ���� caption ������������� ������
    property PointPref:TSpStrValue read getPointPref write setPointPref;
    //���������� ���������� ����� PointPref �NumGeopoint
    //��� ������ ������, ���������������� ������������ �����
    //��������������� ������� ����������������� ������ �����������
    //����������� �������� � � ���� PointPref � NumGeopoint.�
    property Caption;
    {$endregion}
   public const
    PrefNewPoint:char = '�';
   public
    function IsEqualOrdinate(aValue: TSpPoint): boolean; virtual;
    function GetCopy:TSpPoint;
    procedure Assign(Source: TSpPoint);

    property X;
    property Y;
  end;
  {$ENDREGION}

  {$region 'TSpNodeBorder = ���� �������'}
  ///	<summary>
  ///	  ����� ���������� �������
  ///	</summary>
  TSpNodeBorder = class
   {$REGION 'public type'}
   public Type
    {$region 'TSpNb_Kind'}
    ///	<summary>
    ///	  ��� ����� ������� (��� ����� �������)
    ///	</summary>
    TSpNb_Kind = (
      ///	<summary>
      ///	  �������� �� ����������
      ///	</summary>
      SpNb_kUndefined,

      ///	<summary>
      ///	  �����, ��������� �����
      ///	</summary>
      SpNb_kCreated,

      ///	<summary>
      ///	  ������������, ��������� �����
      ///	</summary>
      SpNb_kRemoved,

      ///	<summary>
      ///	  ������������, ������������ �����
      ///	</summary>
      SpNb_kExist,

      ///	<summary>
      ///	  ������������, ���������� �����
      ///	</summary>
      SpNb_kModified
    );
    {$endregion}
    TKinds = set of TSpNb_Kind;
    TSpNb_PointKind = (SpNb_pkNew,SpNb_pkOld);
    TSpNewPoint = class(TSpPoint)
    public
      function GetCopy:TSpNewPoint;
      property NumGeopoint;
      property GeopointZacrep;
      property DeltaGeopount;
      property PointPref;
      property Caption;
    end;
    TSpOldPoint = class(TSpPoint)
    public
     function GetCopy:TSpOldPoint;
     property NumGeopoint;
     property DeltaGeopount;
     property Caption;
    end;

   {$ENDREGION}
   private
    FTypeUnit: string;
    FOldPoint: TSpOldPoint;
    FNewPoint: TSpNewPoint;

   protected
    function GetKind: TSpNb_Kind;
    procedure SetKind(Value: TSpNb_Kind);
    function GetPoint(aKind: TSpNb_PointKind): TSpPoint;
    function IsExist(aKind: TSpNb_PointKind): boolean;

    //�������� �����
    function doCreateCopy: TSpNodeBorder;
    //�������� ������
    procedure doAssingnTo(aDest: TSpNodeBorder); virtual;

   {$REGION 'public'}
   public
    function getCopy: TSpNodeBorder;
    function IsValid: boolean;
    property TypeUnit: string read FTypeUnit write FTypeUnit;
    property OldPoint: TSpOldPoint read FOldPoint;
    property NewPoint: TSpNewPoint read FNewPoint;
    property Point[aKind: TSpNb_PointKind]: TSpPoint read GetPoint; default;
    property IsExistPoint[aKind: TSpNb_PointKind]: boolean read IsExist;
    ///	<summary>
    ///	  ��������� �������. ���� ��������� �������������
    ///	  ����������(�������������) ����� OldPoint � NewPoint
    ///	</summary>
    ///	<remarks>
    ///	  <para>
    ///	    SpNb_kUndefined - Old&amp;New = nil;
    ///	  </para>
    ///	  <para>
    ///	    SpNb_kCreated - Old=nil
    ///	  </para>
    ///	  <para>
    ///	    SpNb_kRemoved - New = nil
    ///	  </para>
    ///	  <para>
    ///	   SpNb_kExist, SpNb_kModified - Old&amp;New&lt;&gt;nil
    ///	  </para>
    ///	</remarks>
    property Kind: TSpNb_Kind read GetKind write SetKind;
    {$ENDREGION}
   end;
   {$endregion}

  {$region 'TSpBorderLandplot = ������� ��'}
  TSpBorderLandplot = class (TObjectList<TSpNodeBorder>)
  {$REGION 'public type'}
  public type
   {$region 'Kind'}
   ///	<summary>
   ///	  ��� �������� �������
   ///	</summary>
   TSpBl_Kind = (
     ///	<summary>
     ///	  ��� �� ���������
     ///	</summary>
     SpBl_kUndefined,

     ///	<summary>
     ///	  �����, ���������������
     ///	</summary>
     SpBl_kCreated,

     ///	<summary>
     ///	  ������������, ��������� ��������������
     ///	</summary>
     SpBl_kRemoved,

     ///	<summary>
     ///	  ������������, ���������� ������� �������
     ///	</summary>
     SpBl_kModified
   );
   TSpBl_Kinds = set of TSpBl_Kind;
   {$endregion}
   {$region 'BorderKind'}
   ///	<summary>
   ///	  ��� �������
   ///	</summary>
   ///	<remarks>
   ///	  <para>
   ///	    ������� "�������" � "����������" ������� ��������������� ��������
   ///	    ������� ���������� ����� (�������������)
   ///	  </para>
   ///	  <para>
   ///	    �(��. ����� getBorderKindOfA)
   ///	  </para>
   ///	</remarks>
   TSpBl_BorderKind = (
     ///	<summary>
     ///	  �����������
     ///	</summary>
     SpBl_bkUndefined,

     ///	<summary>
     ///	  ������� �������
     ///	</summary>
     SpBl_bkOuter,

     ///	<summary>
     ///	  ��������� �������
     ///	</summary>
     SpBl_bkInner
   );
   {$endregion}
   {$region 'ValidKind'}
   TSpBl_ValidKind = (SpBl_vkCount,SpBl_vkClosed);
   TSpBl_ValidKindHelper = record helper for TSpBl_ValidKind
    function ToText: string;
   end;
   TSpBl_ValidKinds = set of TSpBl_ValidKind;
   TSpBl_ValidKindsHelper = record helper for TSpBl_ValidKinds
    strict private
     class var
      FAll: TSpBl_ValidKinds;
     class function initAll: TSpBl_ValidKinds; static; //����������� "������" ���������
     class function getAll:TSpBl_ValidKinds; static;
    public
     function ToText: string;
     class property All: TSpBl_ValidKinds read getAll;
   end;
   {$endregion}
  {$ENDREGION}
  private
   function getBorderKindOf(const aPoints: TMSMPoints): TSpBl_BorderKind;
   function getPoints(const aDest: TMSMPoints; aKind: TSpNodeBorder.TSpNb_PointKind): integer;
   function getBorderKindByOrdinateKind(aKind: TSpNodeBorder.TSpNb_PointKind): TSpBl_BorderKind;
   procedure setBorderKindByOrdinateKind(aKind: TSpNodeBorder.TSpNb_PointKind; aValue: TSpBl_BorderKind);
  protected
   function getKind: TSpBl_Kind;
   procedure setKind(const value: TSpBl_Kind);
   function getBorderKind: TSpBl_BorderKind;
   procedure setBorderKind(aValue: TSpBl_BorderKind);

   function IsValidByClosed: boolean; //��������� �� ���������� �����
   function IsValidByCountItem: boolean; //��������� �� ���-�� �����
  public
   function isValid: boolean;
   ///	<summary>
   ///	  �������� �� ���������� ������� �� ��������� ��-�� ����������
   ///	</summary>
   ///	<param name="aKinds">
   ///	  ���� ���������� �� ������� �������������� �������� �������� ����������
   ///	</param>
   ///	<returns>
   ///	  ����������� �������� ��-�� ���������� ���� ���������� �� ���������
   ///	  ���������, ������� ������ ���������.�
   ///	</returns>
   function isValidEx(aKinds: TSpBl_ValidKinds): TSpBl_ValidKinds;

   ///	<summary>
   ///	  �������� ��-�� ��������� �� ��������������� ��������� ���������
   ///	</summary>
   function getInvalid: TSpBl_ValidKinds;

   ///	<summary>
   ///	  �������� ������� �������������� ������������� ������ ��������.
   ///	</summary>
   ///	<param name="aPointsKind">
   ///	  ��� ����� ���������� ������ �������
   ///	</param>
   ///	<returns>
   ///	  �������� ��������! �.�. �.�.������������� (������� �� �����������
   ///	  ������������� � ��� ��������� �� ���� ������� - borderkind)
   ///	</returns>
   function GetAreaEx(aPointsKind: TSpNodeBorder.TSpNb_PointKind): double;

   ///	<summary>
   ///	  �������� ������� �������������� ������������� ������ ��������.
   ///	</summary>
   ///	<remarks>
   ///	  ���� ������� ��������� ������� �� ������������ ��� "������"���
   ///	  �������� ������� ��������� �� ��� ����� ��������������� ���� "�����"
   ///	  �����
   ///	</remarks>
   function GetArea: double;
   
   property Kind: TSpBl_Kind read getKind write setKind;
   property BorderKind: TSpBl_BorderKind read getBorderKind write setBorderKind;
  end;
  {$endregion}

  {$region 'TSpMBordersLandplot = ��������������(����������) ������� ��'}
  TSpMBordersLandplot = class(TObjectList<TSpBorderLandplot>)
   {$region 'public type'}
   public type
   {$region 'ValidKind'}
   TSpMBl_ValidKind = (SpMBl_vkCount,SpMBl_vkItems,SpMBl_vkBorderKind{,SpMBl_vkSpatialPosition,SpMBl_vkSelfIntersectig});
   TSpMBl_ValidKindHelper = record helper for TSpMBl_ValidKind
    function ToText:string;
   end;
   TSpMBl_ValidKinds = set of TSpMBl_ValidKind;
   TSpMBl_ValidKindsHelper = record helper for TSpMBl_ValidKinds
    strict private
     class var
      FAll: TSpMBl_ValidKinds;
     class function initAll: TSpMBl_ValidKinds; static; //���������� "������" ���������
     class function getAll:TSpMBl_ValidKinds; static;
    public
     class property All: TSpMBl_ValidKinds read getAll;
     function ToText: string;
   end;
   {$endregion}
   {$endregion}
   private
    //FBorder
   protected
    function getKind: TSpBorderLandplot.TSpBl_Kind;
   // procedure setKind(const value: TSpBorderLandplot.TSpBl_Kind);
    
    function isValidByCount: boolean;
    function isValidByItems: boolean;
    function isValidByBorderKind: boolean;
    //function isValidBySpatialPosition: boolean;
    //function isValidBySelfIntersectig: boolean;
   public
    function isValidEx(aKinds: TSpMBl_ValidKinds): TSpMBl_ValidKinds;
    function getInvalid: TSpMBl_ValidKinds;
    function isValid: boolean;

   ///	<summary>
   ///	  �������� ������� �������������� ������������� ������� ���������.
   ///	  ������ � ������ ������� ���������/������_���� ������� (�����
   ///	  ������������ ��� ��������), ����������� - ����������� (�������), �����
   ///	  ������� ����������� �������� �������� ������� ������� ������� � �����
   ///	  �������� ����������.
   ///	</summary>
   ///	<param name="aPointsKind">
   ///	  ��� �����
   ///	</param>
   ///	<returns>
   ///	  ����� ���� ������������ ������ � ������ ����������� ����� ������
   ///	</returns>
   ///	<remarks>
   ///	  ��� ������� ������������ ������������ � �������������
   ///	</remarks>
   function GetAreaEx(aPointsKind: TSpNodeBorder.TSpNb_PointKind): double;
   ///	<summary>
   ///	  �������� ������� �������������� ������������� ������� ���������.
   ///	</summary>
   ///	<remarks>
   ///	  ���� ������� ��������� ������� �� ������������ ��� "������"���
   ///	  �������� ������� ��������� �� ���, ����� ��������������� ���� "�����"
   ///	  �����
   ///	</remarks>
   function GetArea: double;
    
  end;
  {$endregion}

const
 esErrorValue: string = 'error';

implementation

uses SurveyingPlan.Xml.Types.ResStr;

{$region 'TSpPoint '}
procedure TSpPoint.Assign(Source: TSpPoint);
begin
 Source.DoAssingnTo(self);
end;

procedure TSpPoint.BuildCaption;
begin
 self.DoSetCaption(self.PointPref+self.NumGeopoint);
end;

procedure TSpPoint.DoPointPref(const Value: TSpStrValue);
begin
 if value.IsEmpty then begin
  self.FNumGeopoint:= nil;
  EXIT;
 end;
 if AnsiLowerCase(Value)=AnsiLowerCase(TSpPoint.PrefNewPoint) then
  self.FDeltaGeopoint:= TSpPoint.PrefNewPoint
 else
  self.FDeltaGeopoint:= esErrorValue;
end;

procedure TSpPoint.DoSetNumGeopoint(const Value: TSpStrValue);
var
 _int: integer;
begin
 if value.IsEmpty then begin
  self.FNumGeopoint:= nil;
  EXIT;
 end;
 if TryStrToInt(Value,_int) then
  self.FNumGeopoint:= Value
 else
  self.FNumGeopoint:= esErrorValue;
end;

function TSpPoint.GetCopy: TSpPoint;
begin
 Result:= self.DoCreateCopy;
end;

procedure TSpPoint.DoAssingnTo(aDest: TSpPoint);
begin
 aDest.X:= self.X;
 aDest.Y:= self.Y;
 aDest.NumGeopoint:= self.NumGeopoint;
 aDest.GeopointZacrep:= self.GeopointZacrep;
 aDest.DeltaGeopount:= self.DeltaGeopount;
 aDest.PointPref:= self.PointPref;
end;

function TSpPoint.DoCreateCopy: TSpPoint;
begin
 Result:= TSpPoint(self.ClassType.Create);
 self.DoAssingnTo(Result);
end;

function TSpPoint.getDeltageopoint: TSpStrValue;
begin
 Result:= self.FDeltaGeopoint.AsString;
end;

function TSpPoint.getGeopointZacrep: TSpStrValue;
begin
 Result:= self.FGeopointZacrep.AsString;
end;

function TSpPoint.getNumGeopoint: TSpStrValue;
begin
 Result:= self.FNumGeopoint.AsString;
end;

function TSpPoint.getPointPref: TSpStrValue;
begin
 Result:= self.FPointPref.AsString;
end;

function TSpPoint.IsEqualOrdinate(aValue: TSpPoint): boolean;
begin
 if not self.IsEqualPos(aValue) then EXIT(FALSE);
 if self.NumGeopoint<>aValue.NumGeopoint then EXIT(FALSE);
 if self.getGeopointZacrep<>aValue.GeopointZacrep then EXIT(FALSE);
 if self.DeltaGeopount<>aValue.DeltaGeopount then EXIT(FALSE);
 if self.PointPref<>aValue.PointPref then EXIT(FAlSE);
 Result:= TRUE;
end;

procedure TSpPoint.ParseCaption;
begin
 if self.Caption='' then begin
  self.FNumGeopoint:= nil;
  self.FPointPref:= nil;
  EXIT;
 end;
 if AnsiLowerCase(self.Caption[low(self.Caption)])=AnsiLowerCase(TSpPoint.PrefNewPoint) then begin
  self.DoPointPref(TSpPoint.PrefNewPoint);
  self.DoSetNumGeopoint(Copy(self.Caption,Low(self.Caption)+1,Length(self.Caption)-1));
 end;
end;

procedure TSpPoint.setDeltaGeopoint(const value: TSpStrValue);
const
 _cRoundvalue = -1;
var
 _float: Double;
begin
 if value.IsEmpty then begin
  self.FDeltaGeopoint:= nil;
  EXIT;
 end;
 if TryStrToFloat(value,_float) then
  self.FDeltaGeopoint:= RoundTo(_float,_cRoundvalue)
 else
  self.FDeltaGeopoint:= esErrorValue;
end;

procedure TSpPoint.setGeopointZacrep(const value: TSpStrValue);
begin
 if value.IsEmpty then begin
  self.FGeopointZacrep:= nil;
  EXIT;
 end;
 self.FGeopointZacrep:= value;
end;

procedure TSpPoint.setNumGeopoint(const Value: TSpStrValue);
begin
  self.DoSetNumGeopoint(Value);
  self.BuildCaption;
end;

procedure TSpPoint.setPointPref(const Value: TSpStrValue);
begin
 self.DoPointPref(Value);
 self.BuildCaption;
end;

{$ifdef TSpPointFormatInputData}
procedure TSpPoint.SetCaption(Value: string);
begin
 inherited;
 self.ParseCaption;
end;

procedure TSpPoint.SetX(Value: Double);
begin
 Value:= RoundTo(value,-2);
 inherited;
end;

procedure TSpPoint.SetY(Value: Double);
begin
 Value:= RoundTo(value,-2);
 inherited;
end;
{$endif}

{$endregion}

{$region 'TSpStrValueHelper'}
{ TSpStrValueHelper }

function TSpStrValueHelper.IsEmpty: boolean;
begin
 Result:= self='';
end;

function TSpStrValueHelper.IsError: boolean;
begin
 Result:= esErrorValue=self;
end;

function TSpStrValueHelper.IsValid: boolean;
begin
 Result:= not(self.IsEmpty or self.IsError);
end;

{$endregion}

{$region 'TSpNodeBorder'}
{ TSpNodeBorder }

function TSpNodeBorder.GetPoint(aKind: TSpNb_PointKind): TSpPoint;
begin
 if akind =SpNb_pkNew then
  Result:= self.FNewPoint
 else  
  Result:= self.FOldPoint;
end;

function TSpNodeBorder.IsExist(aKind: TSpNb_PointKind): boolean;
begin
 if aKind = SpNb_pkNew then 
   Result:= Assigned(self.FNewPoint)
 else   
   Result:= Assigned(self.FOldPoint);
end;

function TSpNodeBorder.IsValid: boolean;
begin
 Result:= self.Kind<>SpNb_kUndefined;
end;

procedure TSpNodeBorder.SetKind(Value: TSpNb_Kind);
begin
 case value of
   SpNb_kUndefined: begin
    FreeAndNil(self.FOldPoint);
    FreeAndNil(self.FNewPoint);
   end;
   SpNb_kCreated: begin
    FreeAndNil(self.FOldPoint);
    if not Assigned(self.FNewPoint) then
      self.FNewPoint:= TSpNewPoint.Create(0,0);
   end;
   SpNb_kRemoved: begin
    FreeAndNil(self.FNewPoint);
    if not Assigned(self.FOldPoint) then
     self.FOldPoint:= TSpOldPoint.Create(0,0);
   end;
   SpNb_kModified, SpNb_kExist:begin
    if not Assigned(self.FOldPoint) then
     self.FOldPoint:= TSpOldPoint.Create(0,0);
    if not Assigned(self.FNewPoint) then
      self.FNewPoint:= TSpNewPoint.Create(0,0);
   end;
 end;
end;

procedure TSpNodeBorder.DoAssingnTo(aDest: TSpNodeBorder);
begin
 aDest.SetKind(self.GetKind);
 aDest.FTypeUnit:= self.FTypeUnit;
 if aDest.IsExist(SpNb_pkNew) then
  aDest.GetPoint(SpNb_pkNew).Assign(self.GetPoint(SpNb_pkNew));
 if aDest.IsExist(SpNb_pkOld) then
  aDest.GetPoint(SpNb_pkOld).Assign(self.GetPoint(SpNb_pkOld));
end;

function TSpNodeBorder.DoCreateCopy: TSpNodeBorder;
begin
 Result:= TSpNodeBorder(self.ClassType.Create);
 self.DoAssingnTo(Result);
end;

function TSpNodeBorder.getCopy: TSpNodeBorder;
begin
 Result:= self.doCreateCopy;
end;

function TSpNodeBorder.GetKind: TSpNb_Kind;
begin
 Result:= SpNb_kUndefined;
 if self.IsExist(SpNb_pkOld) then
   Result:= SpNb_kRemoved;
 if self.IsExist(SpNb_pkNew) then
  if Result=SpNb_kUndefined then
   Result:= SpNb_kCreated
  else
   if self.FOldPoint.IsEqualPos(self.FNewPoint) then
    Result:= SpNb_kExist
   else
    Result:= SpNb_kModified;
end;
{$endregion}

{$region 'TSpBorderLandplot'}
{ TSpBorderLandplot }

function TSpBorderLandplot.GetArea: double;
begin
 Result:= 0;
 case self.getKind of
   SpBl_kUndefined: EXIT;
   SpBl_kCreated,SpBl_kModified: Result:= self.GetAreaEx(SpNb_pkNew);
   SpBl_kRemoved: Result:= self.GetAreaEx(SpNb_pkOld);
 end;
end;

function TSpBorderLandplot.GetAreaEx(aPointsKind: TSpNodeBorder.TSpNb_PointKind): double;
var
 _points: TMSMPoints;
begin
 _points:= TMSMPoints.Create(FALSE);
 self.GetPoints(_points,aPointsKind);
 Result:= msmGetAreaOf(_points);
 _points.Free;
end;

function TSpBorderLandplot.getBorderKind: TSpBl_BorderKind;
begin
 Result:= TSpBl_BorderKind.SpBl_bkUndefined;
 case self.getKind of
  SpBl_kUndefined: ;
  SpBl_kCreated,SpBl_kModified: Result:= self.getBorderKindByOrdinateKind(TSpNodeBorder.TSpNb_PointKind.SpNb_pkNew);
  SpBl_kRemoved: Result:= self.getBorderKindByOrdinateKind(TSpNodeBorder.TSpNb_PointKind.SpNb_pkOld);
 end;
end;

function TSpBorderLandplot.getBorderKindByOrdinateKind(aKind: TSpNodeBorder.TSpNb_PointKind): TSpBl_BorderKind;
var
 _points: TMSMPoints;
begin
_points:= TMSMPoints.Create(FALSE);
 self.GetPoints(_points,aKind);
 result:= self.getBorderKindOf(_points);
_points.Free;
end;

function TSpBorderLandplot.getBorderKindOf(
  const aPoints: TMSMPoints): TSpBl_BorderKind;
begin
 Result:= SpBl_bkUndefined;
 if aPoints.Count>2 then
   case msmGetDirectionOf(apoints) of
     dcWithHourArrow: Result:= SpBl_bkInner;
    dcWithOutHourArrow: Result:= SpBl_bkOuter;
   end;
end;

function TSpBorderLandplot.getInvalid: TSpBl_ValidKinds;
begin
 Result:= TSpBl_ValidKinds.all - self.isValidEx(TSpBl_ValidKinds.all);
end;

function TSpBorderLandplot.getKind: TSpBl_Kind;
var
 _SpUnit: TSpNodeBorder;
 _Kinds: TSpNodeBorder.TKinds;
begin
 Result:=  SpBl_kUndefined;
 for _SpUnit in self do begin
  Include(_Kinds,_SpUnit.Kind);
  //��� ����������� ���� ������� ��������� ���� ������ ������� (�����)
  if ((SpNb_kCreated in _Kinds) and (SpNb_kRemoved in _Kinds))
  or (SpNb_kExist in _Kinds) or (SpNb_kModified in _Kinds)
  then EXIT(SpBl_kModified);
 end;
 if SpNb_kCreated in _Kinds then EXIT(SpBl_kCreated);
 if SpNb_kRemoved in _Kinds then EXIT(SpBl_kRemoved);
end;

function TSpBorderLandplot.GetPoints(const aDest: TMSMPoints;
                                     aKind: TSpNodeBorder.TSpNb_PointKind): integer;
var
 _SpelementUnit: TSpNodeBorder;
begin
 Result:= aDest.Count;
 for _SpelementUnit in self do begin
  if _SpelementUnit.IsExist(aKind) then
   aDest.Add(_SpelementUnit[aKind]);
 end;
 Result:= aDest.Count-Result;
end;

function TSpBorderLandplot.IsValidByClosed: boolean;
begin
 Result:= false;
 case self.getKind of
  SpBl_kUndefined: ;
  //��� ����� �����
  SpBl_kCreated: Result:= self.First.NewPoint.IsEqualPos(self.Last.NewPoint);
  //���� ����������� ����� �������. �.�. �������� �������� ��� ������ � (���)
  //-��������� ����� �������� ����������, � ��������� ����� ��������� �� �������:
  SpBl_kModified:
    if self.First.IsExistPoint[SpNb_pkNew]
    and self.First.IsExistPoint[SpNb_pkNew] then
      self.First.NewPoint.IsEqualOrdinate(self.Last.NewPoint)
    else
      Result:= FALSE;
   //��� ����� ���������
   SpBl_kRemoved: result:= self.First.NewPoint.IsEqualOrdinate(self.Last.NewPoint);
 end;
end;

function TSpBorderLandplot.IsValidByCountItem: boolean;
begin
 Result:= self.Count>=3;
end;

function TSpBorderLandplot.isValid: boolean;
begin
 Result:= self.getInvalid=[];
end;

function TSpBorderLandplot.isValidEx(aKinds: TSpBl_ValidKinds): TSpBl_ValidKinds;
var
 _vkind: TSpBl_ValidKind;
begin
 Result:= [];
 for _vkind in aKinds do
  case _vkind of
   SpBl_vkCount: if self.IsValidByCountItem then include(Result,SpBl_vkCount);
   SpBl_vkClosed: if self.IsValidByClosed then include(Result,SpBl_vkClosed);
  end;
end;

procedure TSpBorderLandplot.setBorderKind(aValue: TSpBl_BorderKind);
begin
 case self.getKind of
  SpBl_kUndefined:;
  SpBl_kCreated,SpBl_kModified:
    self.setBorderKindByOrdinateKind(TSpNodeBorder.TSpNb_PointKind.SpNb_pkNew,aValue);
  SpBl_kRemoved: self.setBorderKindByOrdinateKind(TSpNodeBorder.TSpNb_PointKind.SpNb_pkOld,aValue);
 end;
end;

procedure TSpBorderLandplot.setBorderKindByOrdinateKind(aKind: TSpNodeBorder.TSpNb_PointKind; aValue: TSpBl_BorderKind);
var
 _points: TMSMPoints;
begin
_points:= TMSMPoints.Create(FALSE);
TRY
 self.GetPoints(_points,aKind);
 if _points.Count<3 then EXIT;
 if self.getBorderKindOf(_points) <> aValue then
  self.Reverse;
FINALLY
 _points.Free;
END;
end;

procedure TSpBorderLandplot.setKind(const value: TSpBl_Kind);
var
 _kind: TSpNodeBorder.TSpNb_Kind;
 _Spun: TSpNodeBorder;
begin
 _kind:= TSpNodeBorder.TSpNb_Kind.SpNb_kUndefined;
 case value of
   SpBl_kUndefined:;
   SpBl_kCreated: _kind:= TSpNodeBorder.TSpNb_Kind.SpNb_kCreated;
   SpBl_kRemoved: _kind:= TSpNodeBorder.TSpNb_Kind.SpNb_kRemoved;
   SpBl_kModified: _kind:= TSpNodeBorder.TSpNb_Kind.SpNb_kModified;
 end;
 for _Spun in self do
  _Spun.Kind:= _kind;
end;
{$endregion}

{ TSpNodeBorder.TSpNewPoint }

function TSpNodeBorder.TSpNewPoint.GetCopy: TSpNewPoint;
begin
 Result:= TSpNewPoint(self.DoCreateCopy);
end;


{ TSpNodeBorder.TSpOldPoint }

function TSpNodeBorder.TSpOldPoint.GetCopy: TSpOldPoint;
begin
 Result:= TSpOldPoint(self.DoCreateCopy);
end;

{$region 'TEsSpatialElement.TSpBl_ValidKindsHelper'}
{ TEsSpatialElement.TSpBl_ValidKindsHelper }

class function TSpBorderLandplot.TSpBl_ValidKindsHelper.getAll: TSpBl_ValidKinds;
begin
 if FALL=[] then FALL:= initALL;
 Result:= FALL;
end;

class function TSpBorderLandplot.TSpBl_ValidKindsHelper.initAll: TSpBl_ValidKinds;
var
 _ctx: TRttiContext;
 _type: TRttiType;
 _i: integer;
begin
 _ctx:= TRttiContext.Create;
 _type:= _ctx.GetType(typeinfo(TSpBl_ValidKind));
 for _i:= _type.AsOrdinal.MinValue to _type.AsOrdinal.MaxValue do
  include(result,TSpBl_ValidKind(_i));
 //_type.Free; ������ ������������� ������ � ����������
 _ctx.Free;
end;

function TSpBorderLandplot.TSpBl_ValidKindsHelper.ToText: string;
var
 _value:TSpBorderLandplot.TSpBl_ValidKind;
 _stb: TStringBuilder;
begin
 _stb:= TStringBuilder.Create;
 for _value in self do
   _stb.AppendLine(_value.ToText);
 Result:= _stb.ToString;
 _stb.Free;
end;
{$endregion}

{ TSpBorderLandplot.TSpBl_ValidKindHelper }

function TSpBorderLandplot.TSpBl_ValidKindHelper.ToText: string;
begin
 case self of
   SpBl_vkCount: Result:= rsSpBl_vkCount;
   SpBl_vkClosed: Result:= rsSpBl_vkClosed;
 end;
end;

{ TEsEntitySpatial.TSpMBl_ValidKindHelper }

function TSpMBordersLandplot.TSpMBl_ValidKindHelper.ToText: string;
begin
 case self of
  SpMBl_vkCount: Result:= rsSpMBl_vkCount;
  SpMBl_vkItems: Result:= rsSpMBl_vkItems;
  SpMBl_vkBorderKind: Result:= rsSpMBl_vkBorderKind;
 end;
end;

{$region 'TSpMBordersLandplot.TSpMBl_ValidKindsHelper'}
{ TSpMBordersLandplot.TSpMBl_ValidKindsHelper }

class function TSpMBordersLandplot.TSpMBl_ValidKindsHelper.getAll: TSpMBl_ValidKinds;
begin
 if FALL=[] then FALL:= initALL;
 Result:= FALL;
end;

class function TSpMBordersLandplot.TSpMBl_ValidKindsHelper.initAll: TSpMBl_ValidKinds;
var
 _ctx: TRttiContext;
 _type: TRttiType;
 _i: integer;
begin
 _ctx:= TRttiContext.Create;
 _type:= _ctx.GetType(typeinfo(TSpMBl_ValidKind));
 for _i:= _type.AsOrdinal.MinValue to _type.AsOrdinal.MaxValue do
  include(result,TSpMBl_ValidKind(_i));
 //_type.Free; ������ ������������� ������ � ����������
 _ctx.Free;
end;

function TSpMBordersLandplot.TSpMBl_ValidKindsHelper.ToText: string;
var
 _value:TSpMBordersLandplot.TSpMBl_ValidKind;
 _stb: TStringBuilder;
begin
 _stb:= TStringBuilder.Create;
 for _value in self do
   _stb.AppendLine(_value.ToText);
 Result:= _stb.ToString;
 _stb.Free;
end;
{$endregion}

{$region 'TSpMBordersLandplot'}
{ TSpMBordersLandplot }

function TSpMBordersLandplot.GetArea: double;
begin
 Result:= 0;
 case self.getKind of
   SpBl_kUndefined: EXIT;
   SpBl_kCreated,SpBl_kModified: Result:= self.GetAreaEx(SpNb_pkNew);
   SpBl_kRemoved: Result:= self.GetAreaEx(SpNb_pkOld);
 end;
end;

function TSpMBordersLandplot.GetAreaEx(aPointsKind: TSpNodeBorder.TSpNb_PointKind): double;
var 
 _spbl: TSpBorderLandplot;
begin
 if self.Count=0 then EXIT(0);
 Result:= 0;
 for _spbl in self do 
  Result:= Result+_spbl.GetAreaEx(aPointsKind);
end;


function TSpMBordersLandplot.getInvalid: TSpMBl_ValidKinds;
begin
 Result:= TSpMBl_ValidKinds.All-self.isValidEx(TSpMBl_ValidKinds.All);
end;

function TSpMBordersLandplot.getKind: TSpBorderLandplot.TSpBl_Kind;
var
 _SpBl: TSpBorderLandplot;
 _Kinds: TSpBorderLandplot.TSpBl_Kinds;
begin
 Result:=  SpBl_kUndefined;
 for _SpBl in self do begin
  Include(_Kinds,_SpBl.Kind);
  //��� ����������� ���� ������� ��������� ���� ������ ������� (�����)
  if ((SpBl_kCreated in _Kinds) and (SpBl_kRemoved in _Kinds)) 
  or (SpBl_kModified in _Kinds)
  then EXIT(SpBl_kModified);
 end;
 if SpBl_kCreated in _Kinds then EXIT(SpBl_kCreated);
 if SpBl_kRemoved in _Kinds then EXIT(SpBl_kRemoved);
end;

function TSpMBordersLandplot.isValid: boolean;
begin
 result:= self.getInvalid=[];
end;

function TSpMBordersLandplot.isValidByBorderKind: boolean;
var
 _i: integer;
begin
 Result:= true;
 if self.Count=0 then EXIT; //��������� ������, ������� ��� ��������� ���������
 if self.First.BorderKind<>SpBl_bkOuter then EXIT(FALSE);
 for _i := 1 to self.Count-1 do
  if self.First.BorderKind<>SpBl_bkInner then EXIT(FALSE);
 Result:= true;
end;

function TSpMBordersLandplot.isValidByCount: boolean;
begin
 Result:= self.Count<>0;
end;

function TSpMBordersLandplot.isValidByItems: boolean;
var
 _SpEl: TSpBorderLandplot;
begin
 for _SpEl in self do
  if not _SpEl.IsValid then EXIT(FALSE);
 Result:= true;
end;

function TSpMBordersLandplot.isValidEx(aKinds: TSpMBl_ValidKinds): TSpMBl_ValidKinds;
var
 _i: TSpMBl_ValidKind;
begin
 Result:= [];
 for _i in aKinds do 
  case  _i of
    SpMBl_vkCount: if self.isValidByCount then Include(Result,SpMBl_vkCount);
    SpMBl_vkItems: if self.isValidByItems then Include(Result,SpMBl_vkItems);
    SpMBl_vkBorderKind: if self.isValidByBorderKind then Include(Result,SpMBl_vkBorderKind);
  end;
end;
{
procedure TSpMBordersLandplot.setKind(
  const value: TSpBorderLandplot.TSpBl_Kind);
begin

end;
}
{$endregion}
end.
