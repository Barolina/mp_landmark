object FrameGeodesisBases: TFrameGeodesisBases
  Left = 0
  Top = 0
  Width = 746
  Height = 453
  TabOrder = 0
  object Panel: TPanel
    Left = 0
    Top = 33
    Width = 746
    Height = 420
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object stringGridDate: TStringGrid
      AlignWithMargins = True
      Left = 3
      Top = 3
      Width = 740
      Height = 414
      HelpType = htKeyword
      Align = alClient
      BevelInner = bvNone
      BevelKind = bkSoft
      BevelOuter = bvNone
      BorderStyle = bsNone
      Color = 13750737
      ColCount = 3
      DefaultColWidth = 217
      DrawingStyle = gdsGradient
      FixedColor = clGradientActiveCaption
      FixedCols = 0
      RowCount = 1
      FixedRows = 0
      GradientEndColor = clBlack
      GradientStartColor = clSkyBlue
      Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goDrawFocusSelected, goRowSizing, goColSizing, goEditing, goTabs]
      PopupMenu = poMenuTable
      TabOrder = 0
      OnKeyPress = stringGridDateKeyPress
      OnKeyUp = stringGridDateKeyUp
      OnSetEditText = stringGridDateSetEditText
    end
  end
  object pnlLabelTabl: TPanel
    Left = 0
    Top = 0
    Width = 746
    Height = 33
    Align = alTop
    BevelEdges = [beTop]
    BevelKind = bkFlat
    BevelOuter = bvNone
    Color = 15790320
    ParentBackground = False
    TabOrder = 1
    object lblPName: TLabel
      Left = 36
      Top = 0
      Width = 125
      Height = 31
      Align = alLeft
      Alignment = taCenter
      AutoSize = False
      Caption = #1053#1072#1079#1074#1072#1085#1080#1077' '#1087#1091#1085#1082#1090#1072' '#1075#1077#1086#1076#1077#1079#1080#1095#1077#1089#1082#1086#1081' '#1089#1077#1090#1080
      Color = 15790320
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Layout = tlCenter
      WordWrap = True
      ExplicitHeight = 49
    end
    object lblPY: TLabel
      Left = 634
      Top = 0
      Width = 115
      Height = 31
      Align = alLeft
      Alignment = taCenter
      AutoSize = False
      Caption = #1050#1086#1086#1088#1076#1080#1085#1072#1090#1072' Y'
      Color = 15790320
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Layout = tlCenter
      WordWrap = True
      ExplicitLeft = 639
      ExplicitTop = -3
      ExplicitHeight = 33
    end
    object lblPKind: TLabel
      Left = 162
      Top = 0
      Width = 191
      Height = 31
      Align = alLeft
      Alignment = taCenter
      AutoSize = False
      Caption = #1058#1080#1087' '#1087#1091#1085#1082#1090#1072' '#1075#1077#1086#1076#1077#1079#1080#1095#1077#1089#1082#1086#1081' '#1089#1077#1090#1080
      Color = 15790320
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Layout = tlCenter
      WordWrap = True
      ExplicitLeft = 202
      ExplicitHeight = 49
    end
    object spl1: TSplitter
      Left = 161
      Top = 0
      Width = 1
      Height = 31
      Color = clBtnText
      ParentColor = False
      ExplicitLeft = 200
      ExplicitHeight = 49
    end
    object spl2: TSplitter
      Left = 353
      Top = 0
      Width = 1
      Height = 31
      Color = clDefault
      ParentColor = False
      ExplicitLeft = 670
      ExplicitHeight = 49
    end
    object spl3: TSplitter
      Left = 633
      Top = 0
      Width = 1
      Height = 31
      Color = clBtnText
      ParentColor = False
      ExplicitLeft = 721
      ExplicitHeight = 49
    end
    object lblN: TLabel
      Left = 0
      Top = 0
      Width = 35
      Height = 31
      Align = alLeft
      Alignment = taCenter
      AutoSize = False
      Caption = #8470
      Color = 15790320
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Layout = tlCenter
      WordWrap = True
      ExplicitHeight = 49
    end
    object spl4: TSplitter
      Left = 35
      Top = 0
      Width = 1
      Height = 31
      Color = clBtnText
      ParentColor = False
      ExplicitHeight = 49
    end
    object lblPKlass: TLabel
      Left = 354
      Top = 0
      Width = 163
      Height = 31
      Align = alLeft
      Alignment = taCenter
      AutoSize = False
      Caption = #1050#1083#1072#1089#1089' '#1075#1077#1086#1076#1077#1079#1080#1095#1077#1089#1082#1086#1081' '#1089#1077#1090#1080
      Color = 15790320
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Layout = tlCenter
      WordWrap = True
      ExplicitLeft = 637
      ExplicitHeight = 49
    end
    object lblPX: TLabel
      Left = 518
      Top = 0
      Width = 115
      Height = 31
      Align = alLeft
      Alignment = taCenter
      AutoSize = False
      Caption = #1050#1086#1086#1088#1076#1080#1085#1072#1090#1072' '#1061
      Color = 15790320
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Layout = tlCenter
      WordWrap = True
      ExplicitHeight = 49
    end
    object spl5: TSplitter
      Left = 517
      Top = 0
      Width = 1
      Height = 31
      Color = clBtnText
      ParentColor = False
      ExplicitLeft = 557
      ExplicitHeight = 49
    end
  end
  object poMenuTable: TPopupMenu
    Left = 224
    Top = 120
    object N1: TMenuItem
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1089#1090#1088#1086#1082#1091
      ShortCut = 16429
      OnClick = SubParcels_Actions_ButtonGroupItems0Click
    end
    object N2: TMenuItem
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1089#1090#1088#1086#1082#1091' '
      ShortCut = 16430
      OnClick = SubParcels_Actions_ButtonGroupItems1Click
    end
  end
end
