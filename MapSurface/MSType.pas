{*******************************************************}
{      |----------------------------------------|       }
{      |-(c) 2009-2010 ������������ ����������.-|       }
{      |-        ���������� ���������.         -|       }
{      |-          kras_ai@mail.ru             -|       }
{      |----------------------------------------|       }
{*******************************************************}
unit MSType;
interface
uses
 MSMType_;
type
 TPointScreen = class;
 TPointsLogic = array of TMSMPoint;
 TRectsLogic = array of TMSMRect;
 TPointsScreen = array of TPointScreen;
 
  TPointScreen = class
   private
    FCaption: string;
    Fx: integer;
    Fy: integer;
   public
    constructor Create(x,y: integer);
    function Compare(x,y: integer): boolean; overload; 
    function Compare(x,y,eps: integer): boolean; overload; 
    procedure Move(Dx,Dy: integer);
    function Distance(x,y: integer): Double; overload; 
    function Distance(aPoint: TPointScreen): Double; overload;
    function GetCopy: TPointScreen;
    procedure Assign(Source: TPointScreen); 
    property x: integer read Fx write Fx;
    property y: integer read Fy write Fy;
    property Caption: string read FCaption write FCaption;
  end;
 
procedure MSPoints2MSMPoints(msPoints: TPointsLogic; msmpoints: TMSMPoints);
implementation
const
 DeltaZero:double = 0.000001;
procedure MSPoints2MSMPoints(msPoints: TPointsLogic; msmpoints: TMSMPoints);
var
 _i: Integer;
begin
 msmpoints.Clear;
 for _I := 0 to high(msPoints)  do  begin
  msmpoints.Add(TMSMPoint.Create(msPoints[_i].x,msPoints[_i].y));
  msmpoints.Last.Caption:= msPoints[_i].Caption;
 end;
end;
function msZero(X : double) : boolean;
begin
 Result := not ((X  > DeltaZero) or (- X > DeltaZero));
end;
function msEqual(X,Y : double) : boolean;
begin
 Result := not ((X - Y > DeltaZero) or (Y - X > DeltaZero));
end;
function msLE(X,Y : double) : boolean;
begin
 Result := msEqual(X,Y) or (X < Y);
end;
function msGE(X,Y : double) : boolean;
begin
 Result := msEqual(X,Y) or (X > Y);
end;
function msCheckRectLog(var x1,y1,x2,y2: Double) : boolean;
var
 _buffer: Double;
begin
 Result:= False;
 if x2<x1 then begin
  _buffer:= x1;
  x1:= x2;
  x2:= _buffer;
  Result:= true;
 end;
 if y2<y1 then begin
  _buffer:=y1;
  y1:= y2;
  y2:= _buffer;
  Result:= true;
 end;
end;
function msCheckRectScr(var x1,y1,x2,y2: Integer) : boolean;
var
 _buffer: integer;
begin
 Result:= False;
 if x2<x1 then begin
  _buffer:= x1;
  x1:= x2;
  x2:= _buffer;
  Result:= true;
 end;
 if y2<y1 then begin
  _buffer:=y1;
  y1:= y2;
  y2:= _buffer;
  Result:= true;
 end;
end;
{ TPointScreen }
procedure TPointScreen.assign(Source: TPointScreen);
begin
 self.Fx:= Source.x;
 self.Fy:= Source.y;
 self.FCaption:= Source.Caption;
end;
function TPointScreen.compare(x, y: integer): boolean;
begin
 result:= (self.Fx = x) and  (self.Fy = y);
end;
function TPointScreen.compare(x, y, eps: integer): boolean;
begin
 result:= (abs(self.Fx-x)<=eps) and (abs(self.Fy-y)<=eps);
end;
constructor TPointScreen.Create(x, y: integer);
begin
 self.Fx:= x;
 self.Fy:= y;
end;
function TPointScreen.Distance(aPoint: TPointScreen): Double;
begin
 Result:= self.Distance(aPoint.x,aPoint.y);
end;
function TPointScreen.Distance(x, y: integer): Double;
begin
 TRY
  Result := sqrt(sqr(self.Fx - x) + sqr(self.Fy - y));
 EXCEPT
  Result:= 0;
 END;
end;
function TPointScreen.getCopy: TpointScreen;
begin
 result:= TpointScreen.create(self.Fx,self.Fy);
end;
procedure TPointScreen.Move(Dx, Dy: Integer);
begin
 self.Fx:= self.Fx+Dx;
 self.Fy:= self.Fy+Dy;
end;
end.
