object FrameSubParContours: TFrameSubParContours
  Left = 0
  Top = 0
  Width = 660
  Height = 276
  VertScrollBar.ButtonSize = 10
  VertScrollBar.Size = 10
  VertScrollBar.Style = ssFlat
  VertScrollBar.Tracking = True
  ParentBackground = False
  TabOrder = 0
  object pnl2: TPanel
    Left = 0
    Top = 0
    Width = 169
    Height = 276
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 0
    object pnl1: TPanel
      Left = 0
      Top = 0
      Width = 169
      Height = 25
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object jvtlbr2: TJvToolBar
        Left = 0
        Top = 0
        Width = 658
        Height = 25
        ParentCustomHint = False
        Align = alLeft
        ButtonHeight = 36
        Caption = 'jvtlbr1'
        Color = 15711412
        DoubleBuffered = False
        DrawingStyle = dsGradient
        EdgeInner = esLowered
        EdgeOuter = esRaised
        Flat = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        GradientEndColor = 15197907
        GradientStartColor = clWhite
        Images = formContainer.ilCommands
        List = True
        GradientDirection = gdHorizontal
        ParentColor = False
        ParentDoubleBuffered = False
        ParentFont = False
        ParentShowHint = False
        AllowTextButtons = True
        ShowHint = False
        TabOrder = 0
        HintColor = 13551291
        object btnContour_prev1: TToolButton
          Left = 0
          Top = 0
          Hint = #13#10#1055#1088#1077#1076'. '#1082#1086#1085#1090#1091#1088' '#1095#1072#1089#1090#1080#13#10
          Action = actContour_prev
          ParentShowHint = False
          ShowHint = True
        end
        object btnContour_next1: TToolButton
          Left = 24
          Top = 0
          Hint = #13#10#1057#1083#1077#1076'. '#1082#1086#1085#1090#1091#1088' '#1095#1072#1089#1090#1080#13#10
          Action = actContour_next
          ParentShowHint = False
          ShowHint = True
        end
        object btnContrours_AddNew1: TToolButton
          Left = 48
          Top = 0
          Hint = #13#10#1044#1086#1073#1072#1074#1080#1090#1100' '#1082#1086#1085#1090#1091#1088' '#1095#1072#1089#1090#1080#13#10#13#10
          Action = actContrours_AddNew
          ParentShowHint = False
          ShowHint = True
        end
        object btnContours_DeleteCurrent1: TToolButton
          Left = 72
          Top = 0
          Hint = #13#10#1059#1076#1072#1083#1080#1090#1100' '#1074#1099#1073#1088#1072#1085#1085#1099#1081' '#1082#1086#1085#1090#1091#1088' '#1095#1072#1089#1090#1080#13#10
          Action = actContours_DeleteCurrent
          ParentShowHint = False
          ShowHint = True
        end
        object btnClipBrd_Copy1: TToolButton
          Left = 96
          Top = 0
          Action = actClipBrd_Copy
        end
        object btnClipBrd_Paste1: TToolButton
          Left = 120
          Top = 0
          Hint = #13#10#1048#1084#1087#1086#1088#1090' '#1095#1072#1089#1090#1077#1081' '#1082#1086#1085#1090#1091#1088#1072#13#10
          Action = actClipBrd_Paste
          ParentShowHint = False
          ShowHint = True
        end
        object btnValid1: TToolButton
          Left = 144
          Top = 0
          Hint = #13#10#1055#1088#1086#1074#1077#1088#1082#1072' '#1082#1086#1088#1088#1088#1077#1082#1090#1085#1086#1089#1090#1080#13#10
          Action = actValid
          ParentShowHint = False
          ShowHint = True
        end
      end
    end
    object pnlAttribute_Panel: TPanel
      Left = 0
      Top = 25
      Width = 169
      Height = 27
      Align = alTop
      Alignment = taLeftJustify
      BevelOuter = bvNone
      Color = clWhite
      ParentBackground = False
      TabOrder = 1
      object pnlAction: TPanel
        Left = 0
        Top = 0
        Width = 169
        Height = 27
        Align = alClient
        BevelEdges = []
        BevelKind = bkFlat
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object btnEdCurContour: TButtonedEdit
          Left = 0
          Top = 0
          Width = 169
          Height = 27
          Hint = #1044#1074#1086#1081#1085#1086#1081' '#1082#1083#1080#1082' '#1084#1099#1096#1080' '#1076#1083#1103' '#1080#1079#1084#1077#1085#1077#1085#1080#1103' '#1079#1085#1072#1095#1077#1085#1080#1103
          Align = alLeft
          Alignment = taCenter
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          BevelWidth = 2
          BorderStyle = bsNone
          Color = 15921123
          Images = formContainer.ilCommands
          ParentShowHint = False
          RightButton.ImageIndex = 6
          RightButton.Visible = True
          ShowHint = True
          TabOrder = 0
          TextHint = #1044#1074#1086#1081#1085#1086#1081' '#1082#1083#1080#1082' '#1084#1099#1096#1080' '#1076#1083#1103' '#1080#1079#1084#1077#1085#1080#1103' '#1080#1079#1085#1072#1095#1077#1085#1080#1103
          OnDblClick = btnEdCurContourDblClick
          OnExit = btnEdCurContourExit
          OnKeyDown = btnEdCurContourKeyDown
          OnRightButtonClick = btnEdCurContourRightButtonClick
        end
        object cbbContoursType: TComboBox
          Left = 403
          Top = -1
          Width = 97
          Height = 21
          Hint = #1040#1082#1090#1080#1074#1085#1099#1081' '#1090#1080#1087' '#1082#1086#1085#1090#1091#1088#1072
          Style = csDropDownList
          Color = 15921123
          DoubleBuffered = False
          ParentDoubleBuffered = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          TextHint = #1040#1082#1090#1080#1074#1085#1099#1081' '#1090#1080#1087' '#1082#1086#1085#1090#1091#1088#1072
          Visible = False
          StyleElements = []
          OnChange = cbbContoursTypeChange
          Items.Strings = (
            #1054#1073#1088#1072#1079#1091#1077#1084#1099#1077' '#1082#1086#1085#1090#1091#1088#1072' '#1084#1085#1086#1075#1086#1082#1086#1085#1090#1091#1088#1085#1086#1075#1086' '#1091#1095#1072#1089#1090#1082#1072
            #1059#1090#1086#1095#1085#1103#1077#1084#1099#1077' '#1089#1091#1097#1077#1089#1090#1074#1091#1102#1097#1080#1077' '#1082#1086#1085#1090#1091#1088#1072' '#1084#1085#1086#1075#1086#1082#1086#1085#1090#1091#1088#1085#1086#1075#1086' '#1091#1095#1072#1089#1090#1082#1072
            
              #1048#1089#1082#1083#1102#1095#1077#1085#1080#1077' '#1075#1088#1072#1085#1080#1094#1099' '#1082#1086#1085#1090#1091#1088#1072' '#1084#1085#1086#1075#1086#1082#1086#1085#1090#1091#1088#1085#1086#1075#1086' '#1091#1095#1072#1089#1090#1082#1072' ('#1080#1089#1082#1083#1102#1095#1077#1085#1080#1077' '#1082 +
              #1086#1085#1090#1091#1088#1072')')
        end
      end
    end
    object Contours_List_ListBox: TListBox
      Left = 0
      Top = 52
      Width = 169
      Height = 224
      Style = lbOwnerDrawVariable
      Align = alClient
      BevelInner = bvNone
      BevelKind = bkFlat
      BevelOuter = bvRaised
      BorderStyle = bsNone
      Color = 15921123
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ItemHeight = 13
      ParentFont = False
      TabOrder = 2
      OnClick = Action_Contours_SetCurrentExecute
      OnKeyDown = Contours_List_ListBoxKeyDown
      OnMouseLeave = Contours_List_ListBoxMouseLeave
    end
  end
  object pnl3: TPanel
    Left = 169
    Top = 0
    Width = 491
    Height = 276
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
  end
  object actlst: TActionList
    Images = formContainer.ilCommands
    Left = 120
    Top = 182
    object actContrours_AddNew: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1082#1086#1085#1090#1091#1088
      Hint = #1044#1086#1073#1072#1074#1080#1090#1100' '#1082#1086#1085#1090#1091#1088
      ImageIndex = 0
      OnExecute = actContrours_AddNewExecute
    end
    object actContours_DeleteCurrent: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1074#1099#1073#1088#1072#1085#1085#1099#1081' '#1082#1086#1085#1090#1091#1088
      Hint = #1059#1076#1072#1083#1080#1090#1100' '#1074#1099#1073#1088#1072#1085#1085#1099#1081' '#1082#1086#1085#1090#1091#1088
      ImageIndex = 1
      OnExecute = actContours_DeleteCurrentExecute
    end
    object actClipBrd_Paste: TAction
      Caption = 'actClipBrd_Paste'
      ImageIndex = 2
      OnExecute = actClipBrd_PasteExecute
    end
    object actClipBrd_Copy: TAction
      Caption = 'actClipBrd_Copy'
      ImageIndex = 3
      OnExecute = actClipBrd_CopyExecute
    end
    object actContour_next: TAction
      Caption = 'actContour_next'
      ImageIndex = 8
      OnExecute = actContour_nextExecute
    end
    object actContour_prev: TAction
      Caption = 'actContour_prev'
      ImageIndex = 7
      OnExecute = actContour_prevExecute
    end
    object actValid: TAction
      Caption = 'actValid'
      ImageIndex = 4
      OnExecute = actValidExecute
    end
  end
end
