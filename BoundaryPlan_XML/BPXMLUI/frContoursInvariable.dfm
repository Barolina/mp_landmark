object FrameContoursInvariable: TFrameContoursInvariable
  Left = 0
  Top = 0
  Width = 642
  Height = 287
  Color = clBtnFace
  ParentBackground = False
  ParentColor = False
  TabOrder = 0
  object PanelContours_List: TPanel
    Left = 0
    Top = 0
    Width = 81
    Height = 287
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 0
    object Contours_List_ButtonGroup: TButtonGroup
      Left = 0
      Top = 0
      Width = 81
      Height = 25
      Align = alTop
      BevelOuter = bvNone
      BorderStyle = bsNone
      ButtonHeight = 25
      ButtonWidth = 25
      Images = formContainer.ilCommands
      Items = <
        item
          Action = Action_Contrours_AddNew
        end
        item
          Action = Action_Contours_DeleteCurrent
        end
        item
          Action = actImportContour
          Hint = #1048#1084#1087#1086#1088#1090' '#1082#1086#1085#1090#1091#1088#1086#1074
          ImageIndex = 2
        end>
      ShowHint = True
      TabOrder = 0
    end
    object Contours_List_ListBox: TListBox
      Left = 0
      Top = 25
      Width = 81
      Height = 262
      Style = lbOwnerDrawVariable
      Align = alClient
      BevelInner = bvNone
      BevelKind = bkFlat
      BorderStyle = bsNone
      Color = clWhite
      ItemHeight = 13
      TabOrder = 1
      OnClick = Action_Contours_SetCurrentExecute
    end
  end
  object ActionList: TActionList
    Left = 600
    Top = 232
    object Action_Contrours_AddNew: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1082#1086#1085#1090#1091#1088
      Hint = #1044#1086#1073#1072#1074#1080#1090#1100' '#1082#1086#1085#1090#1091#1088
      ImageIndex = 0
      OnExecute = Action_Contrours_AddNewExecute
    end
    object Action_Contours_DeleteCurrent: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1074#1099#1073#1088#1072#1085#1085#1099#1081' '#1082#1086#1085#1090#1091#1088
      Hint = #1059#1076#1072#1083#1080#1090#1100' '#1074#1099#1073#1088#1072#1085#1085#1099#1081' '#1082#1086#1085#1090#1091#1088
      ImageIndex = 1
      OnExecute = Action_Contours_DeleteCurrentExecute
    end
    object Action_Contours_SetCurrent: TAction
      Caption = #1057#1076#1077#1083#1072#1090#1100' '#1090#1077#1082#1091#1097#1080#1084
      OnExecute = Action_Contours_SetCurrentExecute
    end
    object actImportContour: TAction
      Caption = 'actImportContour'
      OnExecute = actImportContourExecute
    end
  end
end
