unit SurveyingPlan.Xml.Types;
{$define TSpPointFormatInputData}
interface
 uses
 {$region 'System'}
  System.Rtti,
  System.Math,
  System.SysUtils,
  system.Classes,
  System.Generics.Collections,
  System.Generics.Defaults,
 {$endregion} 
 MSMType_, MSMath_,
 System.RegularExpressions,
 System.RegularExpressionsCore,
 SurveyingPlan.Common;

 type
  SpConsts = record
   const EmptyVal: string = '-';
   const ErrorVal: string = '-������-';
   const NewPointPrefix:char = '�';
  end;

  {$REGION 'TSpPoint - �����'}
  ///	<summary>
  ///	  ����� ������������ ���������� ���������tOrdinate �STD_MPV04
  ///	</summary>
  TSpPoint = class (TMSMPoint)
  public type
  {$region '��� - ������� �����'}
   TPrefix = record
    strict private
     FValue: boolean;
     procedure setValue(aValue: boolean);
     function getValueAsStr: string;
     procedure setValueAsStr(Value: string);
    public
     class operator Implicit(const Value: string): TPrefix;
     class operator Implicit(const Value: TPrefix): string;
     class operator Equal(const a: string; b:TPrefix): boolean;
     class operator Equal(const a: TPrefix; b:TPrefix): boolean;

     class function isAcceptable(const Value: string): boolean; static;

     property IsNew: boolean read FValue write setValue;
     property Value: string read getValueAsStr write setValueAsStr;
   end;
  {$endregion}
  {$region '��� - ����� ������� �����'}
   TNumber = record
    strict private
     FValue: Cardinal;
     FEmpty: boolean;
     procedure setValue(aValue: Cardinal);
     procedure setEmptyState(aValue: boolean);
    public
     class operator Implicit(const Value: string): TNumber;
     class operator Implicit(const Value: Cardinal): TNumber;
     class operator Implicit(const Value: TNumber): string; //if empty, return empty string
     class operator Implicit(const Value: TNumber): cardinal; //ignory empty state

     class function isAcceptable(const Value: string): boolean;  static;

     class operator Equal(const a: cardinal; b:TNumber): boolean;
     class operator Equal(const a: string; b:TNumber): boolean;
     class operator Equal(const a: TNumber; b:TNumber): boolean;

     property Empty: boolean read FEmpty write setEmptyState;
     property Value: Cardinal read FValue write setValue;
   end;
  {$endregion}
  {$region '��� - ����������� ����������� ��������� �����'}
   TDelta = record
    public type
     ExceptionValue = class(Exception);
    strict private
     FValue: Double;
     FEmpty: boolean;
     procedure setValue(aValue: Double);
     procedure setEmptyState(aValue: boolean);
     const ValueRoundTo = -1;
    public
     class operator Implicit(const Value: string): TDelta;
     class operator Implicit(const Value: Double): TDelta;
     class operator Implicit(const Value: TDelta): string;
     class operator Implicit(const Value: TDelta): Double;

     class function isAcceptable(const Value: string): boolean; overload; static;
     class function isAcceptable(const Value: double): boolean; overload; static;

     class operator Equal(const a: Double; b:TDelta): boolean;
     class operator Equal(const a: string; b:TDelta): boolean;
     class operator Equal(const a: TDelta; b:TDelta): boolean;

     property Empty: boolean read FEmpty write setEmptyState;
     property Value: Double read FValue write setValue;
   end;
  {$endregion}
  strict private
    FPrefix: TPrefix; //�������
    FNumber: TNumber; //�����
    FDelta: TDelta; //����������� ����������� ��������� ��.
    FZacrep: string; //�������� �����������
  protected
   {$ifdef TSpPointFormatInputData}
    procedure setX(Value: Double); override;
    procedure setY(Value: Double); override;
    procedure setCaption(Value: string); override;
   {$endif}
    procedure assignTo(aDest: TSpPoint);

    procedure setPrefix(const Value: TPrefix);
    procedure setNumber(const Value: TNumber);
    procedure setDelta(const value:TDelta);
    procedure setZacrep(const value: string);

    //���������� ���������� ����� PointPref �NumGeopoint
    //��� ������ ������, ���������������� ������������ �����
    //��������������� ������� ����������������� ������ �����������
    //����������� �������� � � ���� PointPref � NumGeopoint.�
    property Caption;
    //������� ������� �����. ������������ �������� ����� ����� ��� ������
    //�� ������ �������� �������"�" ���� ������������ ������.
    //��� ������ ������ ����� �������� ��������� � ���� caption ������������� ������
    //�������� ��������������� ������� �� ���� ����� (���� kind)
    property Prefix:TPrefix read FPrefix write setPrefix;
    //����� ������� �����. ����������� ������ ������������� ��������
    //��� ������ ������ ����� �������� ��������� � ���� caption ������������� ������
    property Number: TNumber read FNumber write setNumber;
    //����������� ����������� ��������� �����. ��������� ��������
    //������������� ����� (������������� ����������� �� ������ ��������� �������)
    property Delta: TDelta  read FDelta write setDelta;
    property Zacrep: string read FZacrep write setZacrep;
   public
    constructor Create;
    function isEqualPoint(aValue: TSpPoint): boolean; virtual;

    { TODO : ��������� � ������������ ����� }
    procedure assign(Source: TMSMPoint); virtual;
    function getClone: TMSMPoint; virtual;

    function getCopy: TSpPoint;
  end;
  {$ENDREGION}

  TSpPointsList<T: TSpPoint> = class(TMSMPointsCustom<T>)
  protected
   function doisClosed: boolean;
   function doisClockwise: boolean;
   function dogetArea: double;
   function dogetLength: double;
   function dogetPerimeter: double;
  public
   constructor Create(AOwnsObjects: Boolean = false);

   class function isClosedOf(aPoints: TMSMPointsCustom<T>): boolean;
   class function isClockwiseOf(aPoints: TMSMPointsCustom<T>): boolean;
   class function AreaOf(aPoints: TMSMPointsCustom<T>): double;
   class function dPOf(aPoints: TMSMPointsCustom<T>; aRMS: Double): integer;
   class function LengthOf(aPoints: TMSMPointsCustom<T>): double;
   class function PerimeterOf(aPoints: TMSMPointsCustom<T>): double;

   function getDP(aRMS: double): integer;

   property IsClosed: boolean read doisClockwise;
   property IsClockwise: boolean read doIsClockwise;
   property Area: double read dogetArea;
   property Length: double read dogetLength;
   property Perimeter: double read dogetPerimeter;
  end;

  TSpBorderPoint = class
   public type
    TKind = (psbExisted,psbCreated,psbRemoved);
    TNewPoint = class sealed (TSpPoint)
    public
     function getClone: TMSMPoint; override;
     function getCopy: TNewPoint;

     property Prefix;
     property Number;
     property X;
     property Y;
     property Delta;
     property Zacrep;
   end;
    TOldPoint = class sealed (TSpPoint)
    public
     function getClone: TMSMPoint; override;
     function getCopy: TOldPoint;

     property Number;
     property X;
     property Y;
   end;
   private
    FNewPoint: TNewPoint;
    FOldPoint: TOldPoint;
    procedure dosetKind(aValue: TKind);
   protected
    function getKind: TKind;
    procedure setKind(aValue: TKind);
   public
    constructor Create; overload;
    constructor Create(aKind: TKind); overload;
    function isEqualPoint(aCompare: TSpBorderPoint): Boolean;
    property NewPoint: TNewPoint read FNewPoint;
    property OldPoint: TOldPoint read FOldPoint;
    property Kind: TKind read getKind write setKind;
  end;

  TSpBorder = class abstract(TObjectList<TSpBorderPoint>)
  protected
   function getNewPointsCount: integer;
   function getOldPointsCount: integer;
   function getMaxRmsOf(aList: TList<TSpBorderPoint.TNewPoint>): double; //������������ �������� ������-������������� ����������� �����
  public
   function NewPointsToList(aDest: TList<TSpBorderPoint.TNewPoint>): integer;
   function OldPointsToList(aDest: TList<TSpBorderPoint.TOldPoint>): integer;
   function getArea: double;
   function getDP: integer;
   property NewPointsCount: integer read getNewPointsCount;
   property OldPointsCount: integer read getOldPointsCount;
  end;

  TSpContour = class(TObjectList<TSpBorder>)
   private
    FCaption: string;
   public
    function getCadNumItem(aItem: TCadNumItemsKind): string;
    function getArea: double;
    function getDP: integer;
    procedure _checkDirection;

    property Caption: string read FCaption write FCaption;
   end;

  TSpMContour = class(TObjectList<TSpContour>)
   public
    function getArea: double;
    function getDP: integer;
    procedure _checkDirection;
  end;

  TSpSubParcel =class(TObjectList<TSpBorder>)
  private
    FCaption : string;
  public 
    function getCadNumItem(aItem : TCadNumItemsKind) : string;
    function getArea : Double;
    function getDP : integer;
    procedure _checkDirection;

    property Caption : string read FCaption write FCaption;
  end;  
  
  TSpMSubParcel = class(TObjectList<TSpSubParcel>)
   public
    function getArea: double;
    function getDP: integer;
    procedure _checkDirection;
  end;

  TSpMCSubParcel = class(TObjectList<TSpMSubParcel>)
    public 
      function getArea: double;
      function getDP : integer;
      procedure _checkDirection;
  end;

  TSpValidator = record
   class function Validate(value: TSpBorder; log: TStringBuilder = nil): boolean; overload; static;
   class function Validate(value: TSpContour; log: TStringBuilder = nil): boolean; overload; static;
   class function Validate(value: TSpMContour; log: TStringBuilder = nil): boolean; overload; static;
   class function Validate(value: TSpSubParcel; log: TStringBuilder = nil): boolean; overload; static;
   class function Validate(value: TSpMSubParcel; log : TStringBuilder = nil): Boolean; overload; static;
   class function Validate(value: TSpMCSubParcel; log : TStringBuilder = nil): Boolean; overload; static;
  end;

implementation


resourcestring
  rs05210B41 = '���������� ����� � ��������� ������� ������ ���� ������ ����.';
  rs22DC5ED7 = '������ � ��������� ����� ������� ������ ���������';
  rsAA65D007 = '������� ������� ������� ������� ������ ���� ��������� ������ ������� �������';
  rs830BBDDE = '��������� ������� ������� (�����) ������ ���� ��������� �� ������� �������';
  rsCAB38452 = '������� ������� �������';
  rs3B7C2C72 = '��������� ������� (�����) �������';

 rsInvalidValue = '������������ ��������';

const
 _AreaRoundTo = -2;

{$region 'TSpPoint '}


procedure TSpPoint.Assign(Source: TMSMPoint);
begin
 if Source is TSpPoint then
 TSpPoint(Source).assignTo(self);
end;

procedure TSpPoint.assignTo(aDest: TSpPoint);
begin
 aDest.X:= self.X;
 aDest.Y:= self.Y;
 aDest.setNumber(self.Number);
 aDest.setZacrep(self.Zacrep);
 aDest.setDelta(self.Delta);
 aDest.setPrefix(self.Prefix);
end;

constructor TSpPoint.Create;
begin
 inherited Create(MaxDouble,MaxDouble);
end;

function TSpPoint.getClone: TMSMPoint;
begin
 Result:= TSpPoint.Create;
 TSpPoint(Result).assign(self);
end;

function TSpPoint.GetCopy: TSpPoint;
begin
 Result:= TSpPoint(self.getClone);
end;

function TSpPoint.isEqualPoint(aValue: TSpPoint): boolean;
begin
 Result:= (self.X = aValue.X) and (self.Y = aValue.Y)
  and (self.Prefix=aValue.Prefix)
  and (self.Number=aValue.Number)
  and (self.Delta=aValue.Delta)
  and (self.Zacrep=aValue.Zacrep);
end;

procedure TSpPoint.setDelta(const value: TDelta);
begin
 self.FDelta:= value;
end;

procedure TSpPoint.setZacrep(const value: string);
begin
 self.FZacrep:= value;
end;

procedure TSpPoint.setNumber(const Value: TNumber);
begin
 if Value=self.FNumber then EXIT;
 self.FNumber:= Value;
 {$ifdef TSpPointFormatInputData}
  self.DoSetCaption(self.FNumber);
 {$endif}
end;

procedure TSpPoint.setPrefix(const Value: TPrefix);
begin
 self.FPrefix:= Value;
end;

{$ifdef TSpPointFormatInputData}
procedure TSpPoint.SetCaption(Value: string);
begin
 inherited;
 self.setNumber(value);
end;

procedure TSpPoint.SetX(Value: Double);
begin
 Value:= RoundTo(value,-2);
 inherited;
end;

procedure TSpPoint.SetY(Value: Double);
begin
 Value:= RoundTo(value,-2);
 inherited;
end;
{$endif}

{$endregion}

function _ROUNDmsmGetAreaOf(Points: TMSMPoints) : real;
var
 _s : Real;
 _i : integer;
begin
 Result:= 0.0;
 if Points.Count<3 then exit;
 _s := 0.0;
 if msmGetDirectionOf(Points)=TDirectionCurveKind.dcWithOutHourArrow then begin
  for _i := 0 to Points.Count-2 do
   _s := _s + ( 0.5*( (RoundTo(Points.Items[_i+1].X,-2)-RoundTo(Points.Items[_i].X,-2))*
                     (RoundTo(Points.Items[_i+1].Y,-2)+RoundTo(Points.Items[_i].Y,-2))));
 result := _s + ( 0.5*( (RoundTo(Points.First.X,-2)- RoundTo(Points.Last.X,-2))*
                      (RoundTo(Points.First.Y,-2)+RoundTo(Points.Last.Y,-2))));
 end else begin
   for _i := Points.Count-1 downto 1 do
   _s := _s + ( 0.5*( (-RoundTo(Points.Items[_i-1].X,-2)+RoundTo(Points.Items[_i].X,-2))*
                     (-RoundTo(Points.Items[_i-1].Y,-2)-RoundTo(Points.Items[_i].Y,-2))));

   result := _s + ( 0.5*( (-RoundTo(Points.First.X,-2)+ RoundTo(Points.Last.X,-2))*
                      (RoundTo(Points.First.Y,-2)+RoundTo(Points.Last.Y,-2))));
 end;
end;

{ TSpPointsList<T> }

class function TSpPointsList<T>.AreaOf(aPoints: TMSMPointsCustom<T>): double;
var
 _s : Real;
 _i : integer;
begin
// Result:= abs(msmGetAreaOf(TMSMPoints(aPoints)));
Result:= 0.0;
 if aPoints.Count<3 then exit;
 _s := 0.0;
 for _i := 0 to aPoints.Count-2 do
   _s := _s + ( 0.5*( (RoundTo(aPoints.Items[_i+1].X,-2)-RoundTo(aPoints.Items[_i].X,-2))*
                     (RoundTo(aPoints.Items[_i+1].Y,-2)+RoundTo(aPoints.Items[_i].Y,-2))));
 result := _s + ( 0.5*( (RoundTo(aPoints.First.X,-2)- RoundTo(aPoints.Last.X,-2))*
                      (RoundTo(aPoints.First.Y,-2)+RoundTo(aPoints.Last.Y,-2))));

 //����� ��������� "�� �������" -> ����������� "�������������" ������� (������� ������ �������)
 if Result<0 then begin
   _s := 0.0;
   for _i := aPoints.Count-1 downto 1 do
   _s := _s + ( 0.5*( (-RoundTo(aPoints.Items[_i-1].X,-2)+RoundTo(aPoints.Items[_i].X,-2))*
                     (-RoundTo(aPoints.Items[_i-1].Y,-2)-RoundTo(aPoints.Items[_i].Y,-2))));

   result := _s + ( 0.5*( (-RoundTo(aPoints.First.X,-2)+ RoundTo(aPoints.Last.X,-2))*
                      (RoundTo(aPoints.First.Y,-2)+RoundTo(aPoints.Last.Y,-2))));
 end;
end;

constructor TSpPointsList<T>.Create(AOwnsObjects: Boolean);
begin
 inherited Create(AOwnsObjects);
end;

class function TSpPointsList<T>.dPOf(aPoints: TMSMPointsCustom<T>; aRMS: Double): integer;
var
  _ex: TMSMRect;
  _area: Double;
  _a,_b,_k: Double;
 begin
  _area:= AreaOf(aPoints);
  _ex:= TMSMRect.Create(0,0,0,0);
  msmGetExtentArea(TMSMPoints(aPoints),_ex);
  _ex.DoCheckOrientation;
  _a:= abs(_ex.GetSideX);
  _b:= abs(_ex.GetSideY);
  //_k:= abs(_ex.GetSideX/_ex.GetSideY);
  _K:= abs(max(_a,_b)/min(_a,_b));
  Result:= round(2*aRMS*Sqrt(_Area)*sqrt((1+sqr(_K))/(2*_K)));
end;


function TSpPointsList<T>.dogetArea: double;
begin
 Result:= TSpPointsList<T>.AreaOf(self);
end;

function TSpPointsList<T>.dogetLength: double;
begin
 Result:= TSpPointsList<T>.LengthOf(self);
end;

function TSpPointsList<T>.dogetPerimeter: double;
begin
 Result:= TSpPointsList<T>.PerimeterOf(self);
end;

function TSpPointsList<T>.doisClockwise: boolean;
begin
 Result:= TSpPointsList<T>.isClockwiseOf(self);
end;

function TSpPointsList<T>.doisClosed: boolean;
begin
 Result:= TSpPointsList<T>.isClosedOf(self);
end;

function TSpPointsList<T>.getDP(aRMS: double): integer;
begin
 Result:= dPOf(self,aRMS);
end;

class function TSpPointsList<T>.isClockwiseOf(aPoints: TMSMPointsCustom<T>): boolean;
begin
 Result:= msmGetDirectionOf(TMSMPoints(aPoints))=dcWithHourArrow;
end;

class function TSpPointsList<T>.isClosedOf(aPoints: TMSMPointsCustom<T>): boolean;
begin
 if aPoints.Count<2 then
  Result:= FALSE
 else
  Result:= aPoints.First.IsEqualPos(aPoints.Last);
end;

class function TSpPointsList<T>.LengthOf(aPoints: TMSMPointsCustom<T>): double;
var
 _i: integer;
 _p: T;
begin
 Result:= 0;
 for _i:= 0 to aPoints.Count-2 do
  Result:= Result+msmGetDistanceBetweenPoints(aPoints[_i],aPoints[_i+1])
end;

class function TSpPointsList<T>.PerimeterOf(aPoints: TMSMPointsCustom<T>): double;
begin
 Result:= TSpPointsList<T>.LengthOf(aPoints);
 if not TSpPointsList<T>.isClosedOf(aPoints) then
  Result:= Result+msmGetDistanceBetweenPoints(aPoints.First,aPoints.Last);
end;

{ TSpBorderPoint.TNewPoint }

function TSpBorderPoint.TNewPoint.getClone: TMSMPoint;
begin
 Result:= TSpBorderPoint.TNewPoint.Create;
 TNewPoint(Result).assign(self);
end;

function TSpBorderPoint.TNewPoint.getCopy: TNewPoint;
begin
 Result:= TNewPoint(self.getClone);
end;

{ TOldPoint }

function TSpBorderPoint.TOldPoint.getClone: TMSMPoint;
begin
 Result:= TSpBorderPoint.TOldPoint.Create;
 TOldPoint(Result).assign(self);
end;

function TSpBorderPoint.TOldPoint.getCopy: TOldPoint;
begin
 Result:= TOldPoint(self.getClone);
end;


{ TSpBorderPoint }

constructor TSpBorderPoint.Create;
begin
 inherited;
 self.dosetKind(psbExisted);
end;

constructor TSpBorderPoint.Create(aKind: TKind);
begin
 inherited Create;
 self.dosetKind(aKind);
end;

procedure TSpBorderPoint.dosetKind(aValue: TKind);
begin
 case aValue of
   psbExisted: begin
    if self.FOldPoint = nil then
     self.FOldPoint:= TOldPoint.Create;
    if self.FNewPoint = nil then
     self.FNewPoint:= TNewPoint.Create;
   end;
   psbCreated: FreeAndNil(self.FOldPoint);
   psbRemoved: FreeAndNil(self.FNewPoint);
 end;
end;

function TSpBorderPoint.getKind: TKind;
begin
 if Assigned(self.NewPoint) xor Assigned(self.OldPoint) then begin
  if Assigned(self.NewPoint) then
   Result:= psbCreated
  else
   Result:= psbRemoved;
 end else
 //����� �� ���������, � ���� ������ ���� ���� ���� ��������������� �����
  Result:= psbExisted;
end;

function TSpBorderPoint.isEqualPoint(aCompare: TSpBorderPoint): Boolean;
begin
 if self.Kind<>aCompare.Kind then EXIT(FALSE);
 case self.Kind of
   psbExisted: Result:= self.NewPoint.isEqualPoint(aCompare.NewPoint)
                    and self.OldPoint.isEqualPoint(aCompare.OldPoint);
   psbCreated: Result:= self.NewPoint.isEqualPoint(aCompare.NewPoint);
   psbRemoved: Result:= self.OldPoint.isEqualPoint(aCompare.OldPoint);
 end;
end;

procedure TSpBorderPoint.setKind(aValue: TKind);
begin
 if self.Kind=aValue then EXIT;
 self.dosetKind(aValue);
end;

{ TSpBorder }

function TSpBorder.getArea: double;
var
 _List: TSpPointsList<TSpBorderPoint.TNewPoint>;
begin
 _List:= TSpPointsList<TSpBorderPoint.TNewPoint>.Create(FALSE);
  self.NewPointsToList(_List);
  Result:= RoundTo(_List.Area,_AreaRoundTo);
 _List.Free;
end;

function TSpBorder.getDP: integer;
var
 _rms: Double;
 _List: TSpPointsList<TSpBorderPoint.TNewPoint>;
begin
 _List:= TSpPointsList<TSpBorderPoint.TNewPoint>.Create(FALSE);
 _rms:= self.getMaxRmsOf(_List);
 if _rms=0 then
  Result:= -1
 else
  Result:= _List.getDP(_rms);
 _List.Free;
end;


function TSpBorder.getMaxRmsOf(aList: TList<TSpBorderPoint.TNewPoint>): double;
var
 _point: TSpPoint;
begin
 self.NewPointsToList(aList);
 Result:= 0;
 for _point in aList do
  if not _point.Delta.Empty then
   if _point.Delta.Value>Result then
            Result:= _point.Delta.Value;
end;

function TSpBorder.getNewPointsCount: integer;
var
 _i: integer;
begin
 Result:= 0;
 for _i := 0 to self.Count-1 do
  if self.Items[_i].NewPoint <> nil then
   inc(Result);
end;

function TSpBorder.getOldPointsCount: integer;
var
 _i: integer;
begin
 Result:= 0;
 for _i := 0 to self.Count-1 do
  if self.Items[_i].OldPoint <> nil then
   inc(Result);
end;

function TSpBorder.NewPointsToList(aDest: TList<TSpBorderPoint.TNewPoint>): integer;
var
 _i: integer;
begin
 Result:= 0;
 for _i := 0 to self.Count-1 do
  if self.Items[_i].NewPoint<>nil then begin
   aDest.Add(self.Items[_i].NewPoint);
   inc(Result);
  end;
end;

function TSpBorder.OldPointsToList(aDest: TList<TSpBorderPoint.TOldPoint>): integer;
var
 _i: integer;
begin
 Result:= 0;
 for _i := 0 to self.Count-1 do
  if self.Items[_i].OldPoint<>nil then begin
   aDest.Add(self.Items[_i].OldPoint);
   inc(Result);
  end;
end;

{ TSpPoint.TNumber }

class operator TSpPoint.TNumber.Equal(const a: TNumber; b: TNumber): boolean;
begin
 Result:= (a.Empty=b.Empty) and (a.Value=b.Value);
end;

class operator TSpPoint.TNumber.Equal(const a: string; b: TNumber): boolean;
begin
 Result:= (b.Empty and (a='')) or (IntToStr(b.Value) = a);
end;


class operator TSpPoint.TNumber.Implicit(const Value: string): TNumber;
begin
 if Value='' then
  Result.Empty:= TRUE
 else
  Result.Value:= StrToInt(Value);
end;

class operator TSpPoint.TNumber.Equal(const a: cardinal; b: TNumber): boolean;
begin
  if b.Empty then
   Result:= FALSE
  else
   Result:= b.Value=a;
end;

class operator TSpPoint.TNumber.Implicit(const Value: TNumber): Cardinal;
begin
 Result:= Value.Value;
end;

class function TSpPoint.TNumber.isAcceptable(const Value: string): boolean;
var
 _int: integer;
begin
 Result:= (Value='') or (TryStrToInt(Value,_int));
end;

class operator TSpPoint.TNumber.Implicit(const Value: TNumber): string;
begin
 if Value.Empty then
  Result:= ''
 else
  Result:= IntToStr(Value.Value);
end;

class operator TSpPoint.TNumber.Implicit(const Value: Cardinal): TNumber;
begin
 Result.Value:= Value;
end;

procedure TSpPoint.TNumber.setEmptyState(aValue: boolean);
begin
 self.FEmpty:= aValue;
end;

procedure TSpPoint.TNumber.setValue(aValue: Cardinal);
begin
 self.FValue:= aValue;
 self.setEmptyState(FALSE);
end;

{ TSpPoint.TPrefix }

class operator TSpPoint.TPrefix.Equal(const a: TPrefix; b: TPrefix): boolean;
begin
 Result:= a.Value=b.Value;
end;

function TSpPoint.TPrefix.getValueAsStr: string;
begin
 if self.IsNew then
  Result:= SpConsts.NewPointPrefix
 else
  Result:='';
end;

class operator TSpPoint.TPrefix.Equal(const a: string; b: TPrefix): boolean;
begin
 Result:= a=b.Value;
end;

class operator TSpPoint.TPrefix.Implicit(const Value: TPrefix): string;
begin
 Result:= Value.Value;
end;

class operator TSpPoint.TPrefix.Implicit(const Value: string): TPrefix;
begin
 Result.Value:= Value;
end;

class function TSpPoint.TPrefix.isAcceptable(const Value: string): boolean;
begin
 Result:= (Value='') or (LowerCase(Value)=SpConsts.NewPointPrefix);
end;

procedure TSpPoint.TPrefix.setValue(aValue: Boolean);
begin
  self.FValue:= aValue
end;

procedure TSpPoint.TPrefix.setValueAsStr(Value: string);
begin
 self.setValue(LowerCase(Value)=SpConsts.NewPointPrefix);
end;

{ TSpPoint.TDelta }

class operator TSpPoint.TDelta.Equal(const a: TDelta; b: TDelta): boolean;
begin
 Result:= (a.Empty=b.Empty) and (a.Value=b.Value);
end;

class operator TSpPoint.TDelta.Equal(const a: string; b: TDelta): boolean;
var
 _value: double;
begin
 if (b.Empty and (a='')) then
  Result:= TRUE
 else
  Result:= (StrToFloat(a)=b.Value);
end;

class operator TSpPoint.TDelta.Equal(const a: Double; b: TDelta): boolean;
begin
 Result:= a = b.Value;
end;

class operator TSpPoint.TDelta.Implicit(const Value: TDelta): string;
begin
 if Value.Empty then
  Result:= ''
 else
  Result:= FloatToStr(Value.value);
end;

class operator TSpPoint.TDelta.Implicit(const Value: TDelta): Double;
begin
 Result:= Value.Value;
end;

class operator TSpPoint.TDelta.Implicit(const Value: Double): TDelta;
begin
 Result.Value:= Value;
end;

class operator TSpPoint.TDelta.Implicit(const Value: string): TDelta;
begin
 if Value='' then
  Result.Empty:= TRUE
 else
  Result.Value:= StrToFloat(Value);
end;

class function TSpPoint.TDelta.isAcceptable(const Value: double): boolean;
begin
 //Result:= Value>0.0;
 Result:= Value>=0.0;
end;

class function TSpPoint.TDelta.isAcceptable(const Value: string): boolean;
var
 _value: double;
begin
 if Value='' then
  Result:= true
 else
 if TryStrToFloat(Value,_value) then
  Result:= TDelta.isAcceptable(_value)
 else
  Result:= FALSE;
end;

procedure TSpPoint.TDelta.setEmptyState(aValue: boolean);
begin
 self.FEmpty:= aValue;
end;

procedure TSpPoint.TDelta.setValue(aValue: Double);
begin
 if self.isAcceptable(aValue) then begin
  self.FValue:= RoundTo(aValue,ValueRoundTo);
  self.setEmptyState(FALSE);
 end else
  raise ExceptionValue.Create(rsInvalidValue);
end;


{ TSpValidator }

class function TSpValidator.Validate(value: TSpBorder; log: TStringBuilder): boolean;

procedure _writeInLog(const st: string);
begin
 if Assigned(log) then begin
  log.AppendLine;
  log.Append(st);
 end;
end;

begin
 Result:= true;
 if value.Count<3 then begin
  _writeInLog(rs05210B41);
  EXIT(FALSE);
 end;

 if not value.Last.isEqualPoint(value.First) then begin
   _writeInLog(rs22DC5ED7);
   Result:= false;
 end;
end;

class function TSpValidator.Validate(value: TSpContour; log: TStringBuilder): boolean;
var
 _listp: TSpPointsList<TSpBorderPoint.TNewPoint>;
 _i: integer;
 _border: TSpBorder;
begin
 Result:= true;
{ TODO : �� ����������� ������ ����� }
 for _i := 0 to value.Count-1 do begin
  _border:= value[_i];
 {
  if Assigned(log) then begin
   log.AppendLine;
   if _i=0 then
    log.Append(rsCAB38452)
   else
    log.Append(rs3B7C2C72+' ('+IntToStr(_i)+')');
  end;
 }
  if  TSpValidator.Validate(_border,log) then begin
   _listp:= TSpPointsList<TSpBorderPoint.TNewPoint>.Create(FALSE);
   _border.NewPointsToList(_listp);
   if  (_listp.IsClockwise =(_i=0)) then begin
    Result:= false;
    if Assigned(log) then begin
     log.AppendLine;
     if _i=0 then
      log.Append(rsAA65D007)
     else
      log.Append(rs830BBDDE+' ('+IntToStr(_i)+')')
    end;
   end;
   _listp.Free;
  end else
   Result:= false;


 end;

end;

class function TSpValidator.Validate(value: TSpMContour; log: TStringBuilder): boolean;
var
 _contourLog: TStringBuilder;
 _contour: TSpContour;
begin
 if Assigned(log) then
  _contourLog:= TStringBuilder.Create
 else
  _contourLog:= nil;
 Result:= True;
 for _contour in value do
  if not TSpValidator.Validate(_contour,_contourLog) then begin
   if Assigned(_contourLog) then begin
    log.Append('������ (').Append(value.IndexOf(_contour)).Append(') - ').Append(_contour.Caption).AppendLine;
    log.Append(_contourLog.ToString).AppendLine.Append('------').AppendLine;
    _contourLog.Clear;
   end;
   Result:= false;
  end;
 _contourLog.Free;
end;

class function TSpValidator.Validate(value: TSpMSubParcel; log: TStringBuilder): Boolean;
var
 _subParcelLog : TStringBuilder;
 _subParcel : TSpSubParcel;
begin
 if Assigned(log) then _subParcelLog:= TStringBuilder.Create
 else  _subParcelLog:= nil;
 Result:= True;
 for _subParcel in value do
  if not TSpValidator.Validate(_subParcel,_subParcelLog) then begin
   if Assigned(_subParcelLog) then begin
    log.Append('����� (').Append(value.IndexOf(_subParcel)).Append(') - ').Append(_subParcel.Caption).AppendLine;
    log.Append(_subParcelLog.ToString).AppendLine.Append('------').AppendLine;
    _subParcelLog.Clear;
   end;
   Result:= false;
  end;
 _subParcelLog.Free;
end;

class function TSpValidator.Validate(value: TSpSubParcel; log: TStringBuilder): boolean;
var
 _listp: TSpPointsList<TSpBorderPoint.TNewPoint>;
 _i: integer;
 _border: TSpBorder;
begin
 Result:= true;
 for _i := 0 to value.Count-1 do begin
  _border:= value[_i];
  if  TSpValidator.Validate(_border,log) then begin
   _listp:= TSpPointsList<TSpBorderPoint.TNewPoint>.Create(FALSE);
   _border.NewPointsToList(_listp);
   if  (_listp.IsClockwise =(_i=0)) then begin
    Result:= false;
    if Assigned(log) then begin
     log.AppendLine;
     if _i=0 then
      log.Append(rsAA65D007)
     else
      log.Append(rs830BBDDE+' ('+IntToStr(_i)+')')
    end;
   end;
   _listp.Free;
  end else
   Result:= false;
 end;
end;

class function TSpValidator.Validate(value: TSpMCSubParcel; log: TStringBuilder): Boolean;
var
 _subParcelLog : TStringBuilder;
 _subParcel : TSpMSubParcel;
begin
 if Assigned(log) then _subParcelLog:= TStringBuilder.Create
 else  _subParcelLog:= nil;
 Result:= True;
 for _subParcel in value do
  if not TSpValidator.Validate(_subParcel,_subParcelLog) then begin
   if Assigned(_subParcelLog) then begin
    log.Append(' ������ �����  (').Append(value.IndexOf(_subParcel)).Append(') - ').Append('����� ����� ����� �������').AppendLine;
    log.Append(_subParcelLog.ToString).AppendLine.Append('------').AppendLine;
    _subParcelLog.Clear;
   end;
   Result:= false;
  end;
 _subParcelLog.Free;
end;

{ TSpContour }

function TSpContour.getArea: double;
var
 _border: TSpBorder;
begin
 for _border in self do
  if _border = self.First then
   Result:= _border.getArea
  else
   Result:= Result-_border.getArea;
end;

function TSpContour.getCadNumItem(aItem: TCadNumItemsKind): string;
var
 _math: TMatch;
begin
 _math:= TRegEx.Match(self.Caption,cnRegExCadastralNumber);
 if not _math.Success then Exit('');
 TRY
  Result:= _math.Groups.Item[cnCadNumItems[aItem]].Value;
 EXCEPT
  on e: ERegularExpressionError do Result:= '';
 END;
end;

function TSpContour.getDP: integer;
begin
 if self.Count=0 then
  Result:= -1
 else
  Result:= self.First.getDP;
end;

procedure TSpContour._checkDirection;
var
 _listp: TSpPointsList<TSpBorderPoint.TNewPoint>;
 _i: integer;
 _border: TSpBorder;
begin
 for _i := 0 to self.Count-1 do begin
  _border:= self[_i];
  _listp:= TSpPointsList<TSpBorderPoint.TNewPoint>.Create(FALSE);
  _border.NewPointsToList(_listp);
  if  (_listp.IsClockwise =(_i=0)) then
   _border.Reverse;
  _listp.Free;
 end;
end;

{ TSpMContour }

function TSpMContour.getArea: double;
var
 _contour: TSpContour;
begin
 Result:= 0;
 for _contour in self do
  Result:= Result+_contour.getArea;
end;

function TSpMContour.getDP: integer;
var
 _contour: TSpContour;
begin
 if self.Count=0 then
  exit(-1)
 else
  Result:= 0;
 for _contour in self do
  Result:= Result+_contour.getDP;
end;


procedure TSpMContour._checkDirection;
var
 _contour: TSpContour;
begin
 for _contour in self do
  _contour._checkDirection;

end;

{ TSpSubParcel }

function TSpSubParcel.getArea: Double;
var
 _border: TSpBorder;
begin
 for _border in self do
  if _border = self.First then
   Result:= _border.getArea
  else
   Result:= Result-_border.getArea;
end;

function TSpSubParcel.getCadNumItem(aItem: TCadNumItemsKind): string;
var
 _math: TMatch;
begin
 _math:= TRegEx.Match(self.Caption,cnRegExCadastralNumber);
 if not _math.Success then Exit('');
 TRY
  Result:= _math.Groups.Item[cnCadNumItems[aItem]].Value;
 EXCEPT
  on e: ERegularExpressionError do Result:= '';
 END;
end;

function TSpSubParcel.getDP: integer;
begin
 if self.Count=0 then
  Result:= -1
 else
  Result:= self.First.getDP;
end;

procedure TSpSubParcel._checkDirection;
var
 _listp: TSpPointsList<TSpBorderPoint.TNewPoint>;
 _i: integer;
 _border: TSpBorder;
begin
 for _i := 0 to self.Count-1 do begin
  _border:= self[_i];
  _listp:= TSpPointsList<TSpBorderPoint.TNewPoint>.Create(FALSE);
  _border.NewPointsToList(_listp);
  if  (_listp.IsClockwise =(_i=0)) then
   _border.Reverse;
  _listp.Free;
 end;
end;

{ TSpMSubParcel }

function TSpMSubParcel.getArea: double;
var
 _subParcel : TSpSubParcel;
begin
 Result:= 0;
 for _subParcel in self do
  Result:= Result+_subParcel.getArea;
end;

function TSpMSubParcel.getDP: integer;
var
 _subParcel : TSpSubParcel;
begin
 if self.Count=0 then
  exit(-1)
 else
  Result:= 0;
 for _subParcel in self do
  Result:= Result+_subParcel.getDP;
end;

procedure TSpMSubParcel._checkDirection;
var
 _subParcel : TSpSubParcel;
begin
 for _subParcel in self do
  _subParcel._checkDirection;
end;

{ TSpMCSubParcel }

function TSpMCSubParcel.getArea: double;
var
 _subParcel : TSpMSubParcel;
begin
 Result:= 0;
 for _subParcel in self do
  Result:= Result+_subParcel.getArea;
end;

function TSpMCSubParcel.getDP: integer;
var
 _subParcel : TSpMSubParcel;
begin
 if self.Count=0 then
  exit(-1)
 else
  Result:= 0;
 for _subParcel in self do
  Result:= Result+_subParcel.getDP;
end;

procedure TSpMCSubParcel._checkDirection;
var
 _subParcel : TSpMSubParcel;
begin
 for _subParcel in self do
  _subParcel._checkDirection;
end;

end.

