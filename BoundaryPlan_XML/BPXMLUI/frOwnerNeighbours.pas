unit frOwnerNeighbours;
(*

*)

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ActnList, ButtonGroup, ExtCtrls,

  STD_MP, ImgList, System.Actions,frOwnerNeighbour,XMLIntf,fmContainer,
  JvExControls, JvGroupHeader;

type
  TFrameOwnerNeighbours = class(TFrame)
    PanelContours_List: TPanel;
    Contours_List_ButtonGroup: TButtonGroup;
    ActionList: TActionList;
    Action_Contrours_AddNew: TAction;
    Action_Contours_DeleteCurrent: TAction;
    Action_Contours_SetCurrent: TAction;
    lblAttribute_NameRightr_Label: TLabel;
    edtAttribute_NameRight: TEdit;
    grp: TGroupBox;
    pnl1: TPanel;
    Contours_List_ListBox: TComboBox;
    jvgrphdr1: TJvGroupHeader;
    pnl2: TPanel;

    procedure Action_Contrours_AddNewExecute(Sender: TObject);
    procedure Action_Contours_DeleteCurrentExecute(Sender: TObject);
    procedure Action_Contours_SetCurrentExecute(Sender: TObject);
    procedure lstContours_List_ListBox1DrawItem(Control: TWinControl;
      Index: Integer; Rect: TRect; State: TOwnerDrawState);
    procedure edtAttribute_NameRightChange(Sender: TObject);
  private
   { Private declarations }
   FXmlOwnerNeighbours           : IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours;
   FOwnerNeighbourr_frame        : TFrameOwnerNeighbour;

   procedure Contours_List_DoSetCurrentItem(IndexItem: integer);
   procedure Contours_List_DoRefresh(A: Integer = -1; B: Integer = -1);
   procedure Contours_List_DoUpDate;
   procedure Contours_List_DoAddNewItem;
   procedure Contours_List_DoDeleteItem(IndexItem: Integer);


   procedure OnChange_Attribute_DefinitionOrNumberPP(sender: Tobject);
   procedure OnChange_Attribute_ContactAdress(sender: Tobject);


    procedure Data_attribute_NameRight_write;
    procedure Data_attribute_NameRight_read;

  protected
   procedure InitializationUI;
   procedure SetContoursInvariable(const aContours: IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours);

   procedure Contours_List_SetCurrent(index: integer);
  public
   { Public declarations }
   constructor Create(Owner: TComponent); override;

   function  IsAssignedContours: Boolean;
   function  IsAssignedContoursInvariable: Boolean;

   property OwnerNeighbourr: TFrameOwnerNeighbour read FOwnerNeighbourr_frame;
   property XmlOwnerNeighbours: IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours read FXmlOwnerNeighbours write SetContoursInvariable;
  end;

implementation

uses MsXMLAPI;

{$R *.dfm}


procedure TFrameOwnerNeighbours.Action_Contours_DeleteCurrentExecute(Sender: TObject);
begin
 if not IsAssignedContours then EXIT;
 if self.Contours_List_ListBox.ItemIndex=-1 then EXIT;
 if MessageDlg('������� ������: '+' � ���������������'+'?',mtInformation,mbYesNo,-1) = mrYes then
 begin
     self.Contours_List_DoDeleteItem(self.Contours_List_ListBox.ItemIndex);
     self.Contours_List_SetCurrent(self.Contours_List_ListBox.ItemIndex);
     if not self.FOwnerNeighbourr_frame.IsAssignedContourIvariable then
      self.Contours_List_SetCurrent(0);
 end;
end;

procedure TFrameOwnerNeighbours.Action_Contours_SetCurrentExecute(Sender: TObject);
begin
 self.Contours_List_SetCurrent(self.Contours_List_ListBox.ItemIndex);
end;

procedure TFrameOwnerNeighbours.Action_Contrours_AddNewExecute(Sender: TObject);
begin
 if not self.IsAssignedContours then EXIT;
 self.Contours_List_DoAddNewItem;
 self.Contours_List_SetCurrent(self.Contours_List_ListBox.Items.Count-1);
end;

procedure TFrameOwnerNeighbours.Contours_List_DoRefresh(A, B: Integer);
var
 _Contour: IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbour;
 _i,_j: Integer;
 _ItemName: string;
 _ItemNameDo: string;
begin
 if self.Contours_List_ListBox.Items.Count = 0 then EXIT;

 if (A<0) or (a>=self.Contours_List_ListBox.Items.Count)  then
  a:= 0;

 if (B<A) or (B>=self.Contours_List_ListBox.Items.Count)  then
  B:= self.Contours_List_ListBox.Items.Count-1;

 _j:= self.Contours_List_ListBox.ItemIndex;
     for _i := A to B do begin
     _Contour:= IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbour(pointer(self.Contours_List_ListBox.Items.Objects[_i]));
     _ItemName  := _Contour.NameOwner;
     _ItemNameDo:= _Contour.ContactAddress;
      self.Contours_List_ListBox.Items.Strings[_i]:= _ItemName;
     end;

 self.Contours_List_ListBox.ItemIndex:= _j;
end;



procedure TFrameOwnerNeighbours.Contours_List_DoSetCurrentItem(IndexItem: integer);
var
 _contour: IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbour;
begin
 _contour:= IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbour(pointer(self.Contours_List_ListBox.Items.Objects[IndexItem]));
 self.FOwnerNeighbourr_frame.XmlOwnerNeighbour:= _contour;
end;


procedure TFrameOwnerNeighbours.Contours_List_SetCurrent(index: integer);
begin
 if (index>-1) and (index<self.Contours_List_ListBox.Items.Count) then
 begin
  self.Contours_List_DoSetCurrentItem(index);
  self.Contours_List_ListBox.ItemIndex:= index;
  self.FOwnerNeighbourr_frame.Visible:= True;
 end else
 begin
  self.FOwnerNeighbourr_frame.Visible:= false;
  self.Contours_List_ListBox.ItemIndex:= -1;
  self.FOwnerNeighbourr_frame.XmlOwnerNeighbour:= nil;
 end;
end;


constructor TFrameOwnerNeighbours.Create(Owner: TComponent);
begin
 inherited;
 self.FOwnerNeighbourr_frame:= TFrameOwnerNeighbour.Create(self.grp);
 self.FOwnerNeighbourr_frame.OnChange_Attribute_DefOnNum:= self.OnChange_Attribute_DefinitionOrNumberPP;
 self.FOwnerNeighbourr_frame.OnChange_Attribute_ContactAdress := self.OnChange_Attribute_ContactAdress;
 self.FOwnerNeighbourr_frame.Parent:= self.grp;
 self.FOwnerNeighbourr_frame.Align:= alClient;
 self.FOwnerNeighbourr_frame.Visible:= False;
 self.grp.Visible := true;
end;

procedure TFrameOwnerNeighbours.Data_attribute_NameRight_read;
var
 _node: IXMLNode;
begin
 _node:= self.FXmlOwnerNeighbours.ChildNodes.FindNode('NameRight');
 if (_node<>nil) and (_node.NodeValue<>Null) then
     edtAttribute_NameRight.Text := FXmlOwnerNeighbours.NameRight
 else
    edtAttribute_NameRight.Text := '';
end;

procedure TFrameOwnerNeighbours.Data_attribute_NameRight_write;
begin
  if edtAttribute_NameRight.Text  <> '' then  
    self.FXmlOwnerNeighbours.NameRight:= self.edtAttribute_NameRight.Text
  else
    MsXml_RemoveChildNode(FXmlOwnerNeighbours,'NameRight');
end;

procedure TFrameOwnerNeighbours.edtAttribute_NameRightChange(Sender: TObject);
begin
 Data_attribute_NameRight_write;
end;

procedure TFrameOwnerNeighbours.InitializationUI;
begin
 self.Contours_List_ListBox.Clear;
 if self.IsAssignedContours then begin
  self.Contours_List_DoUpDate;
  self.Contours_List_SetCurrent(0);
  self.Data_attribute_NameRight_read;
 end
else
 begin
   FOwnerNeighbourr_frame.XmlOwnerNeighbour := nil;
   edtAttribute_NameRight.Text := '';
 end;
end;


function TFrameOwnerNeighbours.IsAssignedContours: Boolean;
begin
 Result:= self.FXmlOwnerNeighbours<>nil;
end;

function TFrameOwnerNeighbours.IsAssignedContoursInvariable: Boolean;
begin
 result := self.FXmlOwnerNeighbours <> nil;
end;

procedure TFrameOwnerNeighbours.OnChange_Attribute_ContactAdress(
  sender: Tobject);
begin
 self.Contours_List_DoRefresh(self.Contours_List_ListBox.ItemIndex,self.Contours_List_ListBox.ItemIndex);
end;

procedure TFrameOwnerNeighbours.OnChange_Attribute_DefinitionOrNumberPP(sender: tobject);
begin
 self.Contours_List_DoRefresh(self.Contours_List_ListBox.ItemIndex,self.Contours_List_ListBox.ItemIndex);
end;


procedure TFrameOwnerNeighbours.SetContoursInvariable(
  const aContours: IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours);
begin
 if self.FXmlOwnerNeighbours=aContours then EXIT;
 begin
     self.FXmlOwnerNeighbours := aContours;
     self.Contours_List_SetCurrent(-1);
     self.InitializationUI;
 end;
end;

procedure TFrameOwnerNeighbours.Contours_List_DoAddNewItem;
var
 _contour: IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbour;
 _ItemName: string;
begin
 _contour:= self.FXmlOwnerNeighbours.OwnerNeighbour.Add;
 _ItemName:= IntToStr(self.FXmlOwnerNeighbours.OwnerNeighbour.Count);
 _contour.NameOwner := _ItemName;
 self.Contours_List_ListBox.AddItem(_ItemName,pointer(_contour));
end;

procedure TFrameOwnerNeighbours.Contours_List_DoDeleteItem(IndexItem: Integer);
var
 _contour: IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbour;
 _index: Integer;
begin
 _contour:= IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbour(pointer(self.Contours_List_ListBox.Items.Objects[IndexItem]));
 if self.FOwnerNeighbourr_frame.XmlOwnerNeighbour=_contour then
  self.Contours_List_SetCurrent(IndexItem-1);
 self.Contours_List_ListBox.Items.Delete(IndexItem);
 self.FXmlOwnerNeighbours.OwnerNeighbour.Remove(_contour);
 _contour:= nil;
end;

procedure TFrameOwnerNeighbours.Contours_List_DoUpDate;
var
 _i, _index: Integer;
 _Contour: IXMLRelatedParcels_ParcelNeighbours_ParcelNeighbour_OwnerNeighbours_OwnerNeighbour;
 _ItemName: string;
 _ItemName_Do: string;
begin
 _index:= self.Contours_List_ListBox.ItemIndex;
  self.Contours_List_ListBox.Clear;
     for _i := 0 to self.FXmlOwnerNeighbours.OwnerNeighbour.Count-1 do begin
      _Contour:= self.FXmlOwnerNeighbours.OwnerNeighbour[_i];
     _ItemName   := _Contour.NameOwner;
     _ItemName_Do:= _Contour.ContactAddress;
      _Contour.NameOwner := _ItemName;
      _Contour.ContactAddress := _ItemName_Do;
      self.Contours_List_ListBox.AddItem(_ItemName,pointer(_Contour));
     end;
 self.Contours_List_SetCurrent(_index);
 if not self.FOwnerNeighbourr_frame.IsAssignedContourIvariable then
  self.Contours_List_SetCurrent(0);
end;

procedure TFrameOwnerNeighbours.lstContours_List_ListBox1DrawItem(
  Control: TWinControl; Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin
   With Contours_List_ListBox do
   begin
     if ItemIndex = Index then
       Canvas.Font.Style :=  Canvas.Font.Style + [fsBold]
     else
       Canvas.Font.Style :=  Canvas.Font.Style - [fsBold];
     Canvas.TextOut(Rect.Left +1,Rect.Top +1,items[index]);
   end;
end;

end.

