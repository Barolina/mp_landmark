unit frExistEZParcels;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ActnList, StdCtrls, ComCtrls, Buttons, Mask, ExtCtrls,

  STD_MP, frSubParcels,
  XMLIntf,
  ImgList, BPCommon, ButtonGroup, System.Actions,
  frInnerCadastralNumbers,
  frArea, frAreaDelta, frRelatedParcels, fmComposition_EZ,unRussLang;

type
  TFrameExistEZParcels = class(TFrame)
    pgcExistEZParcels: TPageControl;
    tsArea: TTabSheet;
    lblArea: TLabel;
    chkArea: TCheckBox;
    SubParcels_TabSheet: TTabSheet;
    SubParcels_IsFill_CheckBox: TCheckBox;
    tsCompositionEZ: TTabSheet;
    ActionList: TActionList;
    Panel_Navigation: TPanel;
    SpeedButton_Nav_Back: TSpeedButton;
    SpeedButton_Nav_Next: TSpeedButton;
    cbbNav_GoTo: TComboBox;
    Action_ProvPassCadNums_IsFill: TAction;
    Action_InnerCadNums_IsFill: TAction;
    Action_SubParcels_IsFill: TAction;
    Action_Encumbrance_IsFill: TAction;
    pnlCadastralNumber: TPanel;
    lblListCadastralNUmbers: TLabel;
    lbl1: TLabel;
    medtCadastralNumber: TMaskEdit;
    medtCadNumBlock_value: TMaskEdit;
    tsInnerCadastral: TTabSheet;
    pnllInnerCadastralNumber: TPanel;
    chkInnerCadastralNumber: TCheckBox;
    chkCompositionEZ: TCheckBox;
    tsRelatedParcels: TTabSheet;
    pnlRelatedParcels: TPanel;
    chkRelatedParcels: TCheckBox;
    pnlCompositionEZ: TPanel;
    pnlSubParcels: TPanel;
    pnl1: TPanel;

    procedure SpeedButton_Nav_BackClick(Sender: TObject);
    procedure SpeedButton_Nav_NextClick(Sender: TObject);
    procedure cbbNav_GoToChange(Sender: TObject);
    procedure medtCadastralNumberExit(Sender: TObject);
    procedure medtCadNumBlock_valueExit(Sender: TObject);
    procedure chknnerCadastralNumberClick(Sender: TObject);
    procedure chkCompositionEZClick(Sender: TObject);
    procedure chkRelatedParcelsClick(Sender: TObject);

    procedure PanelExistEZLbl(Sender : TObject);
    procedure MsgDeleteChecked(Sender : TObject);
    procedure SubParcels_IsFill_CheckBoxClick(Sender: TObject);
  private
    { Private declarations }
    FXmlExistEZParcel   : IXMLTExistEZParcel; // ������ ����������������

    FInnerCadNums_frame : TFrameInnerCadastralNumbers;
    FArea               : TFrameArea;
    FArea_Delta         : TFrameAreaDelta;
    FSubParcels_frame   : TFrameSubParcels;
    FRealtyParcels      : TFrameRelatedParcels;
    FCompositionEZ      : TFrameCompostion_EZ;

    procedure UI_AttrExistEZparcel_WriteTo(const aExistParcel
      : IXMLTExistEZParcel);
    procedure UI_AttrExistEZParcel_ReadOf(const aExistParcel
      : IXMLTExistEZParcel);
    procedure UI_CadastralEZBlock_Write_To(const aExistParcel
      : IXMLTExistEZParcel);
    procedure UI_CadastralEZBlock_Read_To(const aExistParcel
      : IXMLTExistEZParcel);

  protected
    procedure InitializationUI;
    procedure SetExistEZParcel(const aExistEZParcel: IXMLTExistEZParcel);
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    function IsAssignedExistEZParcel: Boolean;
    procedure SetActivTab(index: Integer; UseAnimate: Boolean = false);
    property XmlExistEZParcel: IXMLTExistEZParcel read FXmlExistEZParcel
      write SetExistEZParcel;
  end;

implementation

uses MsXMLAPI;

{$R *.dfm}

procedure TFrameExistEZParcels.chkCompositionEZClick(Sender: TObject);
begin
  MsgDeleteChecked(Sender);  
  FCompositionEZ.Visible := chkCompositionEZ.Checked; 
  if chkCompositionEZ.Checked then
    self.FCompositionEZ.XMLCompositionEZ := FXmlExistEZParcel.Composition_EZ
  else
  begin
    MsXml_RemoveChildNode(FXmlExistEZParcel, cnstXmlComposition_EZ);
    FCompositionEZ.XMLCompositionEZ := nil;
  end;
end;

procedure TFrameExistEZParcels.chknnerCadastralNumberClick(Sender: TObject);
begin
  MsgDeleteChecked(Sender);//�������   
  self.FInnerCadNums_frame.Visible := self.chkInnerCadastralNumber.Checked; 
  if self.chkInnerCadastralNumber.Checked then
    self.FInnerCadNums_frame.XmlCadastralNumbersExistEZ :=  FXmlExistEZParcel.Inner_CadastralNumbers
  else
  begin
    MsXml_RemoveChildNode(self.FXmlExistEZParcel, cnstXmlInner_CadastralNumbers);
    self.FInnerCadNums_frame.XmlCadastralNumbersExistEZ := nil;
  end;
end;

procedure TFrameExistEZParcels.chkRelatedParcelsClick(Sender: TObject);
begin
  MsgDeleteChecked(Sender);
  Try
    FRealtyParcels.Visible := chkRelatedParcels.Checked;
    if self.chkRelatedParcels.Checked then
      self.FRealtyParcels.XmlParcelNeighbours :=  FXmlExistEZParcel.RelatedParcels
    else
    begin
      MsXml_RemoveChildNode(self.FXmlExistEZParcel, cnstXmlRelatedParcels);
      self.FRealtyParcels.XmlParcelNeighbours := nil;
    end;
  Except
    on E: Exception do raise Exception.Create('RelatedParcels');
  End;
end;

procedure TFrameExistEZParcels.cbbNav_GoToChange(Sender: TObject);
begin
  self.SetActivTab(self.cbbNav_GoTo.ItemIndex, false);
end;

constructor TFrameExistEZParcels.Create(AOwner: TComponent);
begin
  inherited;
  // NA
  self.SetActivTab(0, false);
  
  self.FInnerCadNums_frame := TFrameInnerCadastralNumbers.Create(self.tsInnerCadastral);
  self.FInnerCadNums_frame.Parent := self.tsInnerCadastral;
  self.FInnerCadNums_frame.Align := alClient;
  self.FInnerCadNums_frame.Visible := false;
  // -- �����
  self.FSubParcels_frame := TFrameSubParcels.Create(self.SubParcels_TabSheet);
  self.FSubParcels_frame.Parent := self.SubParcels_TabSheet;
  self.FSubParcels_frame.Align := alClient;
  self.FSubParcels_frame.Visible := false;
  // -- ������� ���������� �������
  self.FArea := TFrameArea.Create(self.tsArea);
  self.FArea.Parent := self.tsArea;
  self.FArea.Align := alTop;
  // -- �������������� �������� ExistParcel
  self.FArea_Delta := TFrameAreaDelta.Create(self.tsArea);
  self.FArea_Delta.Parent := self.tsArea;
  self.FArea_Delta.Align := alClient;
  // -- ������ ����������������
  self.FCompositionEZ := TFrameCompostion_EZ.Create(self.tsCompositionEZ);
  self.FCompositionEZ.Parent := self.tsCompositionEZ;
  self.FCompositionEZ.Align := alClient;
  Self.FCompositionEZ.Visible := false;
  // -- RelatedParcels
  self.FRealtyParcels := TFrameRelatedParcels.Create(self.tsRelatedParcels);
  self.FRealtyParcels.Parent := self.tsRelatedParcels;
  self.FRealtyParcels.Align := alClient;

end;

procedure TFrameExistEZParcels.SetActivTab(index: Integer; UseAnimate: Boolean = false);
begin
  if (index < 0) or (index > self.pgcExistEZParcels.PageCount - 1) then EXIT;
  self.pgcExistEZParcels.ActivePageIndex := index;

  self.cbbNav_GoTo.ItemIndex := self.pgcExistEZParcels.ActivePageIndex;
  self.SpeedButton_Nav_Back.Enabled := self.cbbNav_GoTo.ItemIndex <> 0;
  self.SpeedButton_Nav_Next.Enabled := self.cbbNav_GoTo.ItemIndex <>self.cbbNav_GoTo.Items.Count - 1;
end;

procedure TFrameExistEZParcels.InitializationUI;
var
  _node: IXMLNode;

begin
  if self.IsAssignedExistEZParcel then
  begin
    UI_AttrExistEZParcel_ReadOf(FXmlExistEZParcel);
    self.medtCadNumBlock_value.Text := FXmlExistEZParcel.CadastralBlock;
    medtCadNumBlock_value.Text := FXmlExistEZParcel.CadastralBlock;

    _node := self.FXmlExistEZParcel.ChildNodes.FindNode(cnstXmlInner_CadastralNumbers);
    self.FInnerCadNums_frame.Visible := (_node <> nil);
    self.chkInnerCadastralNumber.Checked := (_node <> nil);
    self.FInnerCadNums_frame.XmlCadastralNumbersExistEZ :=
      (_node as IXMLTExistEZParcel_Inner_CadastralNumbers);

    { area }
    self.FArea.XmlAreaExistEZ := self.FXmlExistEZParcel.Area;
    { ������������ �������� ������� }
    self.FArea_Delta.XmlExistEZParcel := self.FXmlExistEZParcel;

    _node := self.FXmlExistEZParcel.ChildNodes.FindNode(cnstXmlSubParcels);
    self.FSubParcels_frame.Visible := (_node <> nil);
    self.FSubParcels_frame.Visible := (_node <> nil);
    self.FSubParcels_frame.XmlSubParcels :=   (_node as IXMLTExistParcel_SubParcels);

    _node := self.FXmlExistEZParcel.ChildNodes.FindNode(cnstXmlComposition_EZ);
    self.FCompositionEZ.Visible := (_node <> nil);
    self.chkCompositionEZ.Checked := (_node <> nil);
    self.FCompositionEZ.XMLCompositionEZ :=
      (_node as IXMLTExistEZParcel_Composition_EZ);

    _node := self.FXmlExistEZParcel.ChildNodes.FindNode(cnstXmlRelatedParcels);
    self.FRealtyParcels.Visible := (_node <> nil);
    self.chkRelatedParcels.Checked := (_node <> nil);
    self.FRealtyParcels.XmlParcelNeighbours := (_node as IXMLRelatedParcels);

  end
  else
  begin
    self.FInnerCadNums_frame.XmlCadastralNumbers := nil;
    self.FSubParcels_frame.XmlSubParcels := nil;
    self.FArea.XmlAreaExistEZ := nil;
    self.FArea_Delta.XmlExistEZParcel := nil;
    self.FCompositionEZ.XMLCompositionEZ := nil;
    self.FRealtyParcels.XmlParcelNeighbours := nil;
  end;

end;

function TFrameExistEZParcels.IsAssignedExistEZParcel: Boolean;
begin
  Result := self.FXmlExistEZParcel <> nil;
end;


procedure TFrameExistEZParcels.medtCadastralNumberExit(Sender: TObject);
begin
  if not self.IsAssignedExistEZParcel then    raise Exception.Create(ExMSG_NotAssignedROOT);
  self.UI_AttrExistEZparcel_WriteTo(self.FXmlExistEZParcel);
end;

procedure TFrameExistEZParcels.medtCadNumBlock_valueExit(Sender: TObject);
begin
  if not self.IsAssignedExistEZParcel then
    raise Exception.Create(ExMSG_NotAssignedROOT);
  self.UI_CadastralEZBlock_Write_To(self.FXmlExistEZParcel);
end;

///
/// ����� ��������� �� ����������� �������� ������ 
///
procedure TFrameExistEZParcels.MsgDeleteChecked(Sender: TObject);
begin
  if not (Sender  is TCheckBox) then Exit;   
  if not (Sender as TCheckBox).Checked  then  
  if  MessageDlg(rsDeleteData,mtInformation, mbYesNo,-1) = mrNo then 
  begin
    (Sender as TCheckBox).Checked := not (Sender as TCheckBox).Checked; 
    Exit;
  end;
end;

///
///������ ���������� ��������  ��� ���� ������� ���������� control CheckBox;
///
procedure TFrameExistEZParcels.PanelExistEZLbl(Sender: TObject);
var chkChecked :  TControl;
begin
  chkChecked := (Sender as TPanel).Controls[0];
  TCheckBox(chkChecked).Checked := not TCheckBox(chkChecked).Checked;
end;

procedure TFrameExistEZParcels.SetExistEZParcel(const aExistEZParcel
  : IXMLTExistEZParcel);
begin
  if self.FXmlExistEZParcel = aExistEZParcel then  EXIT;
  
  self.pgcExistEZParcels.TabIndex := 0;
  self.FXmlExistEZParcel := aExistEZParcel;
  self.pgcExistEZParcels.ActivePage := tsInnerCadastral;
  self.InitializationUI;
end;

procedure TFrameExistEZParcels.SpeedButton_Nav_BackClick(Sender: TObject);
begin
  self.SetActivTab(self.cbbNav_GoTo.ItemIndex - 1, false);
end;

procedure TFrameExistEZParcels.SpeedButton_Nav_NextClick(Sender: TObject);
begin
  self.SetActivTab(self.cbbNav_GoTo.ItemIndex + 1, false);
end;

procedure TFrameExistEZParcels.SubParcels_IsFill_CheckBoxClick(Sender: TObject);
var
  node: IXMLNode;
begin
  MsgDeleteChecked(Sender);
  FSubParcels_frame.Visible := SubParcels_IsFill_CheckBox.Checked;
  if self.SubParcels_IsFill_CheckBox.Checked then
  begin
    node := (FXmlExistEZParcel.SubParcels as IXMLNode);
    self.FSubParcels_frame.XmlSubParcels :=(node as IXMLTExistParcel_SubParcels);
  end
  else
  begin
    MsXml_RemoveChildNode(self.FXmlExistEZParcel, cnstXmlSubParcels);
    self.FSubParcels_frame.XmlSubParcels := nil;
  end;
end;

procedure TFrameExistEZParcels.UI_AttrExistEZParcel_ReadOf(const aExistParcel
  : IXMLTExistEZParcel);
begin
  self.medtCadastralNumber.Text := MsXml_ReadAttribute(aExistParcel,cnstXmlCadastralNumber);
end;

procedure TFrameExistEZParcels.UI_AttrExistEZparcel_WriteTo(const aExistParcel
  : IXMLTExistEZParcel);
var
  strKN: string;
begin
  if self.medtCadastralNumber.Text = '' then    EXIT;
  aExistParcel.CadastralNumber := self.medtCadastralNumber.Text;
  aExistParcel.CadastralBlock := BPcCadNum_GetItemValue
    (medtCadastralNumber.Text, [cnDistrict, cnMunicipality, cnBlock]);
  medtCadNumBlock_value.Text := aExistParcel.CadastralBlock;
end;

procedure TFrameExistEZParcels.UI_CadastralEZBlock_Read_To(const aExistParcel
  : IXMLTExistEZParcel);
begin
  self.medtCadNumBlock_value.Text := MsXml_ReadAttribute(aExistParcel,cnstXmlCadastralBlock);
end;

procedure TFrameExistEZParcels.UI_CadastralEZBlock_Write_To(const aExistParcel
  : IXMLTExistEZParcel);
begin
  if self.medtCadNumBlock_value.Text = '' then  EXIT;
  aExistParcel.CadastralBlock := self.medtCadNumBlock_value.Text;
end;

end.
