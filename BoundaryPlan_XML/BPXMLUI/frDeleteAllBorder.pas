unit frDeleteAllBorder;

{
    SpecifyRelatedParcel_DeleteAllBorder
}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ComCtrls,

   STD_MP, ActnList, XMLIntf,
  System.Actions,

  fmSpVCLEditor,
  SurveyingPlan.Xml.Types,
  SurveyingPlan.Xml.Utils;

type
  TFrameDeleteAllBorder = class(TFrame)
  private
    { Private declarations }
    FEditor: TFormEntitySpatial;
    FXMLContour: IXMLContours_DeleteAllBorder;
    FXMLSpecifyRelatedParcel: IXMLTSpecifyRelatedParcel_DeleteAllBorderList;
    FAutoSave: boolean;

    procedure setSpecifyRelatedParcel(aValue: IXMLTSpecifyRelatedParcel_DeleteAllBorderList);
    procedure setContour(aValue: IXMLContours_DeleteAllBorder);
    procedure _doSaveSpecifyRelatedParcel;
    procedure _doSaveContour;
    procedure _doSave(Sender: TObject);
  public type
   TKind = (kUnknown,kSpecifyRelatedParcel,kContour);
  public
    procedure InitializationUI;
    function Kind: TKind;
    { Public declarations }
    constructor Create(Owner: TComponent); override;
    property XMLSpecifyRelatedParcel: IXMLTSpecifyRelatedParcel_DeleteAllBorderList
                        read FXMLSpecifyRelatedParcel write setSpecifyRelatedParcel;
    property XMLContour: IXMLContours_DeleteAllBorder read FXMLContour write setContour;
    ///	<summary>
    ///	  <para>
    ///	    ��������� �������� �� ����-���������� ������ � ����������
    ///	  </para>
    ///	  <para>
    ///	    ��������� (FEditor) ��� ����� ������� xml �����
    ///	  </para>
    ///	</summary>
    property AutoSave: boolean read FAutoSave write FAutoSave;
   end;

implementation



{$R *.dfm}

{ TFrameContour }





{ TFrameDeleteAllBorder }

constructor TFrameDeleteAllBorder.Create(Owner: TComponent);
begin
 inherited;
 self.FEditor:= TFormEntitySpatial.Create(nil);
 self.FEditor.Parent:= self;
 self.FEditor.Align:= alClient;
 self.FEditor.OnNeedSave:= self._doSave;
 self.FEditor.Show;
 self.FAutoSave:= TRUE;
end;

procedure TFrameDeleteAllBorder.InitializationUI;
var
 _contour: TSpContour;
begin
 case self.Kind of
  kUnknown: self.FEditor.ClearALL;
  kSpecifyRelatedParcel:begin
   _contour:= TSpContour.Create;
   _contour.Add(TSpBorder.Create);
   TSpConvert.Border.Convert(self.FXMLSpecifyRelatedParcel,_contour[0]);
   self.FEditor.ReadData(_contour);
   _contour.Free;
  end;
  kContour:begin
   _contour:= TSpContour.Create;
   _contour.Add(TSpBorder.Create);
   TSpConvert.Border.Convert(self.FXMLContour,_contour[0]);
   self.FEditor.ReadData(_contour);
   _contour.Free;
  end;
 end;
end;

function TFrameDeleteAllBorder.Kind: TKind;
begin
 if Assigned(self.FXMLContour) then
  Result:= kContour
 else
  if Assigned(self.FXMLSpecifyRelatedParcel) then
   Result:= kSpecifyRelatedParcel
  else
   Result:= kUnknown;
end;

procedure TFrameDeleteAllBorder.setContour(aValue: IXMLContours_DeleteAllBorder);
begin
 if self.FXMLContour=aValue then EXIT;
 if self.FXMLSpecifyRelatedParcel<>nil then begin
   if self.AutoSave then self._doSaveSpecifyRelatedParcel;
   self.FXMLSpecifyRelatedParcel:= nil;
 end;
 self.FXMLContour:= aValue;
 self.InitializationUI;
end;

procedure TFrameDeleteAllBorder.setSpecifyRelatedParcel(aValue: IXMLTSpecifyRelatedParcel_DeleteAllBorderList);
begin
 if self.FXMLSpecifyRelatedParcel=aValue then EXIT;
 if self.FXMLContour<>nil then begin
   if self.AutoSave then self._doSaveContour;
   self.FXMLContour:= nil;
 end;
 self.FXMLSpecifyRelatedParcel:= aValue;
 self.InitializationUI;
end;

procedure TFrameDeleteAllBorder._doSaveContour;
var
 _Contour: TSpContour;
begin
 _Contour:= TSpContour.Create;
 self.FEditor.WriteData(_Contour);
 if _Contour.Count>0 then
  TSpConvert.Border.Convert(_Contour[0],self.FXMLContour)
 else
  self.FXMLContour.Clear;
 _Contour.Free;
end;

procedure TFrameDeleteAllBorder._doSave(Sender: TObject);
begin
 case self.Kind of
  kUnknown:;
  kSpecifyRelatedParcel: self._doSaveSpecifyRelatedParcel;
  kContour: self._doSaveContour;
 end;
end;

procedure TFrameDeleteAllBorder._doSaveSpecifyRelatedParcel;
var
 _Contour: TSpContour;
begin
 _Contour:= TSpContour.Create;
 self.FEditor.WriteData(_Contour);
 if _Contour.Count>0 then
  TSpConvert.Border.Convert(_Contour[0],self.FXMLSpecifyRelatedParcel)
 else
  self.FXMLSpecifyRelatedParcel.Clear;
 _Contour.Free;
end;

end.

