unit frFIO;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ButtonGroup, StdCtrls, ActnList,

  STD_MP, unRussLang;


const
  rsXML_FIO_Surname = 'Surname';
  rsXML_FIO_First = 'First';
  rsXML_FIO_Patronymic = 'Patronymic';

type
  TFrameFIO = class(TFrame)
    Label1: TLabel;
    FIOSurname_Edit: TEdit;
    Label2: TLabel;
    FIOFirst_Edit: TEdit;
    Label3: TLabel;
    FIOPatronymic_Edit: TEdit;
    procedure FIOSurname_EditExit(Sender: TObject);
    procedure FIOSurname_EditKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FIOFirst_EditExit(Sender: TObject);
    procedure FIOPatronymic_EditExit(Sender: TObject);
  private
    { Private declarations }

    FXmlFIO : IXMLTFIO;

  protected
    procedure InitializationUI;
    procedure SetFIO(const aFIO : IXMLTFIO);
  public
    { Public declarations }
     function isAssignedFIO : boolean;
     procedure UI_FIOSurname_ReadOf(const aFIO: IXMLTFIO);
     procedure UI_FIOFirst_ReadOf(const aFIO: IXMLTFIO);
     procedure UI_FIOPatronymic_ReadOf(const aFIO: IXMLTFIO);
     procedure UI_ReadOf(const aFIO: IXMLTFIO);

     procedure UI_FIOSurname_WriteTo(const aFIO: IXMLTFIO);
     procedure UI_FIOFirst_WriteTo(const aFIO: IXMLTFIO);
     procedure UI_FIOPatronymic_WriteTo(const aFIO: IXMLTFIO);
     procedure UI_WriteTo(const aFIO: IXMLTFIO);

     property XMLFIO : IXMLTFIO read FXmlFIO write SetFIO;
  end;

implementation
uses MsXmlApi;


{$R *.dfm}

{ TFrameFIO }

procedure TFrameFIO.FIOFirst_EditExit(Sender: TObject);
begin
 if not self.isAssignedFIO then raise Exception.Create(ExMSG_NotAssignedROOT);
 self.UI_FIOFirst_WriteTo(self.FXmlFIO);
end;

procedure TFrameFIO.FIOPatronymic_EditExit(Sender: TObject);
begin
 if not self.isAssignedFIO then raise Exception.Create(ExMSG_NotAssignedROOT);
 self.UI_FIOPatronymic_WriteTo(self.FXmlFIO);
end;

procedure TFrameFIO.FIOSurname_EditExit(Sender: TObject);
begin
 if not self.isAssignedFIO then raise Exception.Create(ExMSG_NotAssignedROOT);
 self.UI_FIOSurname_WriteTo(self.FXmlFIO);
end;

procedure TFrameFIO.FIOSurname_EditKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 {TODO:26 if key = VK_RETURN then
  if (Sender is TCustomEdit) then
   TCustomEdit(Sender).Parent.SetFocus;
   }
end;

procedure TFrameFIO.InitializationUI;
begin
 if self.isAssignedFIO then
  self.UI_ReadOf(self.FXmlFIO)
 else begin
  self.FIOSurname_Edit.Text:= '';
  self.FIOFirst_Edit.Text:= '';
  self.FIOPatronymic_Edit.Text:= '';
 end;
end;

function TFrameFIO.isAssignedFIO: boolean;
begin
 Result := FXmlFIO <> nil;
end;

procedure TFrameFIO.SetFIO(const aFIO: IXMLTFIO);
begin
 if self.FXmlFIO=aFIO then EXIT;
 self.FXmlFIO:= aFIO;
 self.InitializationUI;
end;

procedure TFrameFIO.UI_FIOFirst_ReadOf(const aFIO: IXMLTFIO);
begin
self.FIOFirst_Edit.Text:= MsXml_ReadChildNodeValue(aFIO,rsXML_FIO_First);
end;

procedure TFrameFIO.UI_FIOFirst_WriteTo(const aFIO: IXMLTFIO);
var
 _st: string;
begin
 _st:= self.FIOFirst_Edit.Text;
 if _st<>'' then
    aFIO.First:= _st
 else
  MsXml_RemoveChildNode(aFIO,rsXML_FIO_First);
end;

procedure TFrameFIO.UI_FIOPatronymic_ReadOf(const aFIO: IXMLTFIO);
begin
self.FIOPatronymic_Edit.Text:= MsXml_ReadChildNodeValue(aFIO,rsXML_FIO_Patronymic);
end;

procedure TFrameFIO.UI_FIOPatronymic_WriteTo(const aFIO: IXMLTFIO);
var
 _st: string;
begin
 _st:= self.FIOPatronymic_Edit.Text;
 if _st<>'' then
    aFIO.Patronymic:= _st
 else
  MsXml_RemoveChildNode(aFIO,rsXML_FIO_Patronymic);
end;

procedure TFrameFIO.UI_FIOSurname_ReadOf(const aFIO: IXMLTFIO);
begin
 self.FIOSurname_Edit.Text:= MsXml_ReadChildNodeValue(aFIO,rsXML_FIO_Surname);
end;

procedure TFrameFIO.UI_FIOSurname_WriteTo(const aFIO: IXMLTFIO);
var
 _st: string;
begin
 _st:= self.FIOSurname_Edit.Text;
 if _st<>'' then
    aFIO.Surname:= _st
 else
  MsXml_RemoveChildNode(aFIO,rsXML_FIO_Surname);
end;

procedure TFrameFIO.UI_ReadOf(const aFIO: IXMLTFIO);
begin
  UI_FIOSurname_ReadOf(aFIO);
  UI_FIOFirst_ReadOf(aFIO);
  UI_FIOPatronymic_ReadOf(aFIO);
end;

procedure TFrameFIO.UI_WriteTo(const aFIO: IXMLTFIO);
begin
  UI_FIOSurname_WriteTo(aFIO);
  UI_FIOFirst_WriteTo(aFIO);
  UI_FIOPatronymic_WriteTo(aFIO);
end;

end.
