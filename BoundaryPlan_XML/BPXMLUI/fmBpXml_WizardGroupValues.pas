unit fmBpXml_WizardGroupValues;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, Buttons, ExtCtrls, XmlIntf, CheckLst;

type
  TFormBpXml_WizardGroupValues = class(TForm)
    Panel_BottomControl: TPanel;
    BitBtn_Ok: TBitBtn;
    PageControl: TPageControl;
    Source_TabSheet: TTabSheet;
    Dest_TabSheet: TTabSheet;
    Elements_TabSheet: TTabSheet;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Source_ListBox: TListBox;
    Dest_CheckListBox: TCheckListBox;
    Elements_CheckListBox: TCheckListBox;
    NV_Back_BitBtn: TBitBtn;
    NV_Next_BitBtn: TBitBtn;
    BitBtn1: TBitBtn;
    procedure NV_Back_BitBtnClick(Sender: TObject);
    procedure NV_Next_BitBtnClick(Sender: TObject);
    procedure BitBtn_OkClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure Dest_CheckListBoxDblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure SetActivTab(index: integer; UseAnimate: boolean = false);

    procedure ListSource_Clear;
    procedure ListElements_Clear;
    procedure ListDest_Build;

    procedure ListSource_AddItem(Caption: string; const Item: IXMLNode);
    procedure ListElements_AddItem(Caption,XmlName: string);

    procedure RUN;
  end;

var
  FormBpXml_WizardGroupValues: TFormBpXml_WizardGroupValues;

implementation

uses PageControlAnimate, MsXMLAPI;

{$R *.dfm}

{ TFormBpXml_WizardGroupValues }

procedure TFormBpXml_WizardGroupValues.BitBtn1Click(Sender: TObject);
begin
 self.RUN;
 MessageDlg('�������� ��������� �������',mtInformation,[mbOK],-1);
end;

procedure TFormBpXml_WizardGroupValues.BitBtn_OkClick(Sender: TObject);
begin
 self.Close;
end;

procedure TFormBpXml_WizardGroupValues.Dest_CheckListBoxDblClick(
  Sender: TObject);
begin
  if self.Dest_CheckListBox.Items.Count=0 then EXIT;

 if self.Dest_CheckListBox.Checked[0] then
   self.Dest_CheckListBox.CheckAll(cbUnchecked)
 else
   self.Dest_CheckListBox.CheckAll(cbChecked);
end;

procedure TFormBpXml_WizardGroupValues.ListDest_Build;
var
 _i: integer;
begin
 self.Dest_CheckListBox.Clear;
 for _i := 0 to self.Source_ListBox.Count-1 do
  if not self.Source_ListBox.Selected[_i] then
   self.Dest_CheckListBox.AddItem(self.Source_ListBox.Items[_i],pointer(self.Source_ListBox.Items.Objects[_i]));
end;

procedure TFormBpXml_WizardGroupValues.ListElements_AddItem(Caption, XmlName: string);
var
 _p: PString;
begin
 XmlName:= XmlName;
 New(_p);
 _p^:= XmlName;
 self.Elements_CheckListBox.Items.AddObject(Caption,pointer(_p));
 _p:= nil;
end;

procedure TFormBpXml_WizardGroupValues.ListElements_Clear;
var
 _i: integer;
begin
 for _i := 0 to self.Elements_CheckListBox.Items.Count - 1 do
  FreeMem(PString(self.Elements_CheckListBox.Items.Objects[_i]));
 self.Elements_CheckListBox.Items.Clear;
end;

procedure TFormBpXml_WizardGroupValues.ListSource_AddItem(Caption: string;
  const Item: IXMLNode);
begin
 self.Source_ListBox.AddItem(Caption,Pointer(Item));
end;

procedure TFormBpXml_WizardGroupValues.ListSource_Clear;
begin
 self.Source_ListBox.Clear;
end;

procedure TFormBpXml_WizardGroupValues.NV_Back_BitBtnClick(Sender: TObject);
begin
 self.SetActivTab(self.PageControl.ActivePageIndex-1,false);
end;

procedure TFormBpXml_WizardGroupValues.NV_Next_BitBtnClick(Sender: TObject);
begin
 self.SetActivTab(self.PageControl.ActivePageIndex+1,false);
end;

procedure TFormBpXml_WizardGroupValues.RUN;
var
 _Source: IXMLNode;
 _Dest: IXMLNode;
 _Element: string;
 _i,_j: integer;
 _p: PUnicodeString;
begin
 if (self.Source_ListBox.ItemIndex<0) or (self.Source_ListBox.Count = 0) then
  raise Exception.Create('�� �������� ������-��������')
 else
  _Source:= IXMLNode(pointer(self.Source_ListBox.Items.Objects[self.Source_ListBox.ItemIndex]));

  for _i := 0 to self.Dest_CheckListBox.Items.Count-1 do begin
   if not self.Dest_CheckListBox.Checked[_i] then CONTINUE;

   _Dest:= IXMLNode(pointer(self.Dest_CheckListBox.Items.Objects[_i]));

   for _j := 0 to self.Elements_CheckListBox.Items.Count-1 do begin
    if not self.Elements_CheckListBox.Checked[_j] then CONTINUE;

    _Element:= string(pointer(self.Elements_CheckListBox.Items.Objects[_j])^);

    MsXml_CopyChildItem(_Source,_Dest,_Element);
   end;

  end;
end;

procedure TFormBpXml_WizardGroupValues.SetActivTab(index: integer; UseAnimate: boolean= false);
begin
 if (index<0) or (index>self.PageControl.PageCount-1) then EXIT;

 if index = Dest_TabSheet.PageIndex then
  self.ListDest_Build;

 if UseAnimate then
  pcaSetActivTab(self.PageControl,self.PageControl.Pages[index])
 else
  self.PageControl.ActivePageIndex:= index;


 self.NV_Back_BitBtn.Enabled:= self.PageControl.ActivePageIndex <> 0;
 self.NV_Next_BitBtn.Enabled:= self.PageControl.ActivePageIndex <> self.PageControl.PageCount-1;
end;

end.
