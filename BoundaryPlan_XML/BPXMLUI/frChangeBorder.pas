unit frChangeBorder;
{
       19.09.2012 - ���������  ��������  �������� �����
       21,09,212  - ����������� ������ ��� ��������� �����
}
{TODO - -���������� ������ �� ����� �����  ������ �����  - �� �����  07.09.2012}
{______________________________________________________________________________

  SpecifyRelatedParcel  ->ChangeBorder

  ��������� ����� ������� (�� ����� �� �����). � ��� ����� ����������
  ����������� ������� (�����)
}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Grids,
  STD_MP, MSStringParser, {MSClipBoard,} StdCtrls, Menus,
  fmEntitySpatial_Attributs,{ReaderMidMif,}{MSSemantic,}{MSLayers,}{MapSurface,}
  {MSVectorObjects,}MSMType_,
  UnCheckTablesOrdinate, Mask, vcl.clipbrd,unRussLang;

const
  cnstColCount_StringGrid = 6; // --  ColCount StringGrid
  
  //-->> ������  �����  � �������  - 
  N_X               = 1;
  N_Y               = 2;
  N_Num_Geopint     = 3;
  N_Delta_Geopoint  = 5;
  N_Point_Pref      = 4;

  cnstValueChangeSemantic  =   false;

  cnstsetOrdinateChisla  = ['0'..'9',',',#8,'.'];
  cnstsetOrdinateCharOld = ['-'];
  cnstsetOrdinateCharNew = ['�'];
  cnst                   = ['�','�',#8]; //TODO : ��������  ��� �������

type

  TTempGrid = class(TCustomGrid); // -- ���  �������� ������ ��  �������


  TAttributeStrParse = record
      STP          : TMSStringParser;
      StartPositionGrid, //--����� ������ ��� ������ (��������� xy)
      PosSecondsTables, //--������� ������ ������� (��� ������ ������)
      CountToken   : integer //--���������� �� ����� � �������  ��� ������� ���� ������
  end;

  TAttribute_Ordinate = record
    X             : Real;
    Y             : Real;
    Ord_Nmb       : Integer;
    Num_Geopoint  : Integer;
    Delta_Geopoint: string;
    Point_Pref    : string;
  end;

  TAttributeClipTextXML = record
    IsXY          : Boolean ; // -- ������� xy -  ��  �������
    IsEditXml     : Boolean; //-- �������������  �� ����� XML
    SetStrPar     : TAttributeStrParse; //-- ������ �������
    CountToken_   : integer; //-- ��� - �� ���������� ��������� ������
    ChOrdinat     : char ; // -- "-" ���  "�"
    Arow          : integer; //-- ������� �   �������
  end;

  TFrameBorder = class(TFrame)
    pnlNewOrdinate: TPanel;
    pnlChangeBorder: TPanel;
    pnlOrdinate: TPanel;
    pnlBorders: TPanel;
    sgOldOrdinate: TStringGrid;
    sgNewOrdinate: TStringGrid;
    pnlOldOrdinate: TPanel;
    lblOldOrdinate: TLabel;
    lblNewOrdinate: TLabel;
    lbl_old_ordinate: TLabel;
    lblnew_ordinate: TLabel;
    pmOridinate: TPopupMenu;
    miInsertOrdinateNext: TMenuItem;
    miAdd_is_Buffer: TMenuItem;
    miAdd_Row_ChangeBorder: TMenuItem;
    miSetting: TMenuItem;
    edtOldOrdinate: TEdit;
    btnOldOrdinate: TButton;
    opdgDatafile: TOpenDialog;
    pnlImportOldOrdinate: TPanel;
    pnlImport_NewOrdinate: TPanel;
    btnNew_Ordinate: TButton;
    edtNew_Ordinate: TEdit;
    miImportLayers: TMenuItem;
    MaskEdit1: TMaskEdit;
    DeleteRow: TMenuItem;
    procedure sgOldOrdinateKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure sgOldOrdinateSetEditText(Sender: TObject; ACol, ARow: Integer;
      const Value: string);
    procedure sgOldOrdinateGetEditText(Sender: TObject; ACol, ARow: Integer;
      var Value: string);
    procedure miInsertOrdinateNextClick(Sender: TObject);
    procedure sgOldOrdinateDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure sgOldOrdinateSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure miAdd_is_BufferClick(Sender: TObject);
    procedure miAdd_Row_ChangeBorderClick(Sender: TObject);
    procedure sgOldOrdinateMouseActivate(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState;
      X, Y, HitTest: Integer; var MouseActivate: TMouseActivate);
    procedure sgNewOrdinateMouseActivate(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y, HitTest: Integer;
      var MouseActivate: TMouseActivate);
    procedure miSettingClick(Sender: TObject);
    procedure btnOldOrdinateClick(Sender: TObject);
    procedure FrameResize(Sender: TObject);
    procedure miImportLayersClick(Sender: TObject);
    procedure sgOldOrdinateKeyPress(Sender: TObject; var Key: Char);
    procedure DeleteRowClick(Sender: TObject);
  private
    { Private declarations }

    FXMLChangeBordes: IXMLTSpecifyRelatedParcel_ChangeBorderList;
    FXMLChangeBorer          : IXMLTChangeBorder; // -- ���  ���  ��
    FXMLOrdinate             : IXMLTOrdinate;

    ValueCell: string; // - ������� �������� ������ �������
    LRow     : integer;

    SGOrdinate    : TStringGrid;
    SgRow,SgCol   : integer;
    curentTable   : integer;  //-- ����� ��� ������� , ��� ��� ��������� ��� XML
    RecordOrdinate: TAttribute_Ordinate;

    // -->> XML
    function  IsUpdateOrInserXMLSpecifyRelatedParcel(Dest : IXMLTSpecifyRelatedParcel_ChangeBorderList): Boolean;
    procedure ClearXMLSpecifyRelatedparcel(const Dest: IXMLTSpecifyRelatedParcel_ChangeBorderList; flag: Integer);
    procedure DeleteXMLChangeBporders(flag: Integer);
    procedure SetSettingsXMl(flag: Boolean; row, i  : integer );
    procedure SetSpecifyRelatedParcel(const SP: IXMLTSpecifyRelatedParcel_ChangeBorderList);
    procedure SetCurrentSpecifyRelatedParcel();

    // -->> ��������������� ���  ������� ���������
    function  isExistRaw(index : integer; var SetAttrClip  : TAttributeClipTextXML )  : Boolean;
    function  AddtoDateXY(var SetAttrClip : TAttributeClipTextXML ) : boolean;
    procedure InsertElementNumGeopoint(str : string; arow : integer);
    procedure InsertElementDeltaGeopoint(str : string;var arow : integer);
    procedure InsertElementPointPref(str : string; var  arow : integer);
    procedure AddtoDateNumGeopoint(var  SetAttrClip : TAttributeClipTextXML);
    procedure AddtoDateDeltaGeopoint(var SetAttrClip : TAttributeClipTextXML);
    procedure AddtoDatePoint_Pref(var SetAttrClip : TAttributeClipTextXML);
    procedure SetRowsTables(stl : TStringList);


   // -->> StringGrid
    procedure SetSettingsStringGrid(const SG__OldOrdinate: TStringGrid);
    procedure AddEmptyRowStringGrid(Sender: TStringGrid; flag, i_start: Integer;
     Sender_: TStringGrid; count: Integer);
    procedure NoramlizationStringGrid;
    procedure RefreshStringGridOrdinate(Str: TStringGrid;
      Dest: IXMLTChangeBorder; ARow: Integer; isOldNew: Integer);
    procedure DeleteRowStringGrid(StringGrid: TStringGrid; ARow: Integer);
    function  IsOldOrdinate(Sender: TObject): Boolean;
    procedure ImportStltoStringGrid(const Dest: IXMLTSpecifyRelatedParcel_ChangeBorderList;
                                    var stl : TStringList; setTab : TAttributeStrParse;isEditXml  : Boolean);
    function  PositionXYNewOrdinate(str : string) : integer;

    procedure DeleteRowTabls(Sender : TObject); //�������� ������ ��  �����  ������

  public
    constructor Create(AOwner: TComponent);
    procedure InitFormsChangeBorder();
    function InsertOrdinateSpecifyRelatedparel
      (const Dest: IXMLTSpecifyRelatedParcel_ChangeBorderList; StrGrid: TStringGrid;
      stl_i: TStringList; CaptionObj: string = 'xy';
      Delimiter: string = chr(9) + ' '): Integer;
    property XMLChangeBorders: IXMLTSpecifyRelatedParcel_ChangeBorderList read FXMLChangeBordes write SetSpecifyRelatedParcel;
    { Public declarations }

  end;

implementation

uses {ParamProgram,} MsXMLAPI;
{$R *.dfm}

{ TFrameBorder }

//-- >> �������� �������   Tab   ��  '-' ---------------------------------------
function UpapdateChar(str : string) : string;
var i, len : integer;
begin
  i :=2; len  := length(str); Result := '';
  if str[1] =  chr(VK_TAB) then Result := '-'+chr(VK_TAB)  else Result := str[1];
  while i<= len do
  begin
     if ((str[i]  = char(VK_TAB)) and ((i>1)and  (str[i-1]  = char(VK_TAB))))   then Result := Result + '-'+ chr(VK_TAB)
     else      Result := Result + str[i];
     inc(i);
  end;
end;

//-->> ��������� ������ �������  ������� '-' ��� '�'----------------------------
procedure SetStringGridRowChar(StringG : TStringGrid; ch : char; arow : integer);
var i : integer;
begin
  for I := 1 to cnstColCount_StringGrid do
    StringG.Cells[i,arow] := ch;
end;

//-->>  �������  ������  ������� -  <<XY>> -------------------------------------
//-- format :
function TFrameBorder.PositionXYNewOrdinate(str : string) : integer;
var i : integer; str_ : string;
begin
 i := Pos(UpperCase('x'),UpperCase(str));
 Result := 0; //--  ��� ��������� "XY"
 if i>0 then
 begin
     str_ := Copy(str,i+1,length(str)-1);
     Result := Pos(UpperCase('x'),UpperCase(str_))-2;  //'x#9' ��� 'xy#9'
 end
end;

// -->> ---------  i ������� �������--------------------------------------------
procedure TFrameBorder.SetSpecifyRelatedParcel(const SP: IXMLTSpecifyRelatedParcel_ChangeBorderList);
begin
 Try
     FXMLChangeBordes := SP;
     if ( SP <>  nil ) then SetCurrentSpecifyRelatedParcel()
     else InitFormsChangeBorder;
 Except
   on E : Exception  do
   begin
        raise E.Create('������ ��� �������� ����������!');
        Exit;
   end;
 end;
end;

// -->> ----------------------Init attribute ordinate-----------------------------
procedure InitAttributeOrdinate(const Dest: IXMLTOrdinate);
begin
  Dest.X := null;
  Dest.Y := null;
  Dest.Num_Geopoint := null;
 //  Dest.Geopoint_Zacrep := null;
 //  Dest.Delta_Geopoint := null;
 // Dest.Point_Pref := null;
end;

// -->> ---  �������� �� ����� ����� ����� ����������� (������������ ��� ���������)---
// - true  - OldOrdinate, false - NewOrdinate
function TFrameBorder.IsOldOrdinate(Sender: TObject): Boolean;
begin
  Result := (Sender as TStringGrid).Name = sgOldOrdinate.Name;
end;

// -->> -- --��������� ��� ������������� �������  ChangeBorder------------------
//  true = Insert;  i = 0  - OldOrdinate
function TFrameBorder.IsUpdateOrInserXMLSpecifyRelatedParcel(Dest : IXMLTSpecifyRelatedParcel_ChangeBorderList) : Boolean;
begin
  Result := (Dest = nil) or (Dest.Count <=0);
end;


//-->> --  ������ �� ���� - ----------------------------------------------------
//-- TODO : comment
procedure TFrameBorder.miImportLayersClick(Sender: TObject);
var //base : TMSBaseVectorObject;
    points : TMSMPoint;
    points_count  : integer;
    j,i : integer;
    count  :integer;
    stl : TStringList;
begin
 {ith FormMain do
 begin
    if not IsAssignedMapSurface then Exit;
    if CurrentMapSurface.EditingLayerIndex = -1  then
    begin
     MessageDlg('��� �������������� ����!', mtInformation,[mbOK],-1);
     exit;
    end;

    Try
      if MessageDlg('������������� ������?',mtInformation,[mbOK,mbCancel],-1)  = mrOk then
      begin
            stl := TStringList.Create;
            count := CurrentMapSurface.EditingLayer.ObjectsSelectedCount;
            for  i := 0 to count - 1 do
            begin
                base :=  CurrentMapSurface.EditingLayer.ObjectsSelected[i];
                points_count := base.PointsCount-1;
                for j := 0 to points_count do
                begin
                    points := base.Points[j];
                    stl.Add(FloatToStrF(points.X,ffFixed,10,2)+ ' '+FloatToStrF(points.Y,ffFixed,10,2));
                end;
            end;
            InsertOrdinateSpecifyRelatedparel(FXMLSpeciifyRelatedparcel, SGOrdinate,stl);
            stl.Free;
      end; //-- yes -  import
    Except
      on E : Exception do  MessageDlg(E.ClassName+'��� ���������� ��������! ��� '+E.Message, mtInformation,[mbOK],-1); //-- ObjectSelectedCount <> 0
    end;
 end; // --  with Main  }
end;

// -->> ---�������� ������ ����� ������� ������---------------------------------
procedure TFrameBorder.miInsertOrdinateNextClick(Sender: TObject);
var
  i, j: Integer;
  ChangeBorder: IXMLTChangeBorder;
  count: Integer;
  OldOrd : IXMLTOrdinate;
  NewOrd: IXMLTOrdinate;
  col: Integer;
  //-->>
  procedure SetCellsStringgrid( str : string; i,j : integer);
  begin
      sgOldOrdinate.Cells[j, i] := str;
      sgNewOrdinate.Cells[j, i] := str;
  end;

begin
  sgOldOrdinate.RowCount := sgOldOrdinate.RowCount + 1;
  sgNewOrdinate.RowCount := sgNewOrdinate.RowCount + 1;

  count := sgOldOrdinate.RowCount;
  if sgOldOrdinate.Row = 0 then
    for j := 1 to cnstColCount_StringGrid do    SetCellsStringgrid('0',i,j)
  else
    for i := count downto sgOldOrdinate.Row + 1 do
    begin
      SetCellsStringgrid(inttostr(i),i,0);
      for j := 1 to cnstColCount_StringGrid do
      begin
        sgOldOrdinate.Cells[j, i] := sgOldOrdinate.Cells[j, i - 1];
        sgNewOrdinate.Cells[j, i] := sgNewOrdinate.Cells[j, i - 1];
      end;
    end; //for i := count downto sgOldOrdinate.Row + 1 do

  // -- c��������  �������� - � ����� :) -- ���   ������������ ���������
  ChangeBorder := FXMLChangeBordes.Add;
  OldOrd := ChangeBorder.OldOrdinate;
  NewOrd := ChangeBorder.NewOrdinate;
  count := sgOldOrdinate.Row;
  OldOrd.X := sgOldOrdinate.Cells[N_X, count];
  NewOrd.X := sgNewOrdinate.Cells[N_X, count];
  OldOrd.Y := sgOldOrdinate.Cells[N_Y, count];
  NewOrd.Y := sgNewOrdinate.Cells[N_Y, count];
  if (sgOldOrdinate.Cells[N_Num_Geopint, count] <> '') and
    (sgOldOrdinate.Cells[N_Num_Geopint, count] <> '-') then
    OldOrd.Num_Geopoint := StrtoInt(sgOldOrdinate.Cells[N_Num_Geopint, count])
  else    OldOrd.Num_Geopoint := 0;
  if (sgNewOrdinate.Cells[N_Num_Geopint, count] <> '') and
    (sgNewOrdinate.Cells[N_Num_Geopint, count] <> '�') then
    NewOrd.Num_Geopoint := StrtoInt(sgNewOrdinate.Cells[N_Num_Geopint, count])
  else NewOrd.Num_Geopoint := 0;
  OldOrd.Num_Geopoint := 0;
  if (sgOldOrdinate.Cells[N_Delta_Geopoint, count] <> '') and
    (sgOldOrdinate.Cells[N_Delta_Geopoint, count] <> '-') then
  OldOrd.Delta_Geopoint := sgOldOrdinate.Cells[N_Delta_Geopoint, count];
  if (sgNewOrdinate.Cells[N_Delta_Geopoint, count] <> '') and
    (sgNewOrdinate.Cells[N_Delta_Geopoint, count] <> '�') then  
  NewOrd.Delta_Geopoint := sgNewOrdinate.Cells[N_Delta_Geopoint, count];
  if (sgOldOrdinate.Cells[N_Point_Pref, count] <> '') and
    (sgOldOrdinate.Cells[N_Point_Pref, count] <> '-') then   
  OldOrd.Point_Pref := sgOldOrdinate.Cells[N_Point_Pref, count];
  if (sgNewOrdinate.Cells[N_Point_Pref, count] <> '') and
    (sgNewOrdinate.Cells[N_Point_Pref, count] <> '�') then   
  NewOrd.Point_Pref := sgNewOrdinate.Cells[N_Point_Pref, count];
end;

//-- >>-------------------------------------------------------------------------
//-- �������� �������� � �������   ����� ��������� xml {
{              - num_geopoint
              - delta_geopoint
             - point_prefix            }
procedure UpdatateStringGrid(Ordinate: TAttribute_Ordinate;
  Sender: TStringGrid; Row: Integer; i: Integer);
begin
 if  Row >=1 then
 begin
     if Ordinate.Num_Geopoint <> -1 then
        Sender.Cells[N_Num_Geopint, Row] := inttostr(Ordinate.Num_Geopoint + i);
     if Ordinate.Delta_Geopoint <> '' then
        Sender.Cells[N_Delta_Geopoint, Row] := Ordinate.Delta_Geopoint;
     if Ordinate.Point_Pref <> '' then
        Sender.Cells[N_Point_Pref, Row] := Ordinate.Point_Pref;
 end;
end;

//-- >>  �������� �����  ����� ���  �������  ��� ChangeBporder  -------------
//flag - old or new ordiante
procedure TFrameBorder.SetSettingsXMl(flag: Boolean; row, i  : integer );
begin
   case flag of
     true :if FXMLChangeBorer.OldOrdinate <> nil then
                  begin
                    if RecordOrdinate.Num_Geopoint <>  -1 then
                          FXMLChangeBorer.OldOrdinate.Num_Geopoint := RecordOrdinate.Num_Geopoint+i;
                    if (RecordOrdinate.Delta_Geopoint  <> '') and (RecordOrdinate.Delta_Geopoint  <> '-') then
                          FXMLChangeBorer.OldOrdinate.Delta_Geopoint := RecordOrdinate.Delta_Geopoint;
                    if (RecordOrdinate.Point_Pref  <> '') and (RecordOrdinate.Point_Pref  <> '-') then
                         FXMLChangeBorer.OldOrdinate.Point_Pref := RecordOrdinate.Point_Pref;
                    UpdatateStringGrid(RecordOrdinate, SGOrdinate, row, i);
                  end;// true
      false: if FXMLChangeBorer.NewOrdinate <> nil then
                    begin
                      if RecordOrdinate.Num_Geopoint <> -1  then
                         FXMLChangeBorer.NewOrdinate.Num_Geopoint := RecordOrdinate.Num_Geopoint+i;
                      if (RecordOrdinate.Delta_Geopoint <> '' ) and (RecordOrdinate.Delta_Geopoint <> '�') then
                         FXMLChangeBorer.NewOrdinate.Delta_Geopoint :=  RecordOrdinate.Delta_Geopoint;
                      if (RecordOrdinate.Point_Pref <> '') and (RecordOrdinate.Point_Pref <> '�')  then
                         FXMLChangeBorer.NewOrdinate.Point_Pref := RecordOrdinate.Point_Pref;
                      UpdatateStringGrid(RecordOrdinate, SGOrdinate, row, i);
                    end //false
      end; //case
end; // --> SetSettingsXML


// -->> ���������  ����� -------------------------------------------------------- 
procedure TFrameBorder.miSettingClick(Sender: TObject);
var
  i: Integer;
  countUpdate : integer; //-- ���  �����������  ������� �  �������
  currentRow : integer;

begin
 Try
  if not Assigned(FormEntitySpatial_Attributs) then   Application.CreateForm(TFormEntitySpatial_Attributs,  FormEntitySpatial_Attributs);
  FormEntitySpatial_Attributs.RadioGroup_ApplyFor.Visible := false;
  FormEntitySpatial_Attributs.ShowModal;

  if FormEntitySpatial_Attributs.ModalResult = mrOk then
     Try
        with FormEntitySpatial_Attributs do
        begin
              RecordOrdinate.Ord_Nmb := -1;
              RecordOrdinate.Num_Geopoint := -1;
             // RecordOrdinate.Delta_Geopoint := '';
             // RecordOrdinate.Point_Pref := '';
              if CheckBox_NumGeopoint.Checked then RecordOrdinate.Num_Geopoint := StrtoInt(LabeledEdit_NumGeopoint_firstindex.Text);
              if CheckBox_DeltaGeopoint.Checked then RecordOrdinate.Delta_Geopoint := LabeledEdit_DeltaGeopoint_Value.Text;
              if CheckBox_PointPref.Checked then    RecordOrdinate.Point_Pref := LabeledEdit_PointPref_value.Text
        end; //with
        countUpdate := FXMLChangeBordes.Count;
        for i := 0 to countUpdate  -1 do
        begin
              FXMLChangeBorer := FXMLChangeBordes.Items[i];
              if SGOrdinate.Name = sgOldOrdinate.Name then   SetSettingsXMl(true,i+1,i)
              else   SetSettingsXMl(false,i+1,i);
        end;// -- for

      Except
        on E : Exception do begin
                 MessageDlg(E.ClassName+ '�� ������ ����������� ����� ��� ��� ��������� ���  ' +E.Message, mtInformation,[mbOK],-1);
                 Exit;
        end;
      end; //Try

 Finally
   FormEntitySpatial_Attributs.RadioGroup_ApplyFor.Visible := true;
 End;
end;


// -->> --- ----------������� ������� � ������� ---------------------------------
procedure TFrameBorder.DeleteRowClick(Sender: TObject);
begin
 Sender := sgOldOrdinate;
 DeleteRowTabls(Sender);
end;

procedure TFrameBorder.DeleteRowStringGrid(StringGrid: TStringGrid; ARow: Integer);
begin
  if ARow  > 1 then   TTempGrid(StringGrid).DeleteRow(ARow)
  else StringGrid.Rows[ARow].Clear;    //-- ������� ������ �� �������
end;

// -->> --  �������   xmlOldOrdinate or xmlNewOrdinate- �� xmlChangeBordes---------------
procedure TFrameBorder.DeleteXMLChangeBporders;
var
  countSprelParcel , i : integer;
begin
  if FXMLChangeBordes <> nil then
  begin
    countSprelParcel :=  FXMLChangeBordes.Count;
    i := countSprelParcel-1;
    while (i< countSprelParcel )  and (i>=0)do
    begin
       FXMLChangeBorer := FXMLChangeBordes.Items[i];
       case flag of
          0 : MsXml_RemoveChildNode(self.FXMLChangeBorer,cnstXmlOldOrdinate);
          1 : MsXml_RemoveChildNode(self.FXMLChangeBorer,cnstXmlNewOrdinate);
       end;
       if FXMLChangeBorer.ChildNodes.Count =0  then FXMLChangeBordes.Delete(i);
       i:= i-1;
    end;// -- while
  end;
end;


// -->> -- -------�������� StringGrid  � ����� SpecifyRelated ���� ������ �������
// �� ���� | flag - �������������  ���������� 
procedure TFrameBorder.ClearXMLSpecifyRelatedparcel(const Dest: IXMLTSpecifyRelatedParcel_ChangeBorderList; flag: Integer);
begin
  if ((Dest<> nil) or (Dest.Count > 0)) then
    DeleteXMLChangeBporders(flag);
end;

// -->>  ������� �� ������  ��������� ------------------------------------------- 
procedure TFrameBorder.miAdd_is_BufferClick(Sender: TObject);
begin
  if SGOrdinate <> nil then
  begin
   if not Assigned(FrCheckTablesOrdinate) then
    Application.CreateForm(TFrCheckTablesOrdinate,  FrCheckTablesOrdinate);
   FrCheckTablesOrdinate.ShowModal;

   if FrCheckTablesOrdinate.ModalResult  <> mrOk  then Exit;
    if FrCheckTablesOrdinate.rbToTables.Checked then //-- �������� �� ���  �������
     InsertOrdinateSpecifyRelatedparel(FXMLChangeBordes, nil, nil)
    else
     InsertOrdinateSpecifyRelatedparel(FXMLChangeBordes, SGOrdinate, nil);
  end; // -- nil
end;

// -->> --  �������� ������  � ����� ������� �  ������ -------------------------- 
procedure TFrameBorder.miAdd_Row_ChangeBorderClick(Sender: TObject);
begin
  if FXMLChangeBordes <> nil then
  begin
    sgNewOrdinate.RowCount := sgNewOrdinate.RowCount + 1;
    sgOldOrdinate.RowCount := sgOldOrdinate.RowCount + 1;
    sgOldOrdinate.Cells[0, sgOldOrdinate.RowCount - 1] := inttostr
      (sgOldOrdinate.RowCount);
    sgNewOrdinate.Cells[0, sgNewOrdinate.RowCount - 1] := inttostr
      (sgNewOrdinate.RowCount);
    FXMLChangeBorer := FXMLChangeBordes.Add;
  end
  else
    MessageDlg('������� ����������� �����', mtInformation, [mbOK], -1);
end;

// -- >> -- ������  ������ �� mif  ���  txt  ------------------------------------ 
procedure TFrameBorder.btnOldOrdinateClick(Sender: TObject);
var
  stlOrdinate: TStringList;
  i: Integer;
  pos_i: Integer;
  stl: TStringList;
  edtOrd: TEdit;
  tip: string;
  temp�ount : integer;
  str : string;


begin
  stlOrdinate := TStringList.Create;
  stl := TStringList.Create;
  // --�����  �������
  SGOrdinate := sgNewOrdinate;
  edtOrd := edtNew_Ordinate;
  if (Sender as TButton).Name = btnOldOrdinate.Name then
  begin
    SGOrdinate := sgOldOrdinate;
    edtOrd := edtOldOrdinate;
  end;

  if self.opdgDatafile.Execute then
  begin
    edtOrd.Text := opdgDatafile.FileName;
    stlOrdinate.LoadFromFile(opdgDatafile.FileName);
    tip := Copy(opdgDatafile.FileName, length(opdgDatafile.FileName) - 2, 3);

    if tip = 'mif' then  //TODO - > ??????????
    begin // -- mif
      //-->>  ReaderMidmif-----------------------
      if stlOrdinate.count > 0 then
      begin
        pos_i := pos('REGION', stlOrdinate.Strings[0]);     i := 0;
        temp�ount  := stlOrdinate.Count;
        while (pos_i = 0) and (i < temp�ount) do
        begin
          pos_i := pos('REGION', stlOrdinate.Strings[i]);
          inc(i);
        end; // -- while
        inc(pos_i);
        i := i + 2; // ������� REGION � ���  �� ���������
        pos_i := 0;
        while (pos_i = 0) and (i < temp�ount) do
        begin
          stl.Add(stlOrdinate.Strings[i]);
          inc(i);
          pos_i := pos('PEN', stlOrdinate.Strings[i]);
        end; // - while 
        InsertOrdinateSpecifyRelatedparel(FXMLChangeBordes, SGOrdinate,stl);
      end;
    end //-- mif
    else
    begin // -- txt (��������� tab) 
      if stlOrdinate.count > 0 then
      begin
        pos_i := pos(chr(VK_tab), stlOrdinate.Strings[0]);
        i := 0;
        temp�ount := stlOrdinate.Count;
        while (pos_i = 0) and (i < temp�ount) do
        begin
          pos_i := pos(chr(VK_tab), stlOrdinate.Strings[i]);
          inc(i);
        end; // -- while
        while (pos_i <> 0) and (i < temp�ount) do
        begin
          pos_i := pos(chr(VK_tab), stlOrdinate.Strings[i]);
          stl.Add(stlOrdinate.Strings[i]);
          inc(i);
        end; // - while 
        InsertOrdinateSpecifyRelatedparel(FXMLChangeBordes, SGOrdinate, stl);
        end;
      end;// -- txt
    NoramlizationStringGrid;
  end;
  stlOrdinate.Free;
  stl.Free;
end;

// -->> ------------------------------------------------------------------------
procedure InsertRowCellInStringGrtid(Sender: TStringGrid; ARow: Integer;
  ch: char);
begin
  Sender.Cells[0, ARow] := inttostr(ARow);
  Sender.Cells[N_X, ARow] := ch;
  Sender.Cells[N_Y, ARow] := ch;
  Sender.Cells[N_Num_Geopint, ARow] := ch;
  Sender.Cells[N_Delta_Geopoint, ARow] := ch;
  Sender.Cells[N_Point_Pref, ARow] := ch;
end;

// -->> -- ��������   �  �������  �����������    '-'  ��� '�' ------------------- 
// flag = 0 -  ����� � ������� ���������� �������� 
procedure TFrameBorder.AddEmptyRowStringGrid(Sender: TStringGrid;
  flag, i_start: Integer; Sender_: TStringGrid; count: Integer);
var
  i: Integer;
  ch: char;
begin
  ch := '�';
  if flag = 1 then  ch := '-';
  if count = -1 then //-- ��������  ��������
    for i := i_start - 1 to Sender_.RowCount do
      InsertRowCellInStringGrtid(Sender, i, ch)
  else //--������� �� �������
      InsertRowCellInStringGrtid(Sender, count, ch);
end;

// -->> --������������  ������--------------------------------------------------- 
procedure TFrameBorder.NoramlizationStringGrid;
var
  index_string: Integer;
  function IsEmpty(StringGrid : TStringGrid;ch: Char): Boolean;
  begin
     Result := (StringGrid.Rows[1].CommaText = '1,'+ch+','+ch+','+ch+','+ch+','+ch);
     if not Result  then    Result := (StringGrid.Rows[1].CommaText = ','+','+','+','+',');
  end;

begin
  // ���� ������  ���� > ��� ��������.  = �������� �  �������  �����������  "� " ��� "-"
  index_string := 2;
  if sgOldOrdinate.RowCount > sgNewOrdinate.RowCount then
  begin
      if (sgNewOrdinate.RowCount >2) and  ( not IsEmpty(sgNewOrdinate,'�'))   then index_string := sgNewOrdinate.RowCount +1;
      //--����  ��� �������� ���������
      if not IsEmpty(sgNewOrdinate,'�') then index_string := sgOldOrdinate.RowCount-1;
      sgNewOrdinate.RowCount := sgOldOrdinate.RowCount;
      AddEmptyRowStringGrid(sgNewOrdinate,0,index_string,sgOldOrdinate,-1);
  end
  else
        if sgNewOrdinate.RowCount > sgOldOrdinate.RowCount then
        begin
            if (sgOldOrdinate.RowCount >2) and (not IsEmpty(sgOldOrdinate,'-'))  then  index_string := sgOldOrdinate.RowCount +1;
            if not IsEmpty(sgOldOrdinate,'-') then index_string :=sgNewOrdinate.RowCount-1;
            sgOldOrdinate.RowCount := sgNewOrdinate.RowCount;
            AddEmptyRowStringGrid(sgOldOrdinate,1,index_string,sgNewOrdinate,-1);
        end
end;

// -- >> -----------------------------------------------------------------------
procedure TFrameBorder.SetCurrentSpecifyRelatedParcel;
var
  i: Integer;
  Dest: IXMLTChangeBorder;
  ARow: Integer;

  countDestChildNodes : ShortInt;
begin
  ARow := 1;
  InitFormsChangeBorder();
  if FXMLChangeBordes <> nil then
    for i := 0 to FXMLChangeBordes.Count - 1 do
       if FXMLChangeBordes.Items[i] <> nil then
       begin  
              Dest := FXMLChangeBordes.Items[i];
              if ARow > 1 then
              begin
                sgOldOrdinate.RowCount := ARow + 1;
                sgNewOrdinate.RowCount := ARow + 1;
              end;
              if Dest.ChildNodes.Count >0  then
              if (Dest.ChildNodes.count = 2) then // -- ���  ����  Old and New
              begin

                    //(���� �� ��� ����� ����  ������ ) (<NewOrdinate/> or <OldOrdinate/>)
                    if (Dest.ChildNodes[0].AttributeNodes.Count  = 0) then
                       AddEmptyRowStringGrid(sgOldOrdinate, 1, -1, nil, ARow)
                    else
                       RefreshStringGridOrdinate(sgOldOrdinate, Dest, ARow, 0);

                    if  (Dest.ChildNodes[1].AttributeNodes.Count =0 ) then
                       AddEmptyRowStringGrid(sgNewOrdinate, 0, -1, nil, ARow)
                    else
                       RefreshStringGridOrdinate(sgNewOrdinate, Dest, ARow, 1);
              end
              else
                    if  (Dest.ChildNodes.Nodes[0].NodeName = cnstXmlOldOrdinate) then
                    begin
                      RefreshStringGridOrdinate(sgOldOrdinate, Dest, ARow, 0);
                      AddEmptyRowStringGrid(sgNewOrdinate, 0, -1, nil, ARow);
                    end
                    else
                      begin
                         RefreshStringGridOrdinate(sgNewOrdinate, Dest, ARow, 1);
                         AddEmptyRowStringGrid(sgOldOrdinate, 1, -1, nil, ARow);
                      end;
              inc(ARow);
          end;// if CHangeBorder <> nil

  sgOldOrdinate.Repaint;
  sgNewOrdinate.Repaint;
end;

// -->> --  Refresh StringGrid -------------------------------------------------- 
// old_or_new - ������������  ���  ���������� ����������
procedure TFrameBorder.RefreshStringGridOrdinate(Str: TStringGrid;
  Dest: IXMLTChangeBorder; ARow: Integer; isOldNew: Integer);
begin
  Str.Cells[0, ARow] := inttostr(ARow);
  if isOldNew = 0 then
  begin
    Str.Cells[N_X, ARow] := Dest.OldOrdinate.X;
    Str.Cells[N_Y, ARow] := Dest.OldOrdinate.Y;
    if dest.ChildNodes.Nodes[cnstXmlOldOrdinate].AttributeNodes.IndexOf('Num_Geopoint') <> -1  then
      Str.Cells[N_Num_Geopint, ARow] := inttostr(Dest.OldOrdinate.Num_Geopoint)
    else Str.Cells[N_Num_Geopint,ARow] := '-';
    if dest.ChildNodes.Nodes[cnstXmlOldOrdinate].AttributeNodes.IndexOf('Delta_Geopoint') <> -1 then    
    begin
      if Dest.OldOrdinate.Delta_Geopoint = '' then
      begin
          MsXml_RemoveAttribute(Dest.OldOrdinate,'Delta_Geopoint');
          Str.Cells[N_Delta_Geopoint, ARow] := '-'
      end
      else  Str.Cells[N_Delta_Geopoint, ARow] := Dest.OldOrdinate.Delta_Geopoint
    end  
    else Str.Cells[N_Delta_Geopoint,ARow] := '-';
    if dest.ChildNodes.Nodes[cnstXmlOldOrdinate].AttributeNodes.IndexOf('Point_Pref') <> -1 then   
    begin   
      if Dest.OldOrdinate.Point_Pref = '' then
      begin
          MsXml_RemoveAttribute(Dest.OldOrdinate,'Point_Pref');
          Str.Cells[N_Point_Pref, ARow] := '-'
      end
      else
      Str.Cells[N_Point_Pref, ARow] := Dest.OldOrdinate.Point_Pref
    end 
    else Str.Cells[N_Point_Pref, ARow] := '-';
  end //-- old_or_new = 0
  else
  begin
    Str.Cells[N_X, ARow] := Dest.NewOrdinate.X;
    Str.Cells[N_Y, ARow] := Dest.NewOrdinate.Y;
    if ((Dest.ChildNodes.Nodes[cnstXmlNewOrdinate].AttributeNodes.Nodes['Num_Geopoint'].NodeValue <> '') and
       (Dest.ChildNodes.Nodes[cnstXmlNewOrdinate].AttributeNodes.Nodes['Num_Geopoint'].NodeValue <> null))then
      Str.Cells[N_Num_Geopint, ARow] := inttostr(Dest.NewOrdinate.Num_Geopoint)
    else
      Str.Cells[N_Num_Geopint, ARow] := '�';
    if dest.ChildNodes.Nodes[cnstXmlNewOrdinate].AttributeNodes.IndexOf('Delta_Geopoint') <> -1 then    
    begin
      if Dest.NewOrdinate.Delta_Geopoint = '' then
      begin
          MsXml_RemoveAttribute(Dest.NewOrdinate,'Delta_Geopoint');
          Str.Cells[N_Delta_Geopoint, ARow] := '�'
      end
      else
      Str.Cells[N_Delta_Geopoint, ARow] := Dest.NewOrdinate.Delta_Geopoint
    end  
    else Str.Cells[N_Delta_Geopoint, ARow] := '�';  
    if dest.ChildNodes.Nodes[cnstXmlNewOrdinate].AttributeNodes.IndexOf('Point_Pref') <> -1 then      
    begin
      if Dest.NewOrdinate.Point_Pref = '' then
      begin
          MsXml_RemoveAttribute(Dest.NewOrdinate,'Point_Pref');
          Str.Cells[N_Point_Pref, ARow] := '�'
      end
      else
      Str.Cells[N_Point_Pref, ARow] := Dest.NewOrdinate.Point_Pref
    end 
    else Str.Cells[N_Point_Pref, ARow] := '�';
  end;
end;


//-->> -------------------------------------------------------------------------
//-- �������  - �������  ������� �� ������ ������
{-- isInsertxy         - ���� xy ���� ��� ����������
 -- count_token_       - ���������  ���������� �������  � ������
 -- index              - ������ ������ (Num_Geopouint, Point_Pref , Delta_Geopoint
 -- setTab.count_token - ����� ��� ������� ��� �������   )
 }
function TFrameBorder.isExistRaw;
begin
  Result :=   SetAttrClip.IsXY and
              (SetAttrClip.CountToken_ < SetAttrClip.SetStrPar.STP.GetCountToken )  and
             (SetAttrClip.SetStrPar.STP.GetCountToken >= SetAttrClip.SetStrPar.PosSecondsTables +index);
end;

//-->> -----------------------------------------1--------------------------------
function TFrameBorder.AddtoDateXY;
var countToken   : Integer;
    chX          : string;
    chY          : string;
begin
 Result := false;
 SGOrdinate.Cells[N_X, SetAttrClip.Arow + 1] := SetAttrClip.ChOrdinat;
 SGOrdinate.Cells[N_Y, SetAttrClip.Arow + 1] := SetAttrClip.ChOrdinat;

 countToken := SetAttrClip.SetStrPar.STP.GetCountToken;
 chX := SetAttrClip.SetStrPar.STP.GetToken(SetAttrClip.SetStrPar.PosSecondsTables);
 chY := SetAttrClip.SetStrPar.STP.GetToken(SetAttrClip.SetStrPar.PosSecondsTables +1);

 if  (SetAttrClip.CountToken_ < countToken) and
     ((countToken >= SetAttrClip.SetStrPar.PosSecondsTables + 2) or
     (countToken >= SetAttrClip.SetStrPar.PosSecondsTables +3)) and
     (chX <>'-') and
     (chX <>'�') then // XY
  begin
    FXMLOrdinate.X := chX; //0
    FXMLOrdinate.Y := chY;
    SGOrdinate.Cells[N_X, SetAttrClip.Arow + 1] := chX; //0
    SGOrdinate.Cells[N_Y, SetAttrClip.Arow + 1] := chY;
    Result := true;
  end;
  inc(SetAttrClip.CountToken_);
end;  //--AddtoDateXY

//-->> -------------------------------------------------------------------------
procedure TFrameBorder.InsertElementNumGeopoint;
begin
  FXMLOrdinate.Num_Geopoint :=  strtoint(str);
  try
     SGOrdinate.Cells[N_Num_Geopint, arow + 1] :=  inttostr(FXMlOrdinate.Num_Geopoint);
  except on E: Exception do
    //; ������  xy;
  end;
   
end; //--InsertElementNumGeopoint(str : string);

//-->> -------------------------------------------------------------------------
procedure TFrameBorder.InsertElementDeltaGeopoint;
begin
  SGOrdinate.Cells[N_Delta_Geopoint, arow+ 1] := '';
  if (str <> '') and  (str <> '-') and (str <> '�') then
  begin
      FXMLOrdinate.Delta_Geopoint :=  str;
      SGOrdinate.Cells[N_Delta_Geopoint, arow+ 1] :=  FXMLOrdinate.Delta_Geopoint;
  end
 // else MsXml_RemoveAttribute(FXMLOrdinate,'Delta_Geopoint');    
end; //--InsertElementNumGeopoint(str : string);

//-->> -------------------------------------------------------------------------
procedure  TFrameBorder.InsertElementPointPref;
begin
  SGOrdinate.Cells[N_Point_Pref, arow + 1] := '';
  if (str <> '') and  (str <> '-') then
  begin  
    FXMLOrdinate.Point_Pref :=  str;
    SGOrdinate.Cells[N_Point_Pref, arow + 1] :=  FXMLOrdinate.Point_Pref;
  end
end; //--InsertElementNumGeopoint(str : string);

//-->> -------------------------------------------------------------------------
procedure TFrameBorder.AddtoDateNumGeopoint;
begin
 Try
  if isExistRaw(N_Num_Geopint,SetAttrClip) then // >5
  begin
      Inc(SetAttrClip.CountToken_);
      if (SetAttrClip.SetStrPar.STP.GetToken(SetAttrClip.SetStrPar.PosSecondsTables +N_Num_Geopint - 1) <> '-') and
         (SetAttrClip.SetStrPar.STP.GetToken(SetAttrClip.SetStrPar.PosSecondsTables +N_Num_Geopint - 1) <> '') then
          InsertElementNumGeopoint(SetAttrClip.SetStrPar.STP.GetToken(SetAttrClip.SetStrPar.PosSecondsTables +N_Num_Geopint - 1),
                                   SetAttrClip.Arow)
      else
      begin //-- ���  ��������� ���� ������ �����
         if (SetAttrClip.Arow>=1)  then
          InsertElementNumGeopoint(inttostr( strtoint(SGOrdinate.Cells[N_Num_Geopint, SetAttrClip.Arow])+1),
                                             SetAttrClip.Arow)
         else         SGOrdinate.Cells[N_Num_Geopint, SetAttrClip.Arow + 1] := SetAttrClip.ChOrdinat;
      end; //-- begin  
  end // = >5
  else
  begin
    if SetAttrClip.IsXY and (SetAttrClip.isEditXml) and (SetAttrClip.Arow>=1)  and
      (SGOrdinate.Cells[N_Num_Geopint, SetAttrClip.Arow] <>'') and
      (SGOrdinate.Cells[N_Num_Geopint, SetAttrClip.Arow] <> SetAttrClip.ChOrdinat)  then
      InsertElementNumGeopoint(inttostr(strtoint(SGOrdinate.Cells[N_Num_Geopint, SetAttrClip.Arow])+1), SetAttrClip.Arow)
    else     SGOrdinate.Cells[N_Num_Geopint, SetAttrClip.Arow + 1] := SetAttrClip.ChOrdinat;
  end; //begin 
 Except on E: Exception do   
    //--- ��� ��������������  �������  �������� )  Num_Geopoint    
 End;
end; //AddtoDateNumGeopoint();

//-->> -------------------------------------------------------------------------
procedure TFrameBorder.AddtoDateDeltaGeopoint;
begin
 if  isExistRaw(N_Delta_Geopoint, SetAttrClip) then
  begin
    InsertElementDeltaGeopoint(SetAttrClip.SetStrPar.STP.GetToken(SetAttrClip.SetStrPar.PosSecondsTables+N_Delta_Geopoint - 1),
                               SetAttrClip.Arow);
    inc(SetAttrClip.CountToken_);
  end
  else
    if SetAttrClip.IsXY then InsertElementDeltaGeopoint('', SetAttrClip.Arow) 
    else     SGOrdinate.Cells[N_Delta_Geopoint, SetAttrClip.Arow + 1] := SetAttrClip.ChOrdinat;
end; //AddtoDateDeltaGeopoint();

//-->> -------------------------------------------------------------------------
procedure TFrameBorder.AddtoDatePoint_Pref;
begin
  if isExistRaw( N_Point_Pref, SetAttrClip) then
  begin
    InsertElementPointPref(SetAttrClip.SetStrPar.STP.GetToken(SetAttrClip.SetStrPar.PosSecondsTables+N_Point_Pref - 1), SetAttrClip.Arow);
    inc(SetAttrClip.CountToken_);
  end
  else
    if SetAttrClip.IsXY  then InsertElementPointPref('', SetAttrClip.Arow)
    else  SGOrdinate.Cells[N_Point_Pref, SetAttrClip.Arow + 1] := SetAttrClip.ChOrdinat;
end; //AddtoDatePoint_Pref();



//-->> index_start   :  �����  ������ (����  ��������� ����� ) - xy)
//--   index         :  ������� ������  � ������  - ���� ��������� �� ��� �������
//--   count_token   :  ����������  � ������ ���������� � �������
//--   isEditXml     :  ����������� ��� ��������� ����� ���� �  XML
procedure TFrameBorder.ImportStltoStringGrid;
var  i : integer;
     temp_str : string;
     xmlChangeBorder: IXMLTChangeBorder;
     setAttr : TAttributeClipTextXML;

     //-- TODO :
     tempOldOrd : string;
     tempNewOrd : string;
     //-- TODO :

begin
   //-- ������  ������  ������
   setAttr.Arow := 0;
   setAttr.SetStrPar := setTab;
   setAttr.IsEditXml := isEditXml;

  for i :=  setAttr.SetStrPar.StartPositionGrid to stl.Count - 1 do
      begin
        setAttr.IsXY := false;
        setAttr.CountToken_ := 0;
        temp_str := UpapdateChar(stl[i]); //--  update '#9' -> 0
        setAttr.SetStrPar.STP.Parse(temp_str);
        if  setAttr.SetStrPar.STP.GetCountToken  = 1 then    CONTINUE;
        // -- ��������� ��� �������� XML
        if (((not setAttr.isEditXml) and (setAttr.Arow >= Dest.Count)) or (setAttr.isEditXml))  then
                xmlChangeBorder := Dest.Add
        else    xmlChangeBorder := Dest.Items[setAttr.Arow];
        // ���������� ��� ���������
        if IsOldOrdinate(SGOrdinate) then      begin
            FXMlOrdinate := xmlChangeBorder.OldOrdinate; setAttr.ChOrdinat := '-';
        end
        else     begin
             FXMLOrdinate := xmlChangeBorder.NewOrdinate; setAttr.ChOrdinat := '�';
        end;
        // �������  ��������
        SGOrdinate.Cells[0,  setAttr.Arow + 1] := inttostr(setAttr.Arow + 1); //-- �

        setAttr.IsXY :=  AddtoDateXY(setAttr) ;
        //if  setAttr.IsXY then      FXMLOrdinate.Ord_Nmb := setAttr.Arow + 1;

        AddtoDateNumGeopoint(setAttr);        // ����� ������� �����
        AddtoDateDeltaGeopoint(setAttr);         //--�����������
        AddtoDatePoint_Pref(setAttr);         // ������� ������ �����
        if not setAttr.IsXY  then //-- �� ��� �� �����
             if  IsOldOrdinate(SGOrdinate) then  MsXml_RemoveChildNode(xmlChangeBorder,cnstXmlOldOrdinate)
             else MsXml_RemoveChildNode(xmlChangeBorder,cnstXmlNewOrdinate);
         //inc(Result);
        tempOldOrd := xmlChangeBorder.OldOrdinate.XML;
        tempNewOrd := xmlChangeBorder.NewOrdinate.XML;
        inc (setAttr.Arow);
      end;   // -- for
end;   //-- function

//-->> --------------------------------------------------------------------------
procedure  TFrameBorder.SetRowsTables(stl : TStringList);
begin
  if stl.count > 0 then
  begin
      SGOrdinate.RowCount := stl.count + 1;
      case curentTable  of
         0 : sgOldOrdinate.RowCount := stl.Count +1;
         1 : sgNewOrdinate.RowCount := stl.Count +1;
         else begin
                 MessageDlg('��� �������� ������� ���������!', mtInformation, [mbOK],-1);
                 Exit;
              end
      end;//case
  end;
end; //SettingsTables();

// -- >> -- ������� � ������  ������ -- ------------------------------------------
// -- tips  true  ��  ������   | false - ������
function TFrameBorder.InsertOrdinateSpecifyRelatedparel
                      (const Dest: IXMLTSpecifyRelatedParcel_ChangeBorderList;
                       StrGrid: TStringGrid; stl_i: TStringList;
                       CaptionObj: string = 'xy'; Delimiter: string = chr(9) + ' '): Integer;
var
  i: Integer;
  stl: TStringList;
  isEditXml: Boolean; // -- ��������� ��� ����������� � XML
  isDoTables : Boolean; // - ��� ������� ��������� ������������ ��� �������
  posToTables : integer; // - ������� ������ ������� � ������ ������  - "XY"

  setTab : TAttributeStrParse;
  tempIndexStart :  integer;

  //=:==:==:==:==:==:==:==:==:==:==:==:==:==:=
  procedure CopyListString(stl_i: TStringList);
  var
    i: Integer;
  begin
    for i := 0 to stl_i.count - 1 do
      stl.Add(stl_i.Strings[i]);
  end;


  function GetPositionSecondX(index : integer) : integer;
  begin
      Result := index;
      while (setTab.STP.GetToken(Result)= '-') or (setTab.STP.GetToken(Result)='�')  do
          inc(Result);

  end;

 //=:==:==:==:==:==:==:==:==:==:==:==:==:==:==

begin  //-- >
  stl := TStringList.Create;
  //--����� �������
  isDoTables := false;
  curentTable := 0;
  if StrGrid = nil then begin
    isDoTables := true;
    SGOrdinate := sgOldOrdinate;
  end else
   if SGOrdinate.Name = sgNewOrdinate.Name then curentTable :=1;


  if Dest <> nil then begin
   ClearXMLSpecifyRelatedparcel(Dest, curentTable); // -- ����
   setTab.STP:= TMSStringParser.Create(Delimiter);
   if stl_i = nil then begin
   //ClipBrd_ReadStrings(stl)
   Clipboard.Open;
   TRY
   stl.Text:= Clipboard.AsText;
   FINALLY
   Clipboard.Close;
   END;
   end else  CopyListString(stl_i);
          Result := 0;
          isEditXml := IsUpdateOrInserXMLSpecifyRelatedParcel(Dest); // ������� ��� ����������� (trut | false)
          SetRowsTables(stl);
          if ((stl.count > 0) and (stl <> nil)) then
          begin
            Try
                //-- �������  "����� "
                if PositionXYNewOrdinate(stl.Strings[0]) > 0  then    setTab.StartPositionGrid := 1;
                tempIndexStart := setTab.StartPositionGrid; //--��� ������  �������
                //-- �������������  ���������� ��  ��������
                posToTables :=  setTab.STP.Parse(stl.Strings[1]);
                setTab.StartPositionGrid := 0;
                if posToTables  > cnstColCount_StringGrid -1  then  posToTables := posToTables div 2;
                setTab.CountToken := setTab.STP.GetCountToken;
                setTab.PosSecondsTables :=  0;
                // --������� ������������ �  ���  �������
                if isDoTables then
                begin
                  ImportStltoStringGrid(Dest, stl, setTab, isEditXml);
                  SGOrdinate := sgNewOrdinate;
                  SGOrdinate.RowCount := stl.Count+1;
                  setTab.PosSecondsTables := posToTables;
                  setTab.CountToken := posToTables -1 ;

                  if ((posToTables mod  2)   = 0 )  then // �������
                  begin
                      setTab.PosSecondsTables := GetPositionSecondX(posToTables+1);
                      setTab.CountToken :=  posToTables;
                  end;
                  isEditXml := false;
                  ImportStltoStringGrid(Dest,stl,setTab,isEditXml);
                end
                else
                begin
                   SGOrdinate.RowCount := stl.Count+1;
                   setTab.PosSecondsTables := 0;
                   setTab.CountToken := setTab.STP.GetCountToken;
                   ImportStltoStringGrid(Dest,stl,setTab,isEditXml);
                end;
            Except
                MessageDlg('������ ���  �������� ������  ��  �������!�������� ������������!', mtInformation,[mbOK],-1);
            End;

            if (Dest.Count >0) then
            begin
               if sgOldOrdinate.RowCount > Dest.Count  then  sgOldOrdinate.RowCount := Dest.Count +1;
               if sgNewOrdinate.RowCount > Dest.Count  then  sgNewOrdinate.RowCount := Dest.Count +1;
            end;
            NoramlizationStringGrid; // �  ���  -
            setTab.STP.Free;
            stl.Free;
          end
   end //-- Dest <> nil
   else MessageDlg('���  ������������ ������!', mtInformation,[mbOK],-1);
   Result := 0;
   sgOldOrdinate.Refresh;
   sgNewOrdinate.Refresh;
end;

// -- >> -----------------------------------------------------------------------
procedure TFrameBorder.InitFormsChangeBorder;
begin
  sgOldOrdinate.RowCount := 2;
  sgOldOrdinate.Rows[1].Clear;
  sgOldOrdinate.Rows[1].Clear;
  sgNewOrdinate.RowCount := 2;
  sgNewOrdinate.Rows[1].Clear;
  sgNewOrdinate.Rows[1].Clear;
  edtOldOrdinate.Text := '';
  edtNew_Ordinate.Text := '';

end;

// -->>--------------------------------------------------------------------------
procedure TFrameBorder.sgNewOrdinateMouseActivate(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y, HitTest: Integer;
  var MouseActivate: TMouseActivate);
begin
  SGOrdinate := (Sender as TStringGrid);
  curentTable := 1;
end;

// -->> --  �������� ������ ----------------------------------------------------
procedure TFrameBorder.sgOldOrdinateDrawCell(Sender: TObject;
  ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  Format: Word;
  C: array[0..255] of Char;
  //-->>
  procedure SetGridDrawCellCurentColor(StringGrid: TStringGrid;
    aBrush, aFont: TColor);
  begin
    StringGrid.canvas.Brush.color := aBrush;
    StringGrid.canvas.Font.color := aFont;
    StringGrid.canvas.TextRect(Rect, Rect.Left + 2, Rect.Top + 2,
    StringGrid.Cells[ACol, ARow]);
  end;

begin
  if gdFixed in State then   exit;  //-- �������� ����� ������
      if ARow = TStringGrid(Sender).Row then
      begin
        SetGridDrawCellCurentColor(sgNewOrdinate, $00F9E1B9, clWhite);
        SetGridDrawCellCurentColor(sgOldOrdinate, $00F9E1B9, clWhite);
      end
      else
      begin
        SetGridDrawCellCurentColor(sgNewOrdinate, clWhite, clBlack);
        SetGridDrawCellCurentColor(sgOldOrdinate, clWhite, clBlack);
      end;

end;

// -- >> ----------------------------------------------------------------------
procedure TFrameBorder.sgOldOrdinateGetEditText(Sender: TObject;
  ACol, ARow: Integer; var Value: string);
begin
  if (ARow >= 1) and (ACol >= 1) then        ValueCell := Value;
end;

procedure TFrameBorder.DeleteRowTabls(Sender : TObject);
var del_i_row_old , del_i_row_new  : integer;
begin
   Try
        if (Sender.UnitName = 'Menus') then Sender := sgOldOrdinate;

        LRow := (Sender as TStringGrid).Row - 1;
        del_i_row_old := LRow;
        del_i_row_new := LRow;
        if (MessageDlg('������� ������ ��� �' + sgOldOrdinate.Cells[0,LRow+1],
            mtConfirmation, [mbOK, mbCancel], -1) = mrOk) and (LRow >= 0) then
        begin
          DeleteRowStringGrid(sgOldOrdinate, del_i_row_old + 1);
          DeleteRowStringGrid(sgNewOrdinate, del_i_row_new + 1);
          if FXMLChangeBordes.Items[del_i_row_old].ChildNodes.FindNode(cnstXmlOldOrdinate) <>nil then
             MsXml_RemoveChildNode(self.FXMLChangeBordes.Items[del_i_row_old],cnstXmlOldOrdinate);
          if FXMLChangeBordes.Items[del_i_row_new].ChildNodes.FindNode(cnstXmlNewOrdinate) <>nil then
          MsXml_RemoveChildNode(self.FXMLChangeBordes.Items[del_i_row_new],cnstXmlNewOrdinate);
          FXMLChangeBordes.Delete(del_i_row_old);
        end; //-- yes -delete
    Except
      //-- ������ �������   ������������� ������
    end;
end;

// -- >> -----------------------------------------------------------------------
procedure TFrameBorder.sgOldOrdinateKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  del_i_row_old: Integer;
  del_i_row_new: Integer;
begin
  case Key of
    VK_INSERT:
      begin //-- ������� ����� ������ � ����� �������
         miAdd_Row_ChangeBorderClick(Sender);
         if MessageDlg('��������  ������ �� ������ ������!',mtInformation,[mbOK,mbCancel],-1)  = mrOk then
           InsertOrdinateSpecifyRelatedparel(FXMLChangeBordes,(Sender as TStringGrid), nil);
      end;
    VK_HOME:
      begin // ���� ������� 
        // -- TODO ����
         //IndexChangeBorder := IndexChangeBorder + 1;
         //(Sender as TStringGrid).RowCount := (Sender as TStringGrid).RowCount + 1;
        // FXMLChangeBorer := FXMLSpeciifyRelatedparcel.ChangeBorder.Insert(IndexChangeBorder);
      end;
    VK_DELETE:
      begin // �������  ���������� ������
         DeleteRowTabls(Sender);
      end;
  end;
end;

// -->>-------------------------------------------------------------------------
procedure TFrameBorder.sgOldOrdinateMouseActivate(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y, HitTest: Integer;
  var MouseActivate: TMouseActivate);
begin
  SGOrdinate := (Sender as TStringGrid);
  curentTable := 0;
end;

// -->> --  --------------------------------------------------------------------
procedure TFrameBorder.sgOldOrdinateSelectCell(Sender: TObject;
  ACol, ARow: Integer; var CanSelect: Boolean);
var
  SaveEvent: TSelectCellEvent;

  //-->>
  procedure SetGridSelectCell(StringGrid: TStringGrid);
  begin
    SaveEvent := StringGrid.OnSelectCell;
    StringGrid.OnSelectCell := Nil;
    StringGrid.Row := ARow;
    CanSelect := false;
    StringGrid.Refresh;
    StringGrid.OnSelectCell := SaveEvent;
  end;

begin
 Try
  LRow := ARow;
  SgCol := ACol;
  SgRow := ARow;
  if ARow <> TStringGrid(Sender).Row then
  begin
    SetGridSelectCell(sgOldOrdinate);
    SetGridSelectCell(sgNewOrdinate);
  end;
 Except
   on E : Exception do  MessageDlg(E.ClassName+'������ ���������� StringGrid!' + E.Message,mtInformation,[mbok],-1);
 end;

end;

// -- >> --  Edit Data ----------------------------------------------------------
procedure TFrameBorder.sgOldOrdinateSetEditText(Sender: TObject;
  ACol, ARow: Integer; const Value: string);
var
  Ord: IXMLTOrdinate;
  ChangeBorder: IXMLTChangeBorder;
  i  : integer;
begin
  SgRow  := ARow;
  SgCol  := ACol;
  // �������� ������������  - ������ ������ StringGrida
  if ((ARow >= 1) and (ACol >= 1)) then
  begin
    if (Value <> ValueCell) then
    begin
       Try
        //todo :  bilo +1
        if ARow >= FXMLChangeBordes.Count+1 then  ChangeBorder := FXMLChangeBordes.Add
        else      //��������  �������
          ChangeBorder := FXMLChangeBordes.Items[ARow - 1]
       Except
             on E : Exception do
             begin
                    MessageDlg('�� ��������  ����������� �����!', mtInformation, [mbOK],-1);
                    Exit;
             end;
       end;
      if (Sender as TStringGrid).Name = sgOldOrdinate.Name then Ord := ChangeBorder.OldOrdinate
      else   Ord := ChangeBorder.NewOrdinate;


      if (Value <> '-') and (Value <> '�') and (Value <> '') then
          case ACol of
            N_X: Ord.X := Value;
            N_Y: Ord.Y := Value;
            N_Num_Geopint:
            Try
              Ord.Num_Geopoint := StrtoInt(Value);
            Except
              Ord.Num_Geopoint := 0;
            End;
            N_Delta_Geopoint: Ord.Delta_Geopoint := Value;
            N_Point_Pref:Ord.Point_Pref := Value;
           end // case Acol
    end;
  end; // ((ARow >= 1) and (ACol >= 1)
end;

// -->> -- ---------------------------------------------------------------------
procedure TFrameBorder.SetSettingsStringGrid(const SG__OldOrdinate: TStringGrid);
var rec : TRect;
begin
  SG__OldOrdinate.GradientStartColor := $00F3E2D5;
  SG__OldOrdinate.GradientEndColor   := $00E3BB9B;
  SG__OldOrdinate.FixedColor         := $00F3E2D5;
  SG__OldOrdinate.DrawingStyle       := gdsClassic;
  SG__OldOrdinate.DefaultRowHeight   := 17;
  SG__OldOrdinate.Options := SG__OldOrdinate.Options + [goColSizing, goEditing];
  SG__OldOrdinate.RowCount           := 2;
  SG__OldOrdinate.ColCount := cnstColCount_StringGrid;
  SG__OldOrdinate.Cells[0, 0]        := '';
  SG__OldOrdinate.Cells[N_X, 0]      := 'X';
  SG__OldOrdinate.Cells[N_Y, 0]      := 'Y';
  SG__OldOrdinate.Cells[N_Num_Geopint, 0] := '� ������� �����';
  SG__OldOrdinate.Cells[N_Delta_Geopoint, 0] := '�����������';
  SG__OldOrdinate.Cells[N_Point_Pref, 0] := '������� � �����';

  //-- ������  �������
  SG__OldOrdinate.ColWidths[0]   := 30;
  SG__OldOrdinate.ColWidths[N_X] := 90;
  SG__OldOrdinate.ColWidths[N_Y] := 90;
  SG__OldOrdinate.ColWidths[N_Num_Geopint] := 100;
  SG__OldOrdinate.ColWidths[N_Delta_Geopoint] := 100;
  SG__OldOrdinate.ColWidths[N_Point_Pref] := 100;

  pnlNewOrdinate.Height := Round(self.Height/2 - pnlImportOldOrdinate.Height  - lblOldOrdinate.Height)+30;
  pnlOldOrdinate.Height := Round(self.Height/2 - pnlImportOldOrdinate.Height  - lblOldOrdinate.Height)+30;
  SG__OldOrdinate.Height := round (SG__OldOrdinate.VisibleRowCount*SG__OldOrdinate.RowHeights[1]);
  //-- ������
 //421 -
end;

// -- >> Table-----------------------------------------------------------------
constructor TFrameBorder.Create(AOwner: TComponent);
begin
  inherited;
  FXMLChangeBorer := nil;
  SGOrdinate := nil;
  SetSettingsStringGrid(sgOldOrdinate);
  SetSettingsStringGrid(sgNewOrdinate);
  ValueCell := '';
end;

procedure TFrameBorder.FrameResize(Sender: TObject);
begin
 pnlNewOrdinate.Height := Round(self.Height/2 - pnlImportOldOrdinate.Height  - lblOldOrdinate.Height)+30;
 pnlOldOrdinate.Height := Round(self.Height/2 - pnlImportOldOrdinate.Height  - lblOldOrdinate.Height)+30;
 sgOldOrdinate.Height := round (sgOldOrdinate.VisibleRowCount*sgOldOrdinate.RowHeights[1]);
 sgNewOrdinate.Height := round (sgOldOrdinate.VisibleRowCount*sgOldOrdinate.RowHeights[1]);
end;

//-->>--------------------------------------------------------------------------
// -- � ������� OldOrdinate  � ���������   '-'
// -- � ������� NewOrdinate  � ����������  '�'
procedure TFrameBorder.sgOldOrdinateKeyPress(Sender: TObject; var Key: Char);
begin
  if (SgCol >=1) and (SgRow >=1) then
  case SgCol  of
     N_X, N_Y :
          if (not (key in cnstsetOrdinateChisla)) and
            (((TStringGrid(Sender).Name =sgOldOrdinate.Name) and  (not (key  in cnstsetOrdinateCharOld))) or
            ((TStringGrid(Sender).Name =sgNewOrdinate.Name)and (key <> '�'))) then  key := #0
          //-- ��������� ������ ��������
          else if (Key = '-') or (Key = '�')  then
             SetStringGridRowChar(TStringGrid(Sender),key,SgRow);
     N_Delta_Geopoint : if not (key   in cnstsetOrdinateChisla) then  key := #0 ;
     N_Num_Geopint : if not(Key in cnstsetOrdinateChisla) then  Key := #0;
     N_Point_Pref : if (key <> '�') and   (key  <> '�') and (key <> #8) then  key  := #0; //TODO - ���� �  ����������  ��� ��������
     else  key := #0;
  end;
end;

end.
