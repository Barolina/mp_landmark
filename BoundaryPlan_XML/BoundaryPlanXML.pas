unit BoundaryPlanXML;

{$define dontuseFWZipWriter}

interface

uses
  Classes, dialogs, windows,

  fmBPXML_FormParcels,
  fmBPXML_GeneralInformation,
  Fm_SpecifyParcels,
  xmlintf, XMLDoc, STD_MP,
  BpXML_different, MSStringParser,
  TypInfo,
  SysUtils, ShellAPI, Vcl.Controls,
  VCL.forms,
  System.IOUtils,

  unRussLang,IniFiles,System.Variants,

  LMProject,FrPreloader,
  fmNewSubParcels;
 
type
  TBpKind = (bpkRegistration,  //���������� 
             bpkChanges,       //���������
             bpNewSubParcels); //���������� �����

  
  TMSBoundaryPlanXML = class
  private
    FXMLDocument : IXMLDocument;
    FSTDMP       : IXMLSTD_MP;
    FCodeType    : string;
    FXSLT_Name   : string;
    FPathFile    : string;
    FKind        : TBpKind;

    FDialog_GenInf                : TFormBPXML_GeneralInformation;
    FDialogPackage_FormParcels    : TFormBPXML_FormParcels;
    FDialogPackage_SpecifyParcels : TFormSpecifyParcels;
    FDialogPackage_NewSubParcecel : TFormNewSubParcels;

    FOnChangeActivPage : TNotifyEvent;

    { ����� "��������" ��� �����  �� ������� }
    function FindEngtoRussWord(xmlNode : IXMLNode;var str : string) : integer;
    function GetReadGlossarii(path : string) : IXMLNode;
    function UpdateEnglWord(xmlNode : IXMLNode; str : string) : string;
    function GetFileName(Path : string) : string;
  protected
    { �������������� ������ ������ }
    procedure RegenerationErrorListWord(var list : TStringList);

    function GetDGenInf: TFormBPXML_GeneralInformation;
    function GetDFormParcels: TFormBPXML_FormParcels;
    function GetDSpecifyParcels: TFormSpecifyParcels;
    function GetDNewSubParcels : TFormNewSubParcels;

    procedure SetXMLFormParcels;
    procedure SetXMlDocuments;
    procedure SetXMLSpecifyParcels;
    procedure SetXMLNewSubParcels;
  public
    constructor Create;
    destructor Destroy; override;

    procedure SetActivPageParcels(activPage : TTypePackage);
    
    { isTypePackage  - ��� ��������   (-�����������) }
    procedure CreateNew(Kind: TBpKind; Method: integer; isTypePackage: TTypeMP);
    procedure Load(Path: string);
    function  SaveAs(FileName: string): boolean;
    function  SaveCopyAs(FileName: string): boolean;
    function  Save: boolean;
    function  ExportTo(PathDir: string; Guid: string): string;
    function  ValidData(LogOut: TStrings): boolean;
    //
    // Export to Word
    //
    function ExportXmltoWord(pathXml : string) : string;
    procedure DoClose;
    function  IsAssigned: boolean;

    procedure ExecuteDialog_GenInf(aParent: TWinControl = nil);
    procedure ExecuteDialog_Package(aParent: TWinControl = nil);
    procedure CloseDialog_GenInf;
    procedure CloseDialog_Package;

    procedure CopyFileXMl(SavePath : string);
    procedure CopyPackage(toPath, savePath: string;const isTransformXml : integer = 0);

    property CodeType: string read FCodeType;
    property PathFile: string read FPathFile;
    property OnChangeActivPage : TNotifyEvent read FOnChangeActivPage write FOnChangeActivPage;
  end;

implementation

uses MsXMLAPI, UnLandMark, Winapi.SHFolder;

resourcestring
    rsGlName = 'name';
    rsGlDocumentation = 'documentation';
    rsGlType = 'isList';

function _GetSimplifiedGUID: string;
var
  _id: TGUID;
begin
  CreateGUID(_id);
  Result := GUIDToString(_id);
  Delete(Result, 1, 1);
  Delete(Result, length(Result), 1);
end;

{ TMXBoundaryPlanXML }

procedure TMSBoundaryPlanXML.CreateNew;
var
  _node: IXMLNode;
  path : string;
begin
  self.FXMLDocument := NewXMLDocument;
  self.FSTDMP := GetSTD_MP(self.FXMLDocument);
  self.FSTDMP.DeclareNamespace('xsi','http://www.w3.org/2001/XMLSchema-instance');
  Self.FXSLT_Name := cnstXSLTTransForm;
  Self.FSTDMP.EDocument.Version := cnstVersionSTD_MP;
  self.FKind := Kind;
  case Kind of
    bpkRegistration:
      begin
        self.FCodeType := cnstCodeTypeFormParcels;
        self.FSTDMP.EDocument.CodeType := self.FCodeType;
        self.FSTDMP.EDocument.Guid := _GetSimplifiedGUID;
        self.GetDFormParcels.XmlFormParcels := self.FSTDMP.Package.FormParcels;
        OnChangeActivPage(GetDFormParcels.lblFormParcels); {����������� �������}
        SetActivPageParcels(tsNewParcel);
      end;
    bpkChanges:
      begin
        self.FCodeType := cnstCodeTyteSpecifyRelated;
        self.FSTDMP.EDocument.CodeType := self.FCodeType;
        self.FSTDMP.EDocument.Guid := _GetSimplifiedGUID;
        if isTypePackage = tpExistParcel then
        begin
         self.GetDSpecifyParcels.XmlSpecifyParcels_ExistParcels := self.FSTDMP.Package.SpecifyParcel;
         OnChangeActivPage(GetDSpecifyParcels.lblSpecifyparcels);
         SetActivPageParcels(tsExistParcel);
        end
        else
        begin
         self.GetDSpecifyParcels.XmlSpecifyParcels_ExistEZParcels := self.FSTDMP.Package.SpecifyParcel;
         OnChangeActivPage(GetDSpecifyParcels.lblSpecifyParcelsEZ);
         SetActivPageParcels(tsExistEZParcels);
        end;
      end;
     bpNewSubParcels: 
     begin
        self.FCodeType := cnstCodeTyteSpecifyRelated;
        self.FSTDMP.EDocument.CodeType := self.FCodeType;
        self.FSTDMP.EDocument.Guid := _GetSimplifiedGUID;
        self.GetDNewSubParcels.XmlFormParcels := self.FSTDMP.Package.NewSubParcel;
        {����������� �������}
        OnChangeActivPage(GetDNewSubParcels.lblNewSubParcels);
        SetActivPageParcels(tsNewSubParcel);
     end;
  end;
  self.GetDGenInf.XmlSTDMP := self.FSTDMP;
  TSettingPathFile.DeleteTempPath; //TODO : �������������
  SetActivPageParcels(tsContractor);
end;

procedure TMSBoundaryPlanXML.DoClose;
var path : string;
begin
  //������� ������ ������������ ������
  if TFile.Exists(STD_MP_TEMPDIR+ '\'+cnstLogLoadFile) then
   TFile.Delete(STD_MP_TEMPDIR+ '\'+cnstLogLoadFile);{������� ini- ���� ��� ������������ ������� ������}

  TSettingPathFile.DeleteTempPath; //��������� ������ :) �������  ��������� �������
  self.GetDGenInf.XmlSTDMP := nil;
  self.GetDFormParcels.XmlFormParcels := nil;
  self.GetDSpecifyParcels.XmlSpecifyParcels_ExistParcels:= nil;
  self.GetDSpecifyParcels.XmlSpecifyParcels_ExistEZParcels:= nil;
  self.GetDNewSubParcels.XmlFormParcels := nil;
  self.FSTDMP := nil;
  OnChangeActivPage(self);
  end;

function TMSBoundaryPlanXML.ExportTo(PathDir: string; Guid: string): string;
var
  _path: string;
  _StPr: TMSStringParser;
  stlError : TStringList;
begin
  Result := '';
  _path := ExtractFilePath(TSettingPathFile._TempPath) + rsMGisXMLEditor + '_' + Guid + '.xml';
  { ���������� ����� ����}
  _StPr := TMSStringParser.Create('!');
  _StPr.Parse(self.FSTDMP.EDocument.Guid);
  self.FSTDMP.EDocument.Guid := _GetSimplifiedGUID + '!' +
  _StPr.GetToken(_StPr.GetCountToken - 1);
  _StPr.Free;
  self.FSTDMP.EDocument.Guid := Guid;
  self.SaveCopyAs(_path);
  if not MsXml_DOMTransform(_path, STD_MP_WorkDir + 'Transform\' + self.FXSLT_Name, _path + '~') then  EXIT;
  stlError := TStringList.Create; {��������� �� �������}
  try
    if not MsXml_DOMCheck(_path+'~',STD_MP_WorkDir+cnstSTD_MP_Name,stlError) then
       raise Exception.Create('������ xml �� ������������ �����!');
  finally    
     FreeAndNil(stlError);
  end;
  CopyFile(pchar(_path + '~'), pchar(PathDir + rsGKUZU + '_' + self.FSTDMP.EDocument.Guid + '.xml'), false);
  Result := rsGKUZU + '_' + self.FSTDMP.EDocument.Guid + '.xml';
end;

///
/// �������� xml ������ � ����
///
function TMSBoundaryPlanXML.ExportXmltoWord(pathXml : string): string;
  var
  _path: string;

begin
  TFrLoader.Inctance.RunAnimation := true;
  try
      Result := '';
      _path:=  ExtractFilePath(TSettingPathFile._TempPath) + rsMGisXMLEditor + '_' + self.FSTDMP.EDocument.Guid + '.xml';
      self.SaveCopyAs(_path);
      //�� �����������
      if self.FSTDMP.Package.ChildNodes.FindNode('FormParcels') <> nil then
      begin
            if  MsXml_DOMTransform(_path, STD_DirXSLTDOC + 'FormParcels.xsl', _path + '.doc') then
               Result := _path+'.doc';
      end;
      if self.FSTDMP.Package.ChildNodes.FindNode('SpecifyParcel') <> nil then
      begin
            if  MsXml_DOMTransform(_path, STD_DirXSLTDOC + 'ExistParcels.xsl', _path + '.doc') then
                Result := _path+'.doc';
      end;
  finally
      TFrLoader.Inctance.RunAnimation := false
  end;
end;

function TMSBoundaryPlanXML.IsAssigned: boolean;
begin
  Result := self.FSTDMP <> nil;
end;

///
/// <param name="path">���� � ������� ��� xml ����� </param>
///
procedure TMSBoundaryPlanXML.Load(Path: string);
var FileName : string;   
    FrmtPath : string;
begin
try
  TFrLoader.Inctance.RunAnimation := true;
  //ClearDir(TSettingPathFile.MakeTempPath);  {��������� "��������" ^))))))))))))}
  TSettingPathFile.DeleteTempPath;
  TAppliedFileDirect.AppliedFileDirect.WriteSectionOpenIni(rsNameOpenFile,rsNameOpenFile,Path);

  FrmtPath := ExtractFileExt(Path);  {�������� ��� ��������� ������ ��� ����}
  if FrmtPath = '.'+cnstLMProj then
  begin
      FrLandMark.jvlgfl.Add(DateTimeToStr(Now), '', '');
      FrLandMark.jvlgfl.Add(DateTimeToStr(Now), 'name vfibyt ', GetComputerNetName());
      FrLandMark.jvlgfl.Add(DateTimeToStr(Now), '��������� �����, ������������� ',path);

      TLMProject.DecompressProj(Path);{�������������  ������ }

      FrLandMark.jvlgfl.Add(DateTimeToStr(Now), '�������� �������� ���� xml', '');
      FileName := GetFileName(Path);
      FrLandMark.jvlgfl.Add(DateTimeToStr(Now), '�������������� ����', FileName);
      FrLandMark.jvlgfl.Add(DateTimeToStr(Now), '��������� xml ', TSettingPathFile.MakeTempPath+'\'+ FileName+'.'+cnstXML);
      self.FXMLDocument := LoadXMLDocument(TSettingPathFile.MakeTempPath+'\'+ FileName+'.'+cnstXML);

      TFile.Delete(TSettingPathFile.MakeTempPath+'\'+FileName+'.'+cnstXMl);{����� ��� ������� - ���� ������ ��� ������� ��������}
      //TODO : ���� �������� -  ����� ������  = ������� �� ������� ������
      if TFile.Exists(TSettingPathFile.MakeTempPath+'\'+'LandMark.ini') then
          tFile.Delete(TSettingPathFile.MakeTempPath+'\'+'LandMark.ini')

  end
  else  self.FXMLDocument := LoadXMLDocument((Path));
  SetXMlDocuments;  {��������� xml - ������}
  case FKind of
    bpkRegistration: SetXMLFormParcels ;
    bpkChanges: SetXMLSpecifyParcels ;
    bpNewSubParcels: SetXMLNewSubParcels ;
  end;

  self.FPathFile := Path;
finally  
  TFrLoader.Inctance.RunAnimation := false;
end;
end;

///
/// ���������������  ������  ������
///
procedure TMSBoundaryPlanXML.RegenerationErrorListWord(var list: TStringList);
var i, count : Integer;
    str : string;
    Result : TStringList;
    xmlNode : IXMLNode;
begin
  xmlNode := GetReadGlossarii(cnstGlossary+cnstFGlossary);
  Result := TStringList.Create;
  try
     for i := 0 to list.Count-1 do
     begin
       str := list.Strings[i];
       if (str.IndexOf('clHotLight') <> -1)  then   Result.Add(str)  //TODO: ������.  ������ 
       else   Result.Add(UpdateEnglWord(xmlNode,str));
     end;
     list.Clear;
     list.Text := Result.Text;
  finally
     FreeAndNil(Result);
     xmlNode := nil;
  end;
end;

function TMSBoundaryPlanXML.Save: boolean;
begin
  Result := false;
  if self.FPathFile <> '' then   Result := self.SaveAs(self.FPathFile)
end;

function TMSBoundaryPlanXML.SaveAs(FileName: string): boolean;
begin
  Result := true;
  try
    if self.SaveCopyAs(FileName) then
      self.FPathFile := FileName;
  except on E : Exception do
  begin
    Result := false;
    raise Exception.Create(E.Message);
  end;  
  end;
end;

function TMSBoundaryPlanXML.SaveCopyAs(FileName: string): boolean;
begin
  Result := True;
  TRY
    self.FXMLDocument.SaveToFile(FileName);
  EXCEPT
    Result := false;
  END;
end;

procedure TMSBoundaryPlanXML.SetActivPageParcels(activPage: TTypePackage);
begin
  if not Assigned(self.FDialog_GenInf) then Exit;

  case activPage  of
     tsNewParcel :   self.FDialogPackage_FormParcels.jvPageControl_FP.ActivePage :=
                             self.FDialogPackage_FormParcels.ts_NewParcels;
     tsChangeParcel: self.FDialogPackage_FormParcels.jvPageControl_FP.ActivePage :=
                             self.FDialogPackage_FormParcels.ts_ChangeParcels;
     tsFPSpecifyRelatedParcel: self.FDialogPackage_FormParcels.jvPageControl_FP.ActivePage :=
                             self.FDialogPackage_FormParcels.ts_SpecifyRelatedParcel;
     tsExistParcel  : self.FDialogPackage_SpecifyParcels.jvSpecifyParcels.ActivePage :=
                             self.FDialogPackage_SpecifyParcels.tsExisParcel;
     tsSPpecifyRelatedParcel : self.FDialogPackage_SpecifyParcels.jvSpecifyParcels.ActivePage :=
                             self.FDialogPackage_SpecifyParcels.tsSpecifyRelatedParcel;
     tsExistEZParcels        : self.FDialogPackage_SpecifyParcels.jvSpecifyParcels.ActivePage :=
                            self.FDialogPackage_SpecifyParcels.tsExistEzParcels;
     tsExistEZEntryParcels  : self.FDialogPackage_SpecifyParcels.jvSpecifyParcels.ActivePage :=
                            self.FDialogPackage_SpecifyParcels.tsExistEZEntryParcel;
     tsSPESpecifyRelatedParcel : self.FDialogPackage_SpecifyParcels.jvSpecifyParcels.ActivePage :=
                            self.FDialogPackage_SpecifyParcels.tsSpecifyRelatedParcel;
     tsClients               :Self.FDialog_GenInf.GIPageControl.ActivePage :=
                              Self.FDialog_GenInf.TabSheetClient;
     tsConclusion  :Self.FDialog_GenInf.GIPageControl.ActivePage :=
                              Self.FDialog_GenInf.ConclusionTabSheet;
     tsInputDate   :Self.FDialog_GenInf.GIPageControl.ActivePage :=
                              Self.FDialog_GenInf.TabSheetInputDate;
     tsContractor   :Self.FDialog_GenInf.GIPageControl.ActivePage :=
                              Self.FDialog_GenInf.TabSheetContractor;
     tsDocumentation  :Self.FDialog_GenInf.GIPageControl.ActivePage :=
                              Self.FDialog_GenInf.TabSheet2;
     tsNewSubParcel  : Self.FDialogPackage_NewSubParcecel.jvPageControl_FP.ActivePage :=
                             self.FDialogPackage_NewSubParcecel.ts_NewParcels;
  end;
end;


procedure TMSBoundaryPlanXML.SetXMlDocuments;
begin
  self.FSTDMP := self.FXMLDocument.GetDocBinding(cnstRootXSD, TXMLSTD_MP,TargetNamespace) as IXMLSTD_MP;
  self.GetDGenInf.XmlSTDMP := self.FSTDMP;
  self.FCodeType := self.FSTDMP.EDocument.CodeType;

  if FSTDMP.Package.ChildNodes.FindNode('NewSubParcel') <> nil   then  self.FKind := bpNewSubParcels
  else if FSTDMP.Package.ChildNodes.FindNode('FormParcels') <> nil then   self.FKind := bpkRegistration
       else self.FKind := bpkChanges;

  SetActivPageParcels(tsContractor);
end;

procedure TMSBoundaryPlanXML.SetXMLFormParcels;
begin
  self.GetDFormParcels.XmlFormParcels := self.FSTDMP.Package.FormParcels;
  self.FXSLT_Name := cnstXSLTTransForm;
  {����������� �������}
  self.OnChangeActivPage(GetDFormParcels.lblFormParcels);
  SetActivPageParcels(tsNewParcel);
end;

procedure TMSBoundaryPlanXML.SetXMLNewSubParcels;
begin
  self.GetDNewSubParcels.XmlFormParcels := self.FSTDMP.Package.NewSubParcel;
  self.FXSLT_Name := cnstXSLTTransForm;
  {����������� �������}
  self.OnChangeActivPage(GetDNewSubParcels.lblNewSubParcels);
  SetActivPageParcels(tsNewSubParcel);
end;

procedure TMSBoundaryPlanXML.SetXMLSpecifyParcels;
begin
  if FSTDMP.Package.SpecifyParcel.ChildNodes.FindNode(cnstXmlExistEZ) <> nil then
  begin
    self.GetDSpecifyParcels.XmlSpecifyParcels_ExistEZParcels := self.FSTDMP.Package.SpecifyParcel;
    GetDSpecifyParcels.SetPageControl(tpExistEZParcel);
    self.OnChangeActivPage(GetDSpecifyParcels.lblSpecifyParcelsEZ);
    SetActivPageParcels(tsExistEZParcels);
  end
  else
  begin
    self.GetDSpecifyParcels.XmlSpecifyParcels_ExistParcels := self.FSTDMP.Package.SpecifyParcel;
    GetDSpecifyParcels.SetPageControl(tpExistParcel);
    self.OnChangeActivPage(GetDSpecifyParcels.lblSpecifyparcels);
    SetActivPageParcels(tsExistParcel);
  end;
  self.FXSLT_Name := cnstXSLTTransForm;
end;

///
/// ������ ��������  ���� �� �������
///
function TMSBoundaryPlanXML.UpdateEnglWord(xmlNode: IXMLNode;  str: string): string;
var  i : Integer;
     stl  : TStringList;
     enWord,ruWord :string;
     isTypeMn : integer; {��� ��������������� �������}
begin
  if str = NullAsStringValue then Exit;
  Result := ''; enWord := ''; ruWord := ''; isTypeMn := 0;

  stl := TStringList.Create;
  try
      stl := GetClipBoardList(regEnglWord,str); //�������� �������� �����
      for I := 0 to stl.Count-1 do
      begin
         enWord := stl.Strings[i];
         ruWord := enWord;         
         isTypeMn := FindEngtoRussWord(xmlNode,ruWord);
         {�������� �  ��������� }
         if isTypeMn = 0  then str :=  str.Replace(enWord+ '[1]',ruWord);
         str:=  str.Replace(enWord,ruWord);
         str:=  WrapText(str, #13#10, ['/'], 1);
      end;
      Result := str;
  finally
    FreeAndNil(stl);
  end;
end;

///
///  <validateXml 04 ������>
///
function TMSBoundaryPlanXML.ValidData(LogOut: TStrings): boolean;
var
  _path: string;
  regenerateSlError,sl : TStringList; {���  �������� ���������� �� ������� }
begin
  Result := True;
  _path := ExtractFilePath(TSettingPathFile._TempPath) + _GetSimplifiedGUID + '.xml';
  self.SaveCopyAs(_path);

  LogOut.Add(rsTransformingXMLDoc);
  if not MsXml_DOMTransform(_path, STD_MP_WorkDir + cnstTransform + self.FXSLT_Name, _path + '~') then
  begin
    LogOut.Add(rsErrorTransAuthor);
    Result := false;
  end
  else  LogOut.Add(rsCompletedSuccess);

  LogOut.Add('');
  LogOut.Add(rsCheckingDocTheSchema);
  regenerateSlError  := TStringList.Create;
  try
    if  not MsXml_DOMCheck(_path+'~',STD_MP_WorkDir+cnstSTD_MP_Name,regenerateSlError) then
    begin
      RegenerationErrorListWord(regenerateSlError);{ ���������������  }
      LogOut.Add(rsErrorsFound);
      LogOut.AddStrings(regenerateSlError);
      Result := False;
    end
    else LogOut.Add(rsCompletedSuccess);
  finally
    FreeAndNil(regenerateSlError);
  end;

  LogOut.Add('');
  LogOut.Add(rsConsistencyCheckFiles);
  sl := TStringList.Create;
  TAppliedFileDirect.AppliedFileDirect.ReadSectionValues(rsNotLoadFile,sl);
  if sl.Text <> ''  then 
  begin
    LogOut.Add('<b>--- ���������� ������ ---</b> �������������� ������: �������� ������������ �����! !');
    LogOut.Add(sl.Text);
    Result := false;    
  end
  else LogOut.Add(rsCompletedSuccess);
  sl.Free;
  
  LogOut.Add('<b>���� �4\ </b><i>�������� ��������� �� ������������ ���</i>');
  LogOut.Add('<b>--- �� ������������ ---</b>')
end;

{ ����� � "�������� " xml ����� }
procedure TMSBoundaryPlanXML.ExecuteDialog_GenInf(aParent: TWinControl);
begin
 if aParent=nil then begin
  self.GetDGenInf.Parent:= aParent;
  self.GetDGenInf.BorderStyle:=bsSizeable;
  self.GetDGenInf.Align:= alNone;
  self.GetDGenInf.ShowModal;
 end else begin
  self.GetDGenInf.Parent:= aParent;
  self.GetDGenInf.BorderStyle:=bsNone;
  self.GetDGenInf.Align:= alClient;
  self.GetDGenInf.Show;
 end;
end;

procedure TMSBoundaryPlanXML.ExecuteDialog_Package(aParent: TWinControl);
var
 _form: TForm;
begin
  if self.FKind = bpkRegistration then _form := self.GetDFormParcels
  else
    if self.FKind = bpkChanges   then _form := self.GetDSpecifyParcels
    else _form := GetDNewSubParcels;

 if aParent=nil then begin
  _form.Parent:= aParent;
  _form.BorderStyle:=bsSizeable;
  _form.Align:= alNone;
  _form.ShowModal;
 end else begin
  _form.Parent:= aParent;
  _form.BorderStyle:=bsNone;
  _form.Align:= alClient;
  _form.Show;
 end;

end;

procedure TMSBoundaryPlanXML.CloseDialog_GenInf;
begin
 self.GetDGenInf.Hide;
end;

procedure TMSBoundaryPlanXML.CloseDialog_Package;
begin
  if self.FDialogPackage_FormParcels <> nil then
            FDialogPackage_FormParcels.Hide;
  if Self.FDialogPackage_SpecifyParcels<> nil then
           FDialogPackage_SpecifyParcels.Hide;
  if Self.FDialogPackage_NewSubParcecel<> nil then
           FDialogPackage_NewSubParcecel.Hide;
end;

{ ���������� ����� xml }
procedure TMSBoundaryPlanXML.CopyFileXML(SavePath : string);
begin
 if not TDirectory.Exists(SavePath) then  ForceDirectories(SavePath);
 self.SaveAs(ExtractFilePath(savePath) + rsMGisXMLEditor + '_' + _GetSimplifiedGUID + '.xml');
end;

///
///   ������������ ������ ��� ������
///
procedure TMSBoundaryPlanXML.CopyPackage(toPath, savePath: string; const isTransformXml : integer = 0);
var
  pathTransfXMl: string;
  tempPath: string;
  Guid: string;
  NameXml : string;

  /// �������� ���� ������ � ����������� *.xml � ������� ��������
  procedure deleteAllFilesXml(path : string);
  var
      FileName :TSearchRec;
      r :integer;
  begin
   try
    SetCurrentDir(path);
    r := FindFirst('*.xml',faAnyFile,FileName);
    if r = 0 then DeleteFile(FileName.Name);
    while (FindNext(FileName) = 0) do
            DeleteFile(FileName.Name);
   finally
      FindClose(FileName);
   end;
  end;
begin
  tempPath := TSettingPathFile.MakeTempPath + '\';
  Guid := _GetSimplifiedGUID;
  if isTransformXMl = 0  then  savePath := savePath+ '\'+rsGKUZU+'_'+Guid+'\'  {�������������� �����}
  else   if toPath = tempPath then  savePath := savePath +'\';
  if not DirectoryExists(savePath) then   ForceDirectories(savePath);

  { ��������������, ���� �� ��������  ����� }
  if isTransformXml = 0  then
  begin
      NameXml := Guid;
      try
         deleteAllFilesXml(toPath); {������� ���  ����� c ����������� xml (���� ����� �������� �������)}
      except
        raise Exception.Create('�������� ��� ������� Temp ������ ��� ������������ �����');
        Exit;
      end;
      try
        pathTransfXMl := ExportTo(ExtractFilePath(toPath), NameXml); {�������� ���� �������������� xml}
      except
        raise Exception.Create('Xml - ���� �� ����� �� ��������, �������� ������������ �����!');
        Exit;
      end;
      try
        TLMProject.CompressProj(toPath,savePath,rsGKUZU+'_'+NameXml,'zip'); {� ���������� }
      except
        raise Exception.Create('�� ���� ������������ �����! ���������� � ������������! '+
                               #10#13+'*�������� ������� ������� ����!'+#10#13+
                               savePath);
        Exit;
      end;
      try
       // TFile.Delete(ExtractFilePath(toPath)+cnstLogLoadFile);{������� ini- ���� ��� ������������ ������� ������}
      except
        raise Exception.Create('��� ������ ��� ������������ ������ ��� �� ��� ���� ����������!');
        Exit;
      end;
  end
  else
  begin
      FrLandMark.jvlgfl.Add(DateTimeToStr(Now), '','');
      if not TDirectory.Exists(toPath) then  ForceDirectories(toPath);{ ���� ��� ����� ���  ���� xml ��� �������� }
      FrLandMark.jvlgfl.Add(DateTimeToStr(Now), '������� ������� - ��� ��������� ������', toPath);
      NameXml := rsMGisXMLEditor + '_' + Guid;
      FrLandMark.jvlgfl.Add(DateTimeToStr(Now), 'name xml', NameXml);
      if not  self.SaveAs(ExtractFilePath(toPath) + NameXml+ '.xml') then {�������� � ��������� ����������}
      begin
          FrLandMark.jvlgfl.Add(DateTimeToStr(Now), '�� ������� ����������� ', ExtractFilePath(toPath));
          raise Exception.Create('���������....����������, ���������� � ������������  ');
      end
      else
      FrLandMark.jvlgfl.Add(DateTimeToStr(Now), '�������� � ��� ��� ', ExtractFilePath(toPath));
      TLMProject.CompressProj(toPath,savePath,NameXml,cnstLmProj);
      FrLandMark.jvlgfl.Add(DateTimeToStr(Now), '�������',toPath+'    '+savePath);
      MessageDlg('������ ������� ��������!',mtInformation,[mbOK],-1);
        FrLandMark.jvlgfl.Add(DateTimeToStr(Now), '','');
  end;  
end;

constructor TMSBoundaryPlanXML.Create;
begin
  self.FXMLDocument := NewXMLDocument;
end;

destructor TMSBoundaryPlanXML.Destroy;
begin
  self.DoClose;
  inherited;
end;

function TMSBoundaryPlanXML.GetDGenInf: TFormBPXML_GeneralInformation;
begin
  if not Assigned(self.FDialog_GenInf) then
    self.FDialog_GenInf := TFormBPXML_GeneralInformation.Create(nil);
  Result := self.FDialog_GenInf;
end;

function TMSBoundaryPlanXML.GetDNewSubParcels: TFormNewSubParcels;
begin
  if not Assigned(self.FDialogPackage_NewSubParcecel) then
    self.FDialogPackage_NewSubParcecel := TFormNewSubParcels.Create(nil);
  Result := self.FDialogPackage_NewSubParcecel;
end;

function TMSBoundaryPlanXML.GetDFormParcels: TFormBPXML_FormParcels;
begin
  if not Assigned(self.FDialogPackage_FormParcels) then
    self.FDialogPackage_FormParcels := TFormBPXML_FormParcels.Create(nil);
  Result := self.FDialogPackage_FormParcels;
end;

function TMSBoundaryPlanXML.GetDSpecifyParcels: TFormSpecifyParcels;
begin
  if not Assigned(self.FDialogPackage_SpecifyParcels) then
    self.FDialogPackage_SpecifyParcels := TFormSpecifyParcels.Create(nil);
  Result := self.FDialogPackage_SpecifyParcels;
end;

///
///{��������� �� ����  ������������ ��������}
/// <param name="Path">����</param>
/// 
function TMSBoundaryPlanXML.GetFileName(Path: string): string;
var FilePath : string;
begin
  FilePath := ExtractFileName(Path);
  FrLandMark.jvlgfl.Add(DateTimeToStr(Now), 'ExtractFileName = ExtractFileName', FilePath);
  Result := FilePath.Substring(0,Length(FilePath)-7);
  FrLandMark.jvlgfl.Add(DateTimeToStr(Now), '��������� ', Result);
  //TODO: � ������ ����� ������� ������ ���������� LandMARK..... - GKSU.........
end;

///
/// <param  xmlNode ='������� xml'; str ='�������'> </param>
/// <returns> str = '���������� �� ��������'; Result = '��� ��. (��. �� ��.)'</returns>   
///
function TMSBoundaryPlanXML.FindEngtoRussWord(xmlNode: IXMLNode;var str: string): integer;
var i : integer;
    countChildNodes : Integer;
    resStr  : string;
    xmlFindNode : IXMLNode;
begin
  if not Assigned(xmlNode) then Exit;
  i := 0; Result := 0;  resStr := '';
  countChildNodes := xmlNode.ChildNodes.Count;
  try
      while (i< countChildNodes) and (resStr ='') do
      begin
        xmlFindNode := xmlNode.ChildNodes.Get(i);
        if (xmlFindNode.Attributes[rsGlName] = str) then
        begin       
          resStr := xmlFindNode.Attributes[rsGlDocumentation];
          if xmlFindNode.Attributes[rsGlType] <> Null then  Result :=  xmlFindNode.Attributes[rsGlType];
        end;  
        inc(i);
      end;
      if resStr <> ''  then   str := resStr;
  finally    
     xmlFindNode := nil;
  end;
end;


///
/// <param  path = '���� �� �������' > </parma>
///
function TMSBoundaryPlanXML.GetReadGlossarii(path: string): IXMLNode;
var xmlRoot : IXMLDocument;
begin
  xmlRoot := TXMLDocument.Create(nil);
  try
    xmlRoot.FileName := STD_MP_WorkDir + path;
    xmlRoot.Active := true;
    Result := xmlRoot.DocumentElement;
  except 
    xmlRoot := nil;
    Result  := nil;
  end;
end;

end.
