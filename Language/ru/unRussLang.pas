unit unRussLang;

interface

const
///
///  Name XSD
///

   cnstdMethod = 'dMethod.xsd';
   cnstXSLTTransForm  ='V04_STD_MP.xslt';
   cnstFGlossary  = 'GlossarySTD_MPType.xml';
   cnstLmProj     = 'lmproj';
   cnstXML        = 'xml';
   cnstDLLYOUTrack =  'BugTrack.DLL';
   cnstKey        = '\Software\Microsoft\Windows\CurrentVersion\Internet Settings';
   cnstKeyName    = 'ProxyServer';
///
///  BugTracker 
///
   cnstYouTrackHost   = '195.98.73.242';
   cnstYouTrackPort   =  7190;
   cnstYouTrackProjName = 'LM';
///   
/// Direcot
///
   cnstTempDir = 'LandMarkTemp';
   cnstCodeTyteSpecifyRelated = '015';
   cnstCodeTypeFormParcels = '014';
   cnstVersionSTD_MP = '04';
   cnstNameErrorLog = 'Data\Log\errorLog.txt';
   cnstLogLoadFile      =  'LandMarkLogoAppliedFiles.ini';
   cnstTransform = 'Transform\';
   cnstGlossary  = 'Glossary\';

   cnstNewParcelDefinition = '��';
   cnstNewSubParcelDefinition  = '���';
   cnstYYYY_MM_DD = 'yyyy-mm-dd';
   cnstDD_MM_YYYY = 'dd-mm-yyyy';
///
///  XSD FIlE
///
   cnstRootXSD              = 'STD_MP';
   cnstSTD_MP_Name          = 'STD_MP.xsd';
   cnstdRegionsRF           = 'dRegionsRF.xsd';
   cnstdDistrict            = 'dDistrict.xsd';
   cnstdCity                = 'dCity.xsd';
   cnstdUrbanDistrict       = 'dUrbanDistrict.xsd';
   cnstdSovietVillage       = 'dSovietVillage.xsd';
   cnstdInhabitedLocalities = 'dInhabitedLocalities.xsd';
   cnstdUtilizations        = 'dUtilizations.xsd';
   cnstdStreet              = 'dStreets.xsd';
   cnstdLevel1              = 'dLocationLevel1.xsd';
   cnstdLevel2              = 'dLocationLevel2.xsd';
   cnstdLevel3              = 'dLocationLevel3.xsd';
///
///  NAME ELEMENT XML
///
   cnstXmlDate = 'Date'; 
///
///  SUBPARCELS
///
   cnstXmlFormSubParcel = 'FormSubParcel';
   cnstXmlExistSubParcel = 'ExistSubParcel';
   cnstXmlInvariableSubParcel ='InvariableSubParcel';
   
   cnstXmlExistSubParcels = 'ExistSubParcels';
   cnstXmlContours = 'Contours';
   cnstXmlNewContour = 'NewContour';
   cnstXmlExistContour ='ExistContour';
   cnstXmlEntity_Spatial = 'Entity_Spatial';
   cnstXmlComposition_EZ = 'Composition_EZ';
   cnstXmlEncumbrances = 'Encumbrances';
   cnstXmlEncumbrance = 'Encumbrance';   
   cnstXmlSpecifyRelatedParcel = 'SpecifyRelatedParcel';
   cnstXmlSubParcels = 'SubParcels';
   cnstXmlInner_CadastralNumbers = 'Inner_CadastralNumbers';
   cnstXmlRelatedParcels         = 'RelatedParcels';
   cnstXmlAppliedFiles           = 'AppliedFiles';
   cnstXmlAppliedFile = 'AppliedFile';
   cnstXmlAllBorder       = 'AllBorder';
   cnstXmlDeleteAllBorder = 'DeleteAllBorder';
   cnstXmlChangeBprder    = 'ChangeBorder';
   cnstXmlSpecifyRelatedParcel_Contours        = 'Contours';
   cnstXmlOldOrdinate = 'OldOrdinate';
   cnstXmlExistEZEntryParcel  = 'ExistEZEntryParcel';
   cnstXmlExistEZParcels = 'ExistEZParcels';
   cnstXmlNewParcel      = 'NewParcel';
   cnstXmlExistParcel    = 'ExistParcel';
   cnstXmlMax_Area       = 'Max_Area';
   cnstXmlMin_Area       = 'Min_Area';
   cnstXmlArea           = 'Area';
   cnstXmlUnit           = 'Unit';
   cnstXmlDelta          = 'Delta_Area';
   cnstXmlArea_In_GKN    = 'Area_In_GKN';
   cnstXmlNote           = 'Note';
   cnstXmlCadastralNumber = 'CadastralNumber';
   cnstXmlOther = 'Other';
   cnstXmlDocuments = 'Documents';
   cnstXmlDefinition  = 'Definition';
    cnstXmlCadastralBlock = 'CadastralBlock';
///
///  AGENT
///
   cnstXmlAppointment ='Appointment';
///
///  TITLE
///
   cnstXmlReason  = 'Reason';
   cnstXmlPurpose = 'Purpose';

   cnstChangeBorder = 'ChangeBorder';
   cnstXmlExistEZ      = 'ExistEZ';
///
///  Utilizations
///
   cnstXmlUtilization = 'Utilization';
   cnstXmlByDoc = 'ByDoc';
///
///  CHANGEBORDER
///
   cnstXmlNewOrdinate = 'NewOrdinate';
///
///  CLIENT
///
   cnstXmlPerson = 'Person';
   cnstXmlOrganization = 'Organization';
   cnstXmlGovernance = 'Governance';
   cnstXmlForeign_Organization = 'Foreign_Organization';
///
///  RelatedParcels
///
   cnstXmlParcelNeighbour  ='ParcelNeighbour';
   cnstXmlParcelNeighbours ='ParcelNeighbours';
///
///  InpurDate| Data| STd_MP
///
   cnstXmlSurvey = 'Survey';
   cnstXmlScheme_Geodesic_Plotting = 'Scheme_Geodesic_Plotting';
   cnstXmlScheme_Disposition_Parcels = 'Scheme_Disposition_Parcels';
   cnstXmlDiagram_Parcels_SubParcels = 'Diagram_Parcels_SubParcels';
   cnstXmlAgreement_Document = 'Agreement_Document';
   cnstXmlNodalPointSchemes ='NodalPointSchemes';
   cnstXmlAppendix = 'Appendix';
   cnstXmlConclusion = 'Conclusion';
   cnstXmlGeodesic_Bases = 'Geodesic_Bases';
   cnstXmlMeans_Survey  = 'Means_Survey';
   cnstXmlRealty = 'Realty';
   cnstXmlInputDateSubParcels = 'SubParcels';
///
///  Name Atttribute XML
///
   cnstAtrXmlMethod    = 'Method';
   cnstAtrNumber_Record = 'Number_Record';
   cnstAtrDefinition = 'Definition';
   cnstAtrPoint_Pref  = 'Point_Pref';
   cnstAtrNum_Geopoint = 'Num_Geopoint';
   cnstAtrX = 'X';
   cnstAtrY = 'Y';
   cnstAtrDelta_Geopoint = 'Delta_Geopoint';
   cnstAtrArea = 'Area';
   cnstAtrNumber = 'Number';
   cnstAtrSubParcel_Realty ='SubParcel_Realty';
///
///  SHABLON
///
   regCadastralNumber     = '\d+:\d+:\d+:\d+';
   regCadastralBlock      = '\d+:\d+:\d+';
   regFormParcelCadastralNumber = '(\d+:\d+:\d+:\d+)?:��\d+';
   regNewContourDefition ='d+:\d+:\d+:\d+)?:��\d+[(]\d+[)';
   regListComposition_EZ  = '';
   regEnglWord = '(?<word>([A-Za-z_]+))';
   regNameDocuments  = '([^;]*)?[;]([^;]*)?[;](\d{1,2}[\.-]+\d{2}[\.-]+\d{2,4})';

resourcestring
///
///  LandMark Attribute
///
   rsMGisXMLEditor = 'LandMarkXMLEditor';
   rsGKUZU = 'GKUZU';
///
///  List type SpecifyRelatedParcels
///
   rsAltAllBorder = '������ ��������� ������� ������� ��� ������� ��������������� �������';
   rsAltChangeBorder = '��������� ����� ������� (�� ����� �� �����). ���������� ����������� �������.';
   rsAltContours = '���������� ��������� ������� ������� �� ��� � ����� ��������';
   rsAltDeleteAllBorder ='���������� ������� ������� ��������������� �������';

   rsNameTypeSpecifyRelatedParcel = '��� ��������� ������ ������� ��������';
   rsNameMethodFormed  = '������ ����������� ��������� ��������';
   rsNameTypeClients   = '��� ���������';
///
///  List type Client
///
   rsAltPerson = '���������� ����';
   rsAltOrganization = '���������� ����������� ����';
   rsAltForeign_Organization = '����������� ����������� ����';
   rsAltGovernance = '����� ��������������� ������, ����� �������� ��������������';
///
///  Name Element Xml - Russ
///
   rsAltSyrvey  = '�������� � ����������� ���������� � ��������';
   rsAltConclusion = '���������� ������������ ��������';
   rsAltSGP = '����� ������������� ����������';
   rsAltSDP = '����� ������������ ��������� ��������';
   rsAltDPS = '������ ��������� �������� � �� ������';
   rsAltAgreement_Document = '��� ������������ �������������� ������� ���������� �������';
   rsAltNodalPointSchemes = '������ ������� ����� ������ ��������� ��������';
   rsAltAppendix = '����������';
///
///  List Documents
///
   rsSyrvey  = '�������� � ����������� ���������� � ��������';
   rsScheme_Geodesic_Plotting = '����� ������������� ����������';
   rsAgreement_Document = '��� ������������ �������������� ������� ���������� �������';
   rsScheme_Diposition_Parcels ='����� ������������ ��������� ��������';
   rsDiagram_Parcels_SubParcels ='������ ��������� �������� � �� ������';
   rsNodalPointSchemes = '������ ������� ����� ������ ��������� ��������';
   rsAppendix = '����������';
///
///  ProvCadastralNumber
///
   rsCadastralNumber = '����������� �����';
   rsDefinition  = '����������� ��';
   rsOther = '����';
   rsDocuments = '���������- ���������';
   rsMsgCopyDocument  = '���������... ���� ����  ������� ��������� ������...';
   rsMsgClearDir=  '��������� ����� �� �����, ��� �� ���������� ����� �������?';
///
///  CLIENTS
///
   rsChangeTypeClient = '�������� ��� ��������� ���������� �����?';
///
///  AppledFiles
///
   rsNameOpenFile =  '������������ xml';
   rsLoadFile     =  '������������ �����:';
   rsNotLoadFile  =  '����������� �����:';
   rsNameFmApliedFiles = '������ ������';
///
///  Means_Survey
///
   rsMeans_Survey_Name = '�������� ������� (�����������, ����������)';   
   rsMeans_Survey_Certificate = '��������� ����������� ������� (�����������, ����������), ��� ������� ������ �����������';
   rsMeans_Survey_Certificate_Verification = '��������� ������������� � ������� ������� (�����������, ����������)';
///
///  MessageDlg | ErroeMsg LandMark
///
  ExMsg_Data_Invalid    = '���������������� �������� ����: "%s"';
  ExMSG_NotAssignedROOT = '�� �������� �������� �������!!!';

  rs�hangesWillbeLost   = '��� ������������ ��������� ����� �������. ������� �������� xml?';
  rsErrorMsgTypeExist   = '��� ��������� ���� ���������� �������,' + sLineBreak
                            + '����� ��������� ������ ������������� �������� ����� �������!' +
                            sLineBreak + '���������� ���������� ������� ��������?';
  rsNoSelectedValues    = '��� ��������� ��������';
  rsDeleteLine          = '�������  ����� ���  ������� � ';
  rsDeleteLines         = '������  �����  ��� ������� � ';
  rsErrDeleteFile       = '������ ���  ��������  �����';
  rsRelatedParcelsDefinition  = '������� ����������� ����������� ����� ��� ����� �������!';
  rsRelatedNeigboursCadNum    = '������� ����������� �����!';
  rsMsgDeleteFile       =  '������ ���  �������� ����� ��� �������� !';
  rsMsgCopyFile         =  '������ ��� ����������� �����!';
  rsChangeTheValue      =  '��������  ��������!';
  rsEnterTheNumber      =  '������� ����������� �����';
  rsDefinitionNumber          =  '����������� ����������� ����� ��� ����� �������';
  rsEnterThe                  = '������� ';
  rsNoDetetectedLoadFile =  '�� ������ ������ �� ���������� ������������ ������!';
  rsDeleteCurrentCadNum  = '������� ��������� �������(�)?';
  rsDelExistEntryParcel =  '������� �������� � ������������ ��  �������� � ��?';
  rsDeleteData          = '��������� ������ ����� ������� ';

  rsFormParcels  = '����������� ��';
  rsSpecifyParcels = '��������� ��';
  rsSpecifyParcelsEZ ='��������� ��';
  rsNewSubParcel = '����������� ������ ��';
///  
///  Validator
///
  rsTransformingXMLDoc  = '<b>���� �1\</b><i> �������������� ��������� � ������ �������� �����</i>';
  rsErrorTransAuthor =  '<b>������</b> ��������������. �������� ���� ���������, ���������� � �������������';
  rsCompletedSuccess  = '<b>--- ������� �������. ---</b>';
  rsCheckingDocTheSchema =  '<b>���� �2\</b> <i>�������� ��������� �� ������������ ����� �������� �����</i>';
  rsErrorsFound = '<b>--- ���������� ������  ---</b>';
  rsConsistencyCheckFiles  = '<b>���� �3\</b> <i>�������� �� ����������� ������������ ������</i>';

  var
    STD_MP_TEMPDIR : string = '';
    STD_DirXSLTDOC    : string = ''; // ���� �� xslt �� �������������� xml � doc
 implementation

end.
