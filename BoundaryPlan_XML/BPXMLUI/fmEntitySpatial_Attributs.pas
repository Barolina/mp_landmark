unit fmEntitySpatial_Attributs;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ExtCtrls, StdCtrls, Buttons;

type
  TFormEntitySpatial_Attributs = class(TForm)
    CheckBox_NumGeopoint: TCheckBox;
    LabeledEdit_NumGeopoint_firstindex: TLabeledEdit;
    UpDown_NumGeopoint_firstindex: TUpDown;
    CheckBox_PointPref: TCheckBox;
    LabeledEdit_PointPref_value: TLabeledEdit;
    CheckBox_DeltaGeopoint: TCheckBox;
    LabeledEdit_DeltaGeopoint_Value: TLabeledEdit;
    RadioGroup_ApplyFor: TRadioGroup;
    PanelBotControl: TPanel;
    BitBtn_OK: TBitBtn;
    BitBtn_Cancel: TBitBtn;
    Bevel1: TBevel;
    Bevel2: TBevel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEntitySpatial_Attributs: TFormEntitySpatial_Attributs;

implementation

{$R *.dfm}

end.
