object FrameRelatedParcels: TFrameRelatedParcels
  Left = 0
  Top = 0
  Width = 998
  Height = 570
  DockSite = True
  DoubleBuffered = True
  Color = clBtnFace
  ParentBackground = False
  ParentColor = False
  ParentDoubleBuffered = False
  ParentShowHint = False
  ShowHint = False
  TabOrder = 0
  TabStop = True
  object spl: TSplitter
    Left = 0
    Top = 484
    Width = 998
    Height = 12
    Cursor = crVSplit
    Align = alBottom
    Beveled = True
    ExplicitTop = 488
  end
  object pnlRight: TPanel
    Left = 0
    Top = 496
    Width = 998
    Height = 74
    Align = alBottom
    BevelOuter = bvNone
    ParentColor = True
    TabOrder = 0
    Visible = False
    OnClick = actShowOwnerExecute
    object lbl2: TLabel
      Left = 7
      Top = 25
      Width = 63
      Height = 13
      Caption = #1042#1080#1076' '#1087#1088#1072#1074#1072':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      Font.Quality = fqClearType
      ParentFont = False
    end
    object lbl3: TLabel
      Left = 7
      Top = 52
      Width = 272
      Height = 13
      Caption = #1050#1086#1083'-'#1074#1086' '#1087#1088#1072#1074#1086#1086#1073#1083#1072#1076#1072#1090#1077#1083#1077#1081' '#1089#1084#1077#1078#1085#1086#1075#1086' '#1091#1095#1072#1089#1090#1082#1072':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      Font.Quality = fqClearType
      ParentFont = False
    end
    object lblNameRight: TLabel
      Left = 285
      Top = 33
      Width = 4
      Height = 13
      Caption = '-'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Font.Quality = fqClearType
      ParentFont = False
    end
    object lblCountOwner: TLabel
      Left = 285
      Top = 52
      Width = 4
      Height = 13
      Caption = '-'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Font.Quality = fqClearType
      ParentFont = False
    end
  end
  object pnlCadastralNumber: TPanel
    Left = 0
    Top = 0
    Width = 998
    Height = 484
    Align = alClient
    BevelOuter = bvNone
    ParentColor = True
    TabOrder = 1
    object pnlDifinition: TPanel
      Left = 0
      Top = 0
      Width = 998
      Height = 484
      Align = alClient
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 0
      object pnlList: TPanel
        Left = 0
        Top = 0
        Width = 998
        Height = 484
        Align = alClient
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object splattribut: TSplitter
          Left = 681
          Top = 0
          Width = 16
          Height = 484
          Align = alRight
          ExplicitLeft = 689
        end
        object pnlAttributNeighbours: TPanel
          Left = 697
          Top = 0
          Width = 301
          Height = 484
          Align = alRight
          BevelEdges = [beLeft]
          BevelKind = bkFlat
          BevelOuter = bvNone
          Constraints.MinHeight = 166
          Constraints.MinWidth = 301
          TabOrder = 0
          Visible = False
          object ParcelNeighbours_ListBox: TListBox
            Left = 0
            Top = 81
            Width = 299
            Height = 403
            Hint = 
              '|'#1050#1072#1076#1072#1089#1090#1088#1086#1074#1099#1077' '#1085#1086#1084#1077#1088#1072' '#1079#1077#1084#1077#1083#1100#1085#1099#1093' '#1091#1095#1072#1089#1090#1082#1086#1074','#13#10' '#1089#1084#1077#1078#1085#1099#1093' '#1089' '#1091#1090#1086#1095#1085#1103#1077#1084#1099#1084' (' +
              #1086#1073#1088#1072#1079#1091#1077#1084#1099#1084') '#1079#1077#1084#1077#1083#1100#1085#1099#1084' '#1091#1095#1072#1089#1090#1082#1086#1084#13#10'('#1044#1074#1086#1081#1085#1086#1081' '#1082#1083#1080#1082'- '#1076#1083#1103'  '#1080#1079#1084#1077#1085#1077#1085#1080#1103' '#1079#1085 +
              #1072#1095#1077#1085#1080#1103')'
            Style = lbOwnerDrawFixed
            Align = alClient
            BevelEdges = [beTop, beRight]
            BevelInner = bvNone
            BevelKind = bkFlat
            BevelOuter = bvRaised
            BorderStyle = bsNone
            Color = 15921123
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Arial'
            Font.Style = []
            Font.Quality = fqClearType
            ItemHeight = 20
            ParentCtl3D = False
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = ParcelNeighbours_ListBoxClick
            OnDblClick = ParcelNeighbours_ListBoxDblClick
            OnKeyUp = ParcelNeighbours_ListBoxKeyUp
          end
          object pnl1: TPanel
            Left = 0
            Top = 0
            Width = 299
            Height = 81
            Align = alTop
            BevelEdges = []
            BevelKind = bkFlat
            BevelOuter = bvNone
            TabOrder = 1
            object lblNeighbours: TLabel
              AlignWithMargins = True
              Left = 3
              Top = 3
              Width = 258
              Height = 75
              Align = alClient
              Alignment = taCenter
              AutoSize = False
              Caption = 
                #1057#1087#1080#1089#1086#1082' '#1082#1072#1076#1072#1089#1090#1088#1086#1074#1099#1093' '#1085#1086#1084#1077#1088#1086#1074' '#1047#1059', '#13#10#1089#1084#1077#1078#1085#1086#1075#1086' '#1089' '#1091#1090#1086#1095#1085#1103#1077#1084#1099#1084' ('#1086#1073#1088#1072#1079#1091#1077#1084 +
                #1099#1084') '#1047#1059
              Color = clBtnFace
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Pitch = fpFixed
              Font.Style = [fsBold]
              Font.Quality = fqClearType
              ParentColor = False
              ParentFont = False
              Layout = tlCenter
              WordWrap = True
              ExplicitWidth = 206
              ExplicitHeight = 47
            end
            object btngrp1: TButtonGroup
              Left = 264
              Top = 0
              Width = 35
              Height = 81
              Align = alRight
              BevelOuter = bvNone
              BorderStyle = bsNone
              ButtonHeight = 25
              ButtonWidth = 25
              ButtonOptions = [gboFullSize, gboShowCaptions]
              Images = formContainer.ilCommands
              Items = <
                item
                  Hint = #13#10#1044#1086#1073#1072#1074#1080#1090#1100' '#1082#1072#1076#1072#1089#1090#1088#1086#1074#1099#1081' '#1085#1086#1084#1077#1088#13#10#13#10
                  ImageIndex = 0
                  OnClick = actNeightExecute
                end
                item
                  Hint = #13#10#13#10#1059#1076#1072#1083#1080#1090#1100' '#1074#1099#1073#1088#1072#1085#1085#1099#1081' '#1082#1072#1076'. '#1085#1086#1084#1077#1088#13#10
                  ImageIndex = 1
                  OnClick = actHeighExecute
                end
                item
                  Hint = #13#10#1057#1074#1077#1076#1077#1085#1080#1103' '#1086' '#1087#1088#1072#1074#1072#1093' '#1080' '#1087#1088#1072#1074#1086#1086#1073#1083#1072#1076#1072#1090#1077#1083#1103#1093'  '#1089#1084#1077#1078#1085#1086#1075#1086' '#1091#1095#1072#1089#1090#1082#1072#13#10#13#10
                  ImageIndex = 20
                  OnClick = actShowOwnerExecute
                end>
              ShowHint = True
              TabOrder = 0
            end
          end
        end
        object pnlGeneralRelateparcel: TPanel
          Left = 0
          Top = 0
          Width = 681
          Height = 484
          Align = alClient
          BevelOuter = bvNone
          Caption = 'pnlGeneralRelateparcel'
          ParentColor = True
          TabOrder = 1
          ExplicitWidth = 689
          object pnlAttrRelatedParcels: TPanel
            Left = 0
            Top = 0
            Width = 681
            Height = 29
            Align = alTop
            AutoSize = True
            BevelEdges = [beBottom]
            BevelKind = bkFlat
            BevelOuter = bvNone
            TabOrder = 1
            ExplicitWidth = 689
            object lblRelatedParcels: TLabel
              AlignWithMargins = True
              Left = 3
              Top = 3
              Width = 304
              Height = 21
              Align = alLeft
              Alignment = taCenter
              Caption = #1054#1073#1086#1079#1085#1072#1095#1077#1085#1080#1077' '#1093#1072#1088#1072#1082#1090#1077#1088#1085#1086#1081' '#1090#1086#1095#1082#1080' '#1080#1083#1080' '#1095#1072#1089#1090#1080' '#1075#1088#1072#1085#1080#1094#1099
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              Transparent = True
              Layout = tlCenter
              WordWrap = True
              ExplicitHeight = 13
            end
            object lblRelatedCountLabel: TLabel
              Left = 310
              Top = 0
              Width = 3
              Height = 27
              Align = alLeft
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsItalic]
              ParentFont = False
              Layout = tlCenter
              ExplicitHeight = 13
            end
            object btn1: TButtonGroup
              Left = 600
              Top = 0
              Width = 81
              Height = 27
              Align = alRight
              BevelOuter = bvNone
              BorderStyle = bsNone
              ButtonHeight = 25
              ButtonWidth = 25
              Images = formContainer.ilCommands
              Items = <
                item
                  Action = Action_Contrours_AddNew
                  Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1086#1073#1086#1079#1085#1072#1095#1077#1085#1080#1077
                  Hint = #13#10#1044#1086#1073#1072#1074#1080#1090#1100' '#1086#1073#1086#1079#1085#1072#1095#1077#1085#1080#1077#13#10'|'#1044#1086#1073#1072#1074#1080#1090#1100' '#1086#1073#1086#1079#1085#1072#1095#1077#1085#1080#1077
                end
                item
                  Action = Action_Contours_DeleteCurrent
                  Caption = #1059#1076#1072#1083#1080#1090#1100' '#1074#1099#1073#1088#1072#1085#1085#1086#1077' '#1086#1073#1086#1079#1085#1072#1095#1077#1085#1080#1077
                  Hint = 
                    #13#10#1059#1076#1072#1083#1080#1090#1100' '#1074#1099#1073#1088#1072#1085#1085#1099#1081' '#1101#1083#1077#1084#1077#1085#1090' '#13#10#1044#1083#1103' '#1091#1076#1072#1083#1077#1085#1080#1103'  '#1085#1077#1089#1082#1086#1083#1100#1082#1080#1093' '#1101#1083#1077#1084#1077#1085#1090#1086#1074 +
                    ', '#1085#1077#1086#1073#1093#1086#1076#1080#1084#1086' '#1080#1093' '#13#10#1074#1099#1076#1077#1083#1080#1090#1100' '#1080#1093' '#1074' '#1089#1087#1080#1089#1082#1077#1080' '#1085#1072#1078#1072#1090#1100'  '#1082#1083#1072#1074#1080#1096#1091' Del'
                end
                item
                  Caption = #1042#1089#1090#1072#1074#1080#1090#1100' '#1076#1072#1085#1085#1099#1077' '#1080#1079' '#1073#1091#1092#1077#1088' '#1086#1073#1084#1077#1085#1072
                  Hint = 
                    '|'#1042#1089#1090#1072#1074#1080#1090#1100' '#1076#1072#1085#1085#1099#1077' '#1080#1079' '#1073#1091#1092#1077#1088#1072'  '#1086#1073#1084#1077#1085#1072#13#10#1060#1086#1088#1084#1072#1090' '#1076#1072#1085#1085#1099#1093'  ('#1087#1088#1080#1084#1077#1088' )'#13#10#1085'1' +
                    '85-'#1085'185, '#1085'189;70:14:0300083:168;'#1042#1080#1076' '#1087#1088#1072#1074#1072#13#10#13#10#1080#1083#1080#13#10#13#10#1085'185-'#1085'185, '#1085 +
                    '189\t70:14:0300083:168\t'#1042#1080#1076' '#1087#1088#1072#1074#1072
                  ImageIndex = 2
                  OnClick = btnContours_List_ButtonGroupItems0Click
                end>
              ShowHint = True
              TabOrder = 0
              ExplicitLeft = 608
            end
          end
          object scrlbx: TScrollBox
            Left = 0
            Top = 29
            Width = 681
            Height = 455
            Align = alClient
            BevelInner = bvNone
            BevelOuter = bvNone
            BevelKind = bkFlat
            BorderStyle = bsNone
            TabOrder = 0
            object RelatedParcels_ListBox: TListBox
              Left = 0
              Top = 0
              Width = 681
              Height = 440
              Hint = #1057#1087#1080#1089#1086#1082' '#1086#1073#1086#1079#1085#1072#1095#1077#1085#1080#1081' '#1093#1072#1088#1072#1082#1090#1077#1088#1085#1086#1081' '#1090#1086#1095#1082#1080' '#1080#1083#1080' '#1095#1072#1089#1090#1077#1081' '#1075#1088#1072#1085#1080#1094#1099
              Style = lbOwnerDrawFixed
              Align = alClient
              BevelInner = bvNone
              BevelKind = bkFlat
              BevelOuter = bvNone
              BevelWidth = 2
              BorderStyle = bsNone
              Color = 15921123
              Ctl3D = False
              Font.Charset = RUSSIAN_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Arial'
              Font.Style = []
              Font.Quality = fqClearType
              IntegralHeight = True
              ItemHeight = 20
              ParentCtl3D = False
              ParentFont = False
              ParentShowHint = False
              ScrollWidth = 1
              ShowHint = True
              TabOrder = 0
              OnClick = Action_Contours_SetCurrentExecute
              OnDblClick = RelatedParcels_ListBoxDblClick
              OnKeyUp = RelatedParcels_ListBoxKeyUp
            end
          end
        end
      end
    end
  end
  object ActionList: TActionList
    Left = 456
    Top = 224
    object Action_Contrours_AddNew: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1082#1086#1085#1090#1091#1088
      Hint = #1044#1086#1073#1072#1074#1080#1090#1100' '#1082#1086#1085#1090#1091#1088
      ImageIndex = 0
      OnExecute = Action_Contrours_AddNewExecute
    end
    object Action_Contours_DeleteCurrent: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1074#1099#1073#1088#1072#1085#1085#1099#1081' '#1082#1086#1085#1090#1091#1088
      Hint = #1059#1076#1072#1083#1080#1090#1100' '#1074#1099#1073#1088#1072#1085#1085#1099#1081' '#1082#1086#1085#1090#1091#1088
      ImageIndex = 1
      OnExecute = Action_Contours_DeleteCurrentExecute
    end
    object Action_Contours_SetCurrent: TAction
      Caption = #1057#1076#1077#1083#1072#1090#1100' '#1090#1077#1082#1091#1097#1080#1084
      OnExecute = Action_Contours_SetCurrentExecute
    end
    object actHint: TAction
      Caption = 'actHint'
    end
    object actShowOwner: TAction
      OnExecute = actShowOwnerExecute
    end
  end
  object actlstNeigh: TActionList
    Left = 648
    Top = 288
    object actNeight: TAction
      Hint = #1044#1086#1073#1072#1074#1080#1090#1100' '#1082#1086#1085#1090#1091#1088
      ImageIndex = 0
      OnExecute = actNeightExecute
    end
    object actHeigh: TAction
      Hint = #1059#1076#1072#1083#1080#1090#1100' '#1074#1099#1073#1088#1072#1085#1085#1099#1081' '#1082#1086#1085#1090#1091#1088
      ImageIndex = 1
      OnExecute = actHeighExecute
    end
    object actNeigh: TAction
      Caption = #1057#1076#1077#1083#1072#1090#1100' '#1090#1077#1082#1091#1097#1080#1084
      OnExecute = Action_Contours_SetCurrentExecute
    end
  end
end
