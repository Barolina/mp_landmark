unit fmAllDocumentsMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, ButtonGroup, StdCtrls, ActnList,
  System.math, VirtualTrees, Vcl.Buttons, Vcl.ExtCtrls,
  
  alldocuments,fmContainer;

type
  TFormAllDocumentsMain = class(TForm)
    Button2: TButton;
    VirtualStringTree1: TVirtualStringTree;
    Panel1: TPanel;
    btnOk: TBitBtn;
    btnCancel: TBitBtn;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    ButtonGroup1: TButtonGroup;
    procedure VirtualStringTree1GetText(Sender: TBaseVirtualTree;
      Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType;
      var CellText: string);
    procedure VirtualStringTree1IncrementalSearch(Sender: TBaseVirtualTree;
      Node: PVirtualNode; const SearchText: string; var Result: Integer);
    procedure FormCreate(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure btnCollapseItemsClick(Sender: TObject);
    procedure ButtonGroup1Items0Click(Sender: TObject);
  private
    { Private declarations }
    FDoc: TAllDocuments;
    procedure setKey(aKey:string);
    procedure setValue(aValue:string);
    function getKey: string;
    function getValue: string;
  public
    { Public declarations }
    procedure LoadXSD(aPathFile: string);
    procedure CollapseItems;

    property Key: string read getKey write setKey;
    property Value: string read getValue write setValue;
    property Documents:TAllDocuments read FDoc;
  end;

var
  FormAllDocumentsMain: TFormAllDocumentsMain;

implementation

{$R *.dfm}

procedure test(vt: TVirtualStringTree; aDest: PVirtualNode; aSource: TAllDocuments.TNode);
var
 _source: TAllDocuments.TNode;
 _dest: PVirtualNode;
begin
 _dest:= vt.InsertNode(aDest,amAddChildLast,pointer(aSource));
 for _source in aSource.ChildList do
  test(vt,_dest,_source);
end;

procedure TFormAllDocumentsMain.Button2Click(Sender: TObject);
begin
 self.LoadXSD('c:\1.xsd');
end;

procedure TFormAllDocumentsMain.ButtonGroup1Items0Click(Sender: TObject);
begin
 self.CollapseItems;
end;

procedure TFormAllDocumentsMain.CollapseItems;
var
 _vtnode: PVirtualNode;
begin
 self.VirtualStringTree1.BeginUpdate;
 TRY
  for _vtnode in self.VirtualStringTree1.Nodes do
   _vtnode.States:= _vtnode.States-[vsExpanded];
 FINALLY
  self.VirtualStringTree1.EndUpdate;
 END;

end;

procedure TFormAllDocumentsMain.FormCreate(Sender: TObject);
begin
 self.FDoc:= TAllDocuments.Create;
end;

function TFormAllDocumentsMain.getKey: string;
var
 _vtnode: PVirtualNode;
 _data: Pointer;
begin
 Result:= '';
 for _vtnode in self.VirtualStringTree1.SelectedNodes do begin
  _data:= self.VirtualStringTree1.GetNodeData(_vtnode);
  if (_data=nil) then CONTINUE;
  Result:= self.FDoc.getKey(TAllDocuments.TNode(_data^).Id);
  BREAK;
 end;
end;

function TFormAllDocumentsMain.getValue: string;
var
 _vtnode: PVirtualNode;
 _data: Pointer;
begin
 Result:= '';
 for _vtnode in self.VirtualStringTree1.SelectedNodes do begin
  _data:= self.VirtualStringTree1.GetNodeData(_vtnode);
  if (_data=nil) then CONTINUE;
  Result:= self.FDoc.getValue(TAllDocuments.TNode(_data^).Id);
  BREAK;
 end;
end;

procedure TFormAllDocumentsMain.LoadXSD(aPathFile: string);
var
 _source: TAllDocuments.TNode;
begin
 self.FDoc.LoadFromFile(aPathFile);
 self.VirtualStringTree1.NodeDataSize:= SizeOf(TObject);
 self.VirtualStringTree1.Clear;
 for _source in self.FDoc.RootNode.ChildList do
  test(self.VirtualStringTree1,nil,_source);
end;

procedure TFormAllDocumentsMain.setKey(aKey: string);
var
 _id: integer;
 _vtnode: PVirtualNode;
  _data: Pointer;
begin
 self.VirtualStringTree1.ClearSelection;
 _id:= self.FDoc.getIdByKey(aKey);
 if _id=-1 then EXIT;
 for _vtnode in self.VirtualStringTree1.Nodes(true) do begin
   _data:= self.VirtualStringTree1.GetNodeData(_vtnode);
  if (_data=nil) then CONTINUE;
  if TAllDocuments.TNode(_data^).Id = _id then begin
   self.VirtualStringTree1.Selected[_vtnode]:= TRUE;
   self.VirtualStringTree1.FocusedNode:= _vtnode;
   BREAK;
  end;
 end;
end;

procedure TFormAllDocumentsMain.setValue(aValue: string);
var
 _id: integer;
 _vtnode: PVirtualNode;
  _data: Pointer;
begin

 self.VirtualStringTree1.ClearSelection;
 _id:= self.FDoc.getIdByValue(aValue);
 if _id=-1 then EXIT;
 for _vtnode in self.VirtualStringTree1.Nodes(true) do begin
   _data:= self.VirtualStringTree1.GetNodeData(_vtnode);
  if (_data=nil) then CONTINUE;
  if TAllDocuments.TNode(_data^).Id = _id then begin
   self.VirtualStringTree1.Selected[_vtnode]:= TRUE;
   self.VirtualStringTree1.FocusedNode:= _vtnode;
   BREAK;
  end;
 end;
end;

procedure TFormAllDocumentsMain.SpeedButton1Click(Sender: TObject);
var
 _st: string;
begin
 _st:= InputBox('Key','','');
 if _st='' then EXIT;
 self.Key:= _st;


end;

procedure TFormAllDocumentsMain.SpeedButton2Click(Sender: TObject);
var
 _st: string;
begin
 _st:= InputBox('Value','','');
 if _st='' then EXIT;
 self.Value:= _st;


end;

procedure TFormAllDocumentsMain.btnCollapseItemsClick(Sender: TObject);
begin
 self.CollapseItems;
end;

procedure TFormAllDocumentsMain.VirtualStringTree1GetText(
  Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex;
  TextType: TVSTTextType; var CellText: string);
var
 _data: Pointer;
begin
 _data:= sender.GetNodeData(node);
 if (_data<>nil) then
  CellText:= self.FDoc.getValue(TAllDocuments.TNode(_data^).Id);
end;

procedure TFormAllDocumentsMain.VirtualStringTree1IncrementalSearch(
  Sender: TBaseVirtualTree; Node: PVirtualNode; const SearchText: string;
  var Result: Integer);
var
 _data: Pointer;
 _node:TAllDocuments.TNode;
 _value: string;
begin
 _data:= sender.GetNodeData(node);
 if (_data=nil) then EXIT;
 _node:= TAllDocuments.TNode(_data^);
 _value:= self.FDoc.getValue(_node.Id);
 Result := StrLIComp(PAnsiChar(AnsiLowerCase(SearchText)),
                     PAnsiChar(AnsiLowerCase(_value)),
                     Min(Length(SearchText), Length(_value)));

end;

end.

