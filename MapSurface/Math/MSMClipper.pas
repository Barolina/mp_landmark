unit MSMClipper;
interface
 uses
  classes, MSMType_,  Math, MSMCommon_, SysUtils, msmTopology,
  MSVectorObjects, MSLogicalCanvas ;
 type
   TMSMClContourType = (ctContourA, ctHoleA, ctContourB, ctHoleB);
   type
    PMSmclPoint = ^TMSmclPoint;
    TMSmclPoint = record
     Point: TMSMPoint;
     XPos: integer;
     YPos: integer;
    end;
    PMSmclRib = ^TMSmclRib;
    TMSmclRib = record
     UsePlgACn: integer;
     UsePlgBCn: integer;
    end;
   TMSMClipper = class
   private
    FTopology: TMSmVirtualTopology;
    FDelta: integer;
    FRoundTo: integer;
    _IncludingNode: integer;
    _IncludeNode: integer;
    _BindingNodes: integer;
    _BindNodes: integer;
    function GetNodeByPos(X,Y: integer): TMSmVirtualTopoNode;
    function DoInitPoint(aPoint: TMSMPoint): TMSmVirtualTopoNode;
    function GetNodesInRib(Ax,Ay,Bx,By: integer; Dest: TList): integer;
   protected
    procedure TpIncludingNode(Sender: TMSmVirtualTopology; aNode: TMSmVirtualTopoNode; var Index: integer; Accept: TMSmVTopo_Accept);
    procedure TpIncludeNode(Sender: TMSmVirtualTopology; aNode: TMSmVirtualTopoNode; Index: integer);
    procedure TpBindingNodes(Sender: TMSmVirtualTopology; aNode,BindNode: TMSmVirtualTopoNode;
                             var aInitData: pointer; var aSizeData: integer; var IndexBind: integer; Accept: TMSmVTopo_Accept);
    procedure TpBindNodes(Sender: TMSmVirtualTopology; aNode,BindNode: TMSmVirtualTopoNode; IndexBind: integer);
   public
    constructor Create(aOwnerPolygons: boolean);
    destructor Destroy; override;
    function InitPoints(aPoints: TMSMPoints; PolyType: TMSMClContourType; AsClosed: boolean): boolean;
   end;
   TMSMCliperVO = class(TMSBaseVectorObject)
    private
     FClipper: TMSMClipper;
    public
     constructor create;
     destructor Destroy; override;
     procedure Init(ACurver: TMSPolyLineVO);
     procedure Draw(Canvas: TMSLogicalCanvas); override;
   end;
implementation
uses MSMath_;
 
 function _DblAsInt(Value: Double; ADigit: Integer): integer;
 begin
  Result:= Round(value * power(10,abs(ADigit)));
 end;
 function _IntAsDbl(Value: integer; ADigit: Integer): double;
 begin
  Result:= value / power(10,abs(ADigit));
 end;
{ TMSMClipper }
constructor TMSMClipper.Create(aOwnerPolygons: boolean);
begin
 self.FTopology:= TMSmVirtualTopology.Create(TRUE);
 self.FTopology.OnIncludingNode:= self.TpIncludingNode;
 self.FTopology.OnIncludeNode:= self.TpIncludeNode;
 self.FTopology.OnBindingNodes:= self.TpBindingNodes;
 self.FTopology.OnBindNodes:= self.TpBindNodes;
 FRoundTo:= -2;
 FDelta:= 1;
   _IncludingNode:= 0;
    _IncludeNode:=0;
    _BindingNodes:=0;
    _BindNodes:=0;
end;
destructor TMSMClipper.Destroy;
begin
 self.FTopology.Free;
 inherited;
end;
function TMSMClipper.DoInitPoint(aPoint: TMSMPoint): TMSmVirtualTopoNode;
var
 _clpoint: TMSmclPoint;
 _XPos,_YPos: integer;
begin
 _XPos:= _DblAsInt(aPoint.X,FRoundTo);
 _YPos:= _DblAsInt(aPoint.Y,FRoundTo);
 Result:= self.GetNodeByPos(_XPos,_YPos);
 if  Assigned(Result) then EXIT;
 with _clpoint do begin
  Point:= aPoint.GetCopy;
  XPos:= _XPos;
  YPos:= _YPos;
 end;
 Result:= self.FTopology.CreateNode(@_clpoint,SizeOf(TMSmclPoint));
 if not Assigned(Result) then begin
  _clpoint.Point.Free;
  raise Exception.Create('Init ERROR');
 end;
end;
function TMSMClipper.GetNodeByPos(X, Y: integer): TMSmVirtualTopoNode;
var
 _i: integer;
 _data: PMSmclPoint;
begin
 Result:= nil;
 for _i := 0 to self.FTopology.NodesCount-1 do begin
  _data:= self.FTopology.NodesEnum[_i].Data;
  if msmcIsLE(abs(_data^.XPos-X),self.FDelta) and msmcIsLE(abs(_data^.YPos-Y),self.FDelta)  then begin
   Result:= self.FTopology.NodesEnum[_i];
   BREAK;
  end;
 end;
end;
function TMSMClipper.GetNodesInRib(Ax, Ay, Bx, By: integer; Dest: TList): integer;
var
  _i: integer;
  _Node: TMSmVirtualTopoNode;
  _data: PMSmclPoint;
  _x,_y: Double;
 begin
  Result:= 0;
  for _i := 0 to self.FTopology.NodesCount-1 do begin
   _Node:= self.FTopology.NodesEnum[_i];
   _data:= PMSmclPoint(_Node.Data);
   if not Assigned(_data) then CONTINUE;
   if not msmcIsPointInRect(Ax,Ay,Bx,By,_data^.XPos,_data^.YPos) then CONTINUE;
   msmcGetProjectionPointToLine(Ax,Ay,Bx,By,_data^.XPos,_data^.YPos,_x,_y);
   if msmcIsLE(msmcGetDistanceBetweenPoints(_x,_y,_data^.XPos,_data^.YPos),self.FDelta) then begin
    Dest.Add(_Node);
    inc(Result);
   end;
  end;
  Dest.SortList(
                function(Item1, Item2: Pointer): Integer
                var
                 _data1,_data2: PMSmclPoint;
                begin
                 _data1:= PMSmclPoint(TMSmVirtualTopoNode(item1).Data);
                 _data2:= PMSmclPoint(TMSmVirtualTopoNode(item2).Data);
                 if msmcIsGE(
                             msmcGetDistanceBetweenPoints(Ax,Ay,_data1.XPos,_data1.YPos),
                             msmcGetDistanceBetweenPoints(Ax,Ay,_data2.XPos,_data2.YPos)
                             ) then
                  Result:= 1
                 else
                  Result:= 0;
                end
               );
end;
function TMSMClipper.InitPoints(aPoints: TMSMPoints; PolyType: TMSMClContourType; AsClosed: boolean): boolean;
var
 _i: integer;
 _RibsL: TList;
 _node: TMSmVirtualTopoNode;
 _r:TMSmclRib;
begin
 Result:= false;
 _RibsL:= TList.Create;
 for _i := 0 to aPoints.Count-1 do begin
  _node:= self.DoInitPoint(aPoints.Items[_i]);
  _RibsL.Add(_node);
 end;
 for _i := 0 to _RibsL.Count - 2 do
  TMSmVirtualTopoNode(_RibsL.Items[_i]).Bind(TMSmVirtualTopoNode(_RibsL.Items[_i+1]),@_r,sizeof(TMSmclRib));
 if AsClosed then
  TMSmVirtualTopoNode(_RibsL.First).Bind(TMSmVirtualTopoNode(_RibsL.Last),@_r,sizeof(TMSmclRib));
 _RibsL.Free;
 Result:= TRUE;
end;
procedure TMSMClipper.TpBindingNodes(Sender: TMSmVirtualTopology;
                                     aNode,  BindNode: TMSmVirtualTopoNode;
                                     var aInitData: pointer; var aSizeData: integer;
                                     var IndexBind: integer; Accept: TMSmVTopo_Accept);
var
 _i: integer;
 _Nodes: TList;
 _aNodeD,_BindNodeD: PMSmclPoint;
 _Node: TMSmVirtualTopoNode;
 _data: PMSmclPoint;
 _x,_y: Double;
 _angle: double;
begin
inc(_BindingNodes);
 
 _aNodeD:= aNode.Data;
 _BindNodeD:= BindNode.Data;
 _angle:= msmcGetAngleLine(_aNodeD^.XPos,_aNodeD^.YPos,_BindNodeD^.XPos, _BindNodeD^.YPos);
 for _i := 0 to aNode.BindsCount-1 do begin
  _BindNodeD:= aNode.BindNodes[_i].Data;
  if msmcIsLE(_angle,
              msmcGetAngleLine(_aNodeD^.XPos,_aNodeD^.YPos,_BindNodeD^.XPos, _BindNodeD^.YPos)) then begin
   IndexBind:= _i;
   BREAK;
  end;
 end;
 
{
 if BindNode.IndexOfBindNode(aNode)<>-1 then EXIT;
 _Nodes:= TList.Create;
 _aNodeD:= aNode.Data;
 _BindNodeD:= BindNode.Data;
 
 for _i := 0 to Sender.NodesCount-1 do begin
  _Node:= Sender.NodesEnum[_i];
  _data:= PMSmclPoint(_Node.Data);
  if not msmcIsPointInRect(_aNodeD^.XPos,_aNodeD^.YPos,_BindNodeD^.XPos,_BindNodeD^.YPos,_data^.XPos,_data^.YPos) then CONTINUE;
  msmcGetProjectionPointToLine(_aNodeD^.XPos,_aNodeD^.YPos,_BindNodeD^.XPos,_BindNodeD^.YPos,_data^.XPos,_data^.YPos,_x,_y);
  if msmcIsLE(msmcGetDistanceBetweenPoints(_x,_y,_data^.XPos,_data^.YPos),self.FDelta) then
   _Nodes.Add(_Node);
 end;
 
 
 if _Nodes.Count<=2 then 
  _Nodes.Free
 else begin
  Accept(FALSE);
  
  _Nodes.SortList(
                 function(Item1, Item2: Pointer): Integer
                 var
                  _data1,_data2: PMSmclPoint;
                 begin
                  _data1:= PMSmclPoint(TMSmVirtualTopoNode(item1).Data);
                  _data2:= PMSmclPoint(TMSmVirtualTopoNode(item2).Data);
                  if msmcGetDistanceBetweenPoints(_aNodeD^.XPos,_aNodeD^.YPos,_data1.XPos,_data1.YPos) >
                     msmcGetDistanceBetweenPoints(_aNodeD^.XPos,_aNodeD^.YPos,_data2.XPos,_data2.YPos)
                     then
                   Result:= 1
                  else
                   Result:= 0;
                 end
               );
  
  for _i := 0 to _Nodes.Count-2 do
   TMSmVirtualTopoNode(_Nodes.Items[_i]).Bind(TMSmVirtualTopoNode(_Nodes.Items[_i+1]),aInitData,aSizeData,IndexBind);
  _Nodes.Free;
 end;
 
 }
 dec(_BindingNodes);
end;
procedure TMSMClipper.TpBindNodes(Sender: TMSmVirtualTopology; aNode, BindNode: TMSmVirtualTopoNode; IndexBind: integer);
var
 _Node,_BindNode: TMSmVirtualTopoNode;
 _NodeD, _BindNodeD: PMSmclPoint;
 _p1,_p2: TMSMPoint;
 _Points: TMSMPoints;
 _i,_j: integer;
begin
inc(_BindNodes);
 if BindNode.IndexOfBindNode(aNode)<>-1 then begin
  dec(_BindNodes);
  EXIT;
 end;
 _Points:= TMSMPoints.Create(TRUE);
 _NodeD:= aNode.Data;
 _BindNodeD:= BindNode.Data;
 _p1:= TMSMPoint.Create(0,0);
 _p2:= TMSMPoint.Create(0,0);
 for _i := 0 to Sender.NodesCount-1 do begin
  _Node:= Sender.NodesEnum[_i];
  if (_Node = aNode) or (_Node = BindNode) then CONTINUE;
  for _j := 0 to _Node.BindsCount-1 do begin
   _BindNode:= _Node.BindNodes[_j];
   if (_BindNode = aNode) or (_BindNode = BindNode) then CONTINUE;
   if msmGetCommonPointsLines(_NodeD^.Point,_BindNodeD^.Point, PMSmclPoint(_Node.Data)^.Point,
                              PMSmclPoint(_BindNode.Data)^.Point,ltInterval, _p1,_p2) then begin
   if not(_p1.IsEqualPos(_NodeD^.Point) or
          _p1.IsEqualPos(_BindNodeD^.Point) or
          _p1.IsEqualPos(PMSmclPoint(_Node.Data)^.Point) or
          _p1.IsEqualPos(PMSmclPoint(_BindNode.Data)^.Point)) then begin
    _Points.Add(_p1.GetCopy);
    if not _p1.IsEqualPos(_p2) then
     _Points.Add(_p2.GetCopy);
   end;
   end;
  end;
 end;
 _p1.Free; _p2.Free;
 for _i := 0 to _Points.Count-1 do
  self.DoInitPoint(_Points[_i]);
 _Points.Free;
dec(_BindNodes);
end;
procedure TMSMClipper.TpIncludeNode(Sender: TMSmVirtualTopology; aNode: TMSmVirtualTopoNode; Index: integer);
var
 _i,_j: integer;
 _data,_dataA: PMSmclPoint;
 _Node,_NodeA: TMSmVirtualTopoNode;
 _BrowsedNodes: TList;
 _BindNodesL: TList;
 _X,_Y: double;
 _BindData: TMSmclRib;
begin
inc(_IncludeNode);
 _BrowsedNodes:= TList.Create;
 _BindNodesL:= TList.Create;
 for _i := 0 to Sender.NodesCount-1 do begin
  _Node:= sender.NodesEnum[_i];
  if (_Node=aNode) or (_Node.BindsCount=0) then CONTINUE;
  _data:= _Node.Data;
  _BindNodesL.Clear;
  for _j:= 0 to _Node.BindsCount - 1 do begin
   _NodeA:= _Node.BindNodes[_j];
  if _BrowsedNodes.IndexOf(_NodeA)<>-1 then CONTINUE;
   _dataA:= _NodeA.Data;
   if msmcIsEqual(_data^.XPos,PMSmclPoint(aNode.Data)^.XPos) and msmcIsEqual(_data^.YPos,PMSmclPoint(aNode.Data)^.YPos) then CONTINUE;
   if msmcIsEqual(_dataA^.XPos,PMSmclPoint(aNode.Data)^.XPos) and msmcIsEqual(_dataA^.YPos,PMSmclPoint(aNode.Data)^.YPos) then CONTINUE;
   if not msmcIsPointInRect(_data^.Point.X,_data^.Point.Y,_dataA^.Point.X,_dataA^.Point.Y,
                            PMSmclPoint(aNode.Data)^.Point.X,PMSmclPoint(aNode.Data)^.Point.Y) then CONTINUE;
   msmcGetProjectionPointToLine(_data^.Point.X,_data^.Point.Y,_dataA^.Point.X,_dataA^.Point.Y, PMSmclPoint(aNode.Data)^.Point.X,PMSmclPoint(aNode.Data)^.Point.Y,_x,_y);
   if msmcIsLE(msmcGetDistanceBetweenPoints(_DblAsInt(_x,self.FRoundTo),_DblAsInt(_y,self.FRoundTo),
               PMSmclPoint(aNode.Data)^.XPos,PMSmclPoint(aNode.Data)^.YPos),self.FDelta) then begin
    _BindData:= PMSmclRib(_Node.BindData[_j])^;
    _Node.UnBind(_NodeA);
    
    _Node.Bind(aNode,@_BindData,SizeOf(TMSmclRib));
    _NodeA.Bind(aNode,@_BindData,SizeOf(TMSmclRib));
   end;
  end;
  _BrowsedNodes.Add(_Node);
 end;
 _BrowsedNodes.Free;
 _BindNodesL.Free;
 dec(_IncludeNode);
end;
procedure TMSMClipper.TpIncludingNode(Sender: TMSmVirtualTopology; aNode: TMSmVirtualTopoNode; var Index: integer; Accept: TMSmVTopo_Accept);
var
 _i: integer;
 _data: PMSmclPoint;
 _XPos,_YPos: integer;
begin
 inc(_IncludingNode);
 _data:= aNode.Data;
 _XPos:= _data^.XPos;
 _YPos:= _data^.YPos;
 if self.GetNodeByPos(_XPos,_YPos)<>nil then begin
  Accept(FALSE);
  EXIT;
 end;
 
 for _i := 0 to Sender.NodesCount-1 do begin
  _data:= Sender.NodesEnum[_i].Data;
  if _XPos<=_data^.XPos then begin
   index:= _i;
   BREAK;
  end;
 end;
 dec(_IncludingNode);
end;
{ TMSMCliperVO }
constructor TMSMCliperVO.create;
begin
 inherited;
 self.FClipper:= TMSMClipper.Create(FALSE);
end;
destructor TMSMCliperVO.Destroy;
begin
 self.FClipper.Free;
 inherited;
end;
procedure TMSMCliperVO.Draw(Canvas: TMSLogicalCanvas);
var
 _pen: TMSPen;
 _i,_j: integer;
 _NodeD,_BindNodeD: PMSmclPoint;
begin
 inherited;
 _pen:= TMSPen.Create(mscolor(255,0,0));
 for _i := 0 to self.FClipper.FTopology.NodesCount-1 do begin
  _nodeD:=self.FClipper.FTopology.NodesEnum[_i].Data;
  for _j:= 0 to self.FClipper.FTopology.NodesEnum[_i].BindsCount-1 do begin
   _BindNodeD:=self.FClipper.FTopology.NodesEnum[_i].BindNodes[_j].Data;
   canvas.PaintLine(_pen,_noded^.Point.X,_noded^.Point.Y,_BindNodeD^.Point.X,_BindNodeD^.Point.Y);
   canvas.PaintSimpleShape(shCircle,_pen,nil,2,_noded^.Point.X,_noded^.Point.Y);
  end;
 end;
 _pen.Free;
end;
procedure TMSMCliperVO.Init(ACurver: TMSPolyLineVO);
var
 _i: integer;
begin
 for _i := 0 to ACurver.PointsCount-1 do
  self.AddPoint(ACurver.Points[_i].GetCopy);
 self.FClipper.InitPoints(ACurver.ListPoints,ctContourA,true);
end;
end.
