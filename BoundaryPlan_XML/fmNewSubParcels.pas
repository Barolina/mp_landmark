unit fmNewSubParcels;

interface

uses
  Windows, Messages, SysUtils, Forms,
  Dialogs, StdCtrls,

  frNewSubParcel,
  XMLDoc,
  XMLIntf,
  STD_MP,

  Vcl.PlatformDefaultStyleActnCtrls, ExtCtrls, ComCtrls, Controls,
  System.Classes, Vcl.ActnList, Vcl.ActnMan, Vcl.ButtonGroup,
  System.Actions,

  unRussLang,

  fmContainer, JvgPage, JvComponentBase, JvBalloonHint;

type
  TFormNewSubParcels = class(TForm)
    Panel_ControlNewParcel: TPanel;
    NewParcels_ComboBox: TComboBox;
    ActionManager_FormParcels: TActionManager;
    ButtonGroup1: TButtonGroup;
    Label2: TLabel;

    Action_NewParcels_SetCurrent: TAction;
    ActionXML_NewParcel_AddNew: TAction;
    ActionXML_NewParcel_DelCurrent: TAction;
    ActionXML_NewParcel_SetCurrentNext: TAction;
    ActionXML_NewParcel_SetCurrentBack: TAction;
    ActionXML_NewParcel_GroupEdit: TAction;
    actEditDeifinition: TAction;
    jvPageControl_FP: TJvgPageControl;
    ts_NewParcels: TTabSheet;
    lblName: TLabel;
    cbbFormParcel_Type: TComboBox;
    lblNewSubParcels: TLabel;
    actHint: TAction;
    Panel1: TPanel;

    procedure Action_NewParcels_SetCurrentExecute(Sender: TObject);
    procedure ActionXML_NewParcel_AddNewExecute(Sender: TObject);
    procedure ActionXML_NewParcel_DelCurrentExecute(Sender: TObject);


    procedure ActionXML_NewParcel_SetCurrentNextExecute(Sender: TObject);
    procedure ActionXML_NewParcel_SetCurrentBackExecute(Sender: TObject);

    procedure ActionXML_NewParcel_GroupEditExecute(Sender: TObject);
    procedure ButtonGroup1Items5Click(Sender: TObject);
    procedure cbbFormParcel_TypeChange(Sender: TObject);
    procedure medtFormParcel_CadastralBlockKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure medtFormParcel_CadastralBlockExit(Sender: TObject);
  private
    FXmlFormParcels : IXMLTNewSubParcelList; 
    FNewParcel_frame: TFrameNewSubParcel;
    //����������� ��������� �������
    procedure FormParcels_DoCreateNewParcel;
    procedure FormParcels_DoDelete(index: Integer);
    procedure FormParcels_DoSetCurrentNewParcel(index: Integer);

    procedure FormParcel_doRead_CadastralBlock;
    procedure FormParcel_doWrite_CadastralBlock;
    procedure FormParcel_doRead_Type;
    procedure FormParcel_doWrite_Type;

    procedure SetFormParcels(const aFormParcels: IXMLTNewSubParcelList);
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    function IsAssigned_FormParcels: boolean;

    procedure FormParcels_CreateNewParcel;
    procedure FormParcels_UpDateList;
    procedure FormParcels_Delete(index: Integer);
    procedure FormParcels_SetCurrentNewParcel(index: Integer);
    procedure FormParcels_RefreshList(_A: Integer=-1;_B:Integer=-1);
    //--------------------------------------------------------------------------
    property XmlFormParcels: IXMLTNewSubParcelList read FXmlFormParcels write SetFormParcels;
  end;

var
  FormNewSubParcels: TFormNewSubParcels;

implementation

uses MsXMLAPI ;

{$R *.dfm}

{ TFormBoundaryPlan_XML }

procedure TFormNewSubParcels.ActionXML_NewParcel_AddNewExecute(Sender: TObject);
begin
 self.FormParcels_CreateNewParcel;
 self.FormParcels_SetCurrentNewParcel(self.NewParcels_ComboBox.Items.Count-1);
end;

procedure TFormNewSubParcels.ActionXML_NewParcel_DelCurrentExecute(Sender: TObject);
begin
 if MessageDlg('������� ������: '+' '+'?',mtInformation,mbYesNo,-1) = mrNo then  Exit;
 self.FormParcels_Delete(self.NewParcels_ComboBox.ItemIndex);
end;


procedure TFormNewSubParcels.ActionXML_NewParcel_GroupEditExecute(
  Sender: TObject);
var
 _i,_j: Integer;
 _NewParcel: IXMLTNewParcel_SubParcels;
begin
 self.FormParcels_SetCurrentNewParcel(0);
end;

procedure TFormNewSubParcels.ActionXML_NewParcel_SetCurrentBackExecute(
  Sender: TObject);
begin
self.FormParcels_SetCurrentNewParcel(self.NewParcels_ComboBox.ItemIndex-1);
end;

procedure TFormNewSubParcels.ActionXML_NewParcel_SetCurrentNextExecute(
  Sender: TObject);
begin
self.FormParcels_SetCurrentNewParcel(self.NewParcels_ComboBox.ItemIndex+1);
end;

procedure TFormNewSubParcels.Action_NewParcels_SetCurrentExecute(Sender: TObject);
begin
 self.FormParcels_SetCurrentNewParcel(self.NewParcels_ComboBox.ItemIndex);
end;

procedure TFormNewSubParcels.ButtonGroup1Items5Click(Sender: TObject);
var difinition : string;
    currentNewPr : integer;
begin
  if not IsAssigned_FormParcels then exit;
  difinition := InputBox('�������� ������������ ��� ', '����������� ���','');
  if difinition = '' then Exit;
  currentNewPr:= NewParcels_ComboBox.ItemIndex;
  self.FXmlFormParcels.Items[NewParcels_ComboBox.ItemIndex].Definition :=  difinition;
  NewParcels_ComboBox.Items[currentNewPr] := difinition;
  self.FormParcels_SetCurrentNewParcel(currentNewPr); 
end;

procedure TFormNewSubParcels.cbbFormParcel_TypeChange(Sender: TObject);
var
  _node: IXMLNode;
  _curType: integer;
  _newType: integer;
begin
 if not self.FNewParcel_frame.IsAssigned_NewParcel then EXIT;
 _curType:= self.FNewParcel_frame.TypeNewParcel;
 _newType:= self.cbbFormParcel_Type.ItemIndex;
 if (_curType<>-1) and (_newType<>_curType) then
  if MessageDlg('��������� ���� ���������� ������� �������� � ������ ����� ��������� ������ '
                +'������������� �������� ��������������.' + sLineBreak +
                '���������� ���������� �������?', mtConfirmation, [mbYes, mbNo], -1) = mrNo then begin
    self.cbbFormParcel_Type.ItemIndex:= _curType;
    EXIT;
  end;
  self.FormParcel_doWrite_Type;
end;
constructor TFormNewSubParcels.Create(AOwner: TComponent);
begin
  inherited;
  self.jvPageControl_FP.ActivePage:= self.ts_NewParcels;
 //���������� ��
  self.FNewParcel_frame:= TFrameNewSubParcel.Create(self.ts_NewParcels);
  self.FNewParcel_frame.Parent:= self.ts_NewParcels;
  self.FNewParcel_frame.Align:= alClient;
  self.FNewParcel_frame.Visible:= False;
  self.FNewParcel_frame.SetActivTab(0,FALSE);

end;

function TFormNewSubParcels.IsAssigned_FormParcels: boolean;
begin
 Result:= self.FXmlFormParcels<>nil;
end;

procedure TFormNewSubParcels.medtFormParcel_CadastralBlockExit(Sender: TObject);
begin
 self.FormParcel_doWrite_CadastralBlock;
end;

procedure TFormNewSubParcels.medtFormParcel_CadastralBlockKeyUp(
                            Sender: TObject; var Key: Word; Shift: TShiftState);
begin
 if Key = VK_RETURN then
    if (Sender is TCustomEdit) then
      TCustomEdit(Sender).Parent.SetFocus;
end;

procedure TFormNewSubParcels.FormParcels_CreateNewParcel;
begin
 if self.IsAssigned_FormParcels then
 self.FormParcels_DoCreateNewParcel;
 self.FormParcels_DoSetCurrentNewParcel(self.NewParcels_ComboBox.Items.Count-1);
end;

procedure TFormNewSubParcels.FormParcels_Delete(index: Integer);
begin
 if (index>-1) and (index<self.NewParcels_ComboBox.Items.Count) then
  if index=self.NewParcels_ComboBox.ItemIndex then begin
   self.FormParcels_DoDelete(index);
   self.FormParcels_SetCurrentNewParcel(index);
  end else
   self.FormParcels_DoDelete(index);
end;

procedure TFormNewSubParcels.FormParcels_DoSetCurrentNewParcel(index: Integer);
begin
 self.FNewParcel_frame.XmlNewParcel:= IXMLTNewSubParcel(Pointer(self.NewParcels_ComboBox.Items.Objects[index]));
end;

procedure TFormNewSubParcels.FormParcels_RefreshList(_A: Integer=-1;_B:Integer=-1);
var 
 _NewParcel: IXMLTNewSubParcel;
 _i,_j: Integer;
begin
 if self.NewParcels_ComboBox.Items.Count = 0 then EXIT;

 if (_A<0) or (_a>=self.NewParcels_ComboBox.Items.Count)  then
  _a:= 0;

 if (_B<_A) or (_B>=self.NewParcels_ComboBox.Items.Count)  then
  _b:= self.NewParcels_ComboBox.Items.Count-1;
 _j:= self.NewParcels_ComboBox.ItemIndex;
 for _i := _a to _b do begin
  _NewParcel:= IXMLTNewSubParcel(pointer(self.NewParcels_ComboBox.Items.Objects[_i]));
  self.NewParcels_ComboBox.Items.Strings[_i]:= MsXml_ReadAttribute(_NewParcel,cnstAtrDefinition);
 end;
 self.NewParcels_ComboBox.ItemIndex:= _j;
end;

procedure TFormNewSubParcels.FormParcels_SetCurrentNewParcel(index: Integer);
begin
 if (index>-1) and (index<self.NewParcels_ComboBox.Items.Count) then begin
  self.NewParcels_ComboBox.ItemIndex:= index;
  self.FormParcels_DoSetCurrentNewParcel(index);
  self.FNewParcel_frame.Visible:= true;
  self.FormParcel_doRead_Type;
  self.FormParcel_doRead_CadastralBlock;

  self.cbbFormParcel_Type.Enabled:= true;
 end else 
 begin
   self.cbbFormParcel_Type.ItemIndex:=-1;
   self.cbbFormParcel_Type.Enabled:= false;
   self.NewParcels_ComboBox.ItemIndex:= -1;
   self.NewParcels_ComboBox.Text:= '';
   self.FNewParcel_frame.XmlNewParcel:= nil;
   self.FNewParcel_frame.Visible:= false;
 end;
end;

procedure TFormNewSubParcels.FormParcels_UpDateList;
var
 _i,_j: Integer;
 _NewParcel: IXMLTNewSubParcel;
begin
 _j:= self.NewParcels_ComboBox.ItemIndex;
 self.NewParcels_ComboBox.Clear;
 if self.IsAssigned_FormParcels then
  for _i := 0 to self.FXmlFormParcels.Count-1 do 
  begin
   _NewParcel:= self.FXmlFormParcels.Items[_i];
   self.NewParcels_ComboBox.Items.AddObject(_NewParcel.Definition,pointer(_NewParcel));
  end;
 if _j<0 then _j:= 0;
 self.FormParcels_SetCurrentNewParcel(_j);
end;

procedure TFormNewSubParcels.FormParcel_doRead_CadastralBlock;
begin
 if not self.FNewParcel_frame.IsAssigned_NewParcel then EXIT;
// self.medtFormParcel_CadastralBlock.Text :=  MsXml_ReadChildNodeValue(self.FNewParcel_frame.XmlNewParcel, 'CadastralBlock');
end;

procedure TFormNewSubParcels.FormParcel_doRead_Type;
begin
 if not self.FNewParcel_frame.IsAssigned_NewParcel then EXIT;
 self.cbbFormParcel_Type.ItemIndex:= self.FNewParcel_frame.TypeNewParcel;
end;

procedure TFormNewSubParcels.FormParcel_doWrite_CadastralBlock;
begin
 if not self.FNewParcel_frame.IsAssigned_NewParcel then EXIT;
// self.FNewParcel_frame.XmlNewParcel.CadastralBlock:=
//  StringReplace(self.medtFormParcel_CadastralBlock.Text,' ','',[rfReplaceAll,rfIgnoreCase]);
end;

procedure TFormNewSubParcels.FormParcel_doWrite_Type;
begin
 if not self.FNewParcel_frame.IsAssigned_NewParcel then EXIT;
 self.FNewParcel_frame.TypeNewParcel:= self.cbbFormParcel_Type.ItemIndex;
end;

procedure TFormNewSubParcels.SetFormParcels(const aFormParcels: IXMLTNewSubParcelList);
begin
 if self.FXmlFormParcels=aFormParcels then EXIT;
 self.FXmlFormParcels:= aFormParcels;
 self.FormParcels_UpDateList;
end;


procedure TFormNewSubParcels.FormParcels_DoCreateNewParcel;
var
 _NewParcel: IXMLTNewSubParcel;
 _st: string;
begin
 _NewParcel:= self.FXmlFormParcels.Add;
 _st:= cnstNewSubParcelDefinition +IntToStr(self.FXmlFormParcels.Count);
 _NewParcel.Definition := _st;
 _NewParcel.SubParcel_Realty := false;
 self.NewParcels_ComboBox.Items.AddObject(_st,pointer(_NewParcel));
end;

procedure TFormNewSubParcels.FormParcels_DoDelete(index: Integer);
var
 _NewParcel: IXMLTNewSubParcel;
begin
 _NewParcel:=  IXMLTNewSubParcel(Pointer(self.NewParcels_ComboBox.Items.Objects[index]));
 self.FXmlFormParcels.Remove(_NewParcel);
 self.NewParcels_ComboBox.Items.Delete(index);
 _NewParcel:= nil;
end;

end.

