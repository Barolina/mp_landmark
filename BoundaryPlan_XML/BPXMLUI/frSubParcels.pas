unit frSubParcels;
{
  SubParcels
    FormParcels
    ExistParcels
    InvariableParcels
    FormParcels for NewParcels

  For ExistEZParcel, ChangeParcel, ExistParcel, Newrcel
}
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ActnList, ButtonGroup, ExtCtrls,

  VCL.clipbrd,ImgList, System.Actions, Vcl.ComCtrls, Vcl.ToolWin, JvExComCtrls, JvToolBar,

  frSubparcel,
  STD_MP, XMLIntf,
  fmContainer, unRussLang, FrTypeParcel, FrPreloader, JvExStdCtrls, JvCombobox,
  JvExControls, JvGroupHeader, JvGroupBox, JvComponentBase, JvBalloonHint,
  JvgHint, JvHint;

type
  TFrameSubParcels = class(TFrame)
    actlst: TActionList;
    Contours_List_ListBox111: TListBox;
    actContrours_AddNew: TAction;
    actContours_DeleteCurrent: TAction;
    cbbContoursType: TComboBox;
    actClipBrd_Paste: TAction;
    actClipBrd_Copy: TAction;
    pnlControls: TPanel;
    btnEdCurContour: TButtonedEdit;
    actContour_next: TAction;
    actContour_prev: TAction;
    actValid: TAction;
    jvtlbr2: TJvToolBar;
    btnContour_prev1: TToolButton;
    btnContour_next1: TToolButton;
    btnContrours_AddNew1: TToolButton;
    btnContours_DeleteCurrent1: TToolButton;
    btnClipBrd_Copy1: TToolButton;
    btnClipBrd_Paste1: TToolButton;
    btnValid1: TToolButton;
    tlbTypeSubparcels: TToolBar;
    btnFormParcel: TToolButton;
    btnExitSubParcel: TToolButton;
    btnInvariableSubParcel: TToolButton;
    btn1: TToolButton;
    btnEntity_Spatial: TToolButton;
    btnEncumbrance: TToolButton;
    btnArea: TToolButton;
    Contours_List_ListBox: TJvComboBox;
    jvgrphdr1: TJvGroupHeader;
    jvblnhnt1: TJvBalloonHint;
    btnEdit: TToolButton;
    lblDefinitionSubParcel: TLabel;

    procedure actContrours_AddNewExecute(Sender: TObject);
    procedure actContours_DeleteCurrentExecute(Sender: TObject);
    procedure Action_Contours_SetCurrentExecute(Sender: TObject);
        procedure Contours_List_ListBox111MouseLeave(Sender: TObject);
    procedure btnEdCurContourRightButtonClick(Sender: TObject);
    procedure Contours_List_ListBox111KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure btnEdCurContourKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure btnEdCurContourExit(Sender: TObject);

    procedure cbbContoursTypeChange(Sender: TObject);
    procedure actClipBrd_CopyExecute(Sender: TObject);
    procedure actClipBrd_PasteExecute(Sender: TObject);
    procedure actContour_nextExecute(Sender: TObject);
    procedure actContour_prevExecute(Sender: TObject);
    procedure actValidExecute(Sender: TObject);
    procedure btnEdCurContourDblClick(Sender: TObject);
    procedure btnEntity_SpatialClick(Sender: TObject);
    procedure btnEncumbranceClick(Sender: TObject);
    procedure btnAreaClick(Sender: TObject);
    procedure tlbTypeSubparcelsClick(Sender: TObject);
  private
   { Private declarations }
   FXmlSubParcels     : IXMLTExistParcel_SubParcels;    //��������� �����
   FXmlNewSubparcels  : IXMLTNewParcel_SubParcels;      //����������� ����
                    
   FSubParcel_frame   : TFrameSubParcel;

   procedure SubParcelNewListDorefresh(A,B : Integer);
   procedure SubParcelExistListDorefresh(A,B : Integer);
   procedure SubParcelInvariableListDorefresh(A,B : Integer);
   procedure SubParcels_List_DoRefresh(A: Integer = -1; B: Integer = -1);

   procedure SubParcels_List_DoSetCurrentItem(IndexItem: integer); 

   procedure SubParcels_List_DoUpDate;
   procedure SubParcels_List_DoAddNewItem(iEntitySpatial : integer = -1); //Entity_Spatial ot Contours
   procedure SubParcels_List_DoDeleteItem(IndexItem: Integer);

   procedure OnChange_Attribute_DefinitionOrNumberPP(sender: Tobject);
   procedure _setSubParcelKind(aValue: TSubParcelKind);
  protected
  
   procedure InitializationUI;
   procedure SetSubParcels(const aContours: IXMLTExistParcel_SubParcels);
   procedure SetNewSubParcels(const aContours: IXMLTNewParcel_SubParcels);

   procedure SetActivSubParcelUIIndex(index: integer);
   function  GetActivSubParcelUIIndex: integer;

   procedure setActivSubParcelKind(aValue: TSubParcelKind);
   function  getActivSubParcelKind: TSubParcelKind;
  public
   { Public declarations }
   constructor Create(Owner: TComponent); override;

   function IsAssignedSubParcels: Boolean;

   procedure getAsText(aSubParcels: TSubParcelKind; var OutputText: string);
   procedure setAsText(aSubParcels: TSubParcelKind; InputText: string);

   procedure SubParcelsList_Show;
   procedure SubParcelsList_Hide;

   property ActivSubparcelUIIndex: integer read GetActivSubParcelUIIndex write SetActivSubParcelUIIndex;
   property ActivSubParcelKind: TSubParcelKind read getActivSubParcelKind write setActivSubParcelKind;

   property SubParcel: TFrameSubParcel read FSubParcel_frame;
   property XmlSubParcels: IXMLTExistParcel_SubParcels read FXmlSubParcels write SetSubParcels;
   property XmlNewSubParcels: IXMLTNewParcel_SubParcels read FxmlNewSubparcels write SetNewSubParcels;
  end;

implementation

uses MsXMLAPI, SurveyingPlan.DataFormat, SurveyingPlan.Xml.Utils,
     SurveyingPlan.Xml.Types, fmLog;

{$R *.dfm}

{ TFrameContours }

procedure TFrameSubParcels.actClipBrd_CopyExecute(Sender: TObject);
var
 _st: string;
begin
 self.getAsText(self.ActivSubParcelKind,_st);
 Clipboard.Open;
 TRY
  Clipboard.AsText:= _st;
 FINALLY
  Clipboard.Close;
 END;
end;

procedure TFrameSubParcels.actClipBrd_PasteExecute(Sender: TObject);
var
 _st: string;
begin
try
   TFrLoader.Inctance.RunAnimation := true;
   Clipboard.Open;
   TRY
   _st:= Clipboard.AsText;
   FINALLY
    Clipboard.Close;
   END;
   /// 
   /// ������������� ��� ����� ������ ���� ��� �� ����� ��������  � �����
   ///  �� �������� ������������ (����������) (��� ��� ��� �� �������� ���������)
   ///
   if Self.ActivSubParcelKind  =  cInvariableSubParcel then
   begin
     MessageDlg('������ ������� ���� ����� ���� �� ��������������', mtInformation,[mbOK],-1);   
     Exit;
   end; 
   if frmTypeParcel.GetTypeObject = 1  then 
   begin
     MessageDlg('�� ������ ������ �������������� ������ �� ��������������! ',mtInformation,[mbOK],-1);
     exit;
   end;
   self.setAsText(self.ActivSubParcelKind,_st)    
finally  
   TFrLoader.Inctance.RunAnimation := false;
end;
end;

procedure TFrameSubParcels.actContour_nextExecute(Sender: TObject);
begin
 self.ActivSubparcelUIIndex:= self.ActivSubparcelUIIndex+1;
end;

procedure TFrameSubParcels.actContour_prevExecute(Sender: TObject);
begin
self.ActivSubparcelUIIndex:= self.ActivSubparcelUIIndex-1;
end;

procedure TFrameSubParcels.actContours_DeleteCurrentExecute(Sender: TObject);
begin
 if not IsAssignedSubParcels then EXIT;
 if self.Contours_List_ListBox.ItemIndex=-1 then EXIT;
 if MessageDlg('������� ������: '+''+'?',mtInformation,mbYesNo,-1) = mrNo then Exit;


 self.SubParcels_List_DoDeleteItem(self.Contours_List_ListBox.ItemIndex);
 self.SetActivSubParcelUIIndex(self.Contours_List_ListBox.ItemIndex);
 if not self.FSubParcel_frame.IsAssignedSubParcel then
  self.SetActivSubParcelUIIndex(0);
end;

procedure TFrameSubParcels.Action_Contours_SetCurrentExecute(Sender: TObject);
begin
 self.ActivSubparcelUIIndex:= self.Contours_List_ListBox.ItemIndex;
end;

procedure TFrameSubParcels.actContrours_AddNewExecute(Sender: TObject);
begin
 if not self.IsAssignedSubParcels then EXIT;
 if self.ActivSubParcelKind = cUndefine then
 begin 
    MessageDlg('�� ������ �������� ��� �����! ',mtInformation,[mbOK],-1);
    Exit;
 end;

 if self.ActivSubParcelKind <>  cInvariableSubParcel    then  { ��� InvaribaleSubParcels �� ������������ ����� ���� ��������� }
   FSubParcel_frame.IsTypeLandSubParcel := frmTypeParcel.GetTypeObject
 else FSubParcel_frame.IsTypeLandSubParcel := -1;
 self.SubParcels_List_DoAddNewItem(FSubParcel_frame.IsTypeLandSubParcel);
 self.SetActivSubParcelUIIndex(self.Contours_List_ListBox.Items.Count-1);
end;

procedure TFrameSubParcels.actValidExecute(Sender: TObject);
var
 _st: string;
 _msubParcel: TSpMSubParcel;
 _log: TStringBuilder;
 _dlog: TformLog;
begin
 _msubParcel:= TSpMSubParcel.Create;

 case self.ActivSubParcelKind of
   cFormSubParcel:  TSpConvert.MSubParcel.Convert(FXmlSubParcels.FormSubParcel,_msubParcel);
   cExistSubParcel: TSpConvert.MSubParcel.Convert(FXmlSubParcels.ExistSubParcel,_msubParcel);
   cInvariableSubParcel: TSpConvert.MSubParcel.Convert(FXmlSubParcels.InvariableSubParcel,_msubParcel);
  // cNewFormSubParcel : TSpConvert.MSubParcel.Convert(FXmlNewSubParcels.FormSubParcel,_msubParcel);
 end;
 _log:= TStringBuilder.Create;
 _dlog:= TformLog.Create(nil);                                          
 if not TSpValidator.Validate(_msubParcel,_log) then
  _dlog.TextLog:= _log.ToString
 else
  _dlog.TextLog:= '������ �� ����������';
 _dlog.ShowModal;
 _log.Free;
 _dlog.Free;
end;

procedure TFrameSubParcels.btnAreaClick(Sender: TObject);
begin
   if not  self.IsAssignedSubParcels then Exit;
   if self.Contours_List_ListBox.Items.Count <= 0  then Exit;
   self.FSubParcel_frame.NewParcelsPageControl.ActivePage := self.FSubParcel_frame.tsArea;
   jvgrphdr1.Caption := '�����������/�������';
end;

procedure TFrameSubParcels.btnEdCurContourDblClick(Sender: TObject);
var
    nameZU : string;
    index  : integer;
begin
 nameZU:= InputBox('�������� ������� ����� ���/����������� ','������� ������� �����/�����������','');
 if nameZU = ''  then Exit;
 index := Self.Contours_List_ListBox.ItemIndex;
 if index = -1  then Exit;
 case Self.ActivSubParcelKind of
   cUndefine: ;
   cFormSubParcel: FXmlSubParcels.FormSubParcel.Items[index].Definition := nameZU;
   cExistSubParcel: FXmlSubParcels.ExistSubParcel.Items[index].Number_Record := StrToInt(nameZU);
   cInvariableSubParcel: FXmlSubParcels.InvariableSubParcel.Items[index].Number_Record := StrToInt(nameZU);
   cNewFormSubParcel: FXmlNewSubparcels.FormSubParcel[index].Definition := nameZU;
 end;
 Self.Contours_List_ListBox.Items[index] := nameZU;
 self.SetActivSubParcelUIIndex(index);
end;

procedure TFrameSubParcels.btnEdCurContourExit(Sender: TObject);
var
 _index: integer;
begin
 if TryStrToInt(self.btnEdCurContour.Text,_index) then
  self.ActivSubparcelUIIndex:= _index-1
 else
  self.ActivSubparcelUIIndex:= self.Contours_List_ListBox.ItemIndex;
end;

procedure TFrameSubParcels.btnEdCurContourKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if key = VK_RETURN then
  self.SetFocus;
end;

procedure TFrameSubParcels.btnEdCurContourRightButtonClick(Sender: TObject);
begin
 if self.Contours_List_ListBox.Visible then
  self.SubParcelsList_Hide
 else
  self.SubParcelsList_Show;
end;

procedure TFrameSubParcels.btnEncumbranceClick(Sender: TObject);
begin
   if not  self.IsAssignedSubParcels then Exit;
   if self.Contours_List_ListBox.Items.Count <= 0  then Exit;
   self.FSubParcel_frame.NewParcelsPageControl.ActivePage := Self.FSubParcel_frame.tsEncumbrance;
   jvgrphdr1.Caption := '';
   jvgrphdr1.Caption := '�������������� �����';
end;

procedure TFrameSubParcels.btnEntity_SpatialClick(Sender: TObject);
begin
   if not  self.IsAssignedSubParcels then Exit;
   if self.Contours_List_ListBox.Items.Count <= 0  then Exit;
   self.FSubParcel_frame.NewParcelsPageControl.ActivePage := self.FSubParcel_frame.tsEnSp_Attribut;
   jvgrphdr1.Caption := '������������ ��������';
end;

procedure TFrameSubParcels.SubParcelInvariableListDorefresh(A,
  B: Integer);
var
 _SubParcel: IXMLTExistParcel_SubParcels_InvariableSubParcel;
 _i,_j: Integer;
 _ItemName: string;
begin
 if self.Contours_List_ListBox.Items.Count = 0 then EXIT;
 if (A<0) or (a>=self.Contours_List_ListBox.Items.Count)  then a:= 0;

 if (B<A) or (B>=self.Contours_List_ListBox.Items.Count)  then  B:= self.Contours_List_ListBox.Items.Count-1;

 _j:= self.Contours_List_ListBox.ItemIndex;
 for _i := A to B do 
 begin
   _SubParcel := IXMLTExistParcel_SubParcels_InvariableSubParcel(pointer(self.Contours_List_ListBox.Items.Objects[_i]));
  _ItemName := MsXml_ReadAttribute(_SubParcel,cnstAtrNumber_Record);
  self.Contours_List_ListBox.Items.Strings[_i]:= _ItemName;
 end;
 self.Contours_List_ListBox.ItemIndex:= _j;
end;

procedure TFrameSubParcels.SubParcelExistListDorefresh(A, B: Integer);
var
 _SubPacel: IXMLTExistParcel_SubParcels_ExistSubParcel;
 _i,_j: Integer;
 _ItemName: string;
begin
 if self.Contours_List_ListBox.Items.Count = 0 then EXIT;
 if (A<0) or (a>=self.Contours_List_ListBox.Items.Count)  then  a:= 0;
 if (B<A) or (B>=self.Contours_List_ListBox.Items.Count)  then  B:= self.Contours_List_ListBox.Items.Count-1;

 _j:= self.Contours_List_ListBox.ItemIndex;
 for _i := A to B do 
 begin
  _SubPacel:= IXMLTExistParcel_SubParcels_ExistSubParcel(pointer(self.Contours_List_ListBox.Items.Objects[_i]));
  _ItemName:= MsXml_ReadAttribute(_SubPacel,cnstAtrNumber_Record);
  self.Contours_List_ListBox.Items.Strings[_i]:= _ItemName;
 end;
 self.Contours_List_ListBox.ItemIndex:= _j;
end;

procedure TFrameSubParcels.SubParcelNewListDorefresh(A,B : Integer);
var
 _SubParcel: IXMLTExistParcel_SubParcels_FormSubParcel;
 _i,_j: Integer;
 _ItemName: string;
begin
 if self.Contours_List_ListBox.Items.Count = 0 then EXIT;
 if (A<0) or (a>=self.Contours_List_ListBox.Items.Count)  then  a:= 0;
 if (B<A) or (B>=self.Contours_List_ListBox.Items.Count)  then  B:= self.Contours_List_ListBox.Items.Count-1;

 _j:= self.Contours_List_ListBox.ItemIndex;
 for _i := A to B do 
 begin
  _SubParcel:= IXMLTExistParcel_SubParcels_FormSubParcel(pointer(self.Contours_List_ListBox.Items.Objects[_i]));
 _ItemName:= MsXml_ReadAttribute(_SubParcel,cnstAtrDefinition);
  self.Contours_List_ListBox.Items.Strings[_i]:= _ItemName;
 end;
 self.Contours_List_ListBox.ItemIndex:= _j;
end;

procedure TFrameSubParcels.SubParcels_List_DoRefresh(A, B: Integer);
begin
  case self.ActivSubParcelKind of
    cFormSubParcel : SubParcelNewListDorefresh(A,B);
    cExistSubParcel : SubParcelExistListDorefresh(A,B);
    cInvariableSubParcel : SubParcelInvariableListDorefresh(A,B);
    cNewFormSubParcel : SubParcelNewListDorefresh(A,B);
  end;
end;

procedure TFrameSubParcels.SubParcels_List_DoSetCurrentItem(IndexItem: integer);
begin
  case self.ActivSubParcelKind of
    cFormSubParcel :
        self.FSubParcel_frame.XmlSubParcel:=(IXMLTExistParcel_SubParcels_FormSubParcel(pointer(self.Contours_List_ListBox.Items.Objects[IndexItem])) );
    cExistSubParcel : 
        self.FSubParcel_frame.XmlSubParcelExist:= (IXMLTExistParcel_SubParcels_ExistSubParcel(pointer(self.Contours_List_ListBox.Items.Objects[IndexItem])) );
    cInvariableSubParcel : 
         self.FSubParcel_frame.XmlSubParcelInvariable:= IXMLTExistParcel_SubParcels_InvariableSubParcel(pointer(self.Contours_List_ListBox.Items.Objects[IndexItem]));
    cNewFormSubParcel : 
        self.FSubParcel_frame.XmlNewParcelSubParcel:= IXMLTNewParcel_SubParcels_FormSubParcel(pointer(self.Contours_List_ListBox.Items.Objects[IndexItem]));
  end;
end;

procedure TFrameSubParcels.SetActivSubParcelUIIndex(index: integer);
begin
 if (index>-1) and (index<self.Contours_List_ListBox.Items.Count) then
 begin
    self.Contours_List_ListBox.ItemIndex:= index;
    self.SubParcels_List_DoSetCurrentItem(index);
    case self.ActivSubParcelKind of
      cFormSubParcel:  begin
                          self.btnEdCurContour.Text:= FXmlSubParcels.FormSubParcel[index].Definition;
                          self.lblDefinitionSubParcel.Caption := '����������� ����� ��: ';
      end;
      cExistSubParcel: begin
                            self.btnEdCurContour.Text:= IntToStr(FXmlSubParcels.ExistSubParcel[index].Number_Record);
                            self.lblDefinitionSubParcel.Caption := '������� ����� ����� ��: ';
      end;
      cInvariableSubParcel: begin
                                self.btnEdCurContour.Text:= IntToStr(FXmlSubParcels.InvariableSubParcel[index].Number_Record);
                                self.lblDefinitionSubParcel.Caption := '������� ����� ����� ��: ';
      end;
      cNewFormSubParcel : begin
                             self.btnEdCurContour.Text:= FXmlNewSubparcels.FormSubParcel[index].Definition;
                             self.lblDefinitionSubParcel.Caption := '�����������: '
      end;
    end;
    self.FSubParcel_frame.Visible:= True;
    self.btnEntity_Spatial.Down := (self.FSubParcel_frame.NewParcelsPageControl.ActivePage = Self.FSubParcel_frame.tsEnSp_Attribut);
    self.btnEncumbrance.Down := (self.FSubParcel_frame.NewParcelsPageControl.ActivePage = Self.FSubParcel_frame.tsEncumbrance);
    self.btnArea.Down := (self.FSubParcel_frame.NewParcelsPageControl.ActivePage = Self.FSubParcel_frame.tsArea);
    self.jvgrphdr1.Caption := self.FSubParcel_frame.NewParcelsPageControl.ActivePage.Hint;
    Self.jvgrphdr1.Visible := True;
 end else
 begin
    self.lblDefinitionSubParcel.Caption := '';
    self.jvgrphdr1.Visible  := false;
    self.FSubParcel_frame.Visible:= false;
    self.Contours_List_ListBox.ItemIndex:= -1;
    self.btnEdCurContour.Text:='';
    case self.ActivSubParcelKind of
      cFormSubParcel : self.FSubParcel_frame.XmlSubParcel:= nil;
      cExistSubParcel : self.FSubParcel_frame.XmlSubParcelExist := nil;
      cInvariableSubParcel : self.FSubParcel_frame.XmlSubParcelInvariable := nil;
      cNewFormSubParcel : self.FSubParcel_frame.XmlNewParcelSubParcel := nil;
    end;
 end;
end;

constructor TFrameSubParcels.Create(Owner: TComponent);
begin
 inherited;
 self.FSubParcel_frame:= TFrameSubParcel.Create(self);
 self.FSubParcel_frame.OnChange_Attribute_DefOnNum:= self.OnChange_Attribute_DefinitionOrNumberPP;
 self.FSubParcel_frame.Parent:= self;
 self.FSubParcel_frame.Align:= alClient;
 self.FSubParcel_frame.Visible:= False;
 self.tlbTypeSubparcels.Buttons[0].Down := true;
end;

function TFrameSubParcels.GetActivSubParcelUIIndex: integer;
begin
 Result:= self.Contours_List_ListBox.ItemIndex;
end;

procedure TFrameSubParcels.getAsText(aSubParcels: TSubParcelKind; var OutputText: string);
var
 _msubParcel: TSpMSubParcel;
begin
 _msubParcel:= TSpMSubParcel.Create;
 TRY
  case aSubParcels of
    cUndefine: ;
    cFormSubParcel: TSpConvert.MSubParcel.Convert(FXmlSubParcels.FormSubParcel,_msubParcel);
    cExistSubParcel: TSpConvert.MSubParcel.Convert(FXmlSubParcels.ExistSubParcel,_msubParcel);
    cInvariableSubParcel: TSpConvert.MSubParcel.Convert(FXmlSubParcels.InvariableSubParcel,_msubParcel);
//    cNewFormSubParcel: TSpConvert.MSubParcel.Convert(FXmlNewSubParcels.FormSubParcel,_msubParcel);
  end;
  OutputText:= DataFormat.MSubParcel.Text.Build(_msubParcel);
 FINALLY
  _msubParcel.Free;
 END;
end;

function TFrameSubParcels.getActivSubParcelKind: TSubParcelKind;
 function GetItemIndex() : Integer;
 var  i : Integer;
 begin
   Result := -1;
   for I := 0 to self.tlbTypeSubparcels.ButtonCount-1 do
     if tlbTypeSubparcels.Buttons[i].Down then Result := tlbTypeSubparcels.Buttons[i].Tag;
 end;
begin
   { ������ ����� ������ �������� � ����������� �� ExistParcel or NewParcels }
//   if self.cbbContoursType.ItemIndex in [0..cbbContoursType.Items.Count] then
//    Result:= TSubParcelKind(self.cbbContoursType.ItemIndex+1)
//   else
//    Result:= cUndefine;
   if not  Self.tlbTypeSubparcels.Visible  then //�� ���� - ������ ���� ������� -
     Result := cNewFormSubParcel
   else
   begin
       if GetItemIndex in [0..Self.tlbTypeSubparcels.ButtonCount-1] then
         Result := TSubParcelKind(GetItemIndex+1)
       else
         Result := cUndefine;
   end;
end;

procedure TFrameSubParcels.InitializationUI;
begin
 FSubParcel_frame.IsTypeLandSubParcel := 0; //����� ����� EntitySpatial
 self.Contours_List_ListBox.Clear;
 if self.IsAssignedSubParcels then
 begin
  { ���� NewParcels - ��������� � ������ ����� ������ (��� �������� ����������) }
//  if FXmlNewSubparcels <> nil then  cbbContoursType.Items.Add('���������� ����� �������!');  {��� ������������� FormParcels �� ExistParcels}
  if FXmlNewSubparcels <> nil  then tlbTypeSubparcels.Visible := False;

  self.SubParcels_List_DoUpDate;
  self.SetActivSubParcelUIIndex(0);
 end;
end;

function TFrameSubParcels.IsAssignedSubParcels: Boolean;
begin
 if FXmlSubParcels = nil  then
      Result := self.FXmlNewSubparcels <> nil
 else
      Result:= self.FXmlSubParcels<>nil;
end;

procedure TFrameSubParcels.OnChange_Attribute_DefinitionOrNumberPP(sender: tobject);
begin
 self.SubParcels_List_DoRefresh(self.Contours_List_ListBox.ItemIndex,self.Contours_List_ListBox.ItemIndex);
end;

procedure TFrameSubParcels.setAsText(aSubParcels: TSubParcelKind;InputText: string);
var
 _msubParcel: TSpMSubParcel;
 _log: TStringBuilder;
 _dlog: TformLog;
begin
 _msubParcel:= nil;
 _log:= TStringBuilder.Create;
 TRY
  _msubParcel:= DataFormat.MSubParcel.Text.Parse(InputText);
  _msubParcel._checkDirection;

  if not TSpValidator.Validate(_msubParcel,_log) then
  begin
    _dlog:= TformLog.Create(nil);
    _dlog.TextLog:= _log.ToString;
    FreeAndNil(_log);
    _dlog.ShowModal;
    if MessageDlg('������������ ��������� ������?',mtConfirmation,mbYesNo,-1) = mrNo then begin
     _dlog.Free;
     EXIT;
  end;
  _dlog.Free;
 end;
 FreeAndNil(_log);

  case aSubParcels of
    cUndefine: ;
    cFormSubParcel: TSpConvert.MSubParcel.Convert(_msubParcel,FXmlSubParcels.FormSubParcel);
    cExistSubParcel: TSpConvert.MSubParcel.Convert(_msubParcel,FXmlSubParcels.ExistSubParcel);
    cInvariableSubParcel: TSpConvert.MSubParcel.Convert(_msubParcel,FXmlSubParcels.InvariableSubParcel);
    cNewFormSubParcel: TSpConvert.MSubParcel.Convert(_msubParcel,FXmlNewSubParcels);
  end;

 FINALLY
  _msubParcel.Free;
  _log.Free;
 END;
 InitializationUI;
end;

procedure TFrameSubParcels.SetNewSubParcels(const aContours: IXMLTNewParcel_SubParcels);
begin
 if self.FxmlNewSubparcels=aContours then EXIT;
 self.SetActivSubParcelUIIndex(-1);
 self.FXmlNewSubparcels:= aContours;
 self.InitializationUI;
end;

procedure TFrameSubParcels.SetSubParcels(const aContours: IXMLTExistParcel_SubParcels);
begin
 if self.FXmlSubParcels=aContours then EXIT;
 self.SetActivSubParcelUIIndex(-1);
 self.FXmlSubParcels:= aContours;
 self.InitializationUI;
end;

procedure TFrameSubParcels.setActivSubParcelKind(aValue: TSubParcelKind);
begin
  self._setSubParcelKind(aValue);
  if self.IsAssignedSubParcels then
  begin
        self.FSubParcel_frame.ShowFrames(aValue, (FSubParcel_frame.IsTypeLandSubParcel = 0));
        self.FSubParcel_frame.AsAttributeSubParcel(true);
  end;
  self.SubParcels_List_DoUpDate;
end;

procedure TFrameSubParcels._setSubParcelKind(aValue: TSubParcelKind);
var i : Integer;
begin
// case aValue of
//   cUndefine: self.cbbContoursType.ItemIndex:=-1;
//   cFormSubParcel: self.cbbContoursType.ItemIndex:=0;
//   cExistSubParcel: self.cbbContoursType.ItemIndex:=1;
//   cInvariableSubParcel: self.cbbContoursType.ItemIndex:=2;
//   cNewFormSubParcel : Self.cbbContoursType.ItemIndex := 3;
// end;
   case aValue of
     cUndefine:begin
                 for I := 0 to tlbTypeSubparcels.ButtonCount-1 do
                     tlbTypeSubparcels.Buttons[i].Down := false;
     end;
     cFormSubParcel: self.tlbTypeSubparcels.Buttons[0].Down := True;
     cExistSubParcel: self.tlbTypeSubparcels.Buttons[1].Down := True;
     cInvariableSubParcel: self.tlbTypeSubparcels.Buttons[2].Down := True;
     cNewFormSubParcel : self.tlbTypeSubparcels.Visible := false;
   end;
end;

procedure TFrameSubParcels.cbbContoursTypeChange(Sender: TObject);
begin
  self.setActivSubParcelKind(self.getActivSubParcelKind);
end;

procedure TFrameSubParcels.SubParcelsList_Hide;
begin
 self.Contours_List_ListBox.Visible:= False;
end;

procedure TFrameSubParcels.SubParcelsList_Show;
begin
 self.Contours_List_ListBox.Parent:= nil;
 self.Contours_List_ListBox.Parent:= self;
 self.Contours_List_ListBox.Visible:= true;
 self.Contours_List_ListBox.SetFocus;
end;

procedure TFrameSubParcels.SubParcels_List_DoAddNewItem;
var
 _subPacel: IXMLTExistParcel_SubParcels_FormSubParcel;
 _subParcelExist : IXMLTExistParcel_SubParcels_ExistSubParcel;
 _subParcelDeleteAllBorder : IXMLTExistParcel_SubParcels_InvariableSubParcel;
 _subNewFormSubParcel : IXMLTNewParcel_SubParcels_FormSubParcel;
 _ItemName: string;
begin
  case self.ActivSubParcelKind  of
  cFormSubParcel:
  begin
     _subPacel:= FXmlSubParcels.FormSubParcel.Add;
     _ItemName:= IntToStr(FXmlSubParcels.FormSubParcel.Count);
     _subPacel.Definition := _ItemName;
     case iEntitySpatial  of
       0 :  _subPacel.Entity_Spatial.Spatial_Element.Add;
       1 :  _subPacel.Contours.Add;
     end;
     _subPacel.SubParcel_Realty := False;
     _subPacel.Definition:= _ItemName;
     self.Contours_List_ListBox.AddItem(_ItemName,pointer(_subPacel));
  end;
  cExistSubParcel:
  begin
     _subParcelExist:= FXmlSubParcels.ExistSubParcel.Add;
     _ItemName:= IntToStr(FXmlSubParcels.ExistSubParcel.Count);
     _subParcelExist.Number_Record := strtoint(_ItemName);
     case iEntitySpatial  of
       0 :  _subParcelExist.Entity_Spatial.Spatial_Element.Add;
       1 :  _subParcelExist.Contours.Add;
     end;
      _subParcelExist.Number_Record:= strtoint(_ItemName);
     self.Contours_List_ListBox.AddItem(_ItemName,pointer(_subParcelExist));
  end;
  cInvariableSubParcel:  begin
     _subParcelDeleteAllBorder:= FXmlSubParcels.InvariableSubParcel.Add;
     _ItemName:= IntToStr(FXmlSubParcels.InvariableSubParcel.Count);
     _subParcelDeleteAllBorder.Number_Record:= strtoint(_ItemName);
      self.Contours_List_ListBox.AddItem(_ItemName,pointer(_subParcelDeleteAllBorder));
  end;
  cNewFormSubParcel : begin
     _subNewFormSubParcel:= FXmlNewSubParcels.Add;
     _ItemName:= IntToStr(FXmlNewSubparcels.Count);
     _subNewFormSubParcel.Definition := _ItemName;
     case iEntitySpatial  of
       0 :  _subNewFormSubParcel.Entity_Spatial.Spatial_Element.Add;
       1 :  _subNewFormSubParcel.Contours.Add;
     end;
     _subNewFormSubParcel.SubParcel_Realty := False;
     _subNewFormSubParcel.Definition:= _ItemName;
     self.Contours_List_ListBox.AddItem(_ItemName,pointer(_subNewFormSubParcel));
  end;
  end;
end;


procedure TFrameSubParcels.SubParcels_List_DoDeleteItem(IndexItem: Integer);
var
 _subParcel: IXMLTExistParcel_SubParcels_FormSubParcel;
 _subParcelExist : IXMLTExistParcel_SubParcels_ExistSubParcel;
 _subParcelDeleteAllBorder : IXMLTExistParcel_SubParcels_InvariableSubParcel;
 _subNewFormSubParcel : IXMLTNewParcel_SubParcels_FormSubParcel;
 _index: Integer;
begin
 case self.ActivSubParcelKind of
 cFormSubParcel:
 begin
     _subParcel:= IXMLTExistParcel_SubParcels_FormSubParcel(pointer(self.Contours_List_ListBox.Items.Objects[IndexItem]));
     if self.FSubParcel_frame.XmlSubParcel=_subParcel then
      self.SetActivSubParcelUIIndex(IndexItem-1);
     self.Contours_List_ListBox.Items.Delete(IndexItem);
     FXmlSubParcels.FormSubParcel.Remove(_subParcel);
     _subParcel:= nil;
 end;
 cExistSubParcel:
 begin
     _subParcelExist:= IXMLTExistParcel_SubParcels_ExistSubParcel(pointer(self.Contours_List_ListBox.Items.Objects[IndexItem]));
     if self.FSubParcel_frame.XmlSubParcelExist=_subParcelExist then
      self.SetActivSubParcelUIIndex(IndexItem-1);
     self.Contours_List_ListBox.Items.Delete(IndexItem);
     FXmlSubParcels.ExistSubParcel.Remove(_subParcelExist);
     _subParcelExist:= nil;
 end;
 cInvariableSubParcel:
 begin
     _subParcelDeleteAllBorder:= IXMLTExistParcel_SubParcels_InvariableSubParcel(pointer(self.Contours_List_ListBox.Items.Objects[IndexItem]));
     if self.FSubParcel_frame.XmlSubParcelInvariable=_subParcelDeleteAllBorder then
      self.SetActivSubParcelUIIndex(IndexItem-1);
     self.Contours_List_ListBox.Items.Delete(IndexItem);
     FXmlSubParcels.InvariableSubParcel.Remove(_subParcelDeleteAllBorder);
     _subParcelDeleteAllBorder:= nil;
 end;
 cNewFormSubParcel :
 begin
     _subNewFormSubParcel:= IXMLTNewParcel_SubParcels_FormSubParcel(pointer(self.Contours_List_ListBox.Items.Objects[IndexItem]));
     if self.FSubParcel_frame.XmlNewParcelSubParcel=_subNewFormSubParcel then
      self.SetActivSubParcelUIIndex(IndexItem-1);
     self.Contours_List_ListBox.Items.Delete(IndexItem);
     FXmlNewSubparcels.Remove(_subNewFormSubParcel);
     _subNewFormSubParcel:= nil;
 end;
 end;
end;

procedure TFrameSubParcels.SubParcels_List_DoUpDate;
var
 _i, _index: Integer;
 _subParcel: IXMLTExistParcel_SubParcels_FormSubParcel;
 _subparcelExist : IXMLTExistParcel_SubParcels_ExistSubParcel;
 _subPacelDeleteAllBorder : IXMLTExistParcel_SubParcels_InvariableSubParcel;
 _subNewFormSubParcel : IXMLTNewParcel_SubParcels_FormSubParcel;
 _ItemName: string;
 subParcel_Realty : Boolean;
 xmlNode : IXMLNode;
 xmlNodeLst : IXMLNodeList;

 procedure  SetActivTypeSubParcelKind();
 begin
   if FXmlSubParcels <> nil then
   begin
     if self.ActivSubParcelKind = cUndefine then
         if FXmlSubParcels.ChildNodes.FindNode(cnstXmlFormSubParcel) <> nil  then
           self.ActivSubParcelKind := cFormSubParcel
         else
           if FXmlSubParcels.ChildNodes.FindNode(cnstXmlExistSubParcel) <> nil then
             self.ActivSubParcelKind := cExistSubParcel
           else
             if FXmlSubParcels.ChildNodes.FindNode(cnstXmlInvariableSubParcel) <> nil then
              self.ActivSubParcelKind := cInvariableSubParcel;
   end
    else
    begin
       if self.ActivSubParcelKind = cUndefine then
           if FXmlNewSubparcels.ChildNodes.FindNode(cnstXmlFormSubParcel) <> nil then
            self.ActivSubParcelKind := cNewFormSubParcel
    end;
 end;

begin
 SetActivTypeSubParcelKind;
 FSubParcel_frame.Visible := (self.ActivSubParcelKind <> cInvariableSubParcel);
 
 self.Contours_List_ListBox.Clear;
 case self.ActivSubParcelKind of
 cFormSubParcel:
       begin
           for _i := 0 to FXmlSubParcels.FormSubParcel.Count-1 do
           begin
            _subParcel:= FXmlSubParcels.FormSubParcel.Items[_i];
            _ItemName:= MsXml_ReadAttribute(_subParcel,cnstAtrDefinition);
            self.Contours_List_ListBox.AddItem(_ItemName,pointer(_subParcel));
           end;
           _index := FXmlSubParcels.FormSubParcel.Count-1;
       end;
 cExistSubParcel:
       begin
           for _i := 0 to FXmlSubParcels.ExistSubParcel.Count-1 do
           begin
            _subparcelExist:= FXmlSubParcels.ExistSubParcel.Items[_i];
           _ItemName:= MsXml_ReadAttribute(_subparcelExist,cnstAtrNumber_Record);
            self.Contours_List_ListBox.AddItem(_ItemName,pointer(_subparcelExist));
           end;
           _index := FXmlSubParcels.ExistSubParcel.Count -1;
       end;
 cInvariableSubParcel:
       begin
           for _i := 0 to FXmlSubParcels.InvariableSubParcel.Count-1 do begin
            _subPacelDeleteAllBorder:= FXmlSubParcels.InvariableSubParcel.Items[_i];
           _ItemName:= MsXml_ReadAttribute(_subPacelDeleteAllBorder,cnstAtrNumber_Record);
            self.Contours_List_ListBox.AddItem(_ItemName,pointer(_subPacelDeleteAllBorder));
           end;
           _index := FXmlSubParcels.InvariableSubParcel.Count-1;
       end;
cNewFormSubParcel :
      begin
          for _i := 0 to FXmlNewSubparcels.Count-1 do begin
            _subNewFormSubParcel:= FXmlNewSubParcels.FormSubParcel[_i];
            _ItemName:= MsXml_ReadAttribute(_subNewFormSubParcel,cnstAtrDefinition);
            self.Contours_List_ListBox.AddItem(_ItemName,pointer(_subNewFormSubParcel));
           end;
          _index := FXmlNewSubparcels.Count-1;
      end;
 end;
 self.SetActivSubParcelUIIndex(_index);
 if not self.FSubParcel_frame.IsAssignedSubParcel then
 self.SetActivSubParcelUIIndex(0);
end;

procedure TFrameSubParcels.tlbTypeSubparcelsClick(Sender: TObject);
begin
   self.setActivSubParcelKind(self.getActivSubParcelKind);
end;

procedure TFrameSubParcels.Contours_List_ListBox111KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
//  case key of
//   VK_ESCAPE: self.Contours_List_ListBox.Visible:= false;
//   VK_RETURN: self.Contours_List_ListBox.Visible:= false;
//  end;

end;

procedure TFrameSubParcels.Contours_List_ListBox111MouseLeave(Sender: TObject);
begin
// self.Contours_List_ListBox.Visible:= false;
end;
end.
