unit frSpecifyRelatedsParcel;

{
  TODO : SpecifyRelatedParcel
  ��������� ������ ������� ��������
}

interface
uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, Mask, ComCtrls,  Buttons, Grids,

  XMLDoc, XMLIntf, STD_MP, ActnList, ImgList, System.Actions, JvgPage,


  frChangeBorder,
  frExistSubparcels,frDeleteALLBorder,
  frALLBorders,frContours,unRussLang;


type
  TFrameSpecifyParcelrelated = class(TFrame)
    ExistSubParcels_TabSheet: TTabSheet;

    ActionManager_FormParcels: TActionList;
    ActionXML_Attributes_Write_Name: TAction;
    ActionXML_Attributes_Write_AdditionalName: TAction;
    ActionXML_Attributes_Write_Method: TAction;
    ActionXML_Attributes_Write_Definition: TAction;
    ActionXML_CadastralBlock_Write: TAction;
    ActionXML_Category_Write: TAction;
    Action_PrevCadNums_IsFill: TAction;
    Action_ProvPassCadNums_IsFil: TAction;
    Action_InnerCadNums_IsFill: TAction;
    Action_LocationAddress_IsFill: TAction;
    Action_NaturalObject_IsFill: TAction;
    Action_SubParcels_IsFill: TAction;
    Action_Encumbrance_IsFill: TAction;

    ImageList: TImageList;
    Contours_TabShet: TTabSheet;
    Change_BorderTabSheet: TTabSheet;
    DeleteAllBorderTabSheet: TTabSheet;
    AllBorderTabSheet: TTabSheet;
    Panel1: TPanel;
    NewParcelsPageControl: TJvgPageControl;
    pnl1: TPanel;

    procedure Attributes_Definition_EditKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Attributes_AdditionalName_EditKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure CadastralBlock_value_MaskEditKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);

  public type 
   TSpecifyRelatedKind = (cAllBorder, cChangeBorder, cContours,cDeleteAllBorder,cUndefine);

  private
    FXmlSpecifyRelatedParcel : IXMLTSpecifyRelatedParcel; 
    FDeleteAllBordee_        : IXMLTSpecifyRelatedParcel_DeleteAllBorder;
    FActivSpecifyRelated     : TSpecifyRelatedKind;

    FExistSubParcels         : TFrameExistSubParcels;
    FContoursExistParcel     : TFrameContours;
    FDeleteAllBorder         : TFrameDeleteAllBorder;
    FAllBorder               : TFrameAllBorders;
    FChangeBorder_Frame      : TFrameBorder;

    FonChangeTypeRelated     : TNotifyEvent;

    procedure InitializationUI; //���������������� ����������
    procedure ShowContourTabs;
    procedure ShowAllBorderTabs;
    procedure ShowDeleteAllBorderTabs;
    procedure ShowChangeBorderTand;

    procedure HideContourTabs;
    procedure HideAllBorderTabs;
    procedure HideDeleteAllBorderTabs;
    procedure HideChangeBorderTand;

    function  GetActivSpecifyRelatedKind : TSpecifyRelatedKind;
    procedure SetActivSpecifyRelatedKind(aKind : TSpecifyRelatedKind);
     
    //��������� �������
    procedure SetSpecifyRelatedParcel(const aNewParcel: IXMLTSpecifyRelatedParcel);
    function  IsAssigned_SpecifyRelatedParcel: Boolean;
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    property XmlSpecifyRelated: IXMLTSpecifyRelatedParcel read FXmlSpecifyRelatedParcel write SetSpecifyRelatedParcel;
    property ActivSpecifyRelatedKind : TSpecifyRelatedKind read GetActivSpecifyRelatedKind  write SetActivSpecifyRelatedKind;
    property OnChangeTypeRelated : TNotifyEvent  read FonChangeTypeRelated write FonChangeTypeRelated;
  end;


implementation

uses MsXMLAPI;



 procedure _HiddenChangeCheckBoxSate(ChBox: TCheckBox; NewSate: TCheckBoxState);
 var
  _OnClick: TNotifyEvent;
 begin
  if ChBox.State=NewSate then EXIT;
  _OnClick:= ChBox.OnClick;
  ChBox.OnClick:= nil;
  ChBox.State:= NewSate;
  ChBox.OnClick:= _OnClick;
 end;

{$R *.dfm}
{ TFrameSpecifyRelatedparcel }

procedure TFrameSpecifyParcelrelated.Attributes_AdditionalName_EditKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
if key = VK_RETURN then
  if (Sender is TCustomEdit) then
   TCustomEdit(Sender).Parent.SetFocus;
end;

procedure TFrameSpecifyParcelrelated.Attributes_Definition_EditKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
if key = VK_RETURN then
  if (Sender is TCustomEdit) then
   TCustomEdit(Sender).Parent.SetFocus;
end;

procedure TFrameSpecifyParcelrelated.CadastralBlock_value_MaskEditKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
if key = VK_RETURN then
  if (Sender is TCustomEdit) then
   TCustomEdit(Sender).Parent.SetFocus;
end;


constructor TFrameSpecifyParcelrelated.Create(AOwner: TComponent);
begin
  inherited;
  //No one type selected
  FActivSpecifyRelated := cUndefine;
  Self.FContoursExistParcel := TFrameContours.Create(Contours_TabShet);
  self.FContoursExistParcel.Parent := self.Contours_TabShet;
  self.FContoursExistParcel.Align := alClient;
  Self.FContoursExistParcel.ActivContourKind := TFrameContours.TContourKind.cNewContour;
  self.FContoursExistParcel.tlbTypeSubparcels.Visible := false;

  self.FExistSubParcels := TFrameExistSubParcels.Create(ExistSubParcels_TabSheet);
  self.FExistSubParcels.Parent  := self.ExistSubParcels_TabSheet;
  self.FExistSubParcels.Align  :=  alClient;
  self.FExistSubParcels.Visible := false;

  self.FAllBorder    := TFrameAllBorders.Create(Panel1);
  self.FAllBorder.Parent :=   self.Panel1;
  self.FAllBorder.Align := alClient;

  self.FDeleteAllBorder := TFrameDeleteAllBorder.Create(DeleteAllBorderTabSheet);
  self.FDeleteAllBorder.Parent  := self.DeleteAllBorderTabSheet;
  self.FDeleteAllBorder.Align  :=  alClient;

  self.FChangeBorder_Frame := TFrameBorder.Create(Change_BorderTabSheet);
  self.FChangeBorder_Frame.Parent := self.Change_BorderTabSheet;
  self.FChangeBorder_Frame.Align := alClient;

end;


procedure TFrameSpecifyParcelrelated.InitializationUI;
begin
 if self.IsAssigned_SpecifyRelatedParcel then
 begin
    self.NewParcelsPageControl.Visible:= TRUE;
    {���������� ��� ��������� ������ ������� ��������}
    if (FXmlSpecifyRelatedParcel.ChildNodes.FindNode(cnstXmlSpecifyRelatedParcel_Contours) <> nil )then
        FActivSpecifyRelated := cContours
    else
    if (FXmlSpecifyRelatedParcel.ChildNodes.FindNode(cnstXmlDeleteAllBorder) <> nil) then
        FActivSpecifyRelated := cDeleteAllBorder
    else
    if (FXmlSpecifyRelatedParcel.ChildNodes.FindNode(cnstXmlAllBorder)<>nil ) then
        FActivSpecifyRelated := cAllBorder
    else
    if (FXmlSpecifyRelatedParcel.ChildNodes.FindNode(cnstChangeBorder)<> nil)  then
       FActivSpecifyRelated := cChangeBorder;

//-- contour ----------------------------------------------------------------
   if(FActivSpecifyRelated = cContours) then
   begin
      self.FContoursExistParcel.XmlContours:= self.FXmlSpecifyRelatedParcel.Contours;
      ShowContourTabs;
   end
   else
   begin
      self.FContoursExistParcel.XmlContours:= nil;
      MsXml_RemoveChildNode(FXmlSpecifyRelatedParcel,cnstXmlContours);
      HideContourTabs;
   end;
//DeleteAllBorder -----------------------------------------------------------
   if (FActivSpecifyRelated = cDeleteAllBorder)  then
   begin
     if self.FXmlSpecifyRelatedParcel.DeleteAllBorder.Count <=0 then{��� �� ��� � ��� ChangeBorder}
     self.FDeleteAllBorder.XMLSpecifyRelatedParcel:= self.FXmlSpecifyRelatedParcel.DeleteAllBorder;
     ShowDeleteAllBorderTabs;
   end
   else
     begin
        FDeleteAllBorder.XMLSpecifyRelatedParcel := nil;
        MsXml_RemoveChildNode(FXmlSpecifyRelatedParcel,cnstXmlDeleteAllBorder);
        HideDeleteAllBorderTabs;
     end;
//ALlBorder  ----------------------------------------------------------------
   if (FActivSpecifyRelated = cAllBorder)   then
   begin
     self.FAllBorder.XmlAllBorder:= self.FXmlSpecifyRelatedParcel.AllBorder;
     ShowAllBorderTabs;
   end
   else
   begin
        self.FAllBorder.XmlAllBorder:= nil;
        MsXml_RemoveChildNode(FXmlSpecifyRelatedParcel,cnstXmlAllBorder);
        HideAllBorderTabs;
   end;
//ChangeBorder  -------------------------------------------------------------
   if (FActivSpecifyRelated = cChangeBorder) then
   begin
     {��� ������������ ����������� �� ������- ���� �� ��������� ������� ���������}
     if FXmlSpecifyRelatedParcel.ChangeBorder.Count <= 0  then  FXmlSpecifyRelatedParcel.ChangeBorder.Add;
     FChangeBorder_Frame.XMLChangeBorders := FXmlSpecifyRelatedParcel.ChangeBorder;
     ShowChangeBorderTand;
   end
   else
   begin
    // FXmlChangeBorder := nil;
     FChangeBorder_Frame.XMLChangeBorders := nil;
     HideChangeBorderTand;
     MsXml_RemoveChildNode(FXmlSpecifyRelatedParcel,cnstXmlChangeBprder);
   end;

 //--��� ������
 self.FExistSubParcels.XmlExistSubParcels := Self.FXmlSpecifyRelatedParcel.ExistSubParcels;
 FExistSubParcels.Visible := true;

 end else begin
  self.NewParcelsPageControl.Visible:= FALSE;

  self.FContoursExistParcel.XmlContours:= nil;
  self.FExistSubParcels.XmlExistSubParcels := nil;
  self.FDeleteAllBorder.XMLSpecifyRelatedParcel := nil;
  self.FDeleteAllBorder.XMLContour := nil;
  self.FAllBorder.XmlAllBorder := nil;
  self.FChangeBorder_Frame.XMLChangeBorders := nil;
  end;
  self.FonChangeTypeRelated(self);

end;

function TFrameSpecifyParcelrelated.IsAssigned_SpecifyRelatedParcel: Boolean;
begin
 Result:= self.FXmlSpecifyRelatedParcel<>nil;
end;


function TFrameSpecifyParcelrelated.GetActivSpecifyRelatedKind: TSpecifyRelatedKind;
begin
  Result := FActivSpecifyRelated;
end;

procedure TFrameSpecifyParcelrelated.HideAllBorderTabs;
begin
  // FAllBorder.Visible := false
  //AllBorderTabSheet.TabVisible := false;
end;

procedure TFrameSpecifyParcelrelated.HideChangeBorderTand;
begin
  //FChangeBorder_Frame.Visible := false;
  //Change_BorderTabSheet.TabVisible := false;
end;

procedure TFrameSpecifyParcelrelated.HideContourTabs;
begin
 //FContoursExistParcel.Visible := false;
 // Contours_TabShet.TabVisible := false;
end;

procedure TFrameSpecifyParcelrelated.HideDeleteAllBorderTabs;
begin
 // FDeleteAllBorder.Visible:= false;
// DeleteAllBorderTabSheet.TabVisible :=false;
end;

procedure TFrameSpecifyParcelrelated.SetActivSpecifyRelatedKind(aKind: TSpecifyRelatedKind);
begin
 FActivSpecifyRelated := aKind;
end;

procedure TFrameSpecifyParcelrelated.SetSpecifyRelatedParcel;
begin
 if aNewParcel = self.FXmlSpecifyRelatedParcel then Exit;
 self.FXmlSpecifyRelatedParcel:= aNewParcel;
 self.InitializationUI;
end;


procedure TFrameSpecifyParcelrelated.ShowAllBorderTabs;
begin
 FAllBorder.Visible := true;
 NewParcelsPageControl.ActivePage := AllBorderTabSheet;
 //AllBorderTabSheet.TabVisible := true;
end;

procedure TFrameSpecifyParcelrelated.ShowChangeBorderTand;
begin
// Change_BorderTabSheet.TabVisible := true;
 FChangeBorder_Frame.Visible := true;
 NewParcelsPageControl.ActivePage := Change_BorderTabSheet;
end;

procedure TFrameSpecifyParcelrelated.ShowContourTabs;
begin
 FContoursExistParcel.Visible := true;
 NewParcelsPageControl.ActivePage := Contours_TabShet;
 //Contours_TabShet.TabVisible := true;
end;

procedure TFrameSpecifyParcelrelated.ShowDeleteAllBorderTabs;
begin
  FDeleteAllBorder.Visible := true;
  NewParcelsPageControl.ActivePage:= DeleteAllBorderTabSheet;
//  DeleteAllBorderTabSheet.TabVisible := true;
end;

end.
