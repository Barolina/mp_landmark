object FrameArea: TFrameArea
  Left = 0
  Top = 0
  Width = 400
  Height = 55
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  ParentFont = False
  TabOrder = 0
  object AreaValue_Label_Caption: TLabel
    Left = 4
    Top = 7
    Width = 152
    Height = 13
    Caption = #1047#1085#1072#1095#1077#1085#1080#1077' '#1087#1083#1086#1097#1072#1076#1080' ('#1082#1074'.'#1084')'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Innccuracy_Label_Caption: TLabel
    Left = 172
    Top = 7
    Width = 155
    Height = 13
    Caption = #1055#1086#1075#1088#1077#1096#1085#1086#1089#1090#1100' '#1074#1099#1095#1080#1089#1083#1077#1085#1080#1103
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object AreaValue_MaskEdit: TMaskEdit
    Left = 3
    Top = 23
    Width = 153
    Height = 21
    AutoSize = False
    BevelKind = bkFlat
    BorderStyle = bsNone
    Color = 15921123
    ParentShowHint = False
    ShowHint = True
    TabOrder = 0
    Text = ''
    TextHint = #1047#1085#1072#1095#1077#1085#1080#1077' '#1087#1083#1086#1097#1072#1076#1080
    Touch.ParentTabletOptions = False
    Touch.TabletOptions = [toPressAndHold]
    OnExit = Action_AreaValue_WriteExecute
    OnKeyUp = AreaValue_MaskEditKeyUp
  end
  object Innccuracy_MaskEdit: TMaskEdit
    Left = 172
    Top = 23
    Width = 184
    Height = 21
    AutoSelect = False
    AutoSize = False
    BevelKind = bkFlat
    BorderStyle = bsNone
    Color = 15921123
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
    Text = ''
    TextHint = #1087#1086#1075#1088#1077#1096#1085#1086#1089#1090#1100
    StyleElements = []
    OnExit = Action_Innccuracy_WriteExecute
    OnKeyUp = AreaValue_MaskEditKeyUp
  end
  object ActionList: TActionList
    Left = 352
    Top = 8
    object Action_AreaValue_Write: TAction
      Caption = 'Action_AreaValue_Write'
      OnExecute = Action_AreaValue_WriteExecute
    end
    object Action_Innccuracy_Write: TAction
      Caption = 'Action_Innccuracy_Write'
      OnExecute = Action_Innccuracy_WriteExecute
    end
  end
end
