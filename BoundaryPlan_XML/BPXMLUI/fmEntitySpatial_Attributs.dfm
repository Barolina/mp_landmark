object FormEntitySpatial_Attributs: TFormEntitySpatial_Attributs
  Left = 0
  Top = 0
  BorderStyle = bsToolWindow
  Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1072' '#1072#1090#1088#1080#1073#1091#1090#1086#1074' '#1084#1077#1078#1077#1074#1099#1093' '#1090#1086#1095#1077#1082
  ClientHeight = 162
  ClientWidth = 314
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 149
    Top = 1
    Width = 8
    Height = 133
    Shape = bsLeftLine
  end
  object Bevel2: TBevel
    Left = 0
    Top = 64
    Width = 306
    Height = 9
    Shape = bsTopLine
  end
  object CheckBox_NumGeopoint: TCheckBox
    Left = 8
    Top = 5
    Width = 139
    Height = 17
    Caption = #1055#1088#1086#1080#1079#1074#1077#1089#1090#1080' '#1085#1091#1084#1077#1088#1072#1094#1080#1102
    TabOrder = 0
  end
  object LabeledEdit_NumGeopoint_firstindex: TLabeledEdit
    Left = 16
    Top = 37
    Width = 101
    Height = 21
    EditLabel.Width = 96
    EditLabel.Height = 13
    EditLabel.Caption = #1053#1072#1095#1072#1083#1100#1085#1099#1081' '#1080#1085#1076#1077#1082#1089
    NumbersOnly = True
    TabOrder = 1
    Text = '1'
  end
  object UpDown_NumGeopoint_firstindex: TUpDown
    Left = 117
    Top = 37
    Width = 15
    Height = 21
    Associate = LabeledEdit_NumGeopoint_firstindex
    Min = 1
    Max = 10000
    Position = 1
    TabOrder = 2
  end
  object CheckBox_PointPref: TCheckBox
    Left = 163
    Top = 5
    Width = 105
    Height = 17
    Caption = #1047#1072#1076#1072#1090#1100' '#1087#1088#1077#1092#1080#1082#1089
    TabOrder = 3
  end
  object LabeledEdit_PointPref_value: TLabeledEdit
    Left = 175
    Top = 37
    Width = 101
    Height = 21
    EditLabel.Width = 48
    EditLabel.Height = 13
    EditLabel.Caption = #1047#1085#1072#1095#1077#1085#1080#1077
    Enabled = False
    TabOrder = 4
    Text = #1085
  end
  object CheckBox_DeltaGeopoint: TCheckBox
    Left = 8
    Top = 69
    Width = 128
    Height = 17
    Caption = #1047#1072#1076#1072#1090#1100' '#1087#1086#1075#1088#1077#1096#1085#1086#1089#1090#1100
    TabOrder = 5
  end
  object LabeledEdit_DeltaGeopoint_Value: TLabeledEdit
    Left = 16
    Top = 103
    Width = 101
    Height = 21
    EditLabel.Width = 48
    EditLabel.Height = 13
    EditLabel.Caption = #1047#1085#1072#1095#1077#1085#1080#1077
    TabOrder = 6
    Text = '0.5'
  end
  object RadioGroup_ApplyFor: TRadioGroup
    Left = 158
    Top = 68
    Width = 148
    Height = 66
    Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100' '#1072#1090#1088#1080#1073#1091#1090#1099' '#1076#1083#1103
    ItemIndex = 0
    Items.Strings = (
      #1074#1099#1073#1088#1072#1085#1085#1086#1075#1086' '#1101#1083#1077#1084#1077#1085#1090#1072
      #1074#1089#1077#1093' '#1101#1083#1077#1084#1077#1090#1085#1086#1074)
    TabOrder = 7
  end
  object PanelBotControl: TPanel
    Left = 0
    Top = 140
    Width = 314
    Height = 22
    Align = alBottom
    TabOrder = 8
    object BitBtn_OK: TBitBtn
      Left = 163
      Top = 1
      Width = 75
      Height = 20
      Align = alRight
      Caption = #1054#1082
      ModalResult = 1
      TabOrder = 0
    end
    object BitBtn_Cancel: TBitBtn
      Left = 238
      Top = 1
      Width = 75
      Height = 20
      Align = alRight
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
    end
  end
end
