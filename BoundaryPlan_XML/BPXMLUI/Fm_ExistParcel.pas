unit Fm_ExistParcel;

//{$define newEditorEntitySpatial}
interface

uses
  Windows, Messages, SysUtils, Forms,
  Dialogs, StdCtrls, Menus,

  Mask,
  {$ifdef newEditorEntitySpatial}
    fmSpVCLEditor,
  {$else}
    frEntitySpatial,
  {$endif}
  {$ifdef newEditorEntitySpatial}   {$else} {$endif}

  frArea,
  STD_MP,
  XMLDoc,
  XMLIntf,


  frSubParcels,
  BpXML_different,
  BPCommon,
  frInnerCadastralNumbers,
  frAreaDelta,


  RibbonLunaStyleActnCtrls,
  Controls, System.Classes, ComCtrls, ExtCtrls,
  PlatformDefaultStyleActnCtrls, ActnList, ActnMan, Buttons,
  ImgList, ButtonGroup, System.Actions,
  frRelatedParcels,
  frContours,

  unRussLang, JvExStdCtrls, JvCheckBox, Data.Bind.EngExt, Vcl.Bind.DBEngExt,
  System.Rtti, System.Bindings.Outputs, Vcl.Bind.Editors, Data.Bind.Components,
  JvExControls, JvXPCore, JvXPCheckCtrls;

const
  // -- ���� ���������� �������
  cnstTypeEntity_Spatial = 0;
  cnstTypeContours = 1;

type
  TfrmExistParcel = class(TForm)
    pgcSpecifyParcels: TPageControl;
    tsExisParcel: TTabSheet;
    pnlExistParcel: TPanel;
    pnlNavigation: TPanel;
    SpeedButton_Nav_Back: TSpeedButton;
    SpeedButton_Nav_Next: TSpeedButton;
    cbbNav_GoTo: TComboBox;
    ProgressBar: TProgressBar;
    pgcExistParcel: TPageControl;
    tsArea: TTabSheet;
    tsSubparcels: TTabSheet;
    tsEntySpatial: TTabSheet;
    tsComposition_EZ: TTabSheet;
    lblArea: TLabel;
    cbArea: TCheckBox;
    cbInnerCadastralNumber: TCheckBox;
    lblInnerCadastralNumbers: TLabel;
    ActionManagerSpecifyParcel: TActionManager;
    Action_ExistParcels: TAction;
    Action_SpecifyRelatedParcels: TAction;
    ActionAddAtrExistParcel: TAction;
    pnlCadastralNumber: TPanel;
    lblListCadastralNUmbers: TLabel;
    Label2: TLabel;
    cbEnConturs: TCheckBox;
    Act_AddExistparcelCadastralNumber: TAction;
    pnllInnerCadastralNumber: TPanel;
    cbSubParcels: TCheckBox;
    lblSubparcel: TLabel;
    pgxOrdinates: TPageControl;
    tsContours: TTabSheet;
    tsEntitySpatial: TTabSheet;
    tsInnerCadastralNumber: TTabSheet;
    PopupMenu: TPopupMenu;
    PMItem_SlowlyChangeSlide: TMenuItem;
    PMItem_QuickChangeSlide: TMenuItem;
    Action1: TAction;
    edCadastralNumber: TMaskEdit;
    Label1: TLabel;
    meCadNumBlock_value: TMaskEdit;
    LabelName: TLabel;
    cbbAttributes_Name: TComboBox;
    pnlExistParcelAttribute: TPanel;
    tsfrRelatedParcels: TTabSheet;
    lblRelatedParcels: TLabel;
    chkRelatedParcels: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton_Nav_NextClick(Sender: TObject);
    procedure SpeedButton_Nav_BackClick(Sender: TObject);
    procedure cbbNav_GoToChange(Sender: TObject);
    procedure action_ExistParcelsExecute(Sender: TObject);
    procedure Action_SpecifyRelatedParcelsExecute(Sender: TObject);
    procedure cbSubParcelsClick(Sender: TObject);
    procedure cbbAttributes_NameChange(Sender: TObject);
    procedure meCadNumBlock_valueKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Label2Click(Sender: TObject);
    procedure lblInnerCadastralNumbersClick(Sender: TObject);
    procedure lblAreaClick(Sender: TObject);
    procedure lblSubparcelClick(Sender: TObject);
    procedure cbInnerCadastralNumberClick(Sender: TObject);
    procedure PMItem_SlowlyChangeSlideClick(Sender: TObject);
    procedure PMItem_QuickChangeSlideClick(Sender: TObject);
    procedure edCadastralNumberExit(Sender: TObject);
    procedure edCadastralNumberKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cbbAttributes_NameKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure meCadNumBlock_valueExit(Sender: TObject);
    procedure chkRelatedParcelsClick(Sender: TObject);
    procedure lblRelatedParcelsClick(Sender: TObject);
  private
    FXMLExist_Parcel        : IXMLTExistParcel;
    XMlPack                 : IXMLSTD_MP_Package;

    FArea_frame             : TFrameArea; // --> �������
    FArea_frame_delta       : TFrameAreaDelta; // - �������������� �������
    FInner_Cadast_Number    : TFrameInnerCadastralNumbers;
    FSubParcels_frame       : TFrameSubParcels; // --> �����
    {$ifdef newEditorEntitySpatial}
    FEntitySpatial         : TFormEntitySpatial; {$else}
    FEntitySpatial         : TFrameEntity_Spatial; // --> �������� ���������� �����������
    {$endif}
    FContoursExistParcel    : TFrameContours; // --> Conturs
    fRelatedParcels         : TFrameRelatedParcels;

    { Private declarations }
    procedure IsExistElementContours(var index: Integer);
    procedure IsExistElementEntity_Spatial(var index: Integer);

    procedure UI_Attributes_Name_Write;
    procedure InitializationUI;

    procedure UI_AttrExistparcel_WriteTo(const aExistParcel: IXMLTExistParcel);
    procedure UI_AttrExistParcel_ReadOf(const aExistParcel: IXMLTExistParcel);
    procedure UI_CadastralBlock_Write_To(const aExistParcel: IXMLTExistParcel);
    procedure UI_CadastralBlock_Read_To(const aExistParcel: IXMLTExistParcel);

    procedure SetSpecifyParcelsExistParcel(const aExistParcel: IXMLTExistParcel);

  public
    constructor Create(AOwner: TComponent); override;
    function IsAssigned_SpecifyParcels: Boolean;
    function ISAssigned_ExistPacel: Boolean;

    { Public declarations }
    property XmlSpecifyParcels_ExistParcels: IXMLTExistParcel
                                read FXMLExist_Parcel write SetSpecifyParcelsExistParcel;
    procedure SetActivTab(index: Integer; UseAnimate: Boolean = false);
    procedure SetPageControl(isType : TTypeMP);
  end;

var
  frmExistParcel: TfrmExistParcel;

implementation

uses MsXMLAPI;

{$R *.dfm}

procedure TfrmExistParcel.action_ExistParcelsExecute(Sender: TObject);
begin
  pgcSpecifyParcels.ActivePageIndex := 0;
end;

procedure TfrmExistParcel.Action_SpecifyRelatedParcelsExecute
  (Sender: TObject);
begin
  pgcSpecifyParcels.ActivePageIndex := 1;
end;

procedure TfrmExistParcel.IsExistElementContours(var index: Integer);
begin
  if (self.FXMLExist_Parcel.ChildNodes.FindNode(cnstXmlContours) <> nil) and
    (index <> cnstTypeContours) then
  begin
    if (self.FXMLExist_Parcel.Contours.ChildNodes.Count > 0) and
      (MessageDlg(rsErrorMsgTypeExist, mtInformation, [mbOK, mbCancel], -1) <>
      mrOk) then
      index := cnstTypeContours;
  end;
end;

procedure TfrmExistParcel.IsExistElementEntity_Spatial(var index: Integer);
begin
  if (FXMLExist_Parcel.ChildNodes.FindNode(cnstXmlEntity_Spatial) <> nil) and
    (index <> cnstTypeEntity_Spatial) then
  begin
    if (FXMLExist_Parcel.Entity_Spatial.ChildNodes.Count > 0) and
      (MessageDlg(rsErrorMsgTypeExist, mtInformation, [mbOK, mbCancel], -1) <>
      mrOk) then
      index := cnstTypeEntity_Spatial;
  end; // if
end;

function TfrmExistParcel.IsAssigned_SpecifyParcels: Boolean;
begin
  Result := FXMLExist_Parcel <> nil;
end;

{ ����� ����  ����������  ������� (������ ��� ��������������)}
procedure TfrmExistParcel.cbbAttributes_NameChange(Sender: TObject);
var
  _index: Integer;
begin
  if self.FXMLExist_Parcel = nil then
    raise Exception.Create(ExMSG_NotAssignedROOT);

  _index := self.cbbAttributes_Name.ItemIndex;
  self.IsExistElementContours(_index);
  self.IsExistElementEntity_Spatial(_index);

  self.cbbAttributes_Name.ItemIndex := _index;
  self.UI_Attributes_Name_Write
end;

procedure TfrmExistParcel.cbbAttributes_NameKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key = VK_RETURN then
    if (Sender is TComboBox) then
      TComboBox(Sender).Parent.SetFocus;
end;

procedure TfrmExistParcel.meCadNumBlock_valueExit(Sender: TObject);
begin
  if not self.ISAssigned_ExistPacel then
    raise Exception.Create(ExMSG_NotAssignedROOT);
  self.UI_CadastralBlock_Write_To(self.FXMLExist_Parcel);
end;

procedure TfrmExistParcel.meCadNumBlock_valueKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key = VK_RETURN then
    if (Sender is TCustomEdit) then
      TCustomEdit(Sender).Parent.SetFocus;
end;

procedure TfrmExistParcel.PMItem_QuickChangeSlideClick(Sender: TObject);
begin
  self.PMItem_SlowlyChangeSlide.Checked := false;
  self.PMItem_QuickChangeSlide.Checked := true;
end;

procedure TfrmExistParcel.PMItem_SlowlyChangeSlideClick(Sender: TObject);
begin
  self.PMItem_SlowlyChangeSlide.Checked := true;
  self.PMItem_QuickChangeSlide.Checked := false;
end;

procedure TfrmExistParcel.cbbNav_GoToChange(Sender: TObject);
begin
  self.SetActivTab(self.cbbNav_GoTo.ItemIndex,false);
end;

constructor TfrmExistParcel.Create(AOwner: TComponent);
begin
  inherited;

end;

procedure TfrmExistParcel.cbSubParcelsClick(Sender: TObject);
begin
  if not cbSubParcels.Checked then
  begin
    if self.FSubParcels_frame.XmlSubParcels <> nil then

       if MessageDlg(rsDeleteData+lblSubparcel.Caption+'?',mtInformation,mbYesNo,-1) = mrYes then
        begin
            MsXml_RemoveChildNode(self.FXMLExist_Parcel, cnstXmlSubParcels);
            self.FSubParcels_frame.XmlSubParcels := nil;
            self.FSubParcels_frame.Visible := false;
        end else cbSubParcels.Checked := true;
  end
  else
  begin
    if not FSubParcels_frame.Visible then
    begin
        self.FSubParcels_frame.XmlSubParcels := self.FXMLExist_Parcel.SubParcels;
        self.FSubParcels_frame.Visible := true;
    end;
  end
end;

procedure TfrmExistParcel.chkRelatedParcelsClick(Sender: TObject);
begin
  Try
    if not chkRelatedParcels.Checked then
    begin
      if fRelatedParcels.XmlParcelNeighbours <> nil then

      if MessageDlg(rsDeleteData+lblRelatedParcels.Caption+'?',mtInformation,mbYesNo,-1) = mrYes then
      begin
         MsXml_RemoveChildNode(self.FXMLExist_Parcel, cnstXmlRelatedParcels);
         self.fRelatedParcels.XmlParcelNeighbours := nil;
         self.fRelatedParcels.Visible := false;
      end else chkRelatedParcels.Checked := true;
    end
    else
    begin
      if not fRelatedParcels.Visible then
      begin
          self.fRelatedParcels.XmlParcelNeighbours := FXMLExist_Parcel.RelatedParcels;
          self.fRelatedParcels.Visible := true;
      end;
    end
  Except
    on E: Exception do raise E.Create('RelatedParcels');
  End;
end;

procedure TfrmExistParcel.cbInnerCadastralNumberClick(Sender: TObject);
begin
  Try
    if not cbInnerCadastralNumber.Checked then
    begin
     if self.FXMLExist_Parcel.ChildNodes.FindNode(cnstXmlInner_CadastralNumbers)<>nil then
       if MessageDlg(rsDeleteData+lblInnerCadastralNumbers.Caption+'?',mtInformation,mbYesNo,-1) = mrYes then
        begin
            MsXml_RemoveChildNode(self.FXMLExist_Parcel,cnstXmlInner_CadastralNumbers);
            self.FInner_Cadast_Number.XmlCadastralNumbers := nil;
            self.FInner_Cadast_Number.Visible := false;
        end else  cbInnerCadastralNumber.Checked := true;
    end
    else
    if not  FInner_Cadast_Number.Visible then
    begin
        self.FInner_Cadast_Number.XmlCadastralNumbers :=self.FXMLExist_Parcel.Inner_CadastralNumbers;
        self.FInner_Cadast_Number.Visible := true;
    end
  Except
    on E: Exception do
      MessageDlg('Inner not error visible!', mtInformation, [mbOK], -1);
  End;
end;

procedure TfrmExistParcel.FormCreate(Sender: TObject);
begin
  inherited;
  self.SetActivTab(0, false);
  // --����������� ������  �������� ������������ �������������
  self.FInner_Cadast_Number := TFrameInnerCadastralNumbers.Create
    (self.pnllInnerCadastralNumber);
  self.FInner_Cadast_Number.Parent := self.pnllInnerCadastralNumber;
  self.FInner_Cadast_Number.Align := alClient;
  self.FInner_Cadast_Number.Visible := false;
  // -- ������� ���������� �������
  self.FArea_frame := TFrameArea.Create(self.tsArea);
  self.FArea_frame.Parent := self.tsArea;
  self.FArea_frame.Align := alTop;
  // -- �������������� �������� ExistParcel
  self.FArea_frame_delta := TFrameAreaDelta.Create(self.tsArea);
  self.FArea_frame_delta.Parent := self.tsArea;
  self.FArea_frame_delta.Align := alClient;

  // -- ����� {}
  self.FSubParcels_frame := TFrameSubParcels.Create(self.tsSubparcels);
  self.FSubParcels_frame.Parent := self.tsSubparcels;
  self.FSubParcels_frame.Align := alClient;
  // -- �������
  self.FContoursExistParcel := TFrameContours.Create(self.tsContours);
  self.FContoursExistParcel.Parent := self.tsContours;
  self.FContoursExistParcel.Align := alClient;

  // --���������� �������������
  {$ifdef newEditorEntitySpatial}
   self.FEntitySpatial:= TFormEntitySpatial.Create(self.tsEntitySpatial);
   self.FEntitySpatial.Parent := self.tsEntitySpatial;
   self.FEntitySpatial.Align := alClient;
   self.FEntitySpatial.Show;
  {$else}
   self.FEntitySpatial := TFrameEntity_Spatial.Create(self.tsEntitySpatial);
   self.FEntitySpatial.Parent := self.tsEntitySpatial;
   self.FEntitySpatial.Align := alClient;
  {$endif}

  self.fRelatedParcels := TFrameRelatedParcels.Create(tsfrRelatedParcels);
  self.fRelatedParcels.Parent := tsfrRelatedParcels;
  self.fRelatedParcels.Align := alClient;

end;

{
  -- ������ ��� ��: ������ ��� ��������������
}
procedure TfrmExistParcel.UI_Attributes_Name_Write;
begin
  case self.cbbAttributes_Name.ItemIndex of
    cnstTypeEntity_Spatial:
      begin
        MsXml_RemoveChildNode(self.FXMLExist_Parcel, cnstXmlContours);
        MsXml_RemoveChildNode(self.FXMLExist_Parcel, cnstXmlComposition_EZ);
        self.FContoursExistParcel.XmlContours := nil;
        {$ifdef newEditorEntitySpatial}
        self.FEntitySpatial.LoadOfxml(self.FXMLExist_Parcel.Entity_Spatial);
        {$else}
        self.FEntitySpatial.EntitySpatial :=
          self.FXMLExist_Parcel.Entity_Spatial;
         {$endif}
        self.pgxOrdinates.ActivePage := self.tsEntitySpatial;
      end;
    cnstTypeContours:
      begin
        MsXml_RemoveChildNode(self.FXMLExist_Parcel, cnstXmlEntity_Spatial);
        MsXml_RemoveChildNode(self.FXMLExist_Parcel, cnstXmlComposition_EZ);
        {$ifdef newEditorEntitySpatial}
         self.FEntitySpatial.ClearALL;
        {$else}
         self.FEntitySpatial.EntitySpatial := nil;
        {$endif}
        self.FContoursExistParcel.XmlContours := self.FXMLExist_Parcel.Contours;
        self.pgxOrdinates.ActivePage := self.tsContours;
      end;
  else
    raise Exception.Create(format(ExMsg_Data_Invalid,
      [self.LabelName.Caption]));
  end
end;

procedure TfrmExistParcel.UI_CadastralBlock_Read_To(const aExistParcel
  : IXMLTExistParcel);
begin
  self.meCadNumBlock_value.Text := MsXml_ReadAttribute(aExistParcel,'CadastralBlock');
end;

procedure TfrmExistParcel.UI_CadastralBlock_Write_To(const aExistParcel
  : IXMLTExistParcel);
begin
  if self.meCadNumBlock_value.Text = '' then
    EXIT;
  aExistParcel.CadastralBlock := self.meCadNumBlock_value.Text;
end;

procedure TfrmExistParcel.UI_AttrExistParcel_ReadOf(const aExistParcel
  : IXMLTExistParcel);
begin
  self.edCadastralNumber.Text := MsXml_ReadAttribute(aExistParcel,
    'CadastralNumber');
end;

procedure TfrmExistParcel.UI_AttrExistparcel_WriteTo(const aExistParcel
  : IXMLTExistParcel);
var
  strKN: string;
begin
  if self.edCadastralNumber.Text = '' then
    EXIT;
  aExistParcel.CadastralNumber := self.edCadastralNumber.Text;
  aExistParcel.CadastralBlock := BPcCadNum_GetItemValue(edCadastralNumber.Text,
    [cnDistrict, cnMunicipality, cnBlock]);
  meCadNumBlock_value.Text := aExistParcel.CadastralBlock;
end;

{
  -- ���������
}
procedure TfrmExistParcel.SetActivTab(index: Integer;  UseAnimate: Boolean = false);
begin
  if (index < 0) or (index > self.pgcExistParcel.PageCount - 1) then
    EXIT;
  self.pgcExistParcel.ActivePageIndex := index;

  self.cbbNav_GoTo.ItemIndex := self.pgcExistParcel.ActivePageIndex;
  self.SpeedButton_Nav_Back.Enabled := self.cbbNav_GoTo.ItemIndex <> 0;
  self.SpeedButton_Nav_Next.Enabled := self.cbbNav_GoTo.ItemIndex <>
    self.cbbNav_GoTo.Items.Count - 1;
end;


procedure TfrmExistParcel.SetPageControl;
begin
  tsExisParcel.TabVisible := (isType = tpExistParcel);
end;

procedure TfrmExistParcel.InitializationUI;
var
  _node: IXMLNode;
begin
  cbbAttributes_Name.ItemIndex := -1;
  if FXMLExist_Parcel <> nil then
  begin
    pnlExistParcel.Visible := true;
    pnlNavigation.Visible := true;

    UI_AttrExistParcel_ReadOf(FXMLExist_Parcel);
    self.meCadNumBlock_value.Text := FXMLExist_Parcel.CadastralBlock;
    meCadNumBlock_value.Text := FXMLExist_Parcel.CadastralBlock;
    { xmlContounrs }
    _node := FXMLExist_Parcel.ChildNodes.FindNode(cnstXmlContours);
    if _node <> nil then
      cbbAttributes_Name.ItemIndex := cnstTypeContours;
    { xmlContours }
    _node := FXMLExist_Parcel.ChildNodes.FindNode(cnstXmlEntity_Spatial);
    if _node <> nil then
      cbbAttributes_Name.ItemIndex := cnstTypeEntity_Spatial;

     { CadastralINnner }
    _node := self.FXMLExist_Parcel.ChildNodes.FindNode(cnstXmlInner_CadastralNumbers);
    self.FInner_Cadast_Number.Visible := (_node <> nil);
    self.cbInnerCadastralNumber.Checked := (_node <> nil);
    self.FInner_Cadast_Number.XmlCadastralNumbers := (_node as IXMLTInner_CadastralNumbers);
     { area }
    self.FArea_frame.XMLArea := self.FXMLExist_Parcel.Area;
     { ������������ �������� ������� }
    self.FArea_frame_delta.XmlExistParcel := self.FXMLExist_Parcel;

    _node := self.FXMLExist_Parcel.ChildNodes.FindNode(cnstXmlSubParcels);
    self.FSubParcels_frame.Visible := (_node <>nil);
    self.cbSubParcels.Checked := (_node <>nil);
    self.FSubParcels_frame.XmlSubParcels := (_node as IXMLTExistParcel_SubParcels);

    self.pgxOrdinates.ActivePage := nil;
   _node := self.FXMLExist_Parcel.ChildNodes.FindNode(cnstXmlContours);
    if _node <> nil then
    begin
      self.FContoursExistParcel.XmlContours := self.FXMLExist_Parcel.Contours;
      self.pgxOrdinates.ActivePage := self.tsContours;
    end
    else
      self.FContoursExistParcel.XmlContours := nil;
    // --------------------------------------------------------------------------
    _node := self.FXMLExist_Parcel.ChildNodes.FindNode(cnstXmlEntity_Spatial);
    if _node <> nil then
    begin
      {$ifdef newEditorEntitySpatial}
        self.FEntitySpatial.LoadOfxml(self.FXMLExist_Parcel.Entity_Spatial);
      {$else}
        self.FEntitySpatial.EntitySpatial := self.FXMLExist_Parcel.Entity_Spatial;
      {$endif}
      self.pgxOrdinates.ActivePage := self.tsEntitySpatial;
    end
    else
     {$ifdef newEditorEntitySpatial}
      self.FEntitySpatial.ClearALL;
     {$else}
      self.FEntitySpatial.EntitySpatial := nil;
      {$endif}

    _node := self.FXMLExist_Parcel.ChildNodes.FindNode(cnstXmlRelatedParcels);
    self.chkRelatedParcels.Checked := (_node <>nil);
    self.fRelatedParcels.Visible := (_node <> nil);
    self.fRelatedParcels.XmlParcelNeighbours := (_node as IXMLRelatedParcels);
  end
  else
  begin
    pnlExistParcel.Visible := false;
    pnlNavigation.Visible := false;

    self.FArea_frame.XMLArea := nil;

    FArea_frame_delta.XmlExistParcel:=nil;
    FArea_frame_delta.XmlNewParcel:= nil;
    FArea_frame_delta.XmlExistEZParcel:= nil;
    FArea_frame_delta.XmlExistEZExistParcel:= nil;

    FInner_Cadast_Number.XmlCadastralNumbers:= nil;
    FInner_Cadast_Number.XmlCadastralNumbersExistEZ:= nil;
    FInner_Cadast_Number.XmlCadastralNumbersChange:= nil;

    self.FSubParcels_frame.XmlSubParcels := nil;

    self.FEntitySpatial.EntitySpatial := nil;

    self.FContoursExistParcel.XmlContours := nil;

    self.fRelatedParcels.XmlParcelNeighbours := nil;
    meCadNumBlock_value.Text := '';
  end;

end;

function TfrmExistParcel.ISAssigned_ExistPacel: Boolean;
begin
  Result := FXMLExist_Parcel <> nil;
end;

procedure TfrmExistParcel.Label2Click(Sender: TObject);
begin
  cbEnConturs.Checked := not cbEnConturs.Checked;
end;

procedure TfrmExistParcel.lblAreaClick(Sender: TObject);
begin
  cbArea.Checked := not cbArea.Checked;
end;

procedure TfrmExistParcel.lblSubparcelClick(Sender: TObject);
begin
  cbSubParcels.Checked := not cbSubParcels.Checked;
end;

procedure TfrmExistParcel.edCadastralNumberExit(Sender: TObject);
begin
  if not self.ISAssigned_ExistPacel then
    raise Exception.Create(ExMSG_NotAssignedROOT);
  self.UI_AttrExistparcel_WriteTo(self.FXMLExist_Parcel);
end;

procedure TfrmExistParcel.edCadastralNumberKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key = VK_RETURN then
    if (Sender is TMaskEdit) then
      TMaskEdit(Sender).Parent.SetFocus;
end;


procedure TfrmExistParcel.lblInnerCadastralNumbersClick(Sender: TObject);
begin
  cbInnerCadastralNumber.Checked := not cbInnerCadastralNumber.Checked;
end;

procedure TfrmExistParcel.lblRelatedParcelsClick(Sender: TObject);
begin
  chkRelatedParcels.Checked := not chkRelatedParcels.Checked;
end;

procedure TfrmExistParcel.SetSpecifyParcelsExistParcel(
  const aExistParcel: IXMLTExistParcel);
begin
 if self.FXMLExist_Parcel = aExistParcel then  EXIT;
  self.FXMLExist_Parcel := aExistParcel;
  self.InitializationUI;
  self.pgcExistParcel.ActivePage := tsInnerCadastralNumber;
end;


procedure TfrmExistParcel.SpeedButton_Nav_BackClick(Sender: TObject);
begin
  self.SetActivTab(self.cbbNav_GoTo.ItemIndex - 1,false);
end;

procedure TfrmExistParcel.SpeedButton_Nav_NextClick(Sender: TObject);
begin
  self.SetActivTab(self.cbbNav_GoTo.ItemIndex + 1, false);
end;

end.
