object FrameInputDateSubParcels: TFrameInputDateSubParcels
  Left = 0
  Top = 0
  Width = 951
  Height = 495
  TabOrder = 0
  object Panel: TPanel
    Left = 0
    Top = 25
    Width = 951
    Height = 470
    Align = alClient
    BevelEdges = [beRight, beBottom]
    BevelOuter = bvNone
    TabOrder = 0
    object stringGridDate: TStringGrid
      AlignWithMargins = True
      Left = 3
      Top = 3
      Width = 945
      Height = 464
      HelpType = htKeyword
      Align = alClient
      BevelEdges = []
      BevelInner = bvNone
      BevelKind = bkTile
      BevelOuter = bvNone
      BorderStyle = bsNone
      Color = 13750737
      ColCount = 3
      DefaultColWidth = 217
      DrawingStyle = gdsGradient
      FixedColor = 15719625
      FixedCols = 0
      RowCount = 1
      FixedRows = 0
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Font.Quality = fqClearType
      GradientEndColor = clBlack
      GradientStartColor = clSkyBlue
      Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goDrawFocusSelected, goRowSizing, goColSizing, goEditing, goTabs]
      ParentFont = False
      PopupMenu = poMenuTable
      TabOrder = 0
      OnKeyUp = stringGridDateKeyUp
      OnSetEditText = stringGridDateSetEditText
    end
  end
  object pnlLabelTabl: TPanel
    Left = 0
    Top = 0
    Width = 951
    Height = 25
    Align = alTop
    BevelEdges = [beLeft, beTop, beRight]
    BevelKind = bkFlat
    BevelOuter = bvNone
    Color = 15790320
    ParentBackground = False
    TabOrder = 1
    object lblCadNum: TLabel
      Left = 43
      Top = 0
      Width = 318
      Height = 23
      Align = alLeft
      Alignment = taCenter
      AutoSize = False
      Caption = #1050#1072#1076#1072#1089#1090#1088#1086#1074#1099#1081' '#1085#1086#1084#1077#1088' '#1047#1059
      Color = 15790320
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      Font.Quality = fqClearType
      ParentColor = False
      ParentFont = False
      Layout = tlCenter
      WordWrap = True
      ExplicitLeft = 36
      ExplicitTop = 24
      ExplicitHeight = 25
    end
    object lblNumber_Record: TLabel
      Left = 363
      Top = 0
      Width = 469
      Height = 23
      Align = alLeft
      Alignment = taCenter
      AutoSize = False
      Caption = #1059#1095#1077#1090#1085#1099#1077' '#1085#1086#1084#1077#1088#1072' '#1095#1072#1089#1090#1077#1081' '#1079#1077#1084#1077#1083#1100#1085#1086#1075#1086' '#1091#1095#1072#1089#1090#1082#1072
      Color = 15790320
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      Font.Quality = fqClearType
      ParentColor = False
      ParentFont = False
      Layout = tlCenter
      WordWrap = True
      ExplicitLeft = 367
      ExplicitTop = -1
      ExplicitHeight = 21
    end
    object splCadNUm: TSplitter
      Left = 361
      Top = 0
      Width = 2
      Height = 23
      Color = clBtnText
      ParentColor = False
      OnMoved = splCadNUmMoved
      ExplicitLeft = 368
      ExplicitHeight = 25
    end
    object splNumberRecord: TSplitter
      Left = 41
      Top = 0
      Width = 2
      Height = 23
      Color = clDefault
      ParentColor = False
      OnMoved = splNumberRecordMoved
      ExplicitHeight = 25
    end
    object lblN: TLabel
      AlignWithMargins = True
      Left = 3
      Top = 3
      Width = 35
      Height = 17
      Align = alLeft
      Alignment = taCenter
      AutoSize = False
      Caption = #8470
      Color = 15790320
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      Font.Quality = fqClearType
      ParentColor = False
      ParentFont = False
      Layout = tlCenter
      WordWrap = True
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitHeight = 49
    end
    object spl4: TSplitter
      Left = 832
      Top = 0
      Width = 1
      Height = 23
      Color = clBtnText
      ParentColor = False
      ExplicitLeft = 35
      ExplicitHeight = 49
    end
  end
  object poMenuTable: TPopupMenu
    Left = 224
    Top = 120
    object N1: TMenuItem
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1089#1090#1088#1086#1082#1091
      ShortCut = 16429
      OnClick = SubParcels_Actions_ButtonGroupItems0Click
    end
    object N2: TMenuItem
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1089#1090#1088#1086#1082#1091' '
      ShortCut = 16430
      OnClick = SubParcels_Actions_ButtonGroupItems1Click
    end
  end
end
