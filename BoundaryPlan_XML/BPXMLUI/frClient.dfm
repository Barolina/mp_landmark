object FrameClient: TFrameClient
  Left = 0
  Top = 0
  Width = 708
  Height = 272
  TabOrder = 0
  object ToolBarClient: TToolBar
    AlignWithMargins = True
    Left = 3
    Top = 44
    Width = 702
    Height = 21
    AutoSize = True
    ButtonHeight = 21
    ButtonWidth = 185
    DrawingStyle = dsGradient
    EdgeInner = esNone
    EdgeOuter = esNone
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clBlue
    Font.Height = -12
    Font.Name = 'Arial'
    Font.Style = [fsUnderline]
    List = True
    GradientDirection = gdHorizontal
    ParentFont = False
    ParentShowHint = False
    ShowCaptions = True
    ShowHint = False
    TabOrder = 0
    StyleElements = []
    OnClick = ToolBarClientClick
    object ToolButPerson: TToolButton
      Left = 0
      Top = 0
      AutoSize = True
      Caption = #1060#1080#1079#1080#1095#1077#1089#1082#1086#1077' '#1083#1080#1094#1086
      Grouped = True
      ImageIndex = 0
      Style = tbsCheck
      OnClick = ToolBarClientClick
    end
    object ToolButRusshOrg: TToolButton
      Left = 115
      Top = 0
      AutoSize = True
      Caption = #1056#1086#1089#1089#1080#1081#1089#1082#1086#1077'  '#1102#1088'.'#1083#1080#1094#1086
      Grouped = True
      ImageIndex = 1
      Style = tbsCheck
      OnClick = ToolBarClientClick
    end
    object ToolButInostrOrg: TToolButton
      Left = 250
      Top = 0
      AutoSize = True
      Caption = #1048#1085#1086#1089#1090#1088#1072#1085#1085#1086#1077' '#1102#1088'.'#1083#1080#1094#1086
      Grouped = True
      ImageIndex = 2
      Style = tbsCheck
      OnClick = ToolBarClientClick
    end
    object ToolButGovernance: TToolButton
      Left = 392
      Top = 0
      AutoSize = True
      Caption = #1054#1088#1075#1072#1085' '#1075#1086#1089#1091#1076#1072#1088#1089#1090#1074#1077#1085#1085#1086#1081' '#1074#1083#1072#1089#1090#1080
      Grouped = True
      ImageIndex = 3
      Style = tbsCheck
      OnClick = ToolBarClientClick
    end
  end
  object pnlData: TPanel
    Left = 0
    Top = 0
    Width = 708
    Height = 41
    Align = alTop
    BevelOuter = bvNone
    ParentColor = True
    TabOrder = 1
    object Label1: TLabel
      Left = 11
      Top = 18
      Width = 128
      Height = 13
      Caption = #1044#1072#1090#1072' '#1087#1088#1080#1077#1084#1082#1080' '#1088#1072#1073#1086#1090' '
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object jvdtpckrdtDate: TJvDatePickerEdit
      Left = 143
      Top = 12
      Width = 113
      Height = 21
      AllowNoDate = True
      BorderStyle = bsNone
      Checked = True
      Color = 15921123
      DateFormat = 'yyyy-MM-dd'
      DateSeparator = '-'
      BevelKind = bkFlat
      StoreDateFormat = True
      TabOrder = 0
      OnExit = jvdtpckrdtDateExit
    end
  end
end
