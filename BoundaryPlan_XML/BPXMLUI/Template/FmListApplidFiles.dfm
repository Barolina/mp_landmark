object FrmListAppliedFiles: TFrmListAppliedFiles
  Left = 360
  Top = 169
  BorderIcons = []
  BorderStyle = bsToolWindow
  Caption = 'FrmListAppliedFiles'
  ClientHeight = 490
  ClientWidth = 966
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object lstAplpiedFiles: TValueListEditor
    Left = 0
    Top = 0
    Width = 966
    Height = 464
    Align = alClient
    BorderStyle = bsNone
    DropDownRows = 15
    TabOrder = 0
    TitleCaptions.Strings = (
      #1053#1072#1080#1084#1077#1085#1086#1074#1072#1080#1077' '#1088#1072#1079#1076#1077#1083#1072
      #1060#1072#1081#1083)
    OnDrawCell = lstAplpiedFilesDrawCell
    ExplicitTop = -4
    ColWidths = (
      150
      814)
  end
  object pnlClose: TPanel
    Left = 0
    Top = 464
    Width = 966
    Height = 26
    Align = alBottom
    BevelEdges = [beTop]
    BevelKind = bkFlat
    BevelOuter = bvNone
    TabOrder = 1
    object btnCloe: TButton
      Left = 871
      Top = 0
      Width = 95
      Height = 24
      Align = alRight
      BiDiMode = bdLeftToRight
      Caption = 'ok'
      Default = True
      ModalResult = 1
      ParentBiDiMode = False
      TabOrder = 0
      WordWrap = True
      ExplicitLeft = 860
      ExplicitTop = 1
    end
  end
end
