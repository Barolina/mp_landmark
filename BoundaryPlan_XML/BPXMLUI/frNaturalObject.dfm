object FrameNaturalObject: TFrameNaturalObject
  Left = 0
  Top = 0
  Width = 700
  Height = 105
  TabOrder = 0
  object ForestUse_Label_Caption: TLabel
    Left = 15
    Top = 4
    Width = 138
    Height = 13
    Caption = #1062#1077#1083#1077#1074#1086#1077' '#1085#1072#1079#1085#1072#1095#1077#1085#1080#1077' '#1083#1077#1089#1086#1074
  end
  object Label2: TLabel
    Left = 15
    Top = 52
    Width = 141
    Height = 13
    Caption = #1050#1072#1090#1077#1075#1086#1088#1080#1103' '#1079#1072#1097#1080#1090#1085#1099#1093' '#1083#1077#1089#1086#1074
  end
  object ForestUse_ComboBox: TComboBox
    Left = 15
    Top = 23
    Width = 289
    Height = 22
    BevelKind = bkFlat
    Style = csOwnerDrawFixed
    Color = clWhite
    TabOrder = 0
    TextHint = #1042#1099#1073#1080#1088#1080#1090#1077' '#1086#1076#1080#1085' '#1080#1079' '#1074#1072#1088#1080#1072#1085#1090#1086#1074
    OnChange = Action_ForestUse_writeExecute
  end
  object TypeProtectiveForest_Edit: TEdit
    Left = 15
    Top = 69
    Width = 674
    Height = 21
    BevelKind = bkFlat
    BorderStyle = bsNone
    Color = clWhite
    TabOrder = 1
    OnExit = Action_TypeProtectiveForest_writeExecute
    OnKeyUp = TypeProtectiveForest_EditKeyUp
  end
  object ActionList: TActionList
    Left = 656
    Top = 56
    object Action_ForestUse_write: TAction
      Caption = 'Action_ForestUse_write'
      OnExecute = Action_ForestUse_writeExecute
    end
    object Action_TypeProtectiveForest_write: TAction
      Caption = 'Action_TypeProtectiveForest_write'
      OnExecute = Action_TypeProtectiveForest_writeExecute
    end
  end
end
