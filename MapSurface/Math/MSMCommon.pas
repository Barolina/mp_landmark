{*******************************************************}
{      |----------------------------------------|       }
{      |-(c) 2009-2010 ������������ ����������.-|       }
{      |-        ���������� ���������.         -|       }
{      |-          kras_ai@mail.ru             -|       }
{      |----------------------------------------|       }
{*******************************************************}
{
 ������ ������ ����� �������� � ���� �������������� ��� ������
 ��� ��������� ������������ �� ���������� MsMath. ������ ����������
 ���� ������� ���-�� � ������� �� ��������� �������:
 1.MapProcs.pas
 2.msApprox.pas
 3.Mscommon.pas
 4.MSCONV.PAS
 5.MSELL.PAS
 6.Msfilter.pas
 7.msMath.pas
 8.MSMATHEX.PAS
 9.MSMEMMAN.PAS
 10.MSNEW.PAS
 11.MSPROC.PAS
 12.MSTRACE.PAS
 13.MsTypes.pas
}
unit MSMCommon;
interface
uses
 MSMCommon_;
function msmcLineAngle( Sx,Sy,Ex,Ey: Double ) : double;
function msmcAngleBetween(S1x,S1y,E1x,E1y,S2x,S2y,E2x,E2y: double) : double;
procedure msmcRotatePoint(Cx,Cy,Px,Py : Double; angle : double;var ResX,ResY: Double);
implementation
uses
  MsType;
const
  PiDiv180  = Pi / 180;
procedure msmcRotatePoint( Cx,Cy,Px,Py : Double; angle : double; var ResX,ResY: Double);
var
 x,y,SinA,CosA :double;
begin
 Angle:= angle*pi/180;
 SinA := SIN( Angle );
 CosA := COS( Angle );
 Resx := (PX-CX)*CosA - (PY-CY)*SinA + CX;
 Resy := (PX-CX)*SinA + (PY-CY)*CosA + CY;
end; { mrRotatePoint }
function msmcAngleBetween( S1x,S1y,E1x,E1y,S2x,S2y,E2x,E2y: double) : double;
var a : double;
begin
 a := msmcLineAngle(S2x,S2y,E2x,E2y);
 a := a - msmcLineAngle(S1x,S1y,E1x,E1y);
 if msmcIsGE(a,0) then
  Result := a
 else
  Result := a + 360.0;
end; { mrAngleBetween }
function msmcLineAngle( Sx,Sy,Ex,Ey: Double ) : double;
begin
   if msmcisEqual(SX , EX ) then begin
      if SY < EY
         then Result := 90.0
         else Result := 270.0
   end else begin
      Result := ARCTAN( (EY - SY) / (EX - SX) ) / PiDiv180;
      if SX < EX then
         if msmcisLE( SY , EY)
            then
            else Result := Result + 360.0
      else
         Result := Result + 180.0;
   end;
end;  { mrLineAngle }
end.
