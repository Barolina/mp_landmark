<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="text" version="1.0" encoding="UTF-8" indent="yes"/>
<xsl:strip-space elements="*"/>

 <!--TODO:   Если не сколько  элементов  площадей  на один участок ????-->
 
 <!-- =========================================================================================== -->
  <xsl:template match="/*" priority="1">
        <xsl:apply-templates select="//Parcel//Entity_Spatial"/>    		 
  </xsl:template>
 <!-- =========================================================================================== -->
 <xsl:variable name="letterQuote" >  <xsl:text>"</xsl:text></xsl:variable>
 <xsl:variable name="letterBackSpark" >  <xsl:text>",</xsl:text> </xsl:variable>
 <xsl:variable name="letterEnter" >  <xsl:text>"&#10;</xsl:text> </xsl:variable>
 <xsl:variable name="letterApos"> <xsl:text>&apos;</xsl:text> </xsl:variable>
 
  <!-- =========================================================================================== -->
  <xsl:variable name="Cadastral_Block_CadastralNumber" select="//Cadastral_Block/@CadastralNumber"/> 
   <xsl:template match="Cadastral_Block" name="Cadastral_Block">	
         <xsl:apply-templates select="Parcels"/>
  </xsl:template> 
 
 <!--Parcel  не добавлять, если  нет координат  -->   
 <!-- =========================================================================================== --> 
   <xsl:template match="Parcel" name="Parcel">
             <xsl:if test="count(child::Entity_Spatial) >=1 ">
               <!-- <xsl:call-template name="Regio_Cadastr_Vidimus_KV" /> -->                       
	        	     <xsl:apply-templates select="Entity_Spatial"/>			         	        	      
	        	     <xsl:apply-templates select="Areas" />
	        	     <!-- <xsl:apply-templates select="Location"/>-->
	        	     <!-- <xsl:apply-templates select="GetRight"/>-->
			            <!--<xsl:call-template name="SelectEncumbrances_End"/>-->
               <xsl:call-template name="getCategotyParcel"/> 
         	   </xsl:if> 	
         	
             	<xsl:if test="count(Contours) >0 ">
         	          <xsl:apply-templates select="Contours" />   
         	    </xsl:if>         		
   </xsl:template>
 
 <!-- =========================================================================================== -->
 <xsl:template name="getCategotyParcel">
     <xsl:choose>
            <xsl:when test="count(Category) = 0 ">
              <xsl:text> &#10;</xsl:text>                 
            </xsl:when>
            <xsl:otherwise>
                 <xsl:apply-templates select ="Category"/>    
            </xsl:otherwise>
    </xsl:choose>  
 </xsl:template>

<!-- Описание пространственной составляющей сущности данных  -->	
 <!-- =========================================================================================== -->
 <xsl:template match="Entity_Spatial"  name="EntitySpatial">
   
    <xsl:choose>
		<xsl:when test="local-name(parent::node())='Parcel' ">
			<xsl:variable name="vPracelCadNum" select="parent::node()/@CadastralNumber"/>
			<xsl:variable name="vPracelArea" select="parent::node()/Areas/Area[1]/Area"/> 
			<xsl:variable name="vPracelAddress" select="parent::node()/Location/Address/@Note"/> 
			
			<xsl:value-of select="concat($letterQuote,$vPracelCadNum,$letterBackSpark)"/>  			
			<xsl:value-of select="concat($letterQuote,$vPracelArea,$letterBackSpark)"/>
			<xsl:value-of select="concat($letterQuote,$vPracelAddress,$letterQuote)"/>		
			<xsl:value-of select="$letterEnter"/>		
		</xsl:when>
		
		<xsl:when test="local-name(parent::node())='SubParcel' ">
			<xsl:variable name="vSubPracelObjEntry" select="parent::node()/Object_Entry/@CadastralNumber"/>
			<xsl:variable name="vSubPracelNumPP" select="parent::node()/@Number_PP"/>
			<xsl:variable name="vPracelCadNum" select="parent::node()/parent::node()/parent::node()/@CadastralNumber"/>
			<xsl:variable name="vPracelArea" select="Areas/Area[1]/Area"/> 
			<xsl:variable name="vPracelAddress" select="parent::node()/parent::node()/parent::node()/Location/Address/@Note"/> 			
			
			<xsl:choose>
				<xsl:when test="$vSubPracelObjEntry=''">
					<xsl:value-of select="concat($letterQuote,$vPracelCadNum,'\czu',$vSubPracelNumPP,$letterBackSpark)"/>  			
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="concat($letterQuote,$vPracelCadNum,'\czu',$vSubPracelNumPP,'(',$vSubPracelObjEntry,')',$letterBackSpark)"/>  			
				</xsl:otherwise>
			</xsl:choose>

			<xsl:value-of select="concat($letterQuote,$vPracelArea,$letterBackSpark)"/>
			<xsl:value-of select="concat($letterQuote,$vPracelAddress,$letterQuote)"/>		
			<xsl:value-of select="$letterEnter"/>
		</xsl:when>
			
		<xsl:when test="local-name(parent::node())='Contour' ">
			<xsl:variable name="vContoursNumPP" select="parent::node()/@Number_PP"/>
			<xsl:variable name="vPracelCadNum" select="parent::node()/parent::node()/parent::node()/@CadastralNumber"/>
			<xsl:variable name="vPracelArea" select="Areas/Area[1]/Area"/> 
			<xsl:variable name="vPracelAddress" select="parent::node()/parent::node()/parent::node()/Location/Address/@Note"/>
			
			<xsl:value-of select="concat($letterQuote,$vPracelCadNum,'(',$vContoursNumPP,')',$letterBackSpark)"/>  			
			<xsl:value-of select="concat($letterQuote,$vPracelArea,$letterBackSpark)"/>
			<xsl:value-of select="concat($letterQuote,$vPracelAddress,$letterQuote)"/>		
			<xsl:value-of select="$letterEnter"/>					
		</xsl:when>	
	</xsl:choose>

 </xsl:template>
 
</xsl:stylesheet>