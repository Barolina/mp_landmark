object FrameCompostion_EZ: TFrameCompostion_EZ
  Left = 0
  Top = 0
  Width = 590
  Height = 305
  Margins.Left = 0
  Margins.Top = 0
  Margins.Right = 0
  Margins.Bottom = 0
  Align = alClient
  ParentBackground = False
  TabOrder = 0
  ExplicitWidth = 451
  object pgcComposition_EZ: TPageControl
    Left = 0
    Top = 0
    Width = 590
    Height = 305
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    ActivePage = NewEntryParcel_TabSheet
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    Style = tsFlatButtons
    TabOrder = 0
    ExplicitWidth = 451
    object NewEntryParcel_TabSheet: TTabSheet
      AlignWithMargins = True
      Margins.Left = 0
      Margins.Top = 0
      Margins.Right = 0
      Margins.Bottom = 0
      Caption = #1057#1074#1077#1076#1077#1085#1080#1103' '#1086' '#1085#1086#1074#1099#1093' '#1074#1093#1086#1076#1103#1097#1080#1093
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Highlighted = True
      ImageIndex = 2
      ParentFont = False
      ExplicitWidth = 443
      object pnlNewEntryparcel: TPanel
        Left = 0
        Top = 0
        Width = 582
        Height = 37
        Align = alTop
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 1
        ExplicitWidth = 443
        object lblDefinition: TLabel
          Left = 6
          Top = -1
          Width = 119
          Height = 13
          Caption = #1054#1073#1086#1079#1085#1072#1095#1077#1085#1080#1077' '#1085#1072' '#1087#1083#1072#1085#1077':'
        end
        object NewEntryParcelList_ComboBox: TComboBox
          Left = 3
          Top = 13
          Width = 373
          Height = 22
          BevelKind = bkFlat
          Style = csOwnerDrawFixed
          Color = 15921123
          TabOrder = 1
          TextHint = #1053#1086#1074#1099#1077' '#1047#1059' '#1074#1093#1086#1076#1103#1097#1080#1077' '#1074' '#1045#1047
          OnChange = NewEntryParcelList_ComboBoxChange
        end
        object ButtonGroup2: TButtonGroup
          Left = 379
          Top = 11
          Width = 172
          Height = 25
          BevelInner = bvNone
          BevelOuter = bvNone
          BorderStyle = bsNone
          Images = formContainer.ilCommands
          Items = <
            item
              Hint = #13#10#1044#1086#1073#1072#1074#1080#1090#1100#13#10
              ImageIndex = 0
              OnClick = BitBtn_AddDefinitionClick
            end
            item
              Hint = #13#10#1059#1076#1072#1083#1080#1090#1100#13#10
              ImageIndex = 1
              OnClick = BitBtn_DelDefinitionClick
            end
            item
              Caption = #1048#1079#1084#1077#1085#1080#1090#1100' '#1086#1073#1086#1079#1085#1072#1095#1077#1085#1080#1077
              Hint = #13#10#1048#1079#1084#1077#1085#1080#1090#1100' '#1086#1073#1086#1079#1085#1072#1095#1077#1085#1080#1077#13#10
              ImageIndex = 9
              OnClick = ButtonGroup2Items2Click
            end
            item
              Action = actNewEntryparcel_Paste
              Hint = 
                #1042#1089#1090#1072#1074#1080#1090#1100' '#1080#1079' '#1073#1091#1092#1077#1088#1072' '#1086#1073#1084#1077#1085#1072' |'#1042#1089#1090#1072#1074#1080#1090#1100' '#1080#1079' '#1073#1091#1092#1077#1088#1072' '#1086#1073#1084#1077#1085#1072' '#13#10#1060#1086#1088#1084#1072#1090' '#1080#1084 +
                #1087#1086#1088#1090#1072' '#1076#1072#1085#1085#1099'  '#13#10'    \d+\d+\d+\d+:'#1079#1091'N'
            end
            item
              Action = actNewEntryparcel_Valid
              Hint = #13#10#1055#1088#1086#1074#1077#1088#1082#1072' '#1082#1086#1088#1088#1077#1082#1090#1085#1086#1089#1090#1080#13#10
            end
            item
              Action = Action1
              Caption = '&'
              Hint = #13#10#1059#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1087#1086#1088#1103#1076#1086#1082' '#1090#1086#1095#1077#1082#13#10
              ImageIndex = 5
            end>
          ShowHint = True
          TabOrder = 0
        end
      end
      object pgcEntitySpatial: TPageControl
        Left = 0
        Top = 37
        Width = 582
        Height = 237
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        ActivePage = tsArea
        Align = alClient
        TabOrder = 0
        ExplicitWidth = 443
        object tsArea: TTabSheet
          Caption = #1054#1087#1080#1089#1072#1085#1080#1077
          ExplicitWidth = 435
          object Panel1: TPanel
            Left = 0
            Top = 0
            Width = 574
            Height = 28
            Align = alTop
            BevelEdges = [beBottom]
            BevelKind = bkFlat
            BevelOuter = bvNone
            ParentColor = True
            TabOrder = 0
            ExplicitWidth = 435
            object lblName: TLabel
              Left = 3
              Top = 5
              Width = 62
              Height = 13
              Caption = #1058#1080#1087' '#1091#1095#1072#1089#1090#1082#1072
            end
            object cbbName: TComboBox
              Left = 70
              Top = 2
              Width = 145
              Height = 22
              BevelKind = bkFlat
              Style = csOwnerDrawFixed
              Color = 15921123
              ItemIndex = 0
              TabOrder = 0
              Text = #1054#1073#1086#1089#1086#1073#1083#1077#1085#1085#1099#1081
              OnChange = cbbNameChange
              Items.Strings = (
                #1054#1073#1086#1089#1086#1073#1083#1077#1085#1085#1099#1081
                #1059#1089#1083#1086#1074#1085#1099#1081)
            end
          end
        end
        object tsEntitySpatial: TTabSheet
          Caption = #1050#1086#1086#1088#1076#1080#1085#1072#1090#1099
          ImageIndex = 1
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
        end
      end
    end
    object ExistEntryParcels_TabSheet: TTabSheet
      Caption = 'C'#1091#1097#1077#1089#1090#1074#1091#1102#1097#1080#1077' '#1047#1059' '#1074#1082#1083#1102#1095#1072#1077#1084#1099#1077' '#1074' '#1045#1047
      ImageIndex = 2
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object IsFill_ExistEntryParcel_CheckBox: TCheckBox
        Left = 0
        Top = 0
        Width = 443
        Height = 17
        Cursor = crHandPoint
        Align = alTop
        Caption = 
          #1057#1074#1077#1076#1077#1085#1080#1103' '#1086' '#1089#1091#1097#1077#1089#1090#1074#1091#1102#1097#1080#1093' '#1086#1073#1086#1089#1086#1073#1083#1077#1085#1085#1099#1093' '#1080' '#1091#1089#1083#1086#1074#1085#1099#1093' '#1091#1095#1072#1089#1090#1082#1072#1093', '#1074#1082#1083#1102#1095#1072 +
          #1077#1084#1099#1093' '#1074' '#1089#1086#1089#1090#1072#1074' '#1045#1047
        Color = 13171400
        DoubleBuffered = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentColor = False
        ParentDoubleBuffered = False
        ParentFont = False
        TabOrder = 1
        OnClick = IsFill_ExistEntryParcel_CheckBoxClick
        ExplicitWidth = 649
      end
      object ExistEntryParcel_Panel: TPanel
        Left = 0
        Top = 17
        Width = 443
        Height = 257
        Align = alClient
        TabOrder = 0
        ExplicitWidth = 649
        ExplicitHeight = 388
        object meExistEntryParcel: TMaskEdit
          Left = 1
          Top = 1
          Width = 441
          Height = 21
          Hint = #1050#1072#1076#1072#1089#1090#1088#1086#1074#1099#1081' '#1085#1086#1084#1077#1088' '#1082#1074#1072#1088#1090#1072#1083#1072'  (12 - 13 '#1089#1080#1084#1074#1086#1083#1086#1074')'
          Align = alTop
          BevelKind = bkFlat
          BorderStyle = bsNone
          Color = 15921123
          TabOrder = 2
          Text = ''
          TextHint = #1050#1072#1076#1072#1089#1090#1088#1086#1074#1099#1081' '#1085#1086#1084#1077#1088
          Visible = False
          ExplicitWidth = 647
        end
        object btngpCadNumActions: TButtonGroup
          Left = 282
          Top = 22
          Width = 160
          Height = 234
          Align = alRight
          BevelEdges = [beLeft, beRight]
          BevelKind = bkFlat
          BorderStyle = bsNone
          ButtonOptions = [gboFullSize, gboShowCaptions]
          Images = formContainer.ilCommands
          Items = <
            item
              Action = Action_DoAdd
              Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1047#1059'  ('#1089#1087#1080#1089#1086#1082' '#1047#1059')'
              Hint = #1042#1089#1090#1072#1074#1082#1072' '#1089#1087#1080#1089#1082#1072' '#1080#1079' '#1073#1091#1092#1077#1088#1072' '#1086#1073#1084#1077#1085#1072
            end
            item
              Action = Action_DelSel
              Caption = #1059#1076#1072#1083#1080#1090#1100' '#1074#1099#1076#1077#1083#1077#1085#1085#1099#1077
            end>
          ShowHint = True
          TabOrder = 0
          ExplicitLeft = 488
          ExplicitHeight = 365
        end
        object ExistEntryParcelList_ListBox: TListBox
          Left = 1
          Top = 22
          Width = 281
          Height = 234
          Align = alClient
          BevelEdges = [beLeft]
          BevelKind = bkFlat
          BiDiMode = bdLeftToRight
          BorderStyle = bsNone
          Color = 15921123
          ItemHeight = 13
          MultiSelect = True
          ParentBiDiMode = False
          TabOrder = 1
        end
      end
    end
    object tsDeleteParcels: TTabSheet
      AlignWithMargins = True
      Margins.Left = 0
      Margins.Top = 0
      Margins.Right = 0
      Margins.Bottom = 0
      Caption = #1057#1074#1077#1076#1077#1085#1080#1103' '#1086' '#1080#1089#1082#1083#1102#1095#1072#1077#1084#1099#1093
      Highlighted = True
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object DeleteEntryParcels_Panel: TPanel
        Left = 0
        Top = 16
        Width = 443
        Height = 258
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        ExplicitWidth = 649
        ExplicitHeight = 389
        object btngpActionAddDel: TButtonGroup
          Left = 283
          Top = 21
          Width = 160
          Height = 237
          Align = alRight
          BevelEdges = [beLeft, beRight, beBottom]
          BevelKind = bkFlat
          BorderStyle = bsNone
          ButtonOptions = [gboFullSize, gboShowCaptions]
          Images = formContainer.ilCommands
          Items = <
            item
              Action = Action_DeleteEntryParcel_DoAdd
              Caption = #1044#1086#1073#1072#1074#1080#1090#1100
              Hint = #1042#1089#1090#1072#1074#1082#1072' '#1089#1087#1080#1089#1082#1072' '#1080#1079' '#1073#1091#1092#1077#1088#1072' '#1086#1073#1084#1077#1085#1072
            end
            item
              Action = Action_DeleteEntryParcel_DoDelSel
              Caption = #1059#1076#1072#1083#1080#1090#1100' '#1074#1099#1076#1077#1083#1077#1085#1085#1099#1077
            end>
          ShowHint = True
          TabOrder = 0
          ExplicitLeft = 489
          ExplicitHeight = 368
        end
        object DeleteEntryParcelsList_ListBox: TListBox
          Left = 0
          Top = 21
          Width = 283
          Height = 237
          Align = alClient
          BevelEdges = [beLeft, beBottom]
          BevelKind = bkFlat
          BorderStyle = bsNone
          Color = 15921123
          ItemHeight = 13
          MultiSelect = True
          TabOrder = 1
        end
        object meDeleteparcel: TMaskEdit
          Left = 0
          Top = 0
          Width = 582
          Height = 21
          Hint = #1050#1072#1076#1072#1089#1090#1088#1086#1074#1099#1081' '#1085#1086#1084#1077#1088' '#1082#1074#1072#1088#1090#1072#1083#1072'  (12 - 13 '#1089#1080#1084#1074#1086#1083#1086#1074')'
          Align = alTop
          BevelKind = bkFlat
          BorderStyle = bsNone
          Color = 15921123
          TabOrder = 2
          Text = ''
          TextHint = #1050#1072#1076#1072#1089#1090#1088#1086#1074#1099#1081' '#1085#1086#1084#1077#1088
          Visible = False
          ExplicitWidth = 649
        end
      end
      object IsFill_DeleteEntryParcels_CheckBox: TCheckBox
        Left = 0
        Top = 0
        Width = 582
        Height = 16
        Align = alTop
        Caption = 
          #1057#1087#1080#1089#1086#1082' '#1079#1077#1084#1077#1083#1100#1085#1099#1093' '#1091#1095#1072#1089#1090#1086#1074' '#1080#1089#1082#1083#1102#1095#1072#1077#1084#1099#1093' '#1080#1079' '#1089#1086#1089#1090#1072#1074#1072' '#1077#1076#1080#1085#1086#1075#1086' '#1079#1077#1084#1083#1077#1087#1086#1083 +
          #1100#1079#1086#1074#1072#1085#1080#1103
        Color = 13171400
        DoubleBuffered = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentColor = False
        ParentDoubleBuffered = False
        ParentFont = False
        TabOrder = 1
        OnClick = IsFill_DeleteEntryParcels_CheckBoxClick
        ExplicitWidth = 649
      end
    end
  end
  object ActionList: TActionList
    Images = formContainer.ilCommands
    Left = 392
    Top = 114
    object Action_DoAdd: TAction
      Caption = 'Action_ExistEntryParcel_DoAdd'
      ImageIndex = 0
      OnExecute = Action_DoAddExecute
    end
    object Action_DelSel: TAction
      Caption = 'Action_ExistEntryParcel_DelSel'
      ImageIndex = 1
      OnExecute = Action_DelSelExecute
    end
    object Action_DeleteEntryParcel_DoAdd: TAction
      Caption = 'Action_DeleteEntryParcel_DoAdd'
      ImageIndex = 0
      OnExecute = Action_DeleteEntryParcel_DoAddExecute
    end
    object Action_DeleteEntryParcel_DoDelSel: TAction
      Caption = 'Action_DeleteEntryParcel_DoDelSel'
      ImageIndex = 1
      OnExecute = Action_DeleteEntryParcel_DoDelSelExecute
    end
    object actNewEntryparcel_Paste: TAction
      Caption = 'Action1'
      ImageIndex = 2
      OnExecute = actNewEntryparcel_PasteExecute
    end
    object actNewEntryparcel_Valid: TAction
      Caption = 'actNewEntryparcel_Valid'
      ImageIndex = 4
      OnExecute = actNewEntryparcel_ValidExecute
    end
    object Action1: TAction
      Caption = 'Action1'
      OnExecute = Action1Execute
    end
  end
end
