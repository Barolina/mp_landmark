unit LMProject;
  ///
  ///  ��� ������ � �������� ����������
  ///
interface
uses
  Windows, Classes, Controls, SysUtils,
  IOUtils, unRussLang,FWZipWriter,FWZipReader,
  BpXML_different;

type

 TLMProject = class 
   public 
    class procedure CompressProj(toPath,SavePath,NameProj,ZipFormat : string); static;
    class procedure DecompressProj(Path : string); static;
 end;

 implementation

{ TLMProject }

///
/// <param name="toPath"   : ��� ������������ 
///        name="Savepath" : ���� ��������� �����
///        name="NameProj" : ��������� ������ (��� ������ ������ ��������� � ��������� xml �����,)
///                          �������� � ����� ��������, ��� �������� �������������
///        name= "ZipFormat": ��� ������� "zip" - ��� ���������� ��� "lmproj" - �������������� ��������
/// </param>
///
class procedure TLMProject.CompressProj(toPath,SavePath,NameProj,ZipFormat : string);
var    Zip: TFWZipWriter;
begin
  try
    Zip := TFWZipWriter.Create;
    try
      if Zip.AddFolder('', toPath, '*.*',true) = 0 then  raise Exception.Create('������ ���������� ������');
      ForceDirectories('..\'+savePath+'\');
      Zip.BuildZip(savePath+'\'+NameProj+'.'+zipFormat);
    finally
      Zip.Free;
    end;
  except on E: Exception do
     raise Exception.Create(E.Message);
  end;
end;

class procedure TLMProject.DecompressProj;
var
  Zip: TFWZipReader;
  str : string;
begin
  try
    Zip := TFWZipReader.Create;
    try
      Zip.LoadFromFile(Path);
      if Zip.Count >0  then
       begin
          str := Zip.Item[0].FileName;            //��� ��� ������� ������� �������� = ..\
          if Pos('../',str) > 0  then Zip.ExtractAll(TSettingPathFile.MakeTempPath+'\1\')
          else Zip.ExtractAll(TSettingPathFile.MakeTempPath);
          
       end;

//      Zip.ExtractAll(TSettingPathFile.MakeTempPath); {TODO: \1\ ��� ��� �� ���������  �������� ����� Extract, � ��� �������������� � ������ �����}
    finally
      Zip.Free;
    end;
  except  on E: Exception do
      raise E;
  end;
end;



end.
