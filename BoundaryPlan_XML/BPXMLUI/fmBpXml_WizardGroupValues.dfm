object FormBpXml_WizardGroupValues: TFormBpXml_WizardGroupValues
  Left = 0
  Top = 0
  BorderStyle = bsToolWindow
  Caption = #1052#1072#1089#1090#1077#1088' '#1091#1089#1090#1072#1085#1086#1074#1082#1080' '#1075#1088#1091#1087#1086#1074#1099#1093' '#1079#1085#1072#1095#1077#1085#1080#1081
  ClientHeight = 238
  ClientWidth = 324
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Panel_BottomControl: TPanel
    Left = 0
    Top = 213
    Width = 324
    Height = 25
    Align = alBottom
    TabOrder = 0
    object BitBtn_Ok: TBitBtn
      Left = 248
      Top = 1
      Width = 75
      Height = 23
      Align = alRight
      Caption = #1057#1082#1088#1099#1090#1100
      TabOrder = 0
      OnClick = BitBtn_OkClick
    end
    object NV_Back_BitBtn: TBitBtn
      Left = 1
      Top = 1
      Width = 25
      Height = 23
      Align = alLeft
      Caption = '<'
      TabOrder = 1
      OnClick = NV_Back_BitBtnClick
    end
    object NV_Next_BitBtn: TBitBtn
      Left = 26
      Top = 1
      Width = 25
      Height = 23
      Align = alLeft
      Caption = '>'
      TabOrder = 2
      OnClick = NV_Next_BitBtnClick
    end
  end
  object PageControl: TPageControl
    Left = 0
    Top = 0
    Width = 324
    Height = 213
    ActivePage = Dest_TabSheet
    Align = alClient
    TabOrder = 1
    object Source_TabSheet: TTabSheet
      Caption = #1048#1089#1090#1086#1095#1085#1080#1082
      TabVisible = False
      object Label1: TLabel
        Left = 0
        Top = 0
        Width = 316
        Height = 13
        Align = alTop
        Alignment = taCenter
        Caption = #1054#1073#1098#1077#1082#1090'-'#1080#1089#1090#1086#1095#1085#1080#1082
        Color = 16697005
        ParentColor = False
        Transparent = False
        Layout = tlCenter
        ExplicitWidth = 90
      end
      object Source_ListBox: TListBox
        Left = 0
        Top = 13
        Width = 316
        Height = 190
        Align = alClient
        ItemHeight = 13
        TabOrder = 0
      end
    end
    object Dest_TabSheet: TTabSheet
      Caption = #1062#1077#1083#1100
      ImageIndex = 1
      TabVisible = False
      object Label2: TLabel
        Left = 0
        Top = 0
        Width = 316
        Height = 13
        Align = alTop
        Alignment = taCenter
        Caption = #1054#1073#1098#1077#1082#1090#1099'-'#1062#1077#1083#1100
        Color = 16697005
        ParentColor = False
        Transparent = False
        Layout = tlCenter
        ExplicitWidth = 77
      end
      object Dest_CheckListBox: TCheckListBox
        Left = 0
        Top = 13
        Width = 316
        Height = 190
        Align = alClient
        ItemHeight = 13
        TabOrder = 0
        OnDblClick = Dest_CheckListBoxDblClick
      end
    end
    object Elements_TabSheet: TTabSheet
      Caption = 'Elements_TabSheet'
      ImageIndex = 2
      TabVisible = False
      object Label3: TLabel
        Left = 0
        Top = 0
        Width = 316
        Height = 13
        Align = alTop
        Alignment = taCenter
        Caption = #1069#1083#1077#1084#1077#1085#1090#1099' '#1086#1073#1098#1077#1082#1090#1072
        Color = 16697005
        ParentColor = False
        Transparent = False
        Layout = tlCenter
        ExplicitWidth = 97
      end
      object Elements_CheckListBox: TCheckListBox
        Left = 0
        Top = 13
        Width = 316
        Height = 165
        Align = alClient
        ItemHeight = 13
        TabOrder = 0
      end
      object BitBtn1: TBitBtn
        Left = 0
        Top = 178
        Width = 316
        Height = 25
        Align = alBottom
        Caption = '> '#1059#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1079#1085#1072#1095#1077#1085#1080#1103' <'
        TabOrder = 1
        OnClick = BitBtn1Click
        ExplicitTop = 223
        ExplicitWidth = 225
      end
    end
  end
end
