object FormSpecifyParcels: TFormSpecifyParcels
  Left = 0
  Top = 0
  Caption = 'MS.XML '#1059#1090#1086#1095#1085#1077#1085#1080#1077' '#1075#1088#1072#1085#1080#1094' '#1079#1077#1084#1077#1083#1100#1085#1099#1093' '#1091#1095#1072#1089#1090#1082#1086#1074
  ClientHeight = 364
  ClientWidth = 782
  Color = clBtnFace
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  Scaled = False
  PixelsPerInch = 96
  TextHeight = 16
  object lblSpecifyparcels: TLabel
    Left = 560
    Top = -8
    Width = 93
    Height = 16
    Caption = #1059#1090#1086#1095#1085#1077#1085#1080#1077' '#1047#1059
    Visible = False
  end
  object lblSpecifyParcelsEZ: TLabel
    Left = 640
    Top = 8
    Width = 95
    Height = 16
    Caption = #1059#1090#1086#1095#1085#1077#1085#1080#1077' '#1045#1047
    Visible = False
  end
  object jvSpecifyParcels: TJvgPageControl
    Left = 0
    Top = 0
    Width = 782
    Height = 364
    ActivePage = tsExistEZEntryParcel
    Align = alClient
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Style = tsFlatButtons
    TabOrder = 0
    TabStop = False
    TabStyle.Borders = [fsdLeft, fsdTop, fsdRight, fsdBottom]
    TabStyle.BevelInner = bvLowered
    TabStyle.BevelOuter = bvSpace
    TabStyle.Bold = False
    TabStyle.BackgrColor = clInactiveBorder
    TabStyle.Font.Charset = RUSSIAN_CHARSET
    TabStyle.Font.Color = clBlack
    TabStyle.Font.Height = -12
    TabStyle.Font.Name = 'Arial'
    TabStyle.Font.Style = [fsBold]
    TabStyle.TextStyle = fstVolumetric
    TabStyle.CaptionHAlign = fhaCenter
    TabStyle.GlyphHAlign = fhaCenter
    TabStyle.Gradient.ToColor = clSilver
    TabStyle.Gradient.Active = True
    TabStyle.Gradient.Orientation = fgdRightBias
    TabSelectedStyle.Borders = [fsdLeft, fsdTop, fsdRight, fsdBottom]
    TabSelectedStyle.BevelInner = bvNone
    TabSelectedStyle.BevelOuter = bvNone
    TabSelectedStyle.Bold = True
    TabSelectedStyle.BackgrColor = 15921123
    TabSelectedStyle.Font.Charset = ANSI_CHARSET
    TabSelectedStyle.Font.Color = clBlack
    TabSelectedStyle.Font.Height = -12
    TabSelectedStyle.Font.Name = 'Arial'
    TabSelectedStyle.Font.Style = [fsBold]
    TabSelectedStyle.TextStyle = fstVolumetric
    TabSelectedStyle.CaptionHAlign = fhaCenter
    TabSelectedStyle.GlyphHAlign = fhaCenter
    TabSelectedStyle.Gradient.FromColor = 15921123
    TabSelectedStyle.Gradient.ToColor = clSkyBlue
    TabSelectedStyle.Gradient.Active = True
    TabSelectedStyle.Gradient.Orientation = fgdRightBias
    Options = []
    object tsExistEzParcels: TTabSheet
      ImageIndex = 2
      TabVisible = False
    end
    object tsExisParcel: TTabSheet
      Caption = #1057#1074#1077#1076#1077#1085#1080#1103' '#1086' '#1091#1090#1086#1095#1085#1103#1077#1084#1099#1093' '#1079#1077#1084#1077#1083#1100#1085#1099#1093' '#1091#1095#1072#1089#1090#1082#1072#1093
      TabVisible = False
      object pnlExistParcelAttribute: TPanel
        Left = 0
        Top = 0
        Width = 774
        Height = 354
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
      end
    end
    object tsSpecifyRelatedParcel: TTabSheet
      Caption = #1059#1090#1086#1095#1085#1077#1085#1080#1077' '#1075#1088#1072#1085#1080#1094' '#1089#1084#1077#1078#1085#1099#1093' '#1091#1095#1072#1089#1090#1082#1086#1074
      ImageIndex = 2
      TabVisible = False
    end
    object tsExistEZEntryParcel: TTabSheet
      Caption = #1057#1074#1077#1076#1077#1085#1080#1103' '#1086#1073' '#1091#1090#1086#1095#1085#1103#1077#1084#1099#1093' '#1074#1093#1086#1076#1103#1097#1080#1093' '#1074' '#1045#1047' '#1091#1095#1072#1089#1090#1082#1072#1093
      ImageIndex = 3
      TabVisible = False
      object pnlExistEZEParcel: TPanel
        Left = 0
        Top = 0
        Width = 774
        Height = 42
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        BevelEdges = [beLeft, beRight]
        BevelKind = bkFlat
        BevelOuter = bvNone
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 0
        object lbl1: TLabel
          Left = 7
          Top = -2
          Width = 235
          Height = 13
          Caption = #1057#1087#1080#1089#1086#1082' '#1091#1090#1086#1095#1085#1103#1077#1084#1099#1093' '#1074#1093#1086#1076#1103#1097#1080#1093' '#1074' '#1045#1047' '#1047#1059':'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object cbbExistEZEParcel: TComboBox
          Left = 6
          Top = 14
          Width = 319
          Height = 22
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          BevelKind = bkFlat
          Style = csOwnerDrawFixed
          Color = 15921123
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          TextHint = #1059#1090#1086#1095#1085#1103#1077#1084#1099#1077' '#1074#1093#1086#1076#1103#1097#1080#1077' '#1047#1059' '#1074' '#1045#1047
          OnChange = cbbExistEZEParcelChange
        end
        object btnAddExistEZ: TButtonGroup
          Left = 327
          Top = 13
          Width = 179
          Height = 29
          BevelOuter = bvNone
          BorderStyle = bsNone
          ButtonHeight = 25
          ButtonWidth = 25
          Images = formContainer.ilCommands
          Items = <
            item
              Action = actExistEZEntryParcel_AddNew
            end
            item
              Action = actExistEZEntryParcel_DelCurrent
              Hint = #1059#1076#1072#1083#1080#1090#1100' '#1079#1072#1087#1080#1089#1100' '#1091#1090#1086#1095#1085#1103#1077#1084#1086#1075#1086' '#1047#1059' '#1074#1093#1086#1076#1103#1097#1077#1075#1086' '#1074' '#1045#1047
            end
            item
              Action = actExistEZEntryParcel_PasteFromClipBrd
              Hint = #1048#1087#1086#1088#1090' '#1082#1086#1086#1088#1076#1080#1085#1072#1090
            end
            item
              Action = actExistEZEntryParcel_Validate
              Hint = #1055#1088#1086#1074#1077#1088#1082#1072
            end
            item
              Action = actExistEZEntryParcel_CheckDirections
              Caption = '&'
              Hint = #1059#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1087#1086#1088#1103#1076#1086#1082' '#1090#1086#1095#1077#1082
              ImageIndex = 5
            end>
          ShowHint = True
          TabOrder = 1
        end
      end
    end
  end
  object ActionManagerSpecifyParcel: TActionManager
    ActionBars = <
      item
        Items = <
          item
            Action = Action_ExistParcels
          end
          item
            Action = Action_SpecifyRelatedParcels
          end>
      end
      item
        Items = <
          item
            Action = Action_ExistParcels
          end
          item
            Action = Action_SpecifyRelatedParcels
          end>
      end>
    Left = 63
    Top = 88
    StyleName = 'Platform Default'
    object Action_ExistParcels: TAction
      Category = 'SpecifityParcels'
      Caption = #1057#1074#1077#1076#1077#1085#1080#1103
      OnExecute = Action_ExistParcelsExecute
    end
    object Action_SpecifyRelatedParcels: TAction
      Category = 'SpecifityParcels'
      Caption = #1057#1084#1077#1078#1085#1080#1082#1080' '
      OnExecute = Action_SpecifyRelatedParcelsExecute
    end
    object ActionAddAtrExistParcel: TAction
      Category = 'AtributExistParcel'
      Caption = 'ActionAddAtributExistParcel'
    end
    object Act_AddExistparcelCadastralNumber: TAction
      Category = 'AtributExistParcel'
      Caption = 'Act_AddExistparcelCadastralNumber'
    end
  end
  object PopupMenu: TPopupMenu
    Left = 64
    Top = 136
    object PMItem_SlowlyChangeSlide: TMenuItem
      Caption = #1055#1083#1072#1074#1085#1072#1103' '#1089#1084#1077#1085#1072' '#1089#1083#1072#1081#1076#1086#1074
      Checked = True
      GroupIndex = 1
      OnClick = PMItem_SlowlyChangeSlideClick
    end
    object PMItem_QuickChangeSlide: TMenuItem
      Caption = #1054#1073#1099#1095#1085#1072#1103' '#1089#1084#1077#1085#1072' '#1089#1083#1072#1081#1076#1086#1074
      Enabled = False
      GroupIndex = 1
      OnClick = PMItem_QuickChangeSlideClick
    end
  end
  object actlst: TActionList
    Images = formContainer.ilCommands
    Left = 64
    Top = 192
    object actExistEZEntryParcel_AddNew: TAction
      Category = #1059#1090#1086#1095#1085#1103#1077#1084#1099#1077' '#1074#1093#1086#1076#1103#1097#1080#1077
      Caption = 'actExistEZEntryParcel_AddNew'
      Hint = #1044#1086#1073#1072#1074#1080#1090#1100' '#1079#1072#1087#1080#1089#1100' '#1091#1090#1086#1095#1085#1103#1077#1084#1086#1075#1086' '#1047#1059' '#1074#1093#1086#1076#1103#1097#1077#1075#1086' '#1074' '#1045#1047
      ImageIndex = 0
      OnExecute = actExistEZEntryParcel_AddNewExecute
    end
    object actExistEZEntryParcel_DelCurrent: TAction
      Category = #1059#1090#1086#1095#1085#1103#1077#1084#1099#1077' '#1074#1093#1086#1076#1103#1097#1080#1077
      Caption = 'actExistEZEntryParcel_DelCurrent'
      ImageIndex = 1
      OnExecute = actExistEZEntryParcel_DelCurrentExecute
    end
    object actExistEZEntryParcel_PasteFromClipBrd: TAction
      Category = #1059#1090#1086#1095#1085#1103#1077#1084#1099#1077' '#1074#1093#1086#1076#1103#1097#1080#1077
      Caption = 'actExistEZEntryParcel_PasteFromClipBrd'
      ImageIndex = 2
      OnExecute = actExistEZEntryParcel_PasteFromClipBrdExecute
    end
    object actExistEZEntryParcel_Validate: TAction
      Category = #1059#1090#1086#1095#1085#1103#1077#1084#1099#1077' '#1074#1093#1086#1076#1103#1097#1080#1077
      Caption = 'actExistEZEntryParcel_Validate'
      ImageIndex = 4
      OnExecute = actExistEZEntryParcel_ValidateExecute
    end
    object actExistEZEntryParcel_CheckDirections: TAction
      Category = #1059#1090#1086#1095#1085#1103#1077#1084#1099#1077' '#1074#1093#1086#1076#1103#1097#1080#1077
      Caption = 'act1'
      OnExecute = actExistEZEntryParcel_CheckDirectionsExecute
    end
  end
end
