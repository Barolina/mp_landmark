unit Issues.API.Intf;

interface

uses
  IdMultipartFormData, IDHttp;
type

IIssues = interface
  function GetProject:string;
  procedure SetProject(const Value:string);

  procedure LoginBugTracker(aLogin, aPass:string);
  function CreateIssues(aCaption, aDescription:string; aData:TIdMultiPartFormDataStream):string;

  property Project:string read GetProject write SetProject;
end;

implementation

end.
