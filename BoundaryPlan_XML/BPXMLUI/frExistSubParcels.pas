unit frExistSubParcels;
{
  �������� ������ ������������ ������ ����������� �������� �������
  (� �.�. �������������� ������)

  SpecifyRelatedParcel -> ExistSubParcels
}


interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ActnList, ButtonGroup, StdCtrls, ExtCtrls,

  STD_MP, ImgList, System.Actions,
  frExistSubParcel,
  unRussLang,
  FrTypeParcel,fmContainer;

type
  TFrameExistSubParcels = class(TFrame)
    ActionList: TActionList;
    Action_AddNewSubParcel: TAction;
    Action_DeleteSubParcel: TAction;
    Action_SelectSubParcel: TAction;
    PanelSubParcelFrame: TPanel;
    SubParcels_Actions_ButtonGroup: TButtonGroup;
    ExistSubParcels_Count: TListBox;
    pblListElement: TPanel;
    Splitter1: TSplitter;

    procedure Action_AddNewSubParcelExecute(Sender: TObject);
    procedure Action_DeleteSubParcelExecute(Sender: TObject);
    procedure Action_DelAllSubParcelsExecute(Sender: TObject);
    procedure Action_SelectSubParcelExecute(Sender: TObject);
    procedure SubParcels_ComboBox1Change(Sender: TObject);
    procedure Attribute_DefOrNumPP_EditKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ExistSubParcels_CountDblClick(Sender: TObject);

  private
    { Private declarations }
    FXmlExistSubParcels  : IXMLTSpecifyRelatedParcel_ExistSubParcels;
    FExistSubParcel_Frame: TFrameExistSubParcel;

    procedure Data_SubParcel_DoAddNew;
    procedure Data_SubParcel_DoDelete(index: Integer);
    procedure SubParcelsList_DoUpDate;
    procedure SubParcelsList_DoRefresh(A: Integer = -1; B: Integer = -1);

    procedure OnChange_Attribute_DefinitionOrNUmberPP(Sender: TObject);
  protected
    procedure ExistSetSubParcels(const aExistSubParcels
      : IXMLTSpecifyRelatedParcel_ExistSubParcels);
    procedure InitializationUI;
  public
    { Public declarations }
    constructor Create(Owner: TComponent); override;
    function IsAssigned_ExistSubParcels: Boolean;
    procedure SubParcelsList_UpDate;
    procedure SubParcelsList_SetCurrent(index: Integer);

    property XmlExistSubParcels: IXMLTSpecifyRelatedParcel_ExistSubParcels
      read FXmlExistSubParcels write ExistSetSubParcels;
    property ExistSubParcel: TFrameExistSubParcel read FExistSubParcel_Frame;
  end;

implementation

uses MsXMLAPI;

resourcestring
  cnstNameExistParcel = '(� �����)';

{$R *.dfm}
  { TFrameEXistubParcels }

procedure TFrameExistSubParcels.Action_AddNewSubParcelExecute(Sender: TObject);
begin
  if not self.IsAssigned_ExistSubParcels then
    EXIT;
  self.Data_SubParcel_DoAddNew;
  self.SubParcelsList_SetCurrent(self.ExistSubParcels_Count.ItemIndex);

end;

procedure TFrameExistSubParcels.Action_DelAllSubParcelsExecute(Sender: TObject);
begin
  if not IsAssigned_ExistSubParcels then
    EXIT;
  FXmlExistSubParcels.Clear;
  self.SubParcelsList_DoUpDate;
end;

procedure TFrameExistSubParcels.Action_DeleteSubParcelExecute(Sender: TObject);
begin
  if not self.FExistSubParcel_Frame.IsAssignedSubParcel then
    EXIT;
  if MessageDlg('������� ��������� ������ ?', mtConfirmation, mbYesNo, -1) = mrYes
  then
  begin
    self.Data_SubParcel_DoDelete(self.ExistSubParcels_Count.ItemIndex);
    self.SubParcelsList_SetCurrent(self.ExistSubParcels_Count.ItemIndex);
    if not self.FExistSubParcel_Frame.IsAssignedSubParcel then
      self.SubParcelsList_SetCurrent(0);
  end;
end;

procedure TFrameExistSubParcels.Action_SelectSubParcelExecute(Sender: TObject);
begin
  self.SubParcelsList_SetCurrent(self.ExistSubParcels_Count.ItemIndex);
end;

procedure TFrameExistSubParcels.Attribute_DefOrNumPP_EditKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key = VK_RETURN then
    if (Sender is TCustomEdit) then
      TCustomEdit(Sender).Parent.SetFocus;
end;

constructor TFrameExistSubParcels.Create(Owner: TComponent);
begin
  inherited;
  self.InitializationUI;
  // SubParcels_ComboBox.ItemIndex := 0;
  self.FExistSubParcel_Frame := TFrameExistSubParcel.Create
    (self.PanelSubParcelFrame);
  self.FExistSubParcel_Frame.Parent := self.PanelSubParcelFrame;
  self.FExistSubParcel_Frame.Align := alClient;
  self.FExistSubParcel_Frame.OnChangeDefOrNumPP :=
    self.OnChange_Attribute_DefinitionOrNUmberPP;
  self.FExistSubParcel_Frame.Visible := false;
end;

procedure TFrameExistSubParcels.InitializationUI;
begin
  self.ExistSubParcels_Count.Clear;
end;

{ Assigned }
function TFrameExistSubParcels.IsAssigned_ExistSubParcels: Boolean;
begin
  Result := self.FXmlExistSubParcels <> nil;
end;

procedure TFrameExistSubParcels.OnChange_Attribute_DefinitionOrNUmberPP
  (Sender: TObject);
begin
  self.SubParcelsList_DoRefresh(self.ExistSubParcels_Count.ItemIndex,
    self.ExistSubParcels_Count.ItemIndex);
end;

procedure TFrameExistSubParcels.ExistSetSubParcels(const aExistSubParcels
  : IXMLTSpecifyRelatedParcel_ExistSubParcels);
begin
  self.SubParcelsList_SetCurrent(-1);
  self.FXmlExistSubParcels := aExistSubParcels;
  self.InitializationUI;
  if self.IsAssigned_ExistSubParcels then
    self.SubParcelsList_DoUpDate;
end;

procedure TFrameExistSubParcels.ExistSubParcels_CountDblClick(Sender: TObject);
var
  numberEl: string;
  currentEl: Integer;
  lbIndex: Integer;
begin
  if ExistSubParcels_Count.ItemIndex > -1 then
  begin
    lbIndex := ExistSubParcels_Count.ItemIndex;
    currentEl := FXmlExistSubParcels.ExistSubParcel[lbIndex].Number_Record;
    numberEl := InputBox('������� ����� ��', '�������  ������� ����� ��',
      inttostr(currentEl));
    if numberEl = '' then
      EXIT;
    if TryStrToInt(numberEl, currentEl) then
    begin
      FXmlExistSubParcels.ExistSubParcel[lbIndex].Number_Record := currentEl;
      ExistSubParcels_Count.Items[lbIndex] := inttostr(currentEl);
    end;
  end;
end;

procedure TFrameExistSubParcels.SubParcelsList_SetCurrent(index: Integer);
var
  SubParcel: IXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel;
begin
  if (index > -1) and (index < self.ExistSubParcels_Count.Items.Count) then
  begin
    SubParcel := FXmlExistSubParcels.ExistSubParcel[index];
    // SubParcels_ComboBox.ItemIndex := ord((SubParcel.ChildNodes.FindNode('Contours') <> nil));
    self.FExistSubParcel_Frame.XmlSpecifyRelatedParcel_ExistSubParcel :=
      SubParcel;
    self.FExistSubParcel_Frame.Visible := true;
    self.ExistSubParcels_Count.ItemIndex := index;
  end
  else
  begin
    self.FExistSubParcel_Frame.Visible := false;
    self.FExistSubParcel_Frame.XmlSpecifyRelatedParcel_ExistSubParcel := nil;
    self.ExistSubParcels_Count.ItemIndex := -1;
    // SubParcels_ComboBox.ItemIndex := 0;
  end;
end;

procedure TFrameExistSubParcels.SubParcelsList_DoRefresh(A: Integer = -1;
  B: Integer = -1);
var
  _SubParcel: IXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel;
  _i, _j: Integer;
  _ItemName: string;
begin
  if self.ExistSubParcels_Count.Items.Count = 0 then
    EXIT;

  if (A < 0) or (A >= self.ExistSubParcels_Count.Items.Count) then
    A := 0;

  if (B < A) or (B >= self.ExistSubParcels_Count.Items.Count) then
    B := self.ExistSubParcels_Count.Items.Count - 1;
  _j := self.ExistSubParcels_Count.ItemIndex;
  for _i := A to B do
  begin
    _SubParcel := IXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel
      (pointer(self.ExistSubParcels_Count.Items.Objects[_i]));
    _ItemName := MsXml_ReadAttribute(_SubParcel, cnstAtrNumber_Record);
    self.ExistSubParcels_Count.Items.Strings[_i] := _ItemName;
  end;
  self.ExistSubParcels_Count.ItemIndex := _j;
end;

procedure TFrameExistSubParcels.SubParcelsList_DoUpDate;
var
  _index: Integer;
  _i: Integer;
  _SubParcelInvariable
    : IXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel;
  _ItemName: string;
  count_el: Integer;
  IndexType: Integer;
begin
  count_el := -1;
  _index := self.ExistSubParcels_Count.ItemIndex;
  self.ExistSubParcels_Count.Items.Clear;
  for _i := 0 to self.FXmlExistSubParcels.Count - 1 do
  begin
    _SubParcelInvariable := self.FXmlExistSubParcels.ExistSubParcel[_i];
      count_el := count_el + 1;
      _ItemName := MsXml_ReadAttribute(_SubParcelInvariable,
        cnstAtrNumber_Record);
      _SubParcelInvariable.Number_Record := strtoint(_ItemName);
      self.ExistSubParcels_Count.AddItem(_ItemName,
        pointer(_SubParcelInvariable));
  end;
  self.SubParcelsList_SetCurrent(count_el);
  if not self.FExistSubParcel_Frame.IsAssignedSubParcel then
    self.SubParcelsList_SetCurrent(0);
end;

procedure TFrameExistSubParcels.SubParcelsList_UpDate;
begin
  if self.IsAssigned_ExistSubParcels then
    self.SubParcelsList_DoUpDate;
end;

procedure TFrameExistSubParcels.SubParcels_ComboBox1Change(Sender: TObject);
begin
  self.Data_SubParcel_DoAddNew;
  self.SubParcelsList_SetCurrent(self.ExistSubParcels_Count.ItemIndex);
end;

procedure TFrameExistSubParcels.Data_SubParcel_DoAddNew;
var
  _SubParcel: IXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel;
  _ItemName: string;
  Entity_Spatila: IXMLEntity_Spatial;
  Contours: IXMLTSubParcel_Contours;
  IndexType: Integer;
begin
  _SubParcel := self.FXmlExistSubParcels.Add;
  _ItemName := inttostr(self.FXmlExistSubParcels.Count);
  _SubParcel.Number_Record := strtoint(_ItemName);
  IndexType := FrTypeParcel.frmTypeParcel.GetTypeObject;
  // ����� ��������� ������� ��������������� ��������� or Contours
  if IndexType = 0 then
  begin
    Entity_Spatila := _SubParcel.Entity_Spatial;
    MsXml_RemoveChildNode(_SubParcel, 'Contours');
  end
  else
  begin
    Contours := _SubParcel.Contours;
    MsXml_RemoveChildNode(_SubParcel, 'Entity_Spatial');
  end;
  self.ExistSubParcels_Count.AddItem(_ItemName + cnstNameExistParcel,
    pointer(_SubParcel));
  self.ExistSubParcels_Count.ItemIndex :=
    self.ExistSubParcels_Count.Items.Count - 1;
  Contours := nil;
  Entity_Spatila := nil;
end;

procedure TFrameExistSubParcels.Data_SubParcel_DoDelete(index: Integer);
var
  _SubParcel: IXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel;

begin
  _SubParcel := IXMLTSpecifyRelatedParcel_ExistSubParcels_ExistSubParcel
    (pointer(self.ExistSubParcels_Count.Items.Objects[index]));
  if self.FExistSubParcel_Frame.XmlSpecifyRelatedParcel_ExistSubParcel = _SubParcel
  then
    self.SubParcelsList_SetCurrent(index - 1);
  self.ExistSubParcels_Count.Items.Delete(index);
  self.FXmlExistSubParcels.Remove(_SubParcel);
  _SubParcel := nil;
  ExistSubParcels_Count.ItemIndex := ExistSubParcels_Count.Count - 1;
end;

end.
