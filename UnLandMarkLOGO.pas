unit UnLandMarkLOGO;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, ButtonGroup, StdCtrls, ActnList, JvComponentBase, JvAnimTitle, Vcl.Imaging.pngimage,
  Vcl.ExtCtrls;

type
  TFrLOGO = class(TForm)
    imgLOGO: TImage;                         
    jvnmtlView: TJvAnimTitle;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    constructor Create;
    { Public declarations }
  end;

var
  FrLOGO: TFrLOGO;

implementation
uses UnLandMark;

{$R *.dfm}

constructor TFrLOGO.Create;
begin
 inherited Create(nil);
end;

procedure TFrLOGO.FormCreate(Sender: TObject);
begin
 Position:=poScreenCenter;
 FormStyle:=fsStayOnTop;
 BorderStyle:=bsNone;
 BorderIcons:=[];               
end;

end.
