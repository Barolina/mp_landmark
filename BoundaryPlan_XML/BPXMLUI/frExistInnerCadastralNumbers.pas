unit frExistInnerCadastralNumbers;
{
   ExistParcel -> Inner_CadastralNumbers

   ����������� ��� ���� ������ ������, ����������, �������� ��������������
   �������������, ������������� �� ��������� �������
}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, ExtCtrls, ActnList, StdCtrls, Mask, Buttons, ButtonGroup,

  XMLIntf, std_mp,MSStringParser, MSClipBoard, ImgList, BPCommon, System.Actions;

type
  TFrameExistInnerCadastralNumbers = class(TFrame)
    ActionList: TActionList;
    CadNum_ButtonGroup_Actions: TButtonGroup;
    CadNum_ListCN_ListBox: TListBox;
    CadNum_NewCN_MaskEdit: TMaskEdit;
    Action_DoAdd: TAction;
    PanelCentre: TPanel;
    PanelLeft: TPanel;

    Action_CopySel: TAction;
    Action_DelSel: TAction;
    Action_Paste: TAction;
    ImageList: TImageList;
    Pan_InnerCadastralNumber: TPanel;
    RadGInnerCadastralNumber: TRadioGroup;
    procedure Action_DoAddExecute(Sender: TObject);
    procedure Action_CopySelExecute(Sender: TObject);
    procedure Action_DelSelExecute(Sender: TObject);
    procedure Action_PasteExecute(Sender: TObject);
  private
    { Private declarations }
    FXmlInnerCadastralNumbers:  IXMLTInner_CadastralNumbers;
    FXMlCadastralNumbers : IXMLTInner_CadastralNumbers_CadastralNumberList;

    procedure Data_DoAdd;
    procedure Data_DoDeleteSelected;
    procedure Data_DoRead;
    procedure Data_DoWrite;
  protected
    procedure InitializationUI;
    procedure SetCadNums(const aCadastralNumbers: IXMLTInner_CadastralNumbers);
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;

    function IsAssignedCadastralNumbers: Boolean;
     property XmlCadastralNumbers: IXMLTInner_CadastralNumbers read FXmlInnerCadastralNumbers write SetCadNums;

  end;

//function IsValidCadNum(value: string;  IsLand: Boolean): Boolean;

implementation

uses BpXML_different;

{$R *.dfm}
{
 function IsValidCadNum(value: string;  IsLand: Boolean): Boolean;
 var
  _sp: TMSStringParser;
 begin
  _sp:= TMSStringParser.Create(':  _');
  _sp.Parse(value);
  Result:= ((_sp.GetCountToken=4) and (IsLand)) or ((_sp.GetCountToken=3) and (not IsLand));
  if not Result then
   Result:= Length(_sp.GetToken(0))=2;
  if not Result then
   Result:= Length(_sp.GetToken(1))=2;
  if not Result then
   Result:= Length(_sp.GetToken(2))=7;
  if (not Result) and IsLand  then
   Result:= Length(_sp.GetToken(3))<>0;
  _sp.Free;
 end;
}
{ TFrameCadastralNumbersCollections }

procedure TFrameExistInnerCadastralNumbers.Action_CopySelExecute(Sender: TObject);
var
 _stl: TStringList;
 _i: Integer;
begin
 if not self.IsAssignedCadastralNumbers then raise Exception.Create(ExMSG_NotAssignedROOT);
 _stl:= TStringList.Create;
 for  _i:= 0 to self.CadNum_ListCN_ListBox.Count - 1 do
  if self.CadNum_ListCN_ListBox.Selected[_i] then
   _stl.Add(self.CadNum_ListCN_ListBox.Items[_i]);
 ClipBrs_WriteStrings(_stl);
 _stl.Free;
end;

procedure TFrameExistInnerCadastralNumbers.Action_DelSelExecute(
  Sender: TObject);
begin
 if not self.IsAssignedCadastralNumbers then raise Exception.Create(ExMSG_NotAssignedROOT);
 self.Data_DoDeleteSelected;
end;

procedure TFrameExistInnerCadastralNumbers.Action_DoAddExecute(Sender: TObject);
begin
 if not self.IsAssignedCadastralNumbers then raise Exception.Create(ExMSG_NotAssignedROOT);
 self.Data_DoAdd;
end;

procedure TFrameExistInnerCadastralNumbers.Action_PasteExecute(Sender: TObject);
var
 _i: Integer;
 _stl: TStringList;
 _cnum: string;
begin
 if not self.IsAssignedCadastralNumbers then raise Exception.Create(ExMSG_NotAssignedROOT);

 if self.CadNum_ListCN_ListBox.Count<>0 then
   if MessageDlg('������� ��������� � ������ ������?',mtConfirmation,mbYesNo,-1)=mrYes then begin
     self.CadNum_ListCN_ListBox.SelectAll;
     Data_DoDeleteSelected;
   end;
 _stl:= TStringList.Create;
 ClipBrd_ReadStrings(_stl);
 for _i := 0 to _stl.Count - 1 do begin
  _cnum:= _stl[_i];
  self.CadNum_NewCN_MaskEdit.Text:= _cnum;
  try
   self.Data_DoAdd;
  Except
   ShowMessage('�������� ������� :)');
  end;
 end;
 _stl.Free;
end;

constructor TFrameExistInnerCadastralNumbers.Create(AOwner: TComponent);
begin
 inherited;
 self.InitializationUI;
end;

procedure TFrameExistInnerCadastralNumbers.InitializationUI;
begin
 self.CadNum_NewCN_MaskEdit.Text:= '';
 if self.IsAssignedCadastralNumbers then
  self.Data_DoRead
 else
  self.CadNum_ListCN_ListBox.Clear;
end;

function TFrameExistInnerCadastralNumbers.IsAssignedCadastralNumbers: Boolean;
begin
 Result:= self.FXmlInnerCadastralNumbers<>nil;
end;

procedure TFrameExistInnerCadastralNumbers.SetCadNums(const aCadastralNumbers: IXMLTInner_CadastralNumbers);
begin
 if self.FXmlInnerCadastralNumbers=aCadastralNumbers then EXIT;
 self.InitializationUI;
end;

procedure TFrameExistInnerCadastralNumbers.Data_DoAdd;
var
 _cadnum: IXMLNode;
 _cn:string;
begin
 try
  self.CadNum_NewCN_MaskEdit.ValidateEdit;
 except
  raise Exception.Create(Format(ExMsg_Data_Invalid,[self.CadNum_NewCN_MaskEdit.Hint]));
 end;

 _cn:= self.CadNum_NewCN_MaskEdit.Text;
 if _cn='' then EXIT;
 if not BPcCadNum_IsExistItemValue(_cn,[cnDistrict,cnMunicipality,cnBlock,cnNum]) then
  if MessageDlg('��������� ������ �� ������������� ������� ������������ ������.'+sLineBreak+
                '������������� ������������ ����� ������?',mtConfirmation,mbYesNo,-1)=mrNo then
                EXIT;


// if not IsValidCadNum(_cn,true) then
 // raise Exception.Create(Format(ExMsg_Data_Invalid,[self.CadNum_NewCN_MaskEdit.Hint]));
 if self.CadNum_ListCN_ListBox.Items.IndexOf(_cn)<>-1 then exit;
 _cadnum := FXMlCadastralNumbers.Add(self.CadNum_NewCN_MaskEdit.Text);
 self.CadNum_ListCN_ListBox.AddItem(self.CadNum_NewCN_MaskEdit.Text,pointer(_cadnum));

end;

procedure TFrameExistInnerCadastralNumbers.Data_DoDeleteSelected;
var
 _i:integer;
 _CadNum: IXMLNode;
begin
 for _i := self.CadNum_ListCN_ListBox.Count-1 downto 0 do
  if self.CadNum_ListCN_ListBox.Selected[_i] then begin
   _CadNum:= IXMLNode(pointer(self.CadNum_ListCN_ListBox.Items.Objects[_i]));
   self.FXmlCadastralNumbers.Remove(_CadNum);
   self.CadNum_ListCN_ListBox.Items.Delete(_i);
  end;
end;

procedure TFrameExistInnerCadastralNumbers.Data_DoRead;
var
 _i: Integer;
 _CadNum: IXMLNode;
 _StVal: string;
begin
 self.CadNum_ListCN_ListBox.Clear;
 for _i := 0 to self.FXmlCadastralNumbers.Count-1 do
 begin
  _CadNum.NodeValue:= self.FXmlCadastralNumbers.Items[_i];
  _StVal:= _CadNum.NodeValue;
  self.CadNum_ListCN_ListBox.AddItem(_StVal,pointer(_CadNum));
 end;
end;

procedure TFrameExistInnerCadastralNumbers.Data_DoWrite;
var
 _i: Integer;
 _CadNum: IXMLNode;
 _StVal: string;
begin
 self.FXmlCadastralNumbers.Clear;
 for _i := 0 to self.CadNum_ListCN_ListBox.Count-1 do
 begin
  _CadNum.NodeValue:= self.FXmlCadastralNumbers[_i];
  self.CadNum_ListCN_ListBox.Items.Objects[_i]:= pointer(_CadNum);
 end;
end;

end.
