object FrameAreaInnccuracy: TFrameAreaInnccuracy
  Left = 0
  Top = 0
  Width = 192
  Height = 55
  Color = clBtnFace
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  ParentBackground = False
  ParentColor = False
  ParentFont = False
  TabOrder = 0
  object AreaValue_Label_Caption: TLabel
    Left = 9
    Top = 4
    Width = 152
    Height = 13
    Caption = #1047#1085#1072#1095#1077#1085#1080#1077' '#1087#1083#1086#1097#1072#1076#1080' ('#1082#1074'.'#1084')'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object AreaValue_MaskEdit: TMaskEdit
    Left = 9
    Top = 23
    Width = 159
    Height = 21
    BevelKind = bkFlat
    BorderStyle = bsNone
    Color = 15921123
    TabOrder = 0
    Text = ''
    StyleElements = []
    OnExit = Action_AreaValue_WriteExecute
  end
  object ActionList: TActionList
    Left = 632
    Top = 16
    object Action_AreaValue_Write: TAction
      Caption = 'Action_AreaValue_Write'
      OnExecute = Action_AreaValue_WriteExecute
    end
    object Action_Innccuracy_Write: TAction
      Caption = 'Action_Innccuracy_Write'
    end
  end
end
