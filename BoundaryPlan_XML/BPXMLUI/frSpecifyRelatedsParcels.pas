unit frSpecifyRelatedsParcels;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, StdCtrls, ActnList, ButtonGroup, ExtCtrls,

  System.RegularExpressions, MsXmlApi, System.Actions, 
  SurveyingPlan.Common,
  STD_MP, BpXML_different,
  unRussLang,FormCadastralNumber,
  fmContainer, frSpecifyRelatedsParcel, Vcl.ComCtrls, Vcl.ToolWin, JvExComCtrls,
  JvToolBar;

type
  TFrameSpecifyRelatedsParcels = class(TFrame)
    RelatedPanel: TPanel;
    Panel_PanelControlSpecifyRelatedParce: TPanel;
    Label4: TLabel;
    SpecifyRelatedParcelItems_ComboBox: TComboBox;
    RelatedElemPanel: TPanel;
    actlst: TActionList;
    actSpecifyRelatedParcel_AddNew: TAction;
    actSpecifyRelatedParcel_Delete: TAction;
    actSpecifyRelatedParcel_EditDefinition: TAction;
    jvtlbr2: TJvToolBar;
    btn1: TToolButton;
    btnEntity_Spatial: TToolButton;
    btnEncumbrance: TToolButton;
    pnl1: TPanel;
    btnAdd: TToolButton;
    btnDel: TToolButton;
    btnEdit: TToolButton;
    lblArea: TLabel;
    procedure actSpecifyRelatedParcel_AddNewExecute(Sender: TObject);
    procedure actSpecifyRelatedParcel_DeleteExecute(Sender: TObject);
    procedure actSpecifyRelatedParcel_EditDefinitionExecute(Sender: TObject);
    procedure SpecifyRelatedParcelItems_ComboBoxChange(Sender: TObject);
    procedure btnEntity_SpatialClick(Sender: TObject);
    procedure btnEncumbranceClick(Sender: TObject);
  private
    { Private declarations }
    FXmlSpecifyRelatedParcelList : IXMLSTD_MP_Package_SpecifyParcel_SpecifyRelatedParcelList;
    FSpecifyParcelrelated        : TFrameSpecifyParcelrelated;

    procedure SpecifyRelatedParcel_DoCreateNewItem(CadNum: string; Contour: integer = 0);
    procedure SpecifyRelatedParcel_DoDelete(index: Integer);
    procedure SpecifyRelatedParcel_DoSetCurrentItem(index: Integer);

    procedure SetSpecifyRelatedParcelList(aValue: IXMLSTD_MP_Package_SpecifyParcel_SpecifyRelatedParcelList);
   public
    constructor Create(AOwner: TComponent); override;
    function IsAssigned_SpecifyParcels: Boolean;

    procedure OnChangeTypeRelated (Sender : TObject);

     // SpecifyRelated
    procedure SpecifyRelatedParcel_CreateNewItem(CadNum: string; Contour: integer = 0);
    procedure SpecifyRelatedParcel_UpDateList;
    procedure SpecifyRelatedParcel_Delete(index: Integer);
    procedure SpecifyRelatedParcel_SetCurrentItem(index: Integer);

    property XmlSpecifyRelatedParcelList: IXMLSTD_MP_Package_SpecifyParcel_SpecifyRelatedParcelList 
                                          read  FXmlSpecifyRelatedParcelList 
                                          write SetSpecifyRelatedParcelList;
  end;

implementation
 resourcestring
  rs7FCD7C89 = '�� ��������� ������ ����������� �����';
{$R *.dfm}

{ TFrameSpecifyRelatedsParcels }

procedure TFrameSpecifyRelatedsParcels.actSpecifyRelatedParcel_AddNewExecute(
  Sender: TObject);
var
  _CadNum: string;
  _Contour: string;
  _math: TMatch;
  IndexOfSelect_TypeSpecifyRelated : integer;
begin

 TFormsCadastralNumber.Instance.InputValue:= SpecifyRelatedParcelItems_ComboBox.Text;
 TFormsCadastralNumber.Instance.ShowModal;
 if TFormsCadastralNumber.Instance.ModalResult = mrOk then begin
   _CadNum := TFormsCadastralNumber.Instance.InputValue;

 _math:= TRegEx.Match(_CadNum,cnRegCadastralNumberAndContourIndex);
 if not _math.Success then raise Exception.CreateRes(@rs7FCD7C89);
 TRY
    _Contour:= _math.Groups.Item[cnCadNumItems[cnContour]].Value;
    _CadNum:= _math.Groups.Item[cnCadNumItems[cnFullCadNum]].Value;
 EXCEPT
    begin
     _Contour:= '';
     _CadNum:= '';
    end;
 END;
 _CadNum:= _math.Groups.Item[cnCadNumItems[cnFullCadNum]].Value;
{���������� ��� }
 IndexOfSelect_TypeSpecifyRelated := TSTD_MP_ListofIndexGenerate.IndexOfSelect_TypeSpecifyRelated;
 case IndexOfSelect_TypeSpecifyRelated of
   0 : self.FSpecifyParcelrelated.ActivSpecifyRelatedKind := cAllBorder;
   1 : self.FSpecifyParcelrelated.ActivSpecifyRelatedKind := cChangeBorder;
   2 : self.FSpecifyParcelrelated.ActivSpecifyRelatedKind := cContours;
   3 : self.FSpecifyParcelrelated.ActivSpecifyRelatedKind := cDeleteAllBorder
   else self.FSpecifyParcelrelated.ActivSpecifyRelatedKind := cUndefine; 
 end; 
 if _Contour<>'' then
  self.SpecifyRelatedParcel_CreateNewItem(_CadNum,StrToInt(_Contour))
 else
  self.SpecifyRelatedParcel_CreateNewItem(_CadNum);

 self.SpecifyRelatedParcel_SetCurrentItem(self.SpecifyRelatedParcelItems_ComboBox.Items.Count - 1);
 end;
end;

procedure TFrameSpecifyRelatedsParcels.actSpecifyRelatedParcel_DeleteExecute(
  Sender: TObject);
begin
  if MessageDlg('������� ������: '+' � ��������� �������� '+'?',mtInformation,mbYesNo,-1) = mrNo then Exit;
  self.SpecifyRelatedParcel_Delete(self.SpecifyRelatedParcelItems_ComboBox.ItemIndex);
end;

procedure TFrameSpecifyRelatedsParcels.actSpecifyRelatedParcel_EditDefinitionExecute(
  Sender: TObject);
var
  _CadNum: string;
  _Contour: string;
  _math: TMatch;
  newCadNum : string;
  item : Integer;
begin
  if not IsAssigned_SpecifyParcels then exit;
  if self.SpecifyRelatedParcelItems_ComboBox.Items.Count <=0  then Exit;

  TFormsCadastralNumber.Instance.InputValue := self.SpecifyRelatedParcelItems_ComboBox.Text;
  TFormsCadastralNumber.Instance.ShowModal;
  if TFormsCadastralNumber.Instance.ModalResult = mrOk then
  begin
      _CadNum := TFormsCadastralNumber.Instance.InputValue;
      _math := TRegEx.Match(_CadNum, cnRegCadastralNumberAndContourIndex);
      if not _math.Success then raise Exception.CreateRes(@rs7FCD7C89);
      try
         _Contour := _math.Groups.Item[cnCadNumItems[cnContour]].Value;
      except
         _Contour := '';
      end;
      _CadNum := _math.Groups.Item[cnCadNumItems[cnFullCadNum]].Value;
      newCadNum := _CadNum;
      item:= SpecifyRelatedParcelItems_ComboBox.ItemIndex;
      self.FXmlSpecifyRelatedParcelList.Items[SpecifyRelatedParcelItems_ComboBox.ItemIndex].CadastralNumber :=  newCadNum;
      if _Contour <> '' then
      begin
         self.FXmlSpecifyRelatedParcelList.Items[SpecifyRelatedParcelItems_ComboBox.ItemIndex].Number_Record := StrToInt(_Contour);
         newCadNum := newCadNum + '('+_Contour+')';
      end;
      SpecifyRelatedParcelItems_ComboBox.Items[item] := newCadNum;
      self.SpecifyRelatedParcel_SetCurrentItem(item);
  end;
end;

///
///  Activ Page ExistSubParcelspaeg
///
procedure TFrameSpecifyRelatedsParcels.btnEncumbranceClick(Sender: TObject);
begin
  if not IsAssigned_SpecifyParcels then Exit;
  if self.SpecifyRelatedParcelItems_ComboBox.Items.Count <= 0  then Exit;
  if self.FSpecifyParcelrelated.ActivSpecifyRelatedKind = cAllBorder then
     Self.FSpecifyParcelrelated.NewParcelsPageControl.ActivePage := self.FSpecifyParcelrelated.AllBorderTabSheet
  else
     if self.FSpecifyParcelrelated.ActivSpecifyRelatedKind = cChangeBorder then
       self.FSpecifyParcelrelated.NewParcelsPageControl.ActivePage := self.FSpecifyParcelrelated.Change_BorderTabSheet
     else
       if Self.FSpecifyParcelrelated.ActivSpecifyRelatedKind = cContours then
         self.FSpecifyParcelrelated.NewParcelsPageControl.ActivePage := self.FSpecifyParcelrelated.Contours_TabShet
       else
         if self.FSpecifyParcelrelated.ActivSpecifyRelatedKind = cDeleteAllBorder then
           self.FSpecifyParcelrelated.NewParcelsPageControl.ActivePage := self.FSpecifyParcelrelated.DeleteAllBorderTabSheet
         else  ShowMessage('� ������� ������ �� �������� ���������!');
  btnEncumbrance.Down := true;
  btnEntity_Spatial.Down := false;

end;

procedure TFrameSpecifyRelatedsParcels.btnEntity_SpatialClick(Sender: TObject);
begin
  if not IsAssigned_SpecifyParcels then Exit;
  if self.SpecifyRelatedParcelItems_ComboBox.Items.Count <= 0  then Exit;
  self.FSpecifyParcelrelated.NewParcelsPageControl.ActivePage := self.FSpecifyParcelrelated.ExistSubParcels_TabSheet;
  btnEntity_Spatial.Down := True;
  btnEncumbrance.Down := false;
end;

constructor TFrameSpecifyRelatedsParcels.Create(AOwner: TComponent);
begin
  inherited;
  // ���������� ��
  self.FSpecifyParcelrelated := TFrameSpecifyParcelrelated.Create
    (self.RelatedElemPanel);
  self.FSpecifyParcelrelated.Parent := self.RelatedElemPanel;
  self.FSpecifyParcelrelated.Align := alClient;
  self.FSpecifyParcelrelated.Visible := false;
  self.FSpecifyParcelrelated.OnChangeTypeRelated := self.OnChangeTypeRelated;
end;

function TFrameSpecifyRelatedsParcels.IsAssigned_SpecifyParcels: Boolean;
begin
 Result:= self.FXmlSpecifyRelatedParcelList<>nil;
end;

procedure TFrameSpecifyRelatedsParcels.OnChangeTypeRelated(Sender: TObject);
begin
  if not IsAssigned_SpecifyParcels then Exit;
  if self.SpecifyRelatedParcelItems_ComboBox.Items.Count <= 0  then Exit;
  btnEntity_Spatial.Visible := True;
  btnEncumbrance.Visible := true;
  if self.FSpecifyParcelrelated.ActivSpecifyRelatedKind = cAllBorder then
  begin
     self.btnEncumbrance.Caption := '������ ��������';
     self.btnEncumbrance.Hint:= '������ �������� ������� �������� ������� (������� ��������������� �������)';
  end
  else
     if self.FSpecifyParcelrelated.ActivSpecifyRelatedKind = cChangeBorder then
     begin
        self.btnEncumbrance.Caption := '��������� ����� �������';
        self.btnEncumbrance.Hint := '��������� ����� ������� (�� ����� �� �����). � ��� ����� ���������� ����������� ������� (�����)';
     end
     else
       if Self.FSpecifyParcelrelated.ActivSpecifyRelatedKind = cContours then
       begin
         self.btnEncumbrance.Caption:= '������� ��������������� ������';
         self.btnEncumbrance.Hint := '������� ��������������� ������� (���� ������� � ���������� ��������� ���������� ��������������)';
       end
       else
         if self.FSpecifyParcelrelated.ActivSpecifyRelatedKind = cDeleteAllBorder then
         begin
           self.btnEncumbrance.Caption := '���������� ������� �������';
           self.btnEncumbrance.Hint :='���������� ������� ������� ��������������� ������� (���������� �������)';
         end
         else begin
                 btnEntity_Spatial.Visible := false;
                 btnEncumbrance.Visible := false;
         end;
  btnEntity_Spatial.Down := False;
  btnEncumbrance.Down := true;
end;

procedure TFrameSpecifyRelatedsParcels.SetSpecifyRelatedParcelList(aValue: IXMLSTD_MP_Package_SpecifyParcel_SpecifyRelatedParcelList);
begin
 if self.FXmlSpecifyRelatedParcelList=aValue then EXIT;
 self.FXmlSpecifyRelatedParcelList:= aValue;

 self.SpecifyRelatedParcel_UpDateList;
 if self.FXmlSpecifyRelatedParcelList=nil then
  FSpecifyParcelrelated.XmlSpecifyRelated:= nil;
end;

procedure TFrameSpecifyRelatedsParcels.SpecifyRelatedParcelItems_ComboBoxChange(
  Sender: TObject);
begin
  self.SpecifyRelatedParcel_SetCurrentItem(self.SpecifyRelatedParcelItems_ComboBox.ItemIndex);
end;

procedure TFrameSpecifyRelatedsParcels.SpecifyRelatedParcel_CreateNewItem(
  CadNum: string; Contour: integer);
begin
  if not self.IsAssigned_SpecifyParcels then
    EXIT;
  self.SpecifyRelatedParcel_DoCreateNewItem(CadNum, Contour);
  self.SpecifyRelatedParcel_DoSetCurrentItem(self.SpecifyRelatedParcelItems_ComboBox.Items.Count - 1);
end;

procedure TFrameSpecifyRelatedsParcels.SpecifyRelatedParcel_Delete(
  index: Integer);
begin
   if (index > -1) and(index < self.SpecifyRelatedParcelItems_ComboBox.Items.Count) then
  begin
    if index = self.SpecifyRelatedParcelItems_ComboBox.ItemIndex then self.SpecifyRelatedParcel_SetCurrentItem(index-1);
    self.SpecifyRelatedParcel_DoDelete(index);
    self.SpecifyRelatedParcel_SetCurrentItem(0);
  end;
end;

procedure TFrameSpecifyRelatedsParcels.SpecifyRelatedParcel_DoCreateNewItem(CadNum: string; Contour: integer);
var
  _SpRelParcel: IXMLTSpecifyRelatedParcel;
begin
  _SpRelParcel := self.FXmlSpecifyRelatedParcelList.Add;

  _SpRelParcel.CadastralNumber := CadNum;
  if Contour>0 then begin
   _SpRelParcel.Number_Record:= Contour;
   CadNum:= CadNum+'('+IntToStr(Contour)+')';
  end;

  self.SpecifyRelatedParcelItems_ComboBox.AddItem(CadNum,pointer(_SpRelParcel));
end;

procedure TFrameSpecifyRelatedsParcels.SpecifyRelatedParcel_DoDelete(
  index: Integer);
var
  _SpRelParcel: IXMLTSpecifyRelatedParcel;
begin
  _SpRelParcel := IXMLTSpecifyRelatedParcel
    (pointer(self.SpecifyRelatedParcelItems_ComboBox.Items.Objects[index]));
  self.FXmlSpecifyRelatedParcelList.Remove(_SpRelParcel);
  self.SpecifyRelatedParcelItems_ComboBox.Items.Delete(index);
  _SpRelParcel := nil;
end;

procedure TFrameSpecifyRelatedsParcels.SpecifyRelatedParcel_DoSetCurrentItem(
  index: Integer);
var
  SpecifyRelatedParcel: IXMLTSpecifyRelatedParcel;
begin
 self.FSpecifyParcelrelated.XmlSpecifyRelated := IXMLTSpecifyRelatedParcel(pointer(self.SpecifyRelatedParcelItems_ComboBox.Items.Objects[index]));
end;

procedure TFrameSpecifyRelatedsParcels.SpecifyRelatedParcel_SetCurrentItem(
  index: Integer);
begin
  if (index > -1) and (index < self.SpecifyRelatedParcelItems_ComboBox.Items.Count) then
  begin
    self.SpecifyRelatedParcelItems_ComboBox.ItemIndex := index;
    self.SpecifyRelatedParcel_DoSetCurrentItem(index);
    self.FSpecifyParcelrelated.Visible := true;
    btnEntity_Spatial.Visible := True;
    btnEncumbrance.Visible := true;
  end
  else
  begin
    self.FSpecifyParcelrelated.XmlSpecifyRelated := nil;
    self.SpecifyRelatedParcelItems_ComboBox.ItemIndex := -1;
    self.SpecifyRelatedParcelItems_ComboBox.Text := '';
    self.FSpecifyParcelrelated.Visible := false;
    btnEntity_Spatial.Visible := false;
    btnEncumbrance.Visible := false;
  end;
end;

procedure TFrameSpecifyRelatedsParcels.SpecifyRelatedParcel_UpDateList;
var
  _index, _i: Integer;
  _CadNum: string;
  _SpRelParcel: IXMLTSpecifyRelatedParcel;
begin
  _index := self.SpecifyRelatedParcelItems_ComboBox.ItemIndex;
  self.SpecifyRelatedParcelItems_ComboBox.Clear;

  if self.IsAssigned_SpecifyParcels then
    for _i := 0 to self.FXmlSpecifyRelatedParcelList.Count - 1 do begin
      _SpRelParcel := self.FXmlSpecifyRelatedParcelList.Items[_i];
      _CadNum:= MsXml_ReadAttribute(_SpRelParcel,cnstAtrNumber_Record);
      if _CadNum<>'' then
       _CadNum:=  _SpRelParcel.CadastralNumber+'('+_CadNum+')'
      else
       _CadNum:=  _SpRelParcel.CadastralNumber;
      self.SpecifyRelatedParcelItems_ComboBox.AddItem(_CadNum, pointer(_SpRelParcel));
    end;
  if _index < 0 then
    _index := 0;
  self.SpecifyRelatedParcel_SetCurrentItem(_index);
end;


end.
