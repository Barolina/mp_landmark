object FrCheckTablesOrdinate: TFrCheckTablesOrdinate
  Left = 227
  Top = 108
  BorderIcons = []
  BorderStyle = bsToolWindow
  Caption = #1050#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1103' '#1074#1089#1090#1072#1074#1082#1080
  ClientHeight = 64
  ClientWidth = 276
  Color = clWhite
  ParentFont = True
  OldCreateOrder = True
  Position = poMainFormCenter
  Scaled = False
  ScreenSnap = True
  PixelsPerInch = 96
  TextHeight = 13
  object OKBtn: TButton
    Left = 120
    Top = 42
    Width = 75
    Height = 22
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 0
  end
  object CancelBtn: TButton
    Left = 201
    Top = 42
    Width = 75
    Height = 22
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 1
  end
  object pnlSettings: TPanel
    Left = 0
    Top = 0
    Width = 276
    Height = 43
    Align = alTop
    BevelEdges = []
    BevelKind = bkFlat
    BevelOuter = bvNone
    Color = 15921123
    ParentBackground = False
    TabOrder = 2
    object rbToTables: TRadioButton
      Left = 8
      Top = 8
      Width = 120
      Height = 31
      Caption = #1054#1073#1077#1080#1093' '#1090#1072#1073#1083#1080#1094
      Checked = True
      Color = 14924699
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      TabOrder = 0
      TabStop = True
    end
    object rbTable: TRadioButton
      Left = 115
      Top = 8
      Width = 152
      Height = 30
      Caption = #1042#1099#1073#1088#1072#1085#1085#1086#1081' '#1090#1072#1073#1083#1080#1094#1099
      Color = 14924699
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      TabOrder = 1
    end
  end
end
