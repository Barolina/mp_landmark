unit frAddress;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls,

  STD_MP, ActnList, System.Actions, Vcl.ExtCtrls,
  unRussLang, Vcl.ImgList,
  fmAllDocumentsMain, JvExControls, JvLinkLabel;

type
  TFrameAddress = class(TFrame)
    Region_Label_Caption: TLabel;
    ActionList: TActionList;
    Action_WriteRegion: TAction;
    Action_WriteNote: TAction;
    pnlLocation: TPanel;
    lblCod�_KLADR: TLabel;
    edtCod�_KLADR: TEdit;
    lblCode_OKATO: TLabel;
    edtCode_OKATO: TEdit;
    edtDistrict: TEdit;
    CategoryPanelGroup1: TCategoryPanelGroup;
    ctpnlCity: TCategoryPanel;
    ctpnlDistrict: TCategoryPanel;
    ctpnlUrban_District: TCategoryPanel;
    ctpnlLocality: TCategoryPanel;
    ctpnlNote: TCategoryPanel;
    ctpnlSoviet_Village: TCategoryPanel;
    edtCity: TEdit;
    edtUrban_District: TEdit;
    edtLocality: TEdit;
    edtSoviet_Village: TEdit;
    mmoNote: TMemo;
    ImageList: TImageList;
    btnRegion: TButtonedEdit;
    btnEdDistrict: TButtonedEdit;
    btnEdCity: TButtonedEdit;
    btnEdLocality: TButtonedEdit;
    jvlnklblOKato: TJvLinkLabel;
    ctgrypnlOther: TCategoryPanel;
    mmoOther: TMemo;
    ctgrypnlOtherGeneral: TCategoryPanel;
    ctgrypnlOtherStreet: TCategoryPanel;
    btndtStreet: TButtonedEdit;
    edtStreet: TEdit;
    edtLevel1: TEdit;
    btndtLevel1: TButtonedEdit;
    LabelStreet: TLabel;
    Label1: TLabel;
    edtLevel2: TEdit;
    btndtLevel2: TButtonedEdit;
    Label2: TLabel;
    edtLevel3: TEdit;
    btndtLevel3: TButtonedEdit;
    Label3: TLabel;
    edtPostal_Code: TEdit;
    Label4: TLabel;
    procedure _ParentSetFocus(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure btnRegionExit(Sender: TObject);
    procedure edtCode_OKATOExit(Sender: TObject);
    procedure edtCod�_KLADRExit(Sender: TObject);
    procedure edtDistrictExit(Sender: TObject);
    procedure edtCityExit(Sender: TObject);
    procedure edtUrban_DistrictExit(Sender: TObject);
    procedure edtSoviet_VillageExit(Sender: TObject);
    procedure edtLocalityExit(Sender: TObject);
    procedure mmoNoteExit(Sender: TObject);
    procedure btnEdDistrictRightButtonClick(Sender: TObject);
    procedure btnEdCityRightButtonClick(Sender: TObject);
    procedure btnEdLocalityRightButtonClick(Sender: TObject);
    procedure btnRegionRightButtonClick(Sender: TObject);
    procedure jvlnklblOKatoClick(Sender: TObject);
    procedure mmoOtherExit(Sender: TObject);
    procedure btndtStreetRightButtonClick(Sender: TObject);
    procedure edtStreetExit(Sender: TObject);
    procedure btndtLevel1RightButtonClick(Sender: TObject);
    procedure edtLevel1Exit(Sender: TObject);
    procedure btndtLevel2RightButtonClick(Sender: TObject);
    procedure btndtLevel3RightButtonClick(Sender: TObject);
    procedure lbledtPostal_CodeExit(Sender: TObject);
    procedure edtLevel2Exit(Sender: TObject);
    procedure edtLevel3Exit(Sender: TObject);

  public type
   //������ ��������������� ������ ������/������ ������ ������
   TController = record
    private
     [week] FView: TFrameAddress;
    public
     class function Create(aView: TFrameAddress): TController; static;
     procedure readRegion(const aModel: IXMLTAddress);
     procedure writeRegion(const aModel: IXMLTAddress);
     procedure readOKATO(const aModel: IXMLTAddress);
     procedure writeOKATO(const aModel: IXMLTAddress);
     procedure readKLADR(const aModel: IXMLTAddress);
     procedure writeKLADR(const aModel: IXMLTAddress);
     procedure readDistrict(const aModel: IXMLTAddress);
     procedure writeDistrict(const aModel: IXMLTAddress);
     procedure readCity(const aModel: IXMLTAddress);
     procedure writeCity(const aModel: IXMLTAddress);
     procedure readUrban_District(const aModel: IXMLTAddress);
     procedure writeUrban_District(const aModel: IXMLTAddress);
     procedure readSoviet_Village(const aModel: IXMLTAddress);
     procedure writeSoviet_Village(const aModel: IXMLTAddress);
     procedure readLocality(const aModel: IXMLTAddress);
     procedure writeLocality(const aModel: IXMLTAddress);
     procedure readNote(const aModel: IXMLTAddress);
     procedure writeNote(const aModel: IXMLTAddress);
     procedure readOther(const aModel: IXMLTAddress);
     procedure writeOther(const aModel: IXMLTAddress);

     procedure readPostal_Code(const aModel: IXMLTAddress);
     procedure writePostal_Code(const aModel: IXMLTAddress);
     procedure readStreet(const aModel: IXMLTAddress);
     procedure writeStreet(const aModel: IXMLTAddress);
     procedure readLevel1(const aModel: IXMLTAddress);
     procedure writeLevel1(const aModel: IXMLTAddress);
     procedure readLevel2(const aModel: IXMLTAddress);
     procedure writeLevel2(const aModel: IXMLTAddress);
     procedure readLevel3(const aModel: IXMLTAddress);
     procedure writeLevel3(const aModel: IXMLTAddress);


     property View: TFrameAddress read FView;
   end;

  private
    { Private declarations }
    FXmlAddress: IXMLTAddress;
    FController: TController;
    FdRegion: TFormAllDocumentsMain;
    FdDistrict: TFormAllDocumentsMain;
    FdCity:  TFormAllDocumentsMain;
    FdUrbanDistrict: TFormAllDocumentsMain;
    FdInhabitedLocalities: TFormAllDocumentsMain;
    FdStreet : TFormAllDocumentsMain;
    FdLevel1 : TFormAllDocumentsMain;
    FdLevel2 : TFormAllDocumentsMain;
    FdLevel3 : TFormAllDocumentsMain;
  protected
    function getdRegion: TFormAllDocumentsMain;
    function getdDistrict: TFormAllDocumentsMain;
    function getdCity:  TFormAllDocumentsMain;
    function getdUrbanDistrict: TFormAllDocumentsMain;
    function getdInhabitedLocalities: TFormAllDocumentsMain;
    function getdStreet : TFormAllDocumentsMain;
    function getdLevel1 : TFormAllDocumentsMain;
    function getdLevel2 : TFormAllDocumentsMain;
    function getdLevel3 : TFormAllDocumentsMain;

    procedure InitializationUI;

    procedure SetAddress(const aAddress: IXMLTAddress);

  public
    constructor Create(Owner: TComponent); override;

    { Public declarations }
    function IsAssignedAddress: Boolean;

    property dRegion: TFormAllDocumentsMain read getdRegion;
    property dDistrict: TFormAllDocumentsMain read getdDistrict;
    property dCity:  TFormAllDocumentsMain read getdCity;
    property dUrbanDistrict: TFormAllDocumentsMain read getdUrbanDistrict;
    property dInhabitedLocalities: TFormAllDocumentsMain read getdInhabitedLocalities;
    property dStreet : TFormAllDocumentsMain read getdStreet;
    property dLevel1 : TFormAllDocumentsMain read getdLevel1;
    property dLevel2 : TFormAllDocumentsMain read getdLevel2;
    property dLevel3 : TFormAllDocumentsMain read getdLevel3;
    property  XmlAddress: IXMLTAddress read FXmlAddress write SetAddress;
    property Controller: TController read  FController;
  end;

implementation

 uses ShellAPI;


{$R *.dfm}


{ TFrameAddress }



procedure TFrameAddress._ParentSetFocus(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
 if KEY=VK_RETURN then
 if (sender is TWinControl) and TWinControl(sender).Parent.HasParent then
  TWinControl(sender).Parent.SetFocus;
end;

procedure TFrameAddress.btndtLevel1RightButtonClick(Sender: TObject);
begin
 self.dLevel1.Value:= self.btndtLevel1.Text;
 self.dLevel1.ShowModal;
 if self.dLevel1.ModalResult=mrOk then begin
  self.btndtLevel1.Text:=  self.dLevel1.Value;
   self.Controller.writeLocality(self.XmlAddress);
 end;

end;

procedure TFrameAddress.btndtLevel2RightButtonClick(Sender: TObject);
begin
 self.dLevel2.Value:= self.btndtLevel2.Text;
 self.dLevel2.ShowModal;
 if self.dLevel2.ModalResult=mrOk then begin
  self.btndtLevel2.Text:=  self.dLevel2.Value;
   self.Controller.writeLocality(self.XmlAddress);
 end;

end;

procedure TFrameAddress.btndtLevel3RightButtonClick(Sender: TObject);
begin
 self.dLevel3.Value:= self.btndtLevel3.Text;
 self.dLevel3.ShowModal;
 if self.dLevel3.ModalResult=mrOk then begin
  self.btndtLevel3.Text:=  self.dLevel3.Value;
   self.Controller.writeLocality(self.XmlAddress);
 end;

end;

procedure TFrameAddress.btndtStreetRightButtonClick(Sender: TObject);
begin
 self.dStreet.Value:= self.btndtStreet.Text;
 self.dStreet.ShowModal;
 if self.dStreet.ModalResult=mrOk then begin
  self.btndtStreet.Text:=  self.dStreet.Value;
   self.Controller.writeLocality(self.XmlAddress);
 end;
end;

procedure TFrameAddress.btnEdCityRightButtonClick(Sender: TObject);
begin
 self.dCity.Value:=  self.btnEdCity.Text;
 self.dCity.ShowModal;
 if self.dCity.ModalResult=mrOk then begin
  self.btnEdCity.Text:= self.dCity.Value;
  self.Controller.writeCity(self.XmlAddress);
 end;
end;

procedure TFrameAddress.btnEdDistrictRightButtonClick(Sender: TObject);
begin
 self.dDistrict.Value:= self.btnEdDistrict.Text;
 self.dDistrict.ShowModal;
 if self.dDistrict.ModalResult=mrOk then begin
  self.btnEdDistrict.Text:= self.dDistrict.Value;
  self.Controller.writeDistrict(self.XmlAddress);
 end;
end;

procedure TFrameAddress.btnEdLocalityRightButtonClick(Sender: TObject);
begin
 self.dInhabitedLocalities.Value:= self.btnEdLocality.Text;
 self.dInhabitedLocalities.ShowModal;
 if self.dInhabitedLocalities.ModalResult=mrOk then begin
  self.btnEdLocality.Text:=  self.dInhabitedLocalities.Value;
   self.Controller.writeLocality(self.XmlAddress);
 end;
end;

procedure TFrameAddress.btnRegionExit(Sender: TObject);
begin
 self.Controller.writeRegion(self.XmlAddress);
end;


procedure TFrameAddress.btnRegionRightButtonClick(Sender: TObject);
begin
 self.dRegion.Value:= self.btnRegion.Text;
 self.dRegion.ShowModal;
 if self.dRegion.ModalResult=mrOk then begin
   self.btnRegion.Text:= self.dRegion.Value;
   self.Controller.writeRegion(self.XmlAddress);
 end;
end;

constructor TFrameAddress.Create(Owner: TComponent);
begin
 inherited;
 self.FController:= TFrameAddress.TController.Create(self);
end;

procedure TFrameAddress.edtCityExit(Sender: TObject);
begin
  self.Controller.writeCity(self.XmlAddress);
end;

procedure TFrameAddress.edtCode_OKATOExit(Sender: TObject);
begin
 self.Controller.writeOKATO(self.XmlAddress);
end;

procedure TFrameAddress.edtCod�_KLADRExit(Sender: TObject);
begin
 self.Controller.writeKLADR(self.XmlAddress);
end;

procedure TFrameAddress.edtDistrictExit(Sender: TObject);
begin
 self.Controller.writeDistrict(self.XmlAddress);
end;

procedure TFrameAddress.edtLevel1Exit(Sender: TObject);
begin
 self.Controller.writeLevel1(self.XmlAddress);
end;

procedure TFrameAddress.edtLevel2Exit(Sender: TObject);
begin
  self.Controller.writeLevel2(Self.XmlAddress);
end;

procedure TFrameAddress.edtLevel3Exit(Sender: TObject);
begin
 self.Controller.writeLevel3(self.XmlAddress);
end;

procedure TFrameAddress.edtLocalityExit(Sender: TObject);
begin
 self.Controller.writeLocality(self.XmlAddress);
end;

procedure TFrameAddress.edtSoviet_VillageExit(Sender: TObject);
begin
 self.Controller.writeSoviet_Village(self.XmlAddress);
end;

procedure TFrameAddress.edtStreetExit(Sender: TObject);
begin
 self.Controller.writeStreet(self.XmlAddress);
end;

procedure TFrameAddress.edtUrban_DistrictExit(Sender: TObject);
begin
 self.Controller.writeUrban_District(self.XmlAddress);
end;

function TFrameAddress.getdCity: TFormAllDocumentsMain;
begin
 if self.FdCity=nil then begin
  self.FdCity:= TFormAllDocumentsMain.Create(self);
  self.FdCity.LoadXSD(STD_MP_WorkDir+cnstdCity);
 end;
 Result:= self.FdCity;
end;

function TFrameAddress.getdDistrict: TFormAllDocumentsMain;
begin
 if self.FdDistrict=nil then begin
  self.FdDistrict:= TFormAllDocumentsMain.Create(self);
  self.FdDistrict.LoadXSD(STD_MP_WorkDir+cnstdDistrict);
 end;
 Result:= self.FdDistrict;
end;

function TFrameAddress.getdInhabitedLocalities: TFormAllDocumentsMain;
begin
 if self.FdInhabitedLocalities=nil then begin
  self.FdInhabitedLocalities:= TFormAllDocumentsMain.Create(self);
  self.FdInhabitedLocalities.LoadXSD(STD_MP_WorkDir+cnstdInhabitedLocalities);
 end;
 Result:= self.FdInhabitedLocalities;
end;

function TFrameAddress.getdLevel1: TFormAllDocumentsMain;
begin
 if self.Fdlevel1=nil then begin
  self.Fdlevel1:= TFormAllDocumentsMain.Create(self);
  self.Fdlevel1.LoadXSD(STD_MP_WorkDir+cnstdLevel1);
 end;
 Result:= self.Fdlevel1;
end;

function TFrameAddress.getdLevel2: TFormAllDocumentsMain;
begin
 if self.FdLevel2=nil then begin
  self.FdLevel2:= TFormAllDocumentsMain.Create(self);
  self.FdLevel2.LoadXSD(STD_MP_WorkDir+cnstdLevel2);
 end;
 Result:= self.FdLevel2;

end;

function TFrameAddress.getdLevel3: TFormAllDocumentsMain;
begin
 if self.FdLevel3=nil then begin
  self.FdLevel3:= TFormAllDocumentsMain.Create(self);
  self.FdLevel3.LoadXSD(STD_MP_WorkDir+cnstdLevel3);
 end;
 Result:= self.FdLevel3;
end;

function TFrameAddress.getdRegion: TFormAllDocumentsMain;
begin
 if self.FdRegion=nil then begin
  self.FdRegion:= TFormAllDocumentsMain.Create(self);
  self.FdRegion.LoadXSD(STD_MP_WorkDir+cnstdRegionsRF);
 end;
 Result:= self.FdRegion;
end;

function TFrameAddress.getdStreet: TFormAllDocumentsMain;
begin
 if self.FdStreet=nil then begin
  self.FdStreet:= TFormAllDocumentsMain.Create(self);
  self.FdStreet.LoadXSD(STD_MP_WorkDir+cnstdStreet);
 end;
 Result:= self.FdStreet;

end;

function TFrameAddress.getdUrbanDistrict: TFormAllDocumentsMain;
begin
 if self.FdUrbanDistrict=nil then begin
  self.FdUrbanDistrict:= TFormAllDocumentsMain.Create(self);
  self.FdUrbanDistrict.LoadXSD(STD_MP_WorkDir+cnstdUrbanDistrict);
 end;
 Result:= self.FdUrbanDistrict;
end;

procedure TFrameAddress.InitializationUI;
var
 _i: integer;
 _edit: TEdit;

begin
 if self.IsAssignedAddress then begin
  self.Controller.readRegion(self.XmlAddress);
  self.Controller.readOKATO(self.XmlAddress);
  self.Controller.readKLADR(self.XmlAddress);
  self.Controller.readDistrict(self.XmlAddress);
  self.Controller.readCity(self.XmlAddress);
  self.Controller.readUrban_District(self.XmlAddress);
  self.Controller.readSoviet_Village(self.XmlAddress);
  self.Controller.readPostal_Code(self.XmlAddress);
  self.Controller.readStreet(self.XmlAddress);
  self.Controller.readLevel1(self.XmlAddress);
  self.Controller.readLevel2(self.XmlAddress);
  self.Controller.readLevel3(self.XmlAddress);
  self.Controller.readLocality(self.XmlAddress);
  self.Controller.readNote(self.XmlAddress);
  self.Controller.readOther(self.XmlAddress);
 end else
 for _i:= 0 to self.ComponentCount-1 do
  if self.Components[_i] is TEdit then
  TEdit(self.Components[_i]).Text:= '';
end;

function TFrameAddress.IsAssignedAddress: Boolean;
begin
 Result:= self.FXmlAddress<>nil;
end;


procedure TFrameAddress.jvlnklblOKatoClick(Sender: TObject);
begin
  if ShellExecute(0, 'Open', 'http://kladr-rf.ru', nil, nil, SW_RESTORE) < 32 then
      raise Exception.Create('��� ���������� � ����������!');
end;

procedure TFrameAddress.lbledtPostal_CodeExit(Sender: TObject);
begin
  self.Controller.writePostal_Code(self.XmlAddress);
end;

procedure TFrameAddress.mmoNoteExit(Sender: TObject);
begin
 self.Controller.writeNote(self.XmlAddress);
end;

procedure TFrameAddress.mmoOtherExit(Sender: TObject);
begin
 self.Controller.writeOther(self.XmlAddress);
end;

procedure TFrameAddress.SetAddress(const aAddress: IXMLTAddress);
begin
 if self.FXmlAddress=aAddress then EXIT;
 self.FXmlAddress:= aAddress;
 self.InitializationUI;
end;


{ TFrameAddress.TController }

class function TFrameAddress.TController.Create(aView: TFrameAddress): TController;
begin
 Result.FView:= aView;
end;

procedure TFrameAddress.TController.readCity(const aModel: IXMLTAddress);
begin
 self.FView.edtCity.Text:= aModel.City.Name;
 self.FView.btnEdCity.Text:= self.FView.dCity.Documents.Key2Value(aModel.City.Type_);
end;

procedure TFrameAddress.TController.readDistrict(const aModel: IXMLTAddress);
begin
 self.FView.edtDistrict.Text:= aModel.District.Name;
 self.FView.btnEdDistrict.Text:= self.FView.dDistrict.Documents.Key2Value(aModel.District.Type_);
end;

procedure TFrameAddress.TController.readKLADR(const aModel: IXMLTAddress);
begin
 self.FView.edtCod�_KLADR.Text:= aModel.Code_KLADR;
end;

procedure TFrameAddress.TController.readLevel1(const aModel: IXMLTAddress);
begin
 self.FView.edtLevel1.Text:= aModel.Level1.Value;
 self.FView.btndtLevel1.Text:= self.FView.dLevel1.Documents.Key2Value(aModel.Level1.Type_);
end;

procedure TFrameAddress.TController.readLevel2(const aModel: IXMLTAddress);
begin
 self.FView.edtLevel2.Text:= aModel.Level2.Value;
 self.FView.btndtLevel2.Text:= self.FView.dLevel2.Documents.Key2Value(aModel.Level2.Type_);
end;

procedure TFrameAddress.TController.readLevel3(const aModel: IXMLTAddress);
begin
 self.FView.edtLevel3.Text:= aModel.Level3.Value;
 self.FView.btndtLevel3.Text:= self.FView.dLevel3.Documents.Key2Value(aModel.Level3.Type_);
end;

procedure TFrameAddress.TController.readLocality(const aModel: IXMLTAddress);
begin
 self.FView.edtLocality.Text:= aModel.Locality.Name;
 self.FView.btnEdLocality.Text:= self.FView.dInhabitedLocalities.Documents.Key2Value(aModel.Locality.Type_);
end;

procedure TFrameAddress.TController.readNote(const aModel: IXMLTAddress);
begin
 self.FView.mmoNote.Text:= aModel.Note;
end;

procedure TFrameAddress.TController.readOKATO(const aModel: IXMLTAddress);
begin
 self.FView.edtCode_OKATO.Text:= aModel.Code_OKATO;
end;

procedure TFrameAddress.TController.readOther(const aModel: IXMLTAddress);
begin
 self.FView.mmoOther.Text:= aModel.Other;
end;

procedure TFrameAddress.TController.readPostal_Code(const aModel: IXMLTAddress);
begin
 self.FView.edtPostal_Code.Text:= aModel.Postal_Code;
end;

procedure TFrameAddress.TController.readRegion(const aModel: IXMLTAddress);
begin
 self.FView.btnRegion.Text:= self.FView.dRegion.Documents.Key2Value(aModel.Region);
end;

procedure TFrameAddress.TController.readSoviet_Village( const aModel: IXMLTAddress);
begin
 self.FView.edtSoviet_Village.Text:= aModel.Soviet_Village.Name;
 //self.FView.btnEdUrban_District.Text:= self.FView.dUrbanDistrict.Documents.Key2Value(aModel.Urban_District.Type_);
end;

procedure TFrameAddress.TController.readStreet(const aModel: IXMLTAddress);
begin
 self.FView.edtStreet.Text:= aModel.Street.Name;
 self.FView.btndtStreet.Text:= self.FView.dStreet.Documents.Key2Value(aModel.Street.Type_);
end;

procedure TFrameAddress.TController.readUrban_District(const aModel: IXMLTAddress);
begin
 self.FView.edtUrban_District.Text:= aModel.Urban_District.Name;
end;

procedure TFrameAddress.TController.writeCity(const aModel: IXMLTAddress);
begin
 aModel.City.Name:= self.FView.edtCity.Text;
 aModel.City.Type_:= self.FView.dCity.Documents.Value2Key(self.FView.btnEdCity.Text);
end;

procedure TFrameAddress.TController.writeDistrict(const aModel: IXMLTAddress);
begin
 aModel.District.Name:= self.FView.edtDistrict.Text;
 aModel.District.Type_:= self.FView.dDistrict.Documents.Value2Key(self.FView.btnEdDistrict.Text);
end;

procedure TFrameAddress.TController.writeKLADR(const aModel: IXMLTAddress);
begin
 aModel.Code_KLADR:= self.FView.edtCod�_KLADR.Text;
end;

procedure TFrameAddress.TController.writeLevel1(const aModel: IXMLTAddress);
begin
 aModel.Level1.Value:= self.FView.edtLevel1.Text;
 aModel.Level1.Type_:= self.FView.dLevel1.Documents.Value2Key(self.FView.btndtLevel1.Text);
end;

procedure TFrameAddress.TController.writeLevel2(const aModel: IXMLTAddress);
begin
 aModel.Level2.Value:= self.FView.edtLevel2.Text;
 aModel.Level2.Type_:= self.FView.dLevel2.Documents.Value2Key(self.FView.btndtLevel2.Text);
end;

procedure TFrameAddress.TController.writeLevel3(const aModel: IXMLTAddress);
begin
 aModel.Level3.Value:= self.FView.edtLevel3.Text;
 aModel.Level3.Type_:= self.FView.dLevel3.Documents.Value2Key(self.FView.btndtLevel3.Text);
end;

procedure TFrameAddress.TController.writeLocality(const aModel: IXMLTAddress);
begin
 aModel.Locality.Name:= self.FView.edtLocality.Text;
 aModel.Locality.Type_:= self.FView.dInhabitedLocalities.Documents.Value2Key(self.FView.btnEdLocality.Text);
end;

procedure TFrameAddress.TController.writeNote(const aModel: IXMLTAddress);
begin
 aModel.Note:= self.FView.mmoNote.Text;
end;

procedure TFrameAddress.TController.writeOKATO(const aModel: IXMLTAddress);
begin
 aModel.Code_OKATO:= self.FView.edtCode_OKATO.Text;
end;

procedure TFrameAddress.TController.writeOther(const aModel: IXMLTAddress);
begin
 aModel.Other:= self.FView.mmoOther.Text;
end;

procedure TFrameAddress.TController.writePostal_Code(
  const aModel: IXMLTAddress);
begin
 aModel.Postal_Code:= self.FView.edtPostal_Code.Text;
end;

procedure TFrameAddress.TController.writeRegion(const aModel: IXMLTAddress);
begin
 aModel.Region:= self.FView.dRegion.Documents.Value2Key(self.FView.btnRegion.Text);
end;

procedure TFrameAddress.TController.writeSoviet_Village(
  const aModel: IXMLTAddress);
begin
 aModel.Soviet_Village.Name:= self.FView.edtSoviet_Village.Text;
end;

procedure TFrameAddress.TController.writeStreet(const aModel: IXMLTAddress);
begin
 aModel.Street.Name:= self.FView.edtStreet.Text;
 aModel.Street.Type_:= self.FView.dStreet.Documents.Value2Key(self.FView.btndtStreet.Text);
end;

procedure TFrameAddress.TController.writeUrban_District(const aModel: IXMLTAddress);
begin
 aModel.Urban_District.Name:= self.FView.edtUrban_District.Text;
end;

end.
