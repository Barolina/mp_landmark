object FormAllDocumentsMain: TFormAllDocumentsMain
  Left = 0
  Top = 0
  AlphaBlend = True
  BorderStyle = bsSizeToolWin
  Caption = #1050#1083#1072#1089#1089#1080#1092#1080#1082#1072#1090#1086#1088
  ClientHeight = 277
  ClientWidth = 471
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  ShowHint = True
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object VirtualStringTree1: TVirtualStringTree
    Left = 0
    Top = 0
    Width = 471
    Height = 251
    Align = alClient
    Header.AutoSizeIndex = 0
    Header.Font.Charset = DEFAULT_CHARSET
    Header.Font.Color = clWindowText
    Header.Font.Height = -11
    Header.Font.Name = 'Tahoma'
    Header.Font.Style = []
    Header.MainColumn = -1
    IncrementalSearch = isAll
    IncrementalSearchStart = ssAlwaysStartOver
    LineStyle = lsSolid
    NodeDataSize = 4
    TabOrder = 1
    TreeOptions.MiscOptions = [toAcceptOLEDrop, toFullRepaintOnResize, toInitOnSave, toToggleOnDblClick, toWheelPanning]
    TreeOptions.SelectionOptions = [toFullRowSelect]
    OnGetText = VirtualStringTree1GetText
    OnIncrementalSearch = VirtualStringTree1IncrementalSearch
    Columns = <>
  end
  object Panel1: TPanel
    Left = 0
    Top = 251
    Width = 471
    Height = 26
    Align = alBottom
    TabOrder = 0
    object SpeedButton1: TSpeedButton
      Left = 115
      Top = 1
      Width = 23
      Height = 24
      Align = alLeft
      Visible = False
      OnClick = SpeedButton1Click
      ExplicitLeft = 96
      ExplicitTop = 6
      ExplicitHeight = 22
    end
    object SpeedButton2: TSpeedButton
      Left = 138
      Top = 1
      Width = 23
      Height = 24
      Align = alLeft
      Visible = False
      OnClick = SpeedButton2Click
      ExplicitLeft = 93
      ExplicitTop = 6
      ExplicitHeight = 23
    end
    object Button2: TButton
      Left = 40
      Top = 1
      Width = 75
      Height = 24
      Align = alLeft
      Caption = 'C:\1.xsd'
      TabOrder = 2
      Visible = False
      OnClick = Button2Click
    end
    object btnOk: TBitBtn
      Left = 320
      Top = 1
      Width = 75
      Height = 24
      Align = alRight
      Caption = #1054#1082
      ModalResult = 1
      TabOrder = 1
    end
    object btnCancel: TBitBtn
      Left = 395
      Top = 1
      Width = 75
      Height = 24
      Align = alRight
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 0
    end
    object ButtonGroup1: TButtonGroup
      Left = 1
      Top = 1
      Width = 39
      Height = 24
      Align = alLeft
      BevelInner = bvNone
      BevelOuter = bvNone
      BorderStyle = bsNone
      Images = formContainer.ilCommands
      Items = <
        item
          Hint = #1057#1074#1077#1088#1085#1091#1090#1100' '#1101#1083#1077#1084#1077#1085#1090#1099' '#1089#1087#1080#1089#1082#1072
          ImageIndex = 5
          OnClick = ButtonGroup1Items0Click
        end>
      TabOrder = 3
    end
  end
end
