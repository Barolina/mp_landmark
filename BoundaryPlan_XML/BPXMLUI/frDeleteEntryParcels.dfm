object FrameDeleteEntryPArcels: TFrameDeleteEntryPArcels
  Left = 0
  Top = 0
  Width = 753
  Height = 364
  TabOrder = 0
  object PanelCentre: TPanel
    Left = 0
    Top = 0
    Width = 753
    Height = 364
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel1: TPanel
      Left = 0
      Top = 0
      Width = 753
      Height = 28
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object CadNum_NewCN_MaskEdit: TMaskEdit
        Left = 0
        Top = 0
        Width = 753
        Height = 28
        Hint = #1050#1072#1076#1072#1089#1090#1088#1086#1074#1099#1081' '#1085#1086#1084#1077#1088
        Align = alClient
        BevelKind = bkFlat
        BorderStyle = bsNone
        Color = 15921123
        ParentShowHint = False
        ShowHint = False
        TabOrder = 0
        Text = ''
        TextHint = #1050#1072#1076#1072#1089#1090#1088#1086#1074#1099#1081' '#1085#1086#1084#1077#1088
        StyleElements = []
      end
    end
    object CadNum_ListCN_ListBox_: TListBox
      Left = 0
      Top = 34
      Width = 700
      Height = 222
      Style = lbOwnerDrawFixed
      BevelInner = bvNone
      BevelKind = bkFlat
      BevelOuter = bvNone
      BorderStyle = bsNone
      Color = 15790320
      ItemHeight = 14
      MultiSelect = True
      TabOrder = 1
      StyleElements = []
    end
    object CadNum_ListCN_ListBox: TJvListBox
      Left = 0
      Top = 28
      Width = 552
      Height = 336
      Align = alClient
      BorderStyle = bsNone
      Color = clBtnFace
      ExtendedSelect = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Font.Quality = fqClearType
      ItemHeight = 30
      Background.FillMode = bfmTile
      Background.Visible = True
      Flat = True
      ParentFlat = False
      MultiSelect = True
      ParentFont = False
      ScrollBars = ssVertical
      Style = lbOwnerDrawFixed
      TabOrder = 2
    end
    object CadNum_ButtonGroup_Actions: TButtonGroup
      Left = 552
      Top = 28
      Width = 201
      Height = 336
      Align = alRight
      BevelEdges = [beLeft]
      BevelKind = bkFlat
      BorderStyle = bsNone
      ButtonOptions = [gboFullSize, gboShowCaptions]
      Images = formContainer.ilCommands
      Items = <
        item
          Action = Action_DoAdd
          Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1074' '#1089#1087#1080#1089#1086#1082' '
          Hint = #1044#1086#1073#1072#1074#1080#1090#1100' '#1074' '#1089#1087#1080#1089#1086#1082' '
        end
        item
          Action = Action_DelSel
          Caption = #1059#1076#1072#1083#1080#1090#1100' '#1074#1099#1076#1077#1083#1077#1085#1085#1099#1077
          Hint = #1059#1076#1072#1083#1080#1090#1100' '#1074#1099#1076#1077#1083#1077#1085#1085#1099#1077
          ImageIndex = 1
        end
        item
          Action = Action_CopySel
          Caption = #1050#1086#1087#1080#1088#1086#1074#1072#1090#1100' '#1074#1099#1076#1077#1083#1077#1085#1086#1077
          Hint = #1050#1086#1087#1080#1088#1086#1074#1072#1090#1100' '#1074#1099#1076#1077#1083#1077#1085#1086#1077
          ImageIndex = 3
        end
        item
          Action = Action_Paste
          Caption = #1042#1089#1090#1072#1074#1080#1090#1100' '#1089#1087#1080#1089#1086#1082' '#1080#1079' '#1073#1091#1092#1077#1088' '#1086#1073#1084#1077#1085#1072
          Hint = #1042#1089#1090#1072#1074#1080#1090#1100' '#1089#1087#1080#1089#1086#1082' '#1080#1079' '#1073#1091#1092#1077#1088' '#1086#1073#1084#1077#1085#1072
          ImageIndex = 2
        end>
      ShowHint = True
      TabOrder = 3
    end
  end
  object ActionList: TActionList
    Left = 144
    Top = 104
    object Action_DoAdd: TAction
      Caption = 'Action_DoAdd'
      ImageIndex = 0
      OnExecute = Action_DoAddExecute
    end
    object Action_DelSel: TAction
      Caption = 'Action_DelSel'
      ImageIndex = 2
      OnExecute = Action_DelSelExecute
    end
    object Action_CopySel: TAction
      Caption = 'Action_CopySel'
      ImageIndex = 1
      OnExecute = Action_CopySelExecute
    end
    object Action_Paste: TAction
      Caption = 'Action_Paste'
      ImageIndex = 3
      OnExecute = Action_PasteExecute
    end
  end
end
