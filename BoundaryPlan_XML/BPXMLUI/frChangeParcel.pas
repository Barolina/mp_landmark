unit frChangeParcel;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ButtonGroup, StdCtrls, ActnList, Vcl.Buttons, Vcl.ExtCtrls, Vcl.ComCtrls,
  PageControlAnimate,

  XMLDoc, XMLIntf, STD_MP,
  BpCommon,BpXML_different,
  ImgList,
  frInnerCadastralNumbers, frProvCadastralNumbers, unRussLang,
  frSubParcels, System.Actions,
  frDeleteEntryParcels;

type
  TFrameChangeParcel = class(TFrame)
    NewParcelsPageControl: TPageControl;
    ProvPassCadNums_TabSheet: TTabSheet;
    SubParcels_TabSheet: TTabSheet;
    InnerCadNums_TabSheet: TTabSheet;
    Panel_Navigation: TPanel;
    ComboBox_Nav_GoTo: TComboBox;
    Label17: TLabel;
    Label18: TLabel;
    SubParcels_LabelCaption: TLabel;
    ProvPassCadNums_IsFill_CheckBox: TCheckBox;
    InnerCadNums_IsFill_CheckBox: TCheckBox;
    SubParcels_IsFill_CheckBox: TCheckBox;

    ActionManager_FormParcels: TActionList;
    ActionXML_Attributes_Write_Name: TAction;
    ActionXML_Attributes_Write_AdditionalName: TAction;
    ActionXML_Attributes_Write_Method: TAction;
    ActionXML_Attributes_Write_Definition: TAction;
    ActionXML_CadastralBlock_Write: TAction;
    ActionXML_Category_Write: TAction;
    Action_PrevCadNums_IsFill: TAction;
    Action_ProvPassCadNums_IsFil: TAction;
    Action_InnerCadNums_IsFill: TAction;
    Action_LocationAddress_IsFill: TAction;
    Action_NaturalObject_IsFill: TAction;
    Action_SubParcels_IsFill: TAction;
    Action_Encumbrance_IsFill: TAction;
    btn_Nav_Back: TSpeedButton;
    btn_Nav_Next: TSpeedButton;
    pnlChangeParcel: TPanel;
    tsDeleteEntryParcels: TTabSheet;
    lblDeleteEntryParcerls: TLabel;
    chkDeleteEntryParcels: TCheckBox;
    actDeleteEntryParcels_IsFill: TAction;

    procedure btn_Nav_BackClick(Sender: TObject);
    procedure btn_Nav_NextClick(Sender: TObject);
    procedure ComboBox_Nav_GoToChange(Sender: TObject);

    procedure ActionXML_Attributes_Write_DefinitionExecute(Sender: TObject);
    procedure ActionXML_CadastralBlock_WriteExecute(Sender: TObject);
    procedure Action_ProvPassCadNums_IsFilExecute(Sender: TObject);
    procedure Action_InnerCadNums_IsFillExecute(Sender: TObject);
    procedure Action_SubParcels_IsFillExecute(Sender: TObject);

    procedure Attributes_Definition_EditKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Attributes_AdditionalName_EditKeyUp(Sender: TObject;
      var Key: Word; Shift: TShiftState);
    procedure medtCadastralBlock_value_MaskEditKeyUp(Sender: TObject;
      var Key: Word; Shift: TShiftState);
    procedure NewParcelsPageControlChange(Sender: TObject);
    procedure Label17Click(Sender: TObject);
    procedure Label18Click(Sender: TObject);
    procedure SubParcels_LabelCaptionClick(Sender: TObject);
    procedure actDeleteEntryParcels_IsFillExecute(Sender: TObject);
    procedure lblDeleteEntryParcerlsClick(Sender: TObject);

  private
    FXmlNewParcel          : IXMLTChangeParcel; // ������� ��������� �������

    FProvPassCadlNums_frame: TFrameProvCadastralNumber;
    FInnerCadNums_frame    : TFrameInnerCadastralNumbers; // �������� ��
    FSubParcels_frame      : TFrameSubParcels; // ����� ��
    FDeleteEntryParcels_frame : TFrameDeleteEntryPArcels;

    FOnChangeDefinition    : TNotifyEvent;

    procedure InitializationUI; // ���������������� ����������

    // �� ����� ���� ��� ��� ������� ��������������/�������������
    procedure UI_Attributes_Name_Write;
    procedure UI_Attributes_Name_Read;
    // ����������� ��
    procedure UI_Attributes_Definition_Write;
    procedure UI_Attributes_Definition_Read;
    function UI_Attributes_Definition_IsEmpty: Boolean;
    // ��� �������� ������/������
    procedure UI_Attributes_Write;
    procedure UI_Attributes_Read;
    // ����������� �������
    procedure UI_CadastralBlock_Read;
    procedure UI_CadastralBlock_Write;

    // ��������� ������� NewParcel
    procedure SetNewParcel(const aNewParcel: IXMLTChangeParcel);
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    property XmlChangeParcel: IXMLTChangeParcel read FXmlNewParcel write SetNewParcel;
    procedure SetActivTab(index: integer; UseAnimate: Boolean = false);
    function IsAssigned_ChangeParcel: Boolean;

    Property OnChangeDefinition: TNotifyEvent read FOnChangeDefinition
      write FOnChangeDefinition;
  end;

implementation

uses MsXMLAPI;

procedure _HiddenChangeCheckBoxSate(ChBox: TCheckBox; NewSate: TCheckBoxState);
var
  _OnClick: TNotifyEvent;
begin
  if ChBox.State = NewSate then
    EXIT;
  _OnClick := ChBox.OnClick;
  ChBox.OnClick := nil;
  ChBox.State := NewSate;
  ChBox.OnClick := _OnClick;
end;

{$R *.dfm}
{ TFrameNewparcel }

procedure TFrameChangeParcel.actDeleteEntryParcels_IsFillExecute(
  Sender: TObject);
begin
 if not chkDeleteEntryParcels.Checked then
  begin
     if FDeleteEntryParcels_frame.XmlCadastralNumbers <> nil then
        if MessageDlg('������� ������: '+lblDeleteEntryParcerls.Caption+'?',mtInformation,mbYesNo,-1) = mrYes then
          begin
            MsXml_RemoveChildNode(self.FXmlNewParcel,'DeleteEntryParcels');
            self.FDeleteEntryParcels_frame.XmlCadastralNumbers := nil;
            self.FDeleteEntryParcels_frame.Visible := false;
          end
        else chkDeleteEntryParcels.Checked := true;
  end else
  begin
    self.FDeleteEntryParcels_frame.XmlCadastralNumbers := self.FXmlNewParcel.DeleteEntryParcels;
    self.FDeleteEntryParcels_frame.Visible := true;
  end
end;

procedure TFrameChangeParcel.ActionXML_Attributes_Write_DefinitionExecute
  (Sender: TObject);
begin
  if not self.IsAssigned_ChangeParcel then
    raise Exception.Create(ExMSG_NotAssignedROOT);
  self.UI_Attributes_Definition_Write;
end;

procedure TFrameChangeParcel.ActionXML_CadastralBlock_WriteExecute
  (Sender: TObject);
begin
  if not self.IsAssigned_ChangeParcel then
    raise Exception.Create(ExMSG_NotAssignedROOT);
  self.UI_CadastralBlock_Write;
end;

procedure TFrameChangeParcel.Action_InnerCadNums_IsFillExecute(Sender: TObject);
begin
  if not InnerCadNums_IsFill_CheckBox.Checked  then
  begin
     if self.FXmlNewParcel.ChildNodes.FindNode(cnstXmlInner_CadastralNumbers)<>nil then
      if MessageDlg('������� ������: '+Label18.Caption+'?',mtInformation,mbYesNo,-1) = mrYes then
        begin
          MsXml_RemoveChildNode(self.FXmlNewParcel,cnstXmlInner_CadastralNumbers);
          self.FInnerCadNums_frame.XmlCadastralNumbersChange := nil;
          self.FInnerCadNums_frame.Visible := false;
        end else InnerCadNums_IsFill_CheckBox.Checked := true;
  end
  else
  begin
    self.FInnerCadNums_frame.XmlCadastralNumbersChange :=
      self.FXmlNewParcel.Inner_CadastralNumbers;
    self.FInnerCadNums_frame.Visible := true;
  end
end;


procedure TFrameChangeParcel.Action_ProvPassCadNums_IsFilExecute(Sender: TObject);
begin
  if not ProvPassCadNums_IsFill_CheckBox.Checked then
  begin
     if FProvPassCadlNums_frame.XmlCadastralNumbers <> nil then
        if MessageDlg('������� ������: '+Label17.Caption+'?',mtInformation,mbYesNo,-1) = mrYes then
         begin
            MsXml_RemoveChildNode(self.FXmlNewParcel,'Providing_Pass_CadastralNumbers');
            self.FProvPassCadlNums_frame.XmlCadastralNumbers := nil;
            self.FProvPassCadlNums_frame.Visible := false;
          end else ProvPassCadNums_IsFill_CheckBox.Checked := True;
  end
  else
  begin
    self.FProvPassCadlNums_frame.XmlCadastralNumbers :=
      self.FXmlNewParcel.Providing_Pass_CadastralNumbers;
    self.FProvPassCadlNums_frame.Visible := true;
  end
end;

procedure TFrameChangeParcel.Action_SubParcels_IsFillExecute(Sender: TObject);
begin
  if not SubParcels_IsFill_CheckBox.Checked then
  begin
     if FSubParcels_frame.XmlSubParcels <> nil then
        if MessageDlg('������� ������: '+SubParcels_LabelCaption.Caption+'?',mtInformation,mbYesNo,-1) = mrYes then
          begin
            MsXml_RemoveChildNode(self.FXmlNewParcel,cnstXmlSubParcels);
            self.FSubParcels_frame.XmlSubParcels := nil;
            self.FSubParcels_frame.Visible := false;
          end else SubParcels_IsFill_CheckBox.Checked := true;
  end else
  begin
    self.FSubParcels_frame.XmlSubParcels := self.FXmlNewParcel.SubParcels;
    self.FSubParcels_frame.Visible := true;
  end
end;

procedure TFrameChangeParcel.Attributes_AdditionalName_EditKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key = VK_RETURN then
    if (Sender is TCustomEdit) then
      TCustomEdit(Sender).Parent.SetFocus;
end;

procedure TFrameChangeParcel.Attributes_Definition_EditKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key = VK_RETURN then
    if (Sender is TCustomEdit) then
      TCustomEdit(Sender).Parent.SetFocus;
end;

procedure TFrameChangeParcel.medtCadastralBlock_value_MaskEditKeyUp
  (Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if Key = VK_RETURN then
    if (Sender is TCustomEdit) then
      TCustomEdit(Sender).Parent.SetFocus;
end;

procedure TFrameChangeParcel.ComboBox_Nav_GoToChange(Sender: TObject);
begin
  self.SetActivTab(self.ComboBox_Nav_GoTo.ItemIndex, false);
end;

constructor TFrameChangeParcel.Create(AOwner: TComponent);
begin
  inherited;
  self.NewParcelsPageControl.ActivePageIndex := 0;
  // NA
  self.FProvPassCadlNums_frame := TFrameProvCadastralNumber.Create
    (self.ProvPassCadNums_TabSheet);
  self.FProvPassCadlNums_frame.Parent := self.ProvPassCadNums_TabSheet;
  self.FProvPassCadlNums_frame.Align := alClient;

  // NA
  self.FInnerCadNums_frame := TFrameInnerCadastralNumbers.Create
    (self.InnerCadNums_TabSheet);
  self.FInnerCadNums_frame.Parent := self.InnerCadNums_TabSheet;
  self.FInnerCadNums_frame.Align := alClient;

  self.FSubParcels_frame := TFrameSubParcels.Create(self.SubParcels_TabSheet);
  self.FSubParcels_frame.Parent := self.SubParcels_TabSheet;
  self.FSubParcels_frame.Align := alClient;

  self.FDeleteEntryParcels_frame := TFrameDeleteEntryPArcels.Create(self.tsDeleteEntryParcels);
  self.FDeleteEntryParcels_frame.Parent := self.tsDeleteEntryParcels;
  self.FDeleteEntryParcels_frame.Align := alClient;


end;

procedure TFrameChangeParcel.InitializationUI;
var
  _node: IXMLNode;
begin
  if self.IsAssigned_ChangeParcel then
  begin
    self.NewParcelsPageControl.Visible := true;
    self.Panel_Navigation.Visible := true;

    self.UI_Attributes_Read;
    self.UI_CadastralBlock_Read;
    _node := self.FXmlNewParcel.ChildNodes.FindNode('Providing_Pass_CadastralNumbers');
    if _node <> nil then
    begin
      self.FProvPassCadlNums_frame.XmlCadastralNumbers :=
        self.FXmlNewParcel.Providing_Pass_CadastralNumbers;
      self.FProvPassCadlNums_frame.Visible := true;
      _HiddenChangeCheckBoxSate(self.ProvPassCadNums_IsFill_CheckBox,
        cbChecked);
    end
    else
    begin
      self.FProvPassCadlNums_frame.XmlCadastralNumbers := nil;
      self.FProvPassCadlNums_frame.Visible := false;
      _HiddenChangeCheckBoxSate(self.ProvPassCadNums_IsFill_CheckBox,
        cbUnchecked);
    end;

    _node := self.FXmlNewParcel.ChildNodes.FindNode(cnstXmlInner_CadastralNumbers);
    if _node <> nil then
    begin
      self.FInnerCadNums_frame.XmlCadastralNumbersChange :=
        self.FXmlNewParcel.Inner_CadastralNumbers;
      self.FInnerCadNums_frame.Visible := true;
      _HiddenChangeCheckBoxSate(self.InnerCadNums_IsFill_CheckBox, cbChecked);
    end
    else
    begin
      self.FInnerCadNums_frame.XmlCadastralNumbersChange := nil;
      self.FInnerCadNums_frame.Visible := false;
      _HiddenChangeCheckBoxSate(self.InnerCadNums_IsFill_CheckBox, cbUnchecked);
    end;


    _node := self.FXmlNewParcel.ChildNodes.FindNode(cnstXmlSubParcels);
    if _node <> nil then
    begin
      self.FSubParcels_frame.XmlSubParcels := self.FXmlNewParcel.SubParcels;
      self.FSubParcels_frame.Visible := true;
      _HiddenChangeCheckBoxSate(self.SubParcels_IsFill_CheckBox, cbChecked);
    end
    else
    begin
      self.FSubParcels_frame.XmlSubParcels := nil;
      self.FSubParcels_frame.Visible := false;
      _HiddenChangeCheckBoxSate(self.SubParcels_IsFill_CheckBox, cbUnchecked);
    end;

     _node := self.FXmlNewParcel.ChildNodes.FindNode('DeleteEntryParcels');
    if _node <> nil then
    begin
      self.FDeleteEntryParcels_frame.XmlCadastralNumbers := self.FXmlNewParcel.DeleteEntryParcels;
      self.FDeleteEntryParcels_frame.Visible := true;
      _HiddenChangeCheckBoxSate(self.chkDeleteEntryParcels, cbChecked);
    end
    else
    begin
      self.FDeleteEntryParcels_frame.XmlCadastralNumbers := nil;
      self.FDeleteEntryParcels_frame.Visible := false;
      _HiddenChangeCheckBoxSate(self.chkDeleteEntryParcels, cbUnchecked);
    end;
   end
  else
  begin
    self.NewParcelsPageControl.Visible := false;
    self.Panel_Navigation.Visible := false;
    self.FProvPassCadlNums_frame.XmlCadastralNumbers := nil;
    self.FInnerCadNums_frame.XmlCadastralNumbers := nil;
    self.FSubParcels_frame.XmlSubParcels := nil;
    self.FDeleteEntryParcels_frame.XmlCadastralNumbers := nil;
  end;

end;

function TFrameChangeParcel.IsAssigned_ChangeParcel: Boolean;
begin
  Result := self.FXmlNewParcel <> nil;
end;

procedure TFrameChangeParcel.Label17Click(Sender: TObject);
begin
 self.ProvPassCadNums_IsFill_CheckBox.Checked := not self.ProvPassCadNums_IsFill_CheckBox.Checked;
end;

procedure TFrameChangeParcel.Label18Click(Sender: TObject);
begin
 self.InnerCadNums_IsFill_CheckBox.Checked := not self.InnerCadNums_IsFill_CheckBox.Checked;
end;

procedure TFrameChangeParcel.lblDeleteEntryParcerlsClick(Sender: TObject);
begin
 self.chkDeleteEntryParcels.Checked := not self.chkDeleteEntryParcels.Checked;
end;

procedure TFrameChangeParcel.NewParcelsPageControlChange(Sender: TObject);
begin
  self.btn_Nav_Back.Visible :=
    self.NewParcelsPageControl.ActivePageIndex = 0;
  self.btn_Nav_Next.Visible :=
    self.NewParcelsPageControl.ActivePageIndex = self.NewParcelsPageControl.
    PageCount - 1;

end;

procedure TFrameChangeParcel.SetActivTab(index: integer; UseAnimate: Boolean=false);
begin
  if (index < 0) or (index > self.NewParcelsPageControl.PageCount - 1) then
    EXIT;

  if UseAnimate then
    pcaSetActivTab(self.NewParcelsPageControl,
      self.NewParcelsPageControl.Pages[index])
  else
    self.NewParcelsPageControl.ActivePageIndex := index;

  self.ComboBox_Nav_GoTo.ItemIndex :=
    self.NewParcelsPageControl.ActivePageIndex;
  self.btn_Nav_Back.Enabled := self.ComboBox_Nav_GoTo.ItemIndex <> 0;
  self.btn_Nav_Next.Enabled := self.ComboBox_Nav_GoTo.ItemIndex <>
    self.ComboBox_Nav_GoTo.Items.Count - 1;
end;

procedure TFrameChangeParcel.SetNewParcel(const aNewParcel: IXMLTChangeParcel);
begin
  if self.FXmlNewParcel = aNewParcel then
    EXIT;
  self.FXmlNewParcel := aNewParcel;
  self.InitializationUI;
end;

procedure TFrameChangeParcel.SubParcels_LabelCaptionClick(Sender: TObject);
begin
 self.SubParcels_IsFill_CheckBox.Checked := not self.SubParcels_IsFill_CheckBox.Checked;
end;

procedure TFrameChangeParcel.btn_Nav_BackClick(Sender: TObject);
begin
  self.SetActivTab(self.ComboBox_Nav_GoTo.ItemIndex - 1, false);
end;

procedure TFrameChangeParcel.btn_Nav_NextClick(Sender: TObject);
begin
  self.SetActivTab(self.ComboBox_Nav_GoTo.ItemIndex + 1, false);
end;

procedure TFrameChangeParcel.UI_CadastralBlock_Read;
begin
end;

procedure TFrameChangeParcel.UI_CadastralBlock_Write;
begin
end;


procedure TFrameChangeParcel.UI_Attributes_Read;
begin
  self.UI_Attributes_Definition_Read;
  self.UI_Attributes_Name_Read;
end;

function TFrameChangeParcel.UI_Attributes_Definition_IsEmpty: Boolean;
begin
end;

procedure TFrameChangeParcel.UI_Attributes_Definition_Read;
begin
end;


procedure TFrameChangeParcel.UI_Attributes_Name_Read;
var
  _index: integer;
  _st: string;
begin
  _st := MsXml_ReadAttribute(self.FXmlNewParcel, 'Name');
  if TryStrToInt(_st, _index) then
  begin
    case _index of
      1:
        _index := 0;
      5:
        _index := 1;
    end;
  end
  else
    _index := -1;
end;

procedure TFrameChangeParcel.UI_Attributes_Write;
begin
  self.FXmlNewParcel.AttributeNodes.Clear;
  self.UI_Attributes_Name_Write;
  self.UI_Attributes_Definition_Write;
end;

procedure TFrameChangeParcel.UI_Attributes_Definition_Write;
begin
end;

procedure TFrameChangeParcel.UI_Attributes_Name_Write;

begin
end;

end.
