unit frClients;
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, ButtonGroup, StdCtrls, ActnList, Vcl.ImgList, Vcl.ExtCtrls,

  STD_MP, BpXML_different,frClient,fmContainer;

type
  TFrameClients = class(TFrame)
    PanelTopControl: TPanel;
    StatuseSubParcel_Lab: TLabel;
    SubParcels_Actions_ButtonGroup: TButtonGroup;
    Panel: TPanel;
    cbClientList: TListBox;
    procedure SubParcels_Actions_ButtonGroupItems0Click(Sender: TObject);
    procedure SubParcels_Actions_ButtonGroupItems1Click(Sender: TObject);
    procedure cbClientListChange(Sender: TObject);
  private
    { Private declarations }
   FXmlClients: IXMLSTD_MP_Title_ClientList;
   FClient    : TFrameClient;

   procedure Data_Client_DoAddNew;

   procedure Data_Client_DoDelete(index: Integer);
   procedure ClientsList_DoUpDate;

  protected
    procedure SetClients(const aClients: IXMLSTD_MP_Title_ClientList);
    procedure InitializationUI;

  public
    { Public declarations }

   constructor Create(Owner: TComponent); override;
   function  IsAssigned_Clients: Boolean;
   procedure ClientsList_UpDate;
   procedure ClientsList_SetCurrent(index: Integer);

   property XmlClients: IXMLSTD_MP_Title_ClientList read FXmlClients write SetClients;
   property Client: TFrameClient read FClient;

  end;

implementation

{$R *.dfm}

{ TFrameClients }

procedure TFrameClients.cbClientListChange(Sender: TObject);
begin
 self.ClientsList_SetCurrent(self.cbClientList.ItemIndex);
end;

procedure TFrameClients.ClientsList_DoUpDate;
var
 _index: Integer;
 _i: Integer;
 Client: IXMLSTD_MP_Title_Client;
 _ItemName: string;
begin
 _index:= self.cbClientList.ItemIndex;
 self.cbClientList.Items.Clear;
 for _i := 0 to self.FXmlClients.Count-1 do begin
  Client:= self.FXmlClients.Items[_i];
 _ItemName:= inttostr(_i+1);
  self.cbClientList.AddItem(_ItemName,pointer(Client));
 end;

 self.ClientsList_SetCurrent(_index);
 if not self.FClient.IsAssignedClient then
  self.ClientsList_SetCurrent(0);
end;

procedure TFrameClients.ClientsList_SetCurrent(index: Integer);
begin
if (index>-1) and (index<self.cbClientList.Items.Count) then begin
  self.FClient.XMlClient:= IXMLSTD_MP_Title_Client(pointer(self.cbClientList.Items.Objects[index]));
  self.FClient.Visible:= true;
  self.cbClientList.ItemIndex:= index;
 end else 
 begin
  self.FClient.Visible  := false;
  self.FClient.XMlClient:= nil;
  self.cbClientList.ItemIndex:= -1;  
 end;
end;

procedure TFrameClients.ClientsList_UpDate;
begin
 if self.IsAssigned_Clients then
  self.ClientsList_DoUpDate;
end;

constructor TFrameClients.Create(Owner: TComponent);
begin
  inherited;
  self.InitializationUI;
  self.FClient:= TFrameClient.Create(self.Panel);
  self.FClient.Parent:= self.Panel;
  self.FClient.Align:= alClient;
  self.FClient.Visible:= false;
end;

procedure TFrameClients.Data_Client_DoAddNew;
var
 _Client: IXMLSTD_MP_Title_Client;
 _ItemName: string;
begin
  _Client:= self.FXmlClients.Add;
  _ItemName := IntToStr(FXmlClients.Count);
   self.cbClientList.AddItem(_ItemName,pointer(_Client));
end;

procedure TFrameClients.Data_Client_DoDelete(index: Integer);
var
 Client: IXMLSTD_MP_Title_Client;
begin
 Client:= IXMLSTD_MP_Title_Client(pointer(self.cbClientList.Items.Objects[index]));
 if self.FClient.XMlClient=Client then
  self.ClientsList_SetCurrent(index-1);
 self.cbClientList.Items.Delete(index);
 self.FXmlClients.Remove(Client);
 Client:= nil;
end;

procedure TFrameClients.InitializationUI;
begin
   self.cbClientList.Clear;
end;

function TFrameClients.IsAssigned_Clients: Boolean;
begin
Result := FXmlClients <> nil;
end;

procedure TFrameClients.SetClients(const aClients: IXMLSTD_MP_Title_ClientList);
begin
 self.ClientsList_SetCurrent(-1);
 self.FXmlClients:= aClients;
 self.InitializationUI;
 if self.IsAssigned_Clients then
  self.ClientsList_DoUpDate;
end;

procedure TFrameClients.SubParcels_Actions_ButtonGroupItems0Click(
  Sender: TObject);
  var indexType :  integer;
begin
  if not self.IsAssigned_Clients then EXIT;
  Self.Data_Client_DoAddNew;
  self.ClientsList_SetCurrent(self.cbClientList.Items.Count-1);
  indexType := TSTD_MP_ListofIndexGenerate.IndexOfSelect_TypeClient;
  case indexType of
    0 : self.FClient.SetCurrentPerson;
    1 : self.FClient.SetCurrentOrganisation;
    2 : self.FClient.SetCurrentForeign_Organisation;
    3 : self.FClient.SetCurrentGovernance;
    -1: self.FClient.SetCurrentGovernance;
  end;
end;

procedure TFrameClients.SubParcels_Actions_ButtonGroupItems1Click(
  Sender: TObject);
begin
 if not self.FClient.IsAssignedClient then EXIT;
 if MessageDlg('������� ������: '+Caption+'?',mtInformation,mbYesNo,-1) = mrNo then Exit;

 self.Data_Client_DoDelete(self.cbClientList.ItemIndex);
 self.ClientsList_SetCurrent(self.cbClientList.ItemIndex);
 if not self.FClient.IsAssignedClient then
  self.ClientsList_SetCurrent(0);
end;

end.
