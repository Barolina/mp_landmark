unit frChangeParcels;

{
 TODO: ���� �����
}
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, ActnList, StdCtrls, ComCtrls, Buttons, Mask, ExtCtrls,

  PageControlAnimate, STD_MP, frPrevCadastralNumbers, frSubParcels, frEncumbrances, XMLIntf,
  BpXML_different, ImgList, BPCommon, ButtonGroup, System.Actions, frCadastralNumbers,
  frArea, frAreaDelta,frRelatedParcels,fmComposition_EZ;

type
  TFrameExistEZParcels = class(TFrame)
    ChangeParcelsPageControl: TPageControl;
    ProvPassCadNums_TabSheet: TTabSheet;
    Label17: TLabel;
    ProvPassCadNums_IsFill_CheckBox: TCheckBox;
    InnerCadNums_TabSheet: TTabSheet;
    Label18: TLabel;
    InnerCadNums_IsFill_CheckBox: TCheckBox;
    SubParcels_TabSheet: TTabSheet;
    SubParcels_LabelCaption: TLabel;
    SubParcels_IsFill_CheckBox: TCheckBox;
    Encumbrance_TabSheet: TTabSheet;
    Label5: TLabel;
    Encumbrance_IsFill_CheckBox: TCheckBox;
    ActionList: TActionList;
    Panel_Navigation: TPanel;
    SpeedButton_Nav_Back: TSpeedButton;
    SpeedButton_Nav_Next: TSpeedButton;
    ComboBox_Nav_GoTo: TComboBox;
    Action_ProvPassCadNums_IsFill: TAction;
    Action_InnerCadNums_IsFill: TAction;
    Action_SubParcels_IsFill: TAction;
    Action_Encumbrance_IsFill: TAction;
    ImageList: TImageList;

    procedure SpeedButton_Nav_BackClick(Sender: TObject);
    procedure SpeedButton_Nav_NextClick(Sender: TObject);
    procedure ComboBox_Nav_GoToChange(Sender: TObject);
    procedure Action_InnerCadNums_IsFillExecute(Sender: TObject);
    procedure Action_SubParcels_IsFillExecute(Sender: TObject);
    procedure Action_Encumbrance_IsFillExecute(Sender: TObject);
  private
    { Private declarations }
    FXmlExistEZParcel :  IXMLTExistEZParcel; //������ ����������������

    FInnerCadNums_frame: TFrameCadastralNumbers;
    FArea              : TFrameArea;
    FArea_Delta        : TFrameAreaDelta;
    FSubParcels_frame  : TFrameSubParcels;
    FRealtyParcels     : TFrameRelatedParcels;
    FCompositionEZ     : TFrameCompostion_EZ;

  protected
    procedure InitializationUI;
    procedure SetExistEZParcel(const aExistEZParcel: IXMLTExistEZParcel);
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    function IsAssignedChangeParcel: Boolean;
    property XmlExistEZParcel: IXMLTExistEZParcel read FXmlExistEZParcel write SetExistEZParcel;
  end;

implementation

uses MsXMLAPI;

{$R *.dfm}


procedure TFrameExistEZParcels.Action_Encumbrance_IsFillExecute(Sender: TObject);
begin
{ if self.Encumbrance_IsFill_CheckBox.Checked then begin
   self.FEncumbramces_frame.XmlEncumbrances:= self.FXmlChangeParcel.Encumbrances;
   self.FEncumbramces_frame.Visible:= true;
  end else begin
   MsXml_RemoveChildNode(self.FXmlChangeParcel,'Encumbrances');
   self.FEncumbramces_frame.XmlEncumbrances:= nil;
   self.FEncumbramces_frame.Visible:= false;
  end;}
end;

procedure TFrameExistEZParcels.Action_InnerCadNums_IsFillExecute(Sender: TObject);
begin
if self.InnerCadNums_IsFill_CheckBox.Checked then begin
   self.FInnerCadNums_frame.XmlCadastralNumbers:= self.FXmlExistEZParcel.Inner_CadastralNumbers;
   self.FInnerCadNums_frame.Visible:= true;
  end else begin
   MsXml_RemoveChildNode(self.FXmlExistEZParcel,'Inner_CadastralNumbers');
   self.FInnerCadNums_frame.XmlCadastralNumbers:= nil;
   self.FInnerCadNums_frame.Visible:= false;
  end;
end;

procedure TFrameExistEZParcels.Action_SubParcels_IsFillExecute(Sender: TObject);
begin
 if self.SubParcels_IsFill_CheckBox.Checked then begin
  self.FSubParcels_frame.XmlSubParcels:= self.FXmlExistEZParcel.SubParcels;
  self.FSubParcels_frame.Visible:= true;
 end else begin
  MsXml_RemoveChildNode(self.FXmlExistEZParcel,'SubParcels');
  self.FSubParcels_frame.XmlSubParcels:= nil;
  self.FSubParcels_frame.Visible:= false;
 end;
end;

procedure TFrameExistEZParcels.ComboBox_Nav_GoToChange(Sender: TObject);
begin
 pcaSetActivTab(self.ChangeParcelsPageControl,self.ChangeParcelsPageControl.Pages[self.ComboBox_Nav_GoTo.ItemIndex]);
end;

constructor TFrameExistEZParcels.Create(AOwner: TComponent);
begin
  inherited;
  //NA
{  self.FProvPassCadlNums_frame:= TFrameExistInnerCadastralNumbers.Create(self.ProvPassCadNums_TabSheet);
  self.FProvPassCadlNums_frame.Parent:= self.ProvPassCadNums_TabSheet;
  self.FProvPassCadlNums_frame.Align:= alClient;
  self.FProvPassCadlNums_frame.Visible:= false;
 }
  //NA
  self.FInnerCadNums_frame:= TFrameCadastralNumbers.Create(self.InnerCadNums_TabSheet);
  self.FInnerCadNums_frame.Parent:= self.InnerCadNums_TabSheet;
  self.FInnerCadNums_frame.Align:= alClient;
  self.FInnerCadNums_frame.Visible:= false;


  self.FSubParcels_frame:= TFrameSubParcels.Create(self.SubParcels_TabSheet);
  self.FSubParcels_frame.Parent:= self.SubParcels_TabSheet;
  self.FSubParcels_frame.Align:= alClient;
  self.FSubParcels_frame.Visible:= False;

end;

procedure TFrameExistEZParcels.InitializationUI;
var
 _node: IXMLNode;

begin

 if self.IsAssignedChangeParcel then begin
  //self.ChangeParcelsPageControl.Visible:= TRUE;
  //self.Panel_Navigation.Visible:= TRUE;

  _node:= self.FXmlExistEZParcel.ChildNodes.FindNode('Providing_Pass_CadastralNumbers');
  if _node<>nil then begin
//   self.FProvPassCadlNums_frame.XmlCadastralNumbers:= self.FXmlChangeParcel.Providing_Pass_CadastralNumbers;
  { self.FProvPassCadlNums_frame.Visible:= true;
   self.ProvPassCadNums_IsFill_CheckBox.Checked:= True;}
  end else begin
  // self.FProvPassCadlNums_frame.XmlCadastralNumbers:= nil;
 //  self.FProvPassCadlNums_frame.Visible:= false;
   self.ProvPassCadNums_IsFill_CheckBox.Checked:= false;
  end;

  _node:= self.FXmlExistEZParcel.ChildNodes.FindNode('Inner_CadastralNumbers');
  if _node<>nil then begin
//   self.FInnerCadNums_frame.XmlCadastralNumbers:= self.FXmlChangeParcel.Inner_CadastralNumbers;
   self.FInnerCadNums_frame.Visible:= true;
   self.InnerCadNums_IsFill_CheckBox.Checked:= True;
  end else begin
   //self.FInnerCadNums_frame.XmlCadastralNumbers:= nil;
   self.FInnerCadNums_frame.Visible:= false;
   self.InnerCadNums_IsFill_CheckBox.Checked:= false;
  end;

  _node:= self.FXmlExistEZParcel.ChildNodes.FindNode('SubParcels');
  if _node<>nil then begin
//   self.FSubParcels_frame.XmlSubParcels:= self.FXmlChangeParcel.SubParcels;
   self.FSubParcels_frame.Visible:= true;
   self.SubParcels_IsFill_CheckBox.Checked:= True;
  end else begin
//TODO -   self.FSubParcels_frame.XmlSubParcels:= nil;
   self.FSubParcels_frame.Visible:= false;
   self.SubParcels_IsFill_CheckBox.Checked:= false;
  end;

 end else begin
  //self.ChangeParcelsPageControl.Visible:= FALSE;
  //self.Panel_Navigation.Visible:= FALSE;

//  self.FProvPassCadlNums_frame.XmlCadastralNumbers:= nil;
//  self.FInnerCadNums_frame.XmlCadastralNumbers:= nil;
//--  self.FSubParcels_frame.XmlSubParcels:= nil;
//  self.FEncumbramces_frame.XmlEncumbrances:= nil;
 end;

end;

function TFrameExistEZParcels.IsAssignedChangeParcel: Boolean;
begin
 Result:= self.FXmlExistEZParcel <> nil;
end;

procedure TFrameExistEZParcels.SetExistEZParcel( const aExistEZParcel: IXMLTExistEZParcel);
begin
 if self.FXmlExistEZParcel = aExistEZParcel then EXIT;
 self.ChangeParcelsPageControl.TabIndex:= 0;
 self.FXmlExistEZParcel:= aExistEZParcel;
 self.InitializationUI;
end;

procedure TFrameExistEZParcels.SpeedButton_Nav_BackClick(Sender: TObject);
begin
 pcaGoBackTab(self.ChangeParcelsPageControl);
 self.ComboBox_Nav_GoTo.ItemIndex:= self.ChangeParcelsPageControl.ActivePageIndex;
end;

procedure TFrameExistEZParcels.SpeedButton_Nav_NextClick(Sender: TObject);
begin
 pcaGoNextTab(self.ChangeParcelsPageControl);
 self.ComboBox_Nav_GoTo.ItemIndex:= self.ChangeParcelsPageControl.ActivePageIndex;
end;

end.
