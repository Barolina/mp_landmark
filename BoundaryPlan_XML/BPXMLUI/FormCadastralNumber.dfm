object FormsCadastralNumber: TFormsCadastralNumber
  AlignWithMargins = True
  Left = 227
  Top = 108
  AlphaBlend = True
  BorderStyle = bsToolWindow
  Caption = #1042#1074#1077#1076#1080#1090#1077' '#1082#1072#1076#1072#1089#1090#1088#1086#1074#1099#1081' '#1085#1086#1084#1077#1088
  ClientHeight = 49
  ClientWidth = 385
  Color = clBtnFace
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  ScreenSnap = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 27
    Width = 385
    Height = 22
    Align = alBottom
    BevelEdges = []
    BevelKind = bkFlat
    BevelOuter = bvNone
    Color = clWhite
    ParentBackground = False
    TabOrder = 0
    ExplicitTop = 19
    object CancelBtn: TButton
      Left = 310
      Top = 0
      Width = 75
      Height = 22
      Align = alRight
      Cancel = True
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 0
    end
    object OKBtn: TButton
      Left = 235
      Top = 0
      Width = 75
      Height = 22
      Align = alRight
      Caption = 'OK'
      Default = True
      ModalResult = 1
      TabOrder = 1
    end
  end
  object medtInput: TMaskEdit
    Left = 0
    Top = 0
    Width = 385
    Height = 25
    Align = alTop
    BevelEdges = [beLeft, beTop, beRight]
    BevelKind = bkFlat
    BorderStyle = bsNone
    Color = 15921123
    TabOrder = 1
    Text = ''
    StyleElements = []
    ExplicitTop = 14
  end
end
