object FrameOwnerNeighbours: TFrameOwnerNeighbours
  Left = 0
  Top = 0
  Width = 939
  Height = 447
  Color = clBtnFace
  ParentBackground = False
  ParentColor = False
  TabOrder = 0
  object grp: TGroupBox
    Left = 0
    Top = 0
    Width = 939
    Height = 447
    Align = alClient
    Color = clBtnFace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 0
    object pnl1: TPanel
      Left = 2
      Top = 15
      Width = 935
      Height = 34
      Align = alTop
      BevelEdges = [beLeft, beRight]
      BevelKind = bkFlat
      BevelOuter = bvNone
      ParentBackground = False
      TabOrder = 0
      object lblAttribute_NameRightr_Label: TLabel
        Left = 5
        Top = -2
        Width = 60
        Height = 13
        Caption = #1042#1080#1076' '#1087#1088#1072#1074#1072
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object edtAttribute_NameRight: TEdit
        Left = 1
        Top = 13
        Width = 927
        Height = 20
        BevelKind = bkFlat
        BorderStyle = bsNone
        Color = 15921123
        TabOrder = 0
        TextHint = #1042#1080#1076' '#1087#1088#1072#1074#1072
        OnChange = edtAttribute_NameRightChange
      end
    end
    object PanelContours_List: TPanel
      Left = 2
      Top = 49
      Width = 935
      Height = 51
      Align = alTop
      BevelEdges = [beLeft, beRight, beBottom]
      BevelOuter = bvNone
      ParentBackground = False
      TabOrder = 1
      object jvgrphdr1: TJvGroupHeader
        Left = 0
        Top = 0
        Width = 935
        Height = 17
        Align = alTop
        Caption = #1055#1088#1072#1074#1086#1086#1073#1083#1072#1076#1072#1090#1077#1083#1080' '#1089#1084#1077#1078#1085#1086#1075#1086' '#1091#1095#1072#1089#1090#1082#1072
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 12615680
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Pitch = fpFixed
        Font.Style = []
        Font.Quality = fqClearType
        ParentFont = False
        BevelOptions.Height = 1
        BevelOptions.Style = bsShape
        Transparent = True
        ExplicitLeft = -1
        ExplicitTop = -1
        ExplicitWidth = 931
      end
      object Contours_List_ButtonGroup: TButtonGroup
        Left = 360
        Top = 22
        Width = 61
        Height = 27
        BevelOuter = bvNone
        BorderStyle = bsNone
        ButtonHeight = 25
        ButtonWidth = 25
        Images = formContainer.ilCommands
        Items = <
          item
            Action = Action_Contrours_AddNew
            Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1087#1088#1072#1074#1086#1086#1073#1083#1072#1076#1072#1090#1077#1083#1103
            Hint = #13#10#1044#1086#1073#1072#1074#1080#1090#1100' '#1087#1088#1072#1074#1086#1086#1073#1083#1072#1076#1072#1090#1077#1083#1103#13#10
          end
          item
            Action = Action_Contours_DeleteCurrent
            Caption = #1059#1076#1072#1083#1080#1090#1100' '#1087#1088#1072#1074#1086#1086#1073#1083#1072#1076#1072#1090#1077#1083#1103
            Hint = #13#10#1059#1076#1072#1083#1080#1090#1100' '#1087#1088#1072#1074#1086#1086#1073#1083#1072#1076#1072#1090#1077#1083#1103#13#10
          end>
        ShowHint = True
        TabOrder = 0
      end
      object Contours_List_ListBox: TComboBox
        Left = 2
        Top = 23
        Width = 359
        Height = 21
        BevelInner = bvNone
        BevelKind = bkFlat
        BevelOuter = bvRaised
        Color = 15921123
        TabOrder = 1
        TextHint = #1057#1087#1080#1089#1086#1082' '#1087#1088#1072#1086#1086#1073#1083#1072#1076#1072#1090#1077#1083#1077#1081
        OnChange = Action_Contours_SetCurrentExecute
      end
    end
    object pnl2: TPanel
      Left = 2
      Top = 100
      Width = 935
      Height = 345
      Align = alClient
      BevelEdges = [beTop]
      BevelKind = bkFlat
      BevelOuter = bvNone
      ParentBackground = False
      TabOrder = 2
    end
  end
  object ActionList: TActionList
    Left = 600
    Top = 232
    object Action_Contrours_AddNew: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1082#1086#1085#1090#1091#1088
      Hint = #1044#1086#1073#1072#1074#1080#1090#1100' '#1082#1086#1085#1090#1091#1088
      ImageIndex = 0
      OnExecute = Action_Contrours_AddNewExecute
    end
    object Action_Contours_DeleteCurrent: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1074#1099#1073#1088#1072#1085#1085#1099#1081' '#1082#1086#1085#1090#1091#1088
      Hint = #1059#1076#1072#1083#1080#1090#1100' '#1074#1099#1073#1088#1072#1085#1085#1099#1081' '#1082#1086#1085#1090#1091#1088
      ImageIndex = 1
      OnExecute = Action_Contours_DeleteCurrentExecute
    end
    object Action_Contours_SetCurrent: TAction
      Caption = #1057#1076#1077#1083#1072#1090#1100' '#1090#1077#1082#1091#1097#1080#1084
      OnExecute = Action_Contours_SetCurrentExecute
    end
  end
end
