unit frUtilizationList;
{
   ��� NewParcels
}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, ButtonGroup, StdCtrls, ActnList,

   STD_MP, BpXML_different, xmlintf, ImgList, System.Actions, Vcl.Mask,
   unRussLang, Vcl.ExtCtrls
  ,frDocument;

type
  TFrameUtilizationList = class(TFrame)
    Kind_ComboBox: TComboBox;
    ByDoc_Edit: TEdit;
    ActionList: TActionList;
    Action_AddByKind: TAction;
    Action_AddByDoc: TAction;
    Action_DelSel: TAction;
    ImageList: TImageList;
    lblUtilisation: TLabel;
    lblByDoc: TLabel;
    lblAdditionalName: TLabel;
    medtAdditionalName: TMaskEdit;
    pnlDoc: TPanel;
    pnlAttrib: TPanel;
    lblDoc: TLabel;
    chkDoc: TCheckBox;

    procedure medtAdditionalNameExit(Sender: TObject);
    procedure ByDoc_EditExit(Sender: TObject);
    procedure Kind_ComboBoxChange(Sender: TObject);
    procedure chkDocClick(Sender: TObject);
  private
    { Private declarations }
    FXmlUtilizationList: IXMLTUtilization;
    FFrame_Documnet    : TFrameDocument;

    procedure UI_Write_Utilisation();
    procedure UI_Read_Utilisation();
    procedure UI_Write_ByDoc();
    procedure UI_Read_ByDoc();
    procedure UI_Read_AdditionalName();
    procedure UI_Write_AdditionalName();

  protected
    procedure InitializationUI;
    procedure SetUtilizationList(const UtilList: IXMLTUtilization);
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;

    function UI_ByKind_IsValid: boolean;
    function UI_ByDoc_IsEmpty: boolean;

    function IsAssignedUtilizationList: Boolean;
    property XmlUtilizationList: IXMLTUtilization read FXmlUtilizationList write SetUtilizationList;
  end;

implementation

uses MsXMLAPI;
{$R *.dfm}


procedure _HiddenChangeCheckBoxSate(ChBox: TCheckBox; NewSate: TCheckBoxState);
var
  _OnClick: TNotifyEvent;
begin
  if ChBox.State = NewSate then
    EXIT;
  _OnClick := ChBox.OnClick;
  ChBox.OnClick := nil;
  ChBox.State := NewSate;
  ChBox.OnClick := _OnClick;
end;
{ TFrameUtilizationList }

procedure TFrameUtilizationList.ByDoc_EditExit(Sender: TObject);
begin
  if  self.IsAssignedUtilizationList  then
    self.UI_Write_ByDoc;
end;

procedure TFrameUtilizationList.chkDocClick(Sender: TObject);
begin
  if not chkDoc.Checked then
  begin
     if FFrame_Documnet.XMlDOcument <> nil then
        if MessageDlg('������� ������: '+lblByDoc.Caption+'?',mtInformation,mbYesNo,-1) = mrYes then
        begin
           MsXml_RemoveChildNode(self.FXmlUtilizationList, 'DocUtilization');
           self.FFrame_Documnet.XMlDOcument := nil;
           self.FFrame_Documnet.Visible := false;
        end else chkDoc.Checked := True;
  end
  else
  begin
    self.FFrame_Documnet.XMlDOcument := self.FXmlUtilizationList.DocUtilization;
    self.FFrame_Documnet.Visible := true;
  end
end;

constructor TFrameUtilizationList.Create(AOwner: TComponent);
begin
 inherited;
 TSTD_MP_ListofIndexGenerate.BpXml_LoadDictionary(STD_MP_WorkDir+cnstdUtilizations,self.Kind_ComboBox.Items);
 self.FFrame_Documnet := TFrameDocument.Create(self.pnlDoc);
 Self.FFrame_Documnet.Parent := self.pnlDoc;
 self.FFrame_Documnet.Align := alClient;
 self.FFrame_Documnet.Visible := false;
end;

procedure TFrameUtilizationList.InitializationUI;
var _node : IXMLNode;
begin
 self.ByDoc_Edit.Text:= '';
 self.Kind_ComboBox.ItemIndex:= -1;
 if self.IsAssignedUtilizationList then
 begin
    Self.UI_Read_ByDoc;
    Self.UI_Read_Utilisation;
    UI_Read_AdditionalName;
    _node := self.FXmlUtilizationList.ChildNodes.FindNode('DocUtilization');
    if _node <> nil then
    begin
      self.FFrame_Documnet.XMlDOcument := self.FXmlUtilizationList.DocUtilization;
      self.FFrame_Documnet.Visible := true;
      _HiddenChangeCheckBoxSate(self.chkDoc, cbChecked);
    end
    else
    begin
      self.FFrame_Documnet.XMlDOcument := nil;
      self.FFrame_Documnet.Visible := false;
      _HiddenChangeCheckBoxSate(self.chkDoc,cbUnchecked);
    end;
 end
 else
 begin
   self.FFrame_Documnet.XMlDOcument := nil;
 end;
end;

function TFrameUtilizationList.IsAssignedUtilizationList: Boolean;
begin
 Result:= self.FXmlUtilizationList<>nil;
end;

procedure TFrameUtilizationList.Kind_ComboBoxChange(Sender: TObject);
begin
  if not  Self.IsAssignedUtilizationList  then
    self.UI_Write_Utilisation;
end;

procedure TFrameUtilizationList.medtAdditionalNameExit(Sender: TObject);
begin
  if self.IsAssignedUtilizationList then
    self.UI_Write_AdditionalName;
end;

procedure TFrameUtilizationList.SetUtilizationList( const UtilList: IXMLTUtilization);
begin
 if self.FXmlUtilizationList=UtilList then EXIT;
 self.FXmlUtilizationList:= UtilList;
 self.InitializationUI;
end;

function TFrameUtilizationList.UI_ByDoc_IsEmpty: boolean;
begin
 Result:= self.ByDoc_Edit.Text='';
end;

function TFrameUtilizationList.UI_ByKind_IsValid: boolean;
begin
 Result:=  self.Kind_ComboBox.ItemIndex <>-1;
end;

procedure TFrameUtilizationList.UI_Read_AdditionalName;
begin
  self.medtAdditionalName.Text := FXmlUtilizationList.AdditionalName;
end;

procedure TFrameUtilizationList.UI_Read_ByDoc;
begin
 self.ByDoc_Edit.Text := MsXml_ReadAttribute(FXmlUtilizationList,cnstXmlByDoc);
end;

procedure TFrameUtilizationList.UI_Read_Utilisation;
var
  _code: string;
begin
  _code := MsXml_ReadAttribute(self.FXmlUtilizationList, cnstXmlUtilization);
  self.Kind_ComboBox.ItemIndex :=  TSTD_MP_ListofIndexGenerate.BpXml_FindItemByCode(self.Kind_ComboBox.Items, _code);
end;

procedure TFrameUtilizationList.UI_Write_AdditionalName;
begin
  if medtAdditionalName.Text <> ''  then
    FXmlUtilizationList.AdditionalName := medtAdditionalName.Text;
end;

procedure TFrameUtilizationList.UI_Write_ByDoc;
begin
 if ByDoc_Edit.Text  <> '' then
   FXmlUtilizationList.ByDoc := ByDoc_Edit.Text;
end;

procedure TFrameUtilizationList.UI_Write_Utilisation;
var
  _item: TBpXml_DictionaryItem;
  _i: integer;
begin
  _i := self.Kind_ComboBox.ItemIndex;
  if _i = -1 then EXIT;
  _item := TBpXml_DictionaryItem(self.Kind_ComboBox.Items.Objects[_i]);
  self.FXmlUtilizationList.Utilization := _item.Code;
end;

end.
