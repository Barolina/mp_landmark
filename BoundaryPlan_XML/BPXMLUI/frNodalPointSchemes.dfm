object FrameNodalPointSchemes: TFrameNodalPointSchemes
  Left = 0
  Top = 0
  Width = 642
  Height = 287
  TabOrder = 0
  object PanelNodalPointSchemes_List: TPanel
    Left = 0
    Top = 0
    Width = 53
    Height = 287
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 0
    object NodalPointSchemes_List_ButtonGroup: TButtonGroup
      Left = 0
      Top = 0
      Width = 53
      Height = 25
      Align = alTop
      BevelOuter = bvNone
      BorderStyle = bsNone
      ButtonHeight = 25
      ButtonWidth = 25
      Images = formContainer.ilCommands
      Items = <
        item
          Action = Action_Contrours_AddNew
          Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1072#1073#1088#1080#1089' '#1091#1079#1083#1086#1074#1099#1093' '#1090#1086#1095#1077#1082' '#1075#1088#1072#1085#1080#1094' '#1079#1077#1084#1077#1083#1100#1085#1099#1093' '#1091#1095#1072#1089#1090#1082#1086#1074
          Hint = #13#10#1044#1086#1073#1072#1074#1080#1090#1100' '#1072#1073#1088#1080#1089' '#1091#1079#1083#1086#1074#1099#1093' '#1090#1086#1095#1077#1082' '#1075#1088#1072#1085#1080#1094' '#1047#1059#13#10
        end
        item
          Action = Action_NodalPointSchemes_DeleteCurrent
          Caption = #1059#1076#1072#1083#1080#1090#1100' '#1072#1073#1088#1080#1089' '#1091#1079#1083#1086#1074#1099#1093' '#1090#1086#1095#1077#1082' '#1075#1088#1072#1085#1080#1094' '#1079#1077#1084#1077#1083#1100#1085#1099#1093' '#1091#1095#1072#1089#1090#1082#1086#1074
          Hint = #13#10#1059#1076#1072#1083#1080#1090#1100' '#1072#1073#1088#1080#1089' '#1091#1079#1083#1086#1074#1099#1093' '#1090#1086#1095#1077#1082' '#1075#1088#1072#1085#1080#1094' '#1047#1059#13#10
        end>
      ShowHint = True
      TabOrder = 0
    end
    object NodalPointSchemes_List_ListBox: TListBox
      Left = 0
      Top = 25
      Width = 53
      Height = 262
      Style = lbOwnerDrawVariable
      Align = alClient
      BevelEdges = [beLeft, beRight, beBottom]
      BevelKind = bkFlat
      BorderStyle = bsNone
      ItemHeight = 13
      TabOrder = 1
      OnClick = Action_NodalPointSchemes_SetCurrentExecute
    end
  end
  object ActionList: TActionList
    Left = 600
    Top = 232
    object Action_Contrours_AddNew: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1082#1086#1085#1090#1091#1088
      Hint = #1044#1086#1073#1072#1074#1080#1090#1100' '#1082#1086#1085#1090#1091#1088
      ImageIndex = 0
      OnExecute = Action_Contrours_AddNewExecute
    end
    object Action_NodalPointSchemes_DeleteCurrent: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1074#1099#1073#1088#1072#1085#1085#1099#1081' '#1082#1086#1085#1090#1091#1088
      Hint = #1059#1076#1072#1083#1080#1090#1100' '#1074#1099#1073#1088#1072#1085#1085#1099#1081' '#1082#1086#1085#1090#1091#1088
      ImageIndex = 1
      OnExecute = Action_NodalPointSchemes_DeleteCurrentExecute
    end
    object Action_NodalPointSchemes_SetCurrent: TAction
      Caption = #1057#1076#1077#1083#1072#1090#1100' '#1090#1077#1082#1091#1097#1080#1084
      OnExecute = Action_NodalPointSchemes_SetCurrentExecute
    end
  end
end
