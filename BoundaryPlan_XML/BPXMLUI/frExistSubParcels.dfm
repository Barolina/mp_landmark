object FrameExistSubParcels: TFrameExistSubParcels
  Left = 0
  Top = 0
  Width = 700
  Height = 301
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  ParentFont = False
  TabOrder = 0
  object Splitter1: TSplitter
    Left = 81
    Top = 0
    Height = 301
    Beveled = True
    ResizeStyle = rsUpdate
    ExplicitLeft = 71
    ExplicitTop = 36
    ExplicitHeight = 271
  end
  object PanelSubParcelFrame: TPanel
    Left = 84
    Top = 0
    Width = 616
    Height = 301
    Align = alClient
    BevelOuter = bvNone
    Color = clWhite
    ParentBackground = False
    TabOrder = 0
  end
  object pblListElement: TPanel
    Left = 0
    Top = 0
    Width = 81
    Height = 301
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 1
    object SubParcels_Actions_ButtonGroup: TButtonGroup
      Left = 0
      Top = 0
      Width = 81
      Height = 26
      Align = alTop
      BevelInner = bvNone
      BevelOuter = bvNone
      BorderStyle = bsNone
      ButtonHeight = 25
      ButtonWidth = 25
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clBtnFace
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Images = formContainer.ilCommands
      Items = <
        item
          Action = Action_AddNewSubParcel
        end
        item
          Action = Action_DeleteSubParcel
        end>
      TabOrder = 0
      ExplicitLeft = -3
    end
    object ExistSubParcels_Count: TListBox
      Left = 0
      Top = 26
      Width = 81
      Height = 275
      Hint = #1059#1095#1077#1090#1085#1099#1077' '#1085#1086#1084#1077#1088#1072' '#1095#1077#1089#1090#1077#1081' '#1047#1059' ('#1044#1083#1103' '#1080#1079#1084#1077#1085#1077#1085#1080#1103'  '#1076#1074#1072#1078#1076#1099' '#1082#1083#1080#1082#1085#1077#1090#1077')'
      Style = lbOwnerDrawVariable
      Align = alClient
      BevelEdges = [beLeft, beBottom]
      BevelKind = bkFlat
      BorderStyle = bsNone
      Color = clWhite
      ItemHeight = 13
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      StyleElements = []
      OnClick = Action_SelectSubParcelExecute
      OnDblClick = ExistSubParcels_CountDblClick
    end
  end
  object ActionList: TActionList
    Left = 659
    Top = 252
    object Action_AddNewSubParcel: TAction
      Caption = 'Action_AddNewSubParcel'
      ImageIndex = 0
      OnExecute = Action_AddNewSubParcelExecute
    end
    object Action_DeleteSubParcel: TAction
      Caption = 'Action_DeleteSubParcel'
      ImageIndex = 1
      OnExecute = Action_DeleteSubParcelExecute
    end
    object Action_SelectSubParcel: TAction
      Caption = 'Action_SelectSubParcel'
      OnExecute = Action_SelectSubParcelExecute
    end
  end
end
