object FrameBorder: TFrameBorder
  Left = 0
  Top = 0
  Width = 852
  Height = 541
  TabOrder = 0
  OnResize = FrameResize
  object pnlChangeBorder: TPanel
    Left = 0
    Top = 0
    Width = 852
    Height = 541
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object pnlOrdinate: TPanel
      Left = 0
      Top = 0
      Width = 852
      Height = 501
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object pnlNewOrdinate: TPanel
        Left = 0
        Top = 250
        Width = 852
        Height = 304
        Align = alTop
        TabOrder = 0
        object lblNewOrdinate: TLabel
          AlignWithMargins = True
          Left = 4
          Top = 4
          Width = 844
          Height = 13
          Align = alTop
          Alignment = taCenter
          AutoSize = False
          Caption = #1059#1090#1086#1095#1085#1103#1077#1084#1099#1077' '#1082#1086#1086#1088#1076#1080#1085#1072#1090#1099
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          Transparent = False
          ExplicitTop = 1
          ExplicitWidth = 767
        end
        object sgNewOrdinate: TStringGrid
          Left = 1
          Top = 43
          Width = 850
          Height = 260
          Align = alClient
          BevelKind = bkFlat
          BorderStyle = bsNone
          Ctl3D = False
          DefaultColWidth = 70
          Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing]
          ParentCtl3D = False
          PopupMenu = pmOridinate
          TabOrder = 0
          OnDrawCell = sgOldOrdinateDrawCell
          OnKeyPress = sgOldOrdinateKeyPress
          OnKeyUp = sgOldOrdinateKeyUp
          OnMouseActivate = sgNewOrdinateMouseActivate
          OnSelectCell = sgOldOrdinateSelectCell
          OnSetEditText = sgOldOrdinateSetEditText
          RowHeights = (
            24
            24
            24
            24
            24)
        end
        object pnlImport_NewOrdinate: TPanel
          Left = 1
          Top = 20
          Width = 850
          Height = 23
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          object btnNew_Ordinate: TButton
            Left = 784
            Top = 0
            Width = 66
            Height = 23
            Align = alRight
            Caption = '...'
            ImageAlignment = iaCenter
            TabOrder = 0
            OnClick = btnOldOrdinateClick
          end
          object edtNew_Ordinate: TEdit
            Left = 0
            Top = 0
            Width = 784
            Height = 23
            Align = alClient
            BevelInner = bvNone
            BevelOuter = bvNone
            BevelWidth = 2
            TabOrder = 1
            ExplicitHeight = 21
          end
        end
      end
      object pnlOldOrdinate: TPanel
        Left = 0
        Top = 0
        Width = 852
        Height = 250
        Align = alTop
        Caption = 'pnlOldOrdinate'
        TabOrder = 1
        object lblOldOrdinate: TLabel
          AlignWithMargins = True
          Left = 4
          Top = 4
          Width = 844
          Height = 13
          Align = alTop
          Alignment = taCenter
          AutoSize = False
          Caption = #1057#1091#1097#1077#1089#1090#1074#1091#1102#1097#1080#1077' '#1082#1086#1086#1088#1076#1080#1085#1072#1090#1099
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          Transparent = False
          ExplicitLeft = 2
          ExplicitTop = 1
          ExplicitWidth = 312
        end
        object sgOldOrdinate: TStringGrid
          Left = 1
          Top = 43
          Width = 850
          Height = 206
          Align = alClient
          BevelKind = bkFlat
          BorderStyle = bsNone
          Ctl3D = False
          DefaultColWidth = 70
          Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing]
          ParentCtl3D = False
          PopupMenu = pmOridinate
          TabOrder = 0
          OnDrawCell = sgOldOrdinateDrawCell
          OnGetEditText = sgOldOrdinateGetEditText
          OnKeyPress = sgOldOrdinateKeyPress
          OnKeyUp = sgOldOrdinateKeyUp
          OnMouseActivate = sgOldOrdinateMouseActivate
          OnSelectCell = sgOldOrdinateSelectCell
          OnSetEditText = sgOldOrdinateSetEditText
          RowHeights = (
            24
            24
            24
            24
            24)
        end
        object pnlImportOldOrdinate: TPanel
          Left = 1
          Top = 20
          Width = 850
          Height = 23
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          object btnOldOrdinate: TButton
            Left = 784
            Top = 0
            Width = 66
            Height = 23
            Align = alRight
            Caption = '...'
            ImageAlignment = iaCenter
            TabOrder = 0
            OnClick = btnOldOrdinateClick
          end
          object edtOldOrdinate: TEdit
            Left = 0
            Top = 0
            Width = 784
            Height = 23
            Align = alClient
            BevelInner = bvNone
            BevelOuter = bvNone
            TabOrder = 1
            ExplicitHeight = 21
          end
        end
      end
    end
    object pnlBorders: TPanel
      Left = 0
      Top = 501
      Width = 852
      Height = 40
      Align = alBottom
      BevelOuter = bvNone
      Caption = 'pnlBorders'
      TabOrder = 1
      Visible = False
      object lbl_old_ordinate: TLabel
        Left = 24
        Top = 6
        Width = 79
        Height = 13
        Caption = #1050#1086#1083'. '#1101#1083#1077#1084#1077#1085#1090#1086#1074
      end
      object lblnew_ordinate: TLabel
        Left = 336
        Top = 6
        Width = 79
        Height = 13
        Caption = #1050#1086#1083'. '#1101#1083#1077#1084#1077#1085#1090#1086#1074
      end
      object MaskEdit1: TMaskEdit
        Left = 176
        Top = 5
        Width = 121
        Height = 21
        TabOrder = 0
        Text = 'MaskEdit1'
      end
    end
  end
  object pmOridinate: TPopupMenu
    Left = 184
    Top = 144
    object miAdd_is_Buffer: TMenuItem
      Caption = #1042#1089#1090#1072#1074#1080#1090#1100' '#1080#1079' '#1073#1091#1092#1077#1088#1072
      OnClick = miAdd_is_BufferClick
    end
    object miImportLayers: TMenuItem
      Caption = #1048#1084#1087#1086#1088#1090' '#1080#1079' '#1089#1083#1086#1103' '
      Enabled = False
      OnClick = miImportLayersClick
    end
    object miInsertOrdinateNext: TMenuItem
      Break = mbBarBreak
      Caption = #1044#1086#1073#1072#1074#1080#1090' '#1087#1086#1089#1083#1077' '#1090#1077#1082' '#1089#1090#1088#1086#1082#1080' '
      Visible = False
      OnClick = miInsertOrdinateNextClick
    end
    object miAdd_Row_ChangeBorder: TMenuItem
      Break = mbBarBreak
      Caption = #1042#1089#1090#1072#1074#1080#1090#1100' '#1089#1090#1088#1086#1082#1091
      OnClick = miAdd_Row_ChangeBorderClick
    end
    object DeleteRow: TMenuItem
      Caption = #1059#1076#1072#1083#1080#1090#1100'  '#1089#1090#1088#1086#1082#1091
      Hint = #1059#1076#1072#1083#1080#1090#1100' '#1090#1077#1082'. '#1089#1090#1088#1086#1082#1091' ('#1074' '#1086#1073#1086#1080#1093' '#1090#1072#1073#1083#1080#1094#1072#1093')'
      OnClick = DeleteRowClick
    end
    object miSetting: TMenuItem
      Break = mbBarBreak
      Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1072
      OnClick = miSettingClick
    end
  end
  object opdgDatafile: TOpenDialog
    Filter = #1060#1072#1081#1083' '#1075#1088#1072#1092#1080#1082#1080' MapInfo (*.mif)|*.mif|'#1058#1077#1082#1089#1090#1086#1074#1099#1081' '#1076#1086#1082#1091#1084#1077#1085#1090'|*.txt'
    Options = [ofHideReadOnly, ofAllowMultiSelect, ofEnableSizing]
    Left = 571
    Top = 273
  end
end
