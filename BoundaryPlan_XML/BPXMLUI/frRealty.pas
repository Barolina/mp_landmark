unit frRealty;
    {
    �������� � ������� ������, ����������, �������� �������������� �������������
     �� �������� ��� ���������� ��������� ��������
    IXMLSTD_MP_Input_Data_Realty
    }
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ActnList, ButtonGroup, ExtCtrls, Vcl.Menus, Vcl.ImgList, Vcl.Grids,

  STD_MP;

const cnstWCadNum =165;
      cnstWCadNumOther = 371;
      cnstWNameOKS = 165;  
      cnstWN = 35;
      cnstColCount = 4;
      cnstNN = 0;
      cnstNCadNum= 1;
      cnstNCadNumOther = 2;
      cnstNNameOKS =3;
  
type
  THackGrid = class(TStringGrid);

type
  TFrameRealty = class(TFrame)
    ImageList: TImageList;
    Panel: TPanel;
    stringGridDate: TStringGrid;
    poMenuTable: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    pnlLabelTabl: TPanel;
    lblCadNum: TLabel;
    lblNameOKS: TLabel;
    lblCadNumOther: TLabel;
    splCadNum: TSplitter;
    splCadNumOther: TSplitter;
    splNameOks: TSplitter;
    lblN: TLabel;
    spl4: TSplitter;
    procedure SubParcels_Actions_ButtonGroupItems0Click(Sender: TObject);
    procedure stringGridDateSetEditText(Sender: TObject; ACol, ARow: Integer;
      const Value: string);
    procedure stringGridDateKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SubParcels_Actions_ButtonGroupItems1Click(Sender: TObject);
    procedure splCadNumMoved(Sender: TObject);
    procedure splCadNumOtherMoved(Sender: TObject);
    procedure splNameOksMoved(Sender: TObject);
  private
    { Private declarations }

     FXmlRealtys: IXMLSTD_MP_Input_Data_Realty;
     FXMLRealty : IXMLSTD_MP_Input_Data_Realty_OKS;

     procedure UI_ReadOf(const aRealty: IXMLSTD_MP_Input_Data_Realty);

     procedure Data_Realty_DoAddNew;
     procedure Data_Realty_DoDelete;

  protected
     procedure SetRealtys(const aRealtys: IXMLSTD_MP_Input_Data_Realty);
     procedure InitializationUI;

  public
    { Public declarations }
     procedure AddRowStrGrid();
     procedure DelRowStrGrid(aRow : integer);
    
     constructor Create(Owner: TComponent); override;
     function   IsAssigned_Realtys: Boolean;
     property XmlRealtys: IXMLSTD_MP_Input_Data_Realty read FXmlRealtys write SetRealtys;
  end;

implementation


{$R *.dfm}

{ TFrameRealty }

procedure TFrameRealty.AddRowStrGrid;
var rowCount  : integer;
begin
  rowCount := stringGridDate.RowCount;
  StringGridDate.RowCount := rowCount +1;
  stringGridDate.Cells[0,rowCount] := IntToStr(rowCount+1);
end;

constructor TFrameRealty.Create(Owner: TComponent);
begin
  inherited;
  FXmlRealtys := nil;
  FXMLRealty  := nil;
  {setting ^)}
  stringGridDate.ColCount := cnstColCount;
  stringGridDate.ColWidths[cnstNN] := cnstWN; 
  stringGridDate.ColWidths[cnstNCadNum] :=cnstWCadNum; 
  stringGridDate.ColWidths[cnstNCadNumOther] :=cnstWCadNumOther; 
  stringGridDate.ColWidths[cnstNNameOKS] :=cnstWNameOKS; 
  lblCadNum.Width := cnstWCadNum; 
  lblCadNumOther.Width := cnstWCadNumOther;
  lblNameOKS.Width := cnstWNameOKS;

  self.InitializationUI;
end;


procedure TFrameRealty.Data_Realty_DoAddNew;
begin
  if not self.IsAssigned_Realtys then EXIT;
  AddRowStrGrid;
  FXMLRealty := FXmlRealtys.Add;
end;

procedure TFrameRealty.Data_Realty_DoDelete;
var FArow : integer;
begin
   FArow := StringGridDate.Row;
   if (MessageDlg('������� ������ � '+IntToStr(FArow+1)+'?', mtInformation, mbYesNo,-1) = mrYes )
      and (FArow >=0) and (FXmlRealtys.Count > FArow) then
   begin
     FXMLRealty := FXmlRealtys.OKS[FArow];
     FXmlRealtys.Remove(FXMLRealty);
     FXMLRealty := nil;
     DelRowStrGrid(FArow);
   end;
end;

procedure TFrameRealty.DelRowStrGrid(aRow: integer);
begin
  if aRow < 0  then Exit;
  stringGridDate.Rows[aRow].Clear;
  if aRow > 0 then THackGrid(stringGridDate).DeleteRow(aRow);
end;

procedure TFrameRealty.InitializationUI;
begin
  stringGridDate.RowCount := 1;
  THackGrid(stringGridDate).Rows[0].Clear;
  stringGridDate.Cells[cnstNN,0] := '1';
  if  IsAssigned_Realtys then   UI_ReadOf(FXmlRealtys);
end;

function TFrameRealty.IsAssigned_Realtys: Boolean;
begin
  Result := FXmlRealtys <> nil;
end;

procedure TFrameRealty.SetRealtys(const aRealtys: IXMLSTD_MP_Input_Data_Realty);
begin
 if FXmlRealtys = aRealtys then Exit;
 FXmlRealtys := aRealtys;
 InitializationUI;
end;

procedure TFrameRealty.splCadNumMoved(Sender: TObject);
begin
  self.stringGridDate.ColWidths[cnstNCadNum] := lblCadNum.Width;
end;

procedure TFrameRealty.splCadNumOtherMoved(Sender: TObject);
begin
  self.stringGridDate.ColWidths[cnstNCadNumOther] := lblCadNumOther.Width;
end;

procedure TFrameRealty.splNameOksMoved(Sender: TObject);
begin
 self.stringGridDate.ColWidths[cnstNNameOKS] := lblNameOKS.Width;
end;

procedure TFrameRealty.stringGridDateKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var i : integer;
begin
  if (Key = VK_DELETE) and (ssCtrl in Shift) then Data_Realty_DoDelete;
  if (Key = VK_INSERT) and (ssCtrl in Shift )then Data_Realty_DoAddNew;
end;

procedure  TFrameRealty.stringGridDateSetEditText(Sender: TObject; ACol,
  ARow: Integer; const Value: string);
begin
  if not IsAssigned_Realtys then Exit;
  if Value = ''  then Exit;  

  if ARow  >= 0 then
  begin
    if FXmlRealtys.Count > ARow then FXMLRealty := FXmlRealtys.OKS[ARow]
    else FXMLRealty := FXmlRealtys.Add;
    case ACol of
     cnstNCadNum  :   FXMLRealty.CadastralNumber := Value;
     cnstNCadNumOther :   FXMLRealty.CadastralNumber_OtherNumber := Value;
     cnstNNameOKS  :   FXMLRealty.Name_OKS := Value;
    end;
  end;
end;

procedure TFrameRealty.SubParcels_Actions_ButtonGroupItems0Click(
  Sender: TObject);
begin
 Data_Realty_DoAddNew;
end;

procedure TFrameRealty.SubParcels_Actions_ButtonGroupItems1Click(
  Sender: TObject);
begin
 Data_Realty_DoDelete
end;

procedure TFrameRealty.UI_ReadOf(const aRealty: IXMLSTD_MP_Input_Data_Realty);
var  i  : integer;
begin
  StringGridDate.RowCount := aRealty.Count;
  for I := 0 to aRealty.Count -1 do
  begin
      FXMLRealty := aRealty.OKS[i];
      StringGridDate.Cells[cnstNCadNum,i] := FXMLRealty.CadastralNumber;
      StringGridDate.Cells[cnstNCadNumOther,i] := FXMLRealty.CadastralNumber_OtherNumber;
      StringGridDate.Cells[cnstNNameOKS,i] := FXMLRealty.Name_OKS;
      StringGridDate.Cells[cnstNN,i] := IntToStr(i+1);
  end;
  FXMLRealty := nil;
end;


end.
