unit fmDeleteEntryParcels;

//-- ����������� �� �� �������� �������

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, STD_MP, ComCtrls, Grids, EdGrid, DBGrids,

  MSStringParser, fmEntitySpatial_Attributs,
  MSMCommon_, fmEntitySpatial_EditorNeighbours,
  frArea, frEntitySpatial, Buttons, Menus,Clipbrd, ButtonGroup, ActnList,BpXML_different,
  Mask, ImgList, XMLIntf, System.Actions,
  RegularExpressions,unRussLang, fmContainer;


type

  TFrameDeleteEntryParcels = class(TFrame)
    ActionList: TActionList;
    Action_DoAdd: TAction;
    Action_DelSel: TAction;
    chkIsFill_DeleteEntryParcels_CheckBox: TCheckBox;
    medtDeleteparcel: TMaskEdit;
    lstDeleteEntryParcelsList_ListBox: TListBox;
    btngpActionAddDel: TButtonGroup;
    pnlDeleteEntryParcels: TPanel;

    // -- �������� ������� � ��������
    procedure BitBtn_AddDefinitionClick(Sender: TObject);
    procedure BitBtn_DelDefinitionClick(Sender: TObject);
     procedure Action_DoAddExecute(Sender: TObject);
    procedure Action_DelSelExecute(Sender: TObject);
    procedure cbbNameChange(Sender: TObject);


    procedure IsFill_ExistEntryParcel_CheckBoxClick(Sender: TObject);
    procedure chkIsFill_DeleteEntryParcels_CheckBoxClick(Sender: TObject);
    procedure Action_DeleteEntryParcel_DoAddExecute(Sender: TObject);
    procedure Action_DeleteEntryParcel_DoDelSelExecute(Sender: TObject);
    procedure NewEntryParcelList_ComboBoxChange(Sender: TObject);
    procedure actNewEntryparcel_PasteExecute(Sender: TObject);
    procedure actNewEntryparcel_ValidExecute(Sender: TObject);
    procedure Action1Execute(Sender: TObject);
    procedure btngp(Sender: TObject);

  private
    FDeleteEntryParcels : IXMLTChangeParcel_DeleteEntryParcels;
    
   //������ �����������
    function UpDateList_DeleteEntryParcel: integer;

    procedure SetDeleteEntryparcels(const DeleteEntryParcels : IXMLTChangeParcel_DeleteEntryParcels);
    procedure InitializationUI;

    { Private declarations }
  public

    constructor Create(Owner: TComponent); override;
    property XMlDeleteEntryParcels : IXMLTChangeParcel_DeleteEntryParcels 
                                     read FDeleteEntryParcels
                                     write SetDeleteEntryparcels; 

    { Public declarations }
  end;

implementation

uses BPCommon, MsXMLAPI, SurveyingPlan.Xml.Utils, SurveyingPlan.Xml.Types,
     SurveyingPlan.DataFormat, fmLog;

 procedure _HiddenChangeCheckBoxSate(ChBox: TCheckBox; NewSate: TCheckBoxState);
 var
  _OnClick: TNotifyEvent;
 begin
  if ChBox.State=NewSate then EXIT;
  _OnClick:= ChBox.OnClick;
  ChBox.OnClick:= nil;
  ChBox.State:= NewSate;
  ChBox.OnClick:= _OnClick;
 end;


procedure TFrameDeleteEntryParcels.SetDeleteEntryparcels(
  const DeleteEntryParcels: IXMLTChangeParcel_DeleteEntryParcels);
begin
  if self.FDeleteEntryParcels = DeleteEntryParcels then Exit;
  self.FDeleteEntryParcels := DeleteEntryParcels;
  self.InitializationUI;
end;


constructor TFrameDeleteEntryParcels.Create(Owner: TComponent);
begin
 inherited;
 self.FDeleteEntryParcels := nil;
end;

function TFrameDeleteEntryParcels.UpDateList_DeleteEntryParcel: integer;
var
 _i: integer;
 _node: IXMLNode;
 _XMLDeleteEntryParcel: IXMLTChangeParcel_DeleteEntryParcels_DeleteEntryParcel;
begin
 self.lstDeleteEntryParcelsList_ListBox.Items.Clear;
 _node:= self.FDeleteEntryParcels.ChildNodes.FindNode('DeleteEntryParcel');
 if Assigned(_node) then
  for _I := 0 to self.FDeleteEntryParcels.Count -1 do 
  begin
   _XMLDeleteEntryParcel:= self.FDeleteEntryParcels.DeleteEntryParcel[_i];
    self.lstDeleteEntryParcelsList_ListBox.Items.AddObject(_XMLDeleteEntryParcel.CadastralNumber,pointer(_XMLDeleteEntryParcel));
  end;
 Result:= self.lstDeleteEntryParcelsList_ListBox.Items.Count;
end;


// -->>-- ���������� ��������� �������� ���������� �� �� �������� �������-------
procedure TFrameDeleteEntryParcels.InitializationUI;
var
 i : integer;
 _node: ixmlnode;
 _tempBool: Boolean;
begin
 //������� ������� ������
 self.lstDeleteEntryParcelsList_ListBox.Clear;
 if self.FDeleteEntryParcels <> nil then
 begin
    if self.UpDateList_DeleteEntryParcel > 0  then
      _HiddenChangeCheckBoxSate(self.chkIsFill_DeleteEntryParcels_CheckBox,cbChecked)
   else 
      _HiddenChangeCheckBoxSate(self.chkIsFill_DeleteEntryParcels_CheckBox,cbUnchecked); 
 end
 else   self.pnlDeleteEntryParcels.Visible := self.chkIsFill_DeleteEntryParcels_CheckBox.Checked;
end;

procedure TFrameDeleteEntryParcels.Action_DelSelExecute(Sender: TObject);
var
 _i,_j: integer;
 _XMLDeleteParcel: IXMLTChangeParcel_DeleteEntryParcels_DeleteEntryParcel;
begin
 if MessageDlg(rsDeleteCurrentCadNum, mtConfirmation, [mbYes, mbNo], -1) = mrNo then Exit;
 
 for _i := self.lstDeleteEntryParcelsList_ListBox.Items.Count-1 downto 0 do
  if Self.lstDeleteEntryParcelsList_ListBox.Selected[_i] then 
  begin
   _XMLDeleteParcel:= IXMLTChangeParcel_DeleteEntryParcels_DeleteEntryParcel(pointer(Self.lstDeleteEntryParcelsList_ListBox.Items.Objects[_i]));
   for _j := 0 to self.FDeleteEntryParcels.Count-1 do
    if self.FDeleteEntryParcels.DeleteEntryParcel[_j].ChildNodes.IndexOf(_XMLDeleteParcel) >-1 then 
    begin
       self.FDeleteEntryParcels.Delete(_j);
       self.lstDeleteEntryParcelsList_ListBox.Items.Delete(_i);
       BREAK;
    end;
  end;
end;

procedure TFrameDeleteEntryParcels.Action_DoAddExecute(Sender: TObject);
var
 _CadNum: string;
 _XMLDeleteParcel: IXMLTChangeParcel_DeleteEntryParcels_DeleteEntryParcel;
 listCadNum : TStringList;
 i : Integer;
begin
 listCadNum := TStringList.Create;
 listCadNum  :=  GetClipBoardList(regCadastralNumber);
 for i := 0 to listCadNum.Count-1 do
 begin
     _CadNum := listCadNum.Strings[i];
     _XMLDeleteParcel:= self.FDeleteEntryParcels.Add;
     _XMLDeleteParcel.CadastralNumber := _CadNum;
     self.lstDeleteEntryParcelsList_ListBox.AddItem(_CadNum,Pointer(_XMLDeleteParcel));
 end;
 listCadNum.Destroy;
end;


// -->> ------------------------------------------------------------------------
procedure TFrameDeleteEntryParcels.chkIsFill_DeleteEntryParcels_CheckBoxClick(
  Sender: TObject);
begin
 if self.chkIsFill_DeleteEntryParcels_CheckBox.Checked then 
 begin
    self.pnlDeleteEntryParcels.Visible:= TRUE;
    self.UpDateList_DeleteEntryParcel;
 end else 
 begin
    self.pnlDeleteEntryParcels.Visible:= FALSE;
    self.lstDeleteEntryParcelsList_ListBox.Clear;
 end;
end;

{$R *.dfm}

end.
