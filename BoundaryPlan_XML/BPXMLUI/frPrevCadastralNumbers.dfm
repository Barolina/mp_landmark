object FramePrevSCadastralNumbers: TFramePrevSCadastralNumbers
  Left = 0
  Top = 0
  Width = 451
  Height = 304
  Align = alClient
  TabOrder = 0
  object PanelCentre: TPanel
    Left = 0
    Top = 0
    Width = 242
    Height = 304
    Align = alClient
    BevelEdges = []
    BevelKind = bkFlat
    BevelOuter = bvNone
    TabOrder = 0
    object CadNum_ListCN_ListBox: TListBox
      Left = 0
      Top = 21
      Width = 242
      Height = 283
      Align = alClient
      BevelEdges = [beLeft]
      BevelInner = bvNone
      BevelKind = bkFlat
      BevelOuter = bvRaised
      BorderStyle = bsNone
      Color = clBtnFace
      ItemHeight = 13
      MultiSelect = True
      TabOrder = 0
    end
    object CadNum_NewCN_MaskEdit: TMaskEdit
      Left = 0
      Top = 0
      Width = 242
      Height = 21
      Hint = #1050#1072#1076#1072#1089#1090#1088#1086#1074#1099#1081' '#1085#1086#1084#1077#1088
      Align = alTop
      BevelKind = bkFlat
      BorderStyle = bsNone
      Color = 15921123
      MaxLength = 40
      TabOrder = 1
      Text = ''
      ExplicitLeft = 1
      ExplicitTop = 1
      ExplicitWidth = 240
    end
  end
  object PanelLeft: TPanel
    Left = 242
    Top = 0
    Width = 209
    Height = 304
    Align = alRight
    BevelOuter = bvNone
    TabOrder = 1
    object CadNum_ButtonGroup_Actions: TButtonGroup
      Left = 0
      Top = 0
      Width = 209
      Height = 304
      Align = alClient
      BevelEdges = [beLeft, beTop, beRight]
      BevelInner = bvNone
      BevelOuter = bvRaised
      BevelKind = bkFlat
      BorderStyle = bsNone
      ButtonOptions = [gboFullSize, gboShowCaptions]
      Images = formContainer.ilCommands
      Items = <
        item
          Action = Action_DoAdd
          Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1074' '#1089#1087#1080#1089#1086#1082
        end
        item
          Action = Action_DelSel
          Caption = #1059#1076#1072#1083#1080#1090#1100' '#1074#1099#1076#1077#1083#1077#1085#1085#1099#1077
          ImageIndex = 1
        end
        item
          Action = Action_CopySel
          Caption = #1050#1086#1087#1080#1088#1086#1074#1072#1090#1100' '#1074#1099#1076#1077#1083#1077#1085#1086#1077
          ImageIndex = 3
        end
        item
          Action = Action_Paste
          Caption = #1042#1089#1090#1072#1074#1080#1090#1100' '#1089#1087#1080#1089#1086#1082' '#1080#1079' '#1073#1091#1092#1077#1088' '#1086#1073#1084#1077#1085#1072
          ImageIndex = 2
        end>
      TabOrder = 0
    end
  end
  object ActionList: TActionList
    Left = 648
    Top = 248
    object Action_DoAdd: TAction
      Caption = 'Action_DoAdd'
      ImageIndex = 0
      OnExecute = Action_DoAddExecute
    end
    object Action_DelSel: TAction
      Caption = 'Action_DelSel'
      ImageIndex = 2
      OnExecute = Action_DelSelExecute
    end
    object Action_CopySel: TAction
      Caption = 'Action_CopySel'
      ImageIndex = 1
      OnExecute = Action_CopySelExecute
    end
    object Action_Paste: TAction
      Caption = 'Action_Paste'
      ImageIndex = 3
      OnExecute = Action_PasteExecute
    end
  end
end
